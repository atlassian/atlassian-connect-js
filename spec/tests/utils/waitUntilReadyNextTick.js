export function waitUntilReadyNextTick(callback) {
  $(() => {
    setTimeout(callback, 1);
  })
}
