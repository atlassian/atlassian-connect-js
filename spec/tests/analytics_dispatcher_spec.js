import AnalyticsDispatcher from 'src/host/dispatchers/analytics_dispatcher';
import IframeActions from 'src/host/actions/iframe_actions';
import LoadingIndicatorActions from 'src/host/actions/loading_indicator_actions';
import HostApi from '../../src/host/host-api';
import { observeIframeRemoval } from 'src/host/utils/removal-observer';
import getBooleanFeatureFlag from '../../src/host/utils/feature-flag';

const extension = {
  id: 'xxxewjkd',
  addon_key: 'some-addon-key',
  key: 'some-module-key'
};



describe('Analytics Dispatcher', () => {
  beforeEach(() => {
    window.requestIdleCallback = (callback) => setTimeout(callback);
  });

  it('trackLoadingStarted stores the time', () => {
    expect(AnalyticsDispatcher._addons).toEqual({});
    AnalyticsDispatcher.trackLoadingStarted(extension);
    expect(AnalyticsDispatcher._addons[extension.id].startLoading).toEqual(jasmine.any(Number));
  });

  it('should consistently sample load events', () => {
    spyOn(AnalyticsDispatcher, '_shouldSampleEvent').and.returnValue(true);
    expect(AnalyticsDispatcher._shouldSampleLoadEvent({ id: '1234' })).toBe(true);

    AnalyticsDispatcher._shouldSampleEvent.and.returnValue(false);
    // Remember the result for the same ID
    expect(AnalyticsDispatcher._shouldSampleLoadEvent({ id: '1234' })).toBe(true);
    // A new ID should return false
    expect(AnalyticsDispatcher._shouldSampleLoadEvent({ id: '5678' })).toBe(false);
  })

  it('trackUseOfDeprecatedMethod triggers a gasV3 analytics call', () => {
    spyOn(AnalyticsDispatcher, '_trackAndForwardGasV3');
    var methodUsed = 'someDeprecatedMethodName';
    AnalyticsDispatcher.trackGasV3UseOfDeprecatedMethod(methodUsed, extension);
    expect(AnalyticsDispatcher._trackAndForwardGasV3).toHaveBeenCalledWith('operational', {
      action: 'deprecatedMethod',
      actionSubject: 'connectAddon',
      actionSubjectId: extension.addon_key,
      attributes: {
        methodUsed: methodUsed,
        moduleKey: extension.key
      },
      source: extension.addon_key
    });
  });

  it('dispatch/trackExternal triggers a gasV3 analytics call', () => {
    spyOn(AnalyticsDispatcher, '_trackAndForwardGasV3');
    var eventName = 'event.name';
    var eventValue = {
      addonKey: extension.addon_key,
      moduleKey: extension.key,
      value: 'something'
    };
    AnalyticsDispatcher.dispatch(eventName, eventValue);
    expect(AnalyticsDispatcher._trackAndForwardGasV3).toHaveBeenCalledWith('operational', {
      action: 'externalEvent',
      actionSubject: 'connectAddon',
      actionSubjectId: eventName,
      attributes: {
        eventName,
        addonKey: extension.addon_key,
        moduleKey: extension.key,
        value: 'something'
      },
      source: 'connectAddon'
    });

  });

  it('removes the timeout when the entry is removed from addon store', () => {
    AnalyticsDispatcher.trackLoadingStarted(extension);
    expect(AnalyticsDispatcher._addons[extension.id]).toBeDefined();
    let TimeoutId = AnalyticsDispatcher._addons[extension.id];
    spyOn(window, 'clearTimeout');

    AnalyticsDispatcher._resetAnalyticsDueToUnreliable(extension.id);
    expect(AnalyticsDispatcher._addons[extension.id]).toBeUndefined();
    expect(window.clearTimeout).toHaveBeenCalledWith(TimeoutId);
  });

  it('trackIframePerformanceMetrics triggers _analytics.trackIframePerformanceMetrics', () => {
    spyOn(AnalyticsDispatcher, '_shouldSampleEvent').and.returnValue(true);
    spyOn(AnalyticsDispatcher, '_trackGasV3');
    var metrics = {
      domainLookupTime: 111,
      connectionTime: 2222,
      decodedBodySize: 333,
      domContentLoadedTime: 444,
      fetchTime: 555
    };
    AnalyticsDispatcher.trackIframePerformance(metrics, Object.assign({}, extension, {options: {
      pearApp: 'true',
      moduleLocation: 'some-module-location',
      moduleType: 'some-module-type'
    }}));
    expect(AnalyticsDispatcher._trackGasV3).toHaveBeenCalled();
    expect(AnalyticsDispatcher._trackGasV3).toHaveBeenCalledWith('operational', {
      source: extension['addon_key'],
      action: 'iframeRendered',
      actionSubject: 'connectAddon',
      actionSubjectId: extension['addon_key'],
      attributes: {
        key: extension['key'],
        pearApp: true,
        domainLookupTime: metrics.domainLookupTime,
        connectionTime: metrics.connectionTime,
        decodedBodySize: metrics.decodedBodySize,
        domContentLoadedTime: metrics.domContentLoadedTime,
        fetchTime: metrics.fetchTime,
        moduleLocation: 'some-module-location',
        moduleType: 'some-module-type',
        iframeIsCacheable: false
      }
    });
  });

  it('on iframe viewed trigger a gasv3 analytics call', () => {
    spyOn(AnalyticsDispatcher, '_shouldSampleEvent').and.returnValue(true);
    spyOn(AnalyticsDispatcher, '_trackGasV3');
    const extension = {
      addon_key: 'some-addon-key',
      key: 'some-module-key',
      options: {
        moduleType: 'some-module-type',
        pearApp: 'true',
        moduleLocation: 'some-module-location'
      },
      id: 'some-addon-key__some-module-key_1y28nd',
      startLoading: Date.now()
    };
    AnalyticsDispatcher.trackGasV3Visible(extension);

    expect(AnalyticsDispatcher._trackGasV3).toHaveBeenCalledWith('operational', {
      source: 'some-addon-key',
      action: 'iframeViewed',
      actionSubject: 'connectAddon',
      actionSubjectId: 'some-addon-key',
      attributes: {
        iframeIsCacheable: false,
        moduleType: 'some-module-type',
        moduleKey: 'some-module-key',
        moduleLocation: 'some-module-location',
        pearApp: true
      }
    });
  });

  it('on method invoked trigger a gasv3 analytics call', () => {
    spyOn(AnalyticsDispatcher, '_shouldSampleEvent').and.returnValue(true);
    spyOn(AnalyticsDispatcher, '_trackAndForwardGasV3');
    const extension = {
      addon_key: 'some-addon-key',
      key: 'some-module-key',
      options: {
        moduleType: 'some-module-type',
        moduleLocation: 'some-module-location',
        pearApp: 'false',
      },
      id: 'some-addon-key__some-module-key_1y28nd',
    };
    const module = 'foo';
    const fn = 'bar';

    const contextFn = () => {};
    contextFn._context = {};

    const args = [
      undefined, null, 'some string', 1234, {}, [], () => {},
      // Undefined and the context function should be ignored, only at the end
      undefined, contextFn
    ]
    AnalyticsDispatcher.trackGasV3InvokeMethod(extension, module, fn, args);

    expect(AnalyticsDispatcher._trackAndForwardGasV3).toHaveBeenCalledWith(
      'operational',
      {
        action: 'invokeMethod',
        actionSubject: 'connectAddon',
        actionSubjectId: 'some-addon-key',
        attributes: {
          argCount: 7,
          argTypes: ['undefined', 'null', 'string', 'number', 'object', 'array', 'function'],
          method: 'bar',
          module: 'foo',
          moduleKey: 'some-module-key',
          moduleType: 'some-module-type',
          moduleLocation: 'some-module-location',
          pearApp: false,
        },
        source: 'some-addon-key',
      }
    );
  });

  it('should always trigger a gasv3 analytics call for allowlisted methods', () => {
    // Should still be called even with sampling returning false
    spyOn(AnalyticsDispatcher, '_shouldSampleEvent').and.returnValue(false);
    spyOn(AnalyticsDispatcher, '_trackAndForwardGasV3');
    const extension = {
      addon_key: 'some-addon-key',
      key: 'some-module-key',
      options: {
        moduleType: 'some-module-type',
        moduleLocation: 'some-module-location',
        pearApp: 'false',
      },
      id: 'some-addon-key__some-module-key_1y28nd',
    };
    const module = 'foo';
    const fn = 'saveMacro';

    AnalyticsDispatcher.trackGasV3InvokeMethod(extension, module, fn);

    expect(AnalyticsDispatcher._trackAndForwardGasV3).toHaveBeenCalledWith(
      'operational',
      {
        action: 'invokeMethod',
        actionSubject: 'connectAddon',
        actionSubjectId: 'some-addon-key',
        attributes: {
          argCount: 0,
          argTypes: [],
          method: 'saveMacro',
          module: 'foo',
          moduleKey: 'some-module-key',
          moduleType: 'some-module-type',
          moduleLocation: 'some-module-location',
          pearApp: false,
        },
        source: 'some-addon-key',
      }
    );
  });

  it('on iframe-bridge-established trigger a gasv3 analytics call', () => {
    spyOn(AnalyticsDispatcher, '_shouldSampleEvent').and.returnValue(true);
    spyOn(AnalyticsDispatcher, '_trackGasV3');
    const extension = {
      addon_key: 'some-addon-key',
      key: 'some-module-key',
      options: {
        moduleType: 'some-module-type',
        pearApp: 'true',
        moduleLocation: 'some-module-location',
        noDom: true,
      },
      id: 'some-addon-key__some-module-key_1y28nd',
      startLoading: Date.now()
    };
    AnalyticsDispatcher._addons[extension.id] = extension;

    IframeActions.notifyBridgeEstablished(document.createElement('div'), extension);

    expect(AnalyticsDispatcher._trackGasV3).toHaveBeenCalledWith('operational', {
      source: 'some-addon-key',
      action: 'iframeLoaded',
      actionSubject: 'connectAddon',
      actionSubjectId: 'some-addon-key',
      attributes: {
        iframeIsCacheable: false,
        iframeLoadMillis: jasmine.any(Number),
        iframeLoadBucket: jasmine.any(Number),
        moduleType: 'some-module-type',
        moduleKey: 'some-module-key',
        moduleLocation: 'some-module-location',
        pearApp: true
      }
    });
  });

  it('on iframe-bridge-timeout trigger a gasv3 analytics call', () => {
    spyOn(AnalyticsDispatcher, '_shouldSampleEvent').and.returnValue(true);
    spyOn(AnalyticsDispatcher, '_trackGasV3');
    const extension = {
      addon_key: 'some-addon-key',
      key: 'some-module-key',
      options: {
        moduleType: 'some-module-type',
        pearApp: 'true',
        moduleLocation: 'some-module-location',
        noDom: true
      },
      id: 'some-addon-key__some-module-key_1d28nd'
    };

    LoadingIndicatorActions.timeout(document.createElement('div'), extension);

    expect(AnalyticsDispatcher._trackGasV3).toHaveBeenCalledWith('operational', {
      source: 'some-addon-key',
      action: 'iframeTimeout',
      actionSubject: 'connectAddon',
      actionSubjectId: 'some-addon-key',
      attributes: {
        iframeIsCacheable: false,
        moduleType: 'some-module-type',
        moduleKey: 'some-module-key',
        moduleLocation: 'some-module-location',
        pearApp: true,
        connectedStatus: 'true'
      }
    });
  });

  it('web-vitals analytics call should be triggered', () => {
    spyOn(AnalyticsDispatcher, '_shouldSampleEvent').and.returnValue(true);
    spyOn(AnalyticsDispatcher, '_trackAndForwardGasV3');
    const extension = {
      addon_key: 'some-addon-key',
      key: 'some-module-key',
      options: {
        moduleType: 'some-module-type',
        pearApp: 'true',
        moduleLocation: 'some-module-location'
      },
    }

    AnalyticsDispatcher.trackGasV3WebVitals({
      'metric:cls': 0.1,
      'metric:fcp': 1000,
      'metric:lcp': 2000,
      'metric:ttfb': 3000,
    }, extension);

    expect(AnalyticsDispatcher._trackAndForwardGasV3).toHaveBeenCalledWith('operational', {
      source: 'some-addon-key',
      action: 'webVitals',
      actionSubject: 'connectAddon',
      actionSubjectId: 'some-addon-key',
      attributes: {
        'metric:cls': 0.1,
        'metric:fcp': 1000,
        'metric:lcp': 2000,
        'metric:ttfb': 3000,
        iframeIsCacheable: false,
        moduleType: 'some-module-type',
        key: 'some-module-key',
        moduleLocation: 'some-module-location',
        pearApp: true
      }});
  })

  it('should trigger analytics when statsig client is not initialized', () => {
    spyOn(AnalyticsDispatcher, '_trackAndForwardGasV3');
    getBooleanFeatureFlag('some-flag', true);
    expect(AnalyticsDispatcher._trackAndForwardGasV3).toHaveBeenCalledWith('operational', {
      action: 'returned',
      actionSubject: 'defaultFalseFeatureFlag',
      actionSubjectId: 'some-flag',
      attributes: {
        featureFlag: 'some-flag',
      },
      source: 'Host',
    });
  });
});

describe('observeIframes', () => {
  let observer;
  beforeEach(() => {
    // Mocking HostApi.destroy method
    spyOn(HostApi, 'destroy');

    // Pass the mockHostApi to the observeIframes function
    observer = observeIframeRemoval();
  });

  afterEach(() => {
    // Clean up after each test
    observer.disconnect();
  });

  it('should call HostApi.destroy when an iframe is removed', (done) => {
    // Create a test iframe with an ID and append it to the document
    const iframe = document.createElement('iframe');
    iframe.id = 'test-iframe';
    document.body.appendChild(iframe);

    // Remove the iframe and check if HostApi.destroy is called
    document.body.removeChild(iframe);

    // MutationObserver works asynchronously, so we need to wait for it
    setTimeout(() => {
      expect(HostApi.destroy).toHaveBeenCalledWith('test-iframe');
      done();
    }, 50);
  });

  it('should call HostApi.destroy for iframes within a removed node', (done) => {
    // Create a div containing an iframe
    const container = document.createElement('div');
    const nestedIframe = document.createElement('iframe');
    nestedIframe.id = 'nested-iframe';
    container.appendChild(nestedIframe);
    document.body.appendChild(container);

    // Remove the container div
    document.body.removeChild(container);

    // MutationObserver works asynchronously, so we need to wait for it
    setTimeout(() => {
      expect(HostApi.destroy).toHaveBeenCalledWith('nested-iframe');
      done();
    }, 50);
  });

  it('should not call HostApi.destroy if a non-iframe node is removed', (done) => {
    // Create a non-iframe element and append it to the document
    const div = document.createElement('div');
    div.id = 'test-div';
    document.body.appendChild(div);

    // Remove the non-iframe element
    document.body.removeChild(div);

    // MutationObserver works asynchronously, so we need to wait to ensure no calls were made
    setTimeout(() => {
      expect(HostApi.destroy).not.toHaveBeenCalled();
      done();
    }, 50);
  });
});
