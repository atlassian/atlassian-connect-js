import host from 'simple-xdm/host';
import plugin from 'simple-xdm/combined';
import ThemingHost, { ThemingModuleState } from '../../src/host/modules/theming';
import ThemingPlugin, {
  loadLegacyTextColorStylesCdn,
} from '../../src/plugin/theming';
import {
  resetFeatureFlags,
} from '../mocks/mock_feature_flag';

const lightTheme = {
  dark: 'dark',
  light: 'light',
  spacing: 'spacing',
  colorMode: 'light',
};

const darkTheme = {
  dark: 'dark',
  light: 'light',
  spacing: 'spacing',
  colorMode: 'dark',
};

function getFakeCallback(extensionId) {
  const fakeCallback = function () {};
  fakeCallback._context = { extension_id: extensionId || 'abc123' };

  return fakeCallback;
}

describe('theming module', () => {
  beforeEach(() => {
    document.documentElement.removeAttribute('data-theme');
    document.documentElement.removeAttribute('data-color-mode');
    document.documentElement.removeAttribute('data-surface');
    document.querySelectorAll('link[data-surface]').forEach((e) => e.remove());
    document.querySelectorAll('style[data-theme]').forEach((e) => e.remove());
    document.querySelectorAll('link[data-theme]').forEach((e) => e.remove());
    document.querySelectorAll('#atlassian-connect-dark-mode-fix').forEach((e) => e.remove());
    ThemingModuleState.extensionIds = [];
  });

  afterEach(() => {
    resetFeatureFlags()
  })

  describe('host', () => {
    it('should fetch theme and broadcast theme_initialized when initializeTheming is called with an extension ID', () => {
      const observerSpy = spyOn(ThemingModuleState.themeObserver, 'observe');
      const spy = spyOn(host, 'broadcast');

      const fakeCallback = getFakeCallback();

      document.documentElement.dataset.theme =
        'light:light dark:dark spacing:spacing typography:typography';
      document.documentElement.setAttribute('data-color-mode', 'dark');

      ThemingHost.initializeTheming(false, fakeCallback);

      expect(observerSpy).toHaveBeenCalledWith(document.documentElement);

      const initialTheme = darkTheme;

      expect(spy).toHaveBeenCalledWith(
        'theme_initialized',
        { id: fakeCallback._context.extension_id },
        { initialTheme, forceNoCleanup: false, featureFlags: {}, surface: undefined }
      );
    });

    it('should fetch theme and broadcast theme_initialized with forceNoCleanup', () => {
      const observerSpy = spyOn(ThemingModuleState.themeObserver, 'observe');
      const spy = spyOn(host, 'broadcast');
      const fakeCallback = getFakeCallback();

      document.documentElement.dataset.theme =
            'light:light dark:dark spacing:spacing typography:typography';
      document.documentElement.setAttribute('data-color-mode', 'dark');

      ThemingHost.initializeTheming(true, fakeCallback);

      expect(observerSpy).toHaveBeenCalledWith(document.documentElement);

      const initialTheme = darkTheme;

      expect(spy).toHaveBeenCalledWith(
        'theme_initialized',
        { id: fakeCallback._context.extension_id },
        {
          initialTheme,
          forceNoCleanup: true,
          surface: undefined,
          featureFlags: {},
        }
      );
    });

    it('should broadcast theme_initialized when initializeTheming is called more than once', () => {
      const observerSpy = spyOn(ThemingModuleState.themeObserver, 'observe');
      const spy = spyOn(host, 'broadcast');

      const fakeCallback = getFakeCallback();

      document.documentElement.dataset.theme =
        'light:light dark:dark spacing:spacing typography:typography';
      document.documentElement.setAttribute('data-color-mode', 'dark');

      ThemingHost.initializeTheming(fakeCallback);
      ThemingHost.initializeTheming(fakeCallback);

      // Should only observe once even though initializeTheming was called twice for the same ID
      expect(observerSpy).toHaveBeenCalledTimes(1);
      // Should only add the ID to the array once
      expect(ThemingModuleState.extensionIds.length).toBe(1);
      // Should still broadcast the event twice
      expect(spy).toHaveBeenCalledTimes(2);
    });

    it('should NOT broadcast theme_changed when the theme changes in the host if no extension has enabled theming', (done) => {
      document.documentElement.dataset.theme =
        'light:light dark:dark spacing:spacing typography:typography';
      document.documentElement.setAttribute('data-color-mode', 'light');

      const spy = spyOn(host, 'broadcast');

      document.documentElement.dataset.theme =
        'light:light dark:dark spacing:spacing typography:typography';
      document.documentElement.setAttribute('data-color-mode', 'dark');

      setTimeout(() => {
        expect(spy).not.toHaveBeenCalled();
        done();
      });
    });

    it('should broadcast theme_changed when the theme changes in the host and an extension has enabled theming', (done) => {
      const fakeCallback = getFakeCallback();

      document.documentElement.dataset.theme =
        'light:light dark:dark spacing:spacing typography:typography';
      document.documentElement.setAttribute('data-color-mode', 'light');

      ThemingHost.initializeTheming(fakeCallback);

      const spy = spyOn(host, 'broadcast');

      document.documentElement.dataset.theme =
        'light:light dark:dark spacing:spacing typography:typography';
      document.documentElement.setAttribute('data-color-mode', 'dark');

      const newTheme = darkTheme;

      setTimeout(() => {
        expect(spy).toHaveBeenCalledWith(
          'theme_changed',
          { id: fakeCallback._context.extension_id },
          {
            newTheme,
            featureFlags: {},
            surface: undefined,
          }
        );
        done();
      });
    });

    it('should broadcast theme_changed to all extensions that have enabled theming', (done) => {
      const fakeCallback1 = getFakeCallback();
      const fakeCallback2 = getFakeCallback('def456');

      document.documentElement.dataset.theme =
        'light:light dark:dark spacing:spacing typography:typography';
      document.documentElement.setAttribute('data-color-mode', 'light');

      ThemingHost.initializeTheming(fakeCallback1);
      ThemingHost.initializeTheming(fakeCallback2);

      const spy = spyOn(host, 'broadcast');

      document.documentElement.dataset.theme =
        'light:light dark:dark spacing:spacing typography:typography';
      document.documentElement.setAttribute('data-color-mode', 'dark');

      const newTheme = darkTheme;

      setTimeout(() => {
        expect(spy).toHaveBeenCalledTimes(2);
        expect(spy).toHaveBeenCalledWith(
          'theme_changed',
          { id: fakeCallback1._context.extension_id },
          {
            newTheme,
            featureFlags: {},
            surface: undefined,
          }
        );
        expect(spy).toHaveBeenCalledWith(
          'theme_changed',
          { id: fakeCallback2._context.extension_id },
          {
            newTheme,
            featureFlags: {},
            surface: undefined,
          }
        );
        done();
      });
    });
  });

  describe('plugin', () => {
    beforeEach(() => {
      plugin.theming = ThemingHost;
      ThemingPlugin.isThemingEnabled = false;
    });

    it('should register event listeners when initializeThemeListeners is called', () => {
      const spy = spyOn(plugin, 'register');

      ThemingPlugin.initializeThemeListeners();

      expect(spy).toHaveBeenCalledWith({
        theme_initialized: ThemingPlugin.onThemeInitialized,
        theme_changed: ThemingPlugin.onThemeChanged,
      });
    });

    it('should NOT register event listeners when initializeThemeListeners is called if theming is disabled', () => {
      plugin.theming = undefined;
      const spy = spyOn(plugin, 'register');

      ThemingPlugin.initializeThemeListeners();

      expect(spy).not.toHaveBeenCalled();
    });

    it('should set the initial theme when theming is initialized', async (done) => {
      const initialTheme = lightTheme;

      await ThemingPlugin.onThemeInitialized({ initialTheme });

      expect(document.documentElement.dataset.theme).toBe(
        'dark:dark light:light spacing:spacing'
      );
      expect(document.documentElement.getAttribute('data-color-mode')).toBe(
        'light'
      );
      done();
    });

    it('should set the new theme in the DOM when theme changes and theming is initialized', async (done) => {
      const initialTheme = lightTheme;

      await ThemingPlugin.onThemeInitialized({ initialTheme });

      expect(document.documentElement.dataset.theme).toBe(
        'dark:dark light:light spacing:spacing'
      );
      expect(document.documentElement.getAttribute('data-color-mode')).toBe(
        'light'
      );

      const newTheme = darkTheme;

      await ThemingPlugin.onThemeChanged({ newTheme });

      expect(document.documentElement.dataset.theme).toBe(
        'dark:dark light:light spacing:spacing'
      );
      expect(document.documentElement.getAttribute('data-color-mode')).toBe(
        'dark'
      );
      done();
    });

    it('should remove themes on the DOM when theming is disabled', async (done) => {
      const initialTheme = lightTheme;

      await ThemingPlugin.onThemeInitialized({ initialTheme });

      expect(document.documentElement.dataset.theme).toBe(
        'dark:dark light:light spacing:spacing'
      );
      expect(document.documentElement.getAttribute('data-color-mode')).toBe(
        'light'
      );

      const newTheme = {};

      await ThemingPlugin.onThemeChanged({ newTheme });

      expect(document.documentElement.hasAttribute('data-theme')).toBe(false);
      expect(document.documentElement.hasAttribute('data-color-mode')).toBe(
        false
      );

      done();
    });

    it('should not set up themes when initializeTheming is called but the host has theming disabled', async (done) => {
      const initialTheme = {};

      await ThemingPlugin.onThemeInitialized({ initialTheme });

      expect(document.documentElement.hasAttribute('data-theme')).toBe(false);
      expect(document.documentElement.hasAttribute('data-color-mode')).toBe(
        false
      );
      done();
    });

    it('should NOT set the new theme in the DOM when theme changes and theming is NOT initialized', async (done) => {
      const newTheme = darkTheme;

      await ThemingPlugin.onThemeChanged({ newTheme });

      expect(document.documentElement.dataset.theme).toBeUndefined();
      expect(
        document.documentElement.getAttribute('data-color-mode')
      ).toBeNull();
      done();
    });

    describe('Force no clean-up', () => {
      it('should set default themes on the DOM when theming is disabled by default', async (done) => {
        const initialTheme = {};

        await ThemingPlugin.onThemeInitialized({ initialTheme, forceNoCleanup: true });

        expect(document.documentElement.dataset.theme).toBe(
          'dark:dark light:light spacing:spacing'
        );
        expect(document.documentElement.getAttribute('data-color-mode')).toBe(
          'light'
        );

        done();
      });

      it('should set default themes on the DOM when theming is disabled', async (done) => {
        const initialTheme = lightTheme;

        await ThemingPlugin.onThemeInitialized({
          initialTheme,
          forceNoCleanup: true,
        });

        expect(document.documentElement.dataset.theme).toBe(
          'dark:dark light:light spacing:spacing'
        );
        expect(document.documentElement.getAttribute('data-color-mode')).toBe(
          'light'
        );

        const newTheme = {};

        await ThemingPlugin.onThemeChanged({ newTheme });

        expect(document.documentElement.dataset.theme).toBe(
          'dark:dark light:light spacing:spacing'
        );
        expect(document.documentElement.getAttribute('data-color-mode')).toBe(
          'light'
        );

        done();
      });
    });

    describe('should load stylesheets from the Connect CDN', () => {
      it('should set default themes on the DOM', async (done) => {
        const initialTheme = lightTheme;

        await ThemingPlugin.onThemeInitialized({
          initialTheme,
          featureFlags: {},
        });

        expect(
          document.querySelectorAll('link[data-theme="light"]').length
        ).toEqual(1);
        expect(document.documentElement.dataset.theme).toBe(
          'dark:dark light:light spacing:spacing'
        );
        expect(document.documentElement.getAttribute('data-color-mode')).toBe(
          'light'
        );

        done();
      });

      it('should handle theme changes from the parent app', async (done) => {
        const initialTheme = lightTheme;

        await ThemingPlugin.onThemeInitialized({
          initialTheme,
          featureFlags: {},
        });

        expect(
          document.querySelectorAll('link[data-theme="light"]').length
        ).toEqual(1);
        expect(document.documentElement.dataset.theme).toBe(
          'dark:dark light:light spacing:spacing'
        );
        expect(document.documentElement.getAttribute('data-color-mode')).toBe(
          'light'
        );

        const newTheme = darkTheme;

        await ThemingPlugin.onThemeChanged({
          newTheme,
          featureFlags: {},
        });

        expect(
          document.querySelectorAll('link[data-theme="dark"]').length
        ).toEqual(1);
        expect(document.documentElement.dataset.theme).toBe(
          'dark:dark light:light spacing:spacing'
        );
        expect(document.documentElement.getAttribute('data-color-mode')).toBe(
          'dark'
        );

        done();
      });

      it('should clean up themes when parent app disables theming', async (done) => {
        const initialTheme = lightTheme;

        await ThemingPlugin.onThemeInitialized({
          initialTheme,
          featureFlags: {},
        });

        expect(
          document.querySelectorAll('link[data-theme="light"]').length
        ).toEqual(1);
        expect(document.documentElement.dataset.theme).toBe(
          'dark:dark light:light spacing:spacing'
        );
        expect(document.documentElement.getAttribute('data-color-mode')).toBe(
          'light'
        );

        const newTheme = {};

        await ThemingPlugin.onThemeChanged({
          newTheme,
          featureFlags: {},
        });

        expect(document.documentElement.dataset.theme).toBeUndefined();
        expect(
          document.documentElement.getAttribute('data-color-mode')
        ).toBeNull();

        done();
      });
    });

    describe('Surface detection', () => {
      it('should apply supplied surface level', async (done) => {
        await ThemingPlugin.onThemeInitialized({
          initialTheme: lightTheme,
          surface: 'raised',
        });

        expect(document.querySelector('link[data-surface]')).not.toBeFalsy();
        expect(document.documentElement.getAttribute('data-surface')).toContain('raised');
        done();
      });

      it('should remove surface level attr when forceNoCleanup is true (but keep styles)', async (done) => {
        await ThemingPlugin.onThemeInitialized({
          initialTheme: undefined,
          surface: undefined,
          forceNoCleanup: true,
        });

        expect(document.documentElement.getAttribute('data-surface')).toBeNull();
        done();
      });

      it('should remove surface level on clean up', async (done) => {
        await ThemingPlugin.onThemeInitialized({
          initialTheme: lightTheme,
          surface: 'raised',
        });

        expect(document.querySelector('link[data-surface]')).not.toBeFalsy();
        expect(document.documentElement.getAttribute('data-surface')).toContain('raised');

        await ThemingPlugin.onThemeChanged({
          newTheme: undefined,
          surface: undefined,
        });

        expect(document.documentElement.getAttribute('data-surface')).toBeNull();

        done();
      });

      it('should not apply supplied surface level if omitted', async (done) => {
        await ThemingPlugin.onThemeInitialized({ initialTheme: lightTheme });

        expect(
          document.documentElement.getAttribute('data-surface')
        ).toBeNull();
        expect(document.querySelector('link[data-surface]')).toBeNull();
        done();
      });

      it('should not apply supplied surface level if undefined', async (done) => {
        await ThemingPlugin.onThemeInitialized({
          initialTheme: lightTheme,
          surface: undefined,
        });
        expect(
          document.documentElement.getAttribute('data-surface')
        ).toBeNull();
        expect(document.querySelector('link[data-surface]')).toBeNull();
        done();
      });

      it('should apply supplied surface level on theme change', async (done) => {
        await ThemingPlugin.onThemeInitialized({
          initialTheme: lightTheme,
          surface: undefined,
        });
        expect(
          document.documentElement.getAttribute('data-surface')
        ).toBeNull();
        expect(document.querySelector('link[data-surface]')).toBeNull();

        await ThemingPlugin.onThemeChanged({
          newTheme: darkTheme,
          surface: 'overlay',
        });

        expect(document.querySelector('link[data-surface]')).not.toBeFalsy();
        expect(document.documentElement.getAttribute('data-surface')).toContain('overlay');

        done();
      });

      it('should apply supplied surface level on theme change', async (done) => {
        await ThemingPlugin.onThemeInitialized({
          initialTheme: lightTheme,
          surface: 'raised',
        });

        expect(document.querySelector('link[data-surface]')).not.toBeFalsy();
        expect(document.documentElement.getAttribute('data-surface')).toContain('raised');

        await ThemingPlugin.onThemeChanged({
          newTheme: darkTheme,
          surface: 'overlay',
        });

        expect(document.documentElement.getAttribute('data-surface')).toContain('overlay');

        done();
      });

      it('should remove surface level when theming is disabled', async (done) => {
        await ThemingPlugin.onThemeInitialized({
          initialTheme: lightTheme,
          surface: 'overlay',
        });

        expect(document.querySelector('link[data-surface]')).not.toBeFalsy();
        expect(document.documentElement.getAttribute('data-surface')).toContain('overlay');

        await ThemingPlugin.onThemeChanged({ newTheme: darkTheme });

        expect(document.documentElement.getAttribute('data-surface')).toBeNull();

        done();
      });
    });
  });

  describe('legacy text colors', () => {
    it('should append legacy text colors styles tag', async (done) => {
      await loadLegacyTextColorStylesCdn()
      expect(document.querySelector('link[data-legacy-text-colors]')).toBeTruthy()
      done()
    });
  })
});
