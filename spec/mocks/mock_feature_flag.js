/**
 * @param name {string}
 * @param value {boolean}
 */
export function mockFeatureFlag(name, value) {
  window.featureFlags = window.featureFlags || {}
  window.featureFlags[name] = { value }
}
export function resetFeatureFlags() {
  window.featureFlags = {}
}

let mockIframeFlags = {}
export function mockIframeFeatureFlag(name, value) {
  mockIframeFlags[name] = value
  window.AP = window.AP || {}
  window.AP._featureFlag = window.AP._featureFlag || {}
  window.AP._featureFlag.getBooleanFeatureFlag = window.AP._featureFlag.getBooleanFeatureFlag || function() {}
  window.AP._featureFlag.getBooleanFeatureFlag = async (flagName) => mockIframeFlags[flagName]
}
export function resetIframeFeatureFlags() {
  mockIframeFlags = {}
}
