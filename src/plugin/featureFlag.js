import AP from 'simple-xdm/combined';

export const ECOSYSTEM_PFF_GLOBAL_KEY = '__ECOSYSTEM_PLATFORM_FEATURE_FLAGS__';

const allowedPlatformFeatureFlags = ['platform-visual-refresh-icons']

// Sets feature flags in the iframe
export const setPlatformFeatureFlags = () => {
  window[ECOSYSTEM_PFF_GLOBAL_KEY] = {};
  if (AP._featureFlag && AP._featureFlag.getBooleanFeatureFlag) {
    for (const flag of allowedPlatformFeatureFlags) {
      AP._featureFlag.getBooleanFeatureFlag(flag).then((value) => {
        window[ECOSYSTEM_PFF_GLOBAL_KEY][flag] = value;
      }).catch((err) => {
        console.error(err);
      })
    }
  }
}

// Helper to get a boolean feature flag in the iframe
export async function getBooleanFeatureFlagIframe(flagName) {
  if (AP._featureFlag && AP._featureFlag.getBooleanFeatureFlag) {
    return AP._featureFlag.getBooleanFeatureFlag(flagName);
  }
  return false;
}
