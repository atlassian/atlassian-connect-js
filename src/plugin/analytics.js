import AP from 'simple-xdm/combined';
import {onCLS, onFCP, onLCP, onTTFB} from 'web-vitals';

function getMetrics() {
  if (window.performance && window.performance.getEntries) {
    let navigationEntries = window.performance.getEntriesByType('navigation');
    if (navigationEntries && navigationEntries[0]) {
      let timingInfo = navigationEntries[0];
      // dns loookup time
      let domainLookupTime = timingInfo.domainLookupEnd - timingInfo.domainLookupStart;
      let connectStart = timingInfo.connectStart;
      // if it's a tls connection, use the secure connection start instead
      if (timingInfo.secureConnectionStart > 0) {
        connectStart = timingInfo.secureConnectionStart;
      }
      // connection negotiation time
      let connectionTime = timingInfo.connectEnd - connectStart;
      // page body size
      let decodedBodySize = timingInfo.decodedBodySize;
      // time to load dom
      let domContentLoadedTime = timingInfo.domContentLoadedEventEnd - timingInfo.domContentLoadedEventStart;
      // time to download the page
      let fetchTime = timingInfo.responseEnd - timingInfo.fetchStart;

      return {
        domainLookupTime,
        connectionTime,
        decodedBodySize,
        domContentLoadedTime,
        fetchTime
      }
    }
  }
}

function sendMetrics() {
  let metrics = getMetrics();
  if (AP._analytics && AP._analytics.trackIframePerformanceMetrics) {
    AP._analytics.trackIframePerformanceMetrics(metrics);
  }
}


function setupWebVitals(timeout) {
  let metrics = {};

  const collectMetrics = (source) => {
    if (Object.keys(metrics).length > 0) {
      console.log(`[web-vitals] Sending metrics triggered by: ${source}`, metrics);
      if (AP._analytics && AP._analytics.trackWebVitals) {
        AP._analytics.trackWebVitals(metrics);
        metrics = {};
      }
    }
  }

  // Report all available metrics whenever the page is backgrounded or unloaded.
  // https://github.com/GoogleChrome/web-vitals/blob/main/README.md#batch-multiple-reports-together
  addEventListener('visibilitychange', () => {
    if (document.visibilityState === 'hidden') {
      collectMetrics('visibility change');
    }
  });

  Promise.all([onCLS, onFCP, onLCP, onTTFB].map(collector => new Promise(resolve => {
    collector(metric => {
      const {name, value} = metric;
      // Copied from https://bitbucket.org/atlassian/atlassian-frontend-monorepo/src/master/platform/packages/performance/browser-metrics/src/observer/web-vitals-observer.ts#web-vitals-observer.ts-36
      // round FCP, LCP, TTFB to nearest integer.
      // CLS is rounded to two decimal places.
      const metricName = name.toLowerCase();
      metrics[`metric:${metricName}`] = metricName === 'cls' ? parseFloat(value.toFixed(2)) : Math.round(value);
      resolve();
    });
  }))).then(() => collectMetrics('all collected'));

  // Collect any metrics we can after the timeout, if they haven't already been collected.
  setTimeout(() => collectMetrics('timeout'), timeout);
}

export default {
  sendMetrics,
  setupWebVitals
}
