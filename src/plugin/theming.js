import AP from 'simple-xdm/combined';
import { setGlobalTheme } from '@atlaskit/tokens';

function appendStyle(id, url, attr = 'theme') {
  const link = document.createElement('link');
  link.rel = 'stylesheet';
  link.href = url;
  link.dataset[attr] = id;

  document.head.appendChild(link);
}

function themeLoader(id) {
  const stylesheetUrl = `https://connect-cdn.atl-paas.net/themes/atlaskit-tokens_${id}.css`;

  if (document.querySelector(`link[href="${stylesheetUrl}"]`)) {
    return;
  }

  appendStyle(id, stylesheetUrl);
}

function appendSurface(surface) {
  document.documentElement.dataset.surface = surface;

  if (document.querySelector('link[data-surface]')) {
    return;
  }

  appendStyle(surface, 'https://connect-cdn.atl-paas.net/surfaces.css', 'surface');
}

function removeSurface() {
  delete document.documentElement.dataset.surface;
}

export async function loadLegacyTextColorStyles() {
  if (AP._data.options.moduleType !== 'dynamicContentMacros') {
    return;
  }
  loadLegacyTextColorStylesCdn(); // Load via Connect CDN stylesheet
}

export function loadLegacyTextColorStylesCdn() {
  if (document.querySelector('link[data-legacy-text-colors]')) {
    return;
  }
  appendStyle('legacy-text-colors', 'https://connect-cdn.atl-paas.net/legacy-text-colors.css', 'legacyTextColors');
}

async function setTheme(options) {
  await setGlobalTheme(options, themeLoader); // Load via Connect CDN stylesheet
}

let hasRegisteredCSPViolationHandler = false;
function registerCSPViolationHandler() {
  if (hasRegisteredCSPViolationHandler) {
    return;
  }
  hasRegisteredCSPViolationHandler = true;

  window.addEventListener('securitypolicyviolation', (event) => {
    if (event.blockedURI.includes('connect-cdn.atl-paas.net')) {
      console.warn(`CSP violation detected for ${event.violatedDirective} in app ${event.documentURI}
Please add https://connect-cdn.atl-paas.net to your security policy for script-src and style-src.
See https://developer.atlassian.com/platform/marketplace/security-requirements/#connect-apps for details.`);
    }
  });

}

const Theming = {
  isThemingEnabled: false,
  forceNoCleanup: false,

  async onThemeInitialized(data) {
    Theming.isThemingEnabled = true;
    Theming.forceNoCleanup = data.forceNoCleanup || false;
    registerCSPViolationHandler()
    await Theming.onThemeChanged(data);
    if (window.AP && window.AP.theming && window.AP.theming._finishedInitTheming) {
      window.AP.theming._finishedInitTheming();
    }
  },

  async onThemeChanged(data) {
    if (!Theming.isThemingEnabled) {
      return;
    }

    const newTheme = data.initialTheme || data.newTheme;

    if (!newTheme || Object.keys(newTheme).length === 0) {
      if (Theming.forceNoCleanup) {
        await setTheme({ colorMode: 'light' });
        removeSurface();
        return;
      }

      removeSurface();
      document.documentElement.removeAttribute('data-theme');
      document.documentElement.removeAttribute('data-color-mode');
      return;
    }

    await setTheme(newTheme);

    // Remove existing surface styles.
    removeSurface();
    if (!!data.surface) {
      // Surface styles must be applied after themes for specificity.
      appendSurface(data.surface);
    }
  },

  initializeThemeListeners() {
    if (!AP.theming) {
      return;
    }

    AP.register({
      theme_initialized: Theming.onThemeInitialized,
      theme_changed: Theming.onThemeChanged,
    });
  },
};

export default Theming;
