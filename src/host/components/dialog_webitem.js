import WebItemActions from '../actions/webitem_actions';
import EventDispatcher from '../dispatchers/event_dispatcher';
import WebItemUtils from '../utils/webitem';
import DialogExtensionActions from '../actions/dialog_extension_actions';
import $ from '../dollar';
import Util from '../util';
import {Flags} from '../utils/feature-flag';

const ITEM_NAME = 'dialog';
const SELECTOR = '.ap-dialog';
const TRIGGERS = ['click'];
const WEBITEM_UID_KEY = 'dialog-target-uid';
const DEFAULT_WEBITEM_OPTIONS = {
  chrome: true
};

class DialogWebItem {
  constructor(){
    this._dialogWebItem = {
      name: ITEM_NAME,
      selector: SELECTOR,
      triggers: TRIGGERS
    };
  }

  getWebItem(){
    return this._dialogWebItem;
  }

  _dialogOptions(options){
    return Util.extend({}, DEFAULT_WEBITEM_OPTIONS, options || {});
  }

  /**
   * @param {WebItemInvokedEventData} data
   */
  triggered(data) {
    const $target = data.$target;

    var webitemId = $target.data(WEBITEM_UID_KEY);
    var dialogOptions = this._dialogOptions(data.extension.options);
    dialogOptions.id = webitemId;
    DialogExtensionActions.open(data.extension, dialogOptions);
  }

  /**
   * @param {WebItemInvokedEventData} data
   */
  createIfNotExists(data) {
    const $target = data.$target;
    var uid = $target.data(WEBITEM_UID_KEY);

    if(!uid) {
      uid = WebItemUtils.uniqueId();
      $target.data(WEBITEM_UID_KEY, uid);
    }
  }

}

let dialogInstance = new DialogWebItem();
let webitem = dialogInstance.getWebItem();
EventDispatcher.register('webitem-invoked:' + webitem.name, function(data){
  dialogInstance.triggered(data);
});
EventDispatcher.register('before:webitem-invoked:' + webitem.name, dialogInstance.createIfNotExists);

WebItemActions.addWebItem(webitem);
export default dialogInstance;
