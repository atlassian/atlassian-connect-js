import $ from '../dollar';
import EventDispatcher from '../dispatchers/event_dispatcher';
import WebItemActions from '../actions/webitem_actions';
import WebItemUtils, {collectParentElementAttributes, isDialogTrigger, isInlineDialogTrigger} from '../utils/webitem';
import Util from '../util';
import HostApi from '../host-api';

const addedTriggersSet = new WeakSet();
class WebItem {

  constructor() {
    this._webitems = {};
    this._contentResolver = function noop(){};
  }

  setContentResolver(resolver) {
    this._contentResolver = resolver;
  }

  requestContent(extension) {
    if(extension.addon_key && extension.key) {
      return this._contentResolver.call(null, Util.extend({classifier: 'json'}, extension));
    }
  }
  // originally i had this written nicely with Object.values but
  // ie11 didn't like it and i couldn't find a nice pollyfill
  getWebItemsBySelector(selector) {
    let returnVal;
    const keys = Object.getOwnPropertyNames(this._webitems).some((key) => {
      let obj = this._webitems[key];
      if(obj.selector) {
        if(obj.selector.trim() === selector.trim()) {
          returnVal = obj;
          return true;
        }
      };
      return false;
    });
    return returnVal;
  }

  setWebItem(potentialWebItem) {
    return this._webitems[potentialWebItem.name] = {
      name: potentialWebItem.name,
      selector: potentialWebItem.selector,
      triggers: potentialWebItem.triggers
    };

  }

  _removeTriggers(webitem) {
    var onTriggers = WebItemUtils.sanitizeTriggers(webitem.triggers);
    $(() => {
      $('body').off(onTriggers, webitem.selector, this._webitems[webitem.name]._on);
    });
    delete this._webitems[webitem.name]._on;
  }

  _addTriggers (webitem) {
    var onTriggers = WebItemUtils.sanitizeTriggers(webitem.triggers);
    /**
     * @param event {Event}
     */
    webitem._on = (event) => {
      if (event.defaultPrevented) {
        return;
      }
      /** @type {JQuery} */
      const $target = $(event.target).closest(webitem.selector);
      if ($target.hasClass('ap-dialog-ignore')) {
        return;
      }
      event.preventDefault();
      _triggerWebItem($target, event.type);
    };
    $(() => {
      const attachHandlers = (node) => {
        if (addedTriggersSet.has(node)) {
          return;
        }
        addedTriggersSet.add(node);
        $(node).on(onTriggers, webitem._on);
      }
      // First, add handlers for any nodes already in the document
      document.querySelectorAll(webitem.selector).forEach((node) => {
        attachHandlers(node)
      })
      // Next, set up a MutationObserver to add handlers for any nodes added to the document later
      const observer = new MutationObserver(function(mutationsList, observer) {
        mutationsList.forEach(function(mutation) {
          if (mutation.type !== 'childList') {
            return;
          }
          mutation.addedNodes.forEach(function (node) {
            if (node.nodeType !== Node.ELEMENT_NODE) {
              return;
            }
            if (node.matches(webitem.selector)) {
              // The element is being added directly
              attachHandlers(node);
            } else {
              // We need to check children of the added node as well, because a whole subtree can be added at once
              node.querySelectorAll(webitem.selector).forEach((childNode) => {
                attachHandlers(childNode);
              })
            }
          });
        })
      });
        // Start observing the target node for configured mutations
      observer.observe(document.body, { childList: true, subtree: true });

      // Append styles to make the webitem clickable
      $('head').append(`<style type="text/css">${webitem.selector}.ap-link-webitem {pointer-events: auto;cursor: pointer;}</style>`);
    });
  }

}

/**
 * @param {Element} target  The webItem trigger element, used for relatively positioning the inline dialog popup.
 * @param {string|undefined} [cssClass] The webItem's class name as returned by the Connect plugin. Will be taken from the target if not provided.
 * @param {string|undefined} [href] The webItem's href as returned by the Connect plugin. Will be taken from the target if not provided.
 * @param {'click' | 'mouseenter'} [eventType] The trigger type that we want to invoke.
 */
export function triggerWebItem(
  target,
  cssClass,
  href,
  eventType,
) {
  eventType = eventType || 'click';
  const $target = $(target);
  cssClass = cssClass || $target.attr('class');

  // ACJS would only listen for events on these specific classes.
  // However, this function could be called for any webItem, even if it is not a dialog.
  // In that case, we should just do nothing and let the default link behaviour occur.
  if (!isDialogWebItem(cssClass) || (eventType === 'mouseenter' && !isInlineDialogTrigger(cssClass))) {
    return;
  }
  _triggerWebItem($target, eventType, cssClass, href, true);
}

/**
 * We export this function so that consumers can check if Connect will handle the event.
 * If so, they can prevent the default behaviour.
 * @param {string|string[]|null} classes
 * @returns {boolean}
 */
export function isDialogWebItem(classes) {
  return isDialogTrigger(classes) || isInlineDialogTrigger(classes);
}

/**
 * @private
 * @param {JQuery} $target The webItem trigger element. It is expected to have an href or data-href property, and class names representing the trigger type (ap-dialog, ap-inline-dialog, etc).
 * @param {'click' | 'mouseenter'} eventType The trigger type that we want to invoke.
 * @param {string|undefined} [cssClass] The webItem's class name as returned by the Connect plugin.
 * @param {string|undefined} [href] The webItem's href as returned by the Connect plugin.
 * @param {boolean} isModernApi Whether this was triggered via the modern imperative API, used for analytics.
 */
function _triggerWebItem(
  $target,
  eventType,
  cssClass,
  href,
  isModernApi
) {
  cssClass = cssClass || $target.attr('class');
  isModernApi = isModernApi || false;
  const convertedOptions = WebItemUtils.getConfigFromTarget($target, cssClass, href);
  const extensionUrl = convertedOptions && convertedOptions.url ? convertedOptions.url : undefined;
  const extension = {
    addon_key: WebItemUtils.getExtensionKey(cssClass),
    key: WebItemUtils.getKey(cssClass),
    options: WebItemUtils.getOptionsForWebItem($target, cssClass, href),
    url: extensionUrl
  };

  if (extension.addon_key === 'com.addonengine.analytics' && !HostApi.isModuleDefined('analytics')) {
    console.log(`ACJS-1164 Dropping event ${eventType} for plugin ${extension.addon_key} until AP.analytics loads...`);
    return;
  }
  const isInlineDialog = isInlineDialogTrigger(cssClass);
  if (!isModernApi) {
    EventDispatcher.dispatch('analytics-legacy-dialog-opened', {
      extension,
      eventType,
      parentIds: collectParentElementAttributes($target.get(0), [
        'data-testid',
        'data-component-selector',
        'data-fullscreen-id'
      ]),
    });
  }
  WebItemActions.webitemInvoked(isInlineDialog, {
    $target,
    eventType,
    extension,
  })
}

var webItemInstance = new WebItem();

EventDispatcher.register('webitem-added', (data) => {
  webItemInstance._addTriggers(data.webitem);
});

EventDispatcher.register('content-resolver-register-by-extension', function(data){
  webItemInstance.setContentResolver(data.callback);
});

document.addEventListener('aui-responsive-menu-item-created', (e) => {
  var oldWebItem = e.detail.originalItem.querySelector('a[class*="ap-"]');
  if (oldWebItem) {
    var newWebItem = e.detail.newItem.querySelector('a');
    let classList = [].slice.call(oldWebItem.classList);
    classList.forEach(cls => {
      if (/^ap-/.test(cls)) {
        newWebItem.classList.add(cls);
      }
    });
  }
});

export default webItemInstance;
