import WebItemActions from '../actions/webitem_actions';
import InlineDialogWebItemActions from '../actions/inline_dialog_webitem_actions';
import EventDispatcher from '../dispatchers/event_dispatcher';
import InlineDialogComponent from './inline_dialog';
import WebitemComponent from './webitem';
import WebItemUtils from '../utils/webitem';
import IframeContainer from './iframe_container';
import $ from '../dollar';
import IframeCreate from '../iframe-create';
import Util from '../util';
import urlUtils from '../utils/url';
import { getAccessNarrowingSpaceContext } from '../utils/access-narrowing-context';

const ITEM_NAME = 'inline-dialog';
const SELECTOR = '.ap-inline-dialog';
const WEBITEM_UID_KEY = 'inline-dialog-target-uid';

export class InlineDialogWebItem {
  constructor(){
    this._inlineDialogWebItemSpec = {
      name: ITEM_NAME,
      selector: SELECTOR,
      triggers: ['mouseenter', 'click']
    };
    this._inlineDialogWebItems = {};
  }

  getWebItem(){
    return this._inlineDialogWebItemSpec;
  }

  _createInlineDialog(data){
    var $inlineDialog = InlineDialogComponent.render({
      extension: data.extension,
      id: data.id,
      bindTo: data.$target,
      $content: $('<div />'),
      inlineDialogOptions: data.extension.options
    });

    return $inlineDialog;
  }

  /**
   * @param {WebItemInvokedEventData} data
   */
  triggered(data) {
    // don't trigger on hover, when hover is not specified.
    const eventType = data.eventType;
    const onHover = data.extension.options.onHover;
    if (eventType !== 'click' && !onHover) {
      return;
    }
    const $target = data.$target;
    var webitemId = $target.data(WEBITEM_UID_KEY);

    EventDispatcher.dispatch('analytics-inline-dialog-opened', {
      extension: data.extension,
      eventType: eventType,
      onHover: onHover
    })

    if (eventType === 'click' && !onHover) {
      if ($target.attr('data-inline-created') === 'true') {
        // Let AUI handle the click event, otherwise we would destroy and recreate the dialog
        // This will lead to the dialog being hidden immediately after it was opened
        return;
      } else {
        // We have to handle the first click event to set up the dialog
        // We set this property to ignore subsequent clicks and let AUI handle them
        $target.attr('data-inline-created', 'true');
      }
    }

    var $inlineDialog = this._createInlineDialog({
      id: webitemId,
      extension: data.extension,
      $target: $target,
      options: data.extension.options || {}
    });

    $inlineDialog.show();
  }

  opened(data){
    var $existingFrame = data.$el.find('iframe');
    var isExistingFrame = ($existingFrame && $existingFrame.length === 1);
    // existing iframe is already present and src is still valid (either no jwt or jwt has not expired).
    if(isExistingFrame){
      const src = $existingFrame.attr('src');
      const srcPresent = (src.length > 0);
      if(srcPresent) {
        const srcHasJWT = urlUtils.hasJwt(src);
        const srcHasValidJWT = srcHasJWT && !urlUtils.isJwtExpired(src);
        if(srcHasValidJWT || !srcHasJWT) {
          return false;
        }
      }
    }
    const dialogExtension = getAccessNarrowingSpaceContext(data.extension);
    const contentRequest = WebitemComponent.requestContent(dialogExtension);
    if(!contentRequest){
      console.warn('no content resolver found');
      return false;
    }
    contentRequest.then(function(content){
      content.options = content.options || {};
      Util.extend(content.options, {
        autoresize: true,
        widthinpx: true
      });

      InlineDialogWebItemActions.addExtension({
        $el: data.$el,
        extension: content
      });
    });
    return true;
  }

  addExtension(data){
    var addon = IframeCreate(data.extension);
    data.$el.empty().append(addon);
  }

  /**
   * @param {WebItemInvokedEventData} data
   */
  createIfNotExists(data) {
    const $target = data.$target;
    var uid = $target.data(WEBITEM_UID_KEY);

    if(!uid) {
      uid = WebItemUtils.uniqueId();
      $target.data(WEBITEM_UID_KEY, uid);
    }
  }

}

let inlineDialogInstance = new InlineDialogWebItem();
let webitem = inlineDialogInstance.getWebItem();
EventDispatcher.register('before:webitem-invoked:' + webitem.name, function(data){
  inlineDialogInstance.createIfNotExists(data);
});
EventDispatcher.register('webitem-invoked:' + webitem.name, function(data){
  inlineDialogInstance.triggered(data);
});
EventDispatcher.register('inline-dialog-opened', function(data){
  inlineDialogInstance.opened(data);
});
EventDispatcher.register('inline-dialog-extension', function(data){
  inlineDialogInstance.addExtension(data);
});
WebItemActions.addWebItem(webitem);

export default inlineDialogInstance;
