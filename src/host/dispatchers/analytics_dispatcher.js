import EventDispatcher from './event_dispatcher';
import $ from '../dollar';
import observe from '../utils/observe';

/**
 * Timings beyond 20 seconds (connect's load timeout) will be clipped to an X.
 * @const
 * @type {int}
 */
const LOADING_TIME_THRESHOLD = 20000;

/**
 * Trim extra zeros from the load time.
 * @const
 * @type {int}
 */
const LOADING_TIME_TRIMP_PRECISION = 100;

/**
 * @param {number} value
 * @returns {number}
 */
function bucketLoadingTime(value) {
  const bucket = value > LOADING_TIME_THRESHOLD ? LOADING_TIME_THRESHOLD : value;
  return (Math.ceil(bucket / LOADING_TIME_TRIMP_PRECISION) * LOADING_TIME_TRIMP_PRECISION)
}

// Only send 0.1% of content resolver events
const CONTENT_RESOLVER_SAMPLE_RATE = 0.1;

// Only send 0.1% of invoke method events
const INVOKE_METHOD_SAMPLE_RATE = 0.1;


const ALLOWLISTED_METHODS = [
  'saveMacro',
]


class AnalyticsDispatcher {

  constructor() {
    this._addons = {};
    this._sampleLoadEvent = new Map();
  }

  /**
   * Determines whether a sampled event should be sent based on given rate.
   * @param {int} rate Rate as an integer percentage.
   * @returns {boolean}
   */
  _shouldSampleEvent(rate) {
    return Math.random() * 100 <= rate
  }

  /**
   * We want to keep sampling consistent across the various events that are sent for a single iframe load.
   * @param extension
   * @returns {boolean}
   */
  _shouldSampleLoadEvent(extension) {
    // Only send 0.01% of iframe load events
    const IFRAME_LOAD_SAMPLE_RATE = 0.01;
    if (this._sampleLoadEvent.has(extension.id)) {
      return this._sampleLoadEvent.get(extension.id);
    }
    const shouldSample = this._shouldSampleEvent(IFRAME_LOAD_SAMPLE_RATE);
    this._sampleLoadEvent.set(extension.id, shouldSample);
    return shouldSample;
  }

  /**
   * Track an event via GasV3.
   *
   * Warning! Confluence does not use ac/analytics, we have to manually forward it
   * The legacy way of doing this was using individual hooks for each event. Many existing events have dedicated hooks.
   * If you are creating a new event, you can use sendExternal=true to avoid the need for a dedicated hook.
   *
   * https://stash.atlassian.com/projects/ATLASSIAN/repos/atlassian-frontend-monorepo/browse/confluence/packages/confluence-connect-support/src/ConnectAnalyticsPublishSupport.js
   * @param eventType {string}
   * @param event {object}
   * @param sendExternal {boolean}
   * @private
   */
  _trackGasV3(eventType, event, sendExternal) {
    if (event.attributes) {
      event.attributes.apVersion = (window._AP && window._AP.version) ? window._AP.version : undefined;
    }
    if (sendExternal) {
      this._forwardGasV3AnalyticsEvent(eventType, event);
    }

    try {
      const analyticsCrossProduct = window.require('ac/analytics');
      analyticsCrossProduct.emitGasV3(eventType, event);
    } catch (e) {
      if (!window.Confluence && !window.__karma__) {
        // this is not serious. It usually means the product is doing analytics using another mechanism
        // Confluence no longer uses ac/analytics so we should stop reporting it as an error
        console.info('Connect GasV3 catch', e);
      }
    }
  }

  _trackAndForwardGasV3(eventType, event) {
    this._trackGasV3(eventType, event, true);
  }

  _forwardGasV3AnalyticsEvent(eventType, event) {
    EventDispatcher.dispatch('analytics-forward-event', eventType, event)
  }

  _time() {
    return window.performance && window.performance.now ? window.performance.now() : new Date().getTime();
  }

  trackLoadingStarted(extension) {
    if (this._addons && extension && extension.id) {
      extension.startLoading = this._time();
      this._addons[extension.id] = extension;
    } else {
      console.error('ACJS: cannot track loading analytics', this._addons, extension);
    }
  }

  trackGasV3UseOfDeprecatedMethod(methodUsed, extension) {
    this._trackAndForwardGasV3('operational', {
      action: 'deprecatedMethod',
      actionSubject: 'connectAddon',
      actionSubjectId: extension.addon_key,
      attributes: {
        moduleKey: extension.key,
        methodUsed: methodUsed
      },
      source: extension.addon_key
    })
  }

  trackGasV3MultipleDialogOpening(dialogType, extension) {
    this._trackAndForwardGasV3('operational', {
      action: 'multipleDialogOpened',
      actionSubject: 'connectAddon',
      actionSubjectId: extension.addon_key,
      attributes: {
        moduleKey: extension.key,
        dialogType: dialogType
      },
      source: extension.addon_key
    });
  }

  trackIframePerformance(metrics, extension) {
    if (!this._shouldSampleLoadEvent(extension)) {
      return;
    }
    this._trackGasV3('operational', {
      source: extension.addon_key,
      action: 'iframeRendered',
      actionSubject: 'connectAddon',
      actionSubjectId: extension.addon_key,
      attributes: {
        key: extension['key'],
        pearApp: this._getPearApp(extension),
        moduleType: this._getModuleType(extension),
        iframeIsCacheable: this._isCacheable(extension),
        moduleLocation: this._getModuleLocation(extension),
        domainLookupTime: metrics.domainLookupTime,
        connectionTime: metrics.connectionTime,
        decodedBodySize: metrics.decodedBodySize,
        domContentLoadedTime: metrics.domContentLoadedTime,
        fetchTime: metrics.fetchTime
      }
    });
  }

  trackGasV3WebVitals(metrics, extension) {
    if (!this._shouldSampleLoadEvent(extension)) {
      return;
    }
    this._trackAndForwardGasV3('operational', {
      source: extension.addon_key,
      action: 'webVitals',
      actionSubject: 'connectAddon',
      actionSubjectId: extension.addon_key,
      attributes: Object.assign({}, metrics, {
        key: extension['key'],
        pearApp: this._getPearApp(extension),
        moduleType: this._getModuleType(extension),
        iframeIsCacheable: this._isCacheable(extension),
        moduleLocation: this._getModuleLocation(extension),
      })
    })
  }

  dispatch(name, data) {
    this._trackGasV3External(name, data);
  }

  trackExternal(name, data) {
    this._trackGasV3External(name, data);
  }

  _trackGasV3External(name, data) {
    this._trackAndForwardGasV3('operational', {
      action: 'externalEvent',
      actionSubject: 'connectAddon',
      actionSubjectId: name,
      attributes: {
        eventName: name,
        ...(typeof data === 'object' ? data : { values: data })
      },
      source: 'connectAddon'
    });
  }

  /**
  * method called when an iframe's loading metrics gets corrupted
  * to destroy the analytics as they cannot be reliable
  * this should be called when:
  * 1. the product calls iframe creation multiple times for the same connect addon
  * 2. the iframe is moved / repainted causing a window.reload event
  * 3. user right clicks iframe and reloads it
  */
  _resetAnalyticsDueToUnreliable(extensionId) {
    if(!extensionId) {
      throw new Error('Cannot reset analytics due to no extension id');
    }
    if(this._addons[extensionId]) {
      clearTimeout(this._addons[extensionId]);
      delete this._addons[extensionId];
    } else {
      console.info('Cannot clear analytics, cache does not contain extension id');
    }
  }

  _isCacheable(extension) {
    const href = extension.url;
    return href !== undefined && href.indexOf('xdm_e=') === -1;
  }

  _getModuleType(extension) {
    return extension.options ? extension.options.moduleType : undefined;
  }

  _getModuleLocation(extension) {
    return extension.options ? extension.options.moduleLocation : undefined;
  }

  _getPearApp(extension) {
    return extension.options && extension.options.pearApp === 'true';
  }

  trackGasV3Visible (extension) {
    if (!this._shouldSampleLoadEvent(extension)) {
      return;
    }
    this._trackGasV3('operational', {
      action: 'iframeViewed',
      actionSubject: 'connectAddon',
      actionSubjectId: extension['addon_key'],
      attributes: {
        moduleType: this._getModuleType(extension),
        iframeIsCacheable: this._isCacheable(extension),
        moduleKey: extension.key,
        moduleLocation: this._getModuleLocation(extension),
        pearApp: this._getPearApp(extension)
      },
      source: extension.addon_key
    });
  }

  trackGasV3InvokeMethod(extension, module, method, methodArgs) {
    if (!this._shouldSampleEvent(INVOKE_METHOD_SAMPLE_RATE) && !ALLOWLISTED_METHODS.includes(method)) {
      return;
    }

    const args = Array.isArray(methodArgs) ? [...methodArgs] : [];
    const isContextFn = (arg) => typeof arg === 'function' && Boolean(arg._context);

    // Remove undefined values, or the callback context function only at the very end of the array
    while (args.length > 0 && (args[args.length - 1] === undefined || isContextFn(args[args.length - 1]))) {
      args.pop();
    }

    const argTypes = args.map(arg => {
      if (Array.isArray(arg)) {
        return 'array'
      } else if (arg === null) {
        return 'null'
      }
      return typeof arg;
    })

    this._trackAndForwardGasV3('operational', {
      action: 'invokeMethod',
      actionSubject: 'connectAddon',
      actionSubjectId: extension['addon_key'],
      attributes: {
        argCount: args.length,
        argTypes,
        module: module,
        method: method,
        moduleType: this._getModuleType(extension),
        moduleKey: extension.key,
        moduleLocation: this._getModuleLocation(extension),
        pearApp: this._getPearApp(extension)
      },
      source: extension.addon_key
    });
  }

  trackGasV3ContentResolver(isSuccess, extra) {
    if (!this._shouldSampleEvent(CONTENT_RESOLVER_SAMPLE_RATE)) {
      return;
    }
    const extension = extra.extension;
    const attrs = extra.attrs;
    const action = isSuccess ? 'contentResolverSucceeded' : 'contentResolverFailed';
    this._trackGasV3('operational', {
      action: action,
      actionSubject: 'connectAddon',
      actionSubjectId: extension['addon_key'],
      attributes: Object.assign({}, attrs, {
        moduleType: this._getModuleType(extension),
        moduleKey: extension.key,
        moduleLocation: this._getModuleLocation(extension),
        pearApp: this._getPearApp(extension)
      }),
      source: extension.addon_key
    });
  }

  trackGasV3LoadingEnded (extension) {
    if (!this._shouldSampleLoadEvent(extension)) {
      return;
    }
    var iframeLoadMillis = this._time() - this._addons[extension.id].startLoading;
    this._trackGasV3('operational', {
      action: 'iframeLoaded',
      actionSubject: 'connectAddon',
      actionSubjectId: extension['addon_key'],
      attributes: {
        moduleType: this._getModuleType(extension),
        iframeIsCacheable: this._isCacheable(extension),
        iframeLoadMillis: iframeLoadMillis,
        iframeLoadBucket: bucketLoadingTime(iframeLoadMillis),
        moduleKey: extension.key,
        moduleLocation: this._getModuleLocation(extension),
        pearApp: this._getPearApp(extension)
      },
      source: extension.addon_key
    });
  }

  trackGasV3LoadingTimeout (extension) {
    if (!this._shouldSampleLoadEvent(extension)) {
      return;
    }
    this._trackGasV3('operational', {
      action: 'iframeTimeout',
      actionSubject: 'connectAddon',
      actionSubjectId: extension['addon_key'],
      attributes: {
        moduleType: this._getModuleType(extension),
        iframeIsCacheable: this._isCacheable(extension),
        moduleKey: extension.key,
        moduleLocation: this._getModuleLocation(extension),
        pearApp: this._getPearApp(extension),
        connectedStatus: typeof window.navigator.onLine === 'boolean' ? window.navigator.onLine.toString() : 'not-supported'
      },
      source: extension.addon_key
    });
  }

  /**
   * @param {WebItemInvokedEventData} data
   */
  trackGasV3InlineDialogOpened(data) {
    const extension = data.extension;
    this._trackAndForwardGasV3('operational', {
      action: 'inlineDialogOpened',
      actionSubject: 'connectAddon',
      actionSubjectId: extension['addon_key'],
      attributes: {
        moduleKey: extension.key,
        moduleLocation: this._getModuleLocation(extension),
        eventType: data.eventType,
        onHoverEnabled: data.onHover
      },
      source: extension.addon_key
    });
  }

  /**
   * Track a dialog opened via the legacy event-handler (non-imperative) API
   */
  trackGasV3LegacyDialogOpened(data) {
    const extension = data.extension;
    this._trackAndForwardGasV3('operational', {
      action: 'legacyDialogOpened',
      actionSubject: 'connectAddon',
      actionSubjectId: extension['addon_key'],
      attributes: {
        moduleKey: extension.key,
        moduleLocation: this._getModuleLocation(extension),
        parentIds: data.parentIds
      },
      source: extension.addon_key
    });
  }

  /**
   * Tracks when the Statsig client is not initialised before the Statsig feature flag is fetched
   * Jira - https://data-portal.internal.atlassian.com/analytics/registry/67270
   * Confluence - https://data-portal.internal.atlassian.com/analytics/registry/67271
   */
  trackGasV3FeatureFlagDefaultFalse(featureFlag) {
    this._trackAndForwardGasV3('operational', {
      action: 'returned',
      actionSubject: 'defaultFalseFeatureFlag',
      actionSubjectId: featureFlag,
      attributes: {
        featureFlag
      },
      source: 'Host'
    });
  }

  /**
   * Tracks when a Connect iframe is clicked
   */
  trackGasV3IframeClicked(data) {
    this._trackAndForwardGasV3('ui', {
      action: 'iframeClicked',
      actionSubject: 'connectAddon',
      source: 'host',
      attributes: {
        appId: data.addonKey,
        moduleType: data.moduleType,
        moduleLocation: data.moduleLocation,
      }
    })
  }
}

var analytics = new AnalyticsDispatcher();
if ($.fn) {
  EventDispatcher.register('iframe-create', function (data) {
    analytics.trackLoadingStarted(data.extension);
  });
}

EventDispatcher.register('method-invoked', function (data) {
  analytics.trackGasV3InvokeMethod(data.extension, data.module, data.fn, data.args);
});

EventDispatcher.register('iframe-bridge-start', function (data) {
  analytics.trackLoadingStarted(data.extension);
});
EventDispatcher.register('iframe-bridge-established', function (data) {
  analytics.trackGasV3LoadingEnded(data.extension);
  observe(document.getElementById(data.extension.id), () => {
    EventDispatcher.dispatch('iframe-visible', data.extension);
    analytics.trackGasV3Visible(data.extension);
  });
});

EventDispatcher.register('iframe-bridge-timeout', function (data) {
  analytics.trackGasV3LoadingTimeout(data.extension);
});

EventDispatcher.register('analytics-deprecated-method-used', function (data) {
  analytics.trackGasV3UseOfDeprecatedMethod(data.methodUsed, data.extension);
});
EventDispatcher.register('analytics-iframe-performance', function (data) {
  analytics.trackIframePerformance(data.metrics, data.extension);
});
EventDispatcher.register('analytics-web-vitals', function (data) {
  analytics.trackGasV3WebVitals(data.metrics, data.extension);
});

EventDispatcher.register('iframe-destroyed', function (data) {
  analytics._resetAnalyticsDueToUnreliable(data.extension.extension_id);
});

EventDispatcher.register('analytics-external-event-track', function (data) {
  analytics.trackExternal(data.eventName, data.values);
});

EventDispatcher.register('analytics-content-resolver-track', function (data) {
  const success = data.success;
  const extra = data.extra;
  analytics.trackGasV3ContentResolver(success, extra);
});

EventDispatcher.register('analytics-inline-dialog-opened', function (data) {
  analytics.trackGasV3InlineDialogOpened(data);
});

EventDispatcher.register('analytics-legacy-dialog-opened', function (data) {
  analytics.trackGasV3LegacyDialogOpened(data);
});

EventDispatcher.register('analytics-track-iframe-clicked', function (data) {
  analytics.trackGasV3IframeClicked(data);
})



export default analytics;
