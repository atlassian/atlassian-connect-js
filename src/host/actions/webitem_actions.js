import EventDispatcher from '../dispatchers/event_dispatcher';
import WebItemComponent from '../components/webitem';

/**
 * @typedef {{$target: JQuery, eventType: 'click' | 'mouseenter', extension: Object}} WebItemInvokedEventData
 */
export default {
  addWebItem: (potentialWebItem) => {
    let webitem;
    let existing = WebItemComponent.getWebItemsBySelector(potentialWebItem.selector);

    if(existing) {
      return false;
    } else {
      webitem = WebItemComponent.setWebItem(potentialWebItem);
      EventDispatcher.dispatch('webitem-added', {webitem});
    }

  },

  /**
   * @param {boolean} isInlineDialog
   * @param {WebItemInvokedEventData} data
   */
  webitemInvoked: (isInlineDialog, data) => {
    const eventName = isInlineDialog ? 'inline-dialog' : 'dialog'
    EventDispatcher.dispatch('webitem-invoked:' + eventName, data);
  }
};
