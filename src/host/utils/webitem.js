import qs from 'query-string';
import Util from '../util';

function sanitizeTriggers(triggers) {
  var onTriggers;
  if(Array.isArray(triggers)) {
    onTriggers = triggers.join(' ');
  } else if (typeof triggers === 'string') {
    onTriggers = triggers.trim();
  }
  return onTriggers;
}

function uniqueId(){
  return 'webitem-' + Math.floor(Math.random() * 1000000000).toString(16);
}

/**
 * LEGACY: get addon key by webitem for p2
 * @param {string} cssClass
 */
function getExtensionKey(cssClass){
  var m = cssClass ? cssClass.match(/ap-plugin-key-([^\s]*)/) : null;
  return Array.isArray(m) ? m[1] : false;
}

/**
 * LEGACY: get module key by webitem for p2
 * @param {string} cssClass
*/
function getKey(cssClass){
  var m = cssClass ? cssClass.match(/ap-module-key-([^\s]*)/) : null;
  return Array.isArray(m) ? m[1] : false;
}

/**
 * @param cssClass
 */
function getTargetKey(cssClass){
  var m = cssClass ? cssClass.match(/ap-target-key-([^\s]*)/) : null;
  return Array.isArray(m) ? m[1] : false;
}

/**
 * @param cssClass
 * @returns {string}
 */
function getFullKey(cssClass){
  return getExtensionKey(cssClass) + '__' + getKey(cssClass);
}

function getModuleOptionsByAddonAndModuleKey(type, addonKey, moduleKey) {
  var moduleType = type + 'Modules';
  if(window._AP
    && window._AP[moduleType]
    && window._AP[moduleType][addonKey]
    && window._AP[moduleType][addonKey][moduleKey]) {
    return Util.extend({}, window._AP[moduleType][addonKey][moduleKey].options);
  }
}

/**
 * @param {string} type
 * @param {string} cssClass
 */
function getModuleOptionsForWebitem(type, cssClass){
  var addon_key = getExtensionKey(cssClass);
  var targetKey = getTargetKey(cssClass);
  return getModuleOptionsByAddonAndModuleKey(type, addon_key, targetKey);
}

/**
 * gets the connect config from the encoded webitem target (via the url)
 * @param {JQuery} $target
 * @param {string} cssClass
 * @param {string|undefined} href
 */
// eslint-disable-next-line complexity
function getConfigFromTarget($target, cssClass, href){
  let url = href || $target.attr('href');
  var convertedOptions = {};
  var iframeData;
  // adg3 has classes outside of a tag so look for href inside the a
  if (!url) {
    url = $target.find('a').attr('href');
  }
  if (url) {
    var hashIndex = url.indexOf('#');
    if (hashIndex >= 0) {
      var hash = url.substring(hashIndex + 1);
      try {
        iframeData = JSON.parse(decodeURI(hash));
      } catch (e) {
        console.error('ACJS: cannot decode webitem anchor');
      }
      if (iframeData && window._AP && window._AP._convertConnectOptions) {
        convertedOptions = window._AP._convertConnectOptions(iframeData);
      } else {
        console.error('ACJS: cannot convert webitem url to connect iframe options');
      }

    } else {
      // The URL has no hash component so fall back to the old behaviour of providing:
      // add-on key, module key, dialog module options and product context (from the webitem url).
      // This may be the case for web items that were persisted prior to the new storage format whereby a hash
      // fragment is added into the URL detailing the target module info. If this info is
      // not present, the content resolver will be used to resolve the module after the web
      // item is clicked.

      // Old URL format detected. Falling back to old functionality
      var fullKey = getFullKey(cssClass);
      var type = isInlineDialogTrigger(cssClass) ? 'inlineDialog' : 'dialog';
      var options = getModuleOptionsForWebitem(type, cssClass);
      if(!options && window._AP && window._AP[type + 'Options']) {
        options = Util.extend({}, window._AP[type + 'Options'][fullKey]) || {};
      }
      if(!options){
        options = {};
        console.warn('no webitem ' + type + 'Options for ' + fullKey);
      }
      options.productContext = options.productContext || {};
      var query = qs.parse(qs.extract(url));
      Util.extend(options.productContext, query);

      convertedOptions = {
        addon_key: getExtensionKey(cssClass),
        key: getKey(cssClass),
        options: options
      };
    }
  }
  return convertedOptions;
}

/**
 * @param {string|string[]|null} classes
 * @returns {boolean}
 */
export function isInlineDialogTrigger(classes) {
  return classes ? classes.includes('ap-inline-dialog') : false
}

/**
 * @param {string|string[]|null} classes
 * @returns {boolean}
 */
export function isDialogTrigger(classes) {
  return classes ? classes.includes('ap-dialog') : false
}

/**
 * LEGACY - method for handling webitem options for p2
 * @param {JQuery} $target
 * @param {string} cssClass
 * @param {string|undefined} href
 */
function getOptionsForWebItem($target, cssClass, href) {
  var fullKey = getFullKey(cssClass);

  var type = isInlineDialogTrigger(cssClass) ? 'inlineDialog' : 'dialog';
  var options = getModuleOptionsForWebitem(type, cssClass);
  if(!options && window._AP && window._AP[type + 'Options']) {
    options = Util.extend({}, window._AP[type + 'Options'][fullKey]) || {};
  }
  if(!options){
    options = {};
    console.warn('no webitem ' + type + 'Options for ' + fullKey);
  }
  options.productContext = options.productContext || {};
  options.structuredContext = options.structuredContext || {};
  // create product context from url params

  var convertedConfig = getConfigFromTarget($target, cssClass, href);

  if(convertedConfig && convertedConfig.options) {
    Util.extend(options.productContext, convertedConfig.options.productContext);
    Util.extend(options.structuredContext, convertedConfig.options.structuredContext);
    options.contextJwt = convertedConfig.options.contextJwt;
  }

  return options;
}

/**
 * @param {HTMLElement} target
 * @param {string[]} attributeNames
 * @returns {string[]}
 */
export function collectParentElementAttributes(target, attributeNames) {
  // Collect attributes from parent elements of the webitem
  const attributes = [];
  let current = target
  let count = 0;
  while (current && current !== document.body && count++ < 1000) {
    for (let attributeName of attributeNames) {
      const attribute = current.getAttribute(attributeName);
      if (attribute) {
        attributes.push(attribute);
      }
    }
    current = current.parentElement;
  }
  return attributes;
}

export default {
  sanitizeTriggers,
  uniqueId,
  getExtensionKey,
  getKey,
  getOptionsForWebItem,
  getModuleOptionsByAddonAndModuleKey,
  getConfigFromTarget,
};
