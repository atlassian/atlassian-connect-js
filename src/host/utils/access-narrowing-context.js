export function getAccessNarrowingSpaceContext(extension) {
  if (extension?.options && AJS?.Meta?.get) {
    const productContext = {
      ...(extension.options.productContext || {})
    };
    if (AJS.Meta.get('space-key')) {
      productContext['an.spaceKey'] = AJS.Meta.get('space-key');
    }
    return {
      ...extension,
      options: {
        ...extension.options,
        productContext,
      }
    };
  }
  return extension;
}
