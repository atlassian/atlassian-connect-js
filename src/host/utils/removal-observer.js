import HostApi from '../host-api';

export function observeIframeRemoval() {
  const scopedObserver = new MutationObserver((mutationsList) => {
    mutationsList.forEach((mutation) => {
      if (mutation.type === 'childList') {
        mutation.removedNodes.forEach((node) => {
          if (node.nodeType !== Node.ELEMENT_NODE) {
            return;
          }
          const onDisconnect = (node) => {
            HostApi.destroy(node.id);
          };
          // Check if the node itself is an iframe
          if (node.tagName === 'IFRAME' && node.id) {
            onDisconnect(node);
          } else {
            // Check if any iframes exist within the removed node's subtree
            node.querySelectorAll('iframe[id]').forEach((childNode) => {
              onDisconnect(childNode);
            });
          }
        });
      }
    });
  });

  scopedObserver.observe(document.body, { childList: true, subtree: true });
  return scopedObserver;
}
