import FeatureGates from '@atlaskit/feature-gate-js-client';
import AnalyticsDispatcher from '../dispatchers/analytics_dispatcher';

function getBooleanFeatureFlagStatsig(flagName) {
  if (FeatureGates.initializeCompleted()) {
    return FeatureGates.checkGate(flagName);
  }
  AnalyticsDispatcher.trackGasV3FeatureFlagDefaultFalse(flagName);
  console.error(`[ACJS] Feature flag service is not initialised. Default flag '${flagName}' to false`);
  return false;
}


/**
 * For all new feature flags, please create them in Statsig
 * https://hello.atlassian.net/wiki/spaces/ECO/pages/3707522428
 *
 * @param {string} flagName Feature flag name
 * @returns {boolean}
 */
export default function getBooleanFeatureFlag(flagName) {
  return getBooleanFeatureFlagStatsig(flagName);
}

export const Flags = {
  getBooleanFeatureFlag,
}
