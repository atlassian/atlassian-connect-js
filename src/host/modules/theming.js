import simpleXDM from 'simple-xdm/host';
import { getGlobalTheme, ThemeMutationObserver, CURRENT_SURFACE_CSS_VAR } from '@atlaskit/tokens';

import util from '../util';


const getPluginFeatureFlags = () => ({
  // Add feature flags sent to the plugin on theming bootstrap here
})

const getSurfaceValue = (extension_id) => {
  const iframe = document.getElementById(extension_id);
  if (!iframe) {
    return;
  }

  const styles = getComputedStyle(iframe);
  const currentSurface = styles.getPropertyValue(CURRENT_SURFACE_CSS_VAR);
  const raised = styles.getPropertyValue('--ds-surface-raised');
  const overlay = styles.getPropertyValue('--ds-surface-overlay');

  if (currentSurface === raised) {
    return 'raised';
  }

  if (currentSurface === overlay) {
    return 'overlay';
  }
};

/**
 * Broadcasts the new theme to all plugins.
 * @typedef {import("@atlaskit/tokens").ThemeState} ThemeState
 * @param newTheme {ThemeState}
 */
function broadcastThemeChange(newTheme) {
  const featureFlags = getPluginFeatureFlags();
  if (ThemingModuleState.extensionIds.length) {
    ThemingModuleState.extensionIds.forEach((id) => {
      const surface = getSurfaceValue(id);
      simpleXDM.broadcast(
        'theme_changed',
        { id },
        { newTheme, featureFlags, surface }
      );
    });
  }
}

// Exported only for testing
export const ThemingModuleState = {
  /**
   * An array containing extension ID's for each app that has enabled theming.
   * Theme change events are only sent to apps listed in this array.
   */
  extensionIds: [],
  /**
   * Observer that will only trigger on changes to data-theme on the root html element.
   * Dispatches the new theme to all plugins when the theme changes.
   */
  themeObserver: new ThemeMutationObserver((newTheme) => {
    broadcastThemeChange(newTheme);
  })
}

// Only functions that make up the public ACJS theming API should be exported on this object.
const ThemingModule = {
  /**
   * Starts observing theme changes and dispatches the `theme_initialized` event to the plugin.
   * This event causes the plugin to react when the host's theme changes.
   */
  initializeTheming() {
    const callback = util.last(arguments);
    const forceNoCleanup = arguments.length === 1 ? false : arguments[0];

    if (!callback || !callback._context || !callback._context.extension_id) {
      return;
    }

    const { _context: { extension_id } } = callback;

    if (!extension_id) {
      return;
    }

    const initialTheme = getGlobalTheme();
    const surface = getSurfaceValue(extension_id);
    const featureFlags = getPluginFeatureFlags();

    // The theme_initialized event should be broadcast even if it has previously been initialized by the plugin.
    // This is to ensure that theming works in multi-page apps after navigating to a different page.
    // The reason is that isThemingInitialized on the plugin side starts as `false` when all.js is loaded.
    // The theme_initialized event is needed to change it to `true` for any new page.
    simpleXDM.broadcast(
      'theme_initialized',
      { id: extension_id },
      { initialTheme, featureFlags, surface, forceNoCleanup }
    );

    if (!ThemingModuleState.extensionIds.includes(extension_id)) {
      ThemingModuleState.extensionIds.push(extension_id);
      ThemingModuleState.themeObserver.observe(document.documentElement);
    }
  },
  /**
   * This has to be part of the API so the plugin can notify us when theme loading is finished.
   * The underscore indicates it should not be called by apps directly.
   * @private
   */
  _finishedInitTheming() {
  }
};

export default ThemingModule;
