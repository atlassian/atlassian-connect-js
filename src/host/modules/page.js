/**
 * A JavaScript module which provides functions for general page modules.
 * @module Page
 */
import util from '../util';

export default {
  /**
   * Sets the title of the document when a full page iframe is loaded.
   * @memberOf module:Page
   * @method setTitle
   * @param {String} title the title of the document to be set.
   * @example
   * AP.page.setTitle(title)
   */
  setTitle: function (title, callback) {
    callback = util.last(arguments);

    if (
      callback._context.extension.options &&
      callback._context.extension.options.isFullPage
    ) {
      const productSuffix =
        ' - ' + util.guessProductFromModuleDefined(window.connectHost);

      document.title = title + productSuffix;
    }
  },
};
