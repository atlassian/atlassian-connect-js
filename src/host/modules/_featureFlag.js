import simpleXDM from 'simple-xdm/host';
import getBooleanFeatureFlag from '../utils/feature-flag';
import { fg } from '@atlaskit/platform-feature-flags';

const allowedPlatformFeatureFlags = [
  'platform-visual-refresh-icons'
]

function isPlatformAllowedFeatureFlag(flagName) {
  return allowedPlatformFeatureFlags.includes(flagName);
}

function isAllowListedFeatureFlag(flagName) {
  return flagName.indexOf('acjs-iframe-allowlist') === 0;
}

let defined = false;
export default function defineFeatureFlagModule() {
  if (defined) {
    return;
  }
  defined = true;

  const featureFlagModule = {
    getBooleanFeatureFlag(flagName) {
      return new Promise((resolve, reject) => {
        if(isPlatformAllowedFeatureFlag(flagName)) {
          resolve(fg(flagName))
        } else {
          if (!isAllowListedFeatureFlag(flagName)) {
            reject(new Error('Only allowlisted flags can be accessed from the iframe.'))
            return;
          }
          resolve(getBooleanFeatureFlag(flagName))
        }
      })
    }
  }
  simpleXDM.returnsPromise(featureFlagModule.getBooleanFeatureFlag);
  simpleXDM.defineModule('_featureFlag', featureFlagModule)
}
