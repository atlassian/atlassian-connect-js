import EventDispatcher from './dispatchers/event_dispatcher';
import './components/loading_indicator';
import events from './modules/events';
import dialog from './modules/dialog';
import env from './modules/env';
import inlineDialog from './modules/inline-dialog';
import messages from './modules/messages';
import flag from './modules/flag';
import analytics from './modules/analytics';
import scrollPosition from './modules/scroll-position';
import dropdown from './modules/dropdown';
import host from './modules/host';
import theming from './modules/theming';
import page from './modules/page';
import HostApi from './host-api';
import './components/inline_dialog_webitem';
import './components/dialog_webitem';
import './components/dialog_extension';
import simpleXDM from 'simple-xdm/host';
import getBooleanFeatureFlag from './utils/feature-flag';
import { observeIframeRemoval } from './utils/removal-observer';
import DomEventActions from './actions/dom_event_actions';
import AnalyticsAction from './actions/analytics_action';

/**
 * Private namespace for host-side code.
 * @type {*|{}}
 * @private
 * @deprecated use AMD instead of global namespaces. The only thing that should be on _AP is _AP.define and _AP.require.
 */
if (!window._AP) {
  window._AP = {};
}

/*
 * Add version
 */
if (!window._AP.version) {
  window._AP.version = '%%GULP_INJECT_VERSION%%';
}

simpleXDM.defineModule('messages', messages);
simpleXDM.defineModule('flag', flag);
simpleXDM.defineModule('dialog', dialog);
simpleXDM.defineModule('inlineDialog', inlineDialog);
simpleXDM.defineModule('env', env);
simpleXDM.defineModule('events', events);
simpleXDM.defineModule('_analytics', analytics);
simpleXDM.defineModule('scrollPosition', scrollPosition);
simpleXDM.defineModule('dropdown', dropdown);
simpleXDM.defineModule('host', host);
simpleXDM.defineModule('theming', theming);
simpleXDM.defineModule('page', page);

EventDispatcher.register('module-define-custom', function (data) {
  simpleXDM.defineModule(data.name, data.methods);
});

simpleXDM.registerRequestNotifier(function (data) {
  var dispatchEvent = () => {
    if (data.type === 'req') {
      const registeredExtension = simpleXDM.getExtensions(
        (ext) => ext.extension_id === data.extension_id
      )[0];
      if (registeredExtension) {
        const extension = registeredExtension.extension;
        EventDispatcher.dispatch('method-invoked', {
          ...data,
          extension,
        });
      }
    }
  };

  if (typeof window.requestIdleCallback === 'function') {
    window.requestIdleCallback(dispatchEvent, { timeout: 1000 });
  } else {
    dispatchEvent();
  }
});

function onDocumentReady() {
  observeIframeRemoval();
}

if (document.readyState === 'loading') {
  document.addEventListener('DOMContentLoaded', onDocumentReady)
} else {
  onDocumentReady();
}

if (getBooleanFeatureFlag('acjs_iframe_click_analytics')) {
  DomEventActions.registerClickHandler(function (iframe, data) {
    var iframeInfo = JSON.parse(iframe.name || '{}')
    var iframeInfoOptions = iframeInfo.options || {}
    var analyticsData = {
      moduleLocation: iframeInfoOptions.moduleLocation,
      moduleType: iframeInfoOptions.moduleType,
      addonKey: data.addon_key,
    };
    AnalyticsAction.trackIframeClick(analyticsData);
  });
}

simpleXDM.setFeatureFlagGetter(getBooleanFeatureFlag);

export default HostApi;
