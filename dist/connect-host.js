(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory(require('@statsig/js-client'), require('@statsig/client-core')) :
  typeof define === 'function' && define.amd ? define(['@statsig/js-client', '@statsig/client-core'], factory) :
  (global = typeof globalThis !== 'undefined' ? globalThis : global || self, global.connectHost = factory(global.jsClient, global.clientCore));
})(this, (function (jsClient, clientCore) { 'use strict';

  function _extends() {
    _extends = Object.assign ? Object.assign.bind() : function (target) {
      for (var i = 1; i < arguments.length; i++) {
        var source = arguments[i];
        for (var key in source) {
          if (Object.prototype.hasOwnProperty.call(source, key)) {
            target[key] = source[key];
          }
        }
      }
      return target;
    };
    return _extends.apply(this, arguments);
  }

  function _setPrototypeOf(o, p) {
    _setPrototypeOf = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf(o, p) {
      o.__proto__ = p;
      return o;
    };
    return _setPrototypeOf(o, p);
  }

  function _inheritsLoose(subClass, superClass) {
    subClass.prototype = Object.create(superClass.prototype);
    subClass.prototype.constructor = subClass;
    _setPrototypeOf(subClass, superClass);
  }

  var domain;

  // This constructor is used to store event handlers. Instantiating this is
  // faster than explicitly calling `Object.create(null)` to get a "clean" empty
  // object (tested with v8 v4.9).
  function EventHandlers() {}
  EventHandlers.prototype = Object.create(null);
  function EventEmitter() {
    EventEmitter.init.call(this);
  }

  // nodejs oddity
  // require('events') === require('events').EventEmitter
  EventEmitter.EventEmitter = EventEmitter;
  EventEmitter.usingDomains = false;
  EventEmitter.prototype.domain = undefined;
  EventEmitter.prototype._events = undefined;
  EventEmitter.prototype._maxListeners = undefined;

  // By default EventEmitters will print a warning if more than 10 listeners are
  // added to it. This is a useful default which helps finding memory leaks.
  EventEmitter.defaultMaxListeners = 10;
  EventEmitter.init = function () {
    this.domain = null;
    if (EventEmitter.usingDomains) {
      // if there is an active domain, then attach to it.
      if (domain.active ) ;
    }
    if (!this._events || this._events === Object.getPrototypeOf(this)._events) {
      this._events = new EventHandlers();
      this._eventsCount = 0;
    }
    this._maxListeners = this._maxListeners || undefined;
  };

  // Obviously not all Emitters should be limited to 10. This function allows
  // that to be increased. Set to zero for unlimited.
  EventEmitter.prototype.setMaxListeners = function setMaxListeners(n) {
    if (typeof n !== 'number' || n < 0 || isNaN(n)) throw new TypeError('"n" argument must be a positive number');
    this._maxListeners = n;
    return this;
  };
  function $getMaxListeners(that) {
    if (that._maxListeners === undefined) return EventEmitter.defaultMaxListeners;
    return that._maxListeners;
  }
  EventEmitter.prototype.getMaxListeners = function getMaxListeners() {
    return $getMaxListeners(this);
  };

  // These standalone emit* functions are used to optimize calling of event
  // handlers for fast cases because emit() itself often has a variable number of
  // arguments and can be deoptimized because of that. These functions always have
  // the same number of arguments and thus do not get deoptimized, so the code
  // inside them can execute faster.
  function emitNone(handler, isFn, self) {
    if (isFn) handler.call(self);else {
      var len = handler.length;
      var listeners = arrayClone(handler, len);
      for (var i = 0; i < len; ++i) listeners[i].call(self);
    }
  }
  function emitOne(handler, isFn, self, arg1) {
    if (isFn) handler.call(self, arg1);else {
      var len = handler.length;
      var listeners = arrayClone(handler, len);
      for (var i = 0; i < len; ++i) listeners[i].call(self, arg1);
    }
  }
  function emitTwo(handler, isFn, self, arg1, arg2) {
    if (isFn) handler.call(self, arg1, arg2);else {
      var len = handler.length;
      var listeners = arrayClone(handler, len);
      for (var i = 0; i < len; ++i) listeners[i].call(self, arg1, arg2);
    }
  }
  function emitThree(handler, isFn, self, arg1, arg2, arg3) {
    if (isFn) handler.call(self, arg1, arg2, arg3);else {
      var len = handler.length;
      var listeners = arrayClone(handler, len);
      for (var i = 0; i < len; ++i) listeners[i].call(self, arg1, arg2, arg3);
    }
  }
  function emitMany(handler, isFn, self, args) {
    if (isFn) handler.apply(self, args);else {
      var len = handler.length;
      var listeners = arrayClone(handler, len);
      for (var i = 0; i < len; ++i) listeners[i].apply(self, args);
    }
  }
  EventEmitter.prototype.emit = function emit(type) {
    var er, handler, len, args, i, events, domain;
    var doError = type === 'error';
    events = this._events;
    if (events) doError = doError && events.error == null;else if (!doError) return false;
    domain = this.domain;

    // If there is no 'error' event listener then throw.
    if (doError) {
      er = arguments[1];
      if (domain) {
        if (!er) er = new Error('Uncaught, unspecified "error" event');
        er.domainEmitter = this;
        er.domain = domain;
        er.domainThrown = false;
        domain.emit('error', er);
      } else if (er instanceof Error) {
        throw er; // Unhandled 'error' event
      } else {
        // At least give some kind of context to the user
        var err = new Error('Uncaught, unspecified "error" event. (' + er + ')');
        err.context = er;
        throw err;
      }
      return false;
    }
    handler = events[type];
    if (!handler) return false;
    var isFn = typeof handler === 'function';
    len = arguments.length;
    switch (len) {
      // fast cases
      case 1:
        emitNone(handler, isFn, this);
        break;
      case 2:
        emitOne(handler, isFn, this, arguments[1]);
        break;
      case 3:
        emitTwo(handler, isFn, this, arguments[1], arguments[2]);
        break;
      case 4:
        emitThree(handler, isFn, this, arguments[1], arguments[2], arguments[3]);
        break;
      // slower
      default:
        args = new Array(len - 1);
        for (i = 1; i < len; i++) args[i - 1] = arguments[i];
        emitMany(handler, isFn, this, args);
    }
    return true;
  };
  function _addListener(target, type, listener, prepend) {
    var m;
    var events;
    var existing;
    if (typeof listener !== 'function') throw new TypeError('"listener" argument must be a function');
    events = target._events;
    if (!events) {
      events = target._events = new EventHandlers();
      target._eventsCount = 0;
    } else {
      // To avoid recursion in the case that type === "newListener"! Before
      // adding it to the listeners, first emit "newListener".
      if (events.newListener) {
        target.emit('newListener', type, listener.listener ? listener.listener : listener);

        // Re-assign `events` because a newListener handler could have caused the
        // this._events to be assigned to a new object
        events = target._events;
      }
      existing = events[type];
    }
    if (!existing) {
      // Optimize the case of one listener. Don't need the extra array object.
      existing = events[type] = listener;
      ++target._eventsCount;
    } else {
      if (typeof existing === 'function') {
        // Adding the second element, need to change to array.
        existing = events[type] = prepend ? [listener, existing] : [existing, listener];
      } else {
        // If we've already got an array, just append.
        if (prepend) {
          existing.unshift(listener);
        } else {
          existing.push(listener);
        }
      }

      // Check for listener leak
      if (!existing.warned) {
        m = $getMaxListeners(target);
        if (m && m > 0 && existing.length > m) {
          existing.warned = true;
          var w = new Error('Possible EventEmitter memory leak detected. ' + existing.length + ' ' + type + ' listeners added. ' + 'Use emitter.setMaxListeners() to increase limit');
          w.name = 'MaxListenersExceededWarning';
          w.emitter = target;
          w.type = type;
          w.count = existing.length;
          emitWarning(w);
        }
      }
    }
    return target;
  }
  function emitWarning(e) {
    typeof console.warn === 'function' ? console.warn(e) : console.log(e);
  }
  EventEmitter.prototype.addListener = function addListener(type, listener) {
    return _addListener(this, type, listener, false);
  };
  EventEmitter.prototype.on = EventEmitter.prototype.addListener;
  EventEmitter.prototype.prependListener = function prependListener(type, listener) {
    return _addListener(this, type, listener, true);
  };
  function _onceWrap(target, type, listener) {
    var fired = false;
    function g() {
      target.removeListener(type, g);
      if (!fired) {
        fired = true;
        listener.apply(target, arguments);
      }
    }
    g.listener = listener;
    return g;
  }
  EventEmitter.prototype.once = function once(type, listener) {
    if (typeof listener !== 'function') throw new TypeError('"listener" argument must be a function');
    this.on(type, _onceWrap(this, type, listener));
    return this;
  };
  EventEmitter.prototype.prependOnceListener = function prependOnceListener(type, listener) {
    if (typeof listener !== 'function') throw new TypeError('"listener" argument must be a function');
    this.prependListener(type, _onceWrap(this, type, listener));
    return this;
  };

  // emits a 'removeListener' event iff the listener was removed
  EventEmitter.prototype.removeListener = function removeListener(type, listener) {
    var list, events, position, i, originalListener;
    if (typeof listener !== 'function') throw new TypeError('"listener" argument must be a function');
    events = this._events;
    if (!events) return this;
    list = events[type];
    if (!list) return this;
    if (list === listener || list.listener && list.listener === listener) {
      if (--this._eventsCount === 0) this._events = new EventHandlers();else {
        delete events[type];
        if (events.removeListener) this.emit('removeListener', type, list.listener || listener);
      }
    } else if (typeof list !== 'function') {
      position = -1;
      for (i = list.length; i-- > 0;) {
        if (list[i] === listener || list[i].listener && list[i].listener === listener) {
          originalListener = list[i].listener;
          position = i;
          break;
        }
      }
      if (position < 0) return this;
      if (list.length === 1) {
        list[0] = undefined;
        if (--this._eventsCount === 0) {
          this._events = new EventHandlers();
          return this;
        } else {
          delete events[type];
        }
      } else {
        spliceOne(list, position);
      }
      if (events.removeListener) this.emit('removeListener', type, originalListener || listener);
    }
    return this;
  };
  EventEmitter.prototype.removeAllListeners = function removeAllListeners(type) {
    var listeners, events;
    events = this._events;
    if (!events) return this;

    // not listening for removeListener, no need to emit
    if (!events.removeListener) {
      if (arguments.length === 0) {
        this._events = new EventHandlers();
        this._eventsCount = 0;
      } else if (events[type]) {
        if (--this._eventsCount === 0) this._events = new EventHandlers();else delete events[type];
      }
      return this;
    }

    // emit removeListener for all listeners on all events
    if (arguments.length === 0) {
      var keys = Object.keys(events);
      for (var i = 0, key; i < keys.length; ++i) {
        key = keys[i];
        if (key === 'removeListener') continue;
        this.removeAllListeners(key);
      }
      this.removeAllListeners('removeListener');
      this._events = new EventHandlers();
      this._eventsCount = 0;
      return this;
    }
    listeners = events[type];
    if (typeof listeners === 'function') {
      this.removeListener(type, listeners);
    } else if (listeners) {
      // LIFO order
      do {
        this.removeListener(type, listeners[listeners.length - 1]);
      } while (listeners[0]);
    }
    return this;
  };
  EventEmitter.prototype.listeners = function listeners(type) {
    var evlistener;
    var ret;
    var events = this._events;
    if (!events) ret = [];else {
      evlistener = events[type];
      if (!evlistener) ret = [];else if (typeof evlistener === 'function') ret = [evlistener.listener || evlistener];else ret = unwrapListeners(evlistener);
    }
    return ret;
  };
  EventEmitter.listenerCount = function (emitter, type) {
    if (typeof emitter.listenerCount === 'function') {
      return emitter.listenerCount(type);
    } else {
      return listenerCount.call(emitter, type);
    }
  };
  EventEmitter.prototype.listenerCount = listenerCount;
  function listenerCount(type) {
    var events = this._events;
    if (events) {
      var evlistener = events[type];
      if (typeof evlistener === 'function') {
        return 1;
      } else if (evlistener) {
        return evlistener.length;
      }
    }
    return 0;
  }
  EventEmitter.prototype.eventNames = function eventNames() {
    return this._eventsCount > 0 ? Reflect.ownKeys(this._events) : [];
  };

  // About 1.5x faster than the two-arg version of Array#splice().
  function spliceOne(list, index) {
    for (var i = index, k = i + 1, n = list.length; k < n; i += 1, k += 1) list[i] = list[k];
    list.pop();
  }
  function arrayClone(arr, i) {
    var copy = new Array(i);
    while (i--) copy[i] = arr[i];
    return copy;
  }
  function unwrapListeners(arr) {
    var ret = new Array(arr.length);
    for (var i = 0; i < ret.length; ++i) {
      ret[i] = arr[i].listener || arr[i];
    }
    return ret;
  }

  var EventDispatcher = /*#__PURE__*/function (_EventEmitter) {
    _inheritsLoose(EventDispatcher, _EventEmitter);
    function EventDispatcher() {
      var _this;
      _this = _EventEmitter.call(this) || this;
      _this.setMaxListeners(20);
      return _this;
    }
    var _proto = EventDispatcher.prototype;
    _proto.dispatch = function dispatch(action) {
      for (var _len = arguments.length, args = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        args[_key - 1] = arguments[_key];
      }
      this.emit.apply(this, ['before:' + action].concat(args));
      this.emit.apply(this, arguments);
      this.emit.apply(this, ['after:' + action].concat(args));
    };
    _proto.registerOnce = function registerOnce(action, callback) {
      if (typeof action === 'string') {
        this.once(action, callback);
      } else {
        throw 'ACJS: event name must be string';
      }
    };
    _proto.register = function register(action, callback) {
      if (typeof action === 'string') {
        this.on(action, callback);
      } else {
        throw 'ACJS: event name must be string';
      }
    };
    _proto.unregister = function unregister(action, callback) {
      if (typeof action === 'string') {
        this.removeListener(action, callback);
      } else {
        throw 'ACJS: event name must be string';
      }
    };
    return EventDispatcher;
  }(EventEmitter);
  var EventDispatcher$1 = new EventDispatcher();

  var LoadingIndicatorActions = {
    timeout: function timeout($el, extension) {
      EventDispatcher$1.dispatch('iframe-bridge-timeout', {
        $el: $el,
        extension: extension
      });
    },
    cancelled: function cancelled($el, extension) {
      EventDispatcher$1.dispatch('iframe-bridge-cancelled', {
        $el: $el,
        extension: extension
      });
    }
  };

  /**
   * The iframe-side code exposes a jquery-like implementation via _dollar.
   * This runs on the product side to provide AJS.$ under a _dollar module to provide a consistent interface
   * to code that runs on host and iframe.
   */
  var $ = window.AJS && window.AJS.$ || function () {
    console.error('[ACJS] jQuery was not loaded before Connect. If in product frontend, make sure to load via a wrapper, eg withConnectHost()');
  };

  var LOADING_INDICATOR_CLASS = 'ap-status-indicator';
  var LOADING_STATUSES = {
    loading: '<div class="ap-loading"><div class="small-spinner"></div>Loading app...</div>',
    'load-timeout': '<div class="ap-load-timeout"><div class="small-spinner"></div>App is not responding. Wait or <a href="#" class="ap-btn-cancel">cancel</a>?</div>',
    'load-error': 'App failed to load.'
  };
  var LOADING_TIMEOUT = 12000;
  var LoadingIndicator = /*#__PURE__*/function () {
    function LoadingIndicator() {
      this._stateRegistry = {};
    }
    var _proto = LoadingIndicator.prototype;
    _proto._loadingContainer = function _loadingContainer($iframeContainer) {
      return $iframeContainer.find('.' + LOADING_INDICATOR_CLASS);
    };
    _proto.render = function render() {
      var container = document.createElement('div');
      container.classList.add(LOADING_INDICATOR_CLASS);
      container.innerHTML = LOADING_STATUSES.loading;
      var $container = $(container);
      this._startSpinner($container);
      return $container;
    };
    _proto._startSpinner = function _startSpinner($container) {
      // TODO: AUI or spin.js broke something. This is bad but ironically matches v3's implementation.
      setTimeout(function () {
        var spinner = $container.find('.small-spinner');
        if (spinner.length && spinner.spin) {
          spinner.spin({
            lines: 12,
            length: 3,
            width: 2,
            radius: 3,
            trail: 60,
            speed: 1.5,
            zIndex: 1
          });
        }
      }, 10);
    };
    _proto.hide = function hide($iframeContainer, extensionId) {
      this._clearTimeout(extensionId);
      this._loadingContainer($iframeContainer)[0].style.display = 'none';
    };
    _proto.cancelled = function cancelled($iframeContainer, extensionId) {
      var status = LOADING_STATUSES['load-error'];
      this._loadingContainer($iframeContainer).empty().text(status);
    };
    _proto._setupTimeout = function _setupTimeout($container, extension) {
      this._stateRegistry[extension.id] = setTimeout(function () {
        LoadingIndicatorActions.timeout($container, extension);
      }, LOADING_TIMEOUT);
    };
    _proto._clearTimeout = function _clearTimeout(extensionId) {
      if (this._stateRegistry[extensionId]) {
        clearTimeout(this._stateRegistry[extensionId]);
        delete this._stateRegistry[extensionId];
      }
    };
    _proto.timeout = function timeout($iframeContainer, extensionId) {
      var status = $(LOADING_STATUSES['load-timeout']);
      var container = this._loadingContainer($iframeContainer);
      container.empty().append(status);
      this._startSpinner(container);
      $('a.ap-btn-cancel', container).click(function () {
        LoadingIndicatorActions.cancelled($iframeContainer, extensionId);
      });
      this._clearTimeout(extensionId);
      return container;
    };
    return LoadingIndicator;
  }();
  var LoadingComponent = new LoadingIndicator();
  EventDispatcher$1.register('iframe-create', function (data) {
    if (!data.extension.options.noDom) {
      LoadingComponent._setupTimeout(data.$el.parents('.ap-iframe-container'), data.extension);
    }
  });
  EventDispatcher$1.register('iframe-bridge-established', function (data) {
    if (!data.extension.options.noDom) {
      LoadingComponent.hide(data.$el.parents('.ap-iframe-container'), data.extension.id);
    }
  });
  EventDispatcher$1.register('iframe-bridge-timeout', function (data) {
    if (!data.extension.options.noDom) {
      LoadingComponent.timeout(data.$el, data.extension.id);
    }
  });
  EventDispatcher$1.register('iframe-bridge-cancelled', function (data) {
    if (!data.extension.options.noDom) {
      LoadingComponent.cancelled(data.$el, data.extension.id);
    }
  });
  EventDispatcher$1.register('iframe-destroyed', function (data) {
    LoadingComponent._clearTimeout(data.extension.extension_id);
  });

  function _objectWithoutPropertiesLoose(source, excluded) {
    if (source == null) return {};
    var target = {};
    var sourceKeys = Object.keys(source);
    var key, i;
    for (i = 0; i < sourceKeys.length; i++) {
      key = sourceKeys[i];
      if (excluded.indexOf(key) >= 0) continue;
      target[key] = source[key];
    }
    return target;
  }

  function _isNativeReflectConstruct$2() {
    if (typeof Reflect === "undefined" || !Reflect.construct) return false;
    if (Reflect.construct.sham) return false;
    if (typeof Proxy === "function") return true;
    try {
      Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {}));
      return true;
    } catch (e) {
      return false;
    }
  }

  function _construct(Parent, args, Class) {
    if (_isNativeReflectConstruct$2()) {
      _construct = Reflect.construct.bind();
    } else {
      _construct = function _construct(Parent, args, Class) {
        var a = [null];
        a.push.apply(a, args);
        var Constructor = Function.bind.apply(Parent, a);
        var instance = new Constructor();
        if (Class) _setPrototypeOf(instance, Class.prototype);
        return instance;
      };
    }
    return _construct.apply(null, arguments);
  }

  var LOG_PREFIX = "[Simple-XDM] ";
  var nativeBind = Function.prototype.bind;
  var util = {
    locationOrigin: function locationOrigin() {
      if (!window.location.origin) {
        return window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
      } else {
        return window.location.origin;
      }
    },
    randomString: function randomString() {
      return Math.floor(Math.random() * 1000000000).toString(16);
    },
    isString: function isString(str) {
      return typeof str === "string" || str instanceof String;
    },
    argumentsToArray: function argumentsToArray(arrayLike) {
      return Array.prototype.slice.call(arrayLike);
    },
    argumentNames: function argumentNames(fn) {
      return fn.toString().replace(/((\/\/.*$)|(\/\*[^]*?\*\/))/mg, '') // strip comments
      .replace(/[^(]+\(([^)]*)[^]+/, '$1') // get signature
      .match(/([^\s,]+)/g) || [];
    },
    hasCallback: function hasCallback(args) {
      var length = args.length;
      return length > 0 && typeof args[length - 1] === 'function';
    },
    error: function error(msg) {
      if (window.console && window.console.error) {
        var outputError = [];
        if (typeof msg === "string") {
          outputError.push(LOG_PREFIX + msg);
          outputError = outputError.concat(Array.prototype.slice.call(arguments, 1));
        } else {
          outputError.push(LOG_PREFIX);
          outputError = outputError.concat(Array.prototype.slice.call(arguments));
        }
        window.console.error.apply(null, outputError);
      }
    },
    warn: function warn(msg) {
      if (window.console) {
        console.warn(LOG_PREFIX + msg);
      }
    },
    log: function log(msg) {
      if (window.console) {
        window.console.log(LOG_PREFIX + msg);
      }
    },
    _bind: function _bind(thisp, fn) {
      if (nativeBind && fn.bind === nativeBind) {
        return fn.bind(thisp);
      }
      return function () {
        return fn.apply(thisp, arguments);
      };
    },
    throttle: function throttle(func, wait, context) {
      var previous = 0;
      return function () {
        var now = Date.now();
        if (now - previous > wait) {
          previous = now;
          func.apply(context, arguments);
        }
      };
    },
    each: function each(list, iteratee) {
      var length;
      var key;
      if (list) {
        length = list.length;
        if (length != null && typeof list !== 'function') {
          key = 0;
          while (key < length) {
            if (iteratee.call(list[key], key, list[key]) === false) {
              break;
            }
            key += 1;
          }
        } else {
          for (key in list) {
            if (list.hasOwnProperty(key)) {
              if (iteratee.call(list[key], key, list[key]) === false) {
                break;
              }
            }
          }
        }
      }
    },
    extend: function extend(dest) {
      var args = arguments;
      var srcs = [].slice.call(args, 1, args.length);
      srcs.forEach(function (source) {
        if (typeof source === "object") {
          Object.getOwnPropertyNames(source).forEach(function (name) {
            dest[name] = source[name];
          });
        }
      });
      return dest;
    },
    sanitizeStructuredClone: function sanitizeStructuredClone(object) {
      var whiteList = [Boolean, String, Date, RegExp, Blob, File, FileList, ArrayBuffer];
      var blackList = [Error, Node];
      var warn = util.warn;
      var visitedObjects = [];
      function _clone(value) {
        if (typeof value === 'function') {
          warn("A function was detected and removed from the message.");
          return null;
        }
        if (blackList.some(function (t) {
          if (value instanceof t) {
            warn(t.name + " object was detected and removed from the message.");
            return true;
          }
          return false;
        })) {
          return {};
        }
        if (value && typeof value === 'object' && whiteList.every(function (t) {
          return !(value instanceof t);
        })) {
          var newValue;
          if (Array.isArray(value)) {
            newValue = value.map(function (element) {
              return _clone(element);
            });
          } else {
            if (visitedObjects.indexOf(value) > -1) {
              warn("A circular reference was detected and removed from the message.");
              return null;
            }
            visitedObjects.push(value);
            newValue = {};
            for (var name in value) {
              if (value.hasOwnProperty(name)) {
                var clonedValue = _clone(value[name]);
                if (clonedValue !== null) {
                  newValue[name] = clonedValue;
                }
              }
            }
            visitedObjects.pop();
          }
          return newValue;
        }
        return value;
      }
      return _clone(object);
    },
    getOrigin: function getOrigin(url, base) {
      // everything except IE11
      if (typeof URL === 'function') {
        try {
          return new URL(url, base).origin;
        } catch (e) {}
      }
      // ie11 + safari 10
      var doc = document.implementation.createHTMLDocument('');
      if (base) {
        var baseElement = doc.createElement('base');
        baseElement.href = base;
        doc.head.appendChild(baseElement);
      }
      var anchorElement = doc.createElement('a');
      anchorElement.href = url;
      doc.body.appendChild(anchorElement);
      var origin = anchorElement.protocol + '//' + anchorElement.hostname;
      //ie11, only include port if referenced in initial URL
      if (url.match(/\/\/[^/]+:[0-9]+\//)) {
        origin += anchorElement.port ? ':' + anchorElement.port : '';
      }
      return origin;
    }
  };

  var PostMessage = /*#__PURE__*/function () {
    function PostMessage(data) {
      var d = data || {};
      this._registerListener(d.listenOn);
    }
    var _proto = PostMessage.prototype;
    _proto._registerListener = function _registerListener(listenOn) {
      if (!listenOn || !listenOn.addEventListener) {
        listenOn = window;
      }
      listenOn.addEventListener("message", util._bind(this, this._receiveMessage), false);
    };
    _proto._receiveMessage = function _receiveMessage(event) {
      var handler = this._messageHandlers[event.data.type],
        extensionId = event.data.eid,
        reg;
      if (extensionId && this._registeredExtensions) {
        reg = this._registeredExtensions[extensionId];
      }
      if (!handler || !this._checkOrigin(event, reg)) {
        return false;
      }
      handler.call(this, event, reg);
    };
    return PostMessage;
  }();

  var VALID_EVENT_TIME_MS = 30000; //30 seconds
  var XDMRPC = /*#__PURE__*/function (_PostMessage) {
    _inheritsLoose(XDMRPC, _PostMessage);
    var _proto = XDMRPC.prototype;
    _proto._padUndefinedArguments = function _padUndefinedArguments(array, length) {
      return array.length >= length ? array : array.concat(new Array(length - array.length));
    };
    function XDMRPC(config) {
      var _this;
      config = config || {};
      _this = _PostMessage.call(this, config) || this;
      _this._registeredExtensions = config.extensions || {};
      _this._registeredAPIModules = {};
      _this._registeredAPIModules._globals = {};
      _this._pendingCallbacks = {};
      _this._keycodeCallbacks = {};
      _this._clickHandlers = [];
      _this._pendingEvents = {};
      _this._messageHandlers = {
        init: _this._handleInit,
        req: _this._handleRequest,
        resp: _this._handleResponse,
        broadcast: _this._handleBroadcast,
        event_query: _this._handleEventQuery,
        key_triggered: _this._handleKeyTriggered,
        addon_clicked: _this._handleAddonClick,
        get_host_offset: _this._getHostOffset,
        unload: _this._handleUnload
      };
      return _this;
    }
    _proto._verifyAPI = function _verifyAPI(event, reg) {
      var untrustedTargets = event.data.targets;
      if (!untrustedTargets) {
        return;
      }
      var trustedSpec = this.getApiSpec();
      var tampered = false;
      function check(trusted, untrusted) {
        Object.getOwnPropertyNames(untrusted).forEach(function (name) {
          if (typeof untrusted[name] === 'object' && trusted[name]) {
            check(trusted[name], untrusted[name]);
          } else {
            if (untrusted[name] === 'parent' && trusted[name]) {
              tampered = true;
            }
          }
        });
      }
      check(trustedSpec, untrustedTargets);
      if (event.source && event.source.postMessage) {
        // only post a message if the source of the event still exists
        event.source.postMessage({
          type: 'api_tamper',
          tampered: tampered
        }, reg.extension.url);
      } else {
        console.warn("_verifyAPI postMessage skipped as event source missing.");
      }
    };
    _proto._handleInit = function _handleInit(event, reg) {
      if (event.source && event.source.postMessage) {
        // only post a message if the source of the event still exists
        event.source.postMessage({
          type: 'init_received'
        }, reg.extension.url);
      } else {
        console.warn("_handleInit postMessage skipped as event source missing.");
      }
      this._registeredExtensions[reg.extension_id].source = event.source;
      if (reg.initCallback) {
        reg.initCallback(event.data.eid);
        delete reg.initCallback;
      }
      if (event.data.targets) {
        this._verifyAPI(event, reg);
      }
    };
    _proto._getHostOffset = function _getHostOffset(event, _window) {
      var hostWindow = event.source;
      var hostFrameOffset = null;
      var windowReference = _window || window; // For testing

      if (windowReference === windowReference.top && typeof windowReference.getHostOffsetFunctionOverride === 'function') {
        hostFrameOffset = windowReference.getHostOffsetFunctionOverride(hostWindow);
      }
      if (typeof hostFrameOffset !== 'number') {
        hostFrameOffset = 0;
        // Find the closest frame that has the same origin as event source
        while (!this._hasSameOrigin(hostWindow)) {
          // Climb up the iframe tree 1 layer
          hostFrameOffset++;
          hostWindow = hostWindow.parent;
        }
      }
      if (event.source && event.source.postMessage) {
        // only post a message if the source of the event still exists
        event.source.postMessage({
          hostFrameOffset: hostFrameOffset
        }, event.origin);
      } else {
        console.warn("_getHostOffset postMessage skipped as event source missing.");
      }
    };
    _proto._hasSameOrigin = function _hasSameOrigin(window) {
      if (window === window.top) {
        return true;
      }
      try {
        // Try set & read a variable on the given window
        // If we can successfully read the value then it means the given window has the same origin
        // as the window that is currently executing the script
        var testVariableName = 'test_var_' + Math.random().toString(16).substr(2);
        window[testVariableName] = true;
        return window[testVariableName];
      } catch (e) {
        // A exception will be thrown if the windows doesn't have the same origin
      }
      return false;
    };
    _proto._handleResponse = function _handleResponse(event) {
      var data = event.data;
      var pendingCallback = this._pendingCallbacks[data.mid];
      if (pendingCallback) {
        delete this._pendingCallbacks[data.mid];
        pendingCallback.apply(window, data.args);
      }
    };
    _proto.registerRequestNotifier = function registerRequestNotifier(cb) {
      this._registeredRequestNotifier = cb;
    };
    _proto._handleRequest = function _handleRequest(event, reg) {
      function sendResponse() {
        var args = util.sanitizeStructuredClone(util.argumentsToArray(arguments));
        if (event.source && event.source.postMessage) {
          // only post a message if the source of the event still exists
          event.source.postMessage({
            mid: event.data.mid,
            type: 'resp',
            forPlugin: true,
            args: args
          }, reg.extension.url);
        } else {
          console.warn("_handleRequest postMessage skipped as event source missing.");
        }
      }
      var data = event.data;
      var module = this._registeredAPIModules[data.mod];
      var extension = this.getRegisteredExtensions(reg.extension)[0];
      if (module) {
        var fnName = data.fn;
        if (data._cls) {
          var Cls = module[data._cls];
          var ns = data.mod + '-' + data._cls + '-';
          sendResponse._id = data._id;
          if (fnName === 'constructor') {
            if (!Cls._construct) {
              Cls.constructor.prototype._destroy = function () {
                delete this._context._proxies[ns + this._id];
              };
              Cls._construct = function () {
                for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
                  args[_key] = arguments[_key];
                }
                var inst = _construct(Cls.constructor, args);
                var callback = args[args.length - 1];
                inst._id = callback._id;
                inst._context = callback._context;
                inst._context._proxies[ns + inst._id] = inst;
                return inst;
              };
            }
            module = Cls;
            fnName = '_construct';
          } else {
            module = extension._proxies[ns + data._id];
          }
        }
        var method = module[fnName];
        if (method) {
          var methodArgs = data.args;
          var padLength = method.length - 1;
          if (fnName === '_construct') {
            padLength = module.constructor.length - 1;
          }
          sendResponse._context = extension;
          methodArgs = this._padUndefinedArguments(methodArgs, padLength);
          methodArgs.push(sendResponse);
          var promiseResult = method.apply(module, methodArgs);
          if (method.returnsPromise) {
            if (!(typeof promiseResult === 'object' || typeof promiseResult === 'function') || typeof promiseResult.then !== 'function') {
              sendResponse('Defined module method did not return a promise.');
            } else {
              promiseResult.then(function (result) {
                sendResponse(undefined, result);
              }).catch(function (err) {
                err = err instanceof Error ? err.message : err;
                sendResponse(err);
              });
            }
          }
          if (this._registeredRequestNotifier) {
            this._registeredRequestNotifier.call(null, {
              module: data.mod,
              fn: data.fn,
              type: data.type,
              args: methodArgs,
              addon_key: reg.extension.addon_key,
              key: reg.extension.key,
              extension_id: reg.extension_id
            });
          }
        }
      }
    };
    _proto._handleBroadcast = function _handleBroadcast(event, reg) {
      var event_data = event.data;
      var targetSpec = function targetSpec(r) {
        return r.extension.addon_key === reg.extension.addon_key && r.extension_id !== reg.extension_id;
      };
      this.dispatch(event_data.etyp, targetSpec, event_data.evnt, null, null);
    };
    _proto._handleKeyTriggered = function _handleKeyTriggered(event, reg) {
      var eventData = event.data;
      var keycodeEntry = this._keycodeKey(eventData.keycode, eventData.modifiers, reg.extension_id);
      var listeners = this._keycodeCallbacks[keycodeEntry];
      if (listeners) {
        listeners.forEach(function (listener) {
          listener.call(null, {
            addon_key: reg.extension.addon_key,
            key: reg.extension.key,
            extension_id: reg.extension_id,
            keycode: eventData.keycode,
            modifiers: eventData.modifiers
          });
        }, this);
      }
    };
    _proto.defineAPIModule = function defineAPIModule(module, moduleName) {
      moduleName = moduleName || '_globals';
      this._registeredAPIModules[moduleName] = util.extend({}, this._registeredAPIModules[moduleName] || {}, module);
      return this._registeredAPIModules;
    };
    _proto.isAPIModuleDefined = function isAPIModuleDefined(moduleName) {
      return typeof this._registeredAPIModules[moduleName] !== 'undefined';
    };
    _proto._pendingEventKey = function _pendingEventKey(targetSpec, time) {
      var key = targetSpec.addon_key || 'global';
      if (targetSpec.key) {
        key = key + "@@" + targetSpec.key;
      }
      key = key + "@@" + time;
      return key;
    };
    _proto.queueEvent = function queueEvent(type, targetSpec, event, callback) {
      var loaded_frame,
        targets = this._findRegistrations(targetSpec);
      loaded_frame = targets.some(function (target) {
        return target.registered_events !== undefined;
      }, this);
      if (loaded_frame) {
        this.dispatch(type, targetSpec, event, callback);
      } else {
        this._cleanupInvalidEvents();
        var time = new Date().getTime();
        this._pendingEvents[this._pendingEventKey(targetSpec, time)] = {
          type: type,
          targetSpec: targetSpec,
          event: event,
          callback: callback,
          time: time,
          uid: util.randomString()
        };
      }
    };
    _proto._cleanupInvalidEvents = function _cleanupInvalidEvents() {
      var _this2 = this;
      var now = new Date().getTime();
      var keys = Object.keys(this._pendingEvents);
      keys.forEach(function (index) {
        var element = _this2._pendingEvents[index];
        var eventIsValid = now - element.time <= VALID_EVENT_TIME_MS;
        if (!eventIsValid) {
          delete _this2._pendingEvents[index];
        }
      });
    };
    _proto._handleEventQuery = function _handleEventQuery(message, extension) {
      var _this3 = this;
      var executed = {};
      var now = new Date().getTime();
      var keys = Object.keys(this._pendingEvents);
      keys.forEach(function (index) {
        var element = _this3._pendingEvents[index];
        var eventIsValid = now - element.time <= VALID_EVENT_TIME_MS;
        var isSameTarget = !element.targetSpec || _this3._findRegistrations(element.targetSpec).length !== 0;
        if (isSameTarget && element.targetSpec.key) {
          isSameTarget = element.targetSpec.addon_key === extension.extension.addon_key && element.targetSpec.key === extension.extension.key;
        }
        if (eventIsValid && isSameTarget) {
          executed[index] = element;
          element.targetSpec = element.targetSpec || {};
          _this3.dispatch(element.type, element.targetSpec, element.event, element.callback, message.source);
        } else if (!eventIsValid) {
          delete _this3._pendingEvents[index];
        }
      });
      this._registeredExtensions[extension.extension_id].registered_events = message.data.args;
      return executed;
    };
    _proto._handleUnload = function _handleUnload(event, reg) {
      if (!reg) {
        return;
      }
      if (reg.extension_id && this._registeredExtensions[reg.extension_id]) {
        delete this._registeredExtensions[reg.extension_id].source;
      }
      if (reg.unloadCallback) {
        reg.unloadCallback(event.data.eid);
      }
    };
    _proto.dispatch = function dispatch(type, targetSpec, event, callback, source) {
      function sendEvent(reg, evnt) {
        if (reg.source && reg.source.postMessage) {
          var mid;
          if (callback) {
            mid = util.randomString();
            this._pendingCallbacks[mid] = callback;
          }
          reg.source.postMessage({
            type: 'evt',
            mid: mid,
            etyp: type,
            evnt: evnt
          }, reg.extension.url);
        }
      }
      var registrations = this._findRegistrations(targetSpec || {});
      registrations.forEach(function (reg) {
        if (source && !reg.source) {
          reg.source = source;
        }
        if (reg.source) {
          util._bind(this, sendEvent)(reg, event);
        }
      }, this);
    };
    _proto._findRegistrations = function _findRegistrations(targetSpec) {
      var _this4 = this;
      if (this._registeredExtensions.length === 0) {
        util.error('no registered extensions', this._registeredExtensions);
        return [];
      }
      var keys = Object.getOwnPropertyNames(targetSpec);
      var registrations = Object.getOwnPropertyNames(this._registeredExtensions).map(function (key) {
        return _this4._registeredExtensions[key];
      });
      if (targetSpec instanceof Function) {
        return registrations.filter(targetSpec);
      } else {
        return registrations.filter(function (reg) {
          return keys.every(function (key) {
            return reg.extension[key] === targetSpec[key];
          });
        });
      }
    };
    _proto.registerExtension = function registerExtension(extension_id, data) {
      data._proxies = {};
      data.extension_id = extension_id;
      this._registeredExtensions[extension_id] = data;
    };
    _proto._keycodeKey = function _keycodeKey(key, modifiers, extension_id) {
      var code = key;
      if (modifiers) {
        if (typeof modifiers === "string") {
          modifiers = [modifiers];
        }
        modifiers.sort();
        modifiers.forEach(function (modifier) {
          code += '$$' + modifier;
        }, this);
      }
      return code + '__' + extension_id;
    };
    _proto.registerKeyListener = function registerKeyListener(extension_id, key, modifiers, callback) {
      if (typeof modifiers === "string") {
        modifiers = [modifiers];
      }
      var reg = this._registeredExtensions[extension_id];
      var keycodeEntry = this._keycodeKey(key, modifiers, extension_id);
      if (!this._keycodeCallbacks[keycodeEntry]) {
        this._keycodeCallbacks[keycodeEntry] = [];
        reg.source.postMessage({
          type: 'key_listen',
          keycode: key,
          modifiers: modifiers,
          action: 'add'
        }, reg.extension.url);
      }
      this._keycodeCallbacks[keycodeEntry].push(callback);
    };
    _proto.unregisterKeyListener = function unregisterKeyListener(extension_id, key, modifiers, callback) {
      var keycodeEntry = this._keycodeKey(key, modifiers, extension_id);
      var potentialCallbacks = this._keycodeCallbacks[keycodeEntry];
      var reg = this._registeredExtensions[extension_id];
      if (potentialCallbacks) {
        if (callback) {
          var index = potentialCallbacks.indexOf(callback);
          this._keycodeCallbacks[keycodeEntry].splice(index, 1);
        } else {
          delete this._keycodeCallbacks[keycodeEntry];
        }
        if (reg.source && reg.source.postMessage) {
          reg.source.postMessage({
            type: 'key_listen',
            keycode: key,
            modifiers: modifiers,
            action: 'remove'
          }, reg.extension.url);
        }
      }
    };
    _proto.registerClickHandler = function registerClickHandler(callback) {
      if (typeof callback !== 'function') {
        throw new Error('callback must be a function');
      }
      this._clickHandlers.push(callback);
    };
    _proto._handleAddonClick = function _handleAddonClick(event, reg) {
      for (var i = 0; i < this._clickHandlers.length; i++) {
        if (typeof this._clickHandlers[i] === 'function') {
          this._clickHandlers[i]({
            addon_key: reg.extension.addon_key,
            key: reg.extension.key,
            extension_id: reg.extension_id
          });
        }
      }
    };
    _proto.unregisterClickHandler = function unregisterClickHandler() {
      this._clickHandlers = [];
    };
    _proto.getApiSpec = function getApiSpec(addonKey) {
      var _this5 = this;
      function getModuleDefinition(mod) {
        return Object.getOwnPropertyNames(mod).reduce(function (accumulator, memberName) {
          var member = mod[memberName];
          switch (typeof member) {
            case 'function':
              accumulator[memberName] = {
                args: util.argumentNames(member),
                returnsPromise: member.returnsPromise || false
              };
              break;
            case 'object':
              if (member.hasOwnProperty('constructor')) {
                accumulator[memberName] = getModuleDefinition(member);
              }
              break;
          }
          return accumulator;
        }, {});
      }
      return Object.getOwnPropertyNames(this._registeredAPIModules).reduce(function (accumulator, moduleName) {
        var module = _this5._registeredAPIModules[moduleName];
        if (typeof module.addonKey === 'undefined' || module.addonKey === addonKey) {
          accumulator[moduleName] = getModuleDefinition(module);
        }
        return accumulator;
      }, {});
    };
    _proto._originEqual = function _originEqual(url, origin) {
      function strCheck(str) {
        return typeof str === 'string' && str.length > 0;
      }
      var urlOrigin = util.getOrigin(url);
      // check strings are strings and they contain something
      if (!strCheck(url) || !strCheck(origin) || !strCheck(urlOrigin)) {
        return false;
      }
      return origin === urlOrigin;
    }

    // validate origin of postMessage
    ;
    _proto._checkOrigin = function _checkOrigin(event, reg) {
      var no_source_types = ['init'];
      var isNoSourceType = reg && !reg.source && no_source_types.indexOf(event.data.type) > -1;
      var sourceTypeMatches = reg && event.source === reg.source;
      var hasExtensionUrl = reg && this._originEqual(reg.extension.url, event.origin);
      var isValidOrigin = hasExtensionUrl && (isNoSourceType || sourceTypeMatches);

      // get_host_offset fires before init
      if (event.data.type === 'get_host_offset' && window === window.top) {
        isValidOrigin = true;
      }

      // check undefined for chromium (Issue 395010)
      if (event.data.type === 'unload' && (sourceTypeMatches || event.source === undefined)) {
        isValidOrigin = true;
      }
      return isValidOrigin;
    };
    _proto.getRegisteredExtensions = function getRegisteredExtensions(filter) {
      if (filter) {
        return this._findRegistrations(filter);
      }
      return this._registeredExtensions;
    };
    _proto.unregisterExtension = function unregisterExtension(filter) {
      var registrations = this._findRegistrations(filter);
      if (registrations.length !== 0) {
        registrations.forEach(function (registration) {
          var _this6 = this;
          var keys = Object.keys(this._pendingEvents);
          keys.forEach(function (index) {
            var element = _this6._pendingEvents[index];
            var targetSpec = element.targetSpec || {};
            if (targetSpec.addon_key === registration.extension.addon_key && targetSpec.key === registration.extension.key) {
              delete _this6._pendingEvents[index];
            }
          });
          delete this._registeredExtensions[registration.extension_id];
        }, this);
      }
    };
    _proto.setFeatureFlagGetter = function setFeatureFlagGetter(getBooleanFeatureFlag) {
      this._getBooleanFeatureFlag = getBooleanFeatureFlag;
    };
    return XDMRPC;
  }(PostMessage);

  var Connect = /*#__PURE__*/function () {
    function Connect() {
      this._xdm = new XDMRPC();
    }

    /**
     * Send a message to iframes matching the targetSpec. This message is added to
     *  a message queue for delivery to ensure the message is received if an iframe
     *  has not yet loaded
     *
     * @param type The name of the event type
     * @param targetSpec The spec to match against extensions when sending this event
     * @param event The event payload
     * @param callback A callback to be executed when the remote iframe calls its callback
     */
    var _proto = Connect.prototype;
    _proto.dispatch = function dispatch(type, targetSpec, event, callback) {
      this._xdm.queueEvent(type, targetSpec, event, callback);
      return this.getExtensions(targetSpec);
    }

    /**
     * Send a message to iframes matching the targetSpec immediately. This message will
     *  only be sent to iframes that are already open, and will not be delivered if none
     *  are currently open.
     *
     * @param type The name of the event type
     * @param targetSpec The spec to match against extensions when sending this event
     * @param event The event payload
     */;
    _proto.broadcast = function broadcast(type, targetSpec, event) {
      this._xdm.dispatch(type, targetSpec, event, null, null);
      return this.getExtensions(targetSpec);
    };
    _proto._createId = function _createId(extension) {
      if (!extension.addon_key || !extension.key) {
        throw Error('Extensions require addon_key and key');
      }
      return extension.addon_key + '__' + extension.key + '__' + util.randomString();
    }
    /**
    * Creates a new iframed module, without actually creating the DOM element.
    * The iframe attributes are passed to the 'setupCallback', which is responsible for creating
    * the DOM element and returning the window reference.
    *
    * @param extension The extension definition. Example:
    *   {
    *     addon_key: 'my-addon',
    *     key: 'my-module',
    *     url: 'https://example.com/my-module',
    *     options: {
    *         autoresize: false,
    *         hostOrigin: 'https://connect-host.example.com/'
    *     }
    *   }
    *
    * @param initCallback The optional initCallback is called when the bridge between host and iframe is established.
    **/;
    _proto.create = function create(extension, initCallback, unloadCallback) {
      var extension_id = this.registerExtension(extension, initCallback, unloadCallback);
      var options = extension.options || {};
      var data = {
        extension_id: extension_id,
        api: this._xdm.getApiSpec(extension.addon_key),
        origin: util.locationOrigin(),
        options: options
      };
      return {
        id: extension_id,
        name: JSON.stringify(data),
        src: extension.url
      };
    }

    // This is called from ACJS
    // noinspection JSUnusedGlobalSymbols
    ;
    _proto.registerRequestNotifier = function registerRequestNotifier(callback) {
      this._xdm.registerRequestNotifier(callback);
    };
    _proto.registerExtension = function registerExtension(extension, initCallback, unloadCallback) {
      var extension_id = this._createId(extension);
      this._xdm.registerExtension(extension_id, {
        extension: extension,
        initCallback: initCallback,
        unloadCallback: unloadCallback
      });
      return extension_id;
    };
    _proto.registerKeyListener = function registerKeyListener(extension_id, key, modifiers, callback) {
      this._xdm.registerKeyListener(extension_id, key, modifiers, callback);
    };
    _proto.unregisterKeyListener = function unregisterKeyListener(extension_id, key, modifiers, callback) {
      this._xdm.unregisterKeyListener(extension_id, key, modifiers, callback);
    };
    _proto.registerClickHandler = function registerClickHandler(callback) {
      this._xdm.registerClickHandler(callback);
    };
    _proto.unregisterClickHandler = function unregisterClickHandler() {
      this._xdm.unregisterClickHandler();
    };
    _proto.defineModule = function defineModule(moduleName, module, options) {
      this._xdm.defineAPIModule(module, moduleName, options);
    };
    _proto.isModuleDefined = function isModuleDefined(moduleName) {
      return this._xdm.isAPIModuleDefined(moduleName);
    };
    _proto.defineGlobals = function defineGlobals(module) {
      this._xdm.defineAPIModule(module);
    };
    _proto.getExtensions = function getExtensions(filter) {
      return this._xdm.getRegisteredExtensions(filter);
    };
    _proto.unregisterExtension = function unregisterExtension(filter) {
      return this._xdm.unregisterExtension(filter);
    };
    _proto.returnsPromise = function returnsPromise(wrappedMethod) {
      wrappedMethod.returnsPromise = true;
    };
    _proto.setFeatureFlagGetter = function setFeatureFlagGetter(getBooleanFeatureFlag) {
      this._xdm.setFeatureFlagGetter(getBooleanFeatureFlag);
    };
    _proto.registerExistingExtension = function registerExistingExtension(extension_id, data) {
      return this._xdm.registerExtension(extension_id, data);
    };
    return Connect;
  }();

  var host$1 = new Connect();

  var _excluded$2 = ["contextJwt", "url"];
  var EventActions = {
    targetSpecNoDialogs: function targetSpecNoDialogs(registration) {
      var _registration$extensi;
      return (registration == null || (_registration$extensi = registration.extension) == null || (_registration$extensi = _registration$extensi.options) == null ? void 0 : _registration$extensi.moduleType) !== 'dialogs';
    },
    broadcast: function broadcast(type, targetSpec, event) {
      host$1.dispatch(type, targetSpec, event);
      EventDispatcher$1.dispatch('event-dispatch', {
        type: type,
        targetSpec: targetSpec,
        event: event
      });
    },
    broadcastPublic: function broadcastPublic(type, event, sender) {
      EventDispatcher$1.dispatch('event-public-dispatch', {
        type: type,
        event: event,
        sender: sender
      });
      var _ref = sender.options || {};
        _ref.contextJwt;
        _ref.url;
        var filteredOptions = _objectWithoutPropertiesLoose(_ref, _excluded$2);
      host$1.dispatch(type, {}, {
        sender: {
          addonKey: sender.addon_key,
          key: sender.key,
          options: util.sanitizeStructuredClone(filteredOptions)
        },
        event: event
      });
    }
  };

  function escapeSelector(s) {
    if (!s) {
      throw new Error('No selector to escape');
    }
    return s.replace(/[!"#$%&'()*+,.\/:;<=>?@[\\\]^`{|}~]/g, '\\$&');
  }
  function stringToDimension(value) {
    var percent = false;
    var unit = 'px';
    if (typeof value === 'string') {
      percent = value.indexOf('%') === value.length - 1;
      value = parseInt(value, 10);
      if (percent) {
        unit = '%';
      }
    }
    if (!isNaN(value)) {
      return value + unit;
    }
  }
  function getIframeByExtensionId(id) {
    return $('iframe#' + escapeSelector(id));
  }
  function first(arr, numb) {
    if (numb) {
      return arr.slice(0, numb);
    }
    return arr[0];
  }
  function last(arr) {
    return arr[arr.length - 1];
  }
  function pick(obj, keys) {
    if (typeof obj !== 'object') {
      return {};
    }
    return Object.keys(obj).filter(function (key) {
      return keys.indexOf(key) >= 0;
    }).reduce(function (newObj, key) {
      var _Object$assign;
      return Object.assign(newObj, (_Object$assign = {}, _Object$assign[key] = obj[key], _Object$assign));
    }, {});
  }
  function debounce$1(fn, wait) {
    var timeout;
    return function () {
      var ctx = this;
      var args = [].slice.call(arguments);
      function later() {
        timeout = null;
        fn.apply(ctx, args);
      }
      if (timeout) {
        clearTimeout(timeout);
      }
      timeout = setTimeout(later, wait || 50);
    };
  }
  function isSupported(domElem, attr, value, defaultValue) {
    if (domElem && domElem[attr] && domElem[attr].supports) {
      return domElem[attr].supports(value);
    } else {
      return defaultValue;
    }
  }
  function guessProductFromModuleDefined(connectHost) {
    if (connectHost.isModuleDefined('jira')) {
      return 'Jira';
    }
    if (connectHost.isModuleDefined('confluence')) {
      return 'Confluence';
    }
    return '';
  }
  var Util = {
    escapeSelector: escapeSelector,
    stringToDimension: stringToDimension,
    getIframeByExtensionId: getIframeByExtensionId,
    first: first,
    last: last,
    pick: pick,
    debounce: debounce$1,
    isSupported: isSupported,
    extend: Object.assign,
    guessProductFromModuleDefined: guessProductFromModuleDefined
  };

  var events = {
    emit: function emit(name) {
      for (var _len = arguments.length, args = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        args[_key - 1] = arguments[_key];
      }
      var callback = Util.last(args);
      args = Util.first(args, -1);
      EventActions.broadcast(name, {
        addon_key: callback._context.extension.addon_key
      }, args);
    },
    emitPublic: function emitPublic(name) {
      for (var _len2 = arguments.length, args = new Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
        args[_key2 - 1] = arguments[_key2];
      }
      var callback = Util.last(args);
      var extension = callback._context.extension;
      args = Util.first(args, -1);
      EventActions.broadcastPublic(name, args, extension);
    },
    emitToDataProvider: function emitToDataProvider() {
      for (var _len3 = arguments.length, args = new Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
        args[_key3] = arguments[_key3];
      }
      var callback = Util.last(args);
      callback._context.extension;
      args = Util.first(args, -1);
      EventActions.broadcast('dataProviderEvent', {
        addon_key: callback._context.extension.addon_key
      }, args);
    }
  };

  var DialogExtensionActions = {
    open: function open(extension, options) {
      EventDispatcher$1.dispatch('dialog-extension-open', {
        extension: extension,
        options: options
      });
    },
    close: function close() {
      EventDispatcher$1.dispatch('dialog-close-active', {});
    },
    addUserButton: function addUserButton(options, extension) {
      EventDispatcher$1.dispatch('dialog-button-add', {
        button: {
          text: options.text,
          identifier: options.identifier,
          data: {
            userButton: true
          }
        },
        extension: extension
      });
    }
  };

  var DialogActions = {
    close: function close(data) {
      EventDispatcher$1.dispatch('dialog-close', {
        dialog: data.dialog,
        extension: data.extension,
        customData: data.customData
      });
    },
    closeActive: function closeActive(data) {
      EventDispatcher$1.dispatch('dialog-close-active', data);
    },
    clickButton: function clickButton(identifier, $el, extension) {
      EventDispatcher$1.dispatch('dialog-button-click', {
        identifier: identifier,
        $el: $el,
        extension: extension
      });
    },
    toggleButton: function toggleButton(data) {
      EventDispatcher$1.dispatch('dialog-button-toggle', data);
    },
    toggleButtonVisibility: function toggleButtonVisibility(data) {
      EventDispatcher$1.dispatch('dialog-button-toggle-visibility', data);
    }
  };

  var DomEventActions = {
    registerKeyEvent: function registerKeyEvent(data) {
      host$1.registerKeyListener(data.extension_id, data.key, data.modifiers, data.callback);
      EventDispatcher$1.dispatch('dom-event-register', data);
    },
    unregisterKeyEvent: function unregisterKeyEvent(data) {
      host$1.unregisterKeyListener(data.extension_id, data.key, data.modifiers, data.callback);
      EventDispatcher$1.dispatch('dom-event-unregister', data);
    },
    registerWindowKeyEvent: function registerWindowKeyEvent(data) {
      window.addEventListener('keydown', function (event) {
        if (event.keyCode === data.keyCode) {
          data.callback();
        }
      });
    },
    registerClickHandler: function registerClickHandler(handleIframeClick) {
      host$1.registerClickHandler(function (data) {
        var iframe = document.getElementById(data.extension_id);
        if (iframe) {
          handleIframeClick(iframe, data);
        }
      });
    },
    unregisterClickHandler: function unregisterClickHandler() {
      host$1.unregisterClickHandler();
    }
  };

  var ButtonUtils = /*#__PURE__*/function () {
    function ButtonUtils() {}
    var _proto = ButtonUtils.prototype;
    // button identifier for XDM. NOT an id attribute
    _proto.randomIdentifier = function randomIdentifier() {
      return Math.random().toString(16).substring(7);
    };
    return ButtonUtils;
  }();
  var buttonUtilsInstance = new ButtonUtils();

  var threshold = 0.25;
  var targets = [];
  var observe;
  var observed = function observed(target) {
    targets = targets.filter(function (_ref) {
      var element = _ref.element,
        callback = _ref.callback;
      if (element === target) {
        callback();
        return false;
      }
      return true;
    });
  };
  if ('IntersectionObserver' in window && 'IntersectionObserverEntry' in window) {
    var observer = new IntersectionObserver(function (entries) {
      entries.forEach(function (_ref2) {
        var intersectionRatio = _ref2.intersectionRatio,
          target = _ref2.target;
        if (intersectionRatio > 0) {
          observer.unobserve(target);
          observed(target);
        }
      });
    }, {
      threshold: threshold
    });
    observe = observer.observe.bind(observer);
  }
  var observe$1 = (function (element, callback) {
    if (typeof callback === 'function' && element instanceof Element) {
      targets.push({
        element: element,
        callback: callback
      });
      observe(element);
    }
  });

  /**
   * Timings beyond 20 seconds (connect's load timeout) will be clipped to an X.
   * @const
   * @type {int}
   */
  var LOADING_TIME_THRESHOLD = 20000;

  /**
   * Trim extra zeros from the load time.
   * @const
   * @type {int}
   */
  var LOADING_TIME_TRIMP_PRECISION = 100;

  /**
   * @param {number} value
   * @returns {number}
   */
  function bucketLoadingTime(value) {
    var bucket = value > LOADING_TIME_THRESHOLD ? LOADING_TIME_THRESHOLD : value;
    return Math.ceil(bucket / LOADING_TIME_TRIMP_PRECISION) * LOADING_TIME_TRIMP_PRECISION;
  }

  // Only send 0.1% of content resolver events
  var CONTENT_RESOLVER_SAMPLE_RATE = 0.1;

  // Only send 0.1% of invoke method events
  var INVOKE_METHOD_SAMPLE_RATE = 0.1;
  var ALLOWLISTED_METHODS = ['saveMacro'];
  var AnalyticsDispatcher = /*#__PURE__*/function () {
    function AnalyticsDispatcher() {
      this._addons = {};
      this._sampleLoadEvent = new Map();
    }

    /**
     * Determines whether a sampled event should be sent based on given rate.
     * @param {int} rate Rate as an integer percentage.
     * @returns {boolean}
     */
    var _proto = AnalyticsDispatcher.prototype;
    _proto._shouldSampleEvent = function _shouldSampleEvent(rate) {
      return Math.random() * 100 <= rate;
    }

    /**
     * We want to keep sampling consistent across the various events that are sent for a single iframe load.
     * @param extension
     * @returns {boolean}
     */;
    _proto._shouldSampleLoadEvent = function _shouldSampleLoadEvent(extension) {
      // Only send 0.01% of iframe load events
      var IFRAME_LOAD_SAMPLE_RATE = 0.01;
      if (this._sampleLoadEvent.has(extension.id)) {
        return this._sampleLoadEvent.get(extension.id);
      }
      var shouldSample = this._shouldSampleEvent(IFRAME_LOAD_SAMPLE_RATE);
      this._sampleLoadEvent.set(extension.id, shouldSample);
      return shouldSample;
    }

    /**
     * Track an event via GasV3.
     *
     * Warning! Confluence does not use ac/analytics, we have to manually forward it
     * The legacy way of doing this was using individual hooks for each event. Many existing events have dedicated hooks.
     * If you are creating a new event, you can use sendExternal=true to avoid the need for a dedicated hook.
     *
     * https://stash.atlassian.com/projects/ATLASSIAN/repos/atlassian-frontend-monorepo/browse/confluence/packages/confluence-connect-support/src/ConnectAnalyticsPublishSupport.js
     * @param eventType {string}
     * @param event {object}
     * @param sendExternal {boolean}
     * @private
     */;
    _proto._trackGasV3 = function _trackGasV3(eventType, event, sendExternal) {
      if (event.attributes) {
        event.attributes.apVersion = window._AP && window._AP.version ? window._AP.version : undefined;
      }
      if (sendExternal) {
        this._forwardGasV3AnalyticsEvent(eventType, event);
      }
      try {
        var analyticsCrossProduct = window.require('ac/analytics');
        analyticsCrossProduct.emitGasV3(eventType, event);
      } catch (e) {
        if (!window.Confluence && !window.__karma__) {
          // this is not serious. It usually means the product is doing analytics using another mechanism
          // Confluence no longer uses ac/analytics so we should stop reporting it as an error
          console.info('Connect GasV3 catch', e);
        }
      }
    };
    _proto._trackAndForwardGasV3 = function _trackAndForwardGasV3(eventType, event) {
      this._trackGasV3(eventType, event, true);
    };
    _proto._forwardGasV3AnalyticsEvent = function _forwardGasV3AnalyticsEvent(eventType, event) {
      EventDispatcher$1.dispatch('analytics-forward-event', eventType, event);
    };
    _proto._time = function _time() {
      return window.performance && window.performance.now ? window.performance.now() : new Date().getTime();
    };
    _proto.trackLoadingStarted = function trackLoadingStarted(extension) {
      if (this._addons && extension && extension.id) {
        extension.startLoading = this._time();
        this._addons[extension.id] = extension;
      } else {
        console.error('ACJS: cannot track loading analytics', this._addons, extension);
      }
    };
    _proto.trackGasV3UseOfDeprecatedMethod = function trackGasV3UseOfDeprecatedMethod(methodUsed, extension) {
      this._trackAndForwardGasV3('operational', {
        action: 'deprecatedMethod',
        actionSubject: 'connectAddon',
        actionSubjectId: extension.addon_key,
        attributes: {
          moduleKey: extension.key,
          methodUsed: methodUsed
        },
        source: extension.addon_key
      });
    };
    _proto.trackGasV3MultipleDialogOpening = function trackGasV3MultipleDialogOpening(dialogType, extension) {
      this._trackAndForwardGasV3('operational', {
        action: 'multipleDialogOpened',
        actionSubject: 'connectAddon',
        actionSubjectId: extension.addon_key,
        attributes: {
          moduleKey: extension.key,
          dialogType: dialogType
        },
        source: extension.addon_key
      });
    };
    _proto.trackIframePerformance = function trackIframePerformance(metrics, extension) {
      if (!this._shouldSampleLoadEvent(extension)) {
        return;
      }
      this._trackGasV3('operational', {
        source: extension.addon_key,
        action: 'iframeRendered',
        actionSubject: 'connectAddon',
        actionSubjectId: extension.addon_key,
        attributes: {
          key: extension['key'],
          pearApp: this._getPearApp(extension),
          moduleType: this._getModuleType(extension),
          iframeIsCacheable: this._isCacheable(extension),
          moduleLocation: this._getModuleLocation(extension),
          domainLookupTime: metrics.domainLookupTime,
          connectionTime: metrics.connectionTime,
          decodedBodySize: metrics.decodedBodySize,
          domContentLoadedTime: metrics.domContentLoadedTime,
          fetchTime: metrics.fetchTime
        }
      });
    };
    _proto.trackGasV3WebVitals = function trackGasV3WebVitals(metrics, extension) {
      if (!this._shouldSampleLoadEvent(extension)) {
        return;
      }
      this._trackAndForwardGasV3('operational', {
        source: extension.addon_key,
        action: 'webVitals',
        actionSubject: 'connectAddon',
        actionSubjectId: extension.addon_key,
        attributes: Object.assign({}, metrics, {
          key: extension['key'],
          pearApp: this._getPearApp(extension),
          moduleType: this._getModuleType(extension),
          iframeIsCacheable: this._isCacheable(extension),
          moduleLocation: this._getModuleLocation(extension)
        })
      });
    };
    _proto.dispatch = function dispatch(name, data) {
      this._trackGasV3External(name, data);
    };
    _proto.trackExternal = function trackExternal(name, data) {
      this._trackGasV3External(name, data);
    };
    _proto._trackGasV3External = function _trackGasV3External(name, data) {
      this._trackAndForwardGasV3('operational', {
        action: 'externalEvent',
        actionSubject: 'connectAddon',
        actionSubjectId: name,
        attributes: _extends({
          eventName: name
        }, typeof data === 'object' ? data : {
          values: data
        }),
        source: 'connectAddon'
      });
    }

    /**
    * method called when an iframe's loading metrics gets corrupted
    * to destroy the analytics as they cannot be reliable
    * this should be called when:
    * 1. the product calls iframe creation multiple times for the same connect addon
    * 2. the iframe is moved / repainted causing a window.reload event
    * 3. user right clicks iframe and reloads it
    */;
    _proto._resetAnalyticsDueToUnreliable = function _resetAnalyticsDueToUnreliable(extensionId) {
      if (!extensionId) {
        throw new Error('Cannot reset analytics due to no extension id');
      }
      if (this._addons[extensionId]) {
        clearTimeout(this._addons[extensionId]);
        delete this._addons[extensionId];
      } else {
        console.info('Cannot clear analytics, cache does not contain extension id');
      }
    };
    _proto._isCacheable = function _isCacheable(extension) {
      var href = extension.url;
      return href !== undefined && href.indexOf('xdm_e=') === -1;
    };
    _proto._getModuleType = function _getModuleType(extension) {
      return extension.options ? extension.options.moduleType : undefined;
    };
    _proto._getModuleLocation = function _getModuleLocation(extension) {
      return extension.options ? extension.options.moduleLocation : undefined;
    };
    _proto._getPearApp = function _getPearApp(extension) {
      return extension.options && extension.options.pearApp === 'true';
    };
    _proto.trackGasV3Visible = function trackGasV3Visible(extension) {
      if (!this._shouldSampleLoadEvent(extension)) {
        return;
      }
      this._trackGasV3('operational', {
        action: 'iframeViewed',
        actionSubject: 'connectAddon',
        actionSubjectId: extension['addon_key'],
        attributes: {
          moduleType: this._getModuleType(extension),
          iframeIsCacheable: this._isCacheable(extension),
          moduleKey: extension.key,
          moduleLocation: this._getModuleLocation(extension),
          pearApp: this._getPearApp(extension)
        },
        source: extension.addon_key
      });
    };
    _proto.trackGasV3InvokeMethod = function trackGasV3InvokeMethod(extension, module, method, methodArgs) {
      if (!this._shouldSampleEvent(INVOKE_METHOD_SAMPLE_RATE) && !ALLOWLISTED_METHODS.includes(method)) {
        return;
      }
      var args = Array.isArray(methodArgs) ? [].concat(methodArgs) : [];
      var isContextFn = function isContextFn(arg) {
        return typeof arg === 'function' && Boolean(arg._context);
      };

      // Remove undefined values, or the callback context function only at the very end of the array
      while (args.length > 0 && (args[args.length - 1] === undefined || isContextFn(args[args.length - 1]))) {
        args.pop();
      }
      var argTypes = args.map(function (arg) {
        if (Array.isArray(arg)) {
          return 'array';
        } else if (arg === null) {
          return 'null';
        }
        return typeof arg;
      });
      this._trackAndForwardGasV3('operational', {
        action: 'invokeMethod',
        actionSubject: 'connectAddon',
        actionSubjectId: extension['addon_key'],
        attributes: {
          argCount: args.length,
          argTypes: argTypes,
          module: module,
          method: method,
          moduleType: this._getModuleType(extension),
          moduleKey: extension.key,
          moduleLocation: this._getModuleLocation(extension),
          pearApp: this._getPearApp(extension)
        },
        source: extension.addon_key
      });
    };
    _proto.trackGasV3ContentResolver = function trackGasV3ContentResolver(isSuccess, extra) {
      if (!this._shouldSampleEvent(CONTENT_RESOLVER_SAMPLE_RATE)) {
        return;
      }
      var extension = extra.extension;
      var attrs = extra.attrs;
      var action = isSuccess ? 'contentResolverSucceeded' : 'contentResolverFailed';
      this._trackGasV3('operational', {
        action: action,
        actionSubject: 'connectAddon',
        actionSubjectId: extension['addon_key'],
        attributes: Object.assign({}, attrs, {
          moduleType: this._getModuleType(extension),
          moduleKey: extension.key,
          moduleLocation: this._getModuleLocation(extension),
          pearApp: this._getPearApp(extension)
        }),
        source: extension.addon_key
      });
    };
    _proto.trackGasV3LoadingEnded = function trackGasV3LoadingEnded(extension) {
      if (!this._shouldSampleLoadEvent(extension)) {
        return;
      }
      var iframeLoadMillis = this._time() - this._addons[extension.id].startLoading;
      this._trackGasV3('operational', {
        action: 'iframeLoaded',
        actionSubject: 'connectAddon',
        actionSubjectId: extension['addon_key'],
        attributes: {
          moduleType: this._getModuleType(extension),
          iframeIsCacheable: this._isCacheable(extension),
          iframeLoadMillis: iframeLoadMillis,
          iframeLoadBucket: bucketLoadingTime(iframeLoadMillis),
          moduleKey: extension.key,
          moduleLocation: this._getModuleLocation(extension),
          pearApp: this._getPearApp(extension)
        },
        source: extension.addon_key
      });
    };
    _proto.trackGasV3LoadingTimeout = function trackGasV3LoadingTimeout(extension) {
      if (!this._shouldSampleLoadEvent(extension)) {
        return;
      }
      this._trackGasV3('operational', {
        action: 'iframeTimeout',
        actionSubject: 'connectAddon',
        actionSubjectId: extension['addon_key'],
        attributes: {
          moduleType: this._getModuleType(extension),
          iframeIsCacheable: this._isCacheable(extension),
          moduleKey: extension.key,
          moduleLocation: this._getModuleLocation(extension),
          pearApp: this._getPearApp(extension),
          connectedStatus: typeof window.navigator.onLine === 'boolean' ? window.navigator.onLine.toString() : 'not-supported'
        },
        source: extension.addon_key
      });
    }

    /**
     * @param {WebItemInvokedEventData} data
     */;
    _proto.trackGasV3InlineDialogOpened = function trackGasV3InlineDialogOpened(data) {
      var extension = data.extension;
      this._trackAndForwardGasV3('operational', {
        action: 'inlineDialogOpened',
        actionSubject: 'connectAddon',
        actionSubjectId: extension['addon_key'],
        attributes: {
          moduleKey: extension.key,
          moduleLocation: this._getModuleLocation(extension),
          eventType: data.eventType,
          onHoverEnabled: data.onHover
        },
        source: extension.addon_key
      });
    }

    /**
     * Track a dialog opened via the legacy event-handler (non-imperative) API
     */;
    _proto.trackGasV3LegacyDialogOpened = function trackGasV3LegacyDialogOpened(data) {
      var extension = data.extension;
      this._trackAndForwardGasV3('operational', {
        action: 'legacyDialogOpened',
        actionSubject: 'connectAddon',
        actionSubjectId: extension['addon_key'],
        attributes: {
          moduleKey: extension.key,
          moduleLocation: this._getModuleLocation(extension),
          parentIds: data.parentIds
        },
        source: extension.addon_key
      });
    }

    /**
     * Tracks when the Statsig client is not initialised before the Statsig feature flag is fetched
     * Jira - https://data-portal.internal.atlassian.com/analytics/registry/67270
     * Confluence - https://data-portal.internal.atlassian.com/analytics/registry/67271
     */;
    _proto.trackGasV3FeatureFlagDefaultFalse = function trackGasV3FeatureFlagDefaultFalse(featureFlag) {
      this._trackAndForwardGasV3('operational', {
        action: 'returned',
        actionSubject: 'defaultFalseFeatureFlag',
        actionSubjectId: featureFlag,
        attributes: {
          featureFlag: featureFlag
        },
        source: 'Host'
      });
    }

    /**
     * Tracks when a Connect iframe is clicked
     */;
    _proto.trackGasV3IframeClicked = function trackGasV3IframeClicked(data) {
      this._trackAndForwardGasV3('ui', {
        action: 'iframeClicked',
        actionSubject: 'connectAddon',
        source: 'host',
        attributes: {
          appId: data.addonKey,
          moduleType: data.moduleType,
          moduleLocation: data.moduleLocation
        }
      });
    };
    return AnalyticsDispatcher;
  }();
  var analytics$1 = new AnalyticsDispatcher();
  if ($.fn) {
    EventDispatcher$1.register('iframe-create', function (data) {
      analytics$1.trackLoadingStarted(data.extension);
    });
  }
  EventDispatcher$1.register('method-invoked', function (data) {
    analytics$1.trackGasV3InvokeMethod(data.extension, data.module, data.fn, data.args);
  });
  EventDispatcher$1.register('iframe-bridge-start', function (data) {
    analytics$1.trackLoadingStarted(data.extension);
  });
  EventDispatcher$1.register('iframe-bridge-established', function (data) {
    analytics$1.trackGasV3LoadingEnded(data.extension);
    observe$1(document.getElementById(data.extension.id), function () {
      EventDispatcher$1.dispatch('iframe-visible', data.extension);
      analytics$1.trackGasV3Visible(data.extension);
    });
  });
  EventDispatcher$1.register('iframe-bridge-timeout', function (data) {
    analytics$1.trackGasV3LoadingTimeout(data.extension);
  });
  EventDispatcher$1.register('analytics-deprecated-method-used', function (data) {
    analytics$1.trackGasV3UseOfDeprecatedMethod(data.methodUsed, data.extension);
  });
  EventDispatcher$1.register('analytics-iframe-performance', function (data) {
    analytics$1.trackIframePerformance(data.metrics, data.extension);
  });
  EventDispatcher$1.register('analytics-web-vitals', function (data) {
    analytics$1.trackGasV3WebVitals(data.metrics, data.extension);
  });
  EventDispatcher$1.register('iframe-destroyed', function (data) {
    analytics$1._resetAnalyticsDueToUnreliable(data.extension.extension_id);
  });
  EventDispatcher$1.register('analytics-external-event-track', function (data) {
    analytics$1.trackExternal(data.eventName, data.values);
  });
  EventDispatcher$1.register('analytics-content-resolver-track', function (data) {
    var success = data.success;
    var extra = data.extra;
    analytics$1.trackGasV3ContentResolver(success, extra);
  });
  EventDispatcher$1.register('analytics-inline-dialog-opened', function (data) {
    analytics$1.trackGasV3InlineDialogOpened(data);
  });
  EventDispatcher$1.register('analytics-legacy-dialog-opened', function (data) {
    analytics$1.trackGasV3LegacyDialogOpened(data);
  });
  EventDispatcher$1.register('analytics-track-iframe-clicked', function (data) {
    analytics$1.trackGasV3IframeClicked(data);
  });

  var DialogUtils = /*#__PURE__*/function () {
    function DialogUtils() {}
    var _proto = DialogUtils.prototype;
    _proto._maxDimension = function _maxDimension(val, maxPxVal) {
      var parsed = Util.stringToDimension(val);
      var parsedInt = parseInt(parsed, 10);
      var parsedMaxPxVal = parseInt(maxPxVal, 10);
      if (parsed.indexOf('%') > -1 && parsedInt >= 100 // %
      || parsedInt > parsedMaxPxVal) {
        // px
        return '100%';
      }
      return parsed;
    };
    _proto._closeOnEscape = function _closeOnEscape(options) {
      if (options.closeOnEscape === false) {
        return false;
      } else {
        return true;
      }
    };
    _proto._size = function _size(options) {
      var size = options.size;
      if (options.size === 'x-large') {
        size = 'xlarge';
      }
      if (options.size !== 'maximum' && options.width === '100%' && options.height === '100%') {
        size = 'fullscreen';
      }
      return size;
    };
    _proto._header = function _header(text) {
      var headerText = '';
      switch (typeof text) {
        case 'string':
          headerText = text;
          break;
        case 'object':
          headerText = text.value;
          break;
      }
      return headerText;
    };
    _proto._hint = function _hint(text) {
      if (typeof text === 'string') {
        return text;
      }
      return '';
    };
    _proto._chrome = function _chrome(options) {
      var returnval = false;
      if (typeof options.chrome === 'boolean') {
        returnval = options.chrome;
      }
      if (options.size === 'fullscreen') {
        returnval = true;
      }
      if (options.size === 'maximum') {
        returnval = false;
      }
      return returnval;
    };
    _proto._width = function _width(options) {
      if (options.size) {
        return undefined;
      }
      if (options.width) {
        return this._maxDimension(options.width, $(window).width());
      }
      return '50%';
    };
    _proto._height = function _height(options) {
      if (options.size) {
        return undefined;
      }
      if (options.height) {
        return this._maxDimension(options.height, $(window).height());
      }
      return '50%';
    };
    _proto._actions = function _actions(options) {
      var sanitizedActions = [];
      options = options || {};
      if (!options.actions) {
        sanitizedActions = [{
          name: 'submit',
          identifier: 'submit',
          text: options.submitText || 'Submit',
          type: 'primary',
          disabled: true // disable submit button by default (until the dialog has loaded).
        }, {
          name: 'cancel',
          identifier: 'cancel',
          text: options.cancelText || 'Cancel',
          type: 'link',
          immutable: true
        }];
      }
      if (options.buttons) {
        sanitizedActions = sanitizedActions.concat(this._buttons(options));
      }
      return sanitizedActions;
    };
    _proto._id = function _id(str) {
      if (typeof str !== 'string') {
        str = Math.random().toString(36).substring(2, 8);
      }
      return str;
    }
    // user defined action buttons
    ;
    _proto._buttons = function _buttons(options) {
      var buttons = [];
      if (options.buttons && Array.isArray(options.buttons)) {
        options.buttons.forEach(function (button) {
          var text;
          var identifier;
          var disabled = false;
          if (button.text && typeof button.text === 'string') {
            text = button.text;
          }
          if (button.identifier && typeof button.identifier === 'string') {
            identifier = button.identifier;
          } else {
            identifier = buttonUtilsInstance.randomIdentifier();
          }
          if (button.disabled && button.disabled === true) {
            disabled = true;
          }
          buttons.push({
            text: text,
            identifier: identifier,
            type: 'secondary',
            custom: true,
            disabled: disabled
          });
        });
      }
      return buttons;
    };
    _proto._onHide = function _onHide(options) {
      var noop = function noop() {};
      if (typeof options.onHide === 'function') {
        return options.onHide;
      } else {
        return noop;
      }
    };
    _proto.sanitizeOptions = function sanitizeOptions(options) {
      options = options || {};
      var sanitized = {
        chrome: this._chrome(options),
        header: this._header(options.header),
        hint: this._hint(options.hint),
        width: this._width(options),
        height: this._height(options),
        $content: options.$content,
        extension: options.extension,
        actions: this._actions(options),
        id: this._id(options.id),
        size: options.size,
        closeOnEscape: this._closeOnEscape(options),
        onHide: this._onHide(options)
      };
      sanitized.size = this._size(sanitized);
      return sanitized;
    }
    // such a bad idea! this entire concept needs rewriting in the p2 plugin.
    ;
    _proto.moduleOptionsFromGlobal = function moduleOptionsFromGlobal(addon_key, key) {
      var defaultOptions = {
        chrome: true,
        hostFrameOffset: 1
      };
      if (window._AP && window._AP.dialogModules && window._AP.dialogModules[addon_key] && window._AP.dialogModules[addon_key][key]) {
        return Util.extend({}, defaultOptions, window._AP.dialogModules[addon_key][key].options);
      }
      return false;
    }

    // determines information about dialogs that are about to open and are already open
    ;
    _proto.trackMultipleDialogOpening = function trackMultipleDialogOpening(dialogExtension, options) {
      // check for dialogs that are already open
      var trackingDescription;
      var size = this._size(options);
      if ($('.ap-aui-dialog2:visible').length) {
        // am i in the confluence editor? first check for macro dialogs opened through macro browser, second is editing an existing macro
        if ($('#macro-browser-dialog').length || AJS.Confluence && AJS.Confluence.Editor && AJS.Confluence.Editor.currentEditMode) {
          if (size === 'fullscreen') {
            trackingDescription = 'connect-macro-multiple-fullscreen';
          } else {
            trackingDescription = 'connect-macro-multiple';
          }
        } else {
          trackingDescription = 'connect-multiple';
        }
        analytics$1.trackGasV3MultipleDialogOpening(trackingDescription, dialogExtension);
      }
    }

    // abstracts and handles a failure to find active dialog
    ;
    _proto.assertActiveDialogOrThrow = function assertActiveDialogOrThrow(dialogProvider, addon_key) {
      if (!dialogProvider.isActiveDialog(addon_key)) {
        throw new Error('Failed to find an active dialog for: ' + addon_key);
      }
    };
    return DialogUtils;
  }();
  var dialogUtilsInstance = new DialogUtils();

  var IframeActions = {
    notifyIframeCreated: function notifyIframeCreated($el, extension) {
      EventDispatcher$1.dispatch('iframe-create', {
        $el: $el,
        extension: extension
      });
    },
    notifyBridgeEstablished: function notifyBridgeEstablished($el, extension) {
      EventDispatcher$1.dispatch('iframe-bridge-established', {
        $el: $el,
        extension: extension
      });
    },
    notifyIframeDestroyed: function notifyIframeDestroyed(filter) {
      if (typeof filter === 'string') {
        filter = {
          id: filter
        };
      }
      var extensions = host$1.getExtensions(filter);
      extensions.forEach(function (extension) {
        EventDispatcher$1.dispatch('iframe-destroyed', {
          extension: extension
        });
        host$1.unregisterExtension({
          id: extension.extension_id
        });
      }, this);
    },
    notifyUnloaded: function notifyUnloaded($el, extension) {
      EventDispatcher$1.dispatch('iframe-unload', {
        $el: $el,
        extension: extension
      });
    }
  };

  var queryString = {};

  var strictUriEncode = function (str) {
    return encodeURIComponent(str).replace(/[!'()*]/g, function (x) {
      return "%" + x.charCodeAt(0).toString(16).toUpperCase();
    });
  };

  var token = '%[a-f0-9]{2}';
  var singleMatcher = new RegExp('(' + token + ')|([^%]+?)', 'gi');
  var multiMatcher = new RegExp('(' + token + ')+', 'gi');
  function decodeComponents(components, split) {
    try {
      // Try to decode the entire string first
      return [decodeURIComponent(components.join(''))];
    } catch (err) {
      // Do nothing
    }
    if (components.length === 1) {
      return components;
    }
    split = split || 1;

    // Split the array in 2 parts
    var left = components.slice(0, split);
    var right = components.slice(split);
    return Array.prototype.concat.call([], decodeComponents(left), decodeComponents(right));
  }
  function decode$1(input) {
    try {
      return decodeURIComponent(input);
    } catch (err) {
      var tokens = input.match(singleMatcher) || [];
      for (var i = 1; i < tokens.length; i++) {
        input = decodeComponents(tokens, i).join('');
        tokens = input.match(singleMatcher) || [];
      }
      return input;
    }
  }
  function customDecodeURIComponent(input) {
    // Keep track of all the replacements and prefill the map with the `BOM`
    var replaceMap = {
      '%FE%FF': "\uFFFD\uFFFD",
      '%FF%FE': "\uFFFD\uFFFD"
    };
    var match = multiMatcher.exec(input);
    while (match) {
      try {
        // Decode as big chunks as possible
        replaceMap[match[0]] = decodeURIComponent(match[0]);
      } catch (err) {
        var result = decode$1(match[0]);
        if (result !== match[0]) {
          replaceMap[match[0]] = result;
        }
      }
      match = multiMatcher.exec(input);
    }

    // Add `%C2` at the end of the map to make sure it does not replace the combinator before everything else
    replaceMap['%C2'] = "\uFFFD";
    var entries = Object.keys(replaceMap);
    for (var i = 0; i < entries.length; i++) {
      // Replace all decoded components
      var key = entries[i];
      input = input.replace(new RegExp(key, 'g'), replaceMap[key]);
    }
    return input;
  }
  var decodeUriComponent = function (encodedURI) {
    if (typeof encodedURI !== 'string') {
      throw new TypeError('Expected `encodedURI` to be of type `string`, got `' + typeof encodedURI + '`');
    }
    try {
      encodedURI = encodedURI.replace(/\+/g, ' ');

      // Try the built in decoder first
      return decodeURIComponent(encodedURI);
    } catch (err) {
      // Fallback to a more advanced decoder
      return customDecodeURIComponent(encodedURI);
    }
  };

  var splitOnFirst = function (string, separator) {
    if (!(typeof string === 'string' && typeof separator === 'string')) {
      throw new TypeError('Expected the arguments to be of type `string`');
    }
    if (separator === '') {
      return [string];
    }
    var separatorIndex = string.indexOf(separator);
    if (separatorIndex === -1) {
      return [string];
    }
    return [string.slice(0, separatorIndex), string.slice(separatorIndex + separator.length)];
  };

  var filterObj = function (obj, predicate) {
    var ret = {};
    var keys = Object.keys(obj);
    var isArr = Array.isArray(predicate);
    for (var i = 0; i < keys.length; i++) {
      var key = keys[i];
      var val = obj[key];
      if (isArr ? predicate.indexOf(key) !== -1 : predicate(key, val, obj)) {
        ret[key] = val;
      }
    }
    return ret;
  };

  (function (exports) {
  const strictUriEncode$1 = strictUriEncode;
  const decodeComponent = decodeUriComponent;
  const splitOnFirst$1 = splitOnFirst;
  const filterObject = filterObj;

  const isNullOrUndefined = value => value === null || value === undefined;

  const encodeFragmentIdentifier = Symbol('encodeFragmentIdentifier');

  function encoderForArrayFormat(options) {
  	switch (options.arrayFormat) {
  		case 'index':
  			return key => (result, value) => {
  				const index = result.length;

  				if (
  					value === undefined ||
  					(options.skipNull && value === null) ||
  					(options.skipEmptyString && value === '')
  				) {
  					return result;
  				}

  				if (value === null) {
  					return [...result, [encode(key, options), '[', index, ']'].join('')];
  				}

  				return [
  					...result,
  					[encode(key, options), '[', encode(index, options), ']=', encode(value, options)].join('')
  				];
  			};

  		case 'bracket':
  			return key => (result, value) => {
  				if (
  					value === undefined ||
  					(options.skipNull && value === null) ||
  					(options.skipEmptyString && value === '')
  				) {
  					return result;
  				}

  				if (value === null) {
  					return [...result, [encode(key, options), '[]'].join('')];
  				}

  				return [...result, [encode(key, options), '[]=', encode(value, options)].join('')];
  			};

  		case 'colon-list-separator':
  			return key => (result, value) => {
  				if (
  					value === undefined ||
  					(options.skipNull && value === null) ||
  					(options.skipEmptyString && value === '')
  				) {
  					return result;
  				}

  				if (value === null) {
  					return [...result, [encode(key, options), ':list='].join('')];
  				}

  				return [...result, [encode(key, options), ':list=', encode(value, options)].join('')];
  			};

  		case 'comma':
  		case 'separator':
  		case 'bracket-separator': {
  			const keyValueSep = options.arrayFormat === 'bracket-separator' ?
  				'[]=' :
  				'=';

  			return key => (result, value) => {
  				if (
  					value === undefined ||
  					(options.skipNull && value === null) ||
  					(options.skipEmptyString && value === '')
  				) {
  					return result;
  				}

  				// Translate null to an empty string so that it doesn't serialize as 'null'
  				value = value === null ? '' : value;

  				if (result.length === 0) {
  					return [[encode(key, options), keyValueSep, encode(value, options)].join('')];
  				}

  				return [[result, encode(value, options)].join(options.arrayFormatSeparator)];
  			};
  		}

  		default:
  			return key => (result, value) => {
  				if (
  					value === undefined ||
  					(options.skipNull && value === null) ||
  					(options.skipEmptyString && value === '')
  				) {
  					return result;
  				}

  				if (value === null) {
  					return [...result, encode(key, options)];
  				}

  				return [...result, [encode(key, options), '=', encode(value, options)].join('')];
  			};
  	}
  }

  function parserForArrayFormat(options) {
  	let result;

  	switch (options.arrayFormat) {
  		case 'index':
  			return (key, value, accumulator) => {
  				result = /\[(\d*)\]$/.exec(key);

  				key = key.replace(/\[\d*\]$/, '');

  				if (!result) {
  					accumulator[key] = value;
  					return;
  				}

  				if (accumulator[key] === undefined) {
  					accumulator[key] = {};
  				}

  				accumulator[key][result[1]] = value;
  			};

  		case 'bracket':
  			return (key, value, accumulator) => {
  				result = /(\[\])$/.exec(key);
  				key = key.replace(/\[\]$/, '');

  				if (!result) {
  					accumulator[key] = value;
  					return;
  				}

  				if (accumulator[key] === undefined) {
  					accumulator[key] = [value];
  					return;
  				}

  				accumulator[key] = [].concat(accumulator[key], value);
  			};

  		case 'colon-list-separator':
  			return (key, value, accumulator) => {
  				result = /(:list)$/.exec(key);
  				key = key.replace(/:list$/, '');

  				if (!result) {
  					accumulator[key] = value;
  					return;
  				}

  				if (accumulator[key] === undefined) {
  					accumulator[key] = [value];
  					return;
  				}

  				accumulator[key] = [].concat(accumulator[key], value);
  			};

  		case 'comma':
  		case 'separator':
  			return (key, value, accumulator) => {
  				const isArray = typeof value === 'string' && value.includes(options.arrayFormatSeparator);
  				const isEncodedArray = (typeof value === 'string' && !isArray && decode(value, options).includes(options.arrayFormatSeparator));
  				value = isEncodedArray ? decode(value, options) : value;
  				const newValue = isArray || isEncodedArray ? value.split(options.arrayFormatSeparator).map(item => decode(item, options)) : value === null ? value : decode(value, options);
  				accumulator[key] = newValue;
  			};

  		case 'bracket-separator':
  			return (key, value, accumulator) => {
  				const isArray = /(\[\])$/.test(key);
  				key = key.replace(/\[\]$/, '');

  				if (!isArray) {
  					accumulator[key] = value ? decode(value, options) : value;
  					return;
  				}

  				const arrayValue = value === null ?
  					[] :
  					value.split(options.arrayFormatSeparator).map(item => decode(item, options));

  				if (accumulator[key] === undefined) {
  					accumulator[key] = arrayValue;
  					return;
  				}

  				accumulator[key] = [].concat(accumulator[key], arrayValue);
  			};

  		default:
  			return (key, value, accumulator) => {
  				if (accumulator[key] === undefined) {
  					accumulator[key] = value;
  					return;
  				}

  				accumulator[key] = [].concat(accumulator[key], value);
  			};
  	}
  }

  function validateArrayFormatSeparator(value) {
  	if (typeof value !== 'string' || value.length !== 1) {
  		throw new TypeError('arrayFormatSeparator must be single character string');
  	}
  }

  function encode(value, options) {
  	if (options.encode) {
  		return options.strict ? strictUriEncode$1(value) : encodeURIComponent(value);
  	}

  	return value;
  }

  function decode(value, options) {
  	if (options.decode) {
  		return decodeComponent(value);
  	}

  	return value;
  }

  function keysSorter(input) {
  	if (Array.isArray(input)) {
  		return input.sort();
  	}

  	if (typeof input === 'object') {
  		return keysSorter(Object.keys(input))
  			.sort((a, b) => Number(a) - Number(b))
  			.map(key => input[key]);
  	}

  	return input;
  }

  function removeHash(input) {
  	const hashStart = input.indexOf('#');
  	if (hashStart !== -1) {
  		input = input.slice(0, hashStart);
  	}

  	return input;
  }

  function getHash(url) {
  	let hash = '';
  	const hashStart = url.indexOf('#');
  	if (hashStart !== -1) {
  		hash = url.slice(hashStart);
  	}

  	return hash;
  }

  function extract(input) {
  	input = removeHash(input);
  	const queryStart = input.indexOf('?');
  	if (queryStart === -1) {
  		return '';
  	}

  	return input.slice(queryStart + 1);
  }

  function parseValue(value, options) {
  	if (options.parseNumbers && !Number.isNaN(Number(value)) && (typeof value === 'string' && value.trim() !== '')) {
  		value = Number(value);
  	} else if (options.parseBooleans && value !== null && (value.toLowerCase() === 'true' || value.toLowerCase() === 'false')) {
  		value = value.toLowerCase() === 'true';
  	}

  	return value;
  }

  function parse(query, options) {
  	options = Object.assign({
  		decode: true,
  		sort: true,
  		arrayFormat: 'none',
  		arrayFormatSeparator: ',',
  		parseNumbers: false,
  		parseBooleans: false
  	}, options);

  	validateArrayFormatSeparator(options.arrayFormatSeparator);

  	const formatter = parserForArrayFormat(options);

  	// Create an object with no prototype
  	const ret = Object.create(null);

  	if (typeof query !== 'string') {
  		return ret;
  	}

  	query = query.trim().replace(/^[?#&]/, '');

  	if (!query) {
  		return ret;
  	}

  	for (const param of query.split('&')) {
  		if (param === '') {
  			continue;
  		}

  		let [key, value] = splitOnFirst$1(options.decode ? param.replace(/\+/g, ' ') : param, '=');

  		// Missing `=` should be `null`:
  		// http://w3.org/TR/2012/WD-url-20120524/#collect-url-parameters
  		value = value === undefined ? null : ['comma', 'separator', 'bracket-separator'].includes(options.arrayFormat) ? value : decode(value, options);
  		formatter(decode(key, options), value, ret);
  	}

  	for (const key of Object.keys(ret)) {
  		const value = ret[key];
  		if (typeof value === 'object' && value !== null) {
  			for (const k of Object.keys(value)) {
  				value[k] = parseValue(value[k], options);
  			}
  		} else {
  			ret[key] = parseValue(value, options);
  		}
  	}

  	if (options.sort === false) {
  		return ret;
  	}

  	return (options.sort === true ? Object.keys(ret).sort() : Object.keys(ret).sort(options.sort)).reduce((result, key) => {
  		const value = ret[key];
  		if (Boolean(value) && typeof value === 'object' && !Array.isArray(value)) {
  			// Sort object keys, not values
  			result[key] = keysSorter(value);
  		} else {
  			result[key] = value;
  		}

  		return result;
  	}, Object.create(null));
  }

  exports.extract = extract;
  exports.parse = parse;

  exports.stringify = (object, options) => {
  	if (!object) {
  		return '';
  	}

  	options = Object.assign({
  		encode: true,
  		strict: true,
  		arrayFormat: 'none',
  		arrayFormatSeparator: ','
  	}, options);

  	validateArrayFormatSeparator(options.arrayFormatSeparator);

  	const shouldFilter = key => (
  		(options.skipNull && isNullOrUndefined(object[key])) ||
  		(options.skipEmptyString && object[key] === '')
  	);

  	const formatter = encoderForArrayFormat(options);

  	const objectCopy = {};

  	for (const key of Object.keys(object)) {
  		if (!shouldFilter(key)) {
  			objectCopy[key] = object[key];
  		}
  	}

  	const keys = Object.keys(objectCopy);

  	if (options.sort !== false) {
  		keys.sort(options.sort);
  	}

  	return keys.map(key => {
  		const value = object[key];

  		if (value === undefined) {
  			return '';
  		}

  		if (value === null) {
  			return encode(key, options);
  		}

  		if (Array.isArray(value)) {
  			if (value.length === 0 && options.arrayFormat === 'bracket-separator') {
  				return encode(key, options) + '[]';
  			}

  			return value
  				.reduce(formatter(key), [])
  				.join('&');
  		}

  		return encode(key, options) + '=' + encode(value, options);
  	}).filter(x => x.length > 0).join('&');
  };

  exports.parseUrl = (url, options) => {
  	options = Object.assign({
  		decode: true
  	}, options);

  	const [url_, hash] = splitOnFirst$1(url, '#');

  	return Object.assign(
  		{
  			url: url_.split('?')[0] || '',
  			query: parse(extract(url), options)
  		},
  		options && options.parseFragmentIdentifier && hash ? {fragmentIdentifier: decode(hash, options)} : {}
  	);
  };

  exports.stringifyUrl = (object, options) => {
  	options = Object.assign({
  		encode: true,
  		strict: true,
  		[encodeFragmentIdentifier]: true
  	}, options);

  	const url = removeHash(object.url).split('?')[0] || '';
  	const queryFromUrl = exports.extract(object.url);
  	const parsedQueryFromUrl = exports.parse(queryFromUrl, {sort: false});

  	const query = Object.assign(parsedQueryFromUrl, object.query);
  	let queryString = exports.stringify(query, options);
  	if (queryString) {
  		queryString = `?${queryString}`;
  	}

  	let hash = getHash(object.url);
  	if (object.fragmentIdentifier) {
  		hash = `#${options[encodeFragmentIdentifier] ? encode(object.fragmentIdentifier, options) : object.fragmentIdentifier}`;
  	}

  	return `${url}${queryString}${hash}`;
  };

  exports.pick = (input, filter, options) => {
  	options = Object.assign({
  		parseFragmentIdentifier: true,
  		[encodeFragmentIdentifier]: false
  	}, options);

  	const {url, query, fragmentIdentifier} = exports.parseUrl(input, options);
  	return exports.stringifyUrl({
  		url,
  		query: filterObject(query, filter),
  		fragmentIdentifier
  	}, options);
  };

  exports.exclude = (input, filter, options) => {
  	const exclusionFilter = Array.isArray(filter) ? key => !filter.includes(key) : (key, value) => !filter(key, value);

  	return exports.pick(input, exclusionFilter, options);
  };
  }(queryString));

  var toByteArray_1 = toByteArray;
  var revLookup = [];
  var Arr = typeof Uint8Array !== 'undefined' ? Uint8Array : Array;
  var code = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
  for (var i = 0, len = code.length; i < len; ++i) {
    revLookup[code.charCodeAt(i)] = i;
  }

  // Support decoding URL-safe base64 strings, as Node.js does.
  // See: https://en.wikipedia.org/wiki/Base64#URL_applications
  revLookup['-'.charCodeAt(0)] = 62;
  revLookup['_'.charCodeAt(0)] = 63;
  function getLens(b64) {
    var len = b64.length;
    if (len % 4 > 0) {
      throw new Error('Invalid string. Length must be a multiple of 4');
    }

    // Trim off extra bytes after placeholder bytes are found
    // See: https://github.com/beatgammit/base64-js/issues/42
    var validLen = b64.indexOf('=');
    if (validLen === -1) validLen = len;
    var placeHoldersLen = validLen === len ? 0 : 4 - validLen % 4;
    return [validLen, placeHoldersLen];
  }
  function _byteLength(b64, validLen, placeHoldersLen) {
    return (validLen + placeHoldersLen) * 3 / 4 - placeHoldersLen;
  }
  function toByteArray(b64) {
    var tmp;
    var lens = getLens(b64);
    var validLen = lens[0];
    var placeHoldersLen = lens[1];
    var arr = new Arr(_byteLength(b64, validLen, placeHoldersLen));
    var curByte = 0;

    // if there are placeholders, only get up to the last complete 4 chars
    var len = placeHoldersLen > 0 ? validLen - 4 : validLen;
    for (var i = 0; i < len; i += 4) {
      tmp = revLookup[b64.charCodeAt(i)] << 18 | revLookup[b64.charCodeAt(i + 1)] << 12 | revLookup[b64.charCodeAt(i + 2)] << 6 | revLookup[b64.charCodeAt(i + 3)];
      arr[curByte++] = tmp >> 16 & 0xFF;
      arr[curByte++] = tmp >> 8 & 0xFF;
      arr[curByte++] = tmp & 0xFF;
    }
    if (placeHoldersLen === 2) {
      tmp = revLookup[b64.charCodeAt(i)] << 2 | revLookup[b64.charCodeAt(i + 1)] >> 4;
      arr[curByte++] = tmp & 0xFF;
    }
    if (placeHoldersLen === 1) {
      tmp = revLookup[b64.charCodeAt(i)] << 10 | revLookup[b64.charCodeAt(i + 1)] << 4 | revLookup[b64.charCodeAt(i + 2)] >> 2;
      arr[curByte++] = tmp >> 8 & 0xFF;
      arr[curByte++] = tmp & 0xFF;
    }
    return arr;
  }

  function decode(string) {
    var padding = 4 - string.length % 4;
    if (padding === 1) {
      string += '=';
    } else if (padding === 2) {
      string += '==';
    }
    return new TextDecoder().decode(toByteArray_1(string));
  }

  var JWT_SKEW = 60; // in seconds.

  function parseJwtIssuer(jwt) {
    return parseJwtClaims(jwt)['iss'];
  }
  function parseJwtClaims(jwt) {
    if (null === jwt || '' === jwt) {
      throw 'Invalid JWT: must be neither null nor empty-string.';
    }
    var firstPeriodIndex = jwt.indexOf('.');
    var secondPeriodIndex = jwt.indexOf('.', firstPeriodIndex + 1);
    if (firstPeriodIndex < 0 || secondPeriodIndex <= firstPeriodIndex) {
      throw 'Invalid JWT: must contain 2 period (".") characters.';
    }
    var encodedClaims = jwt.substring(firstPeriodIndex + 1, secondPeriodIndex);
    if (null === encodedClaims || '' === encodedClaims) {
      throw 'Invalid JWT: encoded claims must be neither null nor empty-string.';
    }
    var claimsString = decode.call(window, encodedClaims);
    return JSON.parse(claimsString);
  }
  function isJwtExpired$1(jwtString, skew) {
    if (skew === undefined) {
      skew = JWT_SKEW;
    }
    var claims = parseJwtClaims(jwtString);
    var expires = 0;
    var now = Math.floor(Date.now() / 1000); // UTC timestamp now
    if (claims && claims.exp) {
      expires = claims.exp;
    }
    if (expires - skew < now) {
      return true;
    }
    return false;
  }
  EventDispatcher$1.register('jwt-skew-set', function (data) {
    JWT_SKEW = data.skew;
  });
  var jwtUtil = {
    parseJwtIssuer: parseJwtIssuer,
    parseJwtClaims: parseJwtClaims,
    isJwtExpired: isJwtExpired$1
  };

  function isJwtExpired(urlStr) {
    var jwtStr = _getJwt(urlStr);
    return jwtUtil.isJwtExpired(jwtStr);
  }
  function _getJwt(urlStr) {
    var query = queryString.parse(queryString.extract(urlStr));
    return query['jwt'];
  }
  function hasJwt(url) {
    var jwt = _getJwt(url);
    return jwt && _getJwt(url).length !== 0;
  }
  var urlUtils = {
    hasJwt: hasJwt,
    isJwtExpired: isJwtExpired
  };

  var jwtActions = {
    registerContentResolver: function registerContentResolver(data) {
      EventDispatcher$1.dispatch('content-resolver-register-by-extension', data);
    },
    requestRefreshUrl: function requestRefreshUrl(data) {
      if (!data.resolver) {
        throw Error('ACJS: No content resolver supplied');
      }
      var promise = data.resolver.call(null, Util.extend({
        classifier: 'json'
      }, data.extension));
      promise.fail(function (promiseData, error) {
        EventDispatcher$1.dispatch('jwt-url-refreshed-failed', {
          extension: data.extension,
          $container: data.$container,
          errorText: error.text
        });
      });
      promise.done(function (promiseData) {
        var newExtensionConfiguration = {};
        if (typeof promiseData === 'object') {
          newExtensionConfiguration = promiseData;
        } else if (typeof promiseData === 'string') {
          try {
            newExtensionConfiguration = JSON.parse(promiseData);
          } catch (e) {
            console.error('ACJS: invalid response from content resolver');
          }
        }
        data.extension.url = newExtensionConfiguration.url;
        Util.extend(data.extension.options, newExtensionConfiguration.options);
        EventDispatcher$1.dispatch('jwt-url-refreshed', {
          extension: data.extension,
          $container: data.$container,
          url: data.extension.url
        });
      });
      EventDispatcher$1.dispatch('jwt-url-refresh-request', {
        data: data
      });
    },
    setClockSkew: function setClockSkew(skew) {
      if (typeof skew === 'number') {
        EventDispatcher$1.dispatch('jwt-skew-set', {
          skew: skew
        });
      } else {
        console.error('ACJS: invalid JWT clock skew set');
      }
    }
  };

  var iframeUtils = {
    optionsToAttributes: function optionsToAttributes(options) {
      var sanitized = {};
      if (options && typeof options === 'object') {
        if (options.width) {
          sanitized.width = Util.stringToDimension(options.width);
        }
        if (options.height) {
          sanitized.height = Util.stringToDimension(options.height);
        }
        if (typeof options.sandbox === 'string') {
          var domElem = document.createElement('iframe');
          sanitized.sandbox = options.sandbox.split(' ').filter(function (value) {
            return Util.isSupported(domElem, 'sandbox', value, true);
          }).join(' ');
        }
      }
      return sanitized;
    }
  };

  var ExtensionConfigurationOptionsStore = /*#__PURE__*/function () {
    function ExtensionConfigurationOptionsStore() {
      this.store = {};
    }
    var _proto = ExtensionConfigurationOptionsStore.prototype;
    _proto.set = function set(obj, val) {
      if (val) {
        var toSet = {};
        toSet[obj] = val;
      } else {
        toSet = obj;
      }
      Util.extend(this.store, toSet);
    };
    _proto.get = function get(key) {
      if (key) {
        return this.store[key];
      }
      return Util.extend({}, this.store); //clone
    };
    return ExtensionConfigurationOptionsStore;
  }();
  var ExtensionConfigurationOptionsStore$1 = new ExtensionConfigurationOptionsStore();

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _typeof$1(o) {
    "@babel/helpers - typeof";

    return _typeof$1 = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) {
      return typeof o;
    } : function (o) {
      return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o;
    }, _typeof$1(o);
  }

  function _toPrimitive(input, hint) {
    if (_typeof$1(input) !== "object" || input === null) return input;
    var prim = input[Symbol.toPrimitive];
    if (prim !== undefined) {
      var res = prim.call(input, hint || "default");
      if (_typeof$1(res) !== "object") return res;
      throw new TypeError("@@toPrimitive must return a primitive value.");
    }
    return (hint === "string" ? String : Number)(input);
  }

  function _toPropertyKey(arg) {
    var key = _toPrimitive(arg, "string");
    return _typeof$1(key) === "symbol" ? key : String(key);
  }

  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor);
    }
  }
  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    Object.defineProperty(Constructor, "prototype", {
      writable: false
    });
    return Constructor;
  }

  function _defineProperty(obj, key, value) {
    key = _toPropertyKey(key);
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }
    return obj;
  }

  function _objectWithoutProperties(source, excluded) {
    if (source == null) return {};
    var target = _objectWithoutPropertiesLoose(source, excluded);
    var key, i;
    if (Object.getOwnPropertySymbols) {
      var sourceSymbolKeys = Object.getOwnPropertySymbols(source);
      for (i = 0; i < sourceSymbolKeys.length; i++) {
        key = sourceSymbolKeys[i];
        if (excluded.indexOf(key) >= 0) continue;
        if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue;
        target[key] = source[key];
      }
    }
    return target;
  }

  function _arrayWithHoles(arr) {
    if (Array.isArray(arr)) return arr;
  }

  function _iterableToArrayLimit(r, l) {
    var t = null == r ? null : "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"];
    if (null != t) {
      var e,
        n,
        i,
        u,
        a = [],
        f = !0,
        o = !1;
      try {
        if (i = (t = t.call(r)).next, 0 === l) {
          if (Object(t) !== t) return;
          f = !1;
        } else for (; !(f = (e = i.call(t)).done) && (a.push(e.value), a.length !== l); f = !0);
      } catch (r) {
        o = !0, n = r;
      } finally {
        try {
          if (!f && null != t["return"] && (u = t["return"](), Object(u) !== u)) return;
        } finally {
          if (o) throw n;
        }
      }
      return a;
    }
  }

  function _arrayLikeToArray$2(arr, len) {
    if (len == null || len > arr.length) len = arr.length;
    for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i];
    return arr2;
  }

  function _unsupportedIterableToArray$2(o, minLen) {
    if (!o) return;
    if (typeof o === "string") return _arrayLikeToArray$2(o, minLen);
    var n = Object.prototype.toString.call(o).slice(8, -1);
    if (n === "Object" && o.constructor) n = o.constructor.name;
    if (n === "Map" || n === "Set") return Array.from(o);
    if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray$2(o, minLen);
  }

  function _nonIterableRest() {
    throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
  }

  function _slicedToArray(arr, i) {
    return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray$2(arr, i) || _nonIterableRest();
  }

  function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {
    try {
      var info = gen[key](arg);
      var value = info.value;
    } catch (error) {
      reject(error);
      return;
    }
    if (info.done) {
      resolve(value);
    } else {
      Promise.resolve(value).then(_next, _throw);
    }
  }
  function _asyncToGenerator(fn) {
    return function () {
      var self = this,
        args = arguments;
      return new Promise(function (resolve, reject) {
        var gen = fn.apply(self, args);
        function _next(value) {
          asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);
        }
        function _throw(err) {
          asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);
        }
        _next(undefined);
      });
    };
  }

  var regeneratorRuntime$1 = {exports: {}};

  var _typeof = {exports: {}};

  (function (module) {
  function _typeof(o) {
    "@babel/helpers - typeof";

    return module.exports = _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) {
      return typeof o;
    } : function (o) {
      return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o;
    }, module.exports.__esModule = true, module.exports["default"] = module.exports, _typeof(o);
  }
  module.exports = _typeof, module.exports.__esModule = true, module.exports["default"] = module.exports;
  }(_typeof));

  (function (module) {
  var _typeof$1 = _typeof.exports["default"];
  function _regeneratorRuntime() {

    /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */
    module.exports = _regeneratorRuntime = function _regeneratorRuntime() {
      return e;
    }, module.exports.__esModule = true, module.exports["default"] = module.exports;
    var t,
      e = {},
      r = Object.prototype,
      n = r.hasOwnProperty,
      o = Object.defineProperty || function (t, e, r) {
        t[e] = r.value;
      },
      i = "function" == typeof Symbol ? Symbol : {},
      a = i.iterator || "@@iterator",
      c = i.asyncIterator || "@@asyncIterator",
      u = i.toStringTag || "@@toStringTag";
    function define(t, e, r) {
      return Object.defineProperty(t, e, {
        value: r,
        enumerable: !0,
        configurable: !0,
        writable: !0
      }), t[e];
    }
    try {
      define({}, "");
    } catch (t) {
      define = function define(t, e, r) {
        return t[e] = r;
      };
    }
    function wrap(t, e, r, n) {
      var i = e && e.prototype instanceof Generator ? e : Generator,
        a = Object.create(i.prototype),
        c = new Context(n || []);
      return o(a, "_invoke", {
        value: makeInvokeMethod(t, r, c)
      }), a;
    }
    function tryCatch(t, e, r) {
      try {
        return {
          type: "normal",
          arg: t.call(e, r)
        };
      } catch (t) {
        return {
          type: "throw",
          arg: t
        };
      }
    }
    e.wrap = wrap;
    var h = "suspendedStart",
      l = "suspendedYield",
      f = "executing",
      s = "completed",
      y = {};
    function Generator() {}
    function GeneratorFunction() {}
    function GeneratorFunctionPrototype() {}
    var p = {};
    define(p, a, function () {
      return this;
    });
    var d = Object.getPrototypeOf,
      v = d && d(d(values([])));
    v && v !== r && n.call(v, a) && (p = v);
    var g = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(p);
    function defineIteratorMethods(t) {
      ["next", "throw", "return"].forEach(function (e) {
        define(t, e, function (t) {
          return this._invoke(e, t);
        });
      });
    }
    function AsyncIterator(t, e) {
      function invoke(r, o, i, a) {
        var c = tryCatch(t[r], t, o);
        if ("throw" !== c.type) {
          var u = c.arg,
            h = u.value;
          return h && "object" == _typeof$1(h) && n.call(h, "__await") ? e.resolve(h.__await).then(function (t) {
            invoke("next", t, i, a);
          }, function (t) {
            invoke("throw", t, i, a);
          }) : e.resolve(h).then(function (t) {
            u.value = t, i(u);
          }, function (t) {
            return invoke("throw", t, i, a);
          });
        }
        a(c.arg);
      }
      var r;
      o(this, "_invoke", {
        value: function value(t, n) {
          function callInvokeWithMethodAndArg() {
            return new e(function (e, r) {
              invoke(t, n, e, r);
            });
          }
          return r = r ? r.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg();
        }
      });
    }
    function makeInvokeMethod(e, r, n) {
      var o = h;
      return function (i, a) {
        if (o === f) throw new Error("Generator is already running");
        if (o === s) {
          if ("throw" === i) throw a;
          return {
            value: t,
            done: !0
          };
        }
        for (n.method = i, n.arg = a;;) {
          var c = n.delegate;
          if (c) {
            var u = maybeInvokeDelegate(c, n);
            if (u) {
              if (u === y) continue;
              return u;
            }
          }
          if ("next" === n.method) n.sent = n._sent = n.arg;else if ("throw" === n.method) {
            if (o === h) throw o = s, n.arg;
            n.dispatchException(n.arg);
          } else "return" === n.method && n.abrupt("return", n.arg);
          o = f;
          var p = tryCatch(e, r, n);
          if ("normal" === p.type) {
            if (o = n.done ? s : l, p.arg === y) continue;
            return {
              value: p.arg,
              done: n.done
            };
          }
          "throw" === p.type && (o = s, n.method = "throw", n.arg = p.arg);
        }
      };
    }
    function maybeInvokeDelegate(e, r) {
      var n = r.method,
        o = e.iterator[n];
      if (o === t) return r.delegate = null, "throw" === n && e.iterator["return"] && (r.method = "return", r.arg = t, maybeInvokeDelegate(e, r), "throw" === r.method) || "return" !== n && (r.method = "throw", r.arg = new TypeError("The iterator does not provide a '" + n + "' method")), y;
      var i = tryCatch(o, e.iterator, r.arg);
      if ("throw" === i.type) return r.method = "throw", r.arg = i.arg, r.delegate = null, y;
      var a = i.arg;
      return a ? a.done ? (r[e.resultName] = a.value, r.next = e.nextLoc, "return" !== r.method && (r.method = "next", r.arg = t), r.delegate = null, y) : a : (r.method = "throw", r.arg = new TypeError("iterator result is not an object"), r.delegate = null, y);
    }
    function pushTryEntry(t) {
      var e = {
        tryLoc: t[0]
      };
      1 in t && (e.catchLoc = t[1]), 2 in t && (e.finallyLoc = t[2], e.afterLoc = t[3]), this.tryEntries.push(e);
    }
    function resetTryEntry(t) {
      var e = t.completion || {};
      e.type = "normal", delete e.arg, t.completion = e;
    }
    function Context(t) {
      this.tryEntries = [{
        tryLoc: "root"
      }], t.forEach(pushTryEntry, this), this.reset(!0);
    }
    function values(e) {
      if (e || "" === e) {
        var r = e[a];
        if (r) return r.call(e);
        if ("function" == typeof e.next) return e;
        if (!isNaN(e.length)) {
          var o = -1,
            i = function next() {
              for (; ++o < e.length;) if (n.call(e, o)) return next.value = e[o], next.done = !1, next;
              return next.value = t, next.done = !0, next;
            };
          return i.next = i;
        }
      }
      throw new TypeError(_typeof$1(e) + " is not iterable");
    }
    return GeneratorFunction.prototype = GeneratorFunctionPrototype, o(g, "constructor", {
      value: GeneratorFunctionPrototype,
      configurable: !0
    }), o(GeneratorFunctionPrototype, "constructor", {
      value: GeneratorFunction,
      configurable: !0
    }), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, u, "GeneratorFunction"), e.isGeneratorFunction = function (t) {
      var e = "function" == typeof t && t.constructor;
      return !!e && (e === GeneratorFunction || "GeneratorFunction" === (e.displayName || e.name));
    }, e.mark = function (t) {
      return Object.setPrototypeOf ? Object.setPrototypeOf(t, GeneratorFunctionPrototype) : (t.__proto__ = GeneratorFunctionPrototype, define(t, u, "GeneratorFunction")), t.prototype = Object.create(g), t;
    }, e.awrap = function (t) {
      return {
        __await: t
      };
    }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, c, function () {
      return this;
    }), e.AsyncIterator = AsyncIterator, e.async = function (t, r, n, o, i) {
      void 0 === i && (i = Promise);
      var a = new AsyncIterator(wrap(t, r, n, o), i);
      return e.isGeneratorFunction(r) ? a : a.next().then(function (t) {
        return t.done ? t.value : a.next();
      });
    }, defineIteratorMethods(g), define(g, u, "Generator"), define(g, a, function () {
      return this;
    }), define(g, "toString", function () {
      return "[object Generator]";
    }), e.keys = function (t) {
      var e = Object(t),
        r = [];
      for (var n in e) r.push(n);
      return r.reverse(), function next() {
        for (; r.length;) {
          var t = r.pop();
          if (t in e) return next.value = t, next.done = !1, next;
        }
        return next.done = !0, next;
      };
    }, e.values = values, Context.prototype = {
      constructor: Context,
      reset: function reset(e) {
        if (this.prev = 0, this.next = 0, this.sent = this._sent = t, this.done = !1, this.delegate = null, this.method = "next", this.arg = t, this.tryEntries.forEach(resetTryEntry), !e) for (var r in this) "t" === r.charAt(0) && n.call(this, r) && !isNaN(+r.slice(1)) && (this[r] = t);
      },
      stop: function stop() {
        this.done = !0;
        var t = this.tryEntries[0].completion;
        if ("throw" === t.type) throw t.arg;
        return this.rval;
      },
      dispatchException: function dispatchException(e) {
        if (this.done) throw e;
        var r = this;
        function handle(n, o) {
          return a.type = "throw", a.arg = e, r.next = n, o && (r.method = "next", r.arg = t), !!o;
        }
        for (var o = this.tryEntries.length - 1; o >= 0; --o) {
          var i = this.tryEntries[o],
            a = i.completion;
          if ("root" === i.tryLoc) return handle("end");
          if (i.tryLoc <= this.prev) {
            var c = n.call(i, "catchLoc"),
              u = n.call(i, "finallyLoc");
            if (c && u) {
              if (this.prev < i.catchLoc) return handle(i.catchLoc, !0);
              if (this.prev < i.finallyLoc) return handle(i.finallyLoc);
            } else if (c) {
              if (this.prev < i.catchLoc) return handle(i.catchLoc, !0);
            } else {
              if (!u) throw new Error("try statement without catch or finally");
              if (this.prev < i.finallyLoc) return handle(i.finallyLoc);
            }
          }
        }
      },
      abrupt: function abrupt(t, e) {
        for (var r = this.tryEntries.length - 1; r >= 0; --r) {
          var o = this.tryEntries[r];
          if (o.tryLoc <= this.prev && n.call(o, "finallyLoc") && this.prev < o.finallyLoc) {
            var i = o;
            break;
          }
        }
        i && ("break" === t || "continue" === t) && i.tryLoc <= e && e <= i.finallyLoc && (i = null);
        var a = i ? i.completion : {};
        return a.type = t, a.arg = e, i ? (this.method = "next", this.next = i.finallyLoc, y) : this.complete(a);
      },
      complete: function complete(t, e) {
        if ("throw" === t.type) throw t.arg;
        return "break" === t.type || "continue" === t.type ? this.next = t.arg : "return" === t.type ? (this.rval = this.arg = t.arg, this.method = "return", this.next = "end") : "normal" === t.type && e && (this.next = e), y;
      },
      finish: function finish(t) {
        for (var e = this.tryEntries.length - 1; e >= 0; --e) {
          var r = this.tryEntries[e];
          if (r.finallyLoc === t) return this.complete(r.completion, r.afterLoc), resetTryEntry(r), y;
        }
      },
      "catch": function _catch(t) {
        for (var e = this.tryEntries.length - 1; e >= 0; --e) {
          var r = this.tryEntries[e];
          if (r.tryLoc === t) {
            var n = r.completion;
            if ("throw" === n.type) {
              var o = n.arg;
              resetTryEntry(r);
            }
            return o;
          }
        }
        throw new Error("illegal catch attempt");
      },
      delegateYield: function delegateYield(e, r, n) {
        return this.delegate = {
          iterator: values(e),
          resultName: r,
          nextLoc: n
        }, "next" === this.method && (this.arg = t), y;
      }
    }, e;
  }
  module.exports = _regeneratorRuntime, module.exports.__esModule = true, module.exports["default"] = module.exports;
  }(regeneratorRuntime$1));

  // TODO(Babel 8): Remove this file.

  var runtime = regeneratorRuntime$1.exports();
  var regenerator = runtime;

  // Copied from https://github.com/facebook/regenerator/blob/main/packages/runtime/runtime.js#L736=
  try {
    regeneratorRuntime = runtime;
  } catch (accidentalStrictMode) {
    if (typeof globalThis === "object") {
      globalThis.regeneratorRuntime = runtime;
    } else {
      Function("r", "regeneratorRuntime = r")(runtime);
    }
  }

  var eventemitter2 = {exports: {}};

  /*!
   * EventEmitter2
   * https://github.com/hij1nx/EventEmitter2
   *
   * Copyright (c) 2013 hij1nx
   * Licensed under the MIT license.
   */

  (function (module, exports) {
  !function (undefined$1) {
    var isArray = Array.isArray ? Array.isArray : function _isArray(obj) {
      return Object.prototype.toString.call(obj) === "[object Array]";
    };
    var defaultMaxListeners = 10;
    function init() {
      this._events = {};
      if (this._conf) {
        configure.call(this, this._conf);
      }
    }
    function configure(conf) {
      if (conf) {
        this._conf = conf;
        conf.delimiter && (this.delimiter = conf.delimiter);
        this._maxListeners = conf.maxListeners !== undefined$1 ? conf.maxListeners : defaultMaxListeners;
        conf.wildcard && (this.wildcard = conf.wildcard);
        conf.newListener && (this.newListener = conf.newListener);
        conf.verboseMemoryLeak && (this.verboseMemoryLeak = conf.verboseMemoryLeak);
        if (this.wildcard) {
          this.listenerTree = {};
        }
      } else {
        this._maxListeners = defaultMaxListeners;
      }
    }
    function logPossibleMemoryLeak(count, eventName) {
      var errorMsg = '(node) warning: possible EventEmitter memory ' + 'leak detected. ' + count + ' listeners added. ' + 'Use emitter.setMaxListeners() to increase limit.';
      if (this.verboseMemoryLeak) {
        errorMsg += ' Event name: ' + eventName + '.';
      }
      if (typeof process !== 'undefined' && process.emitWarning) {
        var e = new Error(errorMsg);
        e.name = 'MaxListenersExceededWarning';
        e.emitter = this;
        e.count = count;
        process.emitWarning(e);
      } else {
        console.error(errorMsg);
        if (console.trace) {
          console.trace();
        }
      }
    }
    function EventEmitter(conf) {
      this._events = {};
      this.newListener = false;
      this.verboseMemoryLeak = false;
      configure.call(this, conf);
    }
    EventEmitter.EventEmitter2 = EventEmitter; // backwards compatibility for exporting EventEmitter property

    //
    // Attention, function return type now is array, always !
    // It has zero elements if no any matches found and one or more
    // elements (leafs) if there are matches
    //
    function searchListenerTree(handlers, type, tree, i) {
      if (!tree) {
        return [];
      }
      var listeners = [],
        leaf,
        len,
        branch,
        xTree,
        xxTree,
        isolatedBranch,
        endReached,
        typeLength = type.length,
        currentType = type[i],
        nextType = type[i + 1];
      if (i === typeLength && tree._listeners) {
        //
        // If at the end of the event(s) list and the tree has listeners
        // invoke those listeners.
        //
        if (typeof tree._listeners === 'function') {
          handlers && handlers.push(tree._listeners);
          return [tree];
        } else {
          for (leaf = 0, len = tree._listeners.length; leaf < len; leaf++) {
            handlers && handlers.push(tree._listeners[leaf]);
          }
          return [tree];
        }
      }
      if (currentType === '*' || currentType === '**' || tree[currentType]) {
        //
        // If the event emitted is '*' at this part
        // or there is a concrete match at this patch
        //
        if (currentType === '*') {
          for (branch in tree) {
            if (branch !== '_listeners' && tree.hasOwnProperty(branch)) {
              listeners = listeners.concat(searchListenerTree(handlers, type, tree[branch], i + 1));
            }
          }
          return listeners;
        } else if (currentType === '**') {
          endReached = i + 1 === typeLength || i + 2 === typeLength && nextType === '*';
          if (endReached && tree._listeners) {
            // The next element has a _listeners, add it to the handlers.
            listeners = listeners.concat(searchListenerTree(handlers, type, tree, typeLength));
          }
          for (branch in tree) {
            if (branch !== '_listeners' && tree.hasOwnProperty(branch)) {
              if (branch === '*' || branch === '**') {
                if (tree[branch]._listeners && !endReached) {
                  listeners = listeners.concat(searchListenerTree(handlers, type, tree[branch], typeLength));
                }
                listeners = listeners.concat(searchListenerTree(handlers, type, tree[branch], i));
              } else if (branch === nextType) {
                listeners = listeners.concat(searchListenerTree(handlers, type, tree[branch], i + 2));
              } else {
                // No match on this one, shift into the tree but not in the type array.
                listeners = listeners.concat(searchListenerTree(handlers, type, tree[branch], i));
              }
            }
          }
          return listeners;
        }
        listeners = listeners.concat(searchListenerTree(handlers, type, tree[currentType], i + 1));
      }
      xTree = tree['*'];
      if (xTree) {
        //
        // If the listener tree will allow any match for this part,
        // then recursively explore all branches of the tree
        //
        searchListenerTree(handlers, type, xTree, i + 1);
      }
      xxTree = tree['**'];
      if (xxTree) {
        if (i < typeLength) {
          if (xxTree._listeners) {
            // If we have a listener on a '**', it will catch all, so add its handler.
            searchListenerTree(handlers, type, xxTree, typeLength);
          }

          // Build arrays of matching next branches and others.
          for (branch in xxTree) {
            if (branch !== '_listeners' && xxTree.hasOwnProperty(branch)) {
              if (branch === nextType) {
                // We know the next element will match, so jump twice.
                searchListenerTree(handlers, type, xxTree[branch], i + 2);
              } else if (branch === currentType) {
                // Current node matches, move into the tree.
                searchListenerTree(handlers, type, xxTree[branch], i + 1);
              } else {
                isolatedBranch = {};
                isolatedBranch[branch] = xxTree[branch];
                searchListenerTree(handlers, type, {
                  '**': isolatedBranch
                }, i + 1);
              }
            }
          }
        } else if (xxTree._listeners) {
          // We have reached the end and still on a '**'
          searchListenerTree(handlers, type, xxTree, typeLength);
        } else if (xxTree['*'] && xxTree['*']._listeners) {
          searchListenerTree(handlers, type, xxTree['*'], typeLength);
        }
      }
      return listeners;
    }
    function growListenerTree(type, listener) {
      type = typeof type === 'string' ? type.split(this.delimiter) : type.slice();

      //
      // Looks for two consecutive '**', if so, don't add the event at all.
      //
      for (var i = 0, len = type.length; i + 1 < len; i++) {
        if (type[i] === '**' && type[i + 1] === '**') {
          return;
        }
      }
      var tree = this.listenerTree;
      var name = type.shift();
      while (name !== undefined$1) {
        if (!tree[name]) {
          tree[name] = {};
        }
        tree = tree[name];
        if (type.length === 0) {
          if (!tree._listeners) {
            tree._listeners = listener;
          } else {
            if (typeof tree._listeners === 'function') {
              tree._listeners = [tree._listeners];
            }
            tree._listeners.push(listener);
            if (!tree._listeners.warned && this._maxListeners > 0 && tree._listeners.length > this._maxListeners) {
              tree._listeners.warned = true;
              logPossibleMemoryLeak.call(this, tree._listeners.length, name);
            }
          }
          return true;
        }
        name = type.shift();
      }
      return true;
    }

    // By default EventEmitters will print a warning if more than
    // 10 listeners are added to it. This is a useful default which
    // helps finding memory leaks.
    //
    // Obviously not all Emitters should be limited to 10. This function allows
    // that to be increased. Set to zero for unlimited.

    EventEmitter.prototype.delimiter = '.';
    EventEmitter.prototype.setMaxListeners = function (n) {
      if (n !== undefined$1) {
        this._maxListeners = n;
        if (!this._conf) this._conf = {};
        this._conf.maxListeners = n;
      }
    };
    EventEmitter.prototype.event = '';
    EventEmitter.prototype.once = function (event, fn) {
      return this._once(event, fn, false);
    };
    EventEmitter.prototype.prependOnceListener = function (event, fn) {
      return this._once(event, fn, true);
    };
    EventEmitter.prototype._once = function (event, fn, prepend) {
      this._many(event, 1, fn, prepend);
      return this;
    };
    EventEmitter.prototype.many = function (event, ttl, fn) {
      return this._many(event, ttl, fn, false);
    };
    EventEmitter.prototype.prependMany = function (event, ttl, fn) {
      return this._many(event, ttl, fn, true);
    };
    EventEmitter.prototype._many = function (event, ttl, fn, prepend) {
      var self = this;
      if (typeof fn !== 'function') {
        throw new Error('many only accepts instances of Function');
      }
      function listener() {
        if (--ttl === 0) {
          self.off(event, listener);
        }
        return fn.apply(this, arguments);
      }
      listener._origin = fn;
      this._on(event, listener, prepend);
      return self;
    };
    EventEmitter.prototype.emit = function () {
      this._events || init.call(this);
      var type = arguments[0];
      if (type === 'newListener' && !this.newListener) {
        if (!this._events.newListener) {
          return false;
        }
      }
      var al = arguments.length;
      var args, l, i, j;
      var handler;
      if (this._all && this._all.length) {
        handler = this._all.slice();
        if (al > 3) {
          args = new Array(al);
          for (j = 0; j < al; j++) args[j] = arguments[j];
        }
        for (i = 0, l = handler.length; i < l; i++) {
          this.event = type;
          switch (al) {
            case 1:
              handler[i].call(this, type);
              break;
            case 2:
              handler[i].call(this, type, arguments[1]);
              break;
            case 3:
              handler[i].call(this, type, arguments[1], arguments[2]);
              break;
            default:
              handler[i].apply(this, args);
          }
        }
      }
      if (this.wildcard) {
        handler = [];
        var ns = typeof type === 'string' ? type.split(this.delimiter) : type.slice();
        searchListenerTree.call(this, handler, ns, this.listenerTree, 0);
      } else {
        handler = this._events[type];
        if (typeof handler === 'function') {
          this.event = type;
          switch (al) {
            case 1:
              handler.call(this);
              break;
            case 2:
              handler.call(this, arguments[1]);
              break;
            case 3:
              handler.call(this, arguments[1], arguments[2]);
              break;
            default:
              args = new Array(al - 1);
              for (j = 1; j < al; j++) args[j - 1] = arguments[j];
              handler.apply(this, args);
          }
          return true;
        } else if (handler) {
          // need to make copy of handlers because list can change in the middle
          // of emit call
          handler = handler.slice();
        }
      }
      if (handler && handler.length) {
        if (al > 3) {
          args = new Array(al - 1);
          for (j = 1; j < al; j++) args[j - 1] = arguments[j];
        }
        for (i = 0, l = handler.length; i < l; i++) {
          this.event = type;
          switch (al) {
            case 1:
              handler[i].call(this);
              break;
            case 2:
              handler[i].call(this, arguments[1]);
              break;
            case 3:
              handler[i].call(this, arguments[1], arguments[2]);
              break;
            default:
              handler[i].apply(this, args);
          }
        }
        return true;
      } else if (!this._all && type === 'error') {
        if (arguments[1] instanceof Error) {
          throw arguments[1]; // Unhandled 'error' event
        } else {
          throw new Error("Uncaught, unspecified 'error' event.");
        }
      }
      return !!this._all;
    };
    EventEmitter.prototype.emitAsync = function () {
      this._events || init.call(this);
      var type = arguments[0];
      if (type === 'newListener' && !this.newListener) {
        if (!this._events.newListener) {
          return Promise.resolve([false]);
        }
      }
      var promises = [];
      var al = arguments.length;
      var args, l, i, j;
      var handler;
      if (this._all) {
        if (al > 3) {
          args = new Array(al);
          for (j = 1; j < al; j++) args[j] = arguments[j];
        }
        for (i = 0, l = this._all.length; i < l; i++) {
          this.event = type;
          switch (al) {
            case 1:
              promises.push(this._all[i].call(this, type));
              break;
            case 2:
              promises.push(this._all[i].call(this, type, arguments[1]));
              break;
            case 3:
              promises.push(this._all[i].call(this, type, arguments[1], arguments[2]));
              break;
            default:
              promises.push(this._all[i].apply(this, args));
          }
        }
      }
      if (this.wildcard) {
        handler = [];
        var ns = typeof type === 'string' ? type.split(this.delimiter) : type.slice();
        searchListenerTree.call(this, handler, ns, this.listenerTree, 0);
      } else {
        handler = this._events[type];
      }
      if (typeof handler === 'function') {
        this.event = type;
        switch (al) {
          case 1:
            promises.push(handler.call(this));
            break;
          case 2:
            promises.push(handler.call(this, arguments[1]));
            break;
          case 3:
            promises.push(handler.call(this, arguments[1], arguments[2]));
            break;
          default:
            args = new Array(al - 1);
            for (j = 1; j < al; j++) args[j - 1] = arguments[j];
            promises.push(handler.apply(this, args));
        }
      } else if (handler && handler.length) {
        handler = handler.slice();
        if (al > 3) {
          args = new Array(al - 1);
          for (j = 1; j < al; j++) args[j - 1] = arguments[j];
        }
        for (i = 0, l = handler.length; i < l; i++) {
          this.event = type;
          switch (al) {
            case 1:
              promises.push(handler[i].call(this));
              break;
            case 2:
              promises.push(handler[i].call(this, arguments[1]));
              break;
            case 3:
              promises.push(handler[i].call(this, arguments[1], arguments[2]));
              break;
            default:
              promises.push(handler[i].apply(this, args));
          }
        }
      } else if (!this._all && type === 'error') {
        if (arguments[1] instanceof Error) {
          return Promise.reject(arguments[1]); // Unhandled 'error' event
        } else {
          return Promise.reject("Uncaught, unspecified 'error' event.");
        }
      }
      return Promise.all(promises);
    };
    EventEmitter.prototype.on = function (type, listener) {
      return this._on(type, listener, false);
    };
    EventEmitter.prototype.prependListener = function (type, listener) {
      return this._on(type, listener, true);
    };
    EventEmitter.prototype.onAny = function (fn) {
      return this._onAny(fn, false);
    };
    EventEmitter.prototype.prependAny = function (fn) {
      return this._onAny(fn, true);
    };
    EventEmitter.prototype.addListener = EventEmitter.prototype.on;
    EventEmitter.prototype._onAny = function (fn, prepend) {
      if (typeof fn !== 'function') {
        throw new Error('onAny only accepts instances of Function');
      }
      if (!this._all) {
        this._all = [];
      }

      // Add the function to the event listener collection.
      if (prepend) {
        this._all.unshift(fn);
      } else {
        this._all.push(fn);
      }
      return this;
    };
    EventEmitter.prototype._on = function (type, listener, prepend) {
      if (typeof type === 'function') {
        this._onAny(type, listener);
        return this;
      }
      if (typeof listener !== 'function') {
        throw new Error('on only accepts instances of Function');
      }
      this._events || init.call(this);

      // To avoid recursion in the case that type == "newListeners"! Before
      // adding it to the listeners, first emit "newListeners".
      this.emit('newListener', type, listener);
      if (this.wildcard) {
        growListenerTree.call(this, type, listener);
        return this;
      }
      if (!this._events[type]) {
        // Optimize the case of one listener. Don't need the extra array object.
        this._events[type] = listener;
      } else {
        if (typeof this._events[type] === 'function') {
          // Change to array.
          this._events[type] = [this._events[type]];
        }

        // If we've already got an array, just add
        if (prepend) {
          this._events[type].unshift(listener);
        } else {
          this._events[type].push(listener);
        }

        // Check for listener leak
        if (!this._events[type].warned && this._maxListeners > 0 && this._events[type].length > this._maxListeners) {
          this._events[type].warned = true;
          logPossibleMemoryLeak.call(this, this._events[type].length, type);
        }
      }
      return this;
    };
    EventEmitter.prototype.off = function (type, listener) {
      if (typeof listener !== 'function') {
        throw new Error('removeListener only takes instances of Function');
      }
      var handlers,
        leafs = [];
      if (this.wildcard) {
        var ns = typeof type === 'string' ? type.split(this.delimiter) : type.slice();
        leafs = searchListenerTree.call(this, null, ns, this.listenerTree, 0);
      } else {
        // does not use listeners(), so no side effect of creating _events[type]
        if (!this._events[type]) return this;
        handlers = this._events[type];
        leafs.push({
          _listeners: handlers
        });
      }
      for (var iLeaf = 0; iLeaf < leafs.length; iLeaf++) {
        var leaf = leafs[iLeaf];
        handlers = leaf._listeners;
        if (isArray(handlers)) {
          var position = -1;
          for (var i = 0, length = handlers.length; i < length; i++) {
            if (handlers[i] === listener || handlers[i].listener && handlers[i].listener === listener || handlers[i]._origin && handlers[i]._origin === listener) {
              position = i;
              break;
            }
          }
          if (position < 0) {
            continue;
          }
          if (this.wildcard) {
            leaf._listeners.splice(position, 1);
          } else {
            this._events[type].splice(position, 1);
          }
          if (handlers.length === 0) {
            if (this.wildcard) {
              delete leaf._listeners;
            } else {
              delete this._events[type];
            }
          }
          this.emit("removeListener", type, listener);
          return this;
        } else if (handlers === listener || handlers.listener && handlers.listener === listener || handlers._origin && handlers._origin === listener) {
          if (this.wildcard) {
            delete leaf._listeners;
          } else {
            delete this._events[type];
          }
          this.emit("removeListener", type, listener);
        }
      }
      function recursivelyGarbageCollect(root) {
        if (root === undefined$1) {
          return;
        }
        var keys = Object.keys(root);
        for (var i in keys) {
          var key = keys[i];
          var obj = root[key];
          if (obj instanceof Function || typeof obj !== "object" || obj === null) continue;
          if (Object.keys(obj).length > 0) {
            recursivelyGarbageCollect(root[key]);
          }
          if (Object.keys(obj).length === 0) {
            delete root[key];
          }
        }
      }
      recursivelyGarbageCollect(this.listenerTree);
      return this;
    };
    EventEmitter.prototype.offAny = function (fn) {
      var i = 0,
        l = 0,
        fns;
      if (fn && this._all && this._all.length > 0) {
        fns = this._all;
        for (i = 0, l = fns.length; i < l; i++) {
          if (fn === fns[i]) {
            fns.splice(i, 1);
            this.emit("removeListenerAny", fn);
            return this;
          }
        }
      } else {
        fns = this._all;
        for (i = 0, l = fns.length; i < l; i++) this.emit("removeListenerAny", fns[i]);
        this._all = [];
      }
      return this;
    };
    EventEmitter.prototype.removeListener = EventEmitter.prototype.off;
    EventEmitter.prototype.removeAllListeners = function (type) {
      if (arguments.length === 0) {
        !this._events || init.call(this);
        return this;
      }
      if (this.wildcard) {
        var ns = typeof type === 'string' ? type.split(this.delimiter) : type.slice();
        var leafs = searchListenerTree.call(this, null, ns, this.listenerTree, 0);
        for (var iLeaf = 0; iLeaf < leafs.length; iLeaf++) {
          var leaf = leafs[iLeaf];
          leaf._listeners = null;
        }
      } else if (this._events) {
        this._events[type] = null;
      }
      return this;
    };
    EventEmitter.prototype.listeners = function (type) {
      if (this.wildcard) {
        var handlers = [];
        var ns = typeof type === 'string' ? type.split(this.delimiter) : type.slice();
        searchListenerTree.call(this, handlers, ns, this.listenerTree, 0);
        return handlers;
      }
      this._events || init.call(this);
      if (!this._events[type]) this._events[type] = [];
      if (!isArray(this._events[type])) {
        this._events[type] = [this._events[type]];
      }
      return this._events[type];
    };
    EventEmitter.prototype.eventNames = function () {
      return Object.keys(this._events);
    };
    EventEmitter.prototype.listenerCount = function (type) {
      return this.listeners(type).length;
    };
    EventEmitter.prototype.listenersAny = function () {
      if (this._all) {
        return this._all;
      } else {
        return [];
      }
    };
    if (typeof undefined$1 === 'function' && undefined$1.amd) {
      // AMD. Register as an anonymous module.
      undefined$1(function () {
        return EventEmitter;
      });
    } else {
      // CommonJS
      module.exports = EventEmitter;
    }
  }();
  }(eventemitter2));

  function ownKeys$6(e, r) {
    var t = Object.keys(e);
    if (Object.getOwnPropertySymbols) {
      var o = Object.getOwnPropertySymbols(e);
      r && (o = o.filter(function (r) {
        return Object.getOwnPropertyDescriptor(e, r).enumerable;
      })), t.push.apply(t, o);
    }
    return t;
  }
  function _objectSpread$6(e) {
    for (var r = 1; r < arguments.length; r++) {
      var t = null != arguments[r] ? arguments[r] : {};
      r % 2 ? ownKeys$6(Object(t), !0).forEach(function (r) {
        _defineProperty(e, r, t[r]);
      }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys$6(Object(t)).forEach(function (r) {
        Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r));
      });
    }
    return e;
  }
  var ALL_FEATURE_VALUES = '@all-features';
  var Subscriptions = /*#__PURE__*/function () {
    function Subscriptions() {
      _classCallCheck(this, Subscriptions);
      _defineProperty(this, "eventToValue", new Map());
      this.emitter = new eventemitter2.exports.EventEmitter2();
    }
    return _createClass(Subscriptions, [{
      key: "onGateUpdated",
      value: function onGateUpdated(gateName, callback, checkGate, options) {
        var _this = this;
        var value = checkGate(gateName, _objectSpread$6(_objectSpread$6({}, options), {}, {
          fireGateExposure: false
        }));
        if (this.eventToValue.get(callback) === undefined) {
          this.eventToValue.set(callback, value);
        }
        var wrapCallback = function wrapCallback() {
          var value = checkGate(gateName, _objectSpread$6(_objectSpread$6({}, options), {}, {
            fireGateExposure: false
          }));
          var existingValue = _this.eventToValue.get(callback);
          if (existingValue !== value) {
            _this.eventToValue.set(callback, value);
            callback(value);
          }
        };
        this.emitter.on(gateName, wrapCallback);
        return function () {
          _this.emitter.off(gateName, wrapCallback);
        };
      }
    }, {
      key: "onExperimentValueUpdated",
      value: function onExperimentValueUpdated(experimentName, parameterName, defaultValue, callback, getExperimentValue, options) {
        var _this2 = this;
        var experimentEventName = "".concat(experimentName, ".").concat(parameterName);
        var value = getExperimentValue(experimentName, parameterName, defaultValue, _objectSpread$6(_objectSpread$6({}, options), {}, {
          fireExperimentExposure: false
        }));
        if (this.eventToValue.get(callback) === undefined) {
          this.eventToValue.set(callback, value);
        }
        var wrapCallback = function wrapCallback() {
          var value = getExperimentValue(experimentName, parameterName, defaultValue, _objectSpread$6(_objectSpread$6({}, options), {}, {
            fireExperimentExposure: false
          }));
          var existingValue = _this2.eventToValue.get(callback);
          if (existingValue !== value) {
            _this2.eventToValue.set(callback, value);
            callback(value);
          }
        };
        this.emitter.on(experimentEventName, wrapCallback);
        return function () {
          _this2.emitter.off(experimentEventName, wrapCallback);
        };
      }
    }, {
      key: "onAnyUpdated",
      value: function onAnyUpdated(callback) {
        var _this3 = this;
        this.emitter.on(ALL_FEATURE_VALUES, callback);
        return function () {
          _this3.emitter.off(ALL_FEATURE_VALUES, callback);
        };
      }
    }, {
      key: "anyUpdated",
      value: function anyUpdated() {
        var _this4 = this;
        this.emitter.emit(ALL_FEATURE_VALUES);
        this.emitter.eventNames().filter(function (name) {
          return name !== ALL_FEATURE_VALUES;
        }).forEach(function (event) {
          _this4.emitter.emit(event);
        });
      }
    }]);
  }();

  var FEDRAMP_MODERATE = 'fedramp-moderate';

  /**
   * Caution: Consider Alternatives Use of this function is not recommended as a long term solution, as it creates an assumption
   * there are no other isolated environments than just FedRAMP Moderate. You are encouraged to consider alternate solutions,
   * such as Statsig or environment configuration, that don’t require creating a hard dependency between your code features
   * and the FedRAMP environment.
   * See [go-is-fedramp](https://go.atlassian.com/is-fedramp)
   */
  function isFedRamp() {
    var _global$location;
    var global = globalThis;
    // MICROS_PERIMETER is already used by few products, so we need to keep it for backward compatibility
    var env = global.MICROS_PERIMETER || global.UNSAFE_ATL_CONTEXT_BOUNDARY;
    if (env) {
      return env === FEDRAMP_MODERATE;
    }
    var matches = (_global$location = global.location) === null || _global$location === void 0 || (_global$location = _global$location.hostname) === null || _global$location === void 0 ? void 0 : _global$location.match(/atlassian-us-gov-mod\.(com|net)|atlassian-us-gov\.(com|net)|atlassian-fex\.(com|net)|atlassian-stg-fedm\.(com|net)/);
    return matches ? matches.length > 0 : false;
  }

  // Reference: https://github.com/statsig-io/js-lite/blob/main/src/StatsigStore.ts

  var EvaluationReason = /*#__PURE__*/function (EvaluationReason) {
    EvaluationReason["Error"] = "Error";
    EvaluationReason["LocalOverride"] = "LocalOverride";
    EvaluationReason["Unrecognized"] = "Unrecognized";
    EvaluationReason["Uninitialized"] = "Uninitialized";
    EvaluationReason["NetworkNotModified"] = "NetworkNotModified";
    EvaluationReason["Network"] = "Network";
    EvaluationReason["InvalidBootstrap"] = "InvalidBootstrap";
    EvaluationReason["Bootstrap"] = "Bootstrap";
    EvaluationReason["Cache"] = "Cache";
    EvaluationReason["Unknown"] = "Unknown";
    return EvaluationReason;
  }({});

  // Reference: https://github.com/statsig-io/js-lite/blob/main/src/StatsigSDKOptions.ts

  /**
   * The identifiers for the user. Options are restricted to the set that is currently supported.
   */

  /**
   * Base client options. Does not include any options specific to providers
   * @interface BaseClientOptions
   * @property {FeatureGateEnvironment} environment - The environment for the client.
   * @property {string} targetApp - The target app for the client.
   * @property {AnalyticsWebClient} analyticsWebClient - The analytics web client.
   * @property {PerimeterType} perimeter - The perimeter for the client.
   */

  /**
   * The options for the client.
   * @interface ClientOptions
   * @extends {BaseClientOptions}
   * @property {string} apiKey - The API key for the client.
   * @property {fetchTimeoutMs} fetchTimeoutMs - The timeout for the fetch request in milliseconds. Defaults to 5000.
   * @property {boolean} useGatewayURL - Whether to use the gateway URL. Defaults to false.
   */

  /**
   * The custom attributes for the user.
   */

  var FeatureGateEnvironment = /*#__PURE__*/function (FeatureGateEnvironment) {
    FeatureGateEnvironment["Development"] = "development";
    FeatureGateEnvironment["Staging"] = "staging";
    FeatureGateEnvironment["Production"] = "production";
    return FeatureGateEnvironment;
  }({});

  // If adding new values here, please check FeatureGates.getDefaultPerimeter to make sure it still returns something sensible.
  var PerimeterType = /*#__PURE__*/function (PerimeterType) {
    PerimeterType["COMMERCIAL"] = "commercial";
    PerimeterType["FEDRAMP_MODERATE"] = "fedramp-moderate";
    return PerimeterType;
  }({});

  // Type magic to get the JSDoc comments from the Client class methods to appear on the static
  // methods in FeatureGates where the property name and function type are identical

  var _excluded$1 = ["api", "disableCurrentPageLogging", "loggingIntervalMillis", "loggingBufferMaxSize", "localMode", "eventLoggingApi", "eventLoggingApiForRetries", "disableLocalStorage", "ignoreWindowUndefined", "disableAllLogging", "initTimeoutMs", "disableNetworkKeepalive", "overrideStableID", "disableErrorLogging", "disableAutoMetricsLogging"];
  function ownKeys$5(e, r) {
    var t = Object.keys(e);
    if (Object.getOwnPropertySymbols) {
      var o = Object.getOwnPropertySymbols(e);
      r && (o = o.filter(function (r) {
        return Object.getOwnPropertyDescriptor(e, r).enumerable;
      })), t.push.apply(t, o);
    }
    return t;
  }
  function _objectSpread$5(e) {
    for (var r = 1; r < arguments.length; r++) {
      var t = null != arguments[r] ? arguments[r] : {};
      r % 2 ? ownKeys$5(Object(t), !0).forEach(function (r) {
        _defineProperty(e, r, t[r]);
      }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys$5(Object(t)).forEach(function (r) {
        Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r));
      });
    }
    return e;
  }
  var getOptionsWithDefaults = function getOptionsWithDefaults(options) {
    return _objectSpread$5({
      /**
       * If more federal PerimeterTypes are added in the future, this should be updated so
       * that isFedRamp() === true always returns the strictest perimeter.
       */
      perimeter: isFedRamp() ? PerimeterType.FEDRAMP_MODERATE : PerimeterType.COMMERCIAL
    }, options);
  };
  var shallowEquals = function shallowEquals(objectA, objectB) {
    if (!objectA && !objectB) {
      return true;
    }
    if (!objectA || !objectB) {
      return false;
    }
    var aEntries = Object.entries(objectA);
    var bEntries = Object.entries(objectB);
    if (aEntries.length !== bEntries.length) {
      return false;
    }
    var ascendingKeyOrder = function ascendingKeyOrder(_ref, _ref2) {
      var _ref3 = _slicedToArray(_ref, 1),
        key1 = _ref3[0];
      var _ref4 = _slicedToArray(_ref2, 1),
        key2 = _ref4[0];
      return key1.localeCompare(key2);
    };
    aEntries.sort(ascendingKeyOrder);
    bEntries.sort(ascendingKeyOrder);
    for (var i = 0; i < aEntries.length; i++) {
      var _aEntries$i = _slicedToArray(aEntries[i], 2),
        aValue = _aEntries$i[1];
      var _bEntries$i = _slicedToArray(bEntries[i], 2),
        bValue = _bEntries$i[1];
      if (aValue !== bValue) {
        return false;
      }
    }
    return true;
  };

  /**
   * This method creates an instance of StatsigUser from the given set of identifiers and
   * attributes.
   */
  var toStatsigUser = function toStatsigUser(identifiers, customAttributes, sdkKey) {
    var user = {
      customIDs: !(customAttributes !== null && customAttributes !== void 0 && customAttributes.stableID) && sdkKey ? _objectSpread$5({
        stableID: jsClient.StableID.get(sdkKey)
      }, identifiers) : identifiers,
      custom: customAttributes
    };
    if (identifiers.atlassianAccountId) {
      user.userID = identifiers.atlassianAccountId;
    }
    return user;
  };
  var migrateInitializationOptions = function migrateInitializationOptions(options) {
    var api = options.api,
      disableCurrentPageLogging = options.disableCurrentPageLogging,
      loggingIntervalMillis = options.loggingIntervalMillis,
      loggingBufferMaxSize = options.loggingBufferMaxSize,
      localMode = options.localMode,
      eventLoggingApi = options.eventLoggingApi,
      eventLoggingApiForRetries = options.eventLoggingApiForRetries,
      disableLocalStorage = options.disableLocalStorage,
      ignoreWindowUndefined = options.ignoreWindowUndefined,
      disableAllLogging = options.disableAllLogging;
      options.initTimeoutMs;
      options.disableNetworkKeepalive;
      options.overrideStableID;
      options.disableErrorLogging;
      options.disableAutoMetricsLogging;
      var rest = _objectWithoutProperties(options, _excluded$1);
    return _objectSpread$5(_objectSpread$5({}, rest), {}, {
      networkConfig: {
        api: api,
        logEventUrl: eventLoggingApi ? eventLoggingApi + 'rgstr' : undefined,
        logEventFallbackUrls: eventLoggingApiForRetries ? [eventLoggingApiForRetries] : undefined,
        preventAllNetworkTraffic: localMode || !ignoreWindowUndefined && typeof window === 'undefined'
      },
      includeCurrentPageUrlWithEvents: !disableCurrentPageLogging,
      loggingIntervalMs: loggingIntervalMillis,
      loggingBufferMaxSize: loggingBufferMaxSize,
      disableStorage: disableLocalStorage === undefined ? localMode : disableLocalStorage,
      disableLogging: disableAllLogging === undefined ? localMode : disableAllLogging
    });
  };
  var evaluationReasonMappings = Object.entries(EvaluationReason).map(function (_ref5) {
    var _ref6 = _slicedToArray(_ref5, 2),
      key = _ref6[0],
      value = _ref6[1];
    return [key.toLowerCase(), value];
  });
  var migrateEvaluationDetails = function migrateEvaluationDetails(details) {
    var _evaluationReasonMapp, _evaluationReasonMapp2, _details$receivedAt;
    var reasonLower = details.reason.toLowerCase();
    return {
      reason: (_evaluationReasonMapp = (_evaluationReasonMapp2 = evaluationReasonMappings.find(function (_ref7) {
        var _ref8 = _slicedToArray(_ref7, 1),
          key = _ref8[0];
        return reasonLower.includes(key);
      })) === null || _evaluationReasonMapp2 === void 0 ? void 0 : _evaluationReasonMapp2[1]) !== null && _evaluationReasonMapp !== void 0 ? _evaluationReasonMapp : EvaluationReason.Unknown,
      time: (_details$receivedAt = details.receivedAt) !== null && _details$receivedAt !== void 0 ? _details$receivedAt : Date.now()
    };
  };

  // Reference: https://github.com/statsig-io/js-lite/blob/main/src/DynamicConfig.ts
  var DynamicConfig = /*#__PURE__*/function () {
    function DynamicConfig(configName, configValue, ruleID, evaluationDetails) {
      var secondaryExposures = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : [];
      var allocatedExperimentName = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : '';
      var onDefaultValueFallback = arguments.length > 6 && arguments[6] !== undefined ? arguments[6] : null;
      _classCallCheck(this, DynamicConfig);
      this.value = configValue;
      this._name = configName;
      this._ruleID = ruleID;
      this._secondaryExposures = secondaryExposures;
      this._allocatedExperimentName = allocatedExperimentName;
      this._evaluationDetails = evaluationDetails;
      this._onDefaultValueFallback = onDefaultValueFallback;
    }
    return _createClass(DynamicConfig, [{
      key: "get",
      value: function get(key, defaultValue, typeGuard) {
        var _this$_onDefaultValue2;
        var val = this.getValue(key, defaultValue);
        if (val == null) {
          return defaultValue;
        }
        var expectedType = Array.isArray(defaultValue) ? 'array' : _typeof$1(defaultValue);
        var actualType = Array.isArray(val) ? 'array' : _typeof$1(val);
        if (typeGuard) {
          var _this$_onDefaultValue;
          if (typeGuard(val)) {
            this.fireExposure(key);
            return val;
          }
          (_this$_onDefaultValue = this._onDefaultValueFallback) === null || _this$_onDefaultValue === void 0 || _this$_onDefaultValue.call(this, this, key, expectedType, actualType);
          return defaultValue;
        }
        if (defaultValue == null || expectedType === actualType) {
          this.fireExposure(key);
          return val;
        }
        (_this$_onDefaultValue2 = this._onDefaultValueFallback) === null || _this$_onDefaultValue2 === void 0 || _this$_onDefaultValue2.call(this, this, key, expectedType, actualType);
        return defaultValue;
      }
    }, {
      key: "getValue",
      value: function getValue(key, defaultValue) {
        if (key == null) {
          return this.value;
        }
        if (defaultValue == null) {
          defaultValue = null;
        }
        if (this.value[key] == null) {
          return defaultValue;
        }
        this.fireExposure(key);
        return this.value[key];
      }
    }, {
      key: "fireExposure",
      value: function fireExposure(key) {
        // Call the wrapped experiment's get method to fire exposure
        if (this.experiment) {
          this.experiment.get(key);
        }
      }
    }], [{
      key: "fromExperiment",
      value: function fromExperiment(experiment) {
        var _experiment$__evaluat, _experiment$groupName;
        var config = new DynamicConfig(experiment.name, experiment.value, experiment.ruleID, migrateEvaluationDetails(experiment.details), (_experiment$__evaluat = experiment.__evaluation) === null || _experiment$__evaluat === void 0 ? void 0 : _experiment$__evaluat.secondary_exposures, (_experiment$groupName = experiment.groupName) !== null && _experiment$groupName !== void 0 ? _experiment$groupName : undefined);
        config.experiment = experiment;
        return config;
      }
    }]);
  }();

  // Reference: https://github.com/statsig-io/js-lite/blob/main/src/Layer.ts
  var Layer = /*#__PURE__*/function () {
    function Layer(name, layerValue, ruleID, evaluationDetails) {
      var logParameterFunction = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : null;
      var secondaryExposures = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : [];
      var undelegatedSecondaryExposures = arguments.length > 6 && arguments[6] !== undefined ? arguments[6] : [];
      var allocatedExperimentName = arguments.length > 7 && arguments[7] !== undefined ? arguments[7] : '';
      var explicitParameters = arguments.length > 8 && arguments[8] !== undefined ? arguments[8] : [];
      _classCallCheck(this, Layer);
      this._logParameterFunction = logParameterFunction;
      this._name = name;
      this._value = JSON.parse(JSON.stringify(layerValue !== null && layerValue !== void 0 ? layerValue : {}));
      this._ruleID = ruleID !== null && ruleID !== void 0 ? ruleID : '';
      this._evaluationDetails = evaluationDetails;
      this._secondaryExposures = secondaryExposures;
      this._undelegatedSecondaryExposures = undelegatedSecondaryExposures;
      this._allocatedExperimentName = allocatedExperimentName;
      this._explicitParameters = explicitParameters;
    }
    return _createClass(Layer, [{
      key: "get",
      value: function get(key, defaultValue, typeGuard) {
        var _this = this;
        var val = this._value[key];
        if (val == null) {
          return defaultValue;
        }
        var logAndReturn = function logAndReturn() {
          _this._logLayerParameterExposure(key);
          return val;
        };
        if (typeGuard) {
          return typeGuard(val) ? logAndReturn() : defaultValue;
        }
        if (defaultValue == null) {
          return logAndReturn();
        }
        if (_typeof$1(val) === _typeof$1(defaultValue) && Array.isArray(defaultValue) === Array.isArray(val)) {
          return logAndReturn();
        }
        return defaultValue;
      }
    }, {
      key: "getValue",
      value: function getValue(key, defaultValue) {
        // eslint-disable-next-line eqeqeq
        if (defaultValue == undefined) {
          defaultValue = null;
        }
        var val = this._value[key];
        if (val != null) {
          this._logLayerParameterExposure(key);
        }
        return val !== null && val !== void 0 ? val : defaultValue;
      }
    }, {
      key: "_logLayerParameterExposure",
      value: function _logLayerParameterExposure(parameterName) {
        var _this$_logParameterFu;
        (_this$_logParameterFu = this._logParameterFunction) === null || _this$_logParameterFu === void 0 || _this$_logParameterFu.call(this, this, parameterName);
      }
    }], [{
      key: "fromLayer",
      value: function fromLayer(layer) {
        var _layer$__evaluation, _layer$__evaluation2, _layer$__evaluation3, _layer$__evaluation4;
        var value = new Layer(layer.name, layer.__value, layer.ruleID, migrateEvaluationDetails(layer.details), function (_layer, parameterName) {
          return layer.get(parameterName);
        }, (_layer$__evaluation = layer.__evaluation) === null || _layer$__evaluation === void 0 ? void 0 : _layer$__evaluation.secondary_exposures, (_layer$__evaluation2 = layer.__evaluation) === null || _layer$__evaluation2 === void 0 ? void 0 : _layer$__evaluation2.undelegated_secondary_exposures, (_layer$__evaluation3 = layer.__evaluation) === null || _layer$__evaluation3 === void 0 ? void 0 : _layer$__evaluation3.allocated_experiment_name, (_layer$__evaluation4 = layer.__evaluation) === null || _layer$__evaluation4 === void 0 ? void 0 : _layer$__evaluation4.explicit_parameters);
        return value;
      }
    }]);
  }();

  /// <reference types="node" />
  var CLIENT_VERSION = "4.26.4";

  function _assertThisInitialized(self) {
    if (self === void 0) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }
    return self;
  }

  function _possibleConstructorReturn(self, call) {
    if (call && (_typeof$1(call) === "object" || typeof call === "function")) {
      return call;
    } else if (call !== void 0) {
      throw new TypeError("Derived constructors may only return object or undefined");
    }
    return _assertThisInitialized(self);
  }

  function _getPrototypeOf(o) {
    _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function _getPrototypeOf(o) {
      return o.__proto__ || Object.getPrototypeOf(o);
    };
    return _getPrototypeOf(o);
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function");
    }
    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        writable: true,
        configurable: true
      }
    });
    Object.defineProperty(subClass, "prototype", {
      writable: false
    });
    if (superClass) _setPrototypeOf(subClass, superClass);
  }

  function _isNativeFunction(fn) {
    try {
      return Function.toString.call(fn).indexOf("[native code]") !== -1;
    } catch (e) {
      return typeof fn === "function";
    }
  }

  function _wrapNativeSuper(Class) {
    var _cache = typeof Map === "function" ? new Map() : undefined;
    _wrapNativeSuper = function _wrapNativeSuper(Class) {
      if (Class === null || !_isNativeFunction(Class)) return Class;
      if (typeof Class !== "function") {
        throw new TypeError("Super expression must either be null or a function");
      }
      if (typeof _cache !== "undefined") {
        if (_cache.has(Class)) return _cache.get(Class);
        _cache.set(Class, Wrapper);
      }
      function Wrapper() {
        return _construct(Class, arguments, _getPrototypeOf(this).constructor);
      }
      Wrapper.prototype = Object.create(Class.prototype, {
        constructor: {
          value: Wrapper,
          enumerable: false,
          writable: true,
          configurable: true
        }
      });
      return _setPrototypeOf(Wrapper, Class);
    };
    return _wrapNativeSuper(Class);
  }

  function _callSuper$1(t, o, e) {
    return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$1() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e));
  }
  function _isNativeReflectConstruct$1() {
    try {
      var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {}));
    } catch (t) {}
    return (_isNativeReflectConstruct$1 = function _isNativeReflectConstruct() {
      return !!t;
    })();
  }
  var ResponseError = /*#__PURE__*/function (_Error) {
    function ResponseError(message) {
      _classCallCheck(this, ResponseError);
      return _callSuper$1(this, ResponseError, [message]);
    }
    _inherits(ResponseError, _Error);
    return _createClass(ResponseError);
  }(/*#__PURE__*/_wrapNativeSuper(Error));

  function ownKeys$4(e, r) {
    var t = Object.keys(e);
    if (Object.getOwnPropertySymbols) {
      var o = Object.getOwnPropertySymbols(e);
      r && (o = o.filter(function (r) {
        return Object.getOwnPropertyDescriptor(e, r).enumerable;
      })), t.push.apply(t, o);
    }
    return t;
  }
  function _objectSpread$4(e) {
    for (var r = 1; r < arguments.length; r++) {
      var t = null != arguments[r] ? arguments[r] : {};
      r % 2 ? ownKeys$4(Object(t), !0).forEach(function (r) {
        _defineProperty(e, r, t[r]);
      }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys$4(Object(t)).forEach(function (r) {
        Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r));
      });
    }
    return e;
  }
  var DEFAULT_REQUEST_TIMEOUT_MS = 5000;
  var PROD_BASE_URL = 'https://api.atlassian.com/flags';
  var STAGING_BASE_URL = 'https://api.stg.atlassian.com/flags';
  var DEV_BASE_URL = 'https://api.dev.atlassian.com/flags';
  var FEDM_STAGING_BASE_URL = 'https://api.stg.atlassian-us-gov-mod.com/flags';
  var FEDM_PROD_BASE_URL = 'https://api.atlassian-us-gov-mod.com/flags';
  var GATEWAY_BASE_URL = '/gateway/api/flags';
  var Fetcher = /*#__PURE__*/function () {
    function Fetcher() {
      _classCallCheck(this, Fetcher);
    }
    return _createClass(Fetcher, null, [{
      key: "fetchClientSdk",
      value: function () {
        var _fetchClientSdk = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee(fetcherOptions) {
          var targetApp, url;
          return regenerator.wrap(function _callee$(_context) {
            while (1) switch (_context.prev = _context.next) {
              case 0:
                targetApp = fetcherOptions.targetApp;
                url = "/api/v2/frontend/clientSdkKey/".concat(targetApp);
                _context.prev = 2;
                _context.next = 5;
                return this.fetchRequest(url, 'GET', fetcherOptions);
              case 5:
                return _context.abrupt("return", _context.sent);
              case 8:
                _context.prev = 8;
                _context.t0 = _context["catch"](2);
                if (!(_context.t0 instanceof Error)) {
                  _context.next = 12;
                  break;
                }
                throw _context.t0;
              case 12:
                throw Error('Failed to retrieve client sdk key');
              case 13:
              case "end":
                return _context.stop();
            }
          }, _callee, this, [[2, 8]]);
        }));
        function fetchClientSdk(_x) {
          return _fetchClientSdk.apply(this, arguments);
        }
        return fetchClientSdk;
      }()
    }, {
      key: "fetchExperimentValues",
      value: function () {
        var _fetchExperimentValues = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee2(fetcherOptions, identifiers, customAttributes) {
          var requestBody;
          return regenerator.wrap(function _callee2$(_context2) {
            while (1) switch (_context2.prev = _context2.next) {
              case 0:
                requestBody = {
                  identifiers: identifiers,
                  customAttributes: customAttributes,
                  targetApp: fetcherOptions.targetApp
                };
                _context2.prev = 1;
                _context2.next = 4;
                return this.fetchRequest('/api/v2/frontend/experimentValues', 'POST', fetcherOptions, requestBody);
              case 4:
                return _context2.abrupt("return", _context2.sent);
              case 7:
                _context2.prev = 7;
                _context2.t0 = _context2["catch"](1);
                if (!(_context2.t0 instanceof Error)) {
                  _context2.next = 11;
                  break;
                }
                throw _context2.t0;
              case 11:
                throw Error('Failed to retrieve experiment values');
              case 12:
              case "end":
                return _context2.stop();
            }
          }, _callee2, this, [[1, 7]]);
        }));
        function fetchExperimentValues(_x2, _x3, _x4) {
          return _fetchExperimentValues.apply(this, arguments);
        }
        return fetchExperimentValues;
      }()
    }, {
      key: "handleResponseError",
      value: function () {
        var _handleResponseError = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee3(response) {
          var body;
          return regenerator.wrap(function _callee3$(_context3) {
            while (1) switch (_context3.prev = _context3.next) {
              case 0:
                if (response.ok) {
                  _context3.next = 5;
                  break;
                }
                _context3.next = 3;
                return response.text();
              case 3:
                body = _context3.sent;
                throw new ResponseError("Non 2xx response status received, status: ".concat(response.status, ", body: ").concat(JSON.stringify(body)));
              case 5:
                if (!(response.status === 204)) {
                  _context3.next = 7;
                  break;
                }
                throw new ResponseError('Unexpected 204 response');
              case 7:
              case "end":
                return _context3.stop();
            }
          }, _callee3);
        }));
        function handleResponseError(_x5) {
          return _handleResponseError.apply(this, arguments);
        }
        return handleResponseError;
      }()
    }, {
      key: "extractResponseBody",
      value: function () {
        var _extractResponseBody = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee4(response) {
          var value;
          return regenerator.wrap(function _callee4$(_context4) {
            while (1) switch (_context4.prev = _context4.next) {
              case 0:
                _context4.next = 2;
                return response.text();
              case 2:
                value = _context4.sent;
                return _context4.abrupt("return", JSON.parse(value));
              case 4:
              case "end":
                return _context4.stop();
            }
          }, _callee4);
        }));
        function extractResponseBody(_x6) {
          return _extractResponseBody.apply(this, arguments);
        }
        return extractResponseBody;
      }()
    }, {
      key: "getBaseUrl",
      value: function getBaseUrl(serviceEnv) {
        var useGatewayUrl = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
        var perimeter = arguments.length > 2 ? arguments[2] : undefined;
        if (useGatewayUrl) {
          return GATEWAY_BASE_URL;
        }
        if (perimeter === PerimeterType.FEDRAMP_MODERATE) {
          switch (serviceEnv) {
            case FeatureGateEnvironment.Production:
              return FEDM_PROD_BASE_URL;
            case FeatureGateEnvironment.Staging:
              return FEDM_STAGING_BASE_URL;
            default:
              throw new Error("Invalid environment \"".concat(serviceEnv, "\" for \"").concat(perimeter, "\" perimeter"));
          }
        } else if (perimeter === PerimeterType.COMMERCIAL) {
          switch (serviceEnv) {
            case FeatureGateEnvironment.Development:
              return DEV_BASE_URL;
            case FeatureGateEnvironment.Staging:
              return STAGING_BASE_URL;
            default:
              return PROD_BASE_URL;
          }
        } else {
          throw new Error("Invalid perimeter \"".concat(perimeter, "\""));
        }
      }
    }, {
      key: "fetchRequest",
      value: function () {
        var _fetchRequest = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee5(path, method, fetcherOptions, body) {
          var baseUrl, fetchTimeout, abortSignal, abortController, response;
          return regenerator.wrap(function _callee5$(_context5) {
            while (1) switch (_context5.prev = _context5.next) {
              case 0:
                baseUrl = Fetcher.getBaseUrl(fetcherOptions.environment, fetcherOptions.useGatewayURL, fetcherOptions.perimeter);
                fetchTimeout = fetcherOptions.fetchTimeoutMs || DEFAULT_REQUEST_TIMEOUT_MS;
                if (AbortSignal.timeout) {
                  abortSignal = AbortSignal.timeout(fetchTimeout);
                } else if (AbortController) {
                  abortController = new AbortController();
                  abortSignal = abortController.signal;
                  setTimeout(function () {
                    return abortController.abort();
                  }, fetchTimeout);
                }
                _context5.next = 5;
                return fetch("".concat(baseUrl).concat(path), _objectSpread$4({
                  method: method,
                  headers: {
                    'Content-Type': 'application/json',
                    'X-Client-Name': 'feature-gate-js-client',
                    'X-Client-Version': CLIENT_VERSION,
                    'X-API-KEY': fetcherOptions.apiKey
                  },
                  signal: abortSignal
                }, body && {
                  body: JSON.stringify(body)
                }));
              case 5:
                response = _context5.sent;
                _context5.next = 8;
                return this.handleResponseError(response);
              case 8:
                _context5.next = 10;
                return this.extractResponseBody(response);
              case 10:
                return _context5.abrupt("return", _context5.sent);
              case 11:
              case "end":
                return _context5.stop();
            }
          }, _callee5, this);
        }));
        function fetchRequest(_x7, _x8, _x9, _x10) {
          return _fetchRequest.apply(this, arguments);
        }
        return fetchRequest;
      }()
    }]);
  }();

  function _superPropBase(object, property) {
    while (!Object.prototype.hasOwnProperty.call(object, property)) {
      object = _getPrototypeOf(object);
      if (object === null) break;
    }
    return object;
  }

  function _get() {
    if (typeof Reflect !== "undefined" && Reflect.get) {
      _get = Reflect.get.bind();
    } else {
      _get = function _get(target, property, receiver) {
        var base = _superPropBase(target, property);
        if (!base) return;
        var desc = Object.getOwnPropertyDescriptor(base, property);
        if (desc.get) {
          return desc.get.call(arguments.length < 3 ? target : receiver);
        }
        return desc.value;
      };
    }
    return _get.apply(this, arguments);
  }

  function ownKeys$3(e, r) {
    var t = Object.keys(e);
    if (Object.getOwnPropertySymbols) {
      var o = Object.getOwnPropertySymbols(e);
      r && (o = o.filter(function (r) {
        return Object.getOwnPropertyDescriptor(e, r).enumerable;
      })), t.push.apply(t, o);
    }
    return t;
  }
  function _objectSpread$3(e) {
    for (var r = 1; r < arguments.length; r++) {
      var t = null != arguments[r] ? arguments[r] : {};
      r % 2 ? ownKeys$3(Object(t), !0).forEach(function (r) {
        _defineProperty(e, r, t[r]);
      }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys$3(Object(t)).forEach(function (r) {
        Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r));
      });
    }
    return e;
  }
  function _callSuper(t, o, e) {
    return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e));
  }
  function _isNativeReflectConstruct() {
    try {
      var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {}));
    } catch (t) {}
    return (_isNativeReflectConstruct = function _isNativeReflectConstruct() {
      return !!t;
    })();
  }
  function _superPropGet(t, o, e, r) {
    var p = _get(_getPrototypeOf(1 & r ? t.prototype : t), o, e);
    return 2 & r && "function" == typeof p ? function (t) {
      return p.apply(e, t);
    } : p;
  }

  /**
   * Data adapter which only uses bootstrap data and will never fetch from network or cache.
   * We do this because we control the fetching of bootstrap data from FFS in Client.ts whereas the
   * default data adapter fetches from Statsig servers.
   */
  var NoFetchDataAdapter = /*#__PURE__*/function (_DataAdapterCore) {
    function NoFetchDataAdapter() {
      var _this;
      _classCallCheck(this, NoFetchDataAdapter);
      _this = _callSuper(this, NoFetchDataAdapter, ['NoFetchDataAdapter', 'nofetch']);
      _defineProperty(_this, "bootstrapResult", null);
      return _this;
    }

    /**
     * Make sure to call this **before** calling `initializeAsync` or `updateUserAsync` but
     * after the Statsig client has been created!
     */
    _inherits(NoFetchDataAdapter, _DataAdapterCore);
    return _createClass(NoFetchDataAdapter, [{
      key: "setBootstrapData",
      value: function setBootstrapData(data) {
        this.bootstrapResult = data ? {
          source: 'Bootstrap',
          data: JSON.stringify(data),
          receivedAt: Date.now(),
          stableID: jsClient.StableID.get(this._getSdkKey()),
          fullUserHash: null
        } : null;
      }
    }, {
      key: "prefetchData",
      value: function () {
        var _prefetchData = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee(_user, _options) {
          return regenerator.wrap(function _callee$(_context) {
            while (1) switch (_context.prev = _context.next) {
              case 0:
              case "end":
                return _context.stop();
            }
          }, _callee);
        }));
        function prefetchData(_x, _x2) {
          return _prefetchData.apply(this, arguments);
        }
        return prefetchData;
      }()
    }, {
      key: "getDataAsync",
      value: function () {
        var _getDataAsync = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee2(_current, user, _options) {
          return regenerator.wrap(function _callee2$(_context2) {
            while (1) switch (_context2.prev = _context2.next) {
              case 0:
                return _context2.abrupt("return", this.bootstrapResult && _objectSpread$3(_objectSpread$3({}, this.bootstrapResult), {}, {
                  fullUserHash: jsClient._getFullUserHash(user)
                }));
              case 1:
              case "end":
                return _context2.stop();
            }
          }, _callee2, this);
        }));
        function getDataAsync(_x3, _x4, _x5) {
          return _getDataAsync.apply(this, arguments);
        }
        return getDataAsync;
      }()
    }, {
      key: "getDataSync",
      value: function getDataSync(user) {
        return this.bootstrapResult && _objectSpread$3(_objectSpread$3({}, this.bootstrapResult), {}, {
          fullUserHash: jsClient._getFullUserHash(user)
        });
      }
    }, {
      key: "_fetchFromNetwork",
      value: function () {
        var _fetchFromNetwork2 = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee3(_current, _user, _options) {
          return regenerator.wrap(function _callee3$(_context3) {
            while (1) switch (_context3.prev = _context3.next) {
              case 0:
                return _context3.abrupt("return", null);
              case 1:
              case "end":
                return _context3.stop();
            }
          }, _callee3);
        }));
        function _fetchFromNetwork(_x6, _x7, _x8) {
          return _fetchFromNetwork2.apply(this, arguments);
        }
        return _fetchFromNetwork;
      }()
    }, {
      key: "_getCacheKey",
      value: function _getCacheKey(user) {
        // Same logic as default data adapter
        // https://github.com/statsig-io/js-client-monorepo/blob/main/packages/js-client/src/StatsigEvaluationsDataAdapter.ts
        var key = jsClient._getStorageKey(this._getSdkKey(), user);
        return "".concat(jsClient.DataAdapterCachePrefix, ".").concat(this._cacheSuffix, ".").concat(key);
      }
    }, {
      key: "_isCachedResultValidFor204",
      value: function _isCachedResultValidFor204(_result, _user) {
        return false;
      }
    }, {
      key: "setDataLegacy",
      value: function setDataLegacy(data, user) {
        _superPropGet(NoFetchDataAdapter, "setData", this, 3)([data, user]);
      }

      // Do not stringify options property since that includes this adapter and will
      // cause a circular reference when Statsig sends diagnostic events and including
      // values is not necessary and makes the result huge
    }, {
      key: "toJSON",
      value: function toJSON() {
        var result = _objectSpread$3({}, this);
        delete result._options;
        delete result._inMemoryCache;
        delete result.bootstrapResult;
        return result;
      }
    }]);
  }(jsClient.DataAdapterCore);

  function ownKeys$2(e, r) {
    var t = Object.keys(e);
    if (Object.getOwnPropertySymbols) {
      var o = Object.getOwnPropertySymbols(e);
      r && (o = o.filter(function (r) {
        return Object.getOwnPropertyDescriptor(e, r).enumerable;
      })), t.push.apply(t, o);
    }
    return t;
  }
  function _objectSpread$2(e) {
    for (var r = 1; r < arguments.length; r++) {
      var t = null != arguments[r] ? arguments[r] : {};
      r % 2 ? ownKeys$2(Object(t), !0).forEach(function (r) {
        _defineProperty(e, r, t[r]);
      }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys$2(Object(t)).forEach(function (r) {
        Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r));
      });
    }
    return e;
  }
  function _createForOfIteratorHelper(r, e) {
    var t = "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"];
    if (!t) {
      if (Array.isArray(r) || (t = _unsupportedIterableToArray$1(r)) || e && r && "number" == typeof r.length) {
        t && (r = t);
        var _n = 0,
          F = function F() {};
        return {
          s: F,
          n: function n() {
            return _n >= r.length ? {
              done: !0
            } : {
              done: !1,
              value: r[_n++]
            };
          },
          e: function e(r) {
            throw r;
          },
          f: F
        };
      }
      throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
    }
    var o,
      a = !0,
      u = !1;
    return {
      s: function s() {
        t = t.call(r);
      },
      n: function n() {
        var r = t.next();
        return a = r.done, r;
      },
      e: function e(r) {
        u = !0, o = r;
      },
      f: function f() {
        try {
          a || null == t.return || t.return();
        } finally {
          if (u) throw o;
        }
      }
    };
  }
  function _unsupportedIterableToArray$1(r, a) {
    if (r) {
      if ("string" == typeof r) return _arrayLikeToArray$1(r, a);
      var t = {}.toString.call(r).slice(8, -1);
      return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray$1(r, a) : void 0;
    }
  }
  function _arrayLikeToArray$1(r, a) {
    (null == a || a > r.length) && (a = r.length);
    for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e];
    return n;
  }
  var LOCAL_OVERRIDE_REASON = 'LocalOverride:Recognized';
  var LOCAL_STORAGE_KEY = 'STATSIG_OVERRIDES';
  var LEGACY_LOCAL_STORAGE_KEY = 'STATSIG_JS_LITE_LOCAL_OVERRIDES';
  var makeEmptyStore = function makeEmptyStore() {
    return {
      gates: {},
      configs: {},
      layers: {}
    };
  };
  var djb2MapKey = function djb2MapKey(hash, kind) {
    return kind + ':' + hash;
  };

  /**
   * Custom implementation of `@statsig/js-local-overrides` package with support for local storage
   * so we can keep the existing behavior where overrides are cached locally. Also designed for
   * compatibility with the old override system (eg. no `experiments` field, `configs` is used
   * instead).
   *
   * [Reference](https://github.com/statsig-io/js-client-monorepo/blob/main/packages/js-local-overrides/src/LocalOverrideAdapter.ts)
   */
  var PersistentOverrideAdapter = /*#__PURE__*/function () {
    function PersistentOverrideAdapter(localStorageKey) {
      _classCallCheck(this, PersistentOverrideAdapter);
      this._overrides = makeEmptyStore();
      this._djb2Map = new Map();
      this._localStorageKey = localStorageKey;
    }
    return _createClass(PersistentOverrideAdapter, [{
      key: "parseStoredOverrides",
      value: function parseStoredOverrides(localStorageKey) {
        try {
          var json = window.localStorage.getItem(localStorageKey);
          if (!json) {
            return makeEmptyStore();
          }
          return JSON.parse(json);
        } catch (_unused) {
          return makeEmptyStore();
        }
      }
    }, {
      key: "mergeOverrides",
      value: function mergeOverrides() {
        var merged = makeEmptyStore();
        for (var _len = arguments.length, allOverrides = new Array(_len), _key = 0; _key < _len; _key++) {
          allOverrides[_key] = arguments[_key];
        }
        for (var _i = 0, _allOverrides = allOverrides; _i < _allOverrides.length; _i++) {
          var overrides = _allOverrides[_i];
          for (var _i2 = 0, _Object$entries = Object.entries((_overrides$gates = overrides.gates) !== null && _overrides$gates !== void 0 ? _overrides$gates : {}); _i2 < _Object$entries.length; _i2++) {
            var _overrides$gates;
            var _Object$entries$_i = _slicedToArray(_Object$entries[_i2], 2),
              name = _Object$entries$_i[0],
              value = _Object$entries$_i[1];
            merged.gates[name] = value;
          }
          for (var _i3 = 0, _Object$entries2 = Object.entries((_overrides$configs = overrides.configs) !== null && _overrides$configs !== void 0 ? _overrides$configs : {}); _i3 < _Object$entries2.length; _i3++) {
            var _overrides$configs;
            var _Object$entries2$_i = _slicedToArray(_Object$entries2[_i3], 2),
              _name = _Object$entries2$_i[0],
              _value = _Object$entries2$_i[1];
            merged.configs[_name] = _value;
          }
          for (var _i4 = 0, _Object$entries3 = Object.entries((_overrides$layers = overrides.layers) !== null && _overrides$layers !== void 0 ? _overrides$layers : {}); _i4 < _Object$entries3.length; _i4++) {
            var _overrides$layers;
            var _Object$entries3$_i = _slicedToArray(_Object$entries3[_i4], 2),
              _name2 = _Object$entries3$_i[0],
              _value2 = _Object$entries3$_i[1];
            merged.layers[_name2] = _value2;
          }
        }
        return merged;
      }
    }, {
      key: "initFromStoredOverrides",
      value: function initFromStoredOverrides() {
        var storedOverrides = this.mergeOverrides(this._overrides, this.parseStoredOverrides(LEGACY_LOCAL_STORAGE_KEY), this.parseStoredOverrides(this._localStorageKey));

        // In version 4.24.0 we introduced hashes in this override adapter, but had a bug which would cause
        // multiple hashes to continue being created. This code here removes these hashes since we've moved
        // to using a more reliable and easier to maintain map in `_djb2Map`.
        for (var _i5 = 0, _Object$values = Object.values(storedOverrides); _i5 < _Object$values.length; _i5++) {
          var container = _Object$values[_i5];
          var allKeys = new Set(Object.keys(container));
          var _iterator = _createForOfIteratorHelper(allKeys),
            _step;
          try {
            for (_iterator.s(); !(_step = _iterator.n()).done;) {
              var name = _step.value;
              var hash = clientCore._DJB2(name);
              if (allKeys.has(hash)) {
                delete container[hash];
              }
            }
          } catch (err) {
            _iterator.e(err);
          } finally {
            _iterator.f();
          }
        }
        this.applyOverrides(storedOverrides);
      }
    }, {
      key: "saveOverrides",
      value: function saveOverrides() {
        try {
          window.localStorage.setItem(this._localStorageKey, JSON.stringify(this._overrides));
        } catch (_unused2) {
          // ignored - window is not defined in non-browser environments, and we don't save things there
          // (things like SSR, etc)
        }
      }
    }, {
      key: "getOverrides",
      value: function getOverrides() {
        return this.mergeOverrides(this._overrides);
      }
    }, {
      key: "applyOverrides",
      value: function applyOverrides(overrides) {
        var newOverrides = _objectSpread$2(_objectSpread$2({}, makeEmptyStore()), overrides);
        this._djb2Map.clear();
        for (var _i6 = 0, _Object$entries4 = Object.entries(newOverrides); _i6 < _Object$entries4.length; _i6++) {
          var _Object$entries4$_i = _slicedToArray(_Object$entries4[_i6], 2),
            containerName = _Object$entries4$_i[0],
            container = _Object$entries4$_i[1];
          for (var _i7 = 0, _Object$entries5 = Object.entries(container); _i7 < _Object$entries5.length; _i7++) {
            var _Object$entries5$_i = _slicedToArray(_Object$entries5[_i7], 2),
              name = _Object$entries5$_i[0],
              value = _Object$entries5$_i[1];
            this._djb2Map.set(djb2MapKey(clientCore._DJB2(name), containerName), value);
          }
        }
        this._overrides = newOverrides;
      }
    }, {
      key: "setOverrides",
      value: function setOverrides(overrides) {
        this.applyOverrides(overrides);
        this.saveOverrides();
      }
    }, {
      key: "overrideGate",
      value: function overrideGate(name, value) {
        this._overrides.gates[name] = value;
        this._djb2Map.set(djb2MapKey(clientCore._DJB2(name), 'gates'), value);
        this.saveOverrides();
      }
    }, {
      key: "removeGateOverride",
      value: function removeGateOverride(name) {
        delete this._overrides.gates[name];
        this._djb2Map.delete(djb2MapKey(clientCore._DJB2(name), 'gates'));
        this.saveOverrides();
      }
    }, {
      key: "getGateOverride",
      value: function getGateOverride(current, _user) {
        var _this$_overrides$gate;
        var overridden = (_this$_overrides$gate = this._overrides.gates[current.name]) !== null && _this$_overrides$gate !== void 0 ? _this$_overrides$gate : this._djb2Map.get(djb2MapKey(current.name, 'gates'));
        if (overridden == null) {
          return null;
        }
        return _objectSpread$2(_objectSpread$2({}, current), {}, {
          value: overridden,
          details: _objectSpread$2(_objectSpread$2({}, current.details), {}, {
            reason: LOCAL_OVERRIDE_REASON
          })
        });
      }
    }, {
      key: "overrideDynamicConfig",
      value: function overrideDynamicConfig(name, value) {
        this._overrides.configs[name] = value;
        this._djb2Map.set(djb2MapKey(clientCore._DJB2(name), 'configs'), value);
        this.saveOverrides();
      }
    }, {
      key: "removeDynamicConfigOverride",
      value: function removeDynamicConfigOverride(name) {
        delete this._overrides.configs[name];
        this._djb2Map.delete(djb2MapKey(clientCore._DJB2(name), 'configs'));
        this.saveOverrides();
      }
    }, {
      key: "getDynamicConfigOverride",
      value: function getDynamicConfigOverride(current, _user) {
        return this._getConfigOverride(current, this._overrides.configs);
      }
    }, {
      key: "overrideExperiment",
      value: function overrideExperiment(name, value) {
        this._overrides.configs[name] = value;
        this._djb2Map.set(djb2MapKey(clientCore._DJB2(name), 'configs'), value);
        this.saveOverrides();
      }
    }, {
      key: "removeExperimentOverride",
      value: function removeExperimentOverride(name) {
        delete this._overrides.configs[name];
        this._djb2Map.delete(djb2MapKey(clientCore._DJB2(name), 'configs'));
        this.saveOverrides();
      }
    }, {
      key: "getExperimentOverride",
      value: function getExperimentOverride(current, _user) {
        return this._getConfigOverride(current, this._overrides.configs);
      }
    }, {
      key: "overrideLayer",
      value: function overrideLayer(name, value) {
        this._overrides.layers[name] = value;
        this._djb2Map.set(djb2MapKey(clientCore._DJB2(name), 'layers'), value);
        this.saveOverrides();
      }
    }, {
      key: "removeLayerOverride",
      value: function removeLayerOverride(name) {
        delete this._overrides.layers[name];
        this._djb2Map.delete(djb2MapKey(clientCore._DJB2(name), 'layers'));
        this.saveOverrides();
      }
    }, {
      key: "removeAllOverrides",
      value: function removeAllOverrides() {
        this._overrides = makeEmptyStore();
        window.localStorage.removeItem(LOCAL_STORAGE_KEY);
      }
    }, {
      key: "getLayerOverride",
      value: function getLayerOverride(current, _user) {
        var _this$_overrides$laye;
        var overridden = (_this$_overrides$laye = this._overrides.layers[current.name]) !== null && _this$_overrides$laye !== void 0 ? _this$_overrides$laye : this._djb2Map.get(djb2MapKey(current.name, 'layers'));
        if (overridden == null) {
          return null;
        }
        return _objectSpread$2(_objectSpread$2({}, current), {}, {
          __value: overridden,
          get: clientCore._makeTypedGet(current.name, overridden),
          details: _objectSpread$2(_objectSpread$2({}, current.details), {}, {
            reason: LOCAL_OVERRIDE_REASON
          })
        });
      }
    }, {
      key: "_getConfigOverride",
      value: function _getConfigOverride(current, lookup) {
        var _lookup$current$name;
        var overridden = (_lookup$current$name = lookup[current.name]) !== null && _lookup$current$name !== void 0 ? _lookup$current$name : this._djb2Map.get(djb2MapKey(current.name, 'configs'));
        if (overridden == null) {
          return null;
        }
        return _objectSpread$2(_objectSpread$2({}, current), {}, {
          value: overridden,
          get: clientCore._makeTypedGet(current.name, overridden),
          details: _objectSpread$2(_objectSpread$2({}, current.details), {}, {
            reason: LOCAL_OVERRIDE_REASON
          })
        });
      }
    }]);
  }();

  var _excluded = ["sdkKey", "environment", "updateUserCompletionCallback", "perimeter"];
  function ownKeys$1(e, r) {
    var t = Object.keys(e);
    if (Object.getOwnPropertySymbols) {
      var o = Object.getOwnPropertySymbols(e);
      r && (o = o.filter(function (r) {
        return Object.getOwnPropertyDescriptor(e, r).enumerable;
      })), t.push.apply(t, o);
    }
    return t;
  }
  function _objectSpread$1(e) {
    for (var r = 1; r < arguments.length; r++) {
      var t = null != arguments[r] ? arguments[r] : {};
      r % 2 ? ownKeys$1(Object(t), !0).forEach(function (r) {
        _defineProperty(e, r, t[r]);
      }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys$1(Object(t)).forEach(function (r) {
        Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r));
      });
    }
    return e;
  }
  var DEFAULT_CLIENT_KEY = 'client-default-key';
  // default event logging api is Atlassian proxy rather than Statsig's domain, to avoid ad blockers
  var DEFAULT_EVENT_LOGGING_API = 'https://xp.atlassian.com/v1/rgstr';
  var Client = /*#__PURE__*/function () {
    function Client() {
      var _this = this;
      var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
        _ref$localStorageKey = _ref.localStorageKey,
        localStorageKey = _ref$localStorageKey === void 0 ? LOCAL_STORAGE_KEY : _ref$localStorageKey;
      _classCallCheck(this, Client);
      _defineProperty(this, "initPromise", null);
      /** True if an initialize method was called and completed successfully. */
      _defineProperty(this, "initCompleted", false);
      /**
       * True if an initialize method was called and completed, meaning the client is now usable.
       * However if there was an error during initialization it may have initialized with default
       * values. Use {@link initCompleted} to check for this.
       */
      _defineProperty(this, "initWithDefaults", false);
      _defineProperty(this, "hasCheckGateErrorOccurred", false);
      _defineProperty(this, "hasGetExperimentErrorOccurred", false);
      _defineProperty(this, "hasGetExperimentValueErrorOccurred", false);
      _defineProperty(this, "hasGetLayerErrorOccurred", false);
      _defineProperty(this, "hasGetLayerValueErrorOccurred", false);
      _defineProperty(this, "subscriptions", new Subscriptions());
      _defineProperty(this, "dataAdapter", new NoFetchDataAdapter());
      /**
       * Call this if modifying the values being served by the Statsig library since it has its own
       * memoization cache which will not be updated if the values are changed outside of the library.
       */
      _defineProperty(this, "statsigValuesUpdated", function () {
        if (_this.user) {
          // Trigger a reset of the memoize cache
          _this.statsigClient.updateUserSync(_this.user, {
            disableBackgroundCacheRefresh: true
          });
        }
        _this.subscriptions.anyUpdated();
      });
      this.overrideAdapter = new PersistentOverrideAdapter(localStorageKey);
    }

    /**
     * @description
     * This method initializes the client using a network call to fetch the bootstrap values.
     * If the client is inialized with an `analyticsWebClient`, it will send an operational event
     * to GASv3 with the following attributes:
     * - targetApp: the target app of the client
     * - clientVersion: the version of the client
     * - success: whether the initialization was successful
     * - startTime: the time when the initialization started
     * - totalTime: the total time it took to initialize the client
     * - apiKey: the api key used to initialize the client
     * @param clientOptions {ClientOptions}
     * @param identifiers {Identifiers}
     * @param customAttributes {CustomAttributes}
     * @returns {Promise<void>}
     */
    return _createClass(Client, [{
      key: "initialize",
      value: function () {
        var _initialize = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee(clientOptions, identifiers, customAttributes) {
          var _this2 = this;
          var clientOptionsWithDefaults, startTime;
          return regenerator.wrap(function _callee$(_context) {
            while (1) switch (_context.prev = _context.next) {
              case 0:
                clientOptionsWithDefaults = getOptionsWithDefaults(clientOptions);
                if (!this.initPromise) {
                  _context.next = 4;
                  break;
                }
                if (!shallowEquals(clientOptionsWithDefaults, this.initOptions)) {
                  // eslint-disable-next-line no-console
                  console.warn('Feature Gates client already initialized with different options. New options were not applied.');
                }
                return _context.abrupt("return", this.initPromise);
              case 4:
                startTime = performance.now();
                this.initOptions = clientOptionsWithDefaults;
                this.initPromise = this.init(clientOptionsWithDefaults, identifiers, customAttributes).then(function () {
                  _this2.initCompleted = true;
                  _this2.initWithDefaults = true;
                }).finally(function () {
                  var endTime = performance.now();
                  var totalTime = endTime - startTime;
                  _this2.fireClientEvent(startTime, totalTime, 'initialize', _this2.initCompleted, clientOptionsWithDefaults.apiKey);
                });
                return _context.abrupt("return", this.initPromise);
              case 8:
              case "end":
                return _context.stop();
            }
          }, _callee, this);
        }));
        function initialize(_x, _x2, _x3) {
          return _initialize.apply(this, arguments);
        }
        return initialize;
      }()
      /**
       * @description
       * This method initializes the client using the provider given to call to fetch the bootstrap values.
       * If the client is initialized with an `analyticsWebClient`, it will send an operational event
       * to GASv3 with the following attributes:
       * - targetApp: the target app of the client
       * - clientVersion: the version of the client
       * - success: whether the initialization was successful
       * - startTime: the time when the initialization started
       * - totalTime: the total time it took to initialize the client
       * - apiKey: the api key used to initialize the client
       * @param clientOptions {ClientOptions}
       * @param provider {Provider}
       * @param identifiers {Identifiers}
       * @param customAttributes {CustomAttributes}
       * @returns {Promise<void>}
       */
    }, {
      key: "initializeWithProvider",
      value: function () {
        var _initializeWithProvider = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee2(clientOptions, provider, identifiers, customAttributes) {
          var _this3 = this;
          var clientOptionsWithDefaults, startTime;
          return regenerator.wrap(function _callee2$(_context2) {
            while (1) switch (_context2.prev = _context2.next) {
              case 0:
                clientOptionsWithDefaults = getOptionsWithDefaults(clientOptions);
                if (!this.initPromise) {
                  _context2.next = 4;
                  break;
                }
                if (!shallowEquals(clientOptionsWithDefaults, this.initOptions)) {
                  // eslint-disable-next-line no-console
                  console.warn('Feature Gates client already initialized with different options. New options were not applied.');
                }
                return _context2.abrupt("return", this.initPromise);
              case 4:
                startTime = performance.now();
                this.initOptions = clientOptionsWithDefaults;
                this.provider = provider;
                this.provider.setClientVersion(CLIENT_VERSION);
                if (this.provider.setApplyUpdateCallback) {
                  this.provider.setApplyUpdateCallback(this.applyUpdateCallback.bind(this));
                }
                this.initPromise = this.initWithProvider(clientOptionsWithDefaults, provider, identifiers, customAttributes).then(function () {
                  _this3.initCompleted = true;
                  _this3.initWithDefaults = true;
                }).finally(function () {
                  var endTime = performance.now();
                  var totalTime = endTime - startTime;
                  _this3.fireClientEvent(startTime, totalTime, 'initializeWithProvider', _this3.initCompleted, provider.getApiKey ? provider.getApiKey() : undefined);
                });
                return _context2.abrupt("return", this.initPromise);
              case 11:
              case "end":
                return _context2.stop();
            }
          }, _callee2, this);
        }));
        function initializeWithProvider(_x4, _x5, _x6, _x7) {
          return _initializeWithProvider.apply(this, arguments);
        }
        return initializeWithProvider;
      }()
    }, {
      key: "applyUpdateCallback",
      value: function applyUpdateCallback(experimentsResult) {
        try {
          if (this.initCompleted || this.initWithDefaults) {
            this.assertInitialized(this.statsigClient);
            this.dataAdapter.setBootstrapData(experimentsResult.experimentValues);
            this.dataAdapter.setData(JSON.stringify(experimentsResult.experimentValues));
            this.statsigValuesUpdated();
          }
        } catch (error) {
          // eslint-disable-next-line no-console
          console.warn('Error when attempting to apply update', error);
        }
      }
    }, {
      key: "fireClientEvent",
      value: function fireClientEvent(startTime, totalTime, action, success) {
        var _analyticsWebClient,
          _this4 = this;
        var apiKey = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : undefined;
        (_analyticsWebClient = this.initOptions.analyticsWebClient) === null || _analyticsWebClient === void 0 || _analyticsWebClient.then(function (analyticsWebClient) {
          var attributes = _objectSpread$1({
            targetApp: _this4.initOptions.targetApp,
            clientVersion: CLIENT_VERSION,
            success: success,
            startTime: startTime,
            totalTime: totalTime
          }, apiKey && {
            apiKey: apiKey
          });
          analyticsWebClient.sendOperationalEvent({
            action: action,
            actionSubject: 'featureGatesClient',
            attributes: attributes,
            tags: ['measurement'],
            source: '@atlaskit/feature-gate-js-client'
          });
        }).catch(function (err) {
          if (_this4.initOptions.environment !== FeatureGateEnvironment.Production) {
            // eslint-disable-next-line no-console
            console.error('Analytics web client promise did not resolve', err);
          }
        });
      }
    }, {
      key: "initializeFromValues",
      value: function () {
        var _initializeFromValues = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee3(clientOptions, identifiers, customAttributes) {
          var _this5 = this;
          var initializeValues,
            clientOptionsWithDefaults,
            startTime,
            _args3 = arguments;
          return regenerator.wrap(function _callee3$(_context3) {
            while (1) switch (_context3.prev = _context3.next) {
              case 0:
                initializeValues = _args3.length > 3 && _args3[3] !== undefined ? _args3[3] : {};
                clientOptionsWithDefaults = getOptionsWithDefaults(clientOptions);
                if (!this.initPromise) {
                  _context3.next = 5;
                  break;
                }
                if (!shallowEquals(clientOptionsWithDefaults, this.initOptions)) {
                  // eslint-disable-next-line no-console
                  console.warn('Feature Gates client already initialized with different options. New options were not applied.');
                }
                return _context3.abrupt("return", this.initPromise);
              case 5:
                // This makes sure the new Statsig client behaves like the old when bootstrap data is
                // passed, and `has_updates` isn't specified (which happens a lot in product integration tests).
                if (!Object.prototype.hasOwnProperty.call(initializeValues, 'has_updates')) {
                  initializeValues['has_updates'] = true;
                }
                startTime = performance.now();
                this.initOptions = clientOptionsWithDefaults;
                this.initPromise = this.initFromValues(clientOptionsWithDefaults, identifiers, customAttributes, initializeValues).then(function () {
                  _this5.initCompleted = true;
                  _this5.initWithDefaults = true;
                }).finally(function () {
                  var endTime = performance.now();
                  var totalTime = endTime - startTime;
                  _this5.fireClientEvent(startTime, totalTime, 'initializeFromValues', _this5.initCompleted);
                });
                return _context3.abrupt("return", this.initPromise);
              case 10:
              case "end":
                return _context3.stop();
            }
          }, _callee3, this);
        }));
        function initializeFromValues(_x8, _x9, _x10) {
          return _initializeFromValues.apply(this, arguments);
        }
        return initializeFromValues;
      }()
    }, {
      key: "assertInitialized",
      value: function assertInitialized(statsigClient) {
        if (!statsigClient) {
          throw new Error('Client must be initialized before using this method');
        }
      }

      /**
       * This method updates the user using a network call to fetch the new set of values.
       * @param fetchOptions {FetcherOptions}
       * @param identifiers {Identifiers}
       * @param customAttributes {CustomAttributes}
       */
    }, {
      key: "updateUser",
      value: function () {
        var _updateUser = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee4(fetchOptions, identifiers, customAttributes) {
          var fetchOptionsWithDefaults, initializeValuesProducer;
          return regenerator.wrap(function _callee4$(_context4) {
            while (1) switch (_context4.prev = _context4.next) {
              case 0:
                this.assertInitialized(this.statsigClient);
                fetchOptionsWithDefaults = getOptionsWithDefaults(fetchOptions);
                initializeValuesProducer = function initializeValuesProducer() {
                  return Fetcher.fetchExperimentValues(fetchOptionsWithDefaults, identifiers, customAttributes).then(function (_ref2) {
                    var experimentValues = _ref2.experimentValues,
                      customAttributes = _ref2.customAttributes;
                    return {
                      experimentValues: experimentValues,
                      customAttributesFromFetch: customAttributes
                    };
                  });
                };
                _context4.next = 5;
                return this.updateUserUsingInitializeValuesProducer(initializeValuesProducer, identifiers, customAttributes);
              case 5:
              case "end":
                return _context4.stop();
            }
          }, _callee4, this);
        }));
        function updateUser(_x11, _x12, _x13) {
          return _updateUser.apply(this, arguments);
        }
        return updateUser;
      }()
      /**
       * This method updates the user using the provider given on initialisation to get the new set of
       * values.
       * @param identifiers {Identifiers}
       * @param customAttributes {CustomAttributes}
       */
    }, {
      key: "updateUserWithProvider",
      value: function () {
        var _updateUserWithProvider = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee5(identifiers, customAttributes) {
          var _this6 = this;
          return regenerator.wrap(function _callee5$(_context5) {
            while (1) switch (_context5.prev = _context5.next) {
              case 0:
                this.assertInitialized(this.statsigClient);
                if (this.provider) {
                  _context5.next = 3;
                  break;
                }
                throw new Error('Cannot update user using provider as the client was not initialised with a provider');
              case 3:
                _context5.next = 5;
                return this.provider.setProfile(this.initOptions, identifiers, customAttributes);
              case 5:
                _context5.next = 7;
                return this.updateUserUsingInitializeValuesProducer(function () {
                  return _this6.provider.getExperimentValues();
                }, identifiers, customAttributes);
              case 7:
              case "end":
                return _context5.stop();
            }
          }, _callee5, this);
        }));
        function updateUserWithProvider(_x14, _x15) {
          return _updateUserWithProvider.apply(this, arguments);
        }
        return updateUserWithProvider;
      }()
      /**
       * This method updates the user given a new set of bootstrap values obtained from one of the
       * server-side SDKs.
       *
       * @param identifiers {Identifiers}
       * @param customAttributes {CustomAttributes}
       * @param initializeValues {Record<string,unknown>}
       */
    }, {
      key: "updateUserWithValues",
      value: function () {
        var _updateUserWithValues = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee6(identifiers, customAttributes) {
          var initializeValues,
            initializeValuesProducer,
            _args6 = arguments;
          return regenerator.wrap(function _callee6$(_context6) {
            while (1) switch (_context6.prev = _context6.next) {
              case 0:
                initializeValues = _args6.length > 2 && _args6[2] !== undefined ? _args6[2] : {};
                this.assertInitialized(this.statsigClient);
                initializeValuesProducer = function initializeValuesProducer() {
                  return Promise.resolve({
                    experimentValues: initializeValues,
                    customAttributesFromFetch: customAttributes
                  });
                };
                _context6.next = 5;
                return this.updateUserUsingInitializeValuesProducer(initializeValuesProducer, identifiers, customAttributes);
              case 5:
              case "end":
                return _context6.stop();
            }
          }, _callee6, this);
        }));
        function updateUserWithValues(_x16, _x17) {
          return _updateUserWithValues.apply(this, arguments);
        }
        return updateUserWithValues;
      }()
    }, {
      key: "initializeCalled",
      value: function initializeCalled() {
        return this.initPromise != null;
      }
    }, {
      key: "initializeCompleted",
      value: function initializeCompleted() {
        return this.initCompleted;
      }

      /**
       * Returns the value for a feature gate. Returns false if there are errors.
       * @param {string} gateName - The name of the feature gate.
       * @param {Object} options
       * @param {boolean} options.fireGateExposure
       *        Whether or not to fire the exposure event for the gate. Defaults to true.
       *        To log an exposure event manually at a later time, use {@link Client.manuallyLogGateExposure}
       *        (see [Statsig docs about manually logging exposures](https://docs.statsig.com/client/jsClientSDK#manual-exposures-)).
       */
    }, {
      key: "checkGate",
      value: function checkGate(gateName) {
        var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
        try {
          this.assertInitialized(this.statsigClient);
          var _options$fireGateExpo = options.fireGateExposure,
            fireGateExposure = _options$fireGateExpo === void 0 ? true : _options$fireGateExpo;
          return this.statsigClient.checkGate(gateName, {
            disableExposureLog: !fireGateExposure
          });
        } catch (error) {
          // Log the first occurrence of the error
          if (!this.hasCheckGateErrorOccurred) {
            // eslint-disable-next-line no-console
            console.warn({
              msg: 'An error has occurred checking the feature gate. Only the first occurrence of this error is logged.',
              gateName: gateName,
              error: error
            });
            this.hasCheckGateErrorOccurred = true;
          }
          return false;
        }
      }
    }, {
      key: "isGateExist",
      value: function isGateExist(gateName) {
        try {
          this.assertInitialized(this.statsigClient);
          var gate = this.statsigClient.getFeatureGate(gateName, {
            disableExposureLog: true
          });
          return !gate.details.reason.includes('Unrecognized');
        } catch (error) {
          // eslint-disable-next-line no-console
          console.error("Error occurred when trying to check FeatureGate: ".concat(error));
          // in case of error report true to avoid false positives.
          return true;
        }
      }
    }, {
      key: "isExperimentExist",
      value: function isExperimentExist(experimentName) {
        try {
          this.assertInitialized(this.statsigClient);
          var config = this.statsigClient.getExperiment(experimentName, {
            disableExposureLog: true
          });
          return !config.details.reason.includes('Unrecognized');
        } catch (error) {
          // eslint-disable-next-line no-console
          console.error("Error occurred when trying to check Experiment: ".concat(error));
          // in case of error report true to avoid false positives.
          return true;
        }
      }

      /**
       * Manually log a gate exposure (see [Statsig docs about manually logging exposures](https://docs.statsig.com/client/jsClientSDK#manual-exposures-)).
       * This is useful if you have evaluated a gate earlier via {@link Client.checkGate} where
       * <code>options.fireGateExposure</code> is false.
       * @param gateName
       */
    }, {
      key: "manuallyLogGateExposure",
      value: function manuallyLogGateExposure(gateName) {
        this.assertInitialized(this.statsigClient);
        // This is the approach recommended in the docs
        // https://docs.statsig.com/client/javascript-sdk/#manual-exposures-
        this.statsigClient.checkGate(gateName);
      }

      /**
       * Returns the entire config for a given experiment.
       *
       * @param {string} experimentName - The name of the experiment
       * @param {Object} options
       * @param {boolean} options.fireExperimentExposure - Whether or not to fire the exposure event
       * for the experiment. Defaults to true. To log an exposure event manually at a later time, use
       * {@link Client.manuallyLogExperimentExposure} (see [Statsig docs about manually logging exposures](https://docs.statsig.com/client/jsClientSDK#manual-exposures-)).
       * @returns The config for an experiment
       * @example
       * ```ts
       * const experimentConfig = client.getExperiment('example-experiment-name');
       * const backgroundColor: string = experimentConfig.get('backgroundColor', 'yellow');
       * ```
       */
    }, {
      key: "getExperiment",
      value: function getExperiment(experimentName) {
        var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
        try {
          this.assertInitialized(this.statsigClient);
          var _options$fireExperime = options.fireExperimentExposure,
            fireExperimentExposure = _options$fireExperime === void 0 ? true : _options$fireExperime;
          return DynamicConfig.fromExperiment(this.statsigClient.getExperiment(experimentName, {
            disableExposureLog: !fireExperimentExposure
          }));
        } catch (error) {
          // Log the first occurrence of the error
          if (!this.hasGetExperimentErrorOccurred) {
            // eslint-disable-next-line no-console
            console.warn({
              msg: 'An error has occurred getting the experiment. Only the first occurrence of this error is logged.',
              experimentName: experimentName,
              error: error
            });
            this.hasGetExperimentErrorOccurred = true;
          }

          // Return a default value
          return new DynamicConfig(experimentName, {}, '', {
            time: Date.now(),
            reason: EvaluationReason.Error
          });
        }
      }

      /**
       * Returns the value of a given parameter in an experiment config.
       *
       * @template T
       * @param {string} experimentName - The name of the experiment
       * @param {string} parameterName - The name of the parameter to fetch from the experiment config
       * @param {T} defaultValue - The value to serve if the experiment or parameter do not exist, or
       * if the returned value does not match the expected type.
       * @param {Object} options
       * @param {boolean} options.fireExperimentExposure - Whether or not to fire the exposure event
       * for the experiment. Defaults to true. To log an exposure event manually at a later time, use
       * {@link Client.manuallyLogExperimentExposure} (see [Statsig docs about manually logging exposures](https://docs.statsig.com/client/jsClientSDK#manual-exposures-))
       * @param {function} options.typeGuard - A function that asserts that the return value has the
       * expected type. If this function returns false, then the default value will be returned
       * instead. This can be set to protect your code from unexpected values being set remotely. By
       * default, this will be done by asserting that the default value and value are the same primitive
       * type.
       * @returns The value of the parameter if the experiment and parameter both exist, otherwise the
       * default value.
       * @example
       ``` ts
       type ValidColor = 'blue' | 'red' | 'yellow';
       type ValidColorTypeCheck = (value: unknown) => value is ValidColor;
      	 const isValidColor: ValidColorTypeCheck =
      		(value: unknown) => typeof value === 'string' && ['blue', 'red', 'yellow'].includes(value);
      	 const buttonColor: ValidColor = client.getExperimentValue(
      		'example-experiment-name',
      		'backgroundColor',
      		'yellow',
      		{
      				typeGuard: isValidColor
      		}
       );
       ```
      */
    }, {
      key: "getExperimentValue",
      value: function getExperimentValue(experimentName, parameterName, defaultValue) {
        var options = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
        var experiment = this.getExperiment(experimentName, options);
        try {
          var typeGuard = options.typeGuard;
          return experiment.get(parameterName, defaultValue, typeGuard);
        } catch (error) {
          // Log the first occurrence of the error
          if (!this.hasGetExperimentValueErrorOccurred) {
            // eslint-disable-next-line no-console
            console.warn({
              msg: 'An error has occurred getting the experiment value. Only the first occurrence of this error is logged.',
              experimentName: experimentName,
              defaultValue: defaultValue,
              options: options,
              error: error
            });
            this.hasGetExperimentValueErrorOccurred = true;
          }
          return defaultValue;
        }
      }

      /**
       * Manually log an experiment exposure (see [Statsig docs about manually logging exposures](https://docs.statsig.com/client/jsClientSDK#manual-exposures-)).
       * This is useful if you have evaluated an experiment earlier via {@link Client.getExperimentValue} or
       * {@link Client.getExperiment} where <code>options.fireExperimentExposure</code> is false.
       * @param experimentName
       */
    }, {
      key: "manuallyLogExperimentExposure",
      value: function manuallyLogExperimentExposure(experimentName) {
        this.assertInitialized(this.statsigClient);
        // This is the approach recommended in the docs
        // https://docs.statsig.com/client/javascript-sdk/#manual-exposures-
        this.statsigClient.getExperiment(experimentName);
      }

      /**
       * Manually log a layer exposure (see [Statsig docs about manually logging exposures](https://docs.statsig.com/client/jsClientSDK#manual-exposures-)).
       * This is useful if you have evaluated a layer earlier via {@link Client.getLayerValue} where <code>options.fireExperimentExposure</code> is false.
       * @param layerName
       * @param parameterName
       */
    }, {
      key: "manuallyLogLayerExposure",
      value: function manuallyLogLayerExposure(layerName, parameterName) {
        var _this$statsigClient$g;
        this.assertInitialized(this.statsigClient);
        // This is the approach recommended in the docs
        // https://docs.statsig.com/client/javascript-sdk/#manual-exposures-
        (_this$statsigClient$g = this.statsigClient.getLayer(layerName)) === null || _this$statsigClient$g === void 0 || _this$statsigClient$g.get(parameterName);
      }
    }, {
      key: "shutdownStatsig",
      value: function shutdownStatsig() {
        this.assertInitialized(this.statsigClient);
        this.statsigClient.shutdown();
      }

      /**
       * Adds a new override for the given gate.
       *
       * This method is additive, meaning you can call it multiple times with different gate names to
       * build your full set of overrides.
       *
       * Overrides are persisted to the `STATSIG_OVERRIDES` key in localStorage, so they
       * will continue to affect every client that is initialized on the same domain after this method
       * is called. If you are using this API for testing purposes, you should call
       * {@link Client.clearGateOverride} after your tests are completed to remove this
       * localStorage entry.
       *
       * @param {string} gateName
       * @param {boolean} value
       */
    }, {
      key: "overrideGate",
      value: function overrideGate(gateName, value) {
        this.overrideAdapter.overrideGate(gateName, value);
        // Trigger a reset of the memoized gate value
        if (this.user) {
          var _this$statsigClient;
          (_this$statsigClient = this.statsigClient) === null || _this$statsigClient === void 0 || _this$statsigClient.updateUserSync(this.user, {
            disableBackgroundCacheRefresh: true
          });
        }
        this.statsigValuesUpdated();
      }

      /**
       * Removes any overrides that have been set for the given gate.
       */
    }, {
      key: "clearGateOverride",
      value: function clearGateOverride(gateName) {
        this.overrideAdapter.removeGateOverride(gateName);
        this.statsigValuesUpdated();
      }

      /**
       * Adds a new override for the given config (or experiment).
       *
       * This method is additive, meaning you can call it multiple times with different experiment
       * names to build your full set of overrides.
       *
       * Overrides are persisted to the `STATSIG_OVERRIDES` key in localStorage, so they
       * will continue to affect every client that is initialized on the same domain after this method
       * is called. If you are using this API for testing purposes, you should call
       * {@link Client.clearConfigOverride} after your tests are completed to remove this
       * localStorage entry.
       *
       * @param {string} experimentName
       * @param {object} values
       */
    }, {
      key: "overrideConfig",
      value: function overrideConfig(experimentName, values) {
        this.overrideAdapter.overrideDynamicConfig(experimentName, values);
        this.statsigValuesUpdated();
      }

      /**
       * Removes any overrides that have been set for the given experiment.
       * @param {string} experimentName
       */
    }, {
      key: "clearConfigOverride",
      value: function clearConfigOverride(experimentName) {
        this.overrideAdapter.removeDynamicConfigOverride(experimentName);
        this.statsigValuesUpdated();
      }

      /**
       * Set overrides for gates, experiments and layers in batch.
       *
       * Note that these overrides are **not** additive and will completely replace any that have been
       * added via prior calls to {@link Client.overrideConfig} or
       * {@link Client.overrideGate}.
       *
       * Overrides are persisted to the `STATSIG_OVERRIDES` key in localStorage, so they
       * will continue to affect every client that is initialized on the same domain after this method
       * is called. If you are using this API for testing purposes, you should call
       * {@link Client.clearAllOverrides} after your tests are completed to remove this
       * localStorage entry.
       */
    }, {
      key: "setOverrides",
      value: function setOverrides(overrides) {
        this.overrideAdapter.setOverrides(overrides);
        this.statsigValuesUpdated();
      }

      /**
       * @returns The current overrides for gates, configs (including experiments) and layers.
       */
    }, {
      key: "getOverrides",
      value: function getOverrides() {
        return this.overrideAdapter.getOverrides();
      }

      /**
       * Clears overrides for all gates, configs (including experiments) and layers.
       */
    }, {
      key: "clearAllOverrides",
      value: function clearAllOverrides() {
        this.overrideAdapter.removeAllOverrides();
        this.statsigValuesUpdated();
      }

      /**
       * Returns whether the given identifiers and customAttributes align with the current
       * set that is being used by the client.
       *
       * If this method returns false, then the {@link Client.updateUser},
       * {@link Client.updateUserWithValues} or {@link Client.updateUserWithProvider}
       * methods can be used to re-align these values.
       *
       * @param identifiers
       * @param customAttributes
       * @returns a flag indicating whether the clients current configuration aligns with the given values
       */
    }, {
      key: "isCurrentUser",
      value: function isCurrentUser(identifiers, customAttributes) {
        return shallowEquals(this.currentIdentifiers, identifiers) && shallowEquals(this.currentAttributes, customAttributes);
      }

      /**
       * Subscribe to updates where the given callback will be called with the current checkGate value
       * @param gateName
       * @param callback
       * @param options
       * @returns off function to unsubscribe from updates
       */
    }, {
      key: "onGateUpdated",
      value: function onGateUpdated(gateName, callback) {
        var _this7 = this;
        var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
        var wrapCallback = function wrapCallback(value) {
          var _options$fireGateExpo2 = options.fireGateExposure,
            fireGateExposure = _options$fireGateExpo2 === void 0 ? true : _options$fireGateExpo2;
          if (fireGateExposure) {
            _this7.manuallyLogGateExposure(gateName);
          }
          try {
            callback(value);
          } catch (error) {
            // eslint-disable-next-line no-console
            console.warn("Error calling callback for gate ".concat(gateName, " with value ").concat(value), error);
          }
        };
        return this.subscriptions.onGateUpdated(gateName, wrapCallback, this.checkGate.bind(this), options);
      }

      /**
       * Subscribe to updates where the given callback will be called with the current experiment value
       * @param experimentName
       * @param parameterName
       * @param defaultValue
       * @param callback
       * @param options
       * @returns off function to unsubscribe from updates
       */
    }, {
      key: "onExperimentValueUpdated",
      value: function onExperimentValueUpdated(experimentName, parameterName, defaultValue, callback) {
        var _this8 = this;
        var options = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : {};
        var wrapCallback = function wrapCallback(value) {
          var _options$fireExperime2 = options.fireExperimentExposure,
            fireExperimentExposure = _options$fireExperime2 === void 0 ? true : _options$fireExperime2;
          if (fireExperimentExposure) {
            _this8.manuallyLogExperimentExposure(experimentName);
          }
          try {
            callback(value);
          } catch (error) {
            // eslint-disable-next-line no-console
            console.warn("Error calling callback for experiment ".concat(experimentName, " with value ").concat(value), error);
          }
        };
        return this.subscriptions.onExperimentValueUpdated(experimentName, parameterName, defaultValue, wrapCallback, this.getExperimentValue.bind(this), options);
      }

      /**
       * Subscribe so on any update the callback will be called.
       * NOTE: The callback will be called whenever the values are updated even if the values have not
       * changed.
       * @param callback
       * @returns off function to unsubscribe from updates
       */
    }, {
      key: "onAnyUpdated",
      value: function onAnyUpdated(callback) {
        return this.subscriptions.onAnyUpdated(callback);
      }

      /**
       * This method initializes the client using a network call to fetch the bootstrap values for the
       * given user.
       *
       * @param clientOptions
       * @param identifiers
       * @param customAttributes
       * @private
       */
    }, {
      key: "init",
      value: function () {
        var _init = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee7(clientOptions, identifiers, customAttributes) {
          var fromValuesClientOptions, experimentValues, customAttributesFromResult, clientSdkKeyPromise, experimentValuesPromise, _yield$Promise$all, _yield$Promise$all2, experimentValuesResult;
          return regenerator.wrap(function _callee7$(_context7) {
            while (1) switch (_context7.prev = _context7.next) {
              case 0:
                fromValuesClientOptions = _objectSpread$1({}, clientOptions);
                _context7.prev = 1;
                // If client sdk key fetch fails, an error would be thrown and handled instead of waiting for
                // the experiment values request to be settled, and it will fall back to use default values.
                clientSdkKeyPromise = Fetcher.fetchClientSdk(clientOptions).then(function (value) {
                  return fromValuesClientOptions.sdkKey = value.clientSdkKey;
                });
                experimentValuesPromise = Fetcher.fetchExperimentValues(clientOptions, identifiers, customAttributes); // Only wait for the experiment values request to finish and try to initialise the client
                // with experiment values if both requests are successful. Else an error would be thrown and
                // handled by the catch
                _context7.next = 6;
                return Promise.all([clientSdkKeyPromise, experimentValuesPromise]);
              case 6:
                _yield$Promise$all = _context7.sent;
                _yield$Promise$all2 = _slicedToArray(_yield$Promise$all, 2);
                experimentValuesResult = _yield$Promise$all2[1];
                experimentValues = experimentValuesResult.experimentValues;
                customAttributesFromResult = experimentValuesResult.customAttributes;
                _context7.next = 20;
                break;
              case 13:
                _context7.prev = 13;
                _context7.t0 = _context7["catch"](1);
                if (_context7.t0 instanceof Error) {
                  // eslint-disable-next-line no-console
                  console.error("Error occurred when trying to fetch the Feature Gates client values, error: ".concat(_context7.t0 === null || _context7.t0 === void 0 ? void 0 : _context7.t0.message));
                }
                // eslint-disable-next-line no-console
                console.warn("Initialising Statsig client without values");
                _context7.next = 19;
                return this.initFromValues(fromValuesClientOptions, identifiers, customAttributes);
              case 19:
                throw _context7.t0;
              case 20:
                return _context7.abrupt("return", this.initFromValues(fromValuesClientOptions, identifiers, customAttributesFromResult, experimentValues));
              case 21:
              case "end":
                return _context7.stop();
            }
          }, _callee7, this, [[1, 13]]);
        }));
        function init(_x18, _x19, _x20) {
          return _init.apply(this, arguments);
        }
        return init;
      }()
    }, {
      key: "initWithProvider",
      value: function () {
        var _initWithProvider = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee8(baseClientOptions, provider, identifiers, customAttributes) {
          var fromValuesClientOptions, experimentValues, customAttributesFromResult, clientSdkKeyPromise, experimentValuesPromise, _yield$Promise$all3, _yield$Promise$all4, experimentValuesResult;
          return regenerator.wrap(function _callee8$(_context8) {
            while (1) switch (_context8.prev = _context8.next) {
              case 0:
                fromValuesClientOptions = _objectSpread$1(_objectSpread$1({}, baseClientOptions), {}, {
                  disableCurrentPageLogging: true
                });
                _context8.prev = 1;
                _context8.next = 4;
                return provider.setProfile(baseClientOptions, identifiers, customAttributes);
              case 4:
                // If client sdk key fetch fails, an error would be thrown and handled instead of waiting for
                // the experiment values request to be settled, and it will fall back to use default values.
                clientSdkKeyPromise = provider.getClientSdkKey().then(function (value) {
                  return fromValuesClientOptions.sdkKey = value;
                });
                experimentValuesPromise = provider.getExperimentValues(); // Only wait for the experiment values request to finish and try to initialise the client
                // with experiment values if both requests are successful. Else an error would be thrown and
                // handled by the catch
                _context8.next = 8;
                return Promise.all([clientSdkKeyPromise, experimentValuesPromise]);
              case 8:
                _yield$Promise$all3 = _context8.sent;
                _yield$Promise$all4 = _slicedToArray(_yield$Promise$all3, 2);
                experimentValuesResult = _yield$Promise$all4[1];
                experimentValues = experimentValuesResult.experimentValues;
                customAttributesFromResult = experimentValuesResult.customAttributesFromFetch;
                _context8.next = 22;
                break;
              case 15:
                _context8.prev = 15;
                _context8.t0 = _context8["catch"](1);
                if (_context8.t0 instanceof Error) {
                  // eslint-disable-next-line no-console
                  console.error("Error occurred when trying to fetch the Feature Gates client values, error: ".concat(_context8.t0 === null || _context8.t0 === void 0 ? void 0 : _context8.t0.message));
                }
                // eslint-disable-next-line no-console
                console.warn("Initialising Statsig client without values");
                _context8.next = 21;
                return this.initFromValues(fromValuesClientOptions, identifiers, customAttributes);
              case 21:
                throw _context8.t0;
              case 22:
                return _context8.abrupt("return", this.initFromValues(fromValuesClientOptions, identifiers, customAttributesFromResult, experimentValues));
              case 23:
              case "end":
                return _context8.stop();
            }
          }, _callee8, this, [[1, 15]]);
        }));
        function initWithProvider(_x21, _x22, _x23, _x24) {
          return _initWithProvider.apply(this, arguments);
        }
        return initWithProvider;
      }()
      /**
       * This method initializes the client using a set of boostrap values obtained from one of the
       * server-side SDKs.
       *
       * @param clientOptions
       * @param identifiers
       * @param customAttributes
       * @param initializeValues
       * @private
       */
    }, {
      key: "initFromValues",
      value: function () {
        var _initFromValues = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee9(clientOptions, identifiers, customAttributes) {
          var _newClientOptions$net;
          var initializeValues,
            newClientOptions,
            sdkKey,
            environment,
            restClientOptions,
            statsigOptions,
            _args9 = arguments;
          return regenerator.wrap(function _callee9$(_context9) {
            while (1) switch (_context9.prev = _context9.next) {
              case 0:
                initializeValues = _args9.length > 3 && _args9[3] !== undefined ? _args9[3] : {};
                this.overrideAdapter.initFromStoredOverrides();
                this.currentIdentifiers = identifiers;
                this.currentAttributes = customAttributes;
                newClientOptions = migrateInitializationOptions(clientOptions);
                if (!newClientOptions.sdkKey) {
                  newClientOptions.sdkKey = DEFAULT_CLIENT_KEY;
                }
                if (!((_newClientOptions$net = newClientOptions.networkConfig) !== null && _newClientOptions$net !== void 0 && _newClientOptions$net.logEventUrl)) {
                  newClientOptions.networkConfig = _objectSpread$1(_objectSpread$1({}, newClientOptions.networkConfig), {}, {
                    logEventUrl: DEFAULT_EVENT_LOGGING_API
                  });
                }
                if (newClientOptions.perimeter === PerimeterType.FEDRAMP_MODERATE) {
                  // disable all logging in FedRAMP to prevent egress of sensitive data
                  newClientOptions.disableLogging = true;
                }
                sdkKey = newClientOptions.sdkKey, environment = newClientOptions.environment, newClientOptions.updateUserCompletionCallback, newClientOptions.perimeter, restClientOptions = _objectWithoutProperties(newClientOptions, _excluded);
                this.sdkKey = sdkKey;
                this.user = toStatsigUser(identifiers, customAttributes, this.sdkKey);
                statsigOptions = _objectSpread$1(_objectSpread$1({}, restClientOptions), {}, {
                  environment: {
                    tier: environment
                  },
                  includeCurrentPageUrlWithEvents: false,
                  dataAdapter: this.dataAdapter,
                  overrideAdapter: this.overrideAdapter
                });
                _context9.prev = 12;
                this.statsigClient = new jsClient.StatsigClient(sdkKey, this.user, statsigOptions);
                this.dataAdapter.setBootstrapData(initializeValues);
                _context9.next = 17;
                return this.statsigClient.initializeAsync();
              case 17:
                _context9.next = 29;
                break;
              case 19:
                _context9.prev = 19;
                _context9.t0 = _context9["catch"](12);
                if (_context9.t0 instanceof Error) {
                  // eslint-disable-next-line no-console
                  console.error("Error occurred when trying to initialise the Statsig client, error: ".concat(_context9.t0 === null || _context9.t0 === void 0 ? void 0 : _context9.t0.message));
                }
                // eslint-disable-next-line no-console
                console.warn("Initialising Statsig client with default sdk key and without values");
                this.statsigClient = new jsClient.StatsigClient(DEFAULT_CLIENT_KEY, this.user, statsigOptions);
                this.dataAdapter.setBootstrapData();
                _context9.next = 27;
                return this.statsigClient.initializeAsync();
              case 27:
                this.initWithDefaults = true;
                throw _context9.t0;
              case 29:
              case "end":
                return _context9.stop();
            }
          }, _callee9, this, [[12, 19]]);
        }));
        function initFromValues(_x25, _x26, _x27) {
          return _initFromValues.apply(this, arguments);
        }
        return initFromValues;
      }()
      /**
       * This method updates the user for this client with the bootstrap values returned from a given
       * Promise.
       * It uses the customAttributes from fetching experiment values to update the Statsig user but
       * uses the customAttributes from given input to check if the user has changed.
       *
       * @param {Identifiers} identifiers
       * @param {CustomAttributes} customAttributes
       * @param {Promise<InitializeValues>} getInitializeValues
       * @private
       */
    }, {
      key: "updateUserUsingInitializeValuesProducer",
      value: function () {
        var _updateUserUsingInitializeValuesProducer = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee11(getInitializeValues, identifiers, customAttributes) {
          var _this9 = this;
          var originalInitPromise, initializeValuesPromise, updateUserPromise;
          return regenerator.wrap(function _callee11$(_context11) {
            while (1) switch (_context11.prev = _context11.next) {
              case 0:
                this.assertInitialized(this.statsigClient);
                if (this.initPromise) {
                  _context11.next = 3;
                  break;
                }
                throw new Error('The client must be initialized before you can update the user.');
              case 3:
                if (!this.isCurrentUser(identifiers, customAttributes)) {
                  _context11.next = 5;
                  break;
                }
                return _context11.abrupt("return", this.initPromise);
              case 5:
                // Wait for the current initialize/update to finish
                originalInitPromise = this.initPromise;
                _context11.prev = 6;
                _context11.next = 9;
                return this.initPromise;
              case 9:
                _context11.next = 13;
                break;
              case 11:
                _context11.prev = 11;
                _context11.t0 = _context11["catch"](6);
              case 13:
                initializeValuesPromise = getInitializeValues();
                updateUserPromise = this.updateStatsigClientUser(initializeValuesPromise, identifiers, customAttributes); // We replace the init promise here since we are essentially re-initializing the client at this
                // point. Any subsequent calls to await client.initialize() or client.updateUser()
                // will now also await this user update.
                this.initPromise = updateUserPromise.catch(/*#__PURE__*/_asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee10() {
                  return regenerator.wrap(function _callee10$(_context10) {
                    while (1) switch (_context10.prev = _context10.next) {
                      case 0:
                        // If the update failed then it changed nothing, so revert back to the original promise.
                        _this9.initPromise = originalInitPromise;

                        // Set the user profile again to revert back to the current user
                        if (!_this9.provider) {
                          _context10.next = 4;
                          break;
                        }
                        _context10.next = 4;
                        return _this9.provider.setProfile(_this9.initOptions, _this9.currentIdentifiers, _this9.currentAttributes);
                      case 4:
                      case "end":
                        return _context10.stop();
                    }
                  }, _callee10);
                })));
                return _context11.abrupt("return", updateUserPromise);
              case 17:
              case "end":
                return _context11.stop();
            }
          }, _callee11, this, [[6, 11]]);
        }));
        function updateUserUsingInitializeValuesProducer(_x28, _x29, _x30) {
          return _updateUserUsingInitializeValuesProducer.apply(this, arguments);
        }
        return updateUserUsingInitializeValuesProducer;
      }()
      /**
       * This method updates the user on the nested Statsig client
       *
       * @param identifiers
       * @param customAttributes
       * @param initializeValuesPromise
       * @private
       */
    }, {
      key: "updateStatsigClientUser",
      value: function () {
        var _updateStatsigClientUser = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee12(initializeValuesPromise, identifiers, customAttributes) {
          var _this$initOptions, _this$initOptions$upd;
          var initializeValues, user, _updateUserCompletion, _ref4, errMsg, success, errorMessage;
          return regenerator.wrap(function _callee12$(_context12) {
            while (1) switch (_context12.prev = _context12.next) {
              case 0:
                this.assertInitialized(this.statsigClient);
                _context12.prev = 1;
                _context12.next = 4;
                return initializeValuesPromise;
              case 4:
                initializeValues = _context12.sent;
                user = toStatsigUser(identifiers, initializeValues.customAttributesFromFetch, this.sdkKey);
                _context12.next = 13;
                break;
              case 8:
                _context12.prev = 8;
                _context12.t0 = _context12["catch"](1);
                // Make sure the updateUserCompletionCallback is called for any errors in our custom code.
                // This is not necessary for the updateUserWithValues call, because the Statsig client will
                // already invoke the callback itself.
                errMsg = _context12.t0 instanceof Error ? _context12.t0.message : JSON.stringify(_context12.t0);
                (_updateUserCompletion = (_ref4 = this.initOptions).updateUserCompletionCallback) === null || _updateUserCompletion === void 0 || _updateUserCompletion.call(_ref4, false, errMsg);
                throw _context12.t0;
              case 13:
                success = true;
                errorMessage = null;
                _context12.prev = 15;
                this.dataAdapter.setBootstrapData(initializeValues.experimentValues);
                this.user = user;
                _context12.next = 20;
                return this.statsigClient.updateUserAsync(this.user);
              case 20:
                _context12.next = 26;
                break;
              case 22:
                _context12.prev = 22;
                _context12.t1 = _context12["catch"](15);
                success = false;
                errorMessage = String(_context12.t1);
              case 26:
                (_this$initOptions = this.initOptions) === null || _this$initOptions === void 0 || (_this$initOptions$upd = _this$initOptions.updateUserCompletionCallback) === null || _this$initOptions$upd === void 0 || _this$initOptions$upd.call(_this$initOptions, success, errorMessage);
                if (!success) {
                  _context12.next = 33;
                  break;
                }
                this.currentIdentifiers = identifiers;
                this.currentAttributes = customAttributes;
                this.subscriptions.anyUpdated();
                _context12.next = 34;
                break;
              case 33:
                throw new Error('Failed to update user. An unexpected error occured.');
              case 34:
              case "end":
                return _context12.stop();
            }
          }, _callee12, this, [[1, 8], [15, 22]]);
        }));
        function updateStatsigClientUser(_x31, _x32, _x33) {
          return _updateStatsigClientUser.apply(this, arguments);
        }
        return updateStatsigClientUser;
      }()
    }, {
      key: "getPackageVersion",
      value:
      /**
       * @returns string version of the current package in semver style.
       */
      function getPackageVersion() {
        return CLIENT_VERSION;
      }

      /**
       * Returns a specified layer otherwise returns an empty layer as a default value if the layer doesn't exist.
       *
       * @param {string} layerName - The name of the layer
       * @param {Object} options
       * @param {boolean} options.fireLayerExposure - Whether or not to fire the exposure event for the
       * layer. Defaults to true. To log an exposure event manually at a later time, use
       * {@link Client.manuallyLogLayerExposure} (see [Statsig docs about manually logging exposures](https://docs.statsig.com/client/jsClientSDK#manual-exposures-)).
       * @returns A layer
       * @example
       * ```ts
       * const layer = client.getLayer('example-layer-name');
       * const exampletitle: string = layer.get("title", "Welcome to Statsig!");
       * ```
       */
    }, {
      key: "getLayer",
      value: function getLayer(/** The name of the layer */
      layerName) {
        var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
        try {
          this.assertInitialized(this.statsigClient);
          var _options$fireLayerExp = options.fireLayerExposure,
            fireLayerExposure = _options$fireLayerExp === void 0 ? true : _options$fireLayerExp;
          return Layer.fromLayer(this.statsigClient.getLayer(layerName, {
            disableExposureLog: !fireLayerExposure
          }));
        } catch (error) {
          // Log the first occurrence of the error
          if (!this.hasGetLayerErrorOccurred) {
            // eslint-disable-next-line no-console
            console.warn({
              msg: 'An error has occurred getting the layer. Only the first occurrence of this error is logged.',
              layerName: layerName,
              error: error
            });
            this.hasGetLayerErrorOccurred = true;
          }

          // Return a default value
          return Layer.fromLayer(jsClient._makeLayer(layerName, {
            reason: 'Error'
          }, null));
        }
      }

      /**
       * Returns the value of a given parameter in a layer config.
       *
       * @template T
       * @param {string} layerName - The name of the layer
       * @param {string} parameterName - The name of the parameter to fetch from the layer config
       * @param {T} defaultValue - The value to serve if the layer or parameter do not exist, or if the
       * returned value does not match the expected type.
       * @param {Object} options
       * @param {boolean} options.fireLayerExposure - Whether or not to fire the exposure event for the
       * layer. Defaults to true. To log an exposure event manually at a later time, use
       * {@link Client.manuallyLogLayerExposure} (see [Statsig docs about manually logging exposures](https://docs.statsig.com/client/jsClientSDK#manual-exposures-))
       * @param {function} options.typeGuard - A function that asserts that the return value has the expected type. If this function returns false, then the default value will be returned instead. This can be set to protect your code from unexpected values being set remotely. By default, this will be done by asserting that the default value and value are the same primitive type.
       * @returns The value of the parameter if the layer and parameter both exist, otherwise the default value.
       * @example
       * ``` ts
       * type ValidColor = 'blue' | 'red' | 'yellow';
       * type ValidColorTypeCheck = (value: unknown) => value is ValidColor;
       *
       * const isValidColor: ValidColorTypeCheck =
       *    (value: unknown) => typeof value === 'string' && ['blue', 'red', 'yellow'].includes(value);
       *
       * const buttonColor: ValidColor = client.getLayerValue(
       *    'example-layer-name',
       *    'backgroundColor',
       *    'yellow',
       *    {
       *        typeGuard: isValidColor
       *    }
       * );
       * ```
       */
    }, {
      key: "getLayerValue",
      value: function getLayerValue(layerName, parameterName, defaultValue) {
        var options = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
        var layer = this.getLayer(layerName, options);
        try {
          var typeGuard = options.typeGuard;
          return layer.get(parameterName, defaultValue, typeGuard);
        } catch (error) {
          // Log the first occurrence of the error
          if (!this.hasGetLayerValueErrorOccurred) {
            // eslint-disable-next-line no-console
            console.warn({
              msg: 'An error has occurred getting the layer value. Only the first occurrence of this error is logged.',
              layerName: layerName,
              defaultValue: defaultValue,
              options: options,
              error: error
            });
            this.hasGetLayerValueErrorOccurred = true;
          }
          return defaultValue;
        }
      }
    }]);
  }();

  var _FeatureGates;
  /**
   * Access the FeatureGates object via the default export.
   * ```ts
   * import FeatureGates from '@atlaskit/feature-gate-js-client';
   * ```
   */
  var FeatureGates = /*#__PURE__*/function () {
    function FeatureGates() {
      _classCallCheck(this, FeatureGates);
    }
    return _createClass(FeatureGates, null, [{
      key: "isGateExists",
      value: function isGateExists(gateName) {
        return this.client.isGateExist(gateName);
      }
    }, {
      key: "isExperimentExists",
      value: function isExperimentExists(experimentName) {
        return this.client.isExperimentExist(experimentName);
      }
    }]);
  }();
  _FeatureGates = FeatureGates;
  _defineProperty(FeatureGates, "client", new Client());
  _defineProperty(FeatureGates, "hasCheckGateErrorOccurred", false);
  _defineProperty(FeatureGates, "hasGetExperimentValueErrorOccurred", false);
  _defineProperty(FeatureGates, "checkGate", function (gateName, options) {
    try {
      // Check if the CRITERION override mechanism is available
      if (typeof window !== 'undefined' && window.__CRITERION__ && typeof window.__CRITERION__.getFeatureFlagOverride === 'function') {
        // Attempt to retrieve an override value for the feature gate
        var overrideValue = window.__CRITERION__.getFeatureFlagOverride(gateName);
        // If an override value is found, return it immediately
        if (overrideValue !== undefined) {
          return overrideValue;
        }
      }
    } catch (error) {
      // Log the first occurrence of the error
      if (!_FeatureGates.hasCheckGateErrorOccurred) {
        // eslint-disable-next-line no-console
        console.warn({
          msg: 'An error has occurred checking the feature gate from criterion override. Only the first occurrence of this error is logged.',
          gateName: gateName,
          error: error
        });
        _FeatureGates.hasCheckGateErrorOccurred = true;
      }
    }

    // Proceed with the main logic if no override is found
    return _FeatureGates.client.checkGate(gateName, options);
  });
  _defineProperty(FeatureGates, "getExperimentValue", function (experimentName, parameterName, defaultValue, options) {
    try {
      // Check if the CRITERION override mechanism is available
      if (typeof window !== 'undefined' && window.__CRITERION__ && typeof window.__CRITERION__.getExperimentValueOverride === 'function') {
        var overrideValue = window.__CRITERION__.getExperimentValueOverride(experimentName, parameterName);
        if (overrideValue !== undefined && overrideValue !== null) {
          return overrideValue;
        }
      }
    } catch (error) {
      // Log the first occurrence of the error
      if (!_FeatureGates.hasGetExperimentValueErrorOccurred) {
        // eslint-disable-next-line no-console
        console.warn({
          msg: 'An error has occurred getting the experiment value from criterion override. Only the first occurrence of this error is logged.',
          experimentName: experimentName,
          defaultValue: defaultValue,
          options: options,
          error: error
        });
        _FeatureGates.hasGetExperimentValueErrorOccurred = true;
      }
      return defaultValue;
    }

    // Proceed with the main logic if no override is found
    return _FeatureGates.client.getExperimentValue(experimentName, parameterName, defaultValue, options);
  });
  _defineProperty(FeatureGates, "initializeCalled", _FeatureGates.client.initializeCalled.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "initializeCompleted", _FeatureGates.client.initializeCompleted.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "initialize", _FeatureGates.client.initialize.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "initializeWithProvider", _FeatureGates.client.initializeWithProvider.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "initializeFromValues", _FeatureGates.client.initializeFromValues.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "manuallyLogGateExposure", _FeatureGates.client.manuallyLogGateExposure.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "getExperiment", _FeatureGates.client.getExperiment.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "manuallyLogExperimentExposure", _FeatureGates.client.manuallyLogExperimentExposure.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "manuallyLogLayerExposure", _FeatureGates.client.manuallyLogLayerExposure.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "shutdownStatsig", _FeatureGates.client.shutdownStatsig.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "overrideGate", _FeatureGates.client.overrideGate.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "clearGateOverride", _FeatureGates.client.clearGateOverride.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "overrideConfig", _FeatureGates.client.overrideConfig.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "clearConfigOverride", _FeatureGates.client.clearConfigOverride.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "setOverrides", _FeatureGates.client.setOverrides.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "getOverrides", _FeatureGates.client.getOverrides.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "clearAllOverrides", _FeatureGates.client.clearAllOverrides.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "isCurrentUser", _FeatureGates.client.isCurrentUser.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "onGateUpdated", _FeatureGates.client.onGateUpdated.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "onExperimentValueUpdated", _FeatureGates.client.onExperimentValueUpdated.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "onAnyUpdated", _FeatureGates.client.onAnyUpdated.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "updateUser", _FeatureGates.client.updateUser.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "updateUserWithProvider", _FeatureGates.client.updateUserWithProvider.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "updateUserWithValues", _FeatureGates.client.updateUserWithValues.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "getPackageVersion", _FeatureGates.client.getPackageVersion.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "getLayer", _FeatureGates.client.getLayer.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "getLayerValue", _FeatureGates.client.getLayerValue.bind(_FeatureGates.client));
  var boundFGJS = FeatureGates;

  // This makes it possible to get a reference to the FeatureGates client at runtime.
  // This is important for overriding values in Cypress tests, as there needs to be a
  // way to get the exact instance for a window in order to mock some of its methods.
  if (typeof window !== 'undefined') {
    if (window.__FEATUREGATES_JS__ === undefined) {
      window.__FEATUREGATES_JS__ = FeatureGates;
    } else {
      var _boundFGJS, _boundFGJS$getPackage;
      boundFGJS = window.__FEATUREGATES_JS__;
      var boundVersion = ((_boundFGJS = boundFGJS) === null || _boundFGJS === void 0 || (_boundFGJS$getPackage = _boundFGJS.getPackageVersion) === null || _boundFGJS$getPackage === void 0 ? void 0 : _boundFGJS$getPackage.call(_boundFGJS)) || '4.10.0 or earlier';
      if (boundVersion !== CLIENT_VERSION) {
        var message = "Multiple versions of FeatureGateClients found on the current page.\n      The currently bound version is ".concat(boundVersion, " when module version ").concat(CLIENT_VERSION, " was loading.");
        // eslint-disable-next-line no-console
        console.warn(message);
      }
    }
  }

  /**
   * @property {FeatureGates} FeatureGate default export
   */
  var FeatureGates$1 = boundFGJS;

  function getBooleanFeatureFlagStatsig(flagName) {
    if (FeatureGates$1.initializeCompleted()) {
      return FeatureGates$1.checkGate(flagName);
    }
    analytics$1.trackGasV3FeatureFlagDefaultFalse(flagName);
    console.error("[ACJS] Feature flag service is not initialised. Default flag '" + flagName + "' to false");
    return false;
  }

  /**
   * For all new feature flags, please create them in Statsig
   * https://hello.atlassian.net/wiki/spaces/ECO/pages/3707522428
   *
   * @param {string} flagName Feature flag name
   * @returns {boolean}
   */
  function getBooleanFeatureFlag(flagName) {
    return getBooleanFeatureFlagStatsig(flagName);
  }

  var performanceModuleDefined = false;
  var ADDON_KEY_CODEBARREL = 'com.codebarrel.addons.automation';

  /**
   * This is a temporary module for Code Barrel that will be removed when no longer needed.
   */
  function definePerformanceModule() {
    if (performanceModuleDefined) {
      return;
    }
    performanceModuleDefined = true;
    function isPerformanceModuleAllowed(cb) {
      return cb._context.extension.addon_key === ADDON_KEY_CODEBARREL;
    }
    var performanceModule = {
      /**
       * @see https://developer.mozilla.org/en-US/docs/web/api/performance/timing
       * @returns {Promise<PerformanceTiming>}
       */
      getPerformanceTiming: function getPerformanceTiming(cb) {
        return new Promise(function (resolve, reject) {
          if (!isPerformanceModuleAllowed(cb)) {
            reject(new Error('This is a restricted API'));
          }
          // We need to parse + stringify otherwise we get an empty object
          resolve(JSON.parse(JSON.stringify(window.performance.timing)));
        });
      },
      /**
       * @see https://developer.mozilla.org/en-US/docs/Web/API/PerformanceNavigationTiming
       * @returns {Promise<PerformanceNavigationTiming[] | void>}
       */
      getPerformanceNavigationTiming: function getPerformanceNavigationTiming(cb) {
        return new Promise(function (resolve, reject) {
          if (!isPerformanceModuleAllowed(cb)) {
            reject(new Error('This is a restricted API'));
          }
          if (!window.PerformanceNavigationTiming) {
            resolve(undefined);
          }
          var timing = window.performance.getEntriesByType('navigation');
          resolve(JSON.parse(JSON.stringify(timing)));
        });
      }
    };
    host$1.returnsPromise(performanceModule.getPerformanceTiming);
    host$1.returnsPromise(performanceModule.getPerformanceNavigationTiming);
    host$1.defineModule('_performance', performanceModule);
  }

  var _process, _process2;
  // We can't rely on NODE_ENV === 'test' if its value is already configured by the consumer to some other value, so better to use JEST_WORKER_ID
  // https://jestjs.io/docs/environment-variables#jest_worker_id
  var TESTS_MODE = (globalThis === null || globalThis === void 0 || (_process = globalThis.process) === null || _process === void 0 || (_process = _process.env) === null || _process === void 0 ? void 0 : _process.JEST_WORKER_ID) !== undefined;
  var DEBUG_MODE = !TESTS_MODE && (globalThis === null || globalThis === void 0 || (_process2 = globalThis.process) === null || _process2 === void 0 || (_process2 = _process2.env) === null || _process2 === void 0 ? void 0 : _process2.NODE_ENV) !== 'production';
  var debug = function debug() {
    var _console;
    if (!DEBUG_MODE) {
      return;
    }

    // eslint-disable-next-line no-console
    (_console = console).debug.apply(_console, arguments);
  };

  var pkgName = '@atlaskit/platform-feature-flags';
  var PFF_GLOBAL_KEY = '__PLATFORM_FEATURE_FLAGS__';
  var hasProcessEnv = typeof process !== 'undefined' && typeof process.env !== 'undefined';

  // FF global overrides can be configured by test runners or Storybook
  var ENV_ENABLE_PLATFORM_FF = hasProcessEnv ?
  // Use global "process" variable and process.env['FLAG_NAME'] syntax, so it can be replaced by webpack DefinePlugin
  undefined === 'true' : false;

  // STORYBOOK_ENABLE_PLATFORM_FF is included as storybook only allows env vars prefixed with STORYBOOK
  // https://github.com/storybookjs/storybook/issues/12270

  var ENV_STORYBOOK_ENABLE_PLATFORM_FF = hasProcessEnv ?
  // Use global "process" variable and process.env['FLAG_NAME'] syntax, so it can be replaced by webpack DefinePlugin
  undefined === 'true' : false;
  var ENABLE_GLOBAL_PLATFORM_FF_OVERRIDE = ENV_ENABLE_PLATFORM_FF || ENV_STORYBOOK_ENABLE_PLATFORM_FF;
  var DEFAULT_PFF_GLOBAL = {
    // In development mode we want to capture any feature flag checks that happen using the default resolver and log this result when the resolver is replaced.
    // This is because evaluating feature flags when the resolver/FF client is loaded asynchronously could cause unexpected issues.
    earlyResolvedFlags: new Map(),
    booleanResolver: function booleanResolver(flagKey) {
      return false;
    }
  };
  var globalVar = typeof window !== 'undefined' ? window : globalThis;
  globalVar[PFF_GLOBAL_KEY] = globalVar[PFF_GLOBAL_KEY] || DEFAULT_PFF_GLOBAL;
  function resolveBooleanFlag(flagKey) {
    if (ENABLE_GLOBAL_PLATFORM_FF_OVERRIDE) {
      debug('[%s]: The feature flags were enabled while running tests. The flag "%s" will be always enabled.', pkgName, flagKey);
      return true;
    }
    try {
      var _globalVar$PFF_GLOBAL2, _globalVar$PFF_GLOBAL3, _globalVar$PFF_GLOBAL4;
      // booleanResolver will be empty for products like Trello, Elevate, Recruit etc.
      // Currently only Confluence, Jira and Bitbucket has set it.
      if (((_globalVar$PFF_GLOBAL2 = globalVar[PFF_GLOBAL_KEY]) === null || _globalVar$PFF_GLOBAL2 === void 0 ? void 0 : _globalVar$PFF_GLOBAL2.booleanResolver) === undefined || ((_globalVar$PFF_GLOBAL3 = globalVar[PFF_GLOBAL_KEY]) === null || _globalVar$PFF_GLOBAL3 === void 0 ? void 0 : _globalVar$PFF_GLOBAL3.booleanResolver) === null) {
        // eslint-disable-next-line @atlaskit/platform/use-recommended-utils
        return FeatureGates$1.checkGate(flagKey);
      }
      var result = (_globalVar$PFF_GLOBAL4 = globalVar[PFF_GLOBAL_KEY]) === null || _globalVar$PFF_GLOBAL4 === void 0 ? void 0 : _globalVar$PFF_GLOBAL4.booleanResolver(flagKey);
      if (typeof result !== 'boolean') {
        // eslint-disable-next-line no-console
        console.warn("".concat(flagKey, " resolved to a non-boolean value, returning false for safety"));
        return false;
      }
      return result;
    } catch (e) {
      return false;
    }
  }

  /**
   * Returns the value of a feature flag. If the flag does not resolve, it returns the "false" as a default value.
   *
   * @param name
   */
  function fg(name) {
    return resolveBooleanFlag(name);
  }

  var allowedPlatformFeatureFlags = ['platform-visual-refresh-icons'];
  function isPlatformAllowedFeatureFlag(flagName) {
    return allowedPlatformFeatureFlags.includes(flagName);
  }
  function isAllowListedFeatureFlag(flagName) {
    return flagName.indexOf('acjs-iframe-allowlist') === 0;
  }
  var defined = false;
  function defineFeatureFlagModule() {
    if (defined) {
      return;
    }
    defined = true;
    var featureFlagModule = {
      getBooleanFeatureFlag: function getBooleanFeatureFlag$1(flagName) {
        return new Promise(function (resolve, reject) {
          if (isPlatformAllowedFeatureFlag(flagName)) {
            resolve(fg(flagName));
          } else {
            if (!isAllowListedFeatureFlag(flagName)) {
              reject(new Error('Only allowlisted flags can be accessed from the iframe.'));
              return;
            }
            resolve(getBooleanFeatureFlag(flagName));
          }
        });
      }
    };
    host$1.returnsPromise(featureFlagModule.getBooleanFeatureFlag);
    host$1.defineModule('_featureFlag', featureFlagModule);
  }

  // nowhere better to put this. Wires an extension for oldschool and new enviroments
  function createSimpleXdmExtension(extension) {
    var extensionConfig = extensionConfigSanitizer(extension);
    var systemExtensionConfigOptions = ExtensionConfigurationOptionsStore$1.get();
    extension.options = extensionConfig.options = Util.extend({}, extensionConfig.options);
    extension.options.globalOptions = systemExtensionConfigOptions;
    loadConditionalModules(extension.addon_key);
    var iframeAttributes = host$1.create(extensionConfig, function () {
      if (!extension.options.noDOM) {
        extension.$el = $(document.getElementById(extension.id));
      }
      IframeActions.notifyBridgeEstablished(extension.$el, extension);
    }, function () {
      IframeActions.notifyUnloaded(extension.$el, extension);
    });
    // HostApi destroy is relying on previous behaviour of the
    // iframe component wherein it would call simpleXDM.create(extension)
    // and then mutate the extension object with the id returned from the
    // iframeAttributes see changes made in ACJS-760 and ACJS-807
    extensionConfig.id = iframeAttributes.id;
    extension.id = iframeAttributes.id;
    Util.extend(iframeAttributes, iframeUtils.optionsToAttributes(extension.options));
    return {
      iframeAttributes: iframeAttributes,
      extension: extension
    };
  }
  function extensionConfigSanitizer(extension) {
    return {
      addon_key: extension.addon_key,
      key: extension.key,
      url: extension.url,
      options: extension.options
    };
  }
  function loadConditionalModules(addonKey) {
    if (addonKey === ADDON_KEY_CODEBARREL) {
      definePerformanceModule();
    }
    defineFeatureFlagModule();
  }
  var simpleXdmUtils = {
    createSimpleXdmExtension: createSimpleXdmExtension,
    extensionConfigSanitizer: extensionConfigSanitizer
  };

  var Iframe = /*#__PURE__*/function () {
    function Iframe() {
      this._contentResolver = false;
    }
    var _proto = Iframe.prototype;
    _proto.setContentResolver = function setContentResolver(callback) {
      this._contentResolver = callback;
    };
    _proto.resize = function resize(width, height, $el) {
      width = Util.stringToDimension(width);
      height = Util.stringToDimension(height);
      $el.css({
        width: width,
        height: height
      });
      $el.trigger('resized', {
        width: width,
        height: height
      });
    };
    _proto.simpleXdmExtension = function simpleXdmExtension(extension, $container) {
      if (!extension.url || urlUtils.hasJwt(extension.url) && urlUtils.isJwtExpired(extension.url)) {
        if (this._contentResolver) {
          jwtActions.requestRefreshUrl({
            extension: extension,
            resolver: this._contentResolver,
            $container: $container
          });
        } else {
          console.error('JWT is expired and no content resolver was specified');
        }
      } else {
        this._appendExtension($container, this._simpleXdmCreate(extension));
      }
    };
    _proto._simpleXdmCreate = function _simpleXdmCreate(extension) {
      var simpleXdmAttributes = simpleXdmUtils.createSimpleXdmExtension(extension);
      extension.id = simpleXdmAttributes.iframeAttributes.id;
      extension.$el = this.render(simpleXdmAttributes.iframeAttributes);
      return extension;
    };
    _proto._appendExtension = function _appendExtension($container, extension) {
      var existingFrame = $container.find('iframe');
      if (existingFrame.length > 0) {
        existingFrame.destroy();
      }
      if (extension.options.hideIframeUntilLoad) {
        extension.$el.css({
          visibility: 'hidden'
        }).load(function () {
          extension.$el.css({
            visibility: ''
          });
        });
      }
      $container.prepend(extension.$el);
      IframeActions.notifyIframeCreated(extension.$el, extension);
    };
    _proto._appendExtensionError = function _appendExtensionError($container, text) {
      var $error = $('<div class="connect-resolve-error"></div>');
      var $additionalText = $('<p />').text(text);
      $error.append('<p class="error">Error: The content resolver threw the following error:</p>');
      $error.append($additionalText);
      $container.prepend($error);
    };
    _proto.resolverResponse = function resolverResponse(data) {
      var simpleExtension = this._simpleXdmCreate(data.extension);
      this._appendExtension(data.$container, simpleExtension);
    };
    _proto.resolverFailResponse = function resolverFailResponse(data) {
      this._appendExtensionError(data.$container, data.errorText);
    };
    _proto.render = function render(attributes) {
      attributes = attributes || {};
      attributes.referrerpolicy = 'no-referrer';
      return $('<iframe />').attr(attributes).addClass('ap-iframe');
    };
    return Iframe;
  }();
  var IframeComponent = new Iframe();
  EventDispatcher$1.register('iframe-resize', function (data) {
    IframeComponent.resize(data.width, data.height, data.$el);
  });
  EventDispatcher$1.register('content-resolver-register-by-extension', function (data) {
    IframeComponent.setContentResolver(data.callback);
  });
  EventDispatcher$1.register('jwt-url-refreshed', function (data) {
    IframeComponent.resolverResponse(data);
  });
  EventDispatcher$1.register('jwt-url-refreshed-failed', function (data) {
    IframeComponent.resolverFailResponse(data);
  });
  EventDispatcher$1.register('after:iframe-bridge-established', function (data) {
    if (!data.extension.options.noDom) {
      data.$el[0].bridgeEstablished = true;
    } else {
      data.extension.options.bridgeEstablished = true;
    }
  });

  var ButtonActions = {
    clicked: function clicked($el) {
      EventDispatcher$1.dispatch('button-clicked', {
        $el: $el
      });
    },
    toggle: function toggle($el, disabled) {
      EventDispatcher$1.dispatch('button-toggle', {
        $el: $el,
        disabled: disabled
      });
    },
    toggleVisibility: function toggleVisibility($el, hidden) {
      EventDispatcher$1.dispatch('button-toggle-visibility', {
        $el: $el,
        hidden: hidden
      });
    }
  };

  var BUTTON_TYPES = ['primary', 'link', 'secondary'];
  var buttonId = 0;
  var Button$1 = /*#__PURE__*/function () {
    function Button() {
      this.AP_BUTTON_CLASS = 'ap-aui-button';
    }
    var _proto = Button.prototype;
    _proto.setType = function setType($button, type) {
      if (type && BUTTON_TYPES.indexOf(type) >= 0) {
        $button.addClass('aui-button-' + type);
      }
      return $button;
    };
    _proto.setDisabled = function setDisabled($button, disabled) {
      if (typeof disabled !== 'undefined' && !$button.data('immutable')) {
        $button.attr('aria-disabled', disabled);
      }
      return $button;
    };
    _proto.setHidden = function setHidden($button, hidden) {
      if (typeof hidden !== 'undefined' && !$button.data('immutable')) {
        $button.toggle(!hidden);
      }
      return $button;
    };
    _proto._setId = function _setId($button, id) {
      if (!id) {
        id = 'ap-button-' + buttonId;
        buttonId++;
      }
      $button.attr('id', id);
      return $button;
    };
    _proto._additionalClasses = function _additionalClasses($button, classes) {
      if (classes) {
        if (typeof classes !== 'string') {
          classes = classes.join(' ');
        }
        $button.addClass(classes);
      }
      return $button;
    };
    _proto.getName = function getName($button) {
      return $($button).data('name');
    };
    _proto.getText = function getText($button) {
      return $($button).text();
    };
    _proto.getIdentifier = function getIdentifier($button) {
      return $($button).data('identifier');
    };
    _proto.isVisible = function isVisible($button) {
      return $($button).is(':visible');
    };
    _proto.isEnabled = function isEnabled($button) {
      return !($($button).attr('aria-disabled') === 'true');
    };
    _proto.render = function render(options) {
      var $button = $('<button />');
      options = options || {};
      $button.addClass('aui-button ' + this.AP_BUTTON_CLASS);
      $button.text(options.text);
      $button.data(options.data);
      $button.data({
        name: options.name || options.identifier,
        identifier: options.identifier || buttonUtilsInstance.randomIdentifier(),
        immutable: options.immutable || false
      });
      this._additionalClasses($button, options.additionalClasses);
      this.setType($button, options.type);
      this.setDisabled($button, options.disabled || false);
      this._setId($button, options.id);
      return $button;
    };
    return Button;
  }();
  var ButtonComponent = new Button$1();
  // register 1 button listener globally on dom load
  $(function () {
    $('body').on('click', '.' + ButtonComponent.AP_BUTTON_CLASS, function (e) {
      var $button = $(e.target).closest('.' + ButtonComponent.AP_BUTTON_CLASS);
      if ($button.attr('aria-disabled') !== 'true') {
        ButtonActions.clicked($button);
      }
    });
  });
  EventDispatcher$1.register('button-toggle', function (data) {
    ButtonComponent.setDisabled(data.$el, data.disabled);
  });
  EventDispatcher$1.register('button-toggle-visibility', function (data) {
    ButtonComponent.setHidden(data.$el, data.hidden);
  });

  var CONTAINER_CLASSES = ['ap-iframe-container'];
  var IframeContainer = /*#__PURE__*/function () {
    function IframeContainer() {}
    var _proto = IframeContainer.prototype;
    _proto.createExtension = function createExtension(extension, options) {
      var $container = this._renderContainer();
      if (!options || options.loadingIndicator !== false) {
        $container.append(this._renderLoadingIndicator());
      }
      IframeComponent.simpleXdmExtension(extension, $container);
      return $container;
    };
    _proto._renderContainer = function _renderContainer(attributes) {
      var container = $('<div />').attr(attributes || {});
      container.addClass(CONTAINER_CLASSES.join(' '));
      return container;
    };
    _proto._renderLoadingIndicator = function _renderLoadingIndicator() {
      return LoadingComponent.render();
    };
    return IframeContainer;
  }();
  var IframeContainerComponent = new IframeContainer();
  EventDispatcher$1.register('iframe-create', function (data) {
    var id = 'embedded-' + data.extension.id;
    data.extension.$el.parents('.ap-iframe-container').attr('id', id);
  });

  function create(extension) {
    return IframeContainerComponent.createExtension(extension);
  }

  var ModuleActions = {
    defineCustomModule: function defineCustomModule(name, methods) {
      var data = {};
      if (!methods) {
        data.methods = name;
      } else {
        data.methods = methods;
        data.name = name;
      }
      EventDispatcher$1.dispatch('module-define-custom', data);
    }
  };

  var AnalyticsAction = {
    trackDeprecatedMethodUsed: function trackDeprecatedMethodUsed(methodUsed, extension) {
      EventDispatcher$1.dispatch('analytics-deprecated-method-used', {
        methodUsed: methodUsed,
        extension: extension
      });
    },
    trackIframeBridgeStart: function trackIframeBridgeStart(extension) {
      EventDispatcher$1.dispatch('iframe-bridge-start', {
        extension: extension
      });
    },
    trackExternalEvent: function trackExternalEvent(name, values) {
      EventDispatcher$1.dispatch('analytics-external-event-track', {
        eventName: name,
        values: values
      });
    },
    trackContentResolverEvent: function trackContentResolverEvent(success, extra) {
      EventDispatcher$1.dispatch('analytics-content-resolver-track', {
        success: success,
        extra: extra
      });
    },
    trackIframePerformanceMetrics: function trackIframePerformanceMetrics(metrics, extension) {
      if (metrics && Object.getOwnPropertyNames(metrics).length > 0) {
        EventDispatcher$1.dispatch('analytics-iframe-performance', {
          metrics: metrics,
          extension: extension
        });
      }
    },
    trackWebVitals: function trackWebVitals(metrics, extension) {
      if (metrics && Object.keys(metrics).length > 0) {
        EventDispatcher$1.dispatch('analytics-web-vitals', {
          metrics: metrics,
          extension: extension
        });
      }
    },
    trackIframeClick: function trackIframeClick(data) {
      EventDispatcher$1.dispatch('analytics-track-iframe-clicked', data);
    }
  };

  function _createForOfIteratorHelperLoose(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (it) return (it = it.call(o)).next.bind(it); if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; return function () { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }
  function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }
  function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i]; return arr2; }
  function sanitizeTriggers(triggers) {
    var onTriggers;
    if (Array.isArray(triggers)) {
      onTriggers = triggers.join(' ');
    } else if (typeof triggers === 'string') {
      onTriggers = triggers.trim();
    }
    return onTriggers;
  }
  function uniqueId() {
    return 'webitem-' + Math.floor(Math.random() * 1000000000).toString(16);
  }

  /**
   * LEGACY: get addon key by webitem for p2
   * @param {string} cssClass
   */
  function getExtensionKey(cssClass) {
    var m = cssClass ? cssClass.match(/ap-plugin-key-([^\s]*)/) : null;
    return Array.isArray(m) ? m[1] : false;
  }

  /**
   * LEGACY: get module key by webitem for p2
   * @param {string} cssClass
  */
  function getKey(cssClass) {
    var m = cssClass ? cssClass.match(/ap-module-key-([^\s]*)/) : null;
    return Array.isArray(m) ? m[1] : false;
  }

  /**
   * @param cssClass
   */
  function getTargetKey(cssClass) {
    var m = cssClass ? cssClass.match(/ap-target-key-([^\s]*)/) : null;
    return Array.isArray(m) ? m[1] : false;
  }

  /**
   * @param cssClass
   * @returns {string}
   */
  function getFullKey(cssClass) {
    return getExtensionKey(cssClass) + '__' + getKey(cssClass);
  }
  function getModuleOptionsByAddonAndModuleKey(type, addonKey, moduleKey) {
    var moduleType = type + 'Modules';
    if (window._AP && window._AP[moduleType] && window._AP[moduleType][addonKey] && window._AP[moduleType][addonKey][moduleKey]) {
      return Util.extend({}, window._AP[moduleType][addonKey][moduleKey].options);
    }
  }

  /**
   * @param {string} type
   * @param {string} cssClass
   */
  function getModuleOptionsForWebitem(type, cssClass) {
    var addon_key = getExtensionKey(cssClass);
    var targetKey = getTargetKey(cssClass);
    return getModuleOptionsByAddonAndModuleKey(type, addon_key, targetKey);
  }

  /**
   * gets the connect config from the encoded webitem target (via the url)
   * @param {JQuery} $target
   * @param {string} cssClass
   * @param {string|undefined} href
   */
  // eslint-disable-next-line complexity
  function getConfigFromTarget($target, cssClass, href) {
    var url = href || $target.attr('href');
    var convertedOptions = {};
    var iframeData;
    // adg3 has classes outside of a tag so look for href inside the a
    if (!url) {
      url = $target.find('a').attr('href');
    }
    if (url) {
      var hashIndex = url.indexOf('#');
      if (hashIndex >= 0) {
        var hash = url.substring(hashIndex + 1);
        try {
          iframeData = JSON.parse(decodeURI(hash));
        } catch (e) {
          console.error('ACJS: cannot decode webitem anchor');
        }
        if (iframeData && window._AP && window._AP._convertConnectOptions) {
          convertedOptions = window._AP._convertConnectOptions(iframeData);
        } else {
          console.error('ACJS: cannot convert webitem url to connect iframe options');
        }
      } else {
        // The URL has no hash component so fall back to the old behaviour of providing:
        // add-on key, module key, dialog module options and product context (from the webitem url).
        // This may be the case for web items that were persisted prior to the new storage format whereby a hash
        // fragment is added into the URL detailing the target module info. If this info is
        // not present, the content resolver will be used to resolve the module after the web
        // item is clicked.

        // Old URL format detected. Falling back to old functionality
        var fullKey = getFullKey(cssClass);
        var type = isInlineDialogTrigger(cssClass) ? 'inlineDialog' : 'dialog';
        var options = getModuleOptionsForWebitem(type, cssClass);
        if (!options && window._AP && window._AP[type + 'Options']) {
          options = Util.extend({}, window._AP[type + 'Options'][fullKey]) || {};
        }
        if (!options) {
          options = {};
          console.warn('no webitem ' + type + 'Options for ' + fullKey);
        }
        options.productContext = options.productContext || {};
        var query = queryString.parse(queryString.extract(url));
        Util.extend(options.productContext, query);
        convertedOptions = {
          addon_key: getExtensionKey(cssClass),
          key: getKey(cssClass),
          options: options
        };
      }
    }
    return convertedOptions;
  }

  /**
   * @param {string|string[]|null} classes
   * @returns {boolean}
   */
  function isInlineDialogTrigger(classes) {
    return classes ? classes.includes('ap-inline-dialog') : false;
  }

  /**
   * @param {string|string[]|null} classes
   * @returns {boolean}
   */
  function isDialogTrigger(classes) {
    return classes ? classes.includes('ap-dialog') : false;
  }

  /**
   * LEGACY - method for handling webitem options for p2
   * @param {JQuery} $target
   * @param {string} cssClass
   * @param {string|undefined} href
   */
  function getOptionsForWebItem($target, cssClass, href) {
    var fullKey = getFullKey(cssClass);
    var type = isInlineDialogTrigger(cssClass) ? 'inlineDialog' : 'dialog';
    var options = getModuleOptionsForWebitem(type, cssClass);
    if (!options && window._AP && window._AP[type + 'Options']) {
      options = Util.extend({}, window._AP[type + 'Options'][fullKey]) || {};
    }
    if (!options) {
      options = {};
      console.warn('no webitem ' + type + 'Options for ' + fullKey);
    }
    options.productContext = options.productContext || {};
    options.structuredContext = options.structuredContext || {};
    // create product context from url params

    var convertedConfig = getConfigFromTarget($target, cssClass, href);
    if (convertedConfig && convertedConfig.options) {
      Util.extend(options.productContext, convertedConfig.options.productContext);
      Util.extend(options.structuredContext, convertedConfig.options.structuredContext);
      options.contextJwt = convertedConfig.options.contextJwt;
    }
    return options;
  }

  /**
   * @param {HTMLElement} target
   * @param {string[]} attributeNames
   * @returns {string[]}
   */
  function collectParentElementAttributes(target, attributeNames) {
    // Collect attributes from parent elements of the webitem
    var attributes = [];
    var current = target;
    var count = 0;
    while (current && current !== document.body && count++ < 1000) {
      for (var _iterator = _createForOfIteratorHelperLoose(attributeNames), _step; !(_step = _iterator()).done;) {
        var attributeName = _step.value;
        var attribute = current.getAttribute(attributeName);
        if (attribute) {
          attributes.push(attribute);
        }
      }
      current = current.parentElement;
    }
    return attributes;
  }
  var WebItemUtils = {
    sanitizeTriggers: sanitizeTriggers,
    uniqueId: uniqueId,
    getExtensionKey: getExtensionKey,
    getKey: getKey,
    getOptionsForWebItem: getOptionsForWebItem,
    getModuleOptionsByAddonAndModuleKey: getModuleOptionsByAddonAndModuleKey,
    getConfigFromTarget: getConfigFromTarget
  };

  var ModuleProviders = function ModuleProviders() {
    var _this = this;
    this._providers = {};
    this.registerProvider = function (name, provider) {
      _this._providers[name] = provider;
    };
    this.getProvider = function (name) {
      return _this._providers[name];
    };
  };
  var ModuleProviders$1 = new ModuleProviders();

  // This is essentially a copy of the ACJSFrameworkAdaptor/BaseFrameworkAdaptor implementation generated
  // by compiling the connect-module-core typescript implementations of the equivalent classes.

  /**
   * This class provides common behaviour relating to the adaption of functionality to a
   * particular Connect client framework. This is necessary for an interim period during which
   * we have multiple Connect client frameworks that we need to support: ACJS and CaaS Client.
   */
  var ACJSFrameworkAdaptor = function () {
    function ACJSFrameworkAdaptor() {
      this.moduleNamesToModules = new Map();
    }
    /**
     * This method registers a module with the Connect client framework relating to this adaptor instance.
     * @param moduleDefinition the definition of the module.
     */
    ACJSFrameworkAdaptor.prototype.registerModule = function (module, props) {
      var moduleRegistrationName = module.getModuleRegistrationName();
      this.moduleNamesToModules.set(moduleRegistrationName, module);

      // This adaptor implementation doesn't need to register the SimpleXDM definition so the following is
      // commented out.
      //
      // var simpleXdmDefinition = module.getSimpleXdmDefinition(props);
      // this.registerModuleWithHost(moduleRegistrationName, simpleXdmDefinition);
    };
    ACJSFrameworkAdaptor.prototype.getModuleByName = function (moduleName) {
      return this.moduleNamesToModules.get(moduleName);
    };
    ACJSFrameworkAdaptor.prototype.getProviderByModuleName = function (moduleName) {
      var module = this.moduleNamesToModules.get(moduleName);
      if (module && module.isEnabled()) {
        return module.getProvider();
      } else {
        return undefined;
      }
    };
    return ACJSFrameworkAdaptor;
  }();
  var acjsFrameworkAdaptor = new ACJSFrameworkAdaptor();

  /**
   * @typedef {{$target: JQuery, eventType: 'click' | 'mouseenter', extension: Object}} WebItemInvokedEventData
   */
  var WebItemActions = {
    addWebItem: function addWebItem(potentialWebItem) {
      var webitem;
      var existing = webItemInstance.getWebItemsBySelector(potentialWebItem.selector);
      if (existing) {
        return false;
      } else {
        webitem = webItemInstance.setWebItem(potentialWebItem);
        EventDispatcher$1.dispatch('webitem-added', {
          webitem: webitem
        });
      }
    },
    /**
     * @param {boolean} isInlineDialog
     * @param {WebItemInvokedEventData} data
     */
    webitemInvoked: function webitemInvoked(isInlineDialog, data) {
      var eventName = isInlineDialog ? 'inline-dialog' : 'dialog';
      EventDispatcher$1.dispatch('webitem-invoked:' + eventName, data);
    }
  };

  var addedTriggersSet = new WeakSet();
  var WebItem = /*#__PURE__*/function () {
    function WebItem() {
      this._webitems = {};
      this._contentResolver = function noop() {};
    }
    var _proto = WebItem.prototype;
    _proto.setContentResolver = function setContentResolver(resolver) {
      this._contentResolver = resolver;
    };
    _proto.requestContent = function requestContent(extension) {
      if (extension.addon_key && extension.key) {
        return this._contentResolver.call(null, Util.extend({
          classifier: 'json'
        }, extension));
      }
    }
    // originally i had this written nicely with Object.values but
    // ie11 didn't like it and i couldn't find a nice pollyfill
    ;
    _proto.getWebItemsBySelector = function getWebItemsBySelector(selector) {
      var _this = this;
      var returnVal;
      Object.getOwnPropertyNames(this._webitems).some(function (key) {
        var obj = _this._webitems[key];
        if (obj.selector) {
          if (obj.selector.trim() === selector.trim()) {
            returnVal = obj;
            return true;
          }
        }
        return false;
      });
      return returnVal;
    };
    _proto.setWebItem = function setWebItem(potentialWebItem) {
      return this._webitems[potentialWebItem.name] = {
        name: potentialWebItem.name,
        selector: potentialWebItem.selector,
        triggers: potentialWebItem.triggers
      };
    };
    _proto._removeTriggers = function _removeTriggers(webitem) {
      var _this2 = this;
      var onTriggers = WebItemUtils.sanitizeTriggers(webitem.triggers);
      $(function () {
        $('body').off(onTriggers, webitem.selector, _this2._webitems[webitem.name]._on);
      });
      delete this._webitems[webitem.name]._on;
    };
    _proto._addTriggers = function _addTriggers(webitem) {
      var onTriggers = WebItemUtils.sanitizeTriggers(webitem.triggers);
      /**
       * @param event {Event}
       */
      webitem._on = function (event) {
        if (event.defaultPrevented) {
          return;
        }
        /** @type {JQuery} */
        var $target = $(event.target).closest(webitem.selector);
        if ($target.hasClass('ap-dialog-ignore')) {
          return;
        }
        event.preventDefault();
        _triggerWebItem($target, event.type);
      };
      $(function () {
        var attachHandlers = function attachHandlers(node) {
          if (addedTriggersSet.has(node)) {
            return;
          }
          addedTriggersSet.add(node);
          $(node).on(onTriggers, webitem._on);
        };
        // First, add handlers for any nodes already in the document
        document.querySelectorAll(webitem.selector).forEach(function (node) {
          attachHandlers(node);
        });
        // Next, set up a MutationObserver to add handlers for any nodes added to the document later
        var observer = new MutationObserver(function (mutationsList, observer) {
          mutationsList.forEach(function (mutation) {
            if (mutation.type !== 'childList') {
              return;
            }
            mutation.addedNodes.forEach(function (node) {
              if (node.nodeType !== Node.ELEMENT_NODE) {
                return;
              }
              if (node.matches(webitem.selector)) {
                // The element is being added directly
                attachHandlers(node);
              } else {
                // We need to check children of the added node as well, because a whole subtree can be added at once
                node.querySelectorAll(webitem.selector).forEach(function (childNode) {
                  attachHandlers(childNode);
                });
              }
            });
          });
        });
        // Start observing the target node for configured mutations
        observer.observe(document.body, {
          childList: true,
          subtree: true
        });

        // Append styles to make the webitem clickable
        $('head').append("<style type=\"text/css\">" + webitem.selector + ".ap-link-webitem {pointer-events: auto;cursor: pointer;}</style>");
      });
    };
    return WebItem;
  }();
  /**
   * @param {Element} target  The webItem trigger element, used for relatively positioning the inline dialog popup.
   * @param {string|undefined} [cssClass] The webItem's class name as returned by the Connect plugin. Will be taken from the target if not provided.
   * @param {string|undefined} [href] The webItem's href as returned by the Connect plugin. Will be taken from the target if not provided.
   * @param {'click' | 'mouseenter'} [eventType] The trigger type that we want to invoke.
   */
  function triggerWebItem(target, cssClass, href, eventType) {
    eventType = eventType || 'click';
    var $target = $(target);
    cssClass = cssClass || $target.attr('class');

    // ACJS would only listen for events on these specific classes.
    // However, this function could be called for any webItem, even if it is not a dialog.
    // In that case, we should just do nothing and let the default link behaviour occur.
    if (!isDialogWebItem(cssClass) || eventType === 'mouseenter' && !isInlineDialogTrigger(cssClass)) {
      return;
    }
    _triggerWebItem($target, eventType, cssClass, href, true);
  }

  /**
   * We export this function so that consumers can check if Connect will handle the event.
   * If so, they can prevent the default behaviour.
   * @param {string|string[]|null} classes
   * @returns {boolean}
   */
  function isDialogWebItem(classes) {
    return isDialogTrigger(classes) || isInlineDialogTrigger(classes);
  }

  /**
   * @private
   * @param {JQuery} $target The webItem trigger element. It is expected to have an href or data-href property, and class names representing the trigger type (ap-dialog, ap-inline-dialog, etc).
   * @param {'click' | 'mouseenter'} eventType The trigger type that we want to invoke.
   * @param {string|undefined} [cssClass] The webItem's class name as returned by the Connect plugin.
   * @param {string|undefined} [href] The webItem's href as returned by the Connect plugin.
   * @param {boolean} isModernApi Whether this was triggered via the modern imperative API, used for analytics.
   */
  function _triggerWebItem($target, eventType, cssClass, href, isModernApi) {
    cssClass = cssClass || $target.attr('class');
    isModernApi = isModernApi || false;
    var convertedOptions = WebItemUtils.getConfigFromTarget($target, cssClass, href);
    var extensionUrl = convertedOptions && convertedOptions.url ? convertedOptions.url : undefined;
    var extension = {
      addon_key: WebItemUtils.getExtensionKey(cssClass),
      key: WebItemUtils.getKey(cssClass),
      options: WebItemUtils.getOptionsForWebItem($target, cssClass, href),
      url: extensionUrl
    };
    if (extension.addon_key === 'com.addonengine.analytics' && !HostApi$1.isModuleDefined('analytics')) {
      console.log("ACJS-1164 Dropping event " + eventType + " for plugin " + extension.addon_key + " until AP.analytics loads...");
      return;
    }
    var isInlineDialog = isInlineDialogTrigger(cssClass);
    if (!isModernApi) {
      EventDispatcher$1.dispatch('analytics-legacy-dialog-opened', {
        extension: extension,
        eventType: eventType,
        parentIds: collectParentElementAttributes($target.get(0), ['data-testid', 'data-component-selector', 'data-fullscreen-id'])
      });
    }
    WebItemActions.webitemInvoked(isInlineDialog, {
      $target: $target,
      eventType: eventType,
      extension: extension
    });
  }
  var webItemInstance = new WebItem();
  EventDispatcher$1.register('webitem-added', function (data) {
    webItemInstance._addTriggers(data.webitem);
  });
  EventDispatcher$1.register('content-resolver-register-by-extension', function (data) {
    webItemInstance.setContentResolver(data.callback);
  });
  document.addEventListener('aui-responsive-menu-item-created', function (e) {
    var oldWebItem = e.detail.originalItem.querySelector('a[class*="ap-"]');
    if (oldWebItem) {
      var newWebItem = e.detail.newItem.querySelector('a');
      var classList = [].slice.call(oldWebItem.classList);
      classList.forEach(function (cls) {
        if (/^ap-/.test(cls)) {
          newWebItem.classList.add(cls);
        }
      });
    }
  });

  var HostApi = /*#__PURE__*/function () {
    function HostApi() {
      var _this = this;
      this.targetSpecNoDialogs = EventActions.targetSpecNoDialogs;
      this.triggerWebItem = triggerWebItem;
      this.isDialogWebItem = isDialogWebItem;
      this.create = function (extension) {
        return create(simpleXdmUtils.extensionConfigSanitizer(extension));
      };
      this.dialog = {
        create: function create(extension, dialogOptions) {
          var dialogBeanOptions = WebItemUtils.getModuleOptionsByAddonAndModuleKey('dialog', extension.addon_key, extension.key);
          var completeOptions = Util.extend({}, dialogBeanOptions || {}, dialogOptions);
          DialogExtensionActions.open(extension, completeOptions);
        },
        close: function close(addon_key, closeData) {
          var frameworkAdaptor = _this.getFrameworkAdaptor();
          var dialogProvider = frameworkAdaptor.getProviderByModuleName('dialog');
          if (dialogProvider) {
            dialogUtilsInstance.assertActiveDialogOrThrow(dialogProvider, addon_key);
            EventActions.broadcast('dialog.close', {
              addon_key: addon_key
            }, closeData);
            dialogProvider.close();
          } else {
            DialogExtensionActions.close();
          }
        }
      };
      this.registerContentResolver = {
        // This function is not called when a product calls resolveByExtension
        // This is just a wrapper for ACJS to be able to call the product implementation
        // Calls from the product will bypass this method and call the product implementation directly
        resolveByExtension: function resolveByExtension(callback) {
          _this._contentResolver = callback;
          jwtActions.registerContentResolver({
            callback: callback
          });
        }
      };
      this.getContentResolver = function () {
        return _this._contentResolver;
      };
      this.registerProvider = function (componentName, component) {
        ModuleProviders$1.registerProvider(componentName, component);
      };
      this.getProvider = function (componentName) {
        return ModuleProviders$1.getProvider(componentName);
      };
      // We are attaching an instance of ACJSAdaptor to the host so that products are able
      // to retrieve the identical instance of ACJSAdaptor that ACJS is using.
      // The product can override the framework adaptor by calling setFrameworkAdaptor().
      this.frameworkAdaptor = acjsFrameworkAdaptor;
    }
    /**
    * creates an extension
    * returns an object with extension and iframe attributes
    * designed for use with non DOM implementations such as react.
    */
    var _proto = HostApi.prototype;
    _proto.createExtension = function createExtension(extension) {
      extension.options = extension.options || {};
      extension.options.noDom = true;
      var createdExtension = simpleXdmUtils.createSimpleXdmExtension(extension);
      AnalyticsAction.trackIframeBridgeStart(createdExtension.extension);
      return createdExtension;
    }

    /**
     * registers an existing extension with this host
     * Used when the extension has been created by a sub host
     */;
    _proto.registerExistingExtension = function registerExistingExtension(extension_id, data) {
      return host$1.registerExistingExtension(extension_id, data);
    }

    /**
     * The product is responsible for setting the framework adaptor.
     * @param frameworkAdaptor the framework adaptor to use.
     */;
    _proto.setFrameworkAdaptor = function setFrameworkAdaptor(frameworkAdaptor) {
      this.frameworkAdaptor = frameworkAdaptor;
    };
    _proto.getFrameworkAdaptor = function getFrameworkAdaptor() {
      return this.frameworkAdaptor;
    };
    _proto._cleanExtension = function _cleanExtension(extension) {
      return Util.pick(extension, ['id', 'addon_key', 'key', 'options', 'url']);
    };
    _proto.onIframeEstablished = function onIframeEstablished(callback) {
      var wrapper = function wrapper(data) {
        callback.call({}, {
          $el: data.$el,
          extension: this._cleanExtension(data.extension)
        });
      };
      callback._wrapper = wrapper.bind(this);
      EventDispatcher$1.register('after:iframe-bridge-established', callback._wrapper);
    };
    _proto.offIframeEstablished = function offIframeEstablished(callback) {
      if (callback._wrapper) {
        EventDispatcher$1.unregister('after:iframe-bridge-established', callback._wrapper);
      } else {
        throw new Error('cannot unregister event dispatch listener without _wrapper reference');
      }
    };
    _proto.onIframeUnload = function onIframeUnload(callback) {
      var _this2 = this;
      EventDispatcher$1.register('after:iframe-unload', function (data) {
        callback.call({}, {
          $el: data.$el,
          extension: _this2._cleanExtension(data.extension)
        });
      });
    };
    _proto.onPublicEventDispatched = function onPublicEventDispatched(callback) {
      var wrapper = function wrapper(data) {
        callback.call({}, {
          type: data.type,
          event: data.event,
          extension: this._cleanExtension(data.sender)
        });
      };
      callback._wrapper = wrapper.bind(this);
      EventDispatcher$1.register('after:event-public-dispatch', callback._wrapper);
    };
    _proto.offPublicEventDispatched = function offPublicEventDispatched(callback) {
      if (callback._wrapper) {
        EventDispatcher$1.unregister('after:event-public-dispatch', callback._wrapper);
      } else {
        throw new Error('cannot unregister event dispatch listener without _wrapper reference');
      }
    };
    _proto.onKeyEvent = function onKeyEvent(extension_id, key, modifiers, callback) {
      DomEventActions.registerKeyEvent({
        extension_id: extension_id,
        key: key,
        modifiers: modifiers,
        callback: callback
      });
    };
    _proto.offKeyEvent = function offKeyEvent(extension_id, key, modifiers, callback) {
      DomEventActions.unregisterKeyEvent({
        extension_id: extension_id,
        key: key,
        modifiers: modifiers,
        callback: callback
      });
    };
    _proto.onFrameClick = function onFrameClick(handleIframeClick) {
      if (typeof handleIframeClick !== 'function') {
        throw new Error('handleIframeClick must be a function');
      }
      DomEventActions.registerClickHandler(handleIframeClick);
    };
    _proto.offFrameClick = function offFrameClick() {
      DomEventActions.unregisterClickHandler();
    };
    _proto.destroy = function destroy(extension_id) {
      IframeActions.notifyIframeDestroyed({
        id: extension_id
      });
    };
    _proto.defineModule = function defineModule(name, methods) {
      ModuleActions.defineCustomModule(name, methods);
    };
    _proto.isModuleDefined = function isModuleDefined(moduleName) {
      return host$1.isModuleDefined(moduleName);
    };
    _proto.broadcastEvent = function broadcastEvent(type, targetSpec, event) {
      EventActions.broadcast(type, targetSpec, event);
    };
    _proto.getExtensions = function getExtensions(filter) {
      return host$1.getExtensions(filter);
    };
    _proto.trackDeprecatedMethodUsed = function trackDeprecatedMethodUsed(methodUsed, extension) {
      AnalyticsAction.trackDeprecatedMethodUsed(methodUsed, extension);
    };
    _proto.trackAnalyticsEvent = function trackAnalyticsEvent(name, values) {
      AnalyticsAction.trackExternalEvent(name, values);
    };
    _proto.trackContentResolverEvent = function trackContentResolverEvent(success, extra) {
      AnalyticsAction.trackContentResolverEvent(success, extra);
    };
    _proto.setJwtClockSkew = function setJwtClockSkew(skew) {
      jwtActions.setClockSkew(skew);
    };
    _proto.isJwtExpired = function isJwtExpired(jwtString, tokenOnly) {
      if (tokenOnly) {
        return jwtUtil.isJwtExpired(jwtString);
      }
      return urlUtils.isJwtExpired(jwtString);
    };
    _proto.hasJwt = function hasJwt(url) {
      return urlUtils.hasJwt(url);
    };
    _proto.getBooleanFeatureFlag = function getBooleanFeatureFlag$1(flagName) {
      return getBooleanFeatureFlag(flagName);
    }

    // set configuration option system wide for all extensions
    // can be either key,value or an object
    ;
    _proto.setExtensionConfigurationOptions = function setExtensionConfigurationOptions(obj, value) {
      ExtensionConfigurationOptionsStore$1.set(obj, value);
    };
    _proto.getExtensionConfigurationOption = function getExtensionConfigurationOption(val) {
      return ExtensionConfigurationOptionsStore$1.get(val);
    };
    _proto.onIframeTimeout = function onIframeTimeout(callback) {
      var wrapper = function wrapper(data) {
        callback.call({}, {
          extension: this._cleanExtension(data.extension)
        });
      };
      callback._wrapper = wrapper.bind(this);
      EventDispatcher$1.register('after:iframe-bridge-timeout', callback._wrapper);
    };
    _proto.offIframeTimeout = function offIframeTimeout(callback) {
      if (callback._wrapper) {
        EventDispatcher$1.unregister('after:iframe-bridge-timeout', callback._wrapper);
      } else {
        throw new Error('cannot unregister event dispatch listener without _wrapper reference');
      }
    };
    _proto.onIframePerformanceTelemetry = function onIframePerformanceTelemetry(callback) {
      var wrapper = function wrapper(data) {
        callback.call({}, {
          metrics: data.metrics,
          extension: this._cleanExtension(data.extension)
        });
      };
      callback._wrapper = wrapper.bind(this);
      EventDispatcher$1.register('after:analytics-iframe-performance', callback._wrapper);
    };
    _proto.offIframePerformanceTelemetry = function offIframePerformanceTelemetry(callback) {
      if (callback._wrapper) {
        EventDispatcher$1.unregister('after:analytics-iframe-performance', callback._wrapper);
      } else {
        throw new Error('cannot unregister event dispatch listener without _wrapper reference');
      }
    };
    _proto.onIframeVisible = function onIframeVisible(callback) {
      var wrapper = function wrapper(extension) {
        callback.call({}, {
          extension: this._cleanExtension(extension)
        });
      };
      callback._wrapper = wrapper.bind(this);
      EventDispatcher$1.register('after:iframe-visible', callback._wrapper);
    };
    _proto.offIframeVisible = function offIframeVisible(callback) {
      if (callback._wrapper) {
        EventDispatcher$1.unregister('after:iframe-visible', callback._wrapper);
      } else {
        throw new Error('cannot unregister event dispatch listener without _wrapper reference');
      }
    };
    _proto.onContentResolverEvent = function onContentResolverEvent(callback) {
      var wrapper = function wrapper(data) {
        callback.call({}, data);
      };
      callback._wrapper = wrapper.bind(this);
      EventDispatcher$1.register('after:analytics-content-resolver-track', callback._wrapper);
    };
    _proto.offContentResolverEvent = function offContentResolverEvent(callback) {
      if (callback._wrapper) {
        EventDispatcher$1.unregister('after:analytics-content-resolver-track', callback._wrapper);
      } else {
        throw new Error('cannot unregister event dispatch listener without _wrapper reference');
      }
    };
    _proto.onForwardAnalyticsEvent = function onForwardAnalyticsEvent(callback) {
      var wrapper = function wrapper(eventType, eventData) {
        callback.call({}, eventType, eventData);
      };
      callback._wrapper = wrapper.bind(this);
      EventDispatcher$1.register('after:analytics-forward-event', callback._wrapper);
    };
    return HostApi;
  }();
  var HostApi$1 = new HostApi();

  var DLGID_PREFIX = 'ap-dialog-';
  var DIALOG_CLASS = 'ap-aui-dialog2';
  var DLGID_REGEXP = new RegExp("^" + DLGID_PREFIX + "[0-9A-Za-z]+$");
  var DIALOG_SIZES = ['small', 'medium', 'large', 'xlarge', 'fullscreen', 'maximum'];
  var DIALOG_BUTTON_CLASS = 'ap-aui-dialog-button';
  var DIALOG_BUTTON_CUSTOM_CLASS = 'ap-dialog-custom-button';
  var DIALOG_FOOTER_CLASS = 'aui-dialog2-footer';
  var DIALOG_FOOTER_ACTIONS_CLASS = 'aui-dialog2-footer-actions';
  var DIALOG_HEADER_ACTIONS_CLASS = 'header-control-panel';
  function getActiveDialog() {
    var $el = AJS.LayerManager.global.getTopLayer();
    if ($el && DLGID_REGEXP.test($el.attr('id'))) {
      var dialog = AJS.dialog2($el);
      dialog._id = dialog.$el.attr('id').replace(DLGID_PREFIX, '');
      return dialog;
    }
  }
  function getActionBar($dialog) {
    var $actionBar = $dialog.find('.' + DIALOG_HEADER_ACTIONS_CLASS);
    if (!$actionBar.length) {
      $actionBar = $dialog.find('.' + DIALOG_FOOTER_ACTIONS_CLASS);
    }
    return $actionBar;
  }
  function getButtonByIdentifier(id, $dialog) {
    var $actionBar = getActionBar($dialog);
    return $actionBar.find('.aui-button').filter(function () {
      return ButtonComponent.getIdentifier(this) === id;
    });
  }
  var Dialog$1 = /*#__PURE__*/function () {
    function Dialog() {}
    var _proto = Dialog.prototype;
    _proto._renderHeaderCloseBtn = function _renderHeaderCloseBtn() {
      var $close = $('<a />').addClass('aui-dialog2-header-close');
      var $closeBtn = $('<span />').addClass('aui-icon aui-icon-small aui-iconfont-close-dialog').text('Close');
      $close.append($closeBtn);
      return $close;
    }
    //v3 ask DT about this DOM.
    ;
    _proto._renderFullScreenHeader = function _renderFullScreenHeader($header, options) {
      var $titleContainer = $('<div />').addClass('header-title-container aui-item expanded');
      var $title = $('<div />').append($('<span />').addClass('header-title').text(options.header || ''));
      $titleContainer.append($title);
      $header.append($titleContainer).append(this._renderHeaderActions(options.actions, options.extension));
      return $header;
    };
    _proto._renderHeader = function _renderHeader(options) {
      var $header = $('<header />').addClass('aui-dialog2-header');
      if (options.size === 'fullscreen') {
        return this._renderFullScreenHeader($header, options);
      }
      if (options.header) {
        var $title = $('<h2 />').addClass('aui-dialog2-header-main').text(options.header);
        $header.append($title);
      }
      $header.append(this._renderHeaderCloseBtn());
      return $header;
    };
    _proto._renderHeaderActions = function _renderHeaderActions(actions, extension) {
      var $headerControls = $('<div />').addClass('aui-item ' + DIALOG_HEADER_ACTIONS_CLASS);
      actions[0].additionalClasses = ['aui-icon', 'aui-icon-small', 'aui-iconfont-success'];
      actions[1].additionalClasses = ['aui-icon', 'aui-icon-small', 'aui-iconfont-close-dialog'];
      var $actions = this._renderActionButtons(actions, extension);
      $actions.forEach(function ($action) {
        $headerControls.append($action);
      });
      return $headerControls;
    };
    _proto._renderContent = function _renderContent($content) {
      var $el = $('<div />').addClass('aui-dialog2-content');
      if ($content) {
        $el.append($content);
      }
      return $el;
    };
    _proto._renderFooter = function _renderFooter(options) {
      var $footer = $('<footer />').addClass(DIALOG_FOOTER_CLASS);
      if (options.size !== 'fullscreen') {
        var $actions = this._renderFooterActions(options.actions, options.extension);
        $footer.append($actions);
      }
      if (options.hint) {
        var $hint = $('<div />').addClass('aui-dialog2-footer-hint').text(options.hint);
        $footer.append($hint);
      }
      return $footer;
    };
    _proto._renderActionButtons = function _renderActionButtons(actions, extension) {
      var _this = this;
      var actionButtons = [];
      [].concat(actions).forEach(function (action) {
        actionButtons.push(_this._renderDialogButton({
          text: action.text,
          name: action.name,
          type: action.type,
          additionalClasses: action.additionalClasses,
          custom: action.custom || false,
          identifier: action.identifier,
          immutable: action.immutable,
          disabled: action.disabled || false
        }, extension));
      });
      return actionButtons;
    };
    _proto._renderFooterActions = function _renderFooterActions(actions, extension) {
      var $actions = $('<div />').addClass(DIALOG_FOOTER_ACTIONS_CLASS);
      var $buttons = this._renderActionButtons(actions, extension);
      $buttons.forEach(function ($button) {
        $actions.append($button);
      });
      return $actions;
    };
    _proto._renderDialogButton = function _renderDialogButton(options, extension) {
      options.additionalClasses = options.additionalClasses || [];
      options.additionalClasses.push(DIALOG_BUTTON_CLASS);
      if (options.custom) {
        options.additionalClasses.push(DIALOG_BUTTON_CUSTOM_CLASS);
      }
      var $button = ButtonComponent.render(options);
      $button.extension = extension;
      return $button;
    }

    /**
    {
      id: 'some-dialog-id',
      title: 'some header',
      hint: 'some footer hint',
      $content: $(<div />).text('my content'),
      actions: []
    }
    **/;
    _proto.render = function render(options) {
      var originalOptions = Util.extend({}, options);
      var sanitizedOptions = dialogUtilsInstance.sanitizeOptions(options);
      var $dialog = $('<section />').attr({
        role: 'dialog',
        id: DLGID_PREFIX + sanitizedOptions.id
      });
      $dialog.attr('data-aui-modal', 'true');
      $dialog.data({
        'aui-remove-on-hide': true,
        'extension': sanitizedOptions.extension
      });
      $dialog.addClass('aui-layer aui-dialog2 ' + DIALOG_CLASS);
      if (DIALOG_SIZES.indexOf(sanitizedOptions.size) >= 0) {
        $dialog.addClass('aui-dialog2-' + sanitizedOptions.size);
      }
      if (sanitizedOptions.size === 'fullscreen' || sanitizedOptions.size === 'maximum') {
        if (sanitizedOptions.chrome) {
          $dialog.addClass('ap-header-controls');
        }
        $dialog.addClass('aui-dialog2-maximum');
      }
      $dialog.append(this._renderContent(sanitizedOptions.$content));
      if (sanitizedOptions.chrome) {
        $dialog.prepend(this._renderHeader({
          header: sanitizedOptions.header,
          actions: sanitizedOptions.actions,
          size: sanitizedOptions.size
        }));
        $dialog.append(this._renderFooter({
          extension: sanitizedOptions.extension,
          actions: sanitizedOptions.actions,
          hint: sanitizedOptions.hint,
          size: sanitizedOptions.size
        }));
      } else {
        $dialog.addClass('aui-dialog2-chromeless');
      }
      var dialog = AJS.dialog2($dialog);
      dialog._id = sanitizedOptions.id;
      if (sanitizedOptions.size === 'fullscreen') {
        sanitizedOptions.height = sanitizedOptions.width = '100%';
      }
      if (!sanitizedOptions.size || sanitizedOptions.size === 'fullscreen') {
        AJS.layer($dialog).changeSize(sanitizedOptions.width, sanitizedOptions.height);
      }
      if (sanitizedOptions.onHide) {
        dialog.on('hide', sanitizedOptions.onHide);
      }
      dialog.show();
      dialog.$el.data('extension', sanitizedOptions.extension);
      dialog.$el.data('originalOptions', originalOptions);
      return $dialog;
    };
    _proto.setIframeDimensions = function setIframeDimensions($iframe) {
      IframeComponent.resize('100%', '100%', $iframe);
    };
    _proto.getActive = function getActive() {
      return getActiveDialog();
    };
    _proto.buttonIsEnabled = function buttonIsEnabled(identifier) {
      var dialog = getActiveDialog();
      if (dialog) {
        var $button = getButtonByIdentifier(identifier, dialog.$el);
        return ButtonComponent.isEnabled($button);
      }
    };
    _proto.buttonIsVisible = function buttonIsVisible(identifier) {
      var dialog = getActiveDialog();
      if (dialog) {
        var $button = getButtonByIdentifier(identifier, dialog.$el);
        return ButtonComponent.isVisible($button);
      }
    }

    /**
    * takes either a target spec or a filter function
    * returns all matching dialogs
    */;
    _proto.getByExtension = function getByExtension(extension) {
      var filterFunction;
      if (typeof extension === 'function') {
        filterFunction = extension;
      } else {
        var keys = Object.getOwnPropertyNames(extension);
        filterFunction = function filterFunction(dialog) {
          var dialogData = $(dialog).data('extension');
          return keys.every(function (key) {
            return dialogData[key] === extension[key];
          });
        };
      }
      return $('.' + DIALOG_CLASS).toArray().filter(filterFunction).map(function ($el) {
        return AJS.dialog2($el);
      });
    }

    // add user defined button to an existing dialog
    ;
    _proto.addButton = function addButton(extension, options) {
      options.custom = true;
      var $button = this._renderDialogButton(options, extension);
      var $dialog = this.getByExtension({
        addon_key: extension.addon_key,
        key: extension.key
      })[0].$el;
      var $actionBar = getActionBar($dialog);
      $actionBar.append($button);
      return $dialog;
    };
    return Dialog;
  }();
  var DialogComponent = new Dialog$1();
  EventDispatcher$1.register('iframe-bridge-established', function (data) {
    if (data.extension.options.isDialog) {
      var callback;
      var frameworkAdaptor = HostApi$1.getFrameworkAdaptor();
      var dialogProvider = frameworkAdaptor.getProviderByModuleName('dialog');
      if (dialogProvider) {
        callback = dialogProvider.close;
        dialogProvider.setButtonDisabled('submit', false);
      } else {
        DialogActions.toggleButton({
          identifier: 'submit',
          enabled: true
        });
        callback = function callback() {
          DialogActions.close({
            dialog: getActiveDialog(),
            extension: data.extension
          });
        };
      }
      if (!data.extension.options.preventDialogCloseOnEscape) {
        DomEventActions.registerKeyEvent({
          extension_id: data.extension.id,
          key: 27,
          callback: callback
        });
        EventDispatcher$1.registerOnce('dialog-close', function (d) {
          DomEventActions.unregisterKeyEvent({
            extension_id: data.extension.id,
            key: 27
          });
        });
      }
    }
  });
  EventDispatcher$1.register('dialog-close-active', function (data) {
    var activeDialog = getActiveDialog();
    if (activeDialog) {
      DialogActions.close({
        customData: data.customData,
        dialog: activeDialog,
        extension: data.extension
      });
    }
  });
  EventDispatcher$1.register('dialog-close', function (data) {
    if (data.dialog) {
      data.dialog.hide();
    }
  });
  EventDispatcher$1.register('dialog-button-toggle', function (data) {
    var dialog = getActiveDialog();
    if (dialog) {
      var $button = getButtonByIdentifier(data.identifier, dialog.$el);
      ButtonActions.toggle($button, !data.enabled);
    }
  });
  EventDispatcher$1.register('dialog-button-toggle-visibility', function (data) {
    var dialog = getActiveDialog();
    if (dialog) {
      var $button = getButtonByIdentifier(data.identifier, dialog.$el);
      ButtonActions.toggleVisibility($button, data.hidden);
    }
  });
  EventDispatcher$1.register('button-clicked', function (data) {
    var $button = data.$el;
    if ($button.hasClass(DIALOG_BUTTON_CLASS)) {
      var $dialog = $button.parents('.' + DIALOG_CLASS);
      var $iframe = $dialog.find('iframe');
      if ($iframe.length && $iframe[0].bridgeEstablished) {
        DialogActions.clickButton(ButtonComponent.getIdentifier($button), $button, $dialog.data('extension'));
      } else {
        DialogActions.close({
          dialog: getActiveDialog(),
          extension: $button.extension
        });
      }
    }
  });
  if ($.fn) {
    EventDispatcher$1.register('iframe-create', function (data) {
      if (data.extension.options && data.extension.options.isDialog) {
        DialogComponent.setIframeDimensions(data.extension.$el);
      }
    });
    EventDispatcher$1.register('dialog-button-add', function (data) {
      DialogComponent.addButton(data.extension, data.button);
    });
    EventDispatcher$1.register('host-window-resize', Util.debounce(function () {
      $('.' + DIALOG_CLASS).each(function (i, dialog) {
        var $dialog = $(dialog);
        var sanitizedOptions = dialogUtilsInstance.sanitizeOptions($dialog.data('originalOptions'));
        dialog.style.width = sanitizedOptions.width;
        dialog.style.height = sanitizedOptions.height;
      });
    }, 100));
  }
  DomEventActions.registerWindowKeyEvent({
    keyCode: 27,
    callback: function callback() {
      DialogActions.closeActive({
        customData: {},
        extension: null
      });
    }
  });

  function getAccessNarrowingSpaceContext(extension) {
    var _AJS;
    if (extension != null && extension.options && (_AJS = AJS) != null && (_AJS = _AJS.Meta) != null && _AJS.get) {
      var productContext = _extends({}, extension.options.productContext || {});
      if (AJS.Meta.get('space-key')) {
        productContext['an.spaceKey'] = AJS.Meta.get('space-key');
      }
      return _extends({}, extension, {
        options: _extends({}, extension.options, {
          productContext: productContext
        })
      });
    }
    return extension;
  }

  var DialogExtension = /*#__PURE__*/function () {
    function DialogExtension() {}
    var _proto = DialogExtension.prototype;
    _proto.render = function render(extension, dialogOptions) {
      extension.options = extension.options || {};
      dialogOptions = dialogOptions || {};
      extension.options.isDialog = true;
      extension.options.dialogId = dialogOptions.id;
      extension.options.callbackExtensionId = extension.callback_extension_id;
      extension.options.preventDialogCloseOnEscape = dialogOptions.closeOnEscape === false;
      extension.options.hostFrameOffset = dialogOptions.hostFrameOffset;
      extension.options.hideIframeUntilLoad = true;
      var $iframeContainer = IframeContainerComponent.createExtension(extension);
      var $dialog = DialogComponent.render({
        extension: extension,
        $content: $iframeContainer,
        chrome: dialogOptions.chrome,
        width: dialogOptions.width,
        height: dialogOptions.height,
        size: dialogOptions.size,
        header: dialogOptions.header,
        hint: dialogOptions.hint,
        submitText: dialogOptions.submitText,
        cancelText: dialogOptions.cancelText,
        buttons: dialogOptions.buttons,
        onHide: dialogOptions.onHide
      });
      return $dialog;
    };
    _proto.getActiveDialog = function getActiveDialog() {
      return DialogComponent.getActive();
    };
    _proto.buttonIsEnabled = function buttonIsEnabled(identifier) {
      return DialogComponent.buttonIsEnabled(identifier);
    };
    _proto.buttonIsVisible = function buttonIsVisible(identifier) {
      return DialogComponent.buttonIsVisible(identifier);
    };
    _proto.getByExtension = function getByExtension(extension) {
      if (typeof extension === 'string') {
        extension = {
          id: extension
        };
      }
      return DialogComponent.getByExtension(extension);
    };
    return DialogExtension;
  }();
  var DialogExtensionComponent = new DialogExtension();
  EventDispatcher$1.register('dialog-extension-open', function (data) {
    var dialogExtension = getAccessNarrowingSpaceContext(data.extension);
    var dialogOptions = dialogUtilsInstance.sanitizeOptions(data.options);
    var frameworkAdaptor = HostApi$1.getFrameworkAdaptor();
    var dialogProvider = frameworkAdaptor.getProviderByModuleName('dialog');
    if (dialogProvider) {
      // this function should move.
      var getOnClickFunction = function getOnClickFunction(action) {
        var key = dialogExtension.key;
        var addon_key = dialogExtension.addon_key;
        var eventData = {
          button: {
            identifier: action.identifier,
            name: action.identifier,
            text: action.text
          }
        };
        if (['submit', 'cancel'].indexOf(action.identifier) >= 0) {
          EventActions.broadcast("dialog." + action.identifier, {
            addon_key: addon_key,
            key: key
          }, eventData);
        }
        EventActions.broadcast('dialog.button.click', {
          addon_key: addon_key,
          key: key
        }, eventData);
      };
      dialogExtension.options.preventDialogCloseOnEscape = dialogOptions.closeOnEscape === false;
      dialogOptions.actions.map(function (action) {
        return action.onClick = getOnClickFunction.bind(null, action);
      });
      dialogProvider.create(dialogOptions, dialogExtension);
    } else {
      DialogExtensionComponent.render(dialogExtension, data.options);
    }
  });

  var _dialogs = {};
  EventDispatcher$1.register('dialog-close', function (data) {
    var dialog = data.dialog;
    if (dialog && data.extension) {
      var targetSpec = {
        addon_key: data.extension.addon_key,
        id: data.extension.options.callbackExtensionId
      };
      EventActions.broadcast('dialog.close', targetSpec, data.customData);
    }
  });
  EventDispatcher$1.register('dialog-button-click', function (data) {
    var eventData = {
      button: {
        name: ButtonComponent.getName(data.$el),
        identifier: ButtonComponent.getIdentifier(data.$el),
        text: ButtonComponent.getText(data.$el)
      }
    };
    var eventName = 'dialog.button.click';
    var buttonEventFilter = {
      addon_key: data.extension.addon_key,
      key: data.extension.key
    };
    buttonEventFilter.id = data.extension.id;

    // Old buttons, (submit and cancel) use old events
    if (!data.$el.hasClass('ap-dialog-custom-button')) {
      EventActions.broadcast("dialog." + eventData.button.name, buttonEventFilter, eventData);
    }
    EventActions.broadcast(eventName, buttonEventFilter, eventData);
  });

  /**
   * @class Dialog~Dialog
   * @description A dialog object that is returned when a dialog is created using the [dialog module](module-Dialog.html).
   */
  var Dialog = function Dialog(options, callback) {
    callback = Util.last(arguments);
    var _id = callback._id;
    var extension = callback._context.extension;
    var dialogExtension = {
      addon_key: extension.addon_key,
      key: options.key,
      options: Util.pick(extension.options, ['customData', 'productContext']),
      callback_extension_id: extension.id
    };

    // ACJS-185: the following is a really bad idea but we need it
    // for compat until AP.dialog.customData has been deprecated
    dialogExtension.options.customData = options.customData;
    // terrible idea! - we need to remove this from p2 ASAP!
    var dialogModuleOptions = dialogUtilsInstance.moduleOptionsFromGlobal(dialogExtension.addon_key, dialogExtension.key);

    // There is a hostFrameOffset configuration available
    // for modals (window._AP.dialogOptions) and inline modals (window._AP.inlineDialogOptions)
    // which is taken into account during the iframe insertion (inside the dialog).
    // The change below injects hostFrameOffset value from the global module options (window._AP.dialogModules)
    // which is required for establishing a contact with a correct host (solves spa iframe problem).
    if (typeof (dialogModuleOptions || {}).hostFrameOffset === 'number') {
      dialogExtension.options.hostFrameOffset = dialogModuleOptions.hostFrameOffset;
    }
    options = Util.extend({}, dialogModuleOptions || {}, options);
    options.id = _id;
    dialogUtilsInstance.trackMultipleDialogOpening(dialogExtension, options);
    DialogExtensionActions.open(dialogExtension, options);
    this.customData = options.customData;
    _dialogs[_id] = this;
  }

  /**
   * Registers a callback for a dialog event.
   * @method on
   * @memberOf Dialog~Dialog
   * @param {String} event The dialog event to listen for. Valid options are "close".
   * @param {Function} callback The function to be invoked.
   * @noDemo
   * @example
   * AP.dialog.create({
   *   key: 'my-module-key'
   * }).on("close", function() {
   *   console.log("Dialog was closed");
   * });
   */;
  /**
   * @class Dialog~DialogButton
   * @description A dialog button that can be controlled with JavaScript
   */
  var Button = /*#__PURE__*/function () {
    function Button(identifier, callback) {
      callback = Util.last(arguments);
      var frameworkAdaptor = HostApi$1.getFrameworkAdaptor();
      var dialogProvider = frameworkAdaptor.getProviderByModuleName('dialog');
      if (dialogProvider) {
        dialogUtilsInstance.assertActiveDialogOrThrow(dialogProvider, callback._context.extension.addon_key);
        this.name = identifier;
        this.identifier = identifier;
      } else {
        if (!DialogExtensionComponent.getActiveDialog()) {
          throw new Error('Failed to find an active dialog.');
        }
        this.name = identifier;
        this.identifier = identifier;
        this.enabled = DialogExtensionComponent.buttonIsEnabled(identifier);
        this.hidden = !DialogExtensionComponent.buttonIsVisible(identifier);
      }
    }
    /**
     * Sets the button state to enabled
     * @method enable
     * @memberOf Dialog~DialogButton
     * @noDemo
     * @example
     * AP.dialog.getButton('submit').enable();
     */
    var _proto = Button.prototype;
    _proto.enable = function enable() {
      this.setState({
        enabled: true
      });
    }
    /**
     * Sets the button state to disabled. A disabled button cannot be clicked and emits no events.
     * @method disable
     * @memberOf Dialog~DialogButton
     * @noDemo
     * @example
     * AP.dialog.getButton('submit').disable();
     */;
    _proto.disable = function disable() {
      this.setState({
        enabled: false
      });
    }
    /**
     * Query a button for its current state.
     * @method isEnabled
     * @memberOf Dialog~DialogButton
     * @param {Function} callback function to receive the button state.
     * @noDemo
     * @example
     * AP.dialog.getButton('submit').isEnabled(function(enabled){
     *   if(enabled){
     *     //button is enabled
     *   }
     * });
     */;
    _proto.isEnabled = function isEnabled(callback) {
      callback = Util.last(arguments);
      var frameworkAdaptor = HostApi$1.getFrameworkAdaptor();
      var dialogProvider = frameworkAdaptor.getProviderByModuleName('dialog');
      if (dialogProvider) {
        callback(!dialogProvider.isButtonDisabled(this.identifier));
      } else {
        callback(this.enabled);
      }
    }
    /**
     * Toggle the button state between enabled and disabled.
     * @method toggle
     * @memberOf Dialog~DialogButton
     * @noDemo
     * @example
     * AP.dialog.getButton('submit').toggle();
     */;
    _proto.toggle = function toggle() {
      var frameworkAdaptor = HostApi$1.getFrameworkAdaptor();
      var dialogProvider = frameworkAdaptor.getProviderByModuleName('dialog');
      if (dialogProvider) {
        dialogProvider.toggleButton(this.identifier);
      } else {
        this.setState({
          enabled: !this.enabled
        });
      }
    };
    _proto.setState = function setState(state) {
      var frameworkAdaptor = HostApi$1.getFrameworkAdaptor();
      var dialogProvider = frameworkAdaptor.getProviderByModuleName('dialog');
      if (dialogProvider) {
        dialogProvider.setButtonDisabled(this.identifier, !state.enabled);
      } else {
        this.enabled = state.enabled;
        DialogActions.toggleButton({
          identifier: this.identifier,
          enabled: this.enabled
        });
      }
    }
    /**
     * Trigger a callback bound to a button.
     * @method trigger
     * @memberOf Dialog~DialogButton
     * @noDemo
     * @example
     * AP.dialog.getButton('submit').bind(function(){
     *   alert('clicked!');
     * });
     * AP.dialog.getButton('submit').trigger();
     */;
    _proto.trigger = function trigger(callback) {
      callback = Util.last(arguments);
      if (this.enabled) {
        DialogActions.dialogMessage({
          name: this.name,
          extension: callback._context.extension
        });
      }
    }

    /**
     * Query a button for its current hidden/visible state.
     * @method isHidden
     * @memberOf Dialog~DialogButton
     * @param {Function} callback function to receive the button state.
     * @noDemo
     * @example
     * AP.dialog.getButton('submit').isHidden(function(hidden){
     *   if(hidden){
     *     //button is hidden
     *   }
     * });
     */;
    _proto.isHidden = function isHidden(callback) {
      callback = Util.last(arguments);
      var frameworkAdaptor = HostApi$1.getFrameworkAdaptor();
      var dialogProvider = frameworkAdaptor.getProviderByModuleName('dialog');
      if (dialogProvider) {
        callback(dialogProvider.isButtonHidden(this.identifier));
      } else {
        callback(this.hidden);
      }
    }
    /**
     * Sets the button state to hidden
     * @method hide
     * @memberOf Dialog~DialogButton
     * @noDemo
     * @example
     * AP.dialog.getButton('submit').hide();
     */;
    _proto.hide = function hide() {
      this.setHidden(true);
    }
    /**
     * Sets the button state to visible
     * @method show
     * @memberOf Dialog~DialogButton
     * @noDemo
     * @example
     * AP.dialog.getButton('submit').show();
     */;
    _proto.show = function show() {
      this.setHidden(false);
    };
    _proto.setHidden = function setHidden(hidden) {
      var frameworkAdaptor = HostApi$1.getFrameworkAdaptor();
      var dialogProvider = frameworkAdaptor.getProviderByModuleName('dialog');
      if (dialogProvider) {
        dialogProvider.setButtonHidden(this.identifier, hidden);
      } else {
        this.hidden = hidden;
        DialogActions.toggleButtonVisibility({
          identifier: this.identifier,
          hidden: this.hidden
        });
      }
    };
    return Button;
  }();
  function getDialogFromContext(context) {
    return _dialogs[context.extension.options.dialogId];
  }
  var CreateButton = function CreateButton(options, callback) {
    callback = Util.last(arguments);
    var frameworkAdaptor = HostApi$1.getFrameworkAdaptor();
    var dialogProvider = frameworkAdaptor.getProviderByModuleName('dialog');
    if (dialogProvider) {
      dialogUtilsInstance.assertActiveDialogOrThrow(dialogProvider, callback._context.extension.addon_key);
      dialogProvider.createButton({
        identifier: options.identifier,
        text: options.text,
        hidden: false,
        disabled: options.disabled || false,
        onClick: function onClick() {
          EventActions.broadcast('dialog.button.click', {
            addon_key: callback._context.extension.addon_key,
            key: callback._context.extension.key
          }, {
            button: {
              identifier: options.identifier,
              text: options.text
            }
          });
        }
      });
    } else {
      DialogExtensionActions.addUserButton({
        identifier: options.identifier,
        text: options.text
      }, callback._context.extension);
    }
  };
  /**
   * The Dialog module provides a mechanism for launching an add-on's modules as modal dialogs from within an add-on's iframe.
   *
   * A modal dialog displays information without requiring the user to leave the current page.
   *
   * The dialog is opened over the entire window, rather than within the iframe itself.
   *
   * <h3>Styling your dialog to look like a standard Atlassian dialog</h3>
   *
   * By default the dialog iframe is undecorated. It's up to you to style the dialog.
   * <img src="/cloud/connect/images/connectdialogchromelessexample.jpeg" width="100%" />
   *
   * In order to maintain a consistent look and feel between the host application and the add-on, we encourage you to style your dialogs to match Atlassian's Design Guidelines for modal dialogs.
   *
   * To do that, you'll need to add the AUI styles to your dialog.
   *
   * For more information, read about the Atlassian User Interface [dialog component](https://docs.atlassian.com/aui/latest/docs/dialog2.html).
   * @exports Dialog
   */
  var dialog = {
    /**
     * @class Dialog~DialogOptions
     * @description The options supplied to a [dialog.create()](module-Dialog.html) call.
     *
     * @property {String}        key         The module key of a dialog, or the key of a page or web-item that you want to open as a dialog.
     * @property {String}        size        Opens the dialog at a preset size: small, medium, large, x-large or fullscreen (with chrome).
     * @property {Number|String} width       if size is not set, define the width as a percentage (append a % to the number) or pixels.
     * @property {Number|String} height      if size is not set, define the height as a percentage (append a % to the number) or pixels.
     * @property {Boolean}       chrome      (optional) opens the dialog with heading and buttons.
     * @property {String}        header      (optional) text to display in the header if opening a dialog with chrome.
     * @property {String}        submitText  (optional) text for the submit button if opening a dialog with chrome.
     * @property {String}        cancelText  (optional) text for the cancel button if opening a dialog with chrome.
     * @property {Object}        customData  (optional) custom data object that can be accessed from the actual dialog iFrame.
     * @property {Boolean}       closeOnEscape (optional) if true, pressing ESC inside the dialog will close the dialog (default is true).
     * @property {Array}         buttons     (optional) an array of custom buttons to be added to the dialog if opening a dialog with chrome.
     * @property {String}        hint        (optional) Suggested actions or helpful info that will be added to the dialog if opening with chrome.
     */

    /**
     * Creates a dialog for a common dialog, page or web-item module key.
     * @param {Dialog~DialogOptions} options configuration object of dialog options.
     * @method create
     * @noDemo
     * @example
     * AP.dialog.create({
     *   key: 'my-module-key',
     *   width: '500px',
     *   height: '200px',
     *   chrome: true,
     *   buttons: [
     *     {
     *       text: 'my button',
     *       identifier: 'my_unique_identifier'
     *     }
     *   ]
     * }).on("close", callbackFunc);
     *
     * @return {Dialog~Dialog} Dialog object allowing for callback registrations
     */
    create: {
      constructor: Dialog
    },
    /**
     * Closes the currently open dialog. Optionally pass data to listeners of the `dialog.close` event.
     * This will only close a dialog that has been opened by your add-on.
     * You can register for close events using the `dialog.close` event and the [events module](../events/).
     * @param {Object} data An object to be emitted on dialog close.
     * @noDemo
     * @example
     * AP.dialog.close({foo: 'bar'});
     */
    close: function close(data, callback) {
      callback = Util.last(arguments);
      var frameworkAdaptor = HostApi$1.getFrameworkAdaptor();
      var dialogProvider = frameworkAdaptor.getProviderByModuleName('dialog');
      if (dialogProvider) {
        dialogUtilsInstance.assertActiveDialogOrThrow(dialogProvider, callback._context.extension.addon_key);
        EventActions.broadcast('dialog.close', {
          addon_key: callback._context.extension.addon_key
        }, data);
        dialogProvider.close();
      } else {
        var dialogToClose;
        if (callback._context.extension.options.isDialog) {
          dialogToClose = DialogExtensionComponent.getByExtension(callback._context.extension.id)[0];
        } else {
          dialogToClose = DialogExtensionComponent.getActiveDialog();
        }
        DialogActions.close({
          customData: data,
          dialog: dialogToClose,
          extension: callback._context.extension
        });
      }
    },
    /**
     * Passes the custom data Object to the specified callback function.
     * @noDemo
     * @name getCustomData
     * @method
     * @param {Function} callback - Callback method to be executed with the custom data.
     * @example
     * AP.dialog.getCustomData(function (customData) {
     *   console.log(customData);
     * });
     *
     */
    getCustomData: function getCustomData(callback) {
      callback = Util.last(arguments);
      var dialog = getDialogFromContext(callback._context);
      if (dialog) {
        callback(dialog.customData);
      } else {
        callback(undefined);
      }
    },
    /**
    * Stop the dialog from closing when the submit button is clicked
    * @method disableCloseOnSubmit
    * @noDemo
    * @example
    * AP.dialog.disableCloseOnSubmit();
    * AP.events.on('dialog.button.click', function(data){
    *   if(data.button.name === 'submit') {
    *     console.log('submit button pressed');
    *   }
    * }
    */

    /**
     * Returns the button that was requested (either cancel or submit). If the requested button does not exist, an empty Object will be returned instead.
     * @method getButton
     * @returns {Dialog~DialogButton}
     * @noDemo
     * @example
     * AP.dialog.getButton('submit');
     */
    getButton: {
      constructor: Button,
      enable: Button.prototype.enable,
      disable: Button.prototype.disable,
      toggle: Button.prototype.toggle,
      isEnabled: Button.prototype.isEnabled,
      trigger: Button.prototype.trigger,
      hide: Button.prototype.hide,
      show: Button.prototype.show,
      isHidden: Button.prototype.isHidden
    },
    /**
     * Creates a dialog button that can be controlled with javascript
     * @method createButton
     * @returns {Dialog~DialogButton}
     * @noDemo
     * @example
     * AP.dialog.createButton({
     *   text: 'button text',
     *   identifier: 'button.1'
     * }).bind(function mycallback(){});
     */
    createButton: {
      constructor: CreateButton
    }
  };

  EventDispatcher$1.register('iframe-resize', function (data) {
    IframeComponent.resize(data.width, data.height, data.$el);
  });
  EventDispatcher$1.register('iframe-size-to-parent', function (data) {
    var height;
    var $el = Util.getIframeByExtensionId(data.extensionId);
    height = $(window).height() - $el.offset().top - 1 + 'px'; //1px comes from margin given by full-size-general-page
    // When in unit test (env_module_spec.js), $el.get(0).name is an empty string, breaking the test. Thus doing this check.
    if ($el.get(0).name.length > 0 && JSON.parse($el.get(0).name).options.moduleType === 'jiraProjectPages') {
      height = '100%';
    }
    EventDispatcher$1.dispatch('iframe-resize', {
      width: '100%',
      height: height,
      $el: $el
    });
  });
  EventDispatcher$1.register('hide-footer', function (hideFooter) {
    if (hideFooter) {
      $('#footer').css({
        display: 'none'
      });
    }
  });
  window.addEventListener('resize', function (e) {
    EventDispatcher$1.dispatch('host-window-resize', e);
  }, true);
  var EnvActions = {
    iframeResize: function iframeResize(width, height, context) {
      var $el;
      if (context.extension_id) {
        $el = Util.getIframeByExtensionId(context.extension_id);
      } else {
        $el = context;
      }
      EventDispatcher$1.dispatch('iframe-resize', {
        width: width,
        height: height,
        $el: $el,
        extension: context.extension
      });
    },
    sizeToParent: function sizeToParent(extensionId, hideFooter) {
      EventDispatcher$1.dispatch('iframe-size-to-parent', {
        hideFooter: hideFooter,
        extensionId: extensionId
      });
    },
    hideFooter: function hideFooter(_hideFooter) {
      EventDispatcher$1.dispatch('hide-footer', _hideFooter);
    }
  };

  var debounce = Util.debounce;
  var resizeFuncHolder = {};
  // ignore resize events for iframes that use sizeToParent
  var ignoreResizeForExtension = [];
  var sizeToParentExtension = {};

  /**
   * Enables apps to resize their iframes.
   * @exports iframe
   */
  var env = {
    /**
     * Get the location of the current page of the host product.
     *
     * @param {Function} callback function (location) {...} The callback to pass the location to.
     * @example
     * AP.getLocation(function(location){
     *   alert(location);
     * });
     */
    getLocation: function getLocation(callback) {
      callback = Util.last(arguments);
      var pageLocationProvider = ModuleProviders$1.getProvider('get-location');
      if (typeof pageLocationProvider === 'function') {
        callback(pageLocationProvider());
      } else {
        callback(window.location.href);
      }
    },
    /**
     * Resize the iframe to a width and height.
     *
     * Only content within an element with the class `ac-content` is resized automatically.
     * Content without this identifier is sized according to the `body` element, and
     * is *not* dynamically resized. The recommended DOM layout for your app is:
     *
     * ``` html
     * <div class="ac-content">
     *     <p>Hello World</p>
     *     <div id="your-id-here">
     *         <p>App content goes here</p>
     *     </div>
     *
     *     ...this area reserved for the resize sensor divs
     * </div>
     * ```
     *
     * The resize sensor div is added on the iframe's [load event](https://developer.mozilla.org/en-US/docs/Web/API/Window/load_event).
     * Removing the `ac-content` element after this, prevents resizing from working correctly.
     *
     * This method cannot be used in dialogs.
     *
     * @method
     * @param {String} width   The desired width in pixels or percentage.
     * @param {String} height  The desired height in pixels or percentage.
     * @example
     * AP.resize('400','400');
     */
    resize: function resize(width, height, callback) {
      callback = Util.last(arguments);
      var addon = ModuleProviders$1.getProvider('addon');
      if (addon) {
        addon.resize(width, height, callback._context);
      } else {
        var iframeId = callback._context.extension.id;
        var options = callback._context.extension.options;
        if (ignoreResizeForExtension.indexOf(iframeId) !== -1 || options && options.isDialog) {
          return false;
        }
        if (!resizeFuncHolder[iframeId]) {
          resizeFuncHolder[iframeId] = debounce(function (dwidth, dheight, dcallback) {
            EnvActions.iframeResize(dwidth, dheight, dcallback._context);
          }, 50);
        }
        resizeFuncHolder[iframeId](width, height, callback);
      }
      return true;
    },
    /**
     * Resize the iframe so that it takes up the entire page.
     *
     * This method is only available for general page modules.
     *
     * @method
     * @example
     * AP.sizeToParent();
     */
    sizeToParent: debounce(function (hideFooter, callback) {
      callback = Util.last(arguments);
      var addon = ModuleProviders$1.getProvider('addon');
      if (addon) {
        addon.sizeToParent(hideFooter, callback._context);
      } else {
        // sizeToParent is only available for general-pages
        if (callback._context.extension.options.isFullPage) {
          // This adds border between the iframe and the page footer as the connect addon has scrolling content and can't do this
          Util.getIframeByExtensionId(callback._context.extension_id).addClass('full-size-general-page');
          Util.getIframeByExtensionId(callback._context.extension_id).addClass('sized-to-parent');
          EnvActions.sizeToParent(callback._context.extension_id, hideFooter);
          sizeToParentExtension[callback._context.extension_id] = {
            hideFooter: hideFooter
          };
        } else {
          // This is only here to support integration testing
          // see com.atlassian.plugin.connect.test.pageobjects.RemotePage#isNotFullSize()
          Util.getIframeByExtensionId(callback._context.extension_id).addClass('full-size-general-page-fail');
        }
      }
    }),
    /**
     * Hide the footer.
     *
     * @method
     * @param {boolean} hideFooter Whether the footer should be hidden.
     * @ignore
     */
    hideFooter: function hideFooter(_hideFooter) {
      if (_hideFooter) {
        EnvActions.hideFooter(_hideFooter);
      }
    }
  };
  var _removeIframeReferenceAfterUnloadAndDestroyed = function _removeIframeReferenceAfterUnloadAndDestroyed(extensionId) {
    delete resizeFuncHolder[extensionId];
    delete sizeToParentExtension[extensionId];
    if (ignoreResizeForExtension.indexOf(extensionId) !== -1) {
      ignoreResizeForExtension.splice(ignoreResizeForExtension.indexOf(extensionId), 1);
    }
  };
  EventDispatcher$1.register('host-window-resize', function (data) {
    Object.getOwnPropertyNames(sizeToParentExtension).forEach(function (extensionId) {
      EnvActions.sizeToParent(extensionId, sizeToParentExtension[extensionId].hideFooter);
    });
  });
  EventDispatcher$1.register('after:iframe-unload', function (data) {
    _removeIframeReferenceAfterUnloadAndDestroyed(data.extension.id);
  });
  EventDispatcher$1.register('after:iframe-destroyed', function (data) {
    _removeIframeReferenceAfterUnloadAndDestroyed(data.extension.extension.id);
  });
  EventDispatcher$1.register('before:iframe-size-to-parent', function (data) {
    if (ignoreResizeForExtension.indexOf(data.extensionId) === -1) {
      ignoreResizeForExtension.push(data.extensionId);
    }
  });

  var InlineDialogActions = {
    hide: function hide($el) {
      EventDispatcher$1.dispatch('inline-dialog-hide', {
        $el: $el
      });
    },
    refresh: function refresh($el) {
      EventDispatcher$1.dispatch('inline-dialog-refresh', {
        $el: $el
      });
    },
    hideTriggered: function hideTriggered(extension_id, $el) {
      EventDispatcher$1.dispatch('inline-dialog-hidden', {
        extension_id: extension_id,
        $el: $el
      });
    },
    close: function close() {
      EventDispatcher$1.dispatch('inline-dialog-close', {});
    },
    created: function created(data) {
      EventDispatcher$1.dispatch('inline-dialog-opened', {
        $el: data.$el,
        trigger: data.trigger,
        extension: data.extension
      });
    }
  };

  /**
   * The inline dialog is a wrapper for secondary content/controls to be displayed on user request. Consider this component as displayed in context to the triggering control with the dialog overlaying the page content.
   * An inline dialog should be preferred over a modal dialog when a connection between the action has a clear benefit versus having a lower user focus.
   *
   * Inline dialogs can be shown via a [web item target](../../modules/web-item/#target).
   *
   * For more information, read about the Atlassian User Interface [inline dialog component](https://docs.atlassian.com/aui/latest/docs/inline-dialog.html).
   * @module Inline-dialog
   */
  var inlineDialog = {
    /**
     * Hide the inline dialog that contains the iframe where this method is called from.
     * @memberOf module:Inline-dialog
     * @method hide
     * @noDemo
     * @example
     * AP.inlineDialog.hide();
     */
    hide: function hide(callback) {
      callback = Util.last(arguments);
      var frameworkAdaptor = HostApi$1.getFrameworkAdaptor();
      var inlineDialogProvider = frameworkAdaptor.getProviderByModuleName('inlineDialog');
      if (inlineDialogProvider) {
        inlineDialogProvider.hide(callback._context);
      } else {
        InlineDialogActions.close();
      }
    }
  };

  /**
  * Messages are the primary method for providing system feedback in the product user interface.
  * Messages include notifications of various kinds: alerts, confirmations, notices, warnings, info and errors.
  * For visual examples of each kind please see the [Design guide](https://docs.atlassian.com/aui/latest/docs/messages.html).
  * ### Example ###
  * ```
  * //create a message
  * var message = AP.messages.info('plain text title', 'plain text body');
  * ```
  * @deprecated after August 2017 | Please use the Flag module instead.
  * @name Messages
  * @module
  * @ignore
  */
  var MESSAGE_BAR_ID = 'ac-message-container';
  var MESSAGE_TYPES = ['generic', 'error', 'warning', 'success', 'info', 'hint'];
  var MSGID_PREFIX = 'ap-message-';
  var MSGID_REGEXP = new RegExp("^" + MSGID_PREFIX + "[0-9A-fa-f]+$");
  var _messages = {};
  function validateMessageId(msgId) {
    return MSGID_REGEXP.test(msgId);
  }
  function getMessageBar() {
    var $msgBar = $('#' + MESSAGE_BAR_ID);
    if ($msgBar.length < 1) {
      $msgBar = $('<div id="' + MESSAGE_BAR_ID + '" />').appendTo('body');
    }
    return $msgBar;
  }
  function filterMessageOptions(options) {
    var copy = {};
    var allowed = ['closeable', 'fadeout', 'delay', 'duration', 'id'];
    if (typeof options === 'object') {
      allowed.forEach(function (key) {
        if (key in options) {
          copy[key] = options[key];
        }
      });
    }
    return copy;
  }
  var messageCloseListenerCreated = false;
  function showMessage(name, title, body, options) {
    if (!messageCloseListenerCreated) {
      createMessageCloseListener();
      messageCloseListenerCreated = true;
    }
    var $msgBar = getMessageBar();
    options = filterMessageOptions(options);
    $.extend(options, {
      title: title,
      body: AJS.escapeHtml(body)
    });
    if (MESSAGE_TYPES.indexOf(name) < 0) {
      throw 'Invalid message type. Must be: ' + MESSAGE_TYPES.join(', ');
    }
    if (validateMessageId(options.id)) {
      AJS.messages[name]($msgBar, options);
      // Calculate the left offset based on the content width.
      // This ensures the message always stays in the centre of the window.
      $msgBar.css('margin-left', '-' + $msgBar.innerWidth() / 2 + 'px');
    }
  }
  function deprecatedShowMessage(name, title, body, options, callback) {
    var methodUsed = "AP.messages." + name;
    console.warn("DEPRECATED API - AP.messages." + name + " has been deprecated since ACJS 5.0 and will be removed in a future release. Use AP.flag.create instead.");
    AnalyticsAction.trackDeprecatedMethodUsed(methodUsed, callback._context.extension);
    var frameworkAdaptor = HostApi$1.getFrameworkAdaptor();
    var messageProvider = frameworkAdaptor.getProviderByModuleName('messages');
    if (messageProvider) {
      var messageType = name;
      var createMessage = messageProvider[messageType];
      if (!createMessage) {
        messageProvider[messageType] = messageProvider.generic;
      }
      createMessage(title, body, options);
    } else {
      showMessage(name, title, body, options);
    }
  }
  function createMessageCloseListener() {
    $(document).on('aui-message-close', function (e, $msg) {
      var _id = $msg.attr('id').replace(MSGID_PREFIX, '');
      if (_messages[_id]) {
        if ($.isFunction(_messages[_id].onCloseTrigger)) {
          _messages[_id].onCloseTrigger();
        }
        _messages[_id]._destroy();
      }
    });
  }
  function messageModule(messageType) {
    return {
      constructor: function constructor(title, body, options, callback) {
        callback = Util.last(arguments);
        var _id = callback._id;
        if (typeof title !== 'string') {
          title = '';
        }
        if (typeof body !== 'string') {
          body = '';
        }
        if (typeof options !== 'object') {
          options = {};
        }
        options.id = MSGID_PREFIX + _id;
        deprecatedShowMessage(messageType, title, body, options, callback);
        _messages[_id] = this;
      }
    };
  }
  var messages = {
    /**
    * Close a message
    * @deprecated after August 2017 | Please use the Flag module instead.
    * @name clear
    * @method
    * @memberof module:Messages#
    * @param    {String}    id  The id that was returned when the message was created.
    * @example
    * //create a message
    * var message = AP.messages.info('title', 'body');
    * setTimeout(function(){
    *   AP.messages.clear(message);
    * }, 2000);
    */
    clear: function clear(msg) {
      var id = MSGID_PREFIX + msg._id;
      if (validateMessageId(id)) {
        var frameworkAdaptor = HostApi$1.getFrameworkAdaptor();
        var messageProvider = frameworkAdaptor.getProviderByModuleName('messages');
        if (messageProvider) {
          messageProvider.clear(id);
        } else {
          $('#' + id).closeMessage();
        }
      }
    },
    /**
    * Trigger an event when a message is closed
    * @deprecated after August 2017 | Please use the Flag module instead.
    * @name onClose
    * @method
    * @memberof module:Messages#
    * @param    {String}    id  The id that was returned when the message was created.
    * @param    {Function}  callback  The function that is run when the event is triggered
    * @example
    * //create a message
    * var message = AP.messages.info('title', 'body');
    * AP.messages.onClose(message, function() {
    *   console.log(message, ' has been closed!');
    * });
    */
    onClose: function onClose(msg, callback) {
      callback = Util.last(arguments);
      var id = msg._id;
      var frameworkAdaptor = HostApi$1.getFrameworkAdaptor();
      var messageProvider = frameworkAdaptor.getProviderByModuleName('messages');
      if (messageProvider) {
        var fullId = MSGID_PREFIX + msg._id;
        messageProvider.onClose(fullId, callback);
      } else {
        if (_messages[id]) {
          _messages[id].onCloseTrigger = callback;
        }
      }
    },
    /**
    * Show a generic message
    * @deprecated after August 2017 | Please use the Flag module instead.
    * @name generic
    * @method
    * @memberof module:Messages#
    * @param    {String}            title       Sets the title text of the message.
    * @param    {String}            body        The main content of the message.
    * @param    {Object}            options             Message Options
    * @param    {Boolean}           options.closeable   Adds a control allowing the user to close the message, removing it from the page.
    * @param    {Boolean}           options.fadeout     Toggles the fade away on the message
    * @param    {Number}            options.delay       Time to wait (in ms) before starting fadeout animation (ignored if fadeout==false)
    * @param    {Number}            options.duration    Fadeout animation duration in milliseconds (ignored if fadeout==false)
    * @returns  {String}    The id to be used when clearing the message
    * @example
    * //create a message
    * var message = AP.messages.generic('title', 'generic message example');
    */
    generic: messageModule('generic'),
    /**
    * Show an error message
    * @deprecated after August 2017 | Please use the Flag module instead.
    * @name error
    * @method
    * @memberof module:Messages#
    * @param    {String}            title       Sets the title text of the message.
    * @param    {String}            body        The main content of the message.
    * @param    {Object}            options             Message Options
    * @param    {Boolean}           options.closeable   Adds a control allowing the user to close the message, removing it from the page.
    * @param    {Boolean}           options.fadeout     Toggles the fade away on the message
    * @param    {Number}            options.delay       Time to wait (in ms) before starting fadeout animation (ignored if fadeout==false)
    * @param    {Number}            options.duration    Fadeout animation duration in milliseconds (ignored if fadeout==false)
    * @returns  {String}    The id to be used when clearing the message
    * @example
    * //create a message
    * var message = AP.messages.error('title', 'error message example');
    */
    error: messageModule('error'),
    /**
    * Show a warning message
    * @deprecated after August 2017 | Please use the Flag module instead.
    * @name warning
    * @method
    * @memberof module:Messages#
    * @param    {String}            title       Sets the title text of the message.
    * @param    {String}            body        The main content of the message.
    * @param    {Object}            options             Message Options
    * @param    {Boolean}           options.closeable   Adds a control allowing the user to close the message, removing it from the page.
    * @param    {Boolean}           options.fadeout     Toggles the fade away on the message
    * @param    {Number}            options.delay       Time to wait (in ms) before starting fadeout animation (ignored if fadeout==false)
    * @param    {Number}            options.duration    Fadeout animation duration in milliseconds (ignored if fadeout==false)
    * @returns  {String}    The id to be used when clearing the message
    * @example
    * //create a message
    * var message = AP.messages.warning('title', 'warning message example');
    */
    warning: messageModule('warning'),
    /**
    * Show a success message
    * @deprecated after August 2017 | Please use the Flag module instead.
    * @name success
    * @method
    * @memberof module:Messages#
    * @param    {String}            title       Sets the title text of the message.
    * @param    {String}            body        The main content of the message.
    * @param    {Object}            options             Message Options
    * @param    {Boolean}           options.closeable   Adds a control allowing the user to close the message, removing it from the page.
    * @param    {Boolean}           options.fadeout     Toggles the fade away on the message
    * @param    {Number}            options.delay       Time to wait (in ms) before starting fadeout animation (ignored if fadeout==false)
    * @param    {Number}            options.duration    Fadeout animation duration in milliseconds (ignored if fadeout==false)
    * @returns  {String}    The id to be used when clearing the message
    * @example
    * //create a message
    * var message = AP.messages.success('title', 'success message example');
    */
    success: messageModule('success'),
    /**
    * Show an info message
    * @deprecated after August 2017 | Please use the Flag module instead.
    * @name info
    * @method
    * @memberof module:Messages#
    * @param    {String}            title       Sets the title text of the message.
    * @param    {String}            body        The main content of the message.
    * @param    {Object}            options             Message Options
    * @param    {Boolean}           options.closeable   Adds a control allowing the user to close the message, removing it from the page.
    * @param    {Boolean}           options.fadeout     Toggles the fade away on the message
    * @param    {Number}            options.delay       Time to wait (in ms) before starting fadeout animation (ignored if fadeout==false)
    * @param    {Number}            options.duration    Fadeout animation duration in milliseconds (ignored if fadeout==false)
    * @returns  {String}    The id to be used when clearing the message
    * @example
    * //create a message
    * var message = AP.messages.info('title', 'info message example');
    */
    info: messageModule('info'),
    /**
    * Show a hint message
    * @deprecated after August 2017 | Please use the Flag module instead.
    * @name hint
    * @method
    * @memberof module:Messages#
    * @param    {String}            title               Sets the title text of the message.
    * @param    {String}            body                The main content of the message.
    * @param    {Object}            options             Message Options
    * @param    {Boolean}           options.closeable   Adds a control allowing the user to close the message, removing it from the page.
    * @param    {Boolean}           options.fadeout     Toggles the fade away on the message
    * @param    {Number}            options.delay       Time to wait (in ms) before starting fadeout animation (ignored if fadeout==false)
    * @param    {Number}            options.duration    Fadeout animation duration in milliseconds (ignored if fadeout==false)
    * @returns  {String}    The id to be used when clearing the message
    * @example
    * //create a message
    * var message = AP.messages.hint('title', 'hint message example');
    */
    hint: messageModule('hint')
  };

  var FlagActions = {
    // called on action click
    actionInvoked: function actionInvoked(actionId, flagId) {
      EventDispatcher$1.dispatch('flag-action-invoked', {
        id: flagId,
        actionId: actionId
      });
    },
    open: function open(flagId) {
      EventDispatcher$1.dispatch('flag-open', {
        id: flagId
      });
    },
    //called to close a flag
    close: function close(flagId) {
      EventDispatcher$1.dispatch('flag-close', {
        id: flagId
      });
    },
    //called by AUI when closed
    closed: function closed(flagId) {
      EventDispatcher$1.dispatch('flag-closed', {
        id: flagId
      });
    }
  };

  var FLAGID_PREFIX = 'ap-flag-';
  var FLAG_CLASS = 'ac-aui-flag';
  var FLAG_ACTION_CLASS = 'ac-flag-actions';
  var Flag$1 = /*#__PURE__*/function () {
    function Flag() {}
    var _proto = Flag.prototype;
    _proto.cleanKey = function cleanKey(dirtyKey) {
      var cleanFlagKeyRegExp = new RegExp('^' + FLAGID_PREFIX + '(.+)$');
      var matches = dirtyKey.match(cleanFlagKeyRegExp);
      if (matches && matches[1]) {
        return matches[1];
      }
      return null;
    };
    _proto._toHtmlString = function _toHtmlString(str) {
      if ($.type(str) === 'string') {
        return str;
      } else if ($.type(str) === 'object' && str instanceof $) {
        return str.html();
      }
    };
    _proto._renderBody = function _renderBody(body) {
      var body = this._toHtmlString(body);
      var $body = $('<div />').html(body);
      $('<p />').addClass(FLAG_ACTION_CLASS).appendTo($body);
      return $body.html();
    };
    _proto._renderActions = function _renderActions($flag, flagId, actions) {
      var $actionContainer = $flag.find('.' + FLAG_ACTION_CLASS);
      actions = actions || {};
      var $action;
      Object.getOwnPropertyNames(actions).forEach(function (key) {
        $action = $('<a />').attr('href', '#').data({
          'key': key,
          'flag_id': flagId
        }).text(actions[key]);
        $actionContainer.append($action);
      }, this);
      return $flag;
    };
    _proto.render = function render(options) {
      bindFlagDomEvents();
      var _id = FLAGID_PREFIX + options.id;
      var auiFlag = AJS.flag({
        type: options.type,
        title: options.title,
        body: this._renderBody(options.body),
        close: options.close
      });
      auiFlag.setAttribute('id', _id);
      var $auiFlag = $(auiFlag);
      this._renderActions($auiFlag, options.id, options.actions);
      $auiFlag.addClass(FLAG_CLASS);
      $auiFlag.close = auiFlag.close;
      return $auiFlag;
    };
    _proto.close = function close(id) {
      var f = document.getElementById(id);
      f.close();
    };
    return Flag;
  }();
  var FlagComponent = new Flag$1();
  var flagDomEventsBound = false;
  function bindFlagDomEvents() {
    if (flagDomEventsBound) {
      return;
    }
    $(document).on('aui-flag-close', function (e) {
      var _id = e.target.id;
      var cleanFlagId = FlagComponent.cleanKey(_id);
      FlagActions.closed(cleanFlagId);
    });
    $(document).on('click', '.' + FLAG_ACTION_CLASS, function (e) {
      var $target = $(e.target);
      var actionKey = $target.data('key');
      var flagId = $target.data('flag_id');
      FlagActions.actionInvoked(actionKey, flagId);
    });
    flagDomEventsBound = true;
  }
  EventDispatcher$1.register('flag-close', function (data) {
    FlagComponent.close(data.id);
  });

  /**
  * Flags are the primary method for providing system feedback in the product user interface. Messages include notifications of various kinds: alerts, confirmations, notices, warnings, info and errors.
  * @module Flag
  */
  var _flags = {};

  /**
  * @class Flag~Flag
  * @description A flag object created by the [AP.flag]{@link module:Flag} module.
  * @example
  * // complete flag API example:
  * var outFlagId;
  * var flag = AP.flag.create({
  *   title: 'Successfully created a flag.',
  *   body: 'This is a flag.',
  *   type: 'info',
  *   actions: {
  *     'actionOne': 'action name'
  *   }
  * }, function(identifier) {
  * // Each flag will have a unique id. Save it for later.
  *   ourFlagId = identifier;
  * });
  *
  * // listen to flag events
  * AP.events.on('flag.close', function(data) {
  * // a flag was closed. data.flagIdentifier should match ourFlagId
  *   console.log('flag id: ', data.flagIdentifier);
  * });
  * AP.events.on('flag.action', function(data) {
  * // a flag action was clicked. data.actionIdentifier will be 'actionOne'
  * // data.flagIdentifier will equal ourFlagId
  *   console.log('flag id: ', data.flagIdentifier, 'flag action id', data.actionIdentifier);
  * });
  */
  var Flag = /*#__PURE__*/function () {
    function Flag(options, callback) {
      callback = Util.last(arguments);
      if (typeof options !== 'object') {
        return;
      }
      var flagId = callback._id;
      var frameworkAdaptor = HostApi$1.getFrameworkAdaptor();
      var flagProvider = frameworkAdaptor.getProviderByModuleName('flag');
      if (flagProvider) {
        var actions = [];
        if (typeof options.actions === 'object') {
          actions = Object.getOwnPropertyNames(options.actions).map(function (key) {
            return {
              actionKey: key,
              actionText: options.actions[key],
              executeAction: FlagActions.actionInvoked.bind(null, key, flagId)
            };
          });
        }
        var type = options.type || 'info';
        var flagOptions = {
          id: flagId,
          title: options.title,
          body: options.body,
          actions: actions,
          onClose: FlagActions.closed,
          close: options.close,
          type: type.toLowerCase()
        };
        this.flag = flagProvider.create(flagOptions);
        var addonProvider = ModuleProviders$1.getProvider('addon');
        if (addonProvider && addonProvider.registerUnmountCallback) {
          addonProvider.registerUnmountCallback(this.close.bind(this), callback._context);
        }
      } else {
        this.flag = FlagComponent.render({
          type: options.type,
          title: options.title,
          body: AJS.escapeHtml(options.body),
          actions: options.actions,
          close: options.close,
          id: flagId
        });
        FlagActions.open(this.flag.attr('id'));
      }
      this.onTriggers = {};
      this.extension = callback._context.extension;
      _flags[callback._id] = this;
      callback.call(null, callback._id);
    }

    /**
    * @name close
    * @memberof Flag~Flag
    * @method
    * @description Closes the Flag.
    * @example
    * // Display a nice green flag using the Flags JavaScript API.
    * var flag = AP.flag.create({
    *   title: 'Successfully created a flag.',
    *   body: 'This is a flag.',
    *   type: 'info'
    * });
    *
    * // Close the flag.
    * flag.close()
    *
    */
    var _proto = Flag.prototype;
    _proto.close = function close() {
      this.flag.close();
    };
    return Flag;
  }();
  function invokeTrigger(id, eventName, data) {
    if (_flags[id]) {
      var extension = _flags[id].extension;
      data = data || {};
      data.flagIdentifier = id;
      var targetSpec = {
        id: extension.id
      };
      EventActions.broadcast(eventName, targetSpec, data);
    }
  }
  EventDispatcher$1.register('flag-closed', function (data) {
    invokeTrigger(data.id, 'flag.close');
    if (_flags[data.id]) {
      delete _flags[data.id];
    }
  });
  EventDispatcher$1.register('flag-action-invoked', function (data) {
    invokeTrigger(data.id, 'flag.action', {
      actionIdentifier: data.actionId
    });
  });
  var flag = {
    /**
    * @name create
    * @method
    * @description Creates a new flag.
    * @param {Object} options           Options of the flag.
    * @param {String} options.title     The title text of the flag.
    * @param {String} options.body      The body text of the flag.
    * @param {String} options.type=info Sets the type of the message. Valid options are "info", "success", "warning" and "error".
    * @param {String} options.close     The closing behaviour that this flag has. Valid options are "manual", and "auto".
    * @param {Object} options.actions   Map of {actionIdentifier: 'Action link text'} to add to the flag. The actionIdentifier will be passed to a 'flag.action' event if the link is clicked.
    * @returns {Flag~Flag}
    * @example
    * // Display a nice green flag using the Flags JavaScript API.
    * var flag = AP.flag.create({
    *   title: 'Successfully created a flag.',
    *   body: 'This is a flag.',
    *   type: 'success',
    *   actions: {
    *     'actionkey': 'Click me'
    *   }
    * });
    */
    create: {
      constructor: Flag,
      close: Flag.prototype.close
    }
  };

  var analytics = {
    trackDeprecatedMethodUsed: function trackDeprecatedMethodUsed(methodUsed, callback) {
      callback = Util.last(arguments);
      AnalyticsAction.trackDeprecatedMethodUsed(methodUsed, callback._context.extension);
    },
    trackIframePerformanceMetrics: function trackIframePerformanceMetrics(metrics, callback) {
      callback = Util.last(arguments);
      AnalyticsAction.trackIframePerformanceMetrics(metrics, callback._context.extension);
    },
    trackWebVitals: function trackWebVitals(metrics, callback) {
      callback = Util.last(arguments);
      AnalyticsAction.trackWebVitals(metrics, callback._context.extension);
    }
  };

  var TRIGGER_PERCENTAGE = 10; //% before scroll events are fired
  var activeGeneralPageAddon;
  var lastScrollEventTriggered; //top or bottom

  EventDispatcher$1.register('iframe-bridge-established', function (data) {
    if (data.extension.options.isFullPage) {
      window.addEventListener('scroll', scrollEventHandler);
      activeGeneralPageAddon = data.extension.id;
    }
  });
  EventDispatcher$1.register('iframe-destroyed', function (extension) {
    removeScrollEvent();
  });
  EventDispatcher$1.register('iframe-unload', function (extension) {
    removeScrollEvent();
  });
  function removeScrollEvent() {
    window.removeEventListener('scroll', scrollEventHandler);
    activeGeneralPageAddon = undefined;
    lastScrollEventTriggered = undefined;
  }
  function scrollEventHandler() {
    var documentHeight = document.documentElement.scrollHeight;
    var windowHeight = window.innerHeight;
    var boundary = documentHeight * (TRIGGER_PERCENTAGE / 100);
    if (window.pageYOffset <= boundary) {
      triggerEvent('nearTop');
    } else if (windowHeight + window.pageYOffset + boundary >= documentHeight) {
      triggerEvent('nearBottom');
    } else {
      lastScrollEventTriggered = undefined;
    }
  }
  function triggerEvent(type) {
    if (lastScrollEventTriggered === type) {
      return; // only once per scroll.
    }
    EventActions.broadcast('scroll.' + type, {
      id: activeGeneralPageAddon
    }, {});
    lastScrollEventTriggered = type;
  }

  /**
   * Enables apps to get and set the scroll position.
   * @exports Scroll-position
   */
  var scrollPosition = {
    /**
     * Gets the scroll position relative to the browser viewport
     *
     * @param callback {Function} callback to pass the scroll position
     * @noDemo
     * @example
     * AP.scrollPosition.getPosition(function(obj) { console.log(obj); });
     */
    getPosition: function getPosition(callback) {
      callback = Util.last(arguments);
      // scrollPosition.getPosition is only available for general-pages
      if (callback._context.extension.options.isFullPage) {
        var $el = Util.getIframeByExtensionId(callback._context.extension_id);
        var offset = $el.offset();
        var $window = $(window);
        callback({
          scrollY: $window.scrollTop() - offset.top,
          scrollX: $window.scrollLeft() - offset.left,
          width: window.innerWidth,
          height: window.innerHeight
        });
      }
    },
    /**
     * Sets the vertical scroll position relative to the iframe
     *
     * @param y {Number} vertical offset position
     * @param callback {Function} callback to pass the scroll position
     * @noDemo
     * @example
     * AP.scrollPosition.setVerticalPosition(30, function(obj) { console.log(obj); });
     */
    setVerticalPosition: function setVerticalPosition(y, callback) {
      callback = Util.last(arguments);
      if (callback._context.extension.options && callback._context.extension.options.isFullPage) {
        var $el = Util.getIframeByExtensionId(callback._context.extension_id);
        var offset = $el.offset();
        if (typeof y === 'number') {
          document.documentElement.scrollTop = offset.top + y;
        }
      }
    }
  };

  var DropdownActions = {
    // called on action click
    itemSelected: function itemSelected(dropdown_id, item, extension) {
      EventDispatcher$1.dispatch('dropdown-item-selected', {
        id: dropdown_id,
        item: item,
        extension: extension
      });
    }
  };

  /**
  * DO NOT INCLUDE ME IN THE PUBLIC DOCUMENTATION
  * there is no AUI implementation of this
  */
  function buildListItem(listItem) {
    var finishedListItem = {};
    if (typeof listItem === 'string') {
      finishedListItem.content = listItem;
    } else if (listItem.text && typeof listItem.text === 'string') {
      finishedListItem.content = listItem.text;
      if (typeof listItem.disabled === 'boolean') {
        finishedListItem.disabled = listItem.disabled;
      }
      if (typeof listItem.itemId !== 'undefined') {
        finishedListItem.itemId = listItem.itemId;
      }
    } else {
      throw new Error('Unknown dropdown list item format.');
    }
    return finishedListItem;
  }
  function moduleListToApiList(list) {
    return list.map(function (item) {
      if (item.list && Array.isArray(item.list)) {
        var returnval = {
          heading: item.heading
        };
        returnval.items = item.list.map(function (listitem) {
          return buildListItem(listitem);
        });
        return returnval;
      }
    });
  }

  /**
  * @class DropdownItem
  * A single item in a dropdown menu can be a string or an object
  * @param {String} itemId The id of a single dropdown item
  * @param {String} text    The text to display in the dropdown item
  */

  /**
  * @module Dropdown
  * @description Dropdown menu that can go outside the iframe bounds.
  * @example
  * // create a dropdown menu with 1 section and 2 items
  * var mydropdown = {
  *   dropdownId: 'my-dropdown',
  *   list: [{
  *     heading: 'section heading',
  *     list: [
  *       {text: 'one'},
  *       {text: 'two'}
  *     ]
  *   }]
  * };
  *
  * AP.events.on('dropdown-item-selected', (data) =>{
  *   console.log('dropdown item selected', data.dropdownId, data.item);
  * });
  *
  * AP.dropdown.create(mydropdown);
  * // button is an element in our document that triggered the dropdown
  * let rect = document.querySelector('button').getBoundingClientRect();
  * AP.dropdown.showAt('my-dropdown', rect.left, rect.top, rect.width);
  *
  */

  var dropdown = {
    /**
    * @name create
    * @method
    * @description Creates a new dropdown.
    * @param {Object} options             Options of the dropdown.
    * @param {String} options.dropdownId A unique identifier for the dropdown that will be referenced in events.
    * @param {String} options.list        An array containing dropdown items {Dropdown~DropdownItem}
    * @example
    * // create a dropdown menu with 1 section and 2 items
    * var mydropdown = {
    *   dropdownId: 'my-dropdown',
    *   list: [{
    *     heading: 'section heading',
    *     list: [
    *       {text: 'one'},
    *       {text: 'two'}
    *     ]
    *   }]
    * };
    *
    * AP.dropdown.create(mydropdown);
    */
    create: function create(options, callback) {
      callback = Util.last(arguments);
      if (typeof options !== 'object') {
        return;
      }
      var frameworkAdaptor = HostApi$1.getFrameworkAdaptor();
      var dropdownProvider = frameworkAdaptor.getProviderByModuleName('dropdown');
      if (dropdownProvider) {
        var dropdownGroups = moduleListToApiList(options.list);
        var dropdownProviderOptions = {
          dropdownId: options.dropdownId,
          dropdownGroups: dropdownGroups,
          dropdownItemNotifier: function dropdownItemNotifier(data) {
            DropdownActions.itemSelected(data.dropdownId, data.item, callback._context.extension);
          }
        };
        dropdownProvider.create(dropdownProviderOptions, callback._context);
        return dropdownProviderOptions;
      }
    },
    /**
    * @name showAt
    * @method
    * @description Displays a created dropdown menu.
    * @param {String} dropdownId   Id used when creating the dropdown
    * @param {String} x             x position from the edge of your iframe to display
    * @param {String} y             y position from the edge of your iframe to display
    * @param {String} width         Optionally enforce a width for the dropdown menu
    * @example
    * // create a dropdown menu with 1 section and 2 items
    * var mydropdown = {
    *   dropdownId: 'my-dropdown',
    *   list: [{
    *     list:['one', 'two']
    *   }]
    * };
    *
    * AP.dropdown.create(mydropdown);
    * // Get the button that activated the dropdown
    * let rect = document.querySelector('button').getBoundingClientRect();
    * AP.dropdown.showAt('my-dropdown', rect.left, rect.top, rect.width);
    */
    showAt: function showAt(dropdownId, x, y, width) {
      var callback = Util.last(arguments);
      var rect = {
        left: 0,
        top: 0
      };
      var iframe = document.getElementById(callback._context.extension_id);
      if (iframe) {
        rect = iframe.getBoundingClientRect();
      } else {
        console.error('ACJS: no iframe found for dropdown');
      }
      var frameworkAdaptor = HostApi$1.getFrameworkAdaptor();
      var dropdownProvider = frameworkAdaptor.getProviderByModuleName('dropdown');
      if (dropdownProvider) {
        var dropdownProviderArgs = {
          dropdownId: dropdownId,
          x: x,
          y: y,
          width: width
        };
        dropdownProvider.showAt(dropdownProviderArgs, {
          iframeDimensions: rect,
          onItemSelection: function onItemSelection(dropdownId, item) {
            DropdownActions.itemSelected(dropdownId, item, callback._context.extension);
          }
        });
      }
    },
    /**
    * @name hide
    * @method
    * @description Hide a dropdown menu
    * @param {String} dropdownId The id of the dropdown to hide
    * @example
    * AP.dropdown.create('my-dropdown');
    * AP.dropdown.hide('my-dropdown');
    */
    hide: function hide(id) {
      var frameworkAdaptor = HostApi$1.getFrameworkAdaptor();
      var dropdownProvider = frameworkAdaptor.getProviderByModuleName('dropdown');
      if (dropdownProvider) {
        dropdownProvider.hide(id);
      }
    },
    /**
    * @name itemDisable
    * @method
    * @description Disable an item in the dropdown menu
    * @param {String} dropdownId The id of the dropdown
    * @param {String} itemId     The dropdown item to disable
    * @example
    * AP.dropdown.create('my-dropdown');
    * AP.dropdown.itemDisable('my-dropdown', 'item-id');
    */
    itemDisable: function itemDisable(dropdownId, itemId) {
      var frameworkAdaptor = HostApi$1.getFrameworkAdaptor();
      var dropdownProvider = frameworkAdaptor.getProviderByModuleName('dropdown');
      if (dropdownProvider) {
        dropdownProvider.itemDisable(dropdownId, itemId);
      }
    },
    /**
    * @name itemEnable
    * @method
    * @description Hide a dropdown menu
    * @param {String} dropdownId The id of the dropdown
    * @param {String} itemId The id of the dropdown item to enable
    * @example
    * AP.dropdown.create('my-dropdown');
    * AP.dropdown.itemEnable('my-dropdown', 'item-id');
    */
    itemEnable: function itemEnable(dropdownId, itemId) {
      var frameworkAdaptor = HostApi$1.getFrameworkAdaptor();
      var dropdownProvider = frameworkAdaptor.getProviderByModuleName('dropdown');
      if (dropdownProvider) {
        dropdownProvider.itemEnable(dropdownId, itemId);
      }
    }
  };
  EventDispatcher$1.register('dropdown-item-selected', function (data) {
    EventActions.broadcast('dropdown-item-selected', {
      addon_key: data.extension.addon_key,
      key: data.extension.key
    }, {
      dropdownId: data.id,
      item: data.item
    });
  });

  // friendly unload with connectHost.destroy
  EventDispatcher$1.register('iframe-destroyed', function (data) {
    var frameworkAdaptor = HostApi$1.getFrameworkAdaptor();
    var dropdownProvider = frameworkAdaptor.getProviderByModuleName('dropdown');
    if (dropdownProvider) {
      dropdownProvider.destroyByExtension(data.extension.extension_id);
    }
  });

  // unfriendly unload by removing the iframe from the DOM
  EventDispatcher$1.register('after:iframe-unload', function (data) {
    var frameworkAdaptor = HostApi$1.getFrameworkAdaptor();
    var dropdownProvider = frameworkAdaptor.getProviderByModuleName('dropdown');
    if (dropdownProvider) {
      dropdownProvider.destroyByExtension(data.extension.extension_id);
    }
  });

  function deprecateHost(fn, name, alternate, sinceVersion) {
    var called = false;
    return function () {
      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }
      if (!called && typeof console !== 'undefined' && console.warn) {
        called = true;
        console.warn("DEPRECATED API - " + name + " has been deprecated " + (sinceVersion ? "since ACJS " + sinceVersion : 'in ACJS') + (" and will be removed in a future release. " + (alternate ? "Use " + alternate + " instead." : 'No alternative will be provided.')));
        var callback = Util.last(args);
        if (callback && callback._context && callback._context.extension) {
          analytics.trackDeprecatedMethodUsed(name, callback);
        }
      }
      return fn.apply(void 0, args);
    };
  }

  var host = {
    /*
     This function could be used in Connect app for moving focus to Host app.
     As Connect App - iframe app, it can get control. When it's happen - host app events such short-cuts
     stop working. This function could help in this case.
    */
    focus: function focus() {
      window.document.querySelector('a').focus({
        preventScroll: true
      });
      window.document.querySelector('a').blur();
    },
    getSelectedText: deprecateHost(function (callback) {
      callback('');
      return;
    }, 'AP.host.getSelectedText()')
  };

  /**
   * This file contains the source of truth for themes and all associated meta data.
   */

  /**
   * Themes: The internal identifier of a theme.
   * These ids are what the actual theme files/folders are called.
   * style-dictionary will attempt to locate these in the file-system.
   */

  /**
   * ThemeOverrides: The internal identifier of a theme override. Which are themes that contain
   * a subset of tokens intended to override an existing theme. These ids are what the actual
   * theme files/folders are called. style-dictionary will attempt to locate these in the file-system.
   * Theme overrides are temporary and there may not be any defined at times.
   */

  /**
   * Theme kinds: The type of theme.
   * Some themes are entirely focused on Color, whilst others are purely focused on spacing.
   * In the future other types may be introduced such as typography.
   */

  /**
   * Theme modes: The general purpose of a theme.
   * This attr is used to apply the appropriate system-preference option
   * It may also be used as a selector for mode-specific overrides such as light/dark images.
   * The idea is there may exist many color themes, but every theme must either fit into light or dark.
   */
  var themeColorModes = ['light', 'dark', 'auto'];
  /**
   * Theme ids: The value that will be mounted to the DOM as a data attr
   * For example: `data-theme="light:light dark:dark spacing:spacing"
   *
   * These ids must be kebab case
   */
  var themeIds = ['light-increased-contrast', 'light', 'light-future', 'light-brand-refresh', 'dark', 'dark-future', 'dark-increased-contrast', 'dark-brand-refresh', 'legacy-light', 'legacy-dark', 'spacing', 'shape', 'typography-adg3', 'typography-modernized', 'typography-refreshed'];

  var THEME_DATA_ATTRIBUTE = 'data-theme';
  var COLOR_MODE_ATTRIBUTE = 'data-color-mode';
  var CSS_PREFIX = 'ds';
  var CURRENT_SURFACE_CSS_VAR = "--".concat(CSS_PREFIX, "-elevation-surface-current");

  var themeKinds = ['light', 'dark', 'spacing', 'typography', 'shape'];
  var customThemeOptions = 'UNSAFE_themeOptions';
  var isThemeKind = function isThemeKind(themeKind) {
    return themeKinds.find(function (kind) {
      return kind === themeKind;
    }) !== undefined;
  };
  var isThemeIds = function isThemeIds(themeId) {
    return themeIds.find(function (id) {
      return id === themeId;
    }) !== undefined;
  };
  var isColorMode = function isColorMode(modeId) {
    return ['light', 'dark', 'auto'].includes(modeId);
  };
  /**
   * Converts a string that is formatted for the `data-theme` HTML attribute
   * to an object that can be passed to `setGlobalTheme`.
   *
   * @param {string} themes The themes that should be applied.
   *
   * @example
   * ```
   * themeStringToObject('dark:dark light:legacy-light spacing:spacing');
   * // returns { dark: 'dark', light: 'legacy-light', spacing: 'spacing' }
   * ```
   */
  var themeStringToObject = function themeStringToObject(themeState) {
    return themeState.split(' ').map(function (theme) {
      return theme.split(/:([^]*)/);
    }).reduce(function (themeObject, _ref) {
      var _ref2 = _slicedToArray(_ref, 2),
        kind = _ref2[0],
        id = _ref2[1];
      if (kind === 'colorMode' && isColorMode(id)) {
        themeObject[kind] = id;
      }
      if (isThemeKind(kind) && isThemeIds(id)) {
        // @ts-expect-error FIXME - this is a valid ts error
        themeObject[kind] = id;
      }
      if (kind === customThemeOptions) {
        try {
          themeObject[customThemeOptions] = JSON.parse(id);
        } catch (e) {
        }
      }
      return themeObject;
    }, {});
  };

  function ownKeys(e, r) {
    var t = Object.keys(e);
    if (Object.getOwnPropertySymbols) {
      var o = Object.getOwnPropertySymbols(e);
      r && (o = o.filter(function (r) {
        return Object.getOwnPropertyDescriptor(e, r).enumerable;
      })), t.push.apply(t, o);
    }
    return t;
  }
  function _objectSpread(e) {
    for (var r = 1; r < arguments.length; r++) {
      var t = null != arguments[r] ? arguments[r] : {};
      r % 2 ? ownKeys(Object(t), !0).forEach(function (r) {
        _defineProperty(e, r, t[r]);
      }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) {
        Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r));
      });
    }
    return e;
  }
  var isThemeColorMode = function isThemeColorMode(colorMode) {
    return themeColorModes.find(function (mode) {
      return mode === colorMode;
    }) !== undefined;
  };
  var getGlobalTheme = function getGlobalTheme() {
    if (typeof document === 'undefined') {
      return {};
    }
    var element = document.documentElement;
    var colorMode = element.getAttribute(COLOR_MODE_ATTRIBUTE) || '';
    var theme = element.getAttribute(THEME_DATA_ATTRIBUTE) || '';
    return _objectSpread(_objectSpread({}, themeStringToObject(theme)), isThemeColorMode(colorMode) && {
      colorMode: colorMode
    });
  };
  var getGlobalTheme$1 = getGlobalTheme;

  /**
   * A MutationObserver which watches the `<html>` element for changes to the theme.
   *
   * In React, use the {@link useThemeObserver `useThemeObserver`} hook instead.
   *
   * @param {function} callback - A callback function which fires when the theme changes.
   *
   * @example
   * ```
   * const observer = new ThemeMutationObserver((theme) => {});
   * observer.observe();
   * ```
   */
  var ThemeMutationObserver = /*#__PURE__*/function () {
    function ThemeMutationObserver(callback) {
      _classCallCheck(this, ThemeMutationObserver);
      _defineProperty(this, "legacyObserver", null);
      this.callback = callback;
      ThemeMutationObserver.callbacks.add(callback);
    }
    return _createClass(ThemeMutationObserver, [{
      key: "observe",
      value: function observe() {
        if (!ThemeMutationObserver.observer) {
          ThemeMutationObserver.observer = new MutationObserver(function () {
            var theme = getGlobalTheme$1();
            ThemeMutationObserver.callbacks.forEach(function (callback) {
              return callback(theme);
            });
          });
          // Observer only needs to be configured once
          ThemeMutationObserver.observer.observe(document.documentElement, {
            attributeFilter: [THEME_DATA_ATTRIBUTE, COLOR_MODE_ATTRIBUTE]
          });
        }
      }
    }, {
      key: "disconnect",
      value: function disconnect() {
        if (this.callback) {
          ThemeMutationObserver.callbacks.delete(this.callback);
        }
        if (ThemeMutationObserver.callbacks.size === 0 && ThemeMutationObserver.observer) {
          ThemeMutationObserver.observer.disconnect();
          ThemeMutationObserver.observer = null;
        }
      }
    }]);
  }();
  _defineProperty(ThemeMutationObserver, "observer", null);
  _defineProperty(ThemeMutationObserver, "callbacks", new Set());

  var getPluginFeatureFlags = function getPluginFeatureFlags() {
    return {
      // Add feature flags sent to the plugin on theming bootstrap here
    };
  };
  var getSurfaceValue = function getSurfaceValue(extension_id) {
    var iframe = document.getElementById(extension_id);
    if (!iframe) {
      return;
    }
    var styles = getComputedStyle(iframe);
    var currentSurface = styles.getPropertyValue(CURRENT_SURFACE_CSS_VAR);
    var raised = styles.getPropertyValue('--ds-surface-raised');
    var overlay = styles.getPropertyValue('--ds-surface-overlay');
    if (currentSurface === raised) {
      return 'raised';
    }
    if (currentSurface === overlay) {
      return 'overlay';
    }
  };

  /**
   * Broadcasts the new theme to all plugins.
   * @typedef {import("@atlaskit/tokens").ThemeState} ThemeState
   * @param newTheme {ThemeState}
   */
  function broadcastThemeChange(newTheme) {
    var featureFlags = getPluginFeatureFlags();
    if (ThemingModuleState.extensionIds.length) {
      ThemingModuleState.extensionIds.forEach(function (id) {
        var surface = getSurfaceValue(id);
        host$1.broadcast('theme_changed', {
          id: id
        }, {
          newTheme: newTheme,
          featureFlags: featureFlags,
          surface: surface
        });
      });
    }
  }

  // Exported only for testing
  var ThemingModuleState = {
    /**
     * An array containing extension ID's for each app that has enabled theming.
     * Theme change events are only sent to apps listed in this array.
     */
    extensionIds: [],
    /**
     * Observer that will only trigger on changes to data-theme on the root html element.
     * Dispatches the new theme to all plugins when the theme changes.
     */
    themeObserver: new ThemeMutationObserver(function (newTheme) {
      broadcastThemeChange(newTheme);
    })
  };

  // Only functions that make up the public ACJS theming API should be exported on this object.
  var ThemingModule = {
    /**
     * Starts observing theme changes and dispatches the `theme_initialized` event to the plugin.
     * This event causes the plugin to react when the host's theme changes.
     */
    initializeTheming: function initializeTheming() {
      var callback = Util.last(arguments);
      var forceNoCleanup = arguments.length === 1 ? false : arguments[0];
      if (!callback || !callback._context || !callback._context.extension_id) {
        return;
      }
      var extension_id = callback._context.extension_id;
      if (!extension_id) {
        return;
      }
      var initialTheme = getGlobalTheme$1();
      var surface = getSurfaceValue(extension_id);
      var featureFlags = getPluginFeatureFlags();

      // The theme_initialized event should be broadcast even if it has previously been initialized by the plugin.
      // This is to ensure that theming works in multi-page apps after navigating to a different page.
      // The reason is that isThemingInitialized on the plugin side starts as `false` when all.js is loaded.
      // The theme_initialized event is needed to change it to `true` for any new page.
      host$1.broadcast('theme_initialized', {
        id: extension_id
      }, {
        initialTheme: initialTheme,
        featureFlags: featureFlags,
        surface: surface,
        forceNoCleanup: forceNoCleanup
      });
      if (!ThemingModuleState.extensionIds.includes(extension_id)) {
        ThemingModuleState.extensionIds.push(extension_id);
        ThemingModuleState.themeObserver.observe(document.documentElement);
      }
    },
    /**
     * This has to be part of the API so the plugin can notify us when theme loading is finished.
     * The underscore indicates it should not be called by apps directly.
     * @private
     */
    _finishedInitTheming: function _finishedInitTheming() {}
  };

  /**
   * A JavaScript module which provides functions for general page modules.
   * @module Page
   */
  var page = {
    /**
     * Sets the title of the document when a full page iframe is loaded.
     * @memberOf module:Page
     * @method setTitle
     * @param {String} title the title of the document to be set.
     * @example
     * AP.page.setTitle(title)
     */
    setTitle: function setTitle(title, callback) {
      callback = Util.last(arguments);
      if (callback._context.extension.options && callback._context.extension.options.isFullPage) {
        var productSuffix = ' - ' + Util.guessProductFromModuleDefined(window.connectHost);
        document.title = title + productSuffix;
      }
    }
  };

  var InlineDialogWebItemActions = {
    addExtension: function addExtension(data) {
      EventDispatcher$1.dispatch('inline-dialog-extension', {
        $el: data.$el,
        extension: data.extension
      });
    }
  };

  var InlineDialog = /*#__PURE__*/function () {
    function InlineDialog() {}
    var _proto = InlineDialog.prototype;
    _proto.resize = function resize(data) {
      var width = Util.stringToDimension(data.width);
      var height = Util.stringToDimension(data.height);
      var $content = data.$el.find('.contents');
      if ($content.length === 1) {
        $content.css({
          width: width,
          height: height
        });
        InlineDialogActions.refresh(data.$el);
      }
    };
    _proto.refresh = function refresh($el) {
      if (!$el.is(':visible')) {
        return;
      }
      $el[0].popup.reset();
    };
    _proto._getInlineDialog = function _getInlineDialog($el) {
      return AJS.InlineDialog($el);
    };
    _proto._renderContainer = function _renderContainer() {
      return $('<div />').addClass('aui-inline-dialog-contents');
    };
    _proto._displayInlineDialog = function _displayInlineDialog(data) {
      InlineDialogActions.created({
        $el: data.$el,
        trigger: data.trigger,
        extension: data.extension
      });
    };
    _proto.hideInlineDialog = function hideInlineDialog($el) {
      $el.hide();
    };
    _proto.closeInlineDialog = function closeInlineDialog() {
      $('.aui-inline-dialog').filter(function () {
        return $(this).find('.ap-iframe-container').length > 0;
      }).hide();
    };
    _proto.render = function render(data) {
      var _this = this;
      var $inlineDialog = $(document.getElementById('inline-dialog-' + data.id));
      if ($inlineDialog.length !== 0) {
        $inlineDialog.remove();
      }
      var $el = AJS.InlineDialog(data.bindTo,
      //assign unique id to inline Dialog
      data.id, function ($placeholder, trigger, showInlineDialog) {
        $placeholder.append(data.$content);
        _this._displayInlineDialog({
          extension: data.extension,
          $el: $placeholder,
          trigger: trigger
        });
        showInlineDialog();
      }, data.inlineDialogOptions);

      // Increase z-index so it shows above the Jira project sidebar
      $el.css('z-index', 1000);
      return $el;
    };
    return InlineDialog;
  }();
  var InlineDialogComponent = new InlineDialog();
  EventDispatcher$1.register('iframe-resize', function (data) {
    var container = data.$el.parents('.aui-inline-dialog');
    if (container.length === 1) {
      InlineDialogComponent.resize({
        width: data.width,
        height: data.height,
        $el: container
      });
    }
  });
  EventDispatcher$1.register('inline-dialog-refresh', function (data) {
    InlineDialogComponent.refresh(data.$el);
  });
  EventDispatcher$1.register('inline-dialog-hide', function (data) {
    InlineDialogComponent.hideInlineDialog(data.$el);
  });
  EventDispatcher$1.register('inline-dialog-close', function (data) {
    InlineDialogComponent.closeInlineDialog();
  });

  var ITEM_NAME$1 = 'inline-dialog';
  var SELECTOR$1 = '.ap-inline-dialog';
  var WEBITEM_UID_KEY$1 = 'inline-dialog-target-uid';
  var InlineDialogWebItem = /*#__PURE__*/function () {
    function InlineDialogWebItem() {
      this._inlineDialogWebItemSpec = {
        name: ITEM_NAME$1,
        selector: SELECTOR$1,
        triggers: ['mouseenter', 'click']
      };
      this._inlineDialogWebItems = {};
    }
    var _proto = InlineDialogWebItem.prototype;
    _proto.getWebItem = function getWebItem() {
      return this._inlineDialogWebItemSpec;
    };
    _proto._createInlineDialog = function _createInlineDialog(data) {
      var $inlineDialog = InlineDialogComponent.render({
        extension: data.extension,
        id: data.id,
        bindTo: data.$target,
        $content: $('<div />'),
        inlineDialogOptions: data.extension.options
      });
      return $inlineDialog;
    }

    /**
     * @param {WebItemInvokedEventData} data
     */;
    _proto.triggered = function triggered(data) {
      // don't trigger on hover, when hover is not specified.
      var eventType = data.eventType;
      var onHover = data.extension.options.onHover;
      if (eventType !== 'click' && !onHover) {
        return;
      }
      var $target = data.$target;
      var webitemId = $target.data(WEBITEM_UID_KEY$1);
      EventDispatcher$1.dispatch('analytics-inline-dialog-opened', {
        extension: data.extension,
        eventType: eventType,
        onHover: onHover
      });
      if (eventType === 'click' && !onHover) {
        if ($target.attr('data-inline-created') === 'true') {
          // Let AUI handle the click event, otherwise we would destroy and recreate the dialog
          // This will lead to the dialog being hidden immediately after it was opened
          return;
        } else {
          // We have to handle the first click event to set up the dialog
          // We set this property to ignore subsequent clicks and let AUI handle them
          $target.attr('data-inline-created', 'true');
        }
      }
      var $inlineDialog = this._createInlineDialog({
        id: webitemId,
        extension: data.extension,
        $target: $target,
        options: data.extension.options || {}
      });
      $inlineDialog.show();
    };
    _proto.opened = function opened(data) {
      var $existingFrame = data.$el.find('iframe');
      var isExistingFrame = $existingFrame && $existingFrame.length === 1;
      // existing iframe is already present and src is still valid (either no jwt or jwt has not expired).
      if (isExistingFrame) {
        var src = $existingFrame.attr('src');
        var srcPresent = src.length > 0;
        if (srcPresent) {
          var srcHasJWT = urlUtils.hasJwt(src);
          var srcHasValidJWT = srcHasJWT && !urlUtils.isJwtExpired(src);
          if (srcHasValidJWT || !srcHasJWT) {
            return false;
          }
        }
      }
      var dialogExtension = getAccessNarrowingSpaceContext(data.extension);
      var contentRequest = webItemInstance.requestContent(dialogExtension);
      if (!contentRequest) {
        console.warn('no content resolver found');
        return false;
      }
      contentRequest.then(function (content) {
        content.options = content.options || {};
        Util.extend(content.options, {
          autoresize: true,
          widthinpx: true
        });
        InlineDialogWebItemActions.addExtension({
          $el: data.$el,
          extension: content
        });
      });
      return true;
    };
    _proto.addExtension = function addExtension(data) {
      var addon = create(data.extension);
      data.$el.empty().append(addon);
    }

    /**
     * @param {WebItemInvokedEventData} data
     */;
    _proto.createIfNotExists = function createIfNotExists(data) {
      var $target = data.$target;
      var uid = $target.data(WEBITEM_UID_KEY$1);
      if (!uid) {
        uid = WebItemUtils.uniqueId();
        $target.data(WEBITEM_UID_KEY$1, uid);
      }
    };
    return InlineDialogWebItem;
  }();
  var inlineDialogInstance = new InlineDialogWebItem();
  var webitem$1 = inlineDialogInstance.getWebItem();
  EventDispatcher$1.register('before:webitem-invoked:' + webitem$1.name, function (data) {
    inlineDialogInstance.createIfNotExists(data);
  });
  EventDispatcher$1.register('webitem-invoked:' + webitem$1.name, function (data) {
    inlineDialogInstance.triggered(data);
  });
  EventDispatcher$1.register('inline-dialog-opened', function (data) {
    inlineDialogInstance.opened(data);
  });
  EventDispatcher$1.register('inline-dialog-extension', function (data) {
    inlineDialogInstance.addExtension(data);
  });
  WebItemActions.addWebItem(webitem$1);

  var ITEM_NAME = 'dialog';
  var SELECTOR = '.ap-dialog';
  var TRIGGERS = ['click'];
  var WEBITEM_UID_KEY = 'dialog-target-uid';
  var DEFAULT_WEBITEM_OPTIONS = {
    chrome: true
  };
  var DialogWebItem = /*#__PURE__*/function () {
    function DialogWebItem() {
      this._dialogWebItem = {
        name: ITEM_NAME,
        selector: SELECTOR,
        triggers: TRIGGERS
      };
    }
    var _proto = DialogWebItem.prototype;
    _proto.getWebItem = function getWebItem() {
      return this._dialogWebItem;
    };
    _proto._dialogOptions = function _dialogOptions(options) {
      return Util.extend({}, DEFAULT_WEBITEM_OPTIONS, options || {});
    }

    /**
     * @param {WebItemInvokedEventData} data
     */;
    _proto.triggered = function triggered(data) {
      var $target = data.$target;
      var webitemId = $target.data(WEBITEM_UID_KEY);
      var dialogOptions = this._dialogOptions(data.extension.options);
      dialogOptions.id = webitemId;
      DialogExtensionActions.open(data.extension, dialogOptions);
    }

    /**
     * @param {WebItemInvokedEventData} data
     */;
    _proto.createIfNotExists = function createIfNotExists(data) {
      var $target = data.$target;
      var uid = $target.data(WEBITEM_UID_KEY);
      if (!uid) {
        uid = WebItemUtils.uniqueId();
        $target.data(WEBITEM_UID_KEY, uid);
      }
    };
    return DialogWebItem;
  }();
  var dialogInstance = new DialogWebItem();
  var webitem = dialogInstance.getWebItem();
  EventDispatcher$1.register('webitem-invoked:' + webitem.name, function (data) {
    dialogInstance.triggered(data);
  });
  EventDispatcher$1.register('before:webitem-invoked:' + webitem.name, dialogInstance.createIfNotExists);
  WebItemActions.addWebItem(webitem);

  function observeIframeRemoval() {
    var scopedObserver = new MutationObserver(function (mutationsList) {
      mutationsList.forEach(function (mutation) {
        if (mutation.type === 'childList') {
          mutation.removedNodes.forEach(function (node) {
            if (node.nodeType !== Node.ELEMENT_NODE) {
              return;
            }
            var onDisconnect = function onDisconnect(node) {
              HostApi$1.destroy(node.id);
            };
            // Check if the node itself is an iframe
            if (node.tagName === 'IFRAME' && node.id) {
              onDisconnect(node);
            } else {
              // Check if any iframes exist within the removed node's subtree
              node.querySelectorAll('iframe[id]').forEach(function (childNode) {
                onDisconnect(childNode);
              });
            }
          });
        }
      });
    });
    scopedObserver.observe(document.body, {
      childList: true,
      subtree: true
    });
    return scopedObserver;
  }

  /**
   * Private namespace for host-side code.
   * @type {*|{}}
   * @private
   * @deprecated use AMD instead of global namespaces. The only thing that should be on _AP is _AP.define and _AP.require.
   */
  if (!window._AP) {
    window._AP = {};
  }

  /*
   * Add version
   */
  if (!window._AP.version) {
    window._AP.version = '5.3.179';
  }
  host$1.defineModule('messages', messages);
  host$1.defineModule('flag', flag);
  host$1.defineModule('dialog', dialog);
  host$1.defineModule('inlineDialog', inlineDialog);
  host$1.defineModule('env', env);
  host$1.defineModule('events', events);
  host$1.defineModule('_analytics', analytics);
  host$1.defineModule('scrollPosition', scrollPosition);
  host$1.defineModule('dropdown', dropdown);
  host$1.defineModule('host', host);
  host$1.defineModule('theming', ThemingModule);
  host$1.defineModule('page', page);
  EventDispatcher$1.register('module-define-custom', function (data) {
    host$1.defineModule(data.name, data.methods);
  });
  host$1.registerRequestNotifier(function (data) {
    var dispatchEvent = function dispatchEvent() {
      if (data.type === 'req') {
        var registeredExtension = host$1.getExtensions(function (ext) {
          return ext.extension_id === data.extension_id;
        })[0];
        if (registeredExtension) {
          var extension = registeredExtension.extension;
          EventDispatcher$1.dispatch('method-invoked', _extends({}, data, {
            extension: extension
          }));
        }
      }
    };
    if (typeof window.requestIdleCallback === 'function') {
      window.requestIdleCallback(dispatchEvent, {
        timeout: 1000
      });
    } else {
      dispatchEvent();
    }
  });
  function onDocumentReady() {
    observeIframeRemoval();
  }
  if (document.readyState === 'loading') {
    document.addEventListener('DOMContentLoaded', onDocumentReady);
  } else {
    onDocumentReady();
  }
  if (getBooleanFeatureFlag('acjs_iframe_click_analytics')) {
    DomEventActions.registerClickHandler(function (iframe, data) {
      var iframeInfo = JSON.parse(iframe.name || '{}');
      var iframeInfoOptions = iframeInfo.options || {};
      var analyticsData = {
        moduleLocation: iframeInfoOptions.moduleLocation,
        moduleType: iframeInfoOptions.moduleType,
        addonKey: data.addon_key
      };
      AnalyticsAction.trackIframeClick(analyticsData);
    });
  }
  host$1.setFeatureFlagGetter(getBooleanFeatureFlag);

  return HostApi$1;

}));
