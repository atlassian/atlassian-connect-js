var AP = (function (jsClient, clientCore) {
  'use strict';

  function _assertThisInitialized(self) {
    if (self === void 0) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }
    return self;
  }

  function _setPrototypeOf(o, p) {
    _setPrototypeOf = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf(o, p) {
      o.__proto__ = p;
      return o;
    };
    return _setPrototypeOf(o, p);
  }

  function _inheritsLoose(subClass, superClass) {
    subClass.prototype = Object.create(superClass.prototype);
    subClass.prototype.constructor = subClass;
    _setPrototypeOf(subClass, superClass);
  }

  function _isNativeReflectConstruct$2() {
    if (typeof Reflect === "undefined" || !Reflect.construct) return false;
    if (Reflect.construct.sham) return false;
    if (typeof Proxy === "function") return true;
    try {
      Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {}));
      return true;
    } catch (e) {
      return false;
    }
  }

  function _construct(Parent, args, Class) {
    if (_isNativeReflectConstruct$2()) {
      _construct = Reflect.construct.bind();
    } else {
      _construct = function _construct(Parent, args, Class) {
        var a = [null];
        a.push.apply(a, args);
        var Constructor = Function.bind.apply(Parent, a);
        var instance = new Constructor();
        if (Class) _setPrototypeOf(instance, Class.prototype);
        return instance;
      };
    }
    return _construct.apply(null, arguments);
  }

  var LOG_PREFIX = "[Simple-XDM] ";
  var nativeBind = Function.prototype.bind;
  var util = {
    locationOrigin: function locationOrigin() {
      if (!window.location.origin) {
        return window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
      } else {
        return window.location.origin;
      }
    },
    randomString: function randomString() {
      return Math.floor(Math.random() * 1000000000).toString(16);
    },
    isString: function isString(str) {
      return typeof str === "string" || str instanceof String;
    },
    argumentsToArray: function argumentsToArray(arrayLike) {
      return Array.prototype.slice.call(arrayLike);
    },
    argumentNames: function argumentNames(fn) {
      return fn.toString().replace(/((\/\/.*$)|(\/\*[^]*?\*\/))/mg, '') // strip comments
      .replace(/[^(]+\(([^)]*)[^]+/, '$1') // get signature
      .match(/([^\s,]+)/g) || [];
    },
    hasCallback: function hasCallback(args) {
      var length = args.length;
      return length > 0 && typeof args[length - 1] === 'function';
    },
    error: function error(msg) {
      if (window.console && window.console.error) {
        var outputError = [];
        if (typeof msg === "string") {
          outputError.push(LOG_PREFIX + msg);
          outputError = outputError.concat(Array.prototype.slice.call(arguments, 1));
        } else {
          outputError.push(LOG_PREFIX);
          outputError = outputError.concat(Array.prototype.slice.call(arguments));
        }
        window.console.error.apply(null, outputError);
      }
    },
    warn: function warn(msg) {
      if (window.console) {
        console.warn(LOG_PREFIX + msg);
      }
    },
    log: function log(msg) {
      if (window.console) {
        window.console.log(LOG_PREFIX + msg);
      }
    },
    _bind: function _bind(thisp, fn) {
      if (nativeBind && fn.bind === nativeBind) {
        return fn.bind(thisp);
      }
      return function () {
        return fn.apply(thisp, arguments);
      };
    },
    throttle: function throttle(func, wait, context) {
      var previous = 0;
      return function () {
        var now = Date.now();
        if (now - previous > wait) {
          previous = now;
          func.apply(context, arguments);
        }
      };
    },
    each: function each(list, iteratee) {
      var length;
      var key;
      if (list) {
        length = list.length;
        if (length != null && typeof list !== 'function') {
          key = 0;
          while (key < length) {
            if (iteratee.call(list[key], key, list[key]) === false) {
              break;
            }
            key += 1;
          }
        } else {
          for (key in list) {
            if (list.hasOwnProperty(key)) {
              if (iteratee.call(list[key], key, list[key]) === false) {
                break;
              }
            }
          }
        }
      }
    },
    extend: function extend(dest) {
      var args = arguments;
      var srcs = [].slice.call(args, 1, args.length);
      srcs.forEach(function (source) {
        if (typeof source === "object") {
          Object.getOwnPropertyNames(source).forEach(function (name) {
            dest[name] = source[name];
          });
        }
      });
      return dest;
    },
    sanitizeStructuredClone: function sanitizeStructuredClone(object) {
      var whiteList = [Boolean, String, Date, RegExp, Blob, File, FileList, ArrayBuffer];
      var blackList = [Error, Node];
      var warn = util.warn;
      var visitedObjects = [];
      function _clone(value) {
        if (typeof value === 'function') {
          warn("A function was detected and removed from the message.");
          return null;
        }
        if (blackList.some(function (t) {
          if (value instanceof t) {
            warn(t.name + " object was detected and removed from the message.");
            return true;
          }
          return false;
        })) {
          return {};
        }
        if (value && typeof value === 'object' && whiteList.every(function (t) {
          return !(value instanceof t);
        })) {
          var newValue;
          if (Array.isArray(value)) {
            newValue = value.map(function (element) {
              return _clone(element);
            });
          } else {
            if (visitedObjects.indexOf(value) > -1) {
              warn("A circular reference was detected and removed from the message.");
              return null;
            }
            visitedObjects.push(value);
            newValue = {};
            for (var name in value) {
              if (value.hasOwnProperty(name)) {
                var clonedValue = _clone(value[name]);
                if (clonedValue !== null) {
                  newValue[name] = clonedValue;
                }
              }
            }
            visitedObjects.pop();
          }
          return newValue;
        }
        return value;
      }
      return _clone(object);
    },
    getOrigin: function getOrigin(url, base) {
      // everything except IE11
      if (typeof URL === 'function') {
        try {
          return new URL(url, base).origin;
        } catch (e) {}
      }
      // ie11 + safari 10
      var doc = document.implementation.createHTMLDocument('');
      if (base) {
        var baseElement = doc.createElement('base');
        baseElement.href = base;
        doc.head.appendChild(baseElement);
      }
      var anchorElement = doc.createElement('a');
      anchorElement.href = url;
      doc.body.appendChild(anchorElement);
      var origin = anchorElement.protocol + '//' + anchorElement.hostname;
      //ie11, only include port if referenced in initial URL
      if (url.match(/\/\/[^/]+:[0-9]+\//)) {
        origin += anchorElement.port ? ':' + anchorElement.port : '';
      }
      return origin;
    }
  };

  var PostMessage = /*#__PURE__*/function () {
    function PostMessage(data) {
      var d = data || {};
      this._registerListener(d.listenOn);
    }
    var _proto = PostMessage.prototype;
    _proto._registerListener = function _registerListener(listenOn) {
      if (!listenOn || !listenOn.addEventListener) {
        listenOn = window;
      }
      listenOn.addEventListener("message", util._bind(this, this._receiveMessage), false);
    };
    _proto._receiveMessage = function _receiveMessage(event) {
      var handler = this._messageHandlers[event.data.type],
        extensionId = event.data.eid,
        reg;
      if (extensionId && this._registeredExtensions) {
        reg = this._registeredExtensions[extensionId];
      }
      if (!handler || !this._checkOrigin(event, reg)) {
        return false;
      }
      handler.call(this, event, reg);
    };
    return PostMessage;
  }();

  var VALID_EVENT_TIME_MS = 30000; //30 seconds
  var XDMRPC = /*#__PURE__*/function (_PostMessage) {
    _inheritsLoose(XDMRPC, _PostMessage);
    var _proto = XDMRPC.prototype;
    _proto._padUndefinedArguments = function _padUndefinedArguments(array, length) {
      return array.length >= length ? array : array.concat(new Array(length - array.length));
    };
    function XDMRPC(config) {
      var _this;
      config = config || {};
      _this = _PostMessage.call(this, config) || this;
      _this._registeredExtensions = config.extensions || {};
      _this._registeredAPIModules = {};
      _this._registeredAPIModules._globals = {};
      _this._pendingCallbacks = {};
      _this._keycodeCallbacks = {};
      _this._clickHandlers = [];
      _this._pendingEvents = {};
      _this._messageHandlers = {
        init: _this._handleInit,
        req: _this._handleRequest,
        resp: _this._handleResponse,
        broadcast: _this._handleBroadcast,
        event_query: _this._handleEventQuery,
        key_triggered: _this._handleKeyTriggered,
        addon_clicked: _this._handleAddonClick,
        get_host_offset: _this._getHostOffset,
        unload: _this._handleUnload
      };
      return _this;
    }
    _proto._verifyAPI = function _verifyAPI(event, reg) {
      var untrustedTargets = event.data.targets;
      if (!untrustedTargets) {
        return;
      }
      var trustedSpec = this.getApiSpec();
      var tampered = false;
      function check(trusted, untrusted) {
        Object.getOwnPropertyNames(untrusted).forEach(function (name) {
          if (typeof untrusted[name] === 'object' && trusted[name]) {
            check(trusted[name], untrusted[name]);
          } else {
            if (untrusted[name] === 'parent' && trusted[name]) {
              tampered = true;
            }
          }
        });
      }
      check(trustedSpec, untrustedTargets);
      if (event.source && event.source.postMessage) {
        // only post a message if the source of the event still exists
        event.source.postMessage({
          type: 'api_tamper',
          tampered: tampered
        }, reg.extension.url);
      } else {
        console.warn("_verifyAPI postMessage skipped as event source missing.");
      }
    };
    _proto._handleInit = function _handleInit(event, reg) {
      if (event.source && event.source.postMessage) {
        // only post a message if the source of the event still exists
        event.source.postMessage({
          type: 'init_received'
        }, reg.extension.url);
      } else {
        console.warn("_handleInit postMessage skipped as event source missing.");
      }
      this._registeredExtensions[reg.extension_id].source = event.source;
      if (reg.initCallback) {
        reg.initCallback(event.data.eid);
        delete reg.initCallback;
      }
      if (event.data.targets) {
        this._verifyAPI(event, reg);
      }
    };
    _proto._getHostOffset = function _getHostOffset(event, _window) {
      var hostWindow = event.source;
      var hostFrameOffset = null;
      var windowReference = _window || window; // For testing

      if (windowReference === windowReference.top && typeof windowReference.getHostOffsetFunctionOverride === 'function') {
        hostFrameOffset = windowReference.getHostOffsetFunctionOverride(hostWindow);
      }
      if (typeof hostFrameOffset !== 'number') {
        hostFrameOffset = 0;
        // Find the closest frame that has the same origin as event source
        while (!this._hasSameOrigin(hostWindow)) {
          // Climb up the iframe tree 1 layer
          hostFrameOffset++;
          hostWindow = hostWindow.parent;
        }
      }
      if (event.source && event.source.postMessage) {
        // only post a message if the source of the event still exists
        event.source.postMessage({
          hostFrameOffset: hostFrameOffset
        }, event.origin);
      } else {
        console.warn("_getHostOffset postMessage skipped as event source missing.");
      }
    };
    _proto._hasSameOrigin = function _hasSameOrigin(window) {
      if (window === window.top) {
        return true;
      }
      try {
        // Try set & read a variable on the given window
        // If we can successfully read the value then it means the given window has the same origin
        // as the window that is currently executing the script
        var testVariableName = 'test_var_' + Math.random().toString(16).substr(2);
        window[testVariableName] = true;
        return window[testVariableName];
      } catch (e) {
        // A exception will be thrown if the windows doesn't have the same origin
      }
      return false;
    };
    _proto._handleResponse = function _handleResponse(event) {
      var data = event.data;
      var pendingCallback = this._pendingCallbacks[data.mid];
      if (pendingCallback) {
        delete this._pendingCallbacks[data.mid];
        pendingCallback.apply(window, data.args);
      }
    };
    _proto.registerRequestNotifier = function registerRequestNotifier(cb) {
      this._registeredRequestNotifier = cb;
    };
    _proto._handleRequest = function _handleRequest(event, reg) {
      function sendResponse() {
        var args = util.sanitizeStructuredClone(util.argumentsToArray(arguments));
        if (event.source && event.source.postMessage) {
          // only post a message if the source of the event still exists
          event.source.postMessage({
            mid: event.data.mid,
            type: 'resp',
            forPlugin: true,
            args: args
          }, reg.extension.url);
        } else {
          console.warn("_handleRequest postMessage skipped as event source missing.");
        }
      }
      var data = event.data;
      var module = this._registeredAPIModules[data.mod];
      var extension = this.getRegisteredExtensions(reg.extension)[0];
      if (module) {
        var fnName = data.fn;
        if (data._cls) {
          var Cls = module[data._cls];
          var ns = data.mod + '-' + data._cls + '-';
          sendResponse._id = data._id;
          if (fnName === 'constructor') {
            if (!Cls._construct) {
              Cls.constructor.prototype._destroy = function () {
                delete this._context._proxies[ns + this._id];
              };
              Cls._construct = function () {
                for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
                  args[_key] = arguments[_key];
                }
                var inst = _construct(Cls.constructor, args);
                var callback = args[args.length - 1];
                inst._id = callback._id;
                inst._context = callback._context;
                inst._context._proxies[ns + inst._id] = inst;
                return inst;
              };
            }
            module = Cls;
            fnName = '_construct';
          } else {
            module = extension._proxies[ns + data._id];
          }
        }
        var method = module[fnName];
        if (method) {
          var methodArgs = data.args;
          var padLength = method.length - 1;
          if (fnName === '_construct') {
            padLength = module.constructor.length - 1;
          }
          sendResponse._context = extension;
          methodArgs = this._padUndefinedArguments(methodArgs, padLength);
          methodArgs.push(sendResponse);
          var promiseResult = method.apply(module, methodArgs);
          if (method.returnsPromise) {
            if (!(typeof promiseResult === 'object' || typeof promiseResult === 'function') || typeof promiseResult.then !== 'function') {
              sendResponse('Defined module method did not return a promise.');
            } else {
              promiseResult.then(function (result) {
                sendResponse(undefined, result);
              }).catch(function (err) {
                err = err instanceof Error ? err.message : err;
                sendResponse(err);
              });
            }
          }
          if (this._registeredRequestNotifier) {
            this._registeredRequestNotifier.call(null, {
              module: data.mod,
              fn: data.fn,
              type: data.type,
              args: methodArgs,
              addon_key: reg.extension.addon_key,
              key: reg.extension.key,
              extension_id: reg.extension_id
            });
          }
        }
      }
    };
    _proto._handleBroadcast = function _handleBroadcast(event, reg) {
      var event_data = event.data;
      var targetSpec = function targetSpec(r) {
        return r.extension.addon_key === reg.extension.addon_key && r.extension_id !== reg.extension_id;
      };
      this.dispatch(event_data.etyp, targetSpec, event_data.evnt, null, null);
    };
    _proto._handleKeyTriggered = function _handleKeyTriggered(event, reg) {
      var eventData = event.data;
      var keycodeEntry = this._keycodeKey(eventData.keycode, eventData.modifiers, reg.extension_id);
      var listeners = this._keycodeCallbacks[keycodeEntry];
      if (listeners) {
        listeners.forEach(function (listener) {
          listener.call(null, {
            addon_key: reg.extension.addon_key,
            key: reg.extension.key,
            extension_id: reg.extension_id,
            keycode: eventData.keycode,
            modifiers: eventData.modifiers
          });
        }, this);
      }
    };
    _proto.defineAPIModule = function defineAPIModule(module, moduleName) {
      moduleName = moduleName || '_globals';
      this._registeredAPIModules[moduleName] = util.extend({}, this._registeredAPIModules[moduleName] || {}, module);
      return this._registeredAPIModules;
    };
    _proto.isAPIModuleDefined = function isAPIModuleDefined(moduleName) {
      return typeof this._registeredAPIModules[moduleName] !== 'undefined';
    };
    _proto._pendingEventKey = function _pendingEventKey(targetSpec, time) {
      var key = targetSpec.addon_key || 'global';
      if (targetSpec.key) {
        key = key + "@@" + targetSpec.key;
      }
      key = key + "@@" + time;
      return key;
    };
    _proto.queueEvent = function queueEvent(type, targetSpec, event, callback) {
      var loaded_frame,
        targets = this._findRegistrations(targetSpec);
      loaded_frame = targets.some(function (target) {
        return target.registered_events !== undefined;
      }, this);
      if (loaded_frame) {
        this.dispatch(type, targetSpec, event, callback);
      } else {
        this._cleanupInvalidEvents();
        var time = new Date().getTime();
        this._pendingEvents[this._pendingEventKey(targetSpec, time)] = {
          type: type,
          targetSpec: targetSpec,
          event: event,
          callback: callback,
          time: time,
          uid: util.randomString()
        };
      }
    };
    _proto._cleanupInvalidEvents = function _cleanupInvalidEvents() {
      var _this2 = this;
      var now = new Date().getTime();
      var keys = Object.keys(this._pendingEvents);
      keys.forEach(function (index) {
        var element = _this2._pendingEvents[index];
        var eventIsValid = now - element.time <= VALID_EVENT_TIME_MS;
        if (!eventIsValid) {
          delete _this2._pendingEvents[index];
        }
      });
    };
    _proto._handleEventQuery = function _handleEventQuery(message, extension) {
      var _this3 = this;
      var executed = {};
      var now = new Date().getTime();
      var keys = Object.keys(this._pendingEvents);
      keys.forEach(function (index) {
        var element = _this3._pendingEvents[index];
        var eventIsValid = now - element.time <= VALID_EVENT_TIME_MS;
        var isSameTarget = !element.targetSpec || _this3._findRegistrations(element.targetSpec).length !== 0;
        if (isSameTarget && element.targetSpec.key) {
          isSameTarget = element.targetSpec.addon_key === extension.extension.addon_key && element.targetSpec.key === extension.extension.key;
        }
        if (eventIsValid && isSameTarget) {
          executed[index] = element;
          element.targetSpec = element.targetSpec || {};
          _this3.dispatch(element.type, element.targetSpec, element.event, element.callback, message.source);
        } else if (!eventIsValid) {
          delete _this3._pendingEvents[index];
        }
      });
      this._registeredExtensions[extension.extension_id].registered_events = message.data.args;
      return executed;
    };
    _proto._handleUnload = function _handleUnload(event, reg) {
      if (!reg) {
        return;
      }
      if (reg.extension_id && this._registeredExtensions[reg.extension_id]) {
        delete this._registeredExtensions[reg.extension_id].source;
      }
      if (reg.unloadCallback) {
        reg.unloadCallback(event.data.eid);
      }
    };
    _proto.dispatch = function dispatch(type, targetSpec, event, callback, source) {
      function sendEvent(reg, evnt) {
        if (reg.source && reg.source.postMessage) {
          var mid;
          if (callback) {
            mid = util.randomString();
            this._pendingCallbacks[mid] = callback;
          }
          reg.source.postMessage({
            type: 'evt',
            mid: mid,
            etyp: type,
            evnt: evnt
          }, reg.extension.url);
        }
      }
      var registrations = this._findRegistrations(targetSpec || {});
      registrations.forEach(function (reg) {
        if (source && !reg.source) {
          reg.source = source;
        }
        if (reg.source) {
          util._bind(this, sendEvent)(reg, event);
        }
      }, this);
    };
    _proto._findRegistrations = function _findRegistrations(targetSpec) {
      var _this4 = this;
      if (this._registeredExtensions.length === 0) {
        util.error('no registered extensions', this._registeredExtensions);
        return [];
      }
      var keys = Object.getOwnPropertyNames(targetSpec);
      var registrations = Object.getOwnPropertyNames(this._registeredExtensions).map(function (key) {
        return _this4._registeredExtensions[key];
      });
      if (targetSpec instanceof Function) {
        return registrations.filter(targetSpec);
      } else {
        return registrations.filter(function (reg) {
          return keys.every(function (key) {
            return reg.extension[key] === targetSpec[key];
          });
        });
      }
    };
    _proto.registerExtension = function registerExtension(extension_id, data) {
      data._proxies = {};
      data.extension_id = extension_id;
      this._registeredExtensions[extension_id] = data;
    };
    _proto._keycodeKey = function _keycodeKey(key, modifiers, extension_id) {
      var code = key;
      if (modifiers) {
        if (typeof modifiers === "string") {
          modifiers = [modifiers];
        }
        modifiers.sort();
        modifiers.forEach(function (modifier) {
          code += '$$' + modifier;
        }, this);
      }
      return code + '__' + extension_id;
    };
    _proto.registerKeyListener = function registerKeyListener(extension_id, key, modifiers, callback) {
      if (typeof modifiers === "string") {
        modifiers = [modifiers];
      }
      var reg = this._registeredExtensions[extension_id];
      var keycodeEntry = this._keycodeKey(key, modifiers, extension_id);
      if (!this._keycodeCallbacks[keycodeEntry]) {
        this._keycodeCallbacks[keycodeEntry] = [];
        reg.source.postMessage({
          type: 'key_listen',
          keycode: key,
          modifiers: modifiers,
          action: 'add'
        }, reg.extension.url);
      }
      this._keycodeCallbacks[keycodeEntry].push(callback);
    };
    _proto.unregisterKeyListener = function unregisterKeyListener(extension_id, key, modifiers, callback) {
      var keycodeEntry = this._keycodeKey(key, modifiers, extension_id);
      var potentialCallbacks = this._keycodeCallbacks[keycodeEntry];
      var reg = this._registeredExtensions[extension_id];
      if (potentialCallbacks) {
        if (callback) {
          var index = potentialCallbacks.indexOf(callback);
          this._keycodeCallbacks[keycodeEntry].splice(index, 1);
        } else {
          delete this._keycodeCallbacks[keycodeEntry];
        }
        if (reg.source && reg.source.postMessage) {
          reg.source.postMessage({
            type: 'key_listen',
            keycode: key,
            modifiers: modifiers,
            action: 'remove'
          }, reg.extension.url);
        }
      }
    };
    _proto.registerClickHandler = function registerClickHandler(callback) {
      if (typeof callback !== 'function') {
        throw new Error('callback must be a function');
      }
      this._clickHandlers.push(callback);
    };
    _proto._handleAddonClick = function _handleAddonClick(event, reg) {
      for (var i = 0; i < this._clickHandlers.length; i++) {
        if (typeof this._clickHandlers[i] === 'function') {
          this._clickHandlers[i]({
            addon_key: reg.extension.addon_key,
            key: reg.extension.key,
            extension_id: reg.extension_id
          });
        }
      }
    };
    _proto.unregisterClickHandler = function unregisterClickHandler() {
      this._clickHandlers = [];
    };
    _proto.getApiSpec = function getApiSpec(addonKey) {
      var _this5 = this;
      function getModuleDefinition(mod) {
        return Object.getOwnPropertyNames(mod).reduce(function (accumulator, memberName) {
          var member = mod[memberName];
          switch (typeof member) {
            case 'function':
              accumulator[memberName] = {
                args: util.argumentNames(member),
                returnsPromise: member.returnsPromise || false
              };
              break;
            case 'object':
              if (member.hasOwnProperty('constructor')) {
                accumulator[memberName] = getModuleDefinition(member);
              }
              break;
          }
          return accumulator;
        }, {});
      }
      return Object.getOwnPropertyNames(this._registeredAPIModules).reduce(function (accumulator, moduleName) {
        var module = _this5._registeredAPIModules[moduleName];
        if (typeof module.addonKey === 'undefined' || module.addonKey === addonKey) {
          accumulator[moduleName] = getModuleDefinition(module);
        }
        return accumulator;
      }, {});
    };
    _proto._originEqual = function _originEqual(url, origin) {
      function strCheck(str) {
        return typeof str === 'string' && str.length > 0;
      }
      var urlOrigin = util.getOrigin(url);
      // check strings are strings and they contain something
      if (!strCheck(url) || !strCheck(origin) || !strCheck(urlOrigin)) {
        return false;
      }
      return origin === urlOrigin;
    }

    // validate origin of postMessage
    ;
    _proto._checkOrigin = function _checkOrigin(event, reg) {
      var no_source_types = ['init'];
      var isNoSourceType = reg && !reg.source && no_source_types.indexOf(event.data.type) > -1;
      var sourceTypeMatches = reg && event.source === reg.source;
      var hasExtensionUrl = reg && this._originEqual(reg.extension.url, event.origin);
      var isValidOrigin = hasExtensionUrl && (isNoSourceType || sourceTypeMatches);

      // get_host_offset fires before init
      if (event.data.type === 'get_host_offset' && window === window.top) {
        isValidOrigin = true;
      }

      // check undefined for chromium (Issue 395010)
      if (event.data.type === 'unload' && (sourceTypeMatches || event.source === undefined)) {
        isValidOrigin = true;
      }
      return isValidOrigin;
    };
    _proto.getRegisteredExtensions = function getRegisteredExtensions(filter) {
      if (filter) {
        return this._findRegistrations(filter);
      }
      return this._registeredExtensions;
    };
    _proto.unregisterExtension = function unregisterExtension(filter) {
      var registrations = this._findRegistrations(filter);
      if (registrations.length !== 0) {
        registrations.forEach(function (registration) {
          var _this6 = this;
          var keys = Object.keys(this._pendingEvents);
          keys.forEach(function (index) {
            var element = _this6._pendingEvents[index];
            var targetSpec = element.targetSpec || {};
            if (targetSpec.addon_key === registration.extension.addon_key && targetSpec.key === registration.extension.key) {
              delete _this6._pendingEvents[index];
            }
          });
          delete this._registeredExtensions[registration.extension_id];
        }, this);
      }
    };
    _proto.setFeatureFlagGetter = function setFeatureFlagGetter(getBooleanFeatureFlag) {
      this._getBooleanFeatureFlag = getBooleanFeatureFlag;
    };
    return XDMRPC;
  }(PostMessage);

  var Connect = /*#__PURE__*/function () {
    function Connect() {
      this._xdm = new XDMRPC();
    }

    /**
     * Send a message to iframes matching the targetSpec. This message is added to
     *  a message queue for delivery to ensure the message is received if an iframe
     *  has not yet loaded
     *
     * @param type The name of the event type
     * @param targetSpec The spec to match against extensions when sending this event
     * @param event The event payload
     * @param callback A callback to be executed when the remote iframe calls its callback
     */
    var _proto = Connect.prototype;
    _proto.dispatch = function dispatch(type, targetSpec, event, callback) {
      this._xdm.queueEvent(type, targetSpec, event, callback);
      return this.getExtensions(targetSpec);
    }

    /**
     * Send a message to iframes matching the targetSpec immediately. This message will
     *  only be sent to iframes that are already open, and will not be delivered if none
     *  are currently open.
     *
     * @param type The name of the event type
     * @param targetSpec The spec to match against extensions when sending this event
     * @param event The event payload
     */;
    _proto.broadcast = function broadcast(type, targetSpec, event) {
      this._xdm.dispatch(type, targetSpec, event, null, null);
      return this.getExtensions(targetSpec);
    };
    _proto._createId = function _createId(extension) {
      if (!extension.addon_key || !extension.key) {
        throw Error('Extensions require addon_key and key');
      }
      return extension.addon_key + '__' + extension.key + '__' + util.randomString();
    }
    /**
    * Creates a new iframed module, without actually creating the DOM element.
    * The iframe attributes are passed to the 'setupCallback', which is responsible for creating
    * the DOM element and returning the window reference.
    *
    * @param extension The extension definition. Example:
    *   {
    *     addon_key: 'my-addon',
    *     key: 'my-module',
    *     url: 'https://example.com/my-module',
    *     options: {
    *         autoresize: false,
    *         hostOrigin: 'https://connect-host.example.com/'
    *     }
    *   }
    *
    * @param initCallback The optional initCallback is called when the bridge between host and iframe is established.
    **/;
    _proto.create = function create(extension, initCallback, unloadCallback) {
      var extension_id = this.registerExtension(extension, initCallback, unloadCallback);
      var options = extension.options || {};
      var data = {
        extension_id: extension_id,
        api: this._xdm.getApiSpec(extension.addon_key),
        origin: util.locationOrigin(),
        options: options
      };
      return {
        id: extension_id,
        name: JSON.stringify(data),
        src: extension.url
      };
    }

    // This is called from ACJS
    // noinspection JSUnusedGlobalSymbols
    ;
    _proto.registerRequestNotifier = function registerRequestNotifier(callback) {
      this._xdm.registerRequestNotifier(callback);
    };
    _proto.registerExtension = function registerExtension(extension, initCallback, unloadCallback) {
      var extension_id = this._createId(extension);
      this._xdm.registerExtension(extension_id, {
        extension: extension,
        initCallback: initCallback,
        unloadCallback: unloadCallback
      });
      return extension_id;
    };
    _proto.registerKeyListener = function registerKeyListener(extension_id, key, modifiers, callback) {
      this._xdm.registerKeyListener(extension_id, key, modifiers, callback);
    };
    _proto.unregisterKeyListener = function unregisterKeyListener(extension_id, key, modifiers, callback) {
      this._xdm.unregisterKeyListener(extension_id, key, modifiers, callback);
    };
    _proto.registerClickHandler = function registerClickHandler(callback) {
      this._xdm.registerClickHandler(callback);
    };
    _proto.unregisterClickHandler = function unregisterClickHandler() {
      this._xdm.unregisterClickHandler();
    };
    _proto.defineModule = function defineModule(moduleName, module, options) {
      this._xdm.defineAPIModule(module, moduleName, options);
    };
    _proto.isModuleDefined = function isModuleDefined(moduleName) {
      return this._xdm.isAPIModuleDefined(moduleName);
    };
    _proto.defineGlobals = function defineGlobals(module) {
      this._xdm.defineAPIModule(module);
    };
    _proto.getExtensions = function getExtensions(filter) {
      return this._xdm.getRegisteredExtensions(filter);
    };
    _proto.unregisterExtension = function unregisterExtension(filter) {
      return this._xdm.unregisterExtension(filter);
    };
    _proto.returnsPromise = function returnsPromise(wrappedMethod) {
      wrappedMethod.returnsPromise = true;
    };
    _proto.setFeatureFlagGetter = function setFeatureFlagGetter(getBooleanFeatureFlag) {
      this._xdm.setFeatureFlagGetter(getBooleanFeatureFlag);
    };
    _proto.registerExistingExtension = function registerExistingExtension(extension_id, data) {
      return this._xdm.registerExtension(extension_id, data);
    };
    return Connect;
  }();

  function _extends() {
    _extends = Object.assign ? Object.assign.bind() : function (target) {
      for (var i = 1; i < arguments.length; i++) {
        var source = arguments[i];
        for (var key in source) {
          if (Object.prototype.hasOwnProperty.call(source, key)) {
            target[key] = source[key];
          }
        }
      }
      return target;
    };
    return _extends.apply(this, arguments);
  }

  var _each$1 = util.each,
    document$2 = window.document;
  function $$2(sel, context) {
    context = context || document$2;
    var els = [];
    if (sel) {
      if (typeof sel === 'string') {
        var results = context.querySelectorAll(sel),
          arr_results = Array.prototype.slice.call(results);
        Array.prototype.push.apply(els, arr_results);
      } else if (sel.nodeType === 1) {
        els.push(sel);
      } else if (sel === window) {
        els.push(sel);
      } else if (typeof sel === 'function') {
        $$2.onDomLoad(sel);
      }
    }
    util.extend(els, {
      each: function each(it) {
        _each$1(this, it);
        return this;
      },
      bind: function bind(name, callback) {
        this.each(function (i, el) {
          this.bind(el, name, callback);
        });
      },
      attr: function attr(k) {
        var v;
        this.each(function (i, el) {
          v = el[k] || el.getAttribute && el.getAttribute(k);
          return !v;
        });
        return v;
      },
      removeClass: function removeClass(className) {
        return this.each(function (i, el) {
          if (el.className) {
            el.className = el.className.replace(new RegExp('(^|\\s)' + className + '(\\s|$)'), ' ');
          }
        });
      },
      html: function html(_html) {
        return this.each(function (i, el) {
          el.innerHTML = _html;
        });
      },
      append: function append(spec) {
        return this.each(function (i, to) {
          var el = context.createElement(spec.tag);
          _each$1(spec, function (k, v) {
            if (k === '$text') {
              if (el.styleSheet) {
                // style tags in ie
                el.styleSheet.cssText = v;
              } else {
                el.appendChild(context.createTextNode(v));
              }
            } else if (k !== 'tag') {
              el[k] = v;
            }
          });
          to.appendChild(el);
        });
      }
    });
    return els;
  }
  function binder$1(std, odd) {
    std += 'EventListener';
    odd += 'Event';
    return function (el, e, fn) {
      if (el[std]) {
        el[std](e, fn, false);
      } else if (el[odd]) {
        el[odd]('on' + e, fn);
      }
    };
  }
  $$2.bind = binder$1('add', 'attach');
  $$2.unbind = binder$1('remove', 'detach');
  $$2.onDomLoad = function (func) {
    var w = window,
      readyState = w.document.readyState;
    if (readyState === "complete") {
      func.call(w);
    } else {
      $$2.bind(w, "load", function () {
        func.call(w);
      });
    }
  };

  function getContainer() {
    // Look for these two selectors first... you need these to allow for the auto-shrink to work
    // Otherwise, it'll default to document.body which can't auto-grow or auto-shrink
    var container = $$2('.ac-content, #content');
    return container.length > 0 ? container[0] : document.body;
  }

  /**
  * Extension wide configuration values
  */
  var ConfigurationOptions = /*#__PURE__*/function () {
    function ConfigurationOptions() {
      this.options = {};
    }
    var _proto = ConfigurationOptions.prototype;
    _proto._flush = function _flush() {
      this.options = {};
    };
    _proto.get = function get(item) {
      return item ? this.options[item] : this.options;
    };
    _proto.set = function set(data, value) {
      var _this = this;
      if (!data) {
        return;
      }
      if (value) {
        var _data;
        data = (_data = {}, _data[data] = value, _data);
      }
      var keys = Object.getOwnPropertyNames(data);
      keys.forEach(function (key) {
        _this.options[key] = data[key];
      }, this);
    };
    return ConfigurationOptions;
  }();
  var ConfigurationOptions$1 = new ConfigurationOptions();

  var size = function size(width, height, container) {
    var verticalScrollbarWidth = function verticalScrollbarWidth() {
      var sbWidth = window.innerWidth - container.clientWidth;
      // sanity check only
      sbWidth = sbWidth < 0 ? 0 : sbWidth;
      sbWidth = sbWidth > 50 ? 50 : sbWidth;
      return sbWidth;
    };
    var horizontalScrollbarHeight = function horizontalScrollbarHeight() {
      var sbHeight = window.innerHeight - Math.min(container.clientHeight, document.documentElement.clientHeight);
      // sanity check only
      sbHeight = sbHeight < 0 ? 0 : sbHeight;
      sbHeight = sbHeight > 50 ? 50 : sbHeight;
      return sbHeight;
    };
    var w = width == null ? '100%' : width,
      h,
      docHeight;
    var widthInPx = Boolean(ConfigurationOptions$1.get('widthinpx'));
    container = container || getContainer();
    if (!container) {
      util.warn('size called before container or body appeared, ignoring');
    }
    if (widthInPx && typeof w === "string" && w.search('%') !== -1) {
      w = Math.max(container.scrollWidth, container.offsetWidth, container.clientWidth);
    }
    if (height) {
      h = height;
    } else {
      // Determine height of document element
      docHeight = Math.max(container.scrollHeight, document.documentElement.scrollHeight, container.offsetHeight, document.documentElement.offsetHeight, container.clientHeight, document.documentElement.clientHeight);
      if (container === document.body) {
        h = docHeight;
      } else {
        var computed = window.getComputedStyle(container);
        h = container.getBoundingClientRect().height;
        if (h === 0) {
          h = docHeight;
        } else {
          var additionalProperties = ['margin-top', 'margin-bottom'];
          additionalProperties.forEach(function (property) {
            var floated = parseFloat(computed[property]);
            h += floated;
          });
        }
      }
    }

    // Include iframe scroll bars if visible and using exact dimensions
    w = typeof w === 'number' && Math.min(container.scrollHeight, document.documentElement.scrollHeight) > Math.min(container.clientHeight, document.documentElement.clientHeight) ? w + verticalScrollbarWidth() : w;
    h = typeof h === 'number' && container.scrollWidth > container.clientWidth ? h + horizontalScrollbarHeight() : h;
    return {
      w: w,
      h: h
    };
  };

  function EventQueue() {
    this.q = [];
    this.add = function (ev) {
      this.q.push(ev);
    };
    var i, j;
    this.call = function () {
      for (i = 0, j = this.q.length; i < j; i++) {
        this.q[i].call();
      }
    };
  }
  function attachResizeEvent(element, resized) {
    if (!element.resizedAttached) {
      element.resizedAttached = new EventQueue();
      element.resizedAttached.add(resized);
    } else if (element.resizedAttached) {
      element.resizedAttached.add(resized);
      return;
    }

    // padding / margins on the body causes numerous resizing bugs.
    if (element.nodeName === 'BODY') {
      ['padding', 'margin'].forEach(function (attr) {
        element.style[attr + '-bottom'] = '0px';
        element.style[attr + '-top'] = '0px';
      }, this);
    }
    element.resizeSensor = document.createElement('div');
    element.resizeSensor.className = 'ac-resize-sensor';
    var style = 'position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: scroll; z-index: -1; visibility: hidden;';
    var styleChild = 'position: absolute; left: 0; top: 0;';
    element.resizeSensor.style.cssText = style;
    var expand = document.createElement('div');
    expand.className = "ac-resize-sensor-expand";
    expand.style.cssText = style;
    var expandChild = document.createElement('div');
    expand.appendChild(expandChild);
    expandChild.style.cssText = styleChild;
    var shrink = document.createElement('div');
    shrink.className = "ac-resize-sensor-shrink";
    shrink.style.cssText = style;
    var shrinkChild = document.createElement('div');
    shrink.appendChild(shrinkChild);
    shrinkChild.style.cssText = styleChild + ' width: 200%; height: 200%';
    element.resizeSensor.appendChild(expand);
    element.resizeSensor.appendChild(shrink);
    element.appendChild(element.resizeSensor);

    // https://bugzilla.mozilla.org/show_bug.cgi?id=548397
    // do not set body to relative
    if (element.nodeName !== 'BODY' && window.getComputedStyle && window.getComputedStyle(element).position === 'static') {
      element.style.position = 'relative';
    }
    var lastWidth, lastHeight;
    var reset = function reset() {
      expandChild.style.width = expand.offsetWidth + 10 + 'px';
      expandChild.style.height = expand.offsetHeight + 10 + 'px';
      expand.scrollLeft = expand.scrollWidth;
      expand.scrollTop = expand.scrollHeight;
      shrink.scrollLeft = shrink.scrollWidth;
      shrink.scrollTop = shrink.scrollHeight;
      lastWidth = element.offsetWidth;
      lastHeight = element.offsetHeight;
    };
    reset();
    var changed = function changed() {
      if (element.resizedAttached) {
        element.resizedAttached.call();
      }
    };
    var onScroll = function onScroll() {
      if (element.offsetWidth !== lastWidth || element.offsetHeight !== lastHeight) {
        changed();
      }
      reset();
    };
    expand.addEventListener('scroll', onScroll);
    shrink.addEventListener('scroll', onScroll);
    var observerConfig = {
      attributes: true,
      attributeFilter: ['style']
    };
    var observer = new MutationObserver(onScroll);
    element.resizeObserver = observer;
    observer.observe(element, observerConfig);
  }
  var resizeListener = {
    add: function add(fn) {
      var container = getContainer();
      attachResizeEvent(container, fn);
    },
    remove: function remove() {
      var container = getContainer();
      if (container.resizeSensor) {
        container.resizeObserver.disconnect();
        container.removeChild(container.resizeSensor);
        delete container.resizeSensor;
        delete container.resizedAttached;
      }
    }
  };

  var AutoResizeAction = /*#__PURE__*/function () {
    function AutoResizeAction(callback) {
      this.resizeError = util.throttle(function (msg) {
        console.info(msg);
      }, 1000);
      this.dimensionStores = {
        width: [],
        height: []
      };
      this.callback = callback;
    }
    var _proto = AutoResizeAction.prototype;
    _proto._setVal = function _setVal(val, type, time) {
      this.dimensionStores[type] = this.dimensionStores[type].filter(function (entry) {
        return time - entry.setAt < 400;
      });
      this.dimensionStores[type].push({
        val: parseInt(val, 10),
        setAt: time
      });
    };
    _proto._isFlicker = function _isFlicker(val, type) {
      return this.dimensionStores[type].length >= 5;
    };
    _proto.triggered = function triggered(dimensions) {
      dimensions = dimensions || size();
      var now = Date.now();
      this._setVal(dimensions.w, 'width', now);
      this._setVal(dimensions.h, 'height', now);
      var isFlickerWidth = this._isFlicker(dimensions.w, 'width', now);
      var isFlickerHeight = this._isFlicker(dimensions.h, 'height', now);
      if (isFlickerWidth) {
        dimensions.w = "100%";
        this.resizeError("SIMPLE XDM: auto resize flickering width detected, setting to 100%");
      }
      if (isFlickerHeight) {
        var vals = this.dimensionStores['height'].map(function (x) {
          return x.val;
        });
        dimensions.h = Math.max.apply(null, vals) + 'px';
        this.resizeError("SIMPLE XDM: auto resize flickering height detected, setting to: " + dimensions.h);
      }
      this.callback(dimensions.w, dimensions.h);
    };
    return AutoResizeAction;
  }();

  var ConsumerOptions = /*#__PURE__*/function () {
    function ConsumerOptions() {}
    var _proto = ConsumerOptions.prototype;
    _proto._elementExists = function _elementExists($el) {
      return $el && $el.length === 1;
    };
    _proto._elementOptions = function _elementOptions($el) {
      return $el.attr("data-options");
    };
    _proto._getConsumerOptions = function _getConsumerOptions() {
      var options = {},
        $optionElement = $$2("#ac-iframe-options"),
        $scriptElement = $$2("script[src*='/atlassian-connect/all']"),
        $cdnScriptElement = $$2("script[src*='/connect-cdn.atl-paas.net/all']");
      if (!this._elementExists($optionElement) || !this._elementOptions($optionElement)) {
        if (this._elementExists($scriptElement)) {
          $optionElement = $scriptElement;
        } else if (this._elementExists($cdnScriptElement)) {
          $optionElement = $cdnScriptElement;
        }
      }
      if (this._elementExists($optionElement)) {
        // get its data-options attribute, if any
        var optStr = this._elementOptions($optionElement);
        if (optStr) {
          // if found, parse the value into kv pairs following the format of a style element
          optStr.split(";").forEach(function (nvpair) {
            nvpair = nvpair.trim();
            if (nvpair) {
              var nv = nvpair.split(":"),
                k = nv[0].trim(),
                v = nv[1].trim();
              if (k && v != null) {
                options[k] = v === "true" || v === "false" ? v === "true" : v;
              }
            }
          });
        }
      }
      return options;
    };
    _proto._flush = function _flush() {
      delete this._options;
    };
    _proto.get = function get(key) {
      if (!this._options) {
        this._options = this._getConsumerOptions();
      }
      if (key) {
        return this._options[key];
      }
      return this._options;
    };
    return ConsumerOptions;
  }();
  var consumerOptions = new ConsumerOptions();

  var POSSIBLE_MODIFIER_KEYS = ['ctrl', 'shift', 'alt', 'meta'];
  var AP = /*#__PURE__*/function (_PostMessage) {
    _inheritsLoose(AP, _PostMessage);
    function AP(options, initCheck) {
      var _this;
      if (initCheck === void 0) {
        initCheck = true;
      }
      _this = _PostMessage.call(this) || this;
      ConfigurationOptions$1.set(options);
      _this._data = _this._parseInitData();
      ConfigurationOptions$1.set(_this._data.options);
      _this._data.options = _this._data.options || {};
      _this._hostOrigin = _this._data.options.hostOrigin || '*';
      _this._top = window.top;
      _this._host = window.parent || window;
      _this._topHost = _this._getHostFrame(_this._data.options.hostFrameOffset);
      if (_this._topHost !== _this._top) {
        _this._verifyHostFrameOffset();
      }
      _this._initTimeout = 5000;
      _this._initReceived = false;
      _this._initCheck = initCheck;
      _this._isKeyDownBound = false;
      _this._hostModules = {};
      _this._eventHandlers = {};
      _this._pendingCallbacks = {};
      _this._keyListeners = [];
      _this._version = "5.3.179";
      _this._apiTampered = undefined;
      _this._isSubIframe = _this._topHost !== window.parent;
      _this._onConfirmedFns = [];
      _this._promise = Promise;
      if (_this._data.api) {
        _this._setupAPI(_this._data.api);
        _this._setupAPIWithoutRequire(_this._data.api);
      }
      _this._messageHandlers = {
        init_received: _this._handleInitReceived,
        resp: _this._handleResponse,
        evt: _this._handleEvent,
        key_listen: _this._handleKeyListen,
        api_tamper: _this._handleApiTamper
      };
      if (_this._data.origin) {
        _this._sendInit(_this._host, _this._data.origin);
        if (_this._isSubIframe) {
          _this._sendInit(_this._topHost, _this._hostOrigin);
        }
      }
      _this._registerOnUnload();
      _this.resize = util._bind(_assertThisInitialized(_this), function (width, height) {
        if (!getContainer()) {
          util.warn('resize called before container or body appeared, ignoring');
          return;
        }
        var dimensions = size();
        if (!width) {
          width = dimensions.w;
        }
        if (!height) {
          height = dimensions.h;
        }
        if (_this._hostModules.env && _this._hostModules.env.resize) {
          _this._hostModules.env.resize(width, height);
        }
      });
      $$2(util._bind(_assertThisInitialized(_this), _this._autoResizer));
      _this.container = getContainer;
      _this.size = size;
      window.addEventListener('click', function (e) {
        _this._host.postMessage({
          eid: _this._data.extension_id,
          type: 'addon_clicked'
        }, _this._hostOrigin);
      });
      return _this;
    }
    var _proto = AP.prototype;
    _proto._getHostFrame = function _getHostFrame(offset) {
      // Climb up the iframe tree to find the real host
      if (offset && typeof offset === 'number') {
        var hostFrame = window;
        for (var i = 0; i < offset; i++) {
          hostFrame = hostFrame.parent;
        }
        return hostFrame;
      } else {
        return this._top;
      }
    };
    _proto._verifyHostFrameOffset = function _verifyHostFrameOffset() {
      var _this2 = this;
      // Asynchronously verify the host frame option with this._top
      var callback = function callback(e) {
        if (e.source === _this2._top && e.data && typeof e.data.hostFrameOffset === 'number') {
          window.removeEventListener('message', callback);
          if (_this2._getHostFrame(e.data.hostFrameOffset) !== _this2._topHost) {
            util.error('hostFrameOffset tampering detected, setting host frame to top window');
            _this2._topHost = _this2._top;
          }
        }
      };
      window.addEventListener('message', callback);
      this._top.postMessage({
        type: 'get_host_offset'
      }, this._hostOrigin);
    };
    _proto._handleApiTamper = function _handleApiTamper(event) {
      if (event.data.tampered !== false) {
        this._host = undefined;
        this._apiTampered = true;
        util.error('XDM API tampering detected, api disabled');
      } else {
        this._apiTampered = false;
        this._onConfirmedFns.forEach(function (cb) {
          cb.apply(null);
        });
      }
      this._onConfirmedFns = [];
    };
    _proto._registerOnUnload = function _registerOnUnload() {
      $$2.bind(window, 'unload', util._bind(this, function () {
        this._sendUnload(this._host, this._data.origin);
        if (this._isSubIframe) {
          this._sendUnload(this._topHost, this._hostOrigin);
        }
      }));
    };
    _proto._sendUnload = function _sendUnload(frame, origin) {
      frame.postMessage({
        eid: this._data.extension_id,
        type: 'unload'
      }, origin || '*');
    };
    _proto._bindKeyDown = function _bindKeyDown() {
      if (!this._isKeyDownBound) {
        $$2.bind(window, 'keydown', util._bind(this, this._handleKeyDownDomEvent));
        this._isKeyDownBound = true;
      }
    };
    _proto._autoResizer = function _autoResizer() {
      this._enableAutoResize = Boolean(ConfigurationOptions$1.get('autoresize'));
      if (consumerOptions.get('resize') === false || consumerOptions.get('sizeToParent') === true) {
        this._enableAutoResize = false;
      }
      if (this._enableAutoResize) {
        this._initResize();
      }
    }

    /**
    * The initialization data is passed in when the iframe is created as its 'name' attribute.
    * Example:
    * {
    *   extension_id: The ID of this iframe as defined by the host
    *   origin: 'https://example.org'  // The parent's window origin
    *   api: {
    *     _globals: { ... },
    *     messages = {
    *       clear: {},
    *       ...
    *     },
    *     ...
    *   }
    * }
    **/;
    _proto._parseInitData = function _parseInitData(data) {
      try {
        return JSON.parse(data || window.name);
      } catch (e) {
        return {};
      }
    };
    _proto._findTarget = function _findTarget(moduleName, methodName) {
      return this._data.options && this._data.options.targets && this._data.options.targets[moduleName] && this._data.options.targets[moduleName][methodName] ? this._data.options.targets[moduleName][methodName] : 'top';
    };
    _proto._createModule = function _createModule(moduleName, api) {
      var _this3 = this;
      return Object.getOwnPropertyNames(api).reduce(function (accumulator, memberName) {
        var member = api[memberName];
        if (member.hasOwnProperty('constructor')) {
          accumulator[memberName] = _this3._createProxy(moduleName, member, memberName);
        } else {
          accumulator[memberName] = _this3._createMethodHandler({
            mod: moduleName,
            fn: memberName,
            returnsPromise: member.returnsPromise
          });
        }
        return accumulator;
      }, {});
    };
    _proto._setupAPI = function _setupAPI(api) {
      var _this4 = this;
      this._hostModules = Object.getOwnPropertyNames(api).reduce(function (accumulator, moduleName) {
        accumulator[moduleName] = _this4._createModule(moduleName, api[moduleName], api[moduleName]._options);
        return accumulator;
      }, {});
      Object.getOwnPropertyNames(this._hostModules._globals || {}).forEach(function (global) {
        _this4[global] = _this4._hostModules._globals[global];
      });
    };
    _proto._setupAPIWithoutRequire = function _setupAPIWithoutRequire(api) {
      var _this5 = this;
      Object.getOwnPropertyNames(api).forEach(function (moduleName) {
        if (typeof _this5[moduleName] !== "undefined") {
          throw new Error('XDM module: ' + moduleName + ' will collide with existing variable');
        }
        _this5[moduleName] = _this5._createModule(moduleName, api[moduleName]);
      }, this);
    };
    _proto._pendingCallback = function _pendingCallback(mid, fn, metaData) {
      if (metaData) {
        Object.getOwnPropertyNames(metaData).forEach(function (metaDataName) {
          fn[metaDataName] = metaData[metaDataName];
        });
      }
      this._pendingCallbacks[mid] = fn;
    };
    _proto._createProxy = function _createProxy(moduleName, api, className) {
      var module = this._createModule(moduleName, api);
      function Cls(args) {
        if (!(this instanceof Cls)) {
          return new Cls(arguments);
        }
        this._cls = className;
        this._id = util.randomString();
        module.constructor.apply(this, args);
        return this;
      }
      Object.getOwnPropertyNames(module).forEach(function (methodName) {
        if (methodName !== 'constructor') {
          Cls.prototype[methodName] = module[methodName];
        }
      });
      return Cls;
    };
    _proto._createMethodHandler = function _createMethodHandler(methodData) {
      var that = this;
      return function () {
        var args = util.argumentsToArray(arguments);
        var data = {
          eid: that._data.extension_id,
          type: 'req',
          mod: methodData.mod,
          fn: methodData.fn
        };
        var targetOrigin;
        var target;
        var xdmPromise;
        var mid = util.randomString();
        if (that._findTarget(methodData.mod, methodData.fn) === 'top') {
          target = that._topHost;
          targetOrigin = that._hostOrigin;
        } else {
          target = that._host;
          targetOrigin = that._data.origin;
        }
        if (util.hasCallback(args)) {
          data.mid = mid;
          that._pendingCallback(data.mid, args.pop(), {
            useCallback: true,
            isPromiseMethod: Boolean(methodData.returnsPromise)
          });
        } else if (methodData.returnsPromise) {
          data.mid = mid;
          xdmPromise = new Promise(function (resolve, reject) {
            that._pendingCallback(data.mid, function (err, result) {
              if (err || typeof result === 'undefined' && typeof err === 'undefined') {
                reject(err);
              } else {
                resolve(result);
              }
            }, {
              useCallback: false,
              isPromiseMethod: Boolean(methodData.returnsPromise)
            });
          });
          xdmPromise.catch(function (err) {
            util.warn("Failed promise: " + err);
          });
        }
        if (this && this._cls) {
          data._cls = this._cls;
          data._id = this._id;
        }
        data.args = util.sanitizeStructuredClone(args);
        if (that._isSubIframe && typeof that._apiTampered === 'undefined') {
          that._onConfirmedFns.push(function () {
            target.postMessage(data, targetOrigin);
          });
        } else {
          target.postMessage(data, targetOrigin);
        }
        if (xdmPromise) {
          return xdmPromise;
        }
      };
    };
    _proto._handleResponse = function _handleResponse(event) {
      var data = event.data;
      if (!data.forPlugin) {
        return;
      }
      var pendingCallback = this._pendingCallbacks[data.mid];
      if (pendingCallback) {
        delete this._pendingCallbacks[data.mid];
        try {
          // Promise methods always return error result as first arg
          // If a promise method is invoked using callbacks, strip first arg.
          if (pendingCallback.useCallback && pendingCallback.isPromiseMethod) {
            data.args.shift();
          }
          pendingCallback.apply(window, data.args);
        } catch (e) {
          util.error(e.message, e.stack);
        }
      }
    };
    _proto._handleEvent = function _handleEvent(event) {
      var sendResponse = function sendResponse() {
        var args = util.argumentsToArray(arguments);
        event.source.postMessage({
          eid: this._data.extension_id,
          mid: event.data.mid,
          type: 'resp',
          args: args
        }, this._data.origin);
      };
      var data = event.data;
      sendResponse = util._bind(this, sendResponse);
      sendResponse._context = {
        eventName: data.etyp
      };
      function toArray(handlers) {
        if (handlers) {
          if (!Array.isArray(handlers)) {
            handlers = [handlers];
          }
          return handlers;
        }
        return [];
      }
      var handlers = toArray(this._eventHandlers[data.etyp]);
      handlers = handlers.concat(toArray(this._eventHandlers._any));
      handlers.forEach(function (handler) {
        try {
          handler(data.evnt, sendResponse);
        } catch (e) {
          util.error('exception thrown in event callback for:' + data.etyp);
        }
      }, this);
      if (data.mid) {
        sendResponse();
      }
    };
    _proto._handleKeyDownDomEvent = function _handleKeyDownDomEvent(event) {
      var modifiers = [];
      POSSIBLE_MODIFIER_KEYS.forEach(function (modifierKey) {
        if (event[modifierKey + 'Key']) {
          modifiers.push(modifierKey);
        }
      }, this);
      var keyListenerId = this._keyListenerId(event.keyCode, modifiers);
      var requestedKey = this._keyListeners.indexOf(keyListenerId);
      if (requestedKey >= 0) {
        this._host.postMessage({
          eid: this._data.extension_id,
          keycode: event.keyCode,
          modifiers: modifiers,
          type: 'key_triggered'
        }, this._data.origin);
      }
    };
    _proto._keyListenerId = function _keyListenerId(keycode, modifiers) {
      var keyListenerId = keycode;
      if (modifiers) {
        if (typeof modifiers === "string") {
          modifiers = [modifiers];
        }
        modifiers.sort();
        modifiers.forEach(function (modifier) {
          keyListenerId += '$$' + modifier;
        }, this);
      }
      return keyListenerId;
    };
    _proto._handleKeyListen = function _handleKeyListen(postMessageEvent) {
      var keyListenerId = this._keyListenerId(postMessageEvent.data.keycode, postMessageEvent.data.modifiers);
      if (postMessageEvent.data.action === "remove") {
        var index = this._keyListeners.indexOf(keyListenerId);
        this._keyListeners.splice(index, 1);
      } else if (postMessageEvent.data.action === "add") {
        // only bind onKeyDown once a key is registered.
        this._bindKeyDown();
        this._keyListeners.push(keyListenerId);
      }
    };
    _proto._checkOrigin = function _checkOrigin(event) {
      var no_source_types = ['api_tamper'];
      if (event.data && no_source_types.indexOf(event.data.type) > -1) {
        return true;
      }
      if (this._isSubIframe && event.source === this._topHost) {
        return true;
      }
      return event.origin === this._data.origin && event.source === this._host;
    };
    _proto._handleInitReceived = function _handleInitReceived() {
      this._initReceived = true;
    };
    _proto._sendInit = function _sendInit(frame, origin) {
      var _this6 = this;
      var targets;
      if (frame === this._topHost && this._topHost !== window.parent) {
        targets = ConfigurationOptions$1.get('targets');
      }
      frame.postMessage({
        eid: this._data.extension_id,
        type: 'init',
        targets: targets
      }, origin || '*');
      this._initCheck && this._data.options.globalOptions.check_init && setTimeout(function () {
        if (!_this6._initReceived) {
          throw new Error("Initialization message not received");
        }
      }, this._initTimeout);
    };
    _proto.broadcast = function broadcast(event, evnt) {
      if (!util.isString(event)) {
        throw new Error("Event type must be string");
      }
      this._host.postMessage({
        eid: this._data.extension_id,
        type: 'broadcast',
        etyp: event,
        evnt: evnt
      }, this._data.origin);
    };
    _proto.require = function require(modules, callback) {
      var _this7 = this;
      var requiredModules = Array.isArray(modules) ? modules : [modules],
        args = requiredModules.map(function (module) {
          return _this7._hostModules[module] || _this7._hostModules._globals[module];
        });
      callback.apply(window, args);
    };
    _proto.register = function register(handlers) {
      if (typeof handlers === "object") {
        this._eventHandlers = _extends({}, this._eventHandlers, handlers) || {};
        this._host.postMessage({
          eid: this._data.extension_id,
          type: 'event_query',
          args: Object.getOwnPropertyNames(handlers)
        }, this._data.origin);
      }
    };
    _proto.registerAny = function registerAny(handlers) {
      this.register({
        '_any': handlers
      });
    };
    _proto._initResize = function _initResize() {
      var _this8 = this;
      requestAnimationFrame(function () {
        return _this8.resize();
      });
      var autoresize = new AutoResizeAction(this.resize);
      resizeListener.add(util._bind(autoresize, autoresize.triggered));
    };
    return AP;
  }(PostMessage);

  var Combined = /*#__PURE__*/function (_Host) {
    _inheritsLoose(Combined, _Host);
    function Combined(initCheck) {
      var _this;
      _this = _Host.call(this) || this;
      _this.parentTargets = {
        _globals: {}
      };
      var plugin = new AP(undefined, initCheck);
      // export options from plugin to host.
      Object.getOwnPropertyNames(plugin).forEach(function (prop) {
        if (['_hostModules', '_globals'].indexOf(prop) === -1 && this[prop] === undefined) {
          this[prop] = plugin[prop];
        }
      }, _assertThisInitialized(_this));
      ['registerAny', 'register'].forEach(function (prop) {
        this[prop] = Object.getPrototypeOf(plugin)[prop].bind(plugin);
      }, _assertThisInitialized(_this));

      //write plugin modules to host.
      var moduleSpec = plugin._data.api;
      if (typeof moduleSpec === 'object') {
        Object.getOwnPropertyNames(moduleSpec).forEach(function (moduleName) {
          var accumulator = {};
          Object.getOwnPropertyNames(moduleSpec[moduleName]).forEach(function (methodName) {
            // class proxies
            if (moduleSpec[moduleName][methodName].hasOwnProperty('constructor')) {
              accumulator[methodName] = plugin._hostModules[moduleName][methodName].prototype;
            } else {
              // all other methods
              accumulator[methodName] = plugin._hostModules[moduleName][methodName];
              accumulator[methodName]['returnsPromise'] = moduleSpec[moduleName][methodName]['returnsPromise'] || false;
            }
          }, this);
          this._xdm.defineAPIModule(accumulator, moduleName);
        }, _assertThisInitialized(_this));
      }
      _this._hostModules = plugin._hostModules;
      _this.defineGlobal = function (module) {
        this.parentTargets['_globals'] = util.extend({}, this.parentTargets['_globals'], module);
        this._xdm.defineAPIModule(module);
      };
      _this.defineModule = function (moduleName, module) {
        this._xdm.defineAPIModule(module, moduleName);
        this.parentTargets[moduleName] = {};
        Object.getOwnPropertyNames(module).forEach(function (name) {
          this.parentTargets[moduleName][name] = 'parent';
        }, this);
      };
      _this.subCreate = function (extensionOptions, initCallback) {
        extensionOptions.options = extensionOptions.options || {};
        extensionOptions.options.targets = util.extend({}, this.parentTargets, extensionOptions.options.targets);
        var extension = this.create(extensionOptions, initCallback);
        return extension;
      };
      return _this;
    }
    return Combined;
  }(Connect);

  var combined = new Combined();

  function deprecate (fn, name, alternate, sinceVersion) {
    var called = false;
    return function () {
      if (!called && typeof console !== 'undefined' && console.warn) {
        called = true;
        console.warn("DEPRECATED API - " + name + " has been deprecated " + (sinceVersion ? "since ACJS " + sinceVersion : 'in ACJS') + (" and will be removed in a future release. " + (alternate ? "Use " + alternate + " instead." : 'No alternative will be provided.')));
        if (combined._analytics) {
          combined._analytics.trackDeprecatedMethodUsed(name);
        }
      }
      return fn.apply(void 0, arguments);
    };
  }

  // universal iterator utility
  function each(o, it) {
    var l;
    var k;
    if (o) {
      l = o.length;
      if (l != null && typeof o !== 'function') {
        k = 0;
        while (k < l) {
          if (it.call(o[k], k, o[k]) === false) {
            break;
          }
          k += 1;
        }
      } else {
        for (k in o) {
          if (o.hasOwnProperty(k) && it.call(o[k], k, o[k]) === false) {
            break;
          }
        }
      }
    }
  }
  function binder(std, odd) {
    std += 'EventListener';
    odd += 'Event';
    return function (el, e, fn) {
      if (el[std]) {
        el[std](e, fn, false);
      } else if (el[odd]) {
        el[odd]('on' + e, fn);
      }
    };
  }
  function log() {
    var console = this.console;
    if (console && console.log) {
      var args = [].slice.call(arguments);
      if (console.log.apply) {
        console.log.apply(console, args);
      } else {
        for (var i = 0, l = args.length; i < l; i += 1) {
          args[i] = JSON.stringify(args[i]);
        }
        console.log(args.join(' '));
      }
      return true;
    }
  }
  function decodeQueryComponent(encodedURI) {
    return encodedURI == null ? null : decodeURIComponent(encodedURI.replace(/\+/g, '%20'));
  }
  var _util = {
    each: each,
    log: log,
    decodeQueryComponent: decodeQueryComponent,
    bind: binder('add', 'attach'),
    unbind: binder('remove', 'detach'),
    extend: function extend(dest) {
      var args = arguments;
      var srcs = [].slice.call(args, 1, args.length);
      each(srcs, function (i, src) {
        each(src, function (k, v) {
          dest[k] = v;
        });
      });
      return dest;
    },
    trim: function trim(s) {
      return s && s.replace(/^\s+|\s+$/g, '');
    },
    debounce: function debounce(fn, wait) {
      var timeout;
      return function () {
        var ctx = this;
        var args = [].slice.call(arguments);
        function later() {
          timeout = null;
          fn.apply(ctx, args);
        }
        if (timeout) {
          clearTimeout(timeout);
        }
        timeout = setTimeout(later, wait || 50);
      };
    },
    isFunction: function isFunction(fn) {
      return typeof fn === 'function';
    },
    handleError: function handleError(err) {
      if (!log.apply(this, err && err.message ? [err, err.message] : [err])) {
        throw err;
      }
    }
  };

  var _each = _util.each;
  var extend = _util.extend;
  var document$1 = window.document;
  function $(sel, context) {
    context = context || document$1;
    var els = [];
    if (sel) {
      if (typeof sel === 'string') {
        var results = context.querySelectorAll(sel);
        _each(results, function (i, v) {
          els.push(v);
        });
      } else if (sel.nodeType === 1) {
        els.push(sel);
      } else if (sel === window) {
        els.push(sel);
      }
    }
    extend(els, {
      each: function each(it) {
        _each(this, it);
        return this;
      },
      bind: function bind(name, callback) {
        this.each(function (i, el) {
          _util.bind(el, name, callback);
        });
      },
      attr: function attr(k) {
        var v;
        this.each(function (i, el) {
          v = el[k] || el.getAttribute && el.getAttribute(k);
          return !v;
        });
        return v;
      },
      removeClass: function removeClass(className) {
        return this.each(function (i, el) {
          if (el.className) {
            el.className = el.className.replace(new RegExp('(^|\\s)' + className + '(\\s|$)'), ' ');
          }
        });
      },
      html: function html(_html) {
        return this.each(function (i, el) {
          el.innerHTML = _html;
        });
      },
      append: function append(spec) {
        return this.each(function (i, to) {
          var el = context.createElement(spec.tag);
          _each(spec, function (k, v) {
            if (k === '$text') {
              if (el.styleSheet) {
                // style tags in ie
                el.styleSheet.cssText = v;
              } else {
                el.appendChild(context.createTextNode(v));
              }
            } else if (k !== 'tag') {
              el[k] = v;
            }
          });
          to.appendChild(el);
        });
      }
    });
    return els;
  }
  var $$1 = extend($, _util);

  /**
   * The Events module provides a mechanism for emitting and receiving events.<br>
   *
   * A event emitted by `emit` method will only be received by the modules defined in the same add-on.<br>
   * Public events that emitted by `emitPublic` are used for cross add-on communication.
   * They can be received by any add-on modules that are currently presented on the page.
   *
   * This module is currently not supported on mobile. See
   * <a href='https://jira.atlassian.com/browse/JRACLOUD-79910'>JRACLOUD-79910</a> for more details.
   *
   * <h3>Basic example</h3>
   * Add-on A:
   * ```
   * // The following will create an alert message every time the event `customEvent` is triggered.
   * AP.events.on('customEvent', function(){
   *   alert('event fired');
   * });
   *
   *
   * AP.events.emit('customEvent');
   * AP.events.emitPublic('customPublicEvent');
   * ```
   *
   *
   * Add-on B:
   * ```
   * // The following will create an alert message every time the event `customPublicEvent` is triggered by add-on A.
   * AP.events.onPublic('customPublicEvent', function(){
   *   alert('public event fired');
   * });
   * ```
   *
   * @name Events
   * @module
   */
  var Events = /*#__PURE__*/function () {
    function Events() {
      this._events = {};
      this.ANY_PREFIX = '_any';
      this.methods = ['off', 'offAll', 'offAny', 'on', 'onAny', 'once'];
    }
    var _proto = Events.prototype;
    _proto._anyListener = function _anyListener(data, callback) {
      var eventName = callback._context.eventName;
      var any = this._events[this.ANY_PREFIX] || [];
      var byName = this._events[eventName] || [];
      if (!Array.isArray(data)) {
        data = [data];
      }
      any.forEach(function (handler) {
        //clone data before modifying
        var args = data.slice(0);
        args.unshift(eventName);
        args.push({
          args: data,
          name: eventName
        });
        handler.apply(null, args);
      });
      byName.forEach(function (handler) {
        handler.apply(null, data);
      });
    };
    _proto.off = function off(name, listener) {
      if (this._events[name]) {
        var index = this._events[name].indexOf(listener);
        if (index > -1) {
          this._events[name].splice(index, 1);
        }
        if (this._events[name].length === 0) {
          delete this._events[name];
        }
      }
    };
    _proto.offAll = function offAll(name) {
      delete this._events[name];
    };
    _proto.offAny = function offAny(listener) {
      this.off(this.ANY_PREFIX, listener);
    };
    _proto.on = function on(name, listener) {
      if (!this._events[name]) {
        this._events[name] = [];
      }
      this._events[name].push(listener);
    };
    _proto.onAny = function onAny(listener) {
      this.on(this.ANY_PREFIX, listener);
    };
    _proto.once = function once(name, listener) {
      var _that = this;
      function runOnce() {
        listener.apply(null, arguments);
        _that.off(name, runOnce);
      }
      this.on(name, runOnce);
    }
    /**
     * Adds a listener for all occurrences of an event of a particular name.<br>
     * Listener arguments include any arguments passed to `events.emit`, followed by an object describing the complete event information.
     * @name on
     * @method
     * @memberof module:Events#
     * @param {String} name The event name to subscribe the listener to
     * @param {Function} listener A listener callback to subscribe to the event name
     */

    /**
     * Adds a listener for all occurrences of a public event of a particular name.<br>
     * Listener arguments include any arguments passed to `events.emitPublic`, followed by an object describing the complete event information.<br>
     * Event emitter's information will be passed to the first argument of the filter function. The listener callback will only be called when filter function returns `true`.
     * @name onPublic
     * @method
     * @memberof module:Events#
     * @param {String} name The event name to subscribe the listener to
     * @param {Function} listener A listener callback to subscribe to the event name
     * @param {Function} [filter] A filter function to filter the events. Callback will always be called when a matching event occurs if the filter is unspecified
     */

    /**
     * Adds a listener for one occurrence of an event of a particular name.<br>
     * Listener arguments include any argument passed to `events.emit`, followed by an object describing the complete event information.
     * @name once
     * @method
     * @memberof module:Events#
     * @param {String} name The event name to subscribe the listener to
     * @param {Function} listener A listener callback to subscribe to the event name
     */

    /**
     * Adds a listener for one occurrence of a public event of a particular name.<br>
     * Listener arguments include any argument passed to `events.emit`, followed by an object describing the complete event information.<br>
     * Event emitter's information will be passed to the first argument of the filter function. The listener callback will only be called when filter function returns `true`.
     * @name oncePublic
     * @method
     * @memberof module:Events#
     * @param {String} name The event name to subscribe the listener to
     * @param {Function}listener A listener callback to subscribe to the event name
     * @param {Function} [filter] A filter function to filter the events. Callback will always be called when a matching event occurs if the filter is unspecified
     */

    /**
     * Adds a listener for all occurrences of any event, regardless of name.<br>
     * Listener arguments begin with the event name, followed by any arguments passed to `events.emit`, followed by an object describing the complete event information.
     * @name onAny
     * @method
     * @memberof module:Events#
     * @param {Function} listener A listener callback to subscribe for any event name
     */

    /**
     * Adds a listener for all occurrences of any event, regardless of name.<br>
     * Listener arguments begin with the event name, followed by any arguments passed to `events.emit`, followed by an object describing the complete event information.<br>
     * Event emitter's information will be passed to the first argument of the filter function. The listener callback will only be called when filter function returns `true`.
     * @name onAnyPublic
     * @method
     * @memberof module:Events#
     * @param {Function} listener A listener callback to subscribe for any event name
     * @param {Function} [filter] A filter function to filter the events. Callback will always be called when a matching event occurs if the filter is unspecified
     */

    /**
     * Removes a particular listener for an event.
     * @name off
     * @method
     * @memberof module:Events#
     * @param {String} name The event name to unsubscribe the listener from
     * @param {Function} listener The listener callback to unsubscribe from the event name
     */

    /**
     * Removes a particular listener for a public event.
     * @name offPublic
     * @method
     * @memberof module:Events#
     * @param {String} name The event name to unsubscribe the listener from
     * @param {Function} listener The listener callback to unsubscribe from the event name
     */

    /**
     * Removes all listeners from an event name, or unsubscribes all event-name-specific listeners
     * if no name if given.
     * @name offAll
     * @method
     * @memberof module:Events#
     * @param {String} [name] The event name to unsubscribe all listeners from
     */

    /**
     * Removes all listeners from a public event name, or unsubscribes all event-name-specific listeners for public events
     * if no name if given.
     * @name offAllPublic
     * @method
     * @memberof module:Events#
     * @param {String} [name] The event name to unsubscribe all listeners from
     */

    /**
     * Removes an `any` event listener.
     * @name offAny
     * @method
     * @memberof module:Events#
     * @param {Function} listener A listener callback to unsubscribe from any event name
     */

    /**
     * Removes an `anyPublic` event listener.
     * @name offAnyPublic
     * @method
     * @memberof module:Events#
     * @param {Function} listener A listener callback to unsubscribe from any event name
     */

    /**
     * Emits an event on this bus, firing listeners by name as well as all 'any' listeners.<br>
     * Arguments following the name parameter are captured and passed to listeners.
     * @name emit
     * @method
     * @memberof module:Events#
     * @param {String} name The name of event to emit
     * @param {String[]} args 0 or more additional data arguments to deliver with the event
     */

    /**
     * Emits a public event on this bus, firing listeners by name as well as all 'anyPublic' listeners.<br>
     * The event can be received by any add-on modules that are currently presented on the page.<br>
     * Arguments following the name parameter are captured and passed to listeners.
     * @name emitPublic
     * @method
     * @memberof module:Events#
     * @param {String} name The name of event to emit
     * @param {String[]} args 0 or more additional data arguments to deliver with the event
     */;
    return Events;
  }();

  var EventsInstance = new Events();

  var PublicEvents = /*#__PURE__*/function (_Events) {
    _inheritsLoose(PublicEvents, _Events);
    function PublicEvents() {
      var _this;
      _this = _Events.call(this) || this;
      _this.methods = ['offPublic', 'offAllPublic', 'offAnyPublic', 'onPublic', 'onAnyPublic', 'oncePublic'];
      return _this;
    }
    var _proto = PublicEvents.prototype;
    _proto._filterEval = function _filterEval(filter, toCompare) {
      var value = true;
      if (!filter) {
        return value;
      }
      switch (typeof filter) {
        case 'function':
          value = Boolean(filter.call(null, toCompare));
          break;
        case 'object':
          value = Object.getOwnPropertyNames(filter).every(function (prop) {
            return toCompare[prop] === filter[prop];
          });
          break;
      }
      return value;
    };
    _proto.once = function once(name, listener, filter) {
      var that = this;
      function runOnce(data) {
        listener.apply(null, data);
        that.off(name, runOnce);
      }
      this.on(name, runOnce, filter);
    };
    _proto.on = function on(name, listener, filter) {
      listener._wrapped = function (data) {
        if (this._filterEval(filter, data.sender)) {
          listener.apply(null, data.event);
        }
      }.bind(this);
      _Events.prototype.on.call(this, name, listener._wrapped);
    };
    _proto.off = function off(name, listener) {
      if (listener._wrapped) {
        _Events.prototype.off.call(this, name, listener._wrapped);
      } else {
        _Events.prototype.off.call(this, name, listener);
      }
    };
    _proto.onAny = function onAny(listener, filter) {
      listener._wrapped = function (data) {
        if (data.sender && this._filterEval(filter, data.sender)) {
          listener.apply(null, data.event);
        }
      };
      _Events.prototype.onAny.call(this, listener._wrapped);
    };
    _proto.offAny = function offAny(listener) {
      if (listener._wrapped) {
        _Events.prototype.offAny.call(this, name, listener._wrapped);
      } else {
        _Events.prototype.offAny.call(this, name, listener);
      }
    };
    return PublicEvents;
  }(Events);
  var PublicEventsInstance = new PublicEvents();

  var customButtonIncrement = 1;
  var getCustomData = deprecate(function () {
    return combined._data.options.customData;
  }, 'AP.dialog.customData', 'AP.dialog.getCustomData()', '5.0');
  if (combined._hostModules && combined._hostModules.dialog) {
    /**
     * Returns the custom data Object passed to the dialog at creation.
     * @noDemo
     * @deprecated after August 2017 | Please use <code>dialog.getCustomData(callback)</code> instead.
     * @name customData
     * @memberOf module:Dialog
     * @ignore
     * @example
     * var myDataVariable = AP.dialog.customData.myDataVariable;
     *
     * @return {Object} Data Object passed to the dialog on creation.
     */
    Object.defineProperty(combined._hostModules.dialog, 'customData', {
      get: getCustomData
    });
    Object.defineProperty(combined.dialog, 'customData', {
      get: getCustomData
    });
    combined.dialog._disableCloseOnSubmit = false;
    combined.dialog.disableCloseOnSubmit = function () {
      combined.dialog._disableCloseOnSubmit = true;
    };
  }
  var dialogHandlers = {};
  EventsInstance.onAny(eventDelegator);
  function eventDelegator(name, args) {
    var dialogEventMatch = name.match(/^dialog\.(\w+)/);
    if (!dialogEventMatch) {
      return;
    }
    if (name === 'dialog.button.click') {
      customButtonEvent(args.button.identifier, args);
    } else {
      submitOrCancelEvent(dialogEventMatch[1], args);
    }
  }
  function customButtonEvent(buttonIdentifier, args) {
    var callbacks = dialogHandlers[buttonIdentifier];
    if (callbacks && callbacks.length !== 0) {
      try {
        callbacks.forEach(function (callback) {
          callback.call(null, args);
        });
      } catch (err) {
        console.error(err);
      }
    }
  }
  function submitOrCancelEvent(name, args) {
    var handlers = dialogHandlers[name];
    var shouldClose = name !== 'close';
    var context = null;
    // ignore events that are triggered by button clicks
    // allow dialog.close through for close on ESC
    if (shouldClose && typeof args.button === 'undefined') {
      return;
    }

    // if the submit button has been set to not close on click
    if (name === 'submit' && combined.dialog._disableCloseOnSubmit) {
      shouldClose = false;
    }
    try {
      if (handlers) {
        if (args && args.button && args.button.name) {
          context = combined.dialog.getButton(args.button.name);
        }
        shouldClose = handlers.reduce(function (result, cb) {
          return cb.call(context, args) && result;
        }, shouldClose);
      }
    } catch (err) {
      console.error(err);
    } finally {
      var shouldRemoveHandler = shouldClose || name === 'close';
      if (shouldRemoveHandler) {
        delete dialogHandlers[name];
      }
    }
    if (shouldClose) {
      combined.dialog.close();
    }
  }
  function registerHandler(event, callback) {
    if (typeof callback === 'function') {
      if (!dialogHandlers[event]) {
        dialogHandlers[event] = [];
      }
      dialogHandlers[event].push(callback);
    }
  }
  if (combined.dialog && combined.dialog.create) {
    var original_dialogCreate = combined.dialog.create.prototype.constructor.bind({});
    combined.dialog.create = combined._hostModules.dialog.create = function () {
      var dialog = original_dialogCreate.apply(void 0, arguments);
      /**
       * Allows the add-on to register a callback function for the given event. The listener is only called once and must be re-registered if needed.
       * @deprecated after August 2017 | Please use <code>AP.events.on("dialog.close", callback)</code> instead.
       * @memberOf Dialog~Dialog
       * @method on
       * @ignore
       * @param {String} event name of the event to listen for, such as 'close'.
       * @param {Function} callback function to receive the event callback.
       * @noDemo
       * @example
       * AP.dialog.create(opts).on("close", callbackFunc);
       */
      dialog.on = deprecate(registerHandler, 'AP.dialog.on("close", callback)', 'AP.events.on("dialog.close", callback)', '5.0');
      return dialog;
    };
  }
  if (combined.dialog && combined.dialog.getButton) {
    var original_dialogGetButton = combined.dialog.getButton.prototype.constructor.bind({});
    combined.dialog.getButton = combined._hostModules.dialog.getButton = function (name) {
      try {
        var button = original_dialogGetButton(name);
        /**
         * Registers a function to be called when the button is clicked.
         * @deprecated after August 2017 | Please use <code>AP.events.on("dialog.message", callback)</code> instead.
         * @method bind
         * @memberOf Dialog~DialogButton
         * @ignore
         * @param {Function} callback function to be triggered on click or programatically.
         * @noDemo
         * @example
         * AP.dialog.getButton('submit').bind(function(){
         *   alert('clicked!');
         * });
         */
        button.bind = deprecate(function (callback) {
          return registerHandler(name, callback);
        }, 'AP.dialog.getDialogButton().bind()', 'AP.events.on("dialog.message", callback)', '5.0');
        return button;
      } catch (e) {
        return {};
      }
    };
  }
  if (combined.dialog && combined.dialog.createButton) {
    var original_dialogCreateButton = combined.dialog.createButton.prototype.constructor.bind({});
    combined.dialog.createButton = combined._hostModules.dialog.createButton = function (options) {
      var buttonProperties = {};
      if (typeof options !== 'object') {
        buttonProperties.text = options;
        buttonProperties.identifier = options;
      } else {
        buttonProperties = options;
      }
      if (!buttonProperties.identifier) {
        buttonProperties.identifier = 'user.button.' + customButtonIncrement++;
      }
      original_dialogCreateButton(buttonProperties);
      return combined.dialog.getButton(buttonProperties.identifier);
    };
  }

  /**
   * Register callbacks responding to messages from the host dialog, such as "submit" or "cancel"
   * @deprecated after August 2017 | Please use <code>AP.events.on("dialog.message", callback)</code> instead.
   * @memberOf module:Dialog
   * @method onDialogMessage
   * @ignore
   * @param {String} buttonName - button either "cancel" or "submit"
   * @param {Function} listener - callback function invoked when the requested button is pressed
   */
  if (combined.dialog) {
    combined.dialog.onDialogMessage = combined._hostModules.dialog.onDialogMessage = deprecate(registerHandler, 'AP.dialog.onDialogMessage()', 'AP.events.on("dialog.message", callback)', '5.0');
  }
  if (!combined.Dialog) {
    combined.Dialog = combined._hostModules.Dialog = combined.dialog;
  }

  var modules = {};
  function reqAll(deps, callback) {
    var mods = [];
    var i = 0;
    var len = deps.length;
    function addOne(mod) {
      mods.push(mod);
      if (mods.length === len) {
        var exports = [];
        var i = 0;
        for (; i < len; i += 1) {
          exports[i] = mods[i].exports;
        }
        if (callback) {
          callback.apply(window, exports);
        }
      }
    }
    if (deps && deps.length > 0) {
      for (; i < len; i += 1) {
        reqOne(deps[i], addOne);
      }
    } else {
      if (callback) {
        callback();
      }
    }
  }
  function reqOne(name, callback) {
    // naive impl that assumes all modules are already loaded
    callback(getOrCreate(name));
  }
  function getOrCreate(name) {
    // get defined module
    if (modules[name]) {
      return modules[name];
    }

    // get a host module
    var hostModule = getFromHostModules(name);
    if (hostModule) {
      return modules[name] = hostModule;
    }

    // create a new module
    return modules[name] = {
      name: name,
      exports: function () {
        function exports() {
          var target = exports.__target__;
          if (target) {
            return target.apply(window, arguments);
          }
        }
        return exports;
      }()
    };
  }
  function getFromHostModules(name) {
    var module;
    if (combined._hostModules) {
      if (combined._hostModules[name]) {
        module = combined._hostModules[name];
      }
      if (combined._hostModules._globals && combined._hostModules._globals[name]) {
        module = combined._hostModules._globals[name];
      }
      if (module) {
        return {
          name: name,
          exports: module
        };
      }
    }
  }

  // define(name, objOrFn)
  // define(name, deps, fn(dep1, dep2, ...))
  var AMD = {
    define: function define(name, deps, exports) {
      var mod = getOrCreate(name);
      var factory;
      if (!exports) {
        exports = deps;
        deps = [];
      }
      if (exports) {
        factory = typeof exports !== 'function' ? function () {
          return exports;
        } : exports;
        reqAll(deps, function () {
          var exports = factory.apply(window, arguments);
          if (exports) {
            if (typeof exports === 'function') {
              mod.exports.__target__ = exports;
            }
            for (var k in exports) {
              if (exports.hasOwnProperty(k)) {
                mod.exports[k] = exports[k];
              }
            }
          }
        });
      }
    },
    require: function require(deps, callback) {
      reqAll(typeof deps === 'string' ? [deps] : deps, callback);
    }
  };

  function getMeta(name) {
    return $$1("meta[name='ap-" + name + "']").attr('content');
  }
  var Meta = {
    getMeta: getMeta,
    localUrl: function localUrl(path) {
      var url = getMeta('local-base-url');
      return typeof url === 'undefined' || typeof path === 'undefined' ? url : "" + url + path;
    }
  };

  function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {
    try {
      var info = gen[key](arg);
      var value = info.value;
    } catch (error) {
      reject(error);
      return;
    }
    if (info.done) {
      resolve(value);
    } else {
      Promise.resolve(value).then(_next, _throw);
    }
  }
  function _asyncToGenerator(fn) {
    return function () {
      var self = this,
        args = arguments;
      return new Promise(function (resolve, reject) {
        var gen = fn.apply(self, args);
        function _next(value) {
          asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);
        }
        function _throw(err) {
          asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);
        }
        _next(undefined);
      });
    };
  }

  var commonjsGlobal = typeof globalThis !== 'undefined' ? globalThis : typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

  var regeneratorRuntime$1 = {exports: {}};

  var _typeof$1 = {exports: {}};

  (function (module) {
  function _typeof(o) {
    "@babel/helpers - typeof";

    return module.exports = _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) {
      return typeof o;
    } : function (o) {
      return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o;
    }, module.exports.__esModule = true, module.exports["default"] = module.exports, _typeof(o);
  }
  module.exports = _typeof, module.exports.__esModule = true, module.exports["default"] = module.exports;
  }(_typeof$1));

  (function (module) {
  var _typeof = _typeof$1.exports["default"];
  function _regeneratorRuntime() {

    /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */
    module.exports = _regeneratorRuntime = function _regeneratorRuntime() {
      return e;
    }, module.exports.__esModule = true, module.exports["default"] = module.exports;
    var t,
      e = {},
      r = Object.prototype,
      n = r.hasOwnProperty,
      o = Object.defineProperty || function (t, e, r) {
        t[e] = r.value;
      },
      i = "function" == typeof Symbol ? Symbol : {},
      a = i.iterator || "@@iterator",
      c = i.asyncIterator || "@@asyncIterator",
      u = i.toStringTag || "@@toStringTag";
    function define(t, e, r) {
      return Object.defineProperty(t, e, {
        value: r,
        enumerable: !0,
        configurable: !0,
        writable: !0
      }), t[e];
    }
    try {
      define({}, "");
    } catch (t) {
      define = function define(t, e, r) {
        return t[e] = r;
      };
    }
    function wrap(t, e, r, n) {
      var i = e && e.prototype instanceof Generator ? e : Generator,
        a = Object.create(i.prototype),
        c = new Context(n || []);
      return o(a, "_invoke", {
        value: makeInvokeMethod(t, r, c)
      }), a;
    }
    function tryCatch(t, e, r) {
      try {
        return {
          type: "normal",
          arg: t.call(e, r)
        };
      } catch (t) {
        return {
          type: "throw",
          arg: t
        };
      }
    }
    e.wrap = wrap;
    var h = "suspendedStart",
      l = "suspendedYield",
      f = "executing",
      s = "completed",
      y = {};
    function Generator() {}
    function GeneratorFunction() {}
    function GeneratorFunctionPrototype() {}
    var p = {};
    define(p, a, function () {
      return this;
    });
    var d = Object.getPrototypeOf,
      v = d && d(d(values([])));
    v && v !== r && n.call(v, a) && (p = v);
    var g = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(p);
    function defineIteratorMethods(t) {
      ["next", "throw", "return"].forEach(function (e) {
        define(t, e, function (t) {
          return this._invoke(e, t);
        });
      });
    }
    function AsyncIterator(t, e) {
      function invoke(r, o, i, a) {
        var c = tryCatch(t[r], t, o);
        if ("throw" !== c.type) {
          var u = c.arg,
            h = u.value;
          return h && "object" == _typeof(h) && n.call(h, "__await") ? e.resolve(h.__await).then(function (t) {
            invoke("next", t, i, a);
          }, function (t) {
            invoke("throw", t, i, a);
          }) : e.resolve(h).then(function (t) {
            u.value = t, i(u);
          }, function (t) {
            return invoke("throw", t, i, a);
          });
        }
        a(c.arg);
      }
      var r;
      o(this, "_invoke", {
        value: function value(t, n) {
          function callInvokeWithMethodAndArg() {
            return new e(function (e, r) {
              invoke(t, n, e, r);
            });
          }
          return r = r ? r.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg();
        }
      });
    }
    function makeInvokeMethod(e, r, n) {
      var o = h;
      return function (i, a) {
        if (o === f) throw new Error("Generator is already running");
        if (o === s) {
          if ("throw" === i) throw a;
          return {
            value: t,
            done: !0
          };
        }
        for (n.method = i, n.arg = a;;) {
          var c = n.delegate;
          if (c) {
            var u = maybeInvokeDelegate(c, n);
            if (u) {
              if (u === y) continue;
              return u;
            }
          }
          if ("next" === n.method) n.sent = n._sent = n.arg;else if ("throw" === n.method) {
            if (o === h) throw o = s, n.arg;
            n.dispatchException(n.arg);
          } else "return" === n.method && n.abrupt("return", n.arg);
          o = f;
          var p = tryCatch(e, r, n);
          if ("normal" === p.type) {
            if (o = n.done ? s : l, p.arg === y) continue;
            return {
              value: p.arg,
              done: n.done
            };
          }
          "throw" === p.type && (o = s, n.method = "throw", n.arg = p.arg);
        }
      };
    }
    function maybeInvokeDelegate(e, r) {
      var n = r.method,
        o = e.iterator[n];
      if (o === t) return r.delegate = null, "throw" === n && e.iterator["return"] && (r.method = "return", r.arg = t, maybeInvokeDelegate(e, r), "throw" === r.method) || "return" !== n && (r.method = "throw", r.arg = new TypeError("The iterator does not provide a '" + n + "' method")), y;
      var i = tryCatch(o, e.iterator, r.arg);
      if ("throw" === i.type) return r.method = "throw", r.arg = i.arg, r.delegate = null, y;
      var a = i.arg;
      return a ? a.done ? (r[e.resultName] = a.value, r.next = e.nextLoc, "return" !== r.method && (r.method = "next", r.arg = t), r.delegate = null, y) : a : (r.method = "throw", r.arg = new TypeError("iterator result is not an object"), r.delegate = null, y);
    }
    function pushTryEntry(t) {
      var e = {
        tryLoc: t[0]
      };
      1 in t && (e.catchLoc = t[1]), 2 in t && (e.finallyLoc = t[2], e.afterLoc = t[3]), this.tryEntries.push(e);
    }
    function resetTryEntry(t) {
      var e = t.completion || {};
      e.type = "normal", delete e.arg, t.completion = e;
    }
    function Context(t) {
      this.tryEntries = [{
        tryLoc: "root"
      }], t.forEach(pushTryEntry, this), this.reset(!0);
    }
    function values(e) {
      if (e || "" === e) {
        var r = e[a];
        if (r) return r.call(e);
        if ("function" == typeof e.next) return e;
        if (!isNaN(e.length)) {
          var o = -1,
            i = function next() {
              for (; ++o < e.length;) if (n.call(e, o)) return next.value = e[o], next.done = !1, next;
              return next.value = t, next.done = !0, next;
            };
          return i.next = i;
        }
      }
      throw new TypeError(_typeof(e) + " is not iterable");
    }
    return GeneratorFunction.prototype = GeneratorFunctionPrototype, o(g, "constructor", {
      value: GeneratorFunctionPrototype,
      configurable: !0
    }), o(GeneratorFunctionPrototype, "constructor", {
      value: GeneratorFunction,
      configurable: !0
    }), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, u, "GeneratorFunction"), e.isGeneratorFunction = function (t) {
      var e = "function" == typeof t && t.constructor;
      return !!e && (e === GeneratorFunction || "GeneratorFunction" === (e.displayName || e.name));
    }, e.mark = function (t) {
      return Object.setPrototypeOf ? Object.setPrototypeOf(t, GeneratorFunctionPrototype) : (t.__proto__ = GeneratorFunctionPrototype, define(t, u, "GeneratorFunction")), t.prototype = Object.create(g), t;
    }, e.awrap = function (t) {
      return {
        __await: t
      };
    }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, c, function () {
      return this;
    }), e.AsyncIterator = AsyncIterator, e.async = function (t, r, n, o, i) {
      void 0 === i && (i = Promise);
      var a = new AsyncIterator(wrap(t, r, n, o), i);
      return e.isGeneratorFunction(r) ? a : a.next().then(function (t) {
        return t.done ? t.value : a.next();
      });
    }, defineIteratorMethods(g), define(g, u, "Generator"), define(g, a, function () {
      return this;
    }), define(g, "toString", function () {
      return "[object Generator]";
    }), e.keys = function (t) {
      var e = Object(t),
        r = [];
      for (var n in e) r.push(n);
      return r.reverse(), function next() {
        for (; r.length;) {
          var t = r.pop();
          if (t in e) return next.value = t, next.done = !1, next;
        }
        return next.done = !0, next;
      };
    }, e.values = values, Context.prototype = {
      constructor: Context,
      reset: function reset(e) {
        if (this.prev = 0, this.next = 0, this.sent = this._sent = t, this.done = !1, this.delegate = null, this.method = "next", this.arg = t, this.tryEntries.forEach(resetTryEntry), !e) for (var r in this) "t" === r.charAt(0) && n.call(this, r) && !isNaN(+r.slice(1)) && (this[r] = t);
      },
      stop: function stop() {
        this.done = !0;
        var t = this.tryEntries[0].completion;
        if ("throw" === t.type) throw t.arg;
        return this.rval;
      },
      dispatchException: function dispatchException(e) {
        if (this.done) throw e;
        var r = this;
        function handle(n, o) {
          return a.type = "throw", a.arg = e, r.next = n, o && (r.method = "next", r.arg = t), !!o;
        }
        for (var o = this.tryEntries.length - 1; o >= 0; --o) {
          var i = this.tryEntries[o],
            a = i.completion;
          if ("root" === i.tryLoc) return handle("end");
          if (i.tryLoc <= this.prev) {
            var c = n.call(i, "catchLoc"),
              u = n.call(i, "finallyLoc");
            if (c && u) {
              if (this.prev < i.catchLoc) return handle(i.catchLoc, !0);
              if (this.prev < i.finallyLoc) return handle(i.finallyLoc);
            } else if (c) {
              if (this.prev < i.catchLoc) return handle(i.catchLoc, !0);
            } else {
              if (!u) throw new Error("try statement without catch or finally");
              if (this.prev < i.finallyLoc) return handle(i.finallyLoc);
            }
          }
        }
      },
      abrupt: function abrupt(t, e) {
        for (var r = this.tryEntries.length - 1; r >= 0; --r) {
          var o = this.tryEntries[r];
          if (o.tryLoc <= this.prev && n.call(o, "finallyLoc") && this.prev < o.finallyLoc) {
            var i = o;
            break;
          }
        }
        i && ("break" === t || "continue" === t) && i.tryLoc <= e && e <= i.finallyLoc && (i = null);
        var a = i ? i.completion : {};
        return a.type = t, a.arg = e, i ? (this.method = "next", this.next = i.finallyLoc, y) : this.complete(a);
      },
      complete: function complete(t, e) {
        if ("throw" === t.type) throw t.arg;
        return "break" === t.type || "continue" === t.type ? this.next = t.arg : "return" === t.type ? (this.rval = this.arg = t.arg, this.method = "return", this.next = "end") : "normal" === t.type && e && (this.next = e), y;
      },
      finish: function finish(t) {
        for (var e = this.tryEntries.length - 1; e >= 0; --e) {
          var r = this.tryEntries[e];
          if (r.finallyLoc === t) return this.complete(r.completion, r.afterLoc), resetTryEntry(r), y;
        }
      },
      "catch": function _catch(t) {
        for (var e = this.tryEntries.length - 1; e >= 0; --e) {
          var r = this.tryEntries[e];
          if (r.tryLoc === t) {
            var n = r.completion;
            if ("throw" === n.type) {
              var o = n.arg;
              resetTryEntry(r);
            }
            return o;
          }
        }
        throw new Error("illegal catch attempt");
      },
      delegateYield: function delegateYield(e, r, n) {
        return this.delegate = {
          iterator: values(e),
          resultName: r,
          nextLoc: n
        }, "next" === this.method && (this.arg = t), y;
      }
    }, e;
  }
  module.exports = _regeneratorRuntime, module.exports.__esModule = true, module.exports["default"] = module.exports;
  }(regeneratorRuntime$1));

  // TODO(Babel 8): Remove this file.

  var runtime = regeneratorRuntime$1.exports();
  var regenerator = runtime;

  // Copied from https://github.com/facebook/regenerator/blob/main/packages/runtime/runtime.js#L736=
  try {
    regeneratorRuntime = runtime;
  } catch (accidentalStrictMode) {
    if (typeof globalThis === "object") {
      globalThis.regeneratorRuntime = runtime;
    } else {
      Function("r", "regeneratorRuntime = r")(runtime);
    }
  }

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _typeof(o) {
    "@babel/helpers - typeof";

    return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) {
      return typeof o;
    } : function (o) {
      return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o;
    }, _typeof(o);
  }

  function _toPrimitive(input, hint) {
    if (_typeof(input) !== "object" || input === null) return input;
    var prim = input[Symbol.toPrimitive];
    if (prim !== undefined) {
      var res = prim.call(input, hint || "default");
      if (_typeof(res) !== "object") return res;
      throw new TypeError("@@toPrimitive must return a primitive value.");
    }
    return (hint === "string" ? String : Number)(input);
  }

  function _toPropertyKey(arg) {
    var key = _toPrimitive(arg, "string");
    return _typeof(key) === "symbol" ? key : String(key);
  }

  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor);
    }
  }
  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    Object.defineProperty(Constructor, "prototype", {
      writable: false
    });
    return Constructor;
  }

  function _defineProperty(obj, key, value) {
    key = _toPropertyKey(key);
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }
    return obj;
  }

  function _objectWithoutPropertiesLoose(source, excluded) {
    if (source == null) return {};
    var target = {};
    var sourceKeys = Object.keys(source);
    var key, i;
    for (i = 0; i < sourceKeys.length; i++) {
      key = sourceKeys[i];
      if (excluded.indexOf(key) >= 0) continue;
      target[key] = source[key];
    }
    return target;
  }

  function _objectWithoutProperties(source, excluded) {
    if (source == null) return {};
    var target = _objectWithoutPropertiesLoose(source, excluded);
    var key, i;
    if (Object.getOwnPropertySymbols) {
      var sourceSymbolKeys = Object.getOwnPropertySymbols(source);
      for (i = 0; i < sourceSymbolKeys.length; i++) {
        key = sourceSymbolKeys[i];
        if (excluded.indexOf(key) >= 0) continue;
        if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue;
        target[key] = source[key];
      }
    }
    return target;
  }

  function _arrayWithHoles(arr) {
    if (Array.isArray(arr)) return arr;
  }

  function _iterableToArrayLimit(r, l) {
    var t = null == r ? null : "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"];
    if (null != t) {
      var e,
        n,
        i,
        u,
        a = [],
        f = !0,
        o = !1;
      try {
        if (i = (t = t.call(r)).next, 0 === l) {
          if (Object(t) !== t) return;
          f = !1;
        } else for (; !(f = (e = i.call(t)).done) && (a.push(e.value), a.length !== l); f = !0);
      } catch (r) {
        o = !0, n = r;
      } finally {
        try {
          if (!f && null != t["return"] && (u = t["return"](), Object(u) !== u)) return;
        } finally {
          if (o) throw n;
        }
      }
      return a;
    }
  }

  function _arrayLikeToArray$2(arr, len) {
    if (len == null || len > arr.length) len = arr.length;
    for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i];
    return arr2;
  }

  function _unsupportedIterableToArray$2(o, minLen) {
    if (!o) return;
    if (typeof o === "string") return _arrayLikeToArray$2(o, minLen);
    var n = Object.prototype.toString.call(o).slice(8, -1);
    if (n === "Object" && o.constructor) n = o.constructor.name;
    if (n === "Map" || n === "Set") return Array.from(o);
    if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray$2(o, minLen);
  }

  function _nonIterableRest() {
    throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
  }

  function _slicedToArray(arr, i) {
    return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray$2(arr, i) || _nonIterableRest();
  }

  var eventemitter2 = {exports: {}};

  /*!
   * EventEmitter2
   * https://github.com/hij1nx/EventEmitter2
   *
   * Copyright (c) 2013 hij1nx
   * Licensed under the MIT license.
   */

  (function (module, exports) {
  !function (undefined$1) {
    var isArray = Array.isArray ? Array.isArray : function _isArray(obj) {
      return Object.prototype.toString.call(obj) === "[object Array]";
    };
    var defaultMaxListeners = 10;
    function init() {
      this._events = {};
      if (this._conf) {
        configure.call(this, this._conf);
      }
    }
    function configure(conf) {
      if (conf) {
        this._conf = conf;
        conf.delimiter && (this.delimiter = conf.delimiter);
        this._maxListeners = conf.maxListeners !== undefined$1 ? conf.maxListeners : defaultMaxListeners;
        conf.wildcard && (this.wildcard = conf.wildcard);
        conf.newListener && (this.newListener = conf.newListener);
        conf.verboseMemoryLeak && (this.verboseMemoryLeak = conf.verboseMemoryLeak);
        if (this.wildcard) {
          this.listenerTree = {};
        }
      } else {
        this._maxListeners = defaultMaxListeners;
      }
    }
    function logPossibleMemoryLeak(count, eventName) {
      var errorMsg = '(node) warning: possible EventEmitter memory ' + 'leak detected. ' + count + ' listeners added. ' + 'Use emitter.setMaxListeners() to increase limit.';
      if (this.verboseMemoryLeak) {
        errorMsg += ' Event name: ' + eventName + '.';
      }
      if (typeof process !== 'undefined' && process.emitWarning) {
        var e = new Error(errorMsg);
        e.name = 'MaxListenersExceededWarning';
        e.emitter = this;
        e.count = count;
        process.emitWarning(e);
      } else {
        console.error(errorMsg);
        if (console.trace) {
          console.trace();
        }
      }
    }
    function EventEmitter(conf) {
      this._events = {};
      this.newListener = false;
      this.verboseMemoryLeak = false;
      configure.call(this, conf);
    }
    EventEmitter.EventEmitter2 = EventEmitter; // backwards compatibility for exporting EventEmitter property

    //
    // Attention, function return type now is array, always !
    // It has zero elements if no any matches found and one or more
    // elements (leafs) if there are matches
    //
    function searchListenerTree(handlers, type, tree, i) {
      if (!tree) {
        return [];
      }
      var listeners = [],
        leaf,
        len,
        branch,
        xTree,
        xxTree,
        isolatedBranch,
        endReached,
        typeLength = type.length,
        currentType = type[i],
        nextType = type[i + 1];
      if (i === typeLength && tree._listeners) {
        //
        // If at the end of the event(s) list and the tree has listeners
        // invoke those listeners.
        //
        if (typeof tree._listeners === 'function') {
          handlers && handlers.push(tree._listeners);
          return [tree];
        } else {
          for (leaf = 0, len = tree._listeners.length; leaf < len; leaf++) {
            handlers && handlers.push(tree._listeners[leaf]);
          }
          return [tree];
        }
      }
      if (currentType === '*' || currentType === '**' || tree[currentType]) {
        //
        // If the event emitted is '*' at this part
        // or there is a concrete match at this patch
        //
        if (currentType === '*') {
          for (branch in tree) {
            if (branch !== '_listeners' && tree.hasOwnProperty(branch)) {
              listeners = listeners.concat(searchListenerTree(handlers, type, tree[branch], i + 1));
            }
          }
          return listeners;
        } else if (currentType === '**') {
          endReached = i + 1 === typeLength || i + 2 === typeLength && nextType === '*';
          if (endReached && tree._listeners) {
            // The next element has a _listeners, add it to the handlers.
            listeners = listeners.concat(searchListenerTree(handlers, type, tree, typeLength));
          }
          for (branch in tree) {
            if (branch !== '_listeners' && tree.hasOwnProperty(branch)) {
              if (branch === '*' || branch === '**') {
                if (tree[branch]._listeners && !endReached) {
                  listeners = listeners.concat(searchListenerTree(handlers, type, tree[branch], typeLength));
                }
                listeners = listeners.concat(searchListenerTree(handlers, type, tree[branch], i));
              } else if (branch === nextType) {
                listeners = listeners.concat(searchListenerTree(handlers, type, tree[branch], i + 2));
              } else {
                // No match on this one, shift into the tree but not in the type array.
                listeners = listeners.concat(searchListenerTree(handlers, type, tree[branch], i));
              }
            }
          }
          return listeners;
        }
        listeners = listeners.concat(searchListenerTree(handlers, type, tree[currentType], i + 1));
      }
      xTree = tree['*'];
      if (xTree) {
        //
        // If the listener tree will allow any match for this part,
        // then recursively explore all branches of the tree
        //
        searchListenerTree(handlers, type, xTree, i + 1);
      }
      xxTree = tree['**'];
      if (xxTree) {
        if (i < typeLength) {
          if (xxTree._listeners) {
            // If we have a listener on a '**', it will catch all, so add its handler.
            searchListenerTree(handlers, type, xxTree, typeLength);
          }

          // Build arrays of matching next branches and others.
          for (branch in xxTree) {
            if (branch !== '_listeners' && xxTree.hasOwnProperty(branch)) {
              if (branch === nextType) {
                // We know the next element will match, so jump twice.
                searchListenerTree(handlers, type, xxTree[branch], i + 2);
              } else if (branch === currentType) {
                // Current node matches, move into the tree.
                searchListenerTree(handlers, type, xxTree[branch], i + 1);
              } else {
                isolatedBranch = {};
                isolatedBranch[branch] = xxTree[branch];
                searchListenerTree(handlers, type, {
                  '**': isolatedBranch
                }, i + 1);
              }
            }
          }
        } else if (xxTree._listeners) {
          // We have reached the end and still on a '**'
          searchListenerTree(handlers, type, xxTree, typeLength);
        } else if (xxTree['*'] && xxTree['*']._listeners) {
          searchListenerTree(handlers, type, xxTree['*'], typeLength);
        }
      }
      return listeners;
    }
    function growListenerTree(type, listener) {
      type = typeof type === 'string' ? type.split(this.delimiter) : type.slice();

      //
      // Looks for two consecutive '**', if so, don't add the event at all.
      //
      for (var i = 0, len = type.length; i + 1 < len; i++) {
        if (type[i] === '**' && type[i + 1] === '**') {
          return;
        }
      }
      var tree = this.listenerTree;
      var name = type.shift();
      while (name !== undefined$1) {
        if (!tree[name]) {
          tree[name] = {};
        }
        tree = tree[name];
        if (type.length === 0) {
          if (!tree._listeners) {
            tree._listeners = listener;
          } else {
            if (typeof tree._listeners === 'function') {
              tree._listeners = [tree._listeners];
            }
            tree._listeners.push(listener);
            if (!tree._listeners.warned && this._maxListeners > 0 && tree._listeners.length > this._maxListeners) {
              tree._listeners.warned = true;
              logPossibleMemoryLeak.call(this, tree._listeners.length, name);
            }
          }
          return true;
        }
        name = type.shift();
      }
      return true;
    }

    // By default EventEmitters will print a warning if more than
    // 10 listeners are added to it. This is a useful default which
    // helps finding memory leaks.
    //
    // Obviously not all Emitters should be limited to 10. This function allows
    // that to be increased. Set to zero for unlimited.

    EventEmitter.prototype.delimiter = '.';
    EventEmitter.prototype.setMaxListeners = function (n) {
      if (n !== undefined$1) {
        this._maxListeners = n;
        if (!this._conf) this._conf = {};
        this._conf.maxListeners = n;
      }
    };
    EventEmitter.prototype.event = '';
    EventEmitter.prototype.once = function (event, fn) {
      return this._once(event, fn, false);
    };
    EventEmitter.prototype.prependOnceListener = function (event, fn) {
      return this._once(event, fn, true);
    };
    EventEmitter.prototype._once = function (event, fn, prepend) {
      this._many(event, 1, fn, prepend);
      return this;
    };
    EventEmitter.prototype.many = function (event, ttl, fn) {
      return this._many(event, ttl, fn, false);
    };
    EventEmitter.prototype.prependMany = function (event, ttl, fn) {
      return this._many(event, ttl, fn, true);
    };
    EventEmitter.prototype._many = function (event, ttl, fn, prepend) {
      var self = this;
      if (typeof fn !== 'function') {
        throw new Error('many only accepts instances of Function');
      }
      function listener() {
        if (--ttl === 0) {
          self.off(event, listener);
        }
        return fn.apply(this, arguments);
      }
      listener._origin = fn;
      this._on(event, listener, prepend);
      return self;
    };
    EventEmitter.prototype.emit = function () {
      this._events || init.call(this);
      var type = arguments[0];
      if (type === 'newListener' && !this.newListener) {
        if (!this._events.newListener) {
          return false;
        }
      }
      var al = arguments.length;
      var args, l, i, j;
      var handler;
      if (this._all && this._all.length) {
        handler = this._all.slice();
        if (al > 3) {
          args = new Array(al);
          for (j = 0; j < al; j++) args[j] = arguments[j];
        }
        for (i = 0, l = handler.length; i < l; i++) {
          this.event = type;
          switch (al) {
            case 1:
              handler[i].call(this, type);
              break;
            case 2:
              handler[i].call(this, type, arguments[1]);
              break;
            case 3:
              handler[i].call(this, type, arguments[1], arguments[2]);
              break;
            default:
              handler[i].apply(this, args);
          }
        }
      }
      if (this.wildcard) {
        handler = [];
        var ns = typeof type === 'string' ? type.split(this.delimiter) : type.slice();
        searchListenerTree.call(this, handler, ns, this.listenerTree, 0);
      } else {
        handler = this._events[type];
        if (typeof handler === 'function') {
          this.event = type;
          switch (al) {
            case 1:
              handler.call(this);
              break;
            case 2:
              handler.call(this, arguments[1]);
              break;
            case 3:
              handler.call(this, arguments[1], arguments[2]);
              break;
            default:
              args = new Array(al - 1);
              for (j = 1; j < al; j++) args[j - 1] = arguments[j];
              handler.apply(this, args);
          }
          return true;
        } else if (handler) {
          // need to make copy of handlers because list can change in the middle
          // of emit call
          handler = handler.slice();
        }
      }
      if (handler && handler.length) {
        if (al > 3) {
          args = new Array(al - 1);
          for (j = 1; j < al; j++) args[j - 1] = arguments[j];
        }
        for (i = 0, l = handler.length; i < l; i++) {
          this.event = type;
          switch (al) {
            case 1:
              handler[i].call(this);
              break;
            case 2:
              handler[i].call(this, arguments[1]);
              break;
            case 3:
              handler[i].call(this, arguments[1], arguments[2]);
              break;
            default:
              handler[i].apply(this, args);
          }
        }
        return true;
      } else if (!this._all && type === 'error') {
        if (arguments[1] instanceof Error) {
          throw arguments[1]; // Unhandled 'error' event
        } else {
          throw new Error("Uncaught, unspecified 'error' event.");
        }
      }
      return !!this._all;
    };
    EventEmitter.prototype.emitAsync = function () {
      this._events || init.call(this);
      var type = arguments[0];
      if (type === 'newListener' && !this.newListener) {
        if (!this._events.newListener) {
          return Promise.resolve([false]);
        }
      }
      var promises = [];
      var al = arguments.length;
      var args, l, i, j;
      var handler;
      if (this._all) {
        if (al > 3) {
          args = new Array(al);
          for (j = 1; j < al; j++) args[j] = arguments[j];
        }
        for (i = 0, l = this._all.length; i < l; i++) {
          this.event = type;
          switch (al) {
            case 1:
              promises.push(this._all[i].call(this, type));
              break;
            case 2:
              promises.push(this._all[i].call(this, type, arguments[1]));
              break;
            case 3:
              promises.push(this._all[i].call(this, type, arguments[1], arguments[2]));
              break;
            default:
              promises.push(this._all[i].apply(this, args));
          }
        }
      }
      if (this.wildcard) {
        handler = [];
        var ns = typeof type === 'string' ? type.split(this.delimiter) : type.slice();
        searchListenerTree.call(this, handler, ns, this.listenerTree, 0);
      } else {
        handler = this._events[type];
      }
      if (typeof handler === 'function') {
        this.event = type;
        switch (al) {
          case 1:
            promises.push(handler.call(this));
            break;
          case 2:
            promises.push(handler.call(this, arguments[1]));
            break;
          case 3:
            promises.push(handler.call(this, arguments[1], arguments[2]));
            break;
          default:
            args = new Array(al - 1);
            for (j = 1; j < al; j++) args[j - 1] = arguments[j];
            promises.push(handler.apply(this, args));
        }
      } else if (handler && handler.length) {
        handler = handler.slice();
        if (al > 3) {
          args = new Array(al - 1);
          for (j = 1; j < al; j++) args[j - 1] = arguments[j];
        }
        for (i = 0, l = handler.length; i < l; i++) {
          this.event = type;
          switch (al) {
            case 1:
              promises.push(handler[i].call(this));
              break;
            case 2:
              promises.push(handler[i].call(this, arguments[1]));
              break;
            case 3:
              promises.push(handler[i].call(this, arguments[1], arguments[2]));
              break;
            default:
              promises.push(handler[i].apply(this, args));
          }
        }
      } else if (!this._all && type === 'error') {
        if (arguments[1] instanceof Error) {
          return Promise.reject(arguments[1]); // Unhandled 'error' event
        } else {
          return Promise.reject("Uncaught, unspecified 'error' event.");
        }
      }
      return Promise.all(promises);
    };
    EventEmitter.prototype.on = function (type, listener) {
      return this._on(type, listener, false);
    };
    EventEmitter.prototype.prependListener = function (type, listener) {
      return this._on(type, listener, true);
    };
    EventEmitter.prototype.onAny = function (fn) {
      return this._onAny(fn, false);
    };
    EventEmitter.prototype.prependAny = function (fn) {
      return this._onAny(fn, true);
    };
    EventEmitter.prototype.addListener = EventEmitter.prototype.on;
    EventEmitter.prototype._onAny = function (fn, prepend) {
      if (typeof fn !== 'function') {
        throw new Error('onAny only accepts instances of Function');
      }
      if (!this._all) {
        this._all = [];
      }

      // Add the function to the event listener collection.
      if (prepend) {
        this._all.unshift(fn);
      } else {
        this._all.push(fn);
      }
      return this;
    };
    EventEmitter.prototype._on = function (type, listener, prepend) {
      if (typeof type === 'function') {
        this._onAny(type, listener);
        return this;
      }
      if (typeof listener !== 'function') {
        throw new Error('on only accepts instances of Function');
      }
      this._events || init.call(this);

      // To avoid recursion in the case that type == "newListeners"! Before
      // adding it to the listeners, first emit "newListeners".
      this.emit('newListener', type, listener);
      if (this.wildcard) {
        growListenerTree.call(this, type, listener);
        return this;
      }
      if (!this._events[type]) {
        // Optimize the case of one listener. Don't need the extra array object.
        this._events[type] = listener;
      } else {
        if (typeof this._events[type] === 'function') {
          // Change to array.
          this._events[type] = [this._events[type]];
        }

        // If we've already got an array, just add
        if (prepend) {
          this._events[type].unshift(listener);
        } else {
          this._events[type].push(listener);
        }

        // Check for listener leak
        if (!this._events[type].warned && this._maxListeners > 0 && this._events[type].length > this._maxListeners) {
          this._events[type].warned = true;
          logPossibleMemoryLeak.call(this, this._events[type].length, type);
        }
      }
      return this;
    };
    EventEmitter.prototype.off = function (type, listener) {
      if (typeof listener !== 'function') {
        throw new Error('removeListener only takes instances of Function');
      }
      var handlers,
        leafs = [];
      if (this.wildcard) {
        var ns = typeof type === 'string' ? type.split(this.delimiter) : type.slice();
        leafs = searchListenerTree.call(this, null, ns, this.listenerTree, 0);
      } else {
        // does not use listeners(), so no side effect of creating _events[type]
        if (!this._events[type]) return this;
        handlers = this._events[type];
        leafs.push({
          _listeners: handlers
        });
      }
      for (var iLeaf = 0; iLeaf < leafs.length; iLeaf++) {
        var leaf = leafs[iLeaf];
        handlers = leaf._listeners;
        if (isArray(handlers)) {
          var position = -1;
          for (var i = 0, length = handlers.length; i < length; i++) {
            if (handlers[i] === listener || handlers[i].listener && handlers[i].listener === listener || handlers[i]._origin && handlers[i]._origin === listener) {
              position = i;
              break;
            }
          }
          if (position < 0) {
            continue;
          }
          if (this.wildcard) {
            leaf._listeners.splice(position, 1);
          } else {
            this._events[type].splice(position, 1);
          }
          if (handlers.length === 0) {
            if (this.wildcard) {
              delete leaf._listeners;
            } else {
              delete this._events[type];
            }
          }
          this.emit("removeListener", type, listener);
          return this;
        } else if (handlers === listener || handlers.listener && handlers.listener === listener || handlers._origin && handlers._origin === listener) {
          if (this.wildcard) {
            delete leaf._listeners;
          } else {
            delete this._events[type];
          }
          this.emit("removeListener", type, listener);
        }
      }
      function recursivelyGarbageCollect(root) {
        if (root === undefined$1) {
          return;
        }
        var keys = Object.keys(root);
        for (var i in keys) {
          var key = keys[i];
          var obj = root[key];
          if (obj instanceof Function || typeof obj !== "object" || obj === null) continue;
          if (Object.keys(obj).length > 0) {
            recursivelyGarbageCollect(root[key]);
          }
          if (Object.keys(obj).length === 0) {
            delete root[key];
          }
        }
      }
      recursivelyGarbageCollect(this.listenerTree);
      return this;
    };
    EventEmitter.prototype.offAny = function (fn) {
      var i = 0,
        l = 0,
        fns;
      if (fn && this._all && this._all.length > 0) {
        fns = this._all;
        for (i = 0, l = fns.length; i < l; i++) {
          if (fn === fns[i]) {
            fns.splice(i, 1);
            this.emit("removeListenerAny", fn);
            return this;
          }
        }
      } else {
        fns = this._all;
        for (i = 0, l = fns.length; i < l; i++) this.emit("removeListenerAny", fns[i]);
        this._all = [];
      }
      return this;
    };
    EventEmitter.prototype.removeListener = EventEmitter.prototype.off;
    EventEmitter.prototype.removeAllListeners = function (type) {
      if (arguments.length === 0) {
        !this._events || init.call(this);
        return this;
      }
      if (this.wildcard) {
        var ns = typeof type === 'string' ? type.split(this.delimiter) : type.slice();
        var leafs = searchListenerTree.call(this, null, ns, this.listenerTree, 0);
        for (var iLeaf = 0; iLeaf < leafs.length; iLeaf++) {
          var leaf = leafs[iLeaf];
          leaf._listeners = null;
        }
      } else if (this._events) {
        this._events[type] = null;
      }
      return this;
    };
    EventEmitter.prototype.listeners = function (type) {
      if (this.wildcard) {
        var handlers = [];
        var ns = typeof type === 'string' ? type.split(this.delimiter) : type.slice();
        searchListenerTree.call(this, handlers, ns, this.listenerTree, 0);
        return handlers;
      }
      this._events || init.call(this);
      if (!this._events[type]) this._events[type] = [];
      if (!isArray(this._events[type])) {
        this._events[type] = [this._events[type]];
      }
      return this._events[type];
    };
    EventEmitter.prototype.eventNames = function () {
      return Object.keys(this._events);
    };
    EventEmitter.prototype.listenerCount = function (type) {
      return this.listeners(type).length;
    };
    EventEmitter.prototype.listenersAny = function () {
      if (this._all) {
        return this._all;
      } else {
        return [];
      }
    };
    if (typeof undefined$1 === 'function' && undefined$1.amd) {
      // AMD. Register as an anonymous module.
      undefined$1(function () {
        return EventEmitter;
      });
    } else {
      // CommonJS
      module.exports = EventEmitter;
    }
  }();
  }(eventemitter2));

  function ownKeys$7(e, r) {
    var t = Object.keys(e);
    if (Object.getOwnPropertySymbols) {
      var o = Object.getOwnPropertySymbols(e);
      r && (o = o.filter(function (r) {
        return Object.getOwnPropertyDescriptor(e, r).enumerable;
      })), t.push.apply(t, o);
    }
    return t;
  }
  function _objectSpread$7(e) {
    for (var r = 1; r < arguments.length; r++) {
      var t = null != arguments[r] ? arguments[r] : {};
      r % 2 ? ownKeys$7(Object(t), !0).forEach(function (r) {
        _defineProperty(e, r, t[r]);
      }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys$7(Object(t)).forEach(function (r) {
        Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r));
      });
    }
    return e;
  }
  var ALL_FEATURE_VALUES = '@all-features';
  var Subscriptions = /*#__PURE__*/function () {
    function Subscriptions() {
      _classCallCheck(this, Subscriptions);
      _defineProperty(this, "eventToValue", new Map());
      this.emitter = new eventemitter2.exports.EventEmitter2();
    }
    return _createClass(Subscriptions, [{
      key: "onGateUpdated",
      value: function onGateUpdated(gateName, callback, checkGate, options) {
        var _this = this;
        var value = checkGate(gateName, _objectSpread$7(_objectSpread$7({}, options), {}, {
          fireGateExposure: false
        }));
        if (this.eventToValue.get(callback) === undefined) {
          this.eventToValue.set(callback, value);
        }
        var wrapCallback = function wrapCallback() {
          var value = checkGate(gateName, _objectSpread$7(_objectSpread$7({}, options), {}, {
            fireGateExposure: false
          }));
          var existingValue = _this.eventToValue.get(callback);
          if (existingValue !== value) {
            _this.eventToValue.set(callback, value);
            callback(value);
          }
        };
        this.emitter.on(gateName, wrapCallback);
        return function () {
          _this.emitter.off(gateName, wrapCallback);
        };
      }
    }, {
      key: "onExperimentValueUpdated",
      value: function onExperimentValueUpdated(experimentName, parameterName, defaultValue, callback, getExperimentValue, options) {
        var _this2 = this;
        var experimentEventName = "".concat(experimentName, ".").concat(parameterName);
        var value = getExperimentValue(experimentName, parameterName, defaultValue, _objectSpread$7(_objectSpread$7({}, options), {}, {
          fireExperimentExposure: false
        }));
        if (this.eventToValue.get(callback) === undefined) {
          this.eventToValue.set(callback, value);
        }
        var wrapCallback = function wrapCallback() {
          var value = getExperimentValue(experimentName, parameterName, defaultValue, _objectSpread$7(_objectSpread$7({}, options), {}, {
            fireExperimentExposure: false
          }));
          var existingValue = _this2.eventToValue.get(callback);
          if (existingValue !== value) {
            _this2.eventToValue.set(callback, value);
            callback(value);
          }
        };
        this.emitter.on(experimentEventName, wrapCallback);
        return function () {
          _this2.emitter.off(experimentEventName, wrapCallback);
        };
      }
    }, {
      key: "onAnyUpdated",
      value: function onAnyUpdated(callback) {
        var _this3 = this;
        this.emitter.on(ALL_FEATURE_VALUES, callback);
        return function () {
          _this3.emitter.off(ALL_FEATURE_VALUES, callback);
        };
      }
    }, {
      key: "anyUpdated",
      value: function anyUpdated() {
        var _this4 = this;
        this.emitter.emit(ALL_FEATURE_VALUES);
        this.emitter.eventNames().filter(function (name) {
          return name !== ALL_FEATURE_VALUES;
        }).forEach(function (event) {
          _this4.emitter.emit(event);
        });
      }
    }]);
  }();

  var FEDRAMP_MODERATE = 'fedramp-moderate';

  /**
   * Caution: Consider Alternatives Use of this function is not recommended as a long term solution, as it creates an assumption
   * there are no other isolated environments than just FedRAMP Moderate. You are encouraged to consider alternate solutions,
   * such as Statsig or environment configuration, that don’t require creating a hard dependency between your code features
   * and the FedRAMP environment.
   * See [go-is-fedramp](https://go.atlassian.com/is-fedramp)
   */
  function isFedRamp() {
    var _global$location;
    var global = globalThis;
    // MICROS_PERIMETER is already used by few products, so we need to keep it for backward compatibility
    var env = global.MICROS_PERIMETER || global.UNSAFE_ATL_CONTEXT_BOUNDARY;
    if (env) {
      return env === FEDRAMP_MODERATE;
    }
    var matches = (_global$location = global.location) === null || _global$location === void 0 || (_global$location = _global$location.hostname) === null || _global$location === void 0 ? void 0 : _global$location.match(/atlassian-us-gov-mod\.(com|net)|atlassian-us-gov\.(com|net)|atlassian-fex\.(com|net)|atlassian-stg-fedm\.(com|net)/);
    return matches ? matches.length > 0 : false;
  }

  // Reference: https://github.com/statsig-io/js-lite/blob/main/src/StatsigStore.ts

  var EvaluationReason = /*#__PURE__*/function (EvaluationReason) {
    EvaluationReason["Error"] = "Error";
    EvaluationReason["LocalOverride"] = "LocalOverride";
    EvaluationReason["Unrecognized"] = "Unrecognized";
    EvaluationReason["Uninitialized"] = "Uninitialized";
    EvaluationReason["NetworkNotModified"] = "NetworkNotModified";
    EvaluationReason["Network"] = "Network";
    EvaluationReason["InvalidBootstrap"] = "InvalidBootstrap";
    EvaluationReason["Bootstrap"] = "Bootstrap";
    EvaluationReason["Cache"] = "Cache";
    EvaluationReason["Unknown"] = "Unknown";
    return EvaluationReason;
  }({});

  // Reference: https://github.com/statsig-io/js-lite/blob/main/src/StatsigSDKOptions.ts

  /**
   * The identifiers for the user. Options are restricted to the set that is currently supported.
   */

  /**
   * Base client options. Does not include any options specific to providers
   * @interface BaseClientOptions
   * @property {FeatureGateEnvironment} environment - The environment for the client.
   * @property {string} targetApp - The target app for the client.
   * @property {AnalyticsWebClient} analyticsWebClient - The analytics web client.
   * @property {PerimeterType} perimeter - The perimeter for the client.
   */

  /**
   * The options for the client.
   * @interface ClientOptions
   * @extends {BaseClientOptions}
   * @property {string} apiKey - The API key for the client.
   * @property {fetchTimeoutMs} fetchTimeoutMs - The timeout for the fetch request in milliseconds. Defaults to 5000.
   * @property {boolean} useGatewayURL - Whether to use the gateway URL. Defaults to false.
   */

  /**
   * The custom attributes for the user.
   */

  var FeatureGateEnvironment = /*#__PURE__*/function (FeatureGateEnvironment) {
    FeatureGateEnvironment["Development"] = "development";
    FeatureGateEnvironment["Staging"] = "staging";
    FeatureGateEnvironment["Production"] = "production";
    return FeatureGateEnvironment;
  }({});

  // If adding new values here, please check FeatureGates.getDefaultPerimeter to make sure it still returns something sensible.
  var PerimeterType = /*#__PURE__*/function (PerimeterType) {
    PerimeterType["COMMERCIAL"] = "commercial";
    PerimeterType["FEDRAMP_MODERATE"] = "fedramp-moderate";
    return PerimeterType;
  }({});

  // Type magic to get the JSDoc comments from the Client class methods to appear on the static
  // methods in FeatureGates where the property name and function type are identical

  var _excluded$1 = ["api", "disableCurrentPageLogging", "loggingIntervalMillis", "loggingBufferMaxSize", "localMode", "eventLoggingApi", "eventLoggingApiForRetries", "disableLocalStorage", "ignoreWindowUndefined", "disableAllLogging", "initTimeoutMs", "disableNetworkKeepalive", "overrideStableID", "disableErrorLogging", "disableAutoMetricsLogging"];
  function ownKeys$6(e, r) {
    var t = Object.keys(e);
    if (Object.getOwnPropertySymbols) {
      var o = Object.getOwnPropertySymbols(e);
      r && (o = o.filter(function (r) {
        return Object.getOwnPropertyDescriptor(e, r).enumerable;
      })), t.push.apply(t, o);
    }
    return t;
  }
  function _objectSpread$6(e) {
    for (var r = 1; r < arguments.length; r++) {
      var t = null != arguments[r] ? arguments[r] : {};
      r % 2 ? ownKeys$6(Object(t), !0).forEach(function (r) {
        _defineProperty(e, r, t[r]);
      }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys$6(Object(t)).forEach(function (r) {
        Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r));
      });
    }
    return e;
  }
  var getOptionsWithDefaults = function getOptionsWithDefaults(options) {
    return _objectSpread$6({
      /**
       * If more federal PerimeterTypes are added in the future, this should be updated so
       * that isFedRamp() === true always returns the strictest perimeter.
       */
      perimeter: isFedRamp() ? PerimeterType.FEDRAMP_MODERATE : PerimeterType.COMMERCIAL
    }, options);
  };
  var shallowEquals = function shallowEquals(objectA, objectB) {
    if (!objectA && !objectB) {
      return true;
    }
    if (!objectA || !objectB) {
      return false;
    }
    var aEntries = Object.entries(objectA);
    var bEntries = Object.entries(objectB);
    if (aEntries.length !== bEntries.length) {
      return false;
    }
    var ascendingKeyOrder = function ascendingKeyOrder(_ref, _ref2) {
      var _ref3 = _slicedToArray(_ref, 1),
        key1 = _ref3[0];
      var _ref4 = _slicedToArray(_ref2, 1),
        key2 = _ref4[0];
      return key1.localeCompare(key2);
    };
    aEntries.sort(ascendingKeyOrder);
    bEntries.sort(ascendingKeyOrder);
    for (var i = 0; i < aEntries.length; i++) {
      var _aEntries$i = _slicedToArray(aEntries[i], 2),
        aValue = _aEntries$i[1];
      var _bEntries$i = _slicedToArray(bEntries[i], 2),
        bValue = _bEntries$i[1];
      if (aValue !== bValue) {
        return false;
      }
    }
    return true;
  };

  /**
   * This method creates an instance of StatsigUser from the given set of identifiers and
   * attributes.
   */
  var toStatsigUser = function toStatsigUser(identifiers, customAttributes, sdkKey) {
    var user = {
      customIDs: !(customAttributes !== null && customAttributes !== void 0 && customAttributes.stableID) && sdkKey ? _objectSpread$6({
        stableID: jsClient.StableID.get(sdkKey)
      }, identifiers) : identifiers,
      custom: customAttributes
    };
    if (identifiers.atlassianAccountId) {
      user.userID = identifiers.atlassianAccountId;
    }
    return user;
  };
  var migrateInitializationOptions = function migrateInitializationOptions(options) {
    var api = options.api,
      disableCurrentPageLogging = options.disableCurrentPageLogging,
      loggingIntervalMillis = options.loggingIntervalMillis,
      loggingBufferMaxSize = options.loggingBufferMaxSize,
      localMode = options.localMode,
      eventLoggingApi = options.eventLoggingApi,
      eventLoggingApiForRetries = options.eventLoggingApiForRetries,
      disableLocalStorage = options.disableLocalStorage,
      ignoreWindowUndefined = options.ignoreWindowUndefined,
      disableAllLogging = options.disableAllLogging;
      options.initTimeoutMs;
      options.disableNetworkKeepalive;
      options.overrideStableID;
      options.disableErrorLogging;
      options.disableAutoMetricsLogging;
      var rest = _objectWithoutProperties(options, _excluded$1);
    return _objectSpread$6(_objectSpread$6({}, rest), {}, {
      networkConfig: {
        api: api,
        logEventUrl: eventLoggingApi ? eventLoggingApi + 'rgstr' : undefined,
        logEventFallbackUrls: eventLoggingApiForRetries ? [eventLoggingApiForRetries] : undefined,
        preventAllNetworkTraffic: localMode || !ignoreWindowUndefined && typeof window === 'undefined'
      },
      includeCurrentPageUrlWithEvents: !disableCurrentPageLogging,
      loggingIntervalMs: loggingIntervalMillis,
      loggingBufferMaxSize: loggingBufferMaxSize,
      disableStorage: disableLocalStorage === undefined ? localMode : disableLocalStorage,
      disableLogging: disableAllLogging === undefined ? localMode : disableAllLogging
    });
  };
  var evaluationReasonMappings = Object.entries(EvaluationReason).map(function (_ref5) {
    var _ref6 = _slicedToArray(_ref5, 2),
      key = _ref6[0],
      value = _ref6[1];
    return [key.toLowerCase(), value];
  });
  var migrateEvaluationDetails = function migrateEvaluationDetails(details) {
    var _evaluationReasonMapp, _evaluationReasonMapp2, _details$receivedAt;
    var reasonLower = details.reason.toLowerCase();
    return {
      reason: (_evaluationReasonMapp = (_evaluationReasonMapp2 = evaluationReasonMappings.find(function (_ref7) {
        var _ref8 = _slicedToArray(_ref7, 1),
          key = _ref8[0];
        return reasonLower.includes(key);
      })) === null || _evaluationReasonMapp2 === void 0 ? void 0 : _evaluationReasonMapp2[1]) !== null && _evaluationReasonMapp !== void 0 ? _evaluationReasonMapp : EvaluationReason.Unknown,
      time: (_details$receivedAt = details.receivedAt) !== null && _details$receivedAt !== void 0 ? _details$receivedAt : Date.now()
    };
  };

  // Reference: https://github.com/statsig-io/js-lite/blob/main/src/DynamicConfig.ts
  var DynamicConfig = /*#__PURE__*/function () {
    function DynamicConfig(configName, configValue, ruleID, evaluationDetails) {
      var secondaryExposures = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : [];
      var allocatedExperimentName = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : '';
      var onDefaultValueFallback = arguments.length > 6 && arguments[6] !== undefined ? arguments[6] : null;
      _classCallCheck(this, DynamicConfig);
      this.value = configValue;
      this._name = configName;
      this._ruleID = ruleID;
      this._secondaryExposures = secondaryExposures;
      this._allocatedExperimentName = allocatedExperimentName;
      this._evaluationDetails = evaluationDetails;
      this._onDefaultValueFallback = onDefaultValueFallback;
    }
    return _createClass(DynamicConfig, [{
      key: "get",
      value: function get(key, defaultValue, typeGuard) {
        var _this$_onDefaultValue2;
        var val = this.getValue(key, defaultValue);
        if (val == null) {
          return defaultValue;
        }
        var expectedType = Array.isArray(defaultValue) ? 'array' : _typeof(defaultValue);
        var actualType = Array.isArray(val) ? 'array' : _typeof(val);
        if (typeGuard) {
          var _this$_onDefaultValue;
          if (typeGuard(val)) {
            this.fireExposure(key);
            return val;
          }
          (_this$_onDefaultValue = this._onDefaultValueFallback) === null || _this$_onDefaultValue === void 0 || _this$_onDefaultValue.call(this, this, key, expectedType, actualType);
          return defaultValue;
        }
        if (defaultValue == null || expectedType === actualType) {
          this.fireExposure(key);
          return val;
        }
        (_this$_onDefaultValue2 = this._onDefaultValueFallback) === null || _this$_onDefaultValue2 === void 0 || _this$_onDefaultValue2.call(this, this, key, expectedType, actualType);
        return defaultValue;
      }
    }, {
      key: "getValue",
      value: function getValue(key, defaultValue) {
        if (key == null) {
          return this.value;
        }
        if (defaultValue == null) {
          defaultValue = null;
        }
        if (this.value[key] == null) {
          return defaultValue;
        }
        this.fireExposure(key);
        return this.value[key];
      }
    }, {
      key: "fireExposure",
      value: function fireExposure(key) {
        // Call the wrapped experiment's get method to fire exposure
        if (this.experiment) {
          this.experiment.get(key);
        }
      }
    }], [{
      key: "fromExperiment",
      value: function fromExperiment(experiment) {
        var _experiment$__evaluat, _experiment$groupName;
        var config = new DynamicConfig(experiment.name, experiment.value, experiment.ruleID, migrateEvaluationDetails(experiment.details), (_experiment$__evaluat = experiment.__evaluation) === null || _experiment$__evaluat === void 0 ? void 0 : _experiment$__evaluat.secondary_exposures, (_experiment$groupName = experiment.groupName) !== null && _experiment$groupName !== void 0 ? _experiment$groupName : undefined);
        config.experiment = experiment;
        return config;
      }
    }]);
  }();

  // Reference: https://github.com/statsig-io/js-lite/blob/main/src/Layer.ts
  var Layer = /*#__PURE__*/function () {
    function Layer(name, layerValue, ruleID, evaluationDetails) {
      var logParameterFunction = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : null;
      var secondaryExposures = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : [];
      var undelegatedSecondaryExposures = arguments.length > 6 && arguments[6] !== undefined ? arguments[6] : [];
      var allocatedExperimentName = arguments.length > 7 && arguments[7] !== undefined ? arguments[7] : '';
      var explicitParameters = arguments.length > 8 && arguments[8] !== undefined ? arguments[8] : [];
      _classCallCheck(this, Layer);
      this._logParameterFunction = logParameterFunction;
      this._name = name;
      this._value = JSON.parse(JSON.stringify(layerValue !== null && layerValue !== void 0 ? layerValue : {}));
      this._ruleID = ruleID !== null && ruleID !== void 0 ? ruleID : '';
      this._evaluationDetails = evaluationDetails;
      this._secondaryExposures = secondaryExposures;
      this._undelegatedSecondaryExposures = undelegatedSecondaryExposures;
      this._allocatedExperimentName = allocatedExperimentName;
      this._explicitParameters = explicitParameters;
    }
    return _createClass(Layer, [{
      key: "get",
      value: function get(key, defaultValue, typeGuard) {
        var _this = this;
        var val = this._value[key];
        if (val == null) {
          return defaultValue;
        }
        var logAndReturn = function logAndReturn() {
          _this._logLayerParameterExposure(key);
          return val;
        };
        if (typeGuard) {
          return typeGuard(val) ? logAndReturn() : defaultValue;
        }
        if (defaultValue == null) {
          return logAndReturn();
        }
        if (_typeof(val) === _typeof(defaultValue) && Array.isArray(defaultValue) === Array.isArray(val)) {
          return logAndReturn();
        }
        return defaultValue;
      }
    }, {
      key: "getValue",
      value: function getValue(key, defaultValue) {
        // eslint-disable-next-line eqeqeq
        if (defaultValue == undefined) {
          defaultValue = null;
        }
        var val = this._value[key];
        if (val != null) {
          this._logLayerParameterExposure(key);
        }
        return val !== null && val !== void 0 ? val : defaultValue;
      }
    }, {
      key: "_logLayerParameterExposure",
      value: function _logLayerParameterExposure(parameterName) {
        var _this$_logParameterFu;
        (_this$_logParameterFu = this._logParameterFunction) === null || _this$_logParameterFu === void 0 || _this$_logParameterFu.call(this, this, parameterName);
      }
    }], [{
      key: "fromLayer",
      value: function fromLayer(layer) {
        var _layer$__evaluation, _layer$__evaluation2, _layer$__evaluation3, _layer$__evaluation4;
        var value = new Layer(layer.name, layer.__value, layer.ruleID, migrateEvaluationDetails(layer.details), function (_layer, parameterName) {
          return layer.get(parameterName);
        }, (_layer$__evaluation = layer.__evaluation) === null || _layer$__evaluation === void 0 ? void 0 : _layer$__evaluation.secondary_exposures, (_layer$__evaluation2 = layer.__evaluation) === null || _layer$__evaluation2 === void 0 ? void 0 : _layer$__evaluation2.undelegated_secondary_exposures, (_layer$__evaluation3 = layer.__evaluation) === null || _layer$__evaluation3 === void 0 ? void 0 : _layer$__evaluation3.allocated_experiment_name, (_layer$__evaluation4 = layer.__evaluation) === null || _layer$__evaluation4 === void 0 ? void 0 : _layer$__evaluation4.explicit_parameters);
        return value;
      }
    }]);
  }();

  /// <reference types="node" />
  var CLIENT_VERSION = "4.26.4";

  function _possibleConstructorReturn(self, call) {
    if (call && (_typeof(call) === "object" || typeof call === "function")) {
      return call;
    } else if (call !== void 0) {
      throw new TypeError("Derived constructors may only return object or undefined");
    }
    return _assertThisInitialized(self);
  }

  function _getPrototypeOf(o) {
    _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function _getPrototypeOf(o) {
      return o.__proto__ || Object.getPrototypeOf(o);
    };
    return _getPrototypeOf(o);
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function");
    }
    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        writable: true,
        configurable: true
      }
    });
    Object.defineProperty(subClass, "prototype", {
      writable: false
    });
    if (superClass) _setPrototypeOf(subClass, superClass);
  }

  function _isNativeFunction(fn) {
    try {
      return Function.toString.call(fn).indexOf("[native code]") !== -1;
    } catch (e) {
      return typeof fn === "function";
    }
  }

  function _wrapNativeSuper(Class) {
    var _cache = typeof Map === "function" ? new Map() : undefined;
    _wrapNativeSuper = function _wrapNativeSuper(Class) {
      if (Class === null || !_isNativeFunction(Class)) return Class;
      if (typeof Class !== "function") {
        throw new TypeError("Super expression must either be null or a function");
      }
      if (typeof _cache !== "undefined") {
        if (_cache.has(Class)) return _cache.get(Class);
        _cache.set(Class, Wrapper);
      }
      function Wrapper() {
        return _construct(Class, arguments, _getPrototypeOf(this).constructor);
      }
      Wrapper.prototype = Object.create(Class.prototype, {
        constructor: {
          value: Wrapper,
          enumerable: false,
          writable: true,
          configurable: true
        }
      });
      return _setPrototypeOf(Wrapper, Class);
    };
    return _wrapNativeSuper(Class);
  }

  function _callSuper$1(t, o, e) {
    return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct$1() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e));
  }
  function _isNativeReflectConstruct$1() {
    try {
      var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {}));
    } catch (t) {}
    return (_isNativeReflectConstruct$1 = function _isNativeReflectConstruct() {
      return !!t;
    })();
  }
  var ResponseError = /*#__PURE__*/function (_Error) {
    function ResponseError(message) {
      _classCallCheck(this, ResponseError);
      return _callSuper$1(this, ResponseError, [message]);
    }
    _inherits(ResponseError, _Error);
    return _createClass(ResponseError);
  }(/*#__PURE__*/_wrapNativeSuper(Error));

  function ownKeys$5(e, r) {
    var t = Object.keys(e);
    if (Object.getOwnPropertySymbols) {
      var o = Object.getOwnPropertySymbols(e);
      r && (o = o.filter(function (r) {
        return Object.getOwnPropertyDescriptor(e, r).enumerable;
      })), t.push.apply(t, o);
    }
    return t;
  }
  function _objectSpread$5(e) {
    for (var r = 1; r < arguments.length; r++) {
      var t = null != arguments[r] ? arguments[r] : {};
      r % 2 ? ownKeys$5(Object(t), !0).forEach(function (r) {
        _defineProperty(e, r, t[r]);
      }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys$5(Object(t)).forEach(function (r) {
        Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r));
      });
    }
    return e;
  }
  var DEFAULT_REQUEST_TIMEOUT_MS = 5000;
  var PROD_BASE_URL = 'https://api.atlassian.com/flags';
  var STAGING_BASE_URL = 'https://api.stg.atlassian.com/flags';
  var DEV_BASE_URL = 'https://api.dev.atlassian.com/flags';
  var FEDM_STAGING_BASE_URL = 'https://api.stg.atlassian-us-gov-mod.com/flags';
  var FEDM_PROD_BASE_URL = 'https://api.atlassian-us-gov-mod.com/flags';
  var GATEWAY_BASE_URL = '/gateway/api/flags';
  var Fetcher = /*#__PURE__*/function () {
    function Fetcher() {
      _classCallCheck(this, Fetcher);
    }
    return _createClass(Fetcher, null, [{
      key: "fetchClientSdk",
      value: function () {
        var _fetchClientSdk = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee(fetcherOptions) {
          var targetApp, url;
          return regenerator.wrap(function _callee$(_context) {
            while (1) switch (_context.prev = _context.next) {
              case 0:
                targetApp = fetcherOptions.targetApp;
                url = "/api/v2/frontend/clientSdkKey/".concat(targetApp);
                _context.prev = 2;
                _context.next = 5;
                return this.fetchRequest(url, 'GET', fetcherOptions);
              case 5:
                return _context.abrupt("return", _context.sent);
              case 8:
                _context.prev = 8;
                _context.t0 = _context["catch"](2);
                if (!(_context.t0 instanceof Error)) {
                  _context.next = 12;
                  break;
                }
                throw _context.t0;
              case 12:
                throw Error('Failed to retrieve client sdk key');
              case 13:
              case "end":
                return _context.stop();
            }
          }, _callee, this, [[2, 8]]);
        }));
        function fetchClientSdk(_x) {
          return _fetchClientSdk.apply(this, arguments);
        }
        return fetchClientSdk;
      }()
    }, {
      key: "fetchExperimentValues",
      value: function () {
        var _fetchExperimentValues = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee2(fetcherOptions, identifiers, customAttributes) {
          var requestBody;
          return regenerator.wrap(function _callee2$(_context2) {
            while (1) switch (_context2.prev = _context2.next) {
              case 0:
                requestBody = {
                  identifiers: identifiers,
                  customAttributes: customAttributes,
                  targetApp: fetcherOptions.targetApp
                };
                _context2.prev = 1;
                _context2.next = 4;
                return this.fetchRequest('/api/v2/frontend/experimentValues', 'POST', fetcherOptions, requestBody);
              case 4:
                return _context2.abrupt("return", _context2.sent);
              case 7:
                _context2.prev = 7;
                _context2.t0 = _context2["catch"](1);
                if (!(_context2.t0 instanceof Error)) {
                  _context2.next = 11;
                  break;
                }
                throw _context2.t0;
              case 11:
                throw Error('Failed to retrieve experiment values');
              case 12:
              case "end":
                return _context2.stop();
            }
          }, _callee2, this, [[1, 7]]);
        }));
        function fetchExperimentValues(_x2, _x3, _x4) {
          return _fetchExperimentValues.apply(this, arguments);
        }
        return fetchExperimentValues;
      }()
    }, {
      key: "handleResponseError",
      value: function () {
        var _handleResponseError = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee3(response) {
          var body;
          return regenerator.wrap(function _callee3$(_context3) {
            while (1) switch (_context3.prev = _context3.next) {
              case 0:
                if (response.ok) {
                  _context3.next = 5;
                  break;
                }
                _context3.next = 3;
                return response.text();
              case 3:
                body = _context3.sent;
                throw new ResponseError("Non 2xx response status received, status: ".concat(response.status, ", body: ").concat(JSON.stringify(body)));
              case 5:
                if (!(response.status === 204)) {
                  _context3.next = 7;
                  break;
                }
                throw new ResponseError('Unexpected 204 response');
              case 7:
              case "end":
                return _context3.stop();
            }
          }, _callee3);
        }));
        function handleResponseError(_x5) {
          return _handleResponseError.apply(this, arguments);
        }
        return handleResponseError;
      }()
    }, {
      key: "extractResponseBody",
      value: function () {
        var _extractResponseBody = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee4(response) {
          var value;
          return regenerator.wrap(function _callee4$(_context4) {
            while (1) switch (_context4.prev = _context4.next) {
              case 0:
                _context4.next = 2;
                return response.text();
              case 2:
                value = _context4.sent;
                return _context4.abrupt("return", JSON.parse(value));
              case 4:
              case "end":
                return _context4.stop();
            }
          }, _callee4);
        }));
        function extractResponseBody(_x6) {
          return _extractResponseBody.apply(this, arguments);
        }
        return extractResponseBody;
      }()
    }, {
      key: "getBaseUrl",
      value: function getBaseUrl(serviceEnv) {
        var useGatewayUrl = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
        var perimeter = arguments.length > 2 ? arguments[2] : undefined;
        if (useGatewayUrl) {
          return GATEWAY_BASE_URL;
        }
        if (perimeter === PerimeterType.FEDRAMP_MODERATE) {
          switch (serviceEnv) {
            case FeatureGateEnvironment.Production:
              return FEDM_PROD_BASE_URL;
            case FeatureGateEnvironment.Staging:
              return FEDM_STAGING_BASE_URL;
            default:
              throw new Error("Invalid environment \"".concat(serviceEnv, "\" for \"").concat(perimeter, "\" perimeter"));
          }
        } else if (perimeter === PerimeterType.COMMERCIAL) {
          switch (serviceEnv) {
            case FeatureGateEnvironment.Development:
              return DEV_BASE_URL;
            case FeatureGateEnvironment.Staging:
              return STAGING_BASE_URL;
            default:
              return PROD_BASE_URL;
          }
        } else {
          throw new Error("Invalid perimeter \"".concat(perimeter, "\""));
        }
      }
    }, {
      key: "fetchRequest",
      value: function () {
        var _fetchRequest = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee5(path, method, fetcherOptions, body) {
          var baseUrl, fetchTimeout, abortSignal, abortController, response;
          return regenerator.wrap(function _callee5$(_context5) {
            while (1) switch (_context5.prev = _context5.next) {
              case 0:
                baseUrl = Fetcher.getBaseUrl(fetcherOptions.environment, fetcherOptions.useGatewayURL, fetcherOptions.perimeter);
                fetchTimeout = fetcherOptions.fetchTimeoutMs || DEFAULT_REQUEST_TIMEOUT_MS;
                if (AbortSignal.timeout) {
                  abortSignal = AbortSignal.timeout(fetchTimeout);
                } else if (AbortController) {
                  abortController = new AbortController();
                  abortSignal = abortController.signal;
                  setTimeout(function () {
                    return abortController.abort();
                  }, fetchTimeout);
                }
                _context5.next = 5;
                return fetch("".concat(baseUrl).concat(path), _objectSpread$5({
                  method: method,
                  headers: {
                    'Content-Type': 'application/json',
                    'X-Client-Name': 'feature-gate-js-client',
                    'X-Client-Version': CLIENT_VERSION,
                    'X-API-KEY': fetcherOptions.apiKey
                  },
                  signal: abortSignal
                }, body && {
                  body: JSON.stringify(body)
                }));
              case 5:
                response = _context5.sent;
                _context5.next = 8;
                return this.handleResponseError(response);
              case 8:
                _context5.next = 10;
                return this.extractResponseBody(response);
              case 10:
                return _context5.abrupt("return", _context5.sent);
              case 11:
              case "end":
                return _context5.stop();
            }
          }, _callee5, this);
        }));
        function fetchRequest(_x7, _x8, _x9, _x10) {
          return _fetchRequest.apply(this, arguments);
        }
        return fetchRequest;
      }()
    }]);
  }();

  function _superPropBase(object, property) {
    while (!Object.prototype.hasOwnProperty.call(object, property)) {
      object = _getPrototypeOf(object);
      if (object === null) break;
    }
    return object;
  }

  function _get() {
    if (typeof Reflect !== "undefined" && Reflect.get) {
      _get = Reflect.get.bind();
    } else {
      _get = function _get(target, property, receiver) {
        var base = _superPropBase(target, property);
        if (!base) return;
        var desc = Object.getOwnPropertyDescriptor(base, property);
        if (desc.get) {
          return desc.get.call(arguments.length < 3 ? target : receiver);
        }
        return desc.value;
      };
    }
    return _get.apply(this, arguments);
  }

  function ownKeys$4(e, r) {
    var t = Object.keys(e);
    if (Object.getOwnPropertySymbols) {
      var o = Object.getOwnPropertySymbols(e);
      r && (o = o.filter(function (r) {
        return Object.getOwnPropertyDescriptor(e, r).enumerable;
      })), t.push.apply(t, o);
    }
    return t;
  }
  function _objectSpread$4(e) {
    for (var r = 1; r < arguments.length; r++) {
      var t = null != arguments[r] ? arguments[r] : {};
      r % 2 ? ownKeys$4(Object(t), !0).forEach(function (r) {
        _defineProperty(e, r, t[r]);
      }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys$4(Object(t)).forEach(function (r) {
        Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r));
      });
    }
    return e;
  }
  function _callSuper(t, o, e) {
    return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e));
  }
  function _isNativeReflectConstruct() {
    try {
      var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {}));
    } catch (t) {}
    return (_isNativeReflectConstruct = function _isNativeReflectConstruct() {
      return !!t;
    })();
  }
  function _superPropGet(t, o, e, r) {
    var p = _get(_getPrototypeOf(1 & r ? t.prototype : t), o, e);
    return 2 & r && "function" == typeof p ? function (t) {
      return p.apply(e, t);
    } : p;
  }

  /**
   * Data adapter which only uses bootstrap data and will never fetch from network or cache.
   * We do this because we control the fetching of bootstrap data from FFS in Client.ts whereas the
   * default data adapter fetches from Statsig servers.
   */
  var NoFetchDataAdapter = /*#__PURE__*/function (_DataAdapterCore) {
    function NoFetchDataAdapter() {
      var _this;
      _classCallCheck(this, NoFetchDataAdapter);
      _this = _callSuper(this, NoFetchDataAdapter, ['NoFetchDataAdapter', 'nofetch']);
      _defineProperty(_this, "bootstrapResult", null);
      return _this;
    }

    /**
     * Make sure to call this **before** calling `initializeAsync` or `updateUserAsync` but
     * after the Statsig client has been created!
     */
    _inherits(NoFetchDataAdapter, _DataAdapterCore);
    return _createClass(NoFetchDataAdapter, [{
      key: "setBootstrapData",
      value: function setBootstrapData(data) {
        this.bootstrapResult = data ? {
          source: 'Bootstrap',
          data: JSON.stringify(data),
          receivedAt: Date.now(),
          stableID: jsClient.StableID.get(this._getSdkKey()),
          fullUserHash: null
        } : null;
      }
    }, {
      key: "prefetchData",
      value: function () {
        var _prefetchData = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee(_user, _options) {
          return regenerator.wrap(function _callee$(_context) {
            while (1) switch (_context.prev = _context.next) {
              case 0:
              case "end":
                return _context.stop();
            }
          }, _callee);
        }));
        function prefetchData(_x, _x2) {
          return _prefetchData.apply(this, arguments);
        }
        return prefetchData;
      }()
    }, {
      key: "getDataAsync",
      value: function () {
        var _getDataAsync = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee2(_current, user, _options) {
          return regenerator.wrap(function _callee2$(_context2) {
            while (1) switch (_context2.prev = _context2.next) {
              case 0:
                return _context2.abrupt("return", this.bootstrapResult && _objectSpread$4(_objectSpread$4({}, this.bootstrapResult), {}, {
                  fullUserHash: jsClient._getFullUserHash(user)
                }));
              case 1:
              case "end":
                return _context2.stop();
            }
          }, _callee2, this);
        }));
        function getDataAsync(_x3, _x4, _x5) {
          return _getDataAsync.apply(this, arguments);
        }
        return getDataAsync;
      }()
    }, {
      key: "getDataSync",
      value: function getDataSync(user) {
        return this.bootstrapResult && _objectSpread$4(_objectSpread$4({}, this.bootstrapResult), {}, {
          fullUserHash: jsClient._getFullUserHash(user)
        });
      }
    }, {
      key: "_fetchFromNetwork",
      value: function () {
        var _fetchFromNetwork2 = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee3(_current, _user, _options) {
          return regenerator.wrap(function _callee3$(_context3) {
            while (1) switch (_context3.prev = _context3.next) {
              case 0:
                return _context3.abrupt("return", null);
              case 1:
              case "end":
                return _context3.stop();
            }
          }, _callee3);
        }));
        function _fetchFromNetwork(_x6, _x7, _x8) {
          return _fetchFromNetwork2.apply(this, arguments);
        }
        return _fetchFromNetwork;
      }()
    }, {
      key: "_getCacheKey",
      value: function _getCacheKey(user) {
        // Same logic as default data adapter
        // https://github.com/statsig-io/js-client-monorepo/blob/main/packages/js-client/src/StatsigEvaluationsDataAdapter.ts
        var key = jsClient._getStorageKey(this._getSdkKey(), user);
        return "".concat(jsClient.DataAdapterCachePrefix, ".").concat(this._cacheSuffix, ".").concat(key);
      }
    }, {
      key: "_isCachedResultValidFor204",
      value: function _isCachedResultValidFor204(_result, _user) {
        return false;
      }
    }, {
      key: "setDataLegacy",
      value: function setDataLegacy(data, user) {
        _superPropGet(NoFetchDataAdapter, "setData", this, 3)([data, user]);
      }

      // Do not stringify options property since that includes this adapter and will
      // cause a circular reference when Statsig sends diagnostic events and including
      // values is not necessary and makes the result huge
    }, {
      key: "toJSON",
      value: function toJSON() {
        var result = _objectSpread$4({}, this);
        delete result._options;
        delete result._inMemoryCache;
        delete result.bootstrapResult;
        return result;
      }
    }]);
  }(jsClient.DataAdapterCore);

  function ownKeys$3(e, r) {
    var t = Object.keys(e);
    if (Object.getOwnPropertySymbols) {
      var o = Object.getOwnPropertySymbols(e);
      r && (o = o.filter(function (r) {
        return Object.getOwnPropertyDescriptor(e, r).enumerable;
      })), t.push.apply(t, o);
    }
    return t;
  }
  function _objectSpread$3(e) {
    for (var r = 1; r < arguments.length; r++) {
      var t = null != arguments[r] ? arguments[r] : {};
      r % 2 ? ownKeys$3(Object(t), !0).forEach(function (r) {
        _defineProperty(e, r, t[r]);
      }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys$3(Object(t)).forEach(function (r) {
        Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r));
      });
    }
    return e;
  }
  function _createForOfIteratorHelper$1(r, e) {
    var t = "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"];
    if (!t) {
      if (Array.isArray(r) || (t = _unsupportedIterableToArray$1(r)) || e && r && "number" == typeof r.length) {
        t && (r = t);
        var _n = 0,
          F = function F() {};
        return {
          s: F,
          n: function n() {
            return _n >= r.length ? {
              done: !0
            } : {
              done: !1,
              value: r[_n++]
            };
          },
          e: function e(r) {
            throw r;
          },
          f: F
        };
      }
      throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
    }
    var o,
      a = !0,
      u = !1;
    return {
      s: function s() {
        t = t.call(r);
      },
      n: function n() {
        var r = t.next();
        return a = r.done, r;
      },
      e: function e(r) {
        u = !0, o = r;
      },
      f: function f() {
        try {
          a || null == t.return || t.return();
        } finally {
          if (u) throw o;
        }
      }
    };
  }
  function _unsupportedIterableToArray$1(r, a) {
    if (r) {
      if ("string" == typeof r) return _arrayLikeToArray$1(r, a);
      var t = {}.toString.call(r).slice(8, -1);
      return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray$1(r, a) : void 0;
    }
  }
  function _arrayLikeToArray$1(r, a) {
    (null == a || a > r.length) && (a = r.length);
    for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e];
    return n;
  }
  var LOCAL_OVERRIDE_REASON = 'LocalOverride:Recognized';
  var LOCAL_STORAGE_KEY = 'STATSIG_OVERRIDES';
  var LEGACY_LOCAL_STORAGE_KEY = 'STATSIG_JS_LITE_LOCAL_OVERRIDES';
  var makeEmptyStore = function makeEmptyStore() {
    return {
      gates: {},
      configs: {},
      layers: {}
    };
  };
  var djb2MapKey = function djb2MapKey(hash, kind) {
    return kind + ':' + hash;
  };

  /**
   * Custom implementation of `@statsig/js-local-overrides` package with support for local storage
   * so we can keep the existing behavior where overrides are cached locally. Also designed for
   * compatibility with the old override system (eg. no `experiments` field, `configs` is used
   * instead).
   *
   * [Reference](https://github.com/statsig-io/js-client-monorepo/blob/main/packages/js-local-overrides/src/LocalOverrideAdapter.ts)
   */
  var PersistentOverrideAdapter = /*#__PURE__*/function () {
    function PersistentOverrideAdapter(localStorageKey) {
      _classCallCheck(this, PersistentOverrideAdapter);
      this._overrides = makeEmptyStore();
      this._djb2Map = new Map();
      this._localStorageKey = localStorageKey;
    }
    return _createClass(PersistentOverrideAdapter, [{
      key: "parseStoredOverrides",
      value: function parseStoredOverrides(localStorageKey) {
        try {
          var json = window.localStorage.getItem(localStorageKey);
          if (!json) {
            return makeEmptyStore();
          }
          return JSON.parse(json);
        } catch (_unused) {
          return makeEmptyStore();
        }
      }
    }, {
      key: "mergeOverrides",
      value: function mergeOverrides() {
        var merged = makeEmptyStore();
        for (var _len = arguments.length, allOverrides = new Array(_len), _key = 0; _key < _len; _key++) {
          allOverrides[_key] = arguments[_key];
        }
        for (var _i = 0, _allOverrides = allOverrides; _i < _allOverrides.length; _i++) {
          var overrides = _allOverrides[_i];
          for (var _i2 = 0, _Object$entries = Object.entries((_overrides$gates = overrides.gates) !== null && _overrides$gates !== void 0 ? _overrides$gates : {}); _i2 < _Object$entries.length; _i2++) {
            var _overrides$gates;
            var _Object$entries$_i = _slicedToArray(_Object$entries[_i2], 2),
              name = _Object$entries$_i[0],
              value = _Object$entries$_i[1];
            merged.gates[name] = value;
          }
          for (var _i3 = 0, _Object$entries2 = Object.entries((_overrides$configs = overrides.configs) !== null && _overrides$configs !== void 0 ? _overrides$configs : {}); _i3 < _Object$entries2.length; _i3++) {
            var _overrides$configs;
            var _Object$entries2$_i = _slicedToArray(_Object$entries2[_i3], 2),
              _name = _Object$entries2$_i[0],
              _value = _Object$entries2$_i[1];
            merged.configs[_name] = _value;
          }
          for (var _i4 = 0, _Object$entries3 = Object.entries((_overrides$layers = overrides.layers) !== null && _overrides$layers !== void 0 ? _overrides$layers : {}); _i4 < _Object$entries3.length; _i4++) {
            var _overrides$layers;
            var _Object$entries3$_i = _slicedToArray(_Object$entries3[_i4], 2),
              _name2 = _Object$entries3$_i[0],
              _value2 = _Object$entries3$_i[1];
            merged.layers[_name2] = _value2;
          }
        }
        return merged;
      }
    }, {
      key: "initFromStoredOverrides",
      value: function initFromStoredOverrides() {
        var storedOverrides = this.mergeOverrides(this._overrides, this.parseStoredOverrides(LEGACY_LOCAL_STORAGE_KEY), this.parseStoredOverrides(this._localStorageKey));

        // In version 4.24.0 we introduced hashes in this override adapter, but had a bug which would cause
        // multiple hashes to continue being created. This code here removes these hashes since we've moved
        // to using a more reliable and easier to maintain map in `_djb2Map`.
        for (var _i5 = 0, _Object$values = Object.values(storedOverrides); _i5 < _Object$values.length; _i5++) {
          var container = _Object$values[_i5];
          var allKeys = new Set(Object.keys(container));
          var _iterator = _createForOfIteratorHelper$1(allKeys),
            _step;
          try {
            for (_iterator.s(); !(_step = _iterator.n()).done;) {
              var name = _step.value;
              var hash = clientCore._DJB2(name);
              if (allKeys.has(hash)) {
                delete container[hash];
              }
            }
          } catch (err) {
            _iterator.e(err);
          } finally {
            _iterator.f();
          }
        }
        this.applyOverrides(storedOverrides);
      }
    }, {
      key: "saveOverrides",
      value: function saveOverrides() {
        try {
          window.localStorage.setItem(this._localStorageKey, JSON.stringify(this._overrides));
        } catch (_unused2) {
          // ignored - window is not defined in non-browser environments, and we don't save things there
          // (things like SSR, etc)
        }
      }
    }, {
      key: "getOverrides",
      value: function getOverrides() {
        return this.mergeOverrides(this._overrides);
      }
    }, {
      key: "applyOverrides",
      value: function applyOverrides(overrides) {
        var newOverrides = _objectSpread$3(_objectSpread$3({}, makeEmptyStore()), overrides);
        this._djb2Map.clear();
        for (var _i6 = 0, _Object$entries4 = Object.entries(newOverrides); _i6 < _Object$entries4.length; _i6++) {
          var _Object$entries4$_i = _slicedToArray(_Object$entries4[_i6], 2),
            containerName = _Object$entries4$_i[0],
            container = _Object$entries4$_i[1];
          for (var _i7 = 0, _Object$entries5 = Object.entries(container); _i7 < _Object$entries5.length; _i7++) {
            var _Object$entries5$_i = _slicedToArray(_Object$entries5[_i7], 2),
              name = _Object$entries5$_i[0],
              value = _Object$entries5$_i[1];
            this._djb2Map.set(djb2MapKey(clientCore._DJB2(name), containerName), value);
          }
        }
        this._overrides = newOverrides;
      }
    }, {
      key: "setOverrides",
      value: function setOverrides(overrides) {
        this.applyOverrides(overrides);
        this.saveOverrides();
      }
    }, {
      key: "overrideGate",
      value: function overrideGate(name, value) {
        this._overrides.gates[name] = value;
        this._djb2Map.set(djb2MapKey(clientCore._DJB2(name), 'gates'), value);
        this.saveOverrides();
      }
    }, {
      key: "removeGateOverride",
      value: function removeGateOverride(name) {
        delete this._overrides.gates[name];
        this._djb2Map.delete(djb2MapKey(clientCore._DJB2(name), 'gates'));
        this.saveOverrides();
      }
    }, {
      key: "getGateOverride",
      value: function getGateOverride(current, _user) {
        var _this$_overrides$gate;
        var overridden = (_this$_overrides$gate = this._overrides.gates[current.name]) !== null && _this$_overrides$gate !== void 0 ? _this$_overrides$gate : this._djb2Map.get(djb2MapKey(current.name, 'gates'));
        if (overridden == null) {
          return null;
        }
        return _objectSpread$3(_objectSpread$3({}, current), {}, {
          value: overridden,
          details: _objectSpread$3(_objectSpread$3({}, current.details), {}, {
            reason: LOCAL_OVERRIDE_REASON
          })
        });
      }
    }, {
      key: "overrideDynamicConfig",
      value: function overrideDynamicConfig(name, value) {
        this._overrides.configs[name] = value;
        this._djb2Map.set(djb2MapKey(clientCore._DJB2(name), 'configs'), value);
        this.saveOverrides();
      }
    }, {
      key: "removeDynamicConfigOverride",
      value: function removeDynamicConfigOverride(name) {
        delete this._overrides.configs[name];
        this._djb2Map.delete(djb2MapKey(clientCore._DJB2(name), 'configs'));
        this.saveOverrides();
      }
    }, {
      key: "getDynamicConfigOverride",
      value: function getDynamicConfigOverride(current, _user) {
        return this._getConfigOverride(current, this._overrides.configs);
      }
    }, {
      key: "overrideExperiment",
      value: function overrideExperiment(name, value) {
        this._overrides.configs[name] = value;
        this._djb2Map.set(djb2MapKey(clientCore._DJB2(name), 'configs'), value);
        this.saveOverrides();
      }
    }, {
      key: "removeExperimentOverride",
      value: function removeExperimentOverride(name) {
        delete this._overrides.configs[name];
        this._djb2Map.delete(djb2MapKey(clientCore._DJB2(name), 'configs'));
        this.saveOverrides();
      }
    }, {
      key: "getExperimentOverride",
      value: function getExperimentOverride(current, _user) {
        return this._getConfigOverride(current, this._overrides.configs);
      }
    }, {
      key: "overrideLayer",
      value: function overrideLayer(name, value) {
        this._overrides.layers[name] = value;
        this._djb2Map.set(djb2MapKey(clientCore._DJB2(name), 'layers'), value);
        this.saveOverrides();
      }
    }, {
      key: "removeLayerOverride",
      value: function removeLayerOverride(name) {
        delete this._overrides.layers[name];
        this._djb2Map.delete(djb2MapKey(clientCore._DJB2(name), 'layers'));
        this.saveOverrides();
      }
    }, {
      key: "removeAllOverrides",
      value: function removeAllOverrides() {
        this._overrides = makeEmptyStore();
        window.localStorage.removeItem(LOCAL_STORAGE_KEY);
      }
    }, {
      key: "getLayerOverride",
      value: function getLayerOverride(current, _user) {
        var _this$_overrides$laye;
        var overridden = (_this$_overrides$laye = this._overrides.layers[current.name]) !== null && _this$_overrides$laye !== void 0 ? _this$_overrides$laye : this._djb2Map.get(djb2MapKey(current.name, 'layers'));
        if (overridden == null) {
          return null;
        }
        return _objectSpread$3(_objectSpread$3({}, current), {}, {
          __value: overridden,
          get: clientCore._makeTypedGet(current.name, overridden),
          details: _objectSpread$3(_objectSpread$3({}, current.details), {}, {
            reason: LOCAL_OVERRIDE_REASON
          })
        });
      }
    }, {
      key: "_getConfigOverride",
      value: function _getConfigOverride(current, lookup) {
        var _lookup$current$name;
        var overridden = (_lookup$current$name = lookup[current.name]) !== null && _lookup$current$name !== void 0 ? _lookup$current$name : this._djb2Map.get(djb2MapKey(current.name, 'configs'));
        if (overridden == null) {
          return null;
        }
        return _objectSpread$3(_objectSpread$3({}, current), {}, {
          value: overridden,
          get: clientCore._makeTypedGet(current.name, overridden),
          details: _objectSpread$3(_objectSpread$3({}, current.details), {}, {
            reason: LOCAL_OVERRIDE_REASON
          })
        });
      }
    }]);
  }();

  var _excluded = ["sdkKey", "environment", "updateUserCompletionCallback", "perimeter"];
  function ownKeys$2(e, r) {
    var t = Object.keys(e);
    if (Object.getOwnPropertySymbols) {
      var o = Object.getOwnPropertySymbols(e);
      r && (o = o.filter(function (r) {
        return Object.getOwnPropertyDescriptor(e, r).enumerable;
      })), t.push.apply(t, o);
    }
    return t;
  }
  function _objectSpread$2(e) {
    for (var r = 1; r < arguments.length; r++) {
      var t = null != arguments[r] ? arguments[r] : {};
      r % 2 ? ownKeys$2(Object(t), !0).forEach(function (r) {
        _defineProperty(e, r, t[r]);
      }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys$2(Object(t)).forEach(function (r) {
        Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r));
      });
    }
    return e;
  }
  var DEFAULT_CLIENT_KEY = 'client-default-key';
  // default event logging api is Atlassian proxy rather than Statsig's domain, to avoid ad blockers
  var DEFAULT_EVENT_LOGGING_API = 'https://xp.atlassian.com/v1/rgstr';
  var Client = /*#__PURE__*/function () {
    function Client() {
      var _this = this;
      var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
        _ref$localStorageKey = _ref.localStorageKey,
        localStorageKey = _ref$localStorageKey === void 0 ? LOCAL_STORAGE_KEY : _ref$localStorageKey;
      _classCallCheck(this, Client);
      _defineProperty(this, "initPromise", null);
      /** True if an initialize method was called and completed successfully. */
      _defineProperty(this, "initCompleted", false);
      /**
       * True if an initialize method was called and completed, meaning the client is now usable.
       * However if there was an error during initialization it may have initialized with default
       * values. Use {@link initCompleted} to check for this.
       */
      _defineProperty(this, "initWithDefaults", false);
      _defineProperty(this, "hasCheckGateErrorOccurred", false);
      _defineProperty(this, "hasGetExperimentErrorOccurred", false);
      _defineProperty(this, "hasGetExperimentValueErrorOccurred", false);
      _defineProperty(this, "hasGetLayerErrorOccurred", false);
      _defineProperty(this, "hasGetLayerValueErrorOccurred", false);
      _defineProperty(this, "subscriptions", new Subscriptions());
      _defineProperty(this, "dataAdapter", new NoFetchDataAdapter());
      /**
       * Call this if modifying the values being served by the Statsig library since it has its own
       * memoization cache which will not be updated if the values are changed outside of the library.
       */
      _defineProperty(this, "statsigValuesUpdated", function () {
        if (_this.user) {
          // Trigger a reset of the memoize cache
          _this.statsigClient.updateUserSync(_this.user, {
            disableBackgroundCacheRefresh: true
          });
        }
        _this.subscriptions.anyUpdated();
      });
      this.overrideAdapter = new PersistentOverrideAdapter(localStorageKey);
    }

    /**
     * @description
     * This method initializes the client using a network call to fetch the bootstrap values.
     * If the client is inialized with an `analyticsWebClient`, it will send an operational event
     * to GASv3 with the following attributes:
     * - targetApp: the target app of the client
     * - clientVersion: the version of the client
     * - success: whether the initialization was successful
     * - startTime: the time when the initialization started
     * - totalTime: the total time it took to initialize the client
     * - apiKey: the api key used to initialize the client
     * @param clientOptions {ClientOptions}
     * @param identifiers {Identifiers}
     * @param customAttributes {CustomAttributes}
     * @returns {Promise<void>}
     */
    return _createClass(Client, [{
      key: "initialize",
      value: function () {
        var _initialize = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee(clientOptions, identifiers, customAttributes) {
          var _this2 = this;
          var clientOptionsWithDefaults, startTime;
          return regenerator.wrap(function _callee$(_context) {
            while (1) switch (_context.prev = _context.next) {
              case 0:
                clientOptionsWithDefaults = getOptionsWithDefaults(clientOptions);
                if (!this.initPromise) {
                  _context.next = 4;
                  break;
                }
                if (!shallowEquals(clientOptionsWithDefaults, this.initOptions)) {
                  // eslint-disable-next-line no-console
                  console.warn('Feature Gates client already initialized with different options. New options were not applied.');
                }
                return _context.abrupt("return", this.initPromise);
              case 4:
                startTime = performance.now();
                this.initOptions = clientOptionsWithDefaults;
                this.initPromise = this.init(clientOptionsWithDefaults, identifiers, customAttributes).then(function () {
                  _this2.initCompleted = true;
                  _this2.initWithDefaults = true;
                }).finally(function () {
                  var endTime = performance.now();
                  var totalTime = endTime - startTime;
                  _this2.fireClientEvent(startTime, totalTime, 'initialize', _this2.initCompleted, clientOptionsWithDefaults.apiKey);
                });
                return _context.abrupt("return", this.initPromise);
              case 8:
              case "end":
                return _context.stop();
            }
          }, _callee, this);
        }));
        function initialize(_x, _x2, _x3) {
          return _initialize.apply(this, arguments);
        }
        return initialize;
      }()
      /**
       * @description
       * This method initializes the client using the provider given to call to fetch the bootstrap values.
       * If the client is initialized with an `analyticsWebClient`, it will send an operational event
       * to GASv3 with the following attributes:
       * - targetApp: the target app of the client
       * - clientVersion: the version of the client
       * - success: whether the initialization was successful
       * - startTime: the time when the initialization started
       * - totalTime: the total time it took to initialize the client
       * - apiKey: the api key used to initialize the client
       * @param clientOptions {ClientOptions}
       * @param provider {Provider}
       * @param identifiers {Identifiers}
       * @param customAttributes {CustomAttributes}
       * @returns {Promise<void>}
       */
    }, {
      key: "initializeWithProvider",
      value: function () {
        var _initializeWithProvider = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee2(clientOptions, provider, identifiers, customAttributes) {
          var _this3 = this;
          var clientOptionsWithDefaults, startTime;
          return regenerator.wrap(function _callee2$(_context2) {
            while (1) switch (_context2.prev = _context2.next) {
              case 0:
                clientOptionsWithDefaults = getOptionsWithDefaults(clientOptions);
                if (!this.initPromise) {
                  _context2.next = 4;
                  break;
                }
                if (!shallowEquals(clientOptionsWithDefaults, this.initOptions)) {
                  // eslint-disable-next-line no-console
                  console.warn('Feature Gates client already initialized with different options. New options were not applied.');
                }
                return _context2.abrupt("return", this.initPromise);
              case 4:
                startTime = performance.now();
                this.initOptions = clientOptionsWithDefaults;
                this.provider = provider;
                this.provider.setClientVersion(CLIENT_VERSION);
                if (this.provider.setApplyUpdateCallback) {
                  this.provider.setApplyUpdateCallback(this.applyUpdateCallback.bind(this));
                }
                this.initPromise = this.initWithProvider(clientOptionsWithDefaults, provider, identifiers, customAttributes).then(function () {
                  _this3.initCompleted = true;
                  _this3.initWithDefaults = true;
                }).finally(function () {
                  var endTime = performance.now();
                  var totalTime = endTime - startTime;
                  _this3.fireClientEvent(startTime, totalTime, 'initializeWithProvider', _this3.initCompleted, provider.getApiKey ? provider.getApiKey() : undefined);
                });
                return _context2.abrupt("return", this.initPromise);
              case 11:
              case "end":
                return _context2.stop();
            }
          }, _callee2, this);
        }));
        function initializeWithProvider(_x4, _x5, _x6, _x7) {
          return _initializeWithProvider.apply(this, arguments);
        }
        return initializeWithProvider;
      }()
    }, {
      key: "applyUpdateCallback",
      value: function applyUpdateCallback(experimentsResult) {
        try {
          if (this.initCompleted || this.initWithDefaults) {
            this.assertInitialized(this.statsigClient);
            this.dataAdapter.setBootstrapData(experimentsResult.experimentValues);
            this.dataAdapter.setData(JSON.stringify(experimentsResult.experimentValues));
            this.statsigValuesUpdated();
          }
        } catch (error) {
          // eslint-disable-next-line no-console
          console.warn('Error when attempting to apply update', error);
        }
      }
    }, {
      key: "fireClientEvent",
      value: function fireClientEvent(startTime, totalTime, action, success) {
        var _analyticsWebClient,
          _this4 = this;
        var apiKey = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : undefined;
        (_analyticsWebClient = this.initOptions.analyticsWebClient) === null || _analyticsWebClient === void 0 || _analyticsWebClient.then(function (analyticsWebClient) {
          var attributes = _objectSpread$2({
            targetApp: _this4.initOptions.targetApp,
            clientVersion: CLIENT_VERSION,
            success: success,
            startTime: startTime,
            totalTime: totalTime
          }, apiKey && {
            apiKey: apiKey
          });
          analyticsWebClient.sendOperationalEvent({
            action: action,
            actionSubject: 'featureGatesClient',
            attributes: attributes,
            tags: ['measurement'],
            source: '@atlaskit/feature-gate-js-client'
          });
        }).catch(function (err) {
          if (_this4.initOptions.environment !== FeatureGateEnvironment.Production) {
            // eslint-disable-next-line no-console
            console.error('Analytics web client promise did not resolve', err);
          }
        });
      }
    }, {
      key: "initializeFromValues",
      value: function () {
        var _initializeFromValues = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee3(clientOptions, identifiers, customAttributes) {
          var _this5 = this;
          var initializeValues,
            clientOptionsWithDefaults,
            startTime,
            _args3 = arguments;
          return regenerator.wrap(function _callee3$(_context3) {
            while (1) switch (_context3.prev = _context3.next) {
              case 0:
                initializeValues = _args3.length > 3 && _args3[3] !== undefined ? _args3[3] : {};
                clientOptionsWithDefaults = getOptionsWithDefaults(clientOptions);
                if (!this.initPromise) {
                  _context3.next = 5;
                  break;
                }
                if (!shallowEquals(clientOptionsWithDefaults, this.initOptions)) {
                  // eslint-disable-next-line no-console
                  console.warn('Feature Gates client already initialized with different options. New options were not applied.');
                }
                return _context3.abrupt("return", this.initPromise);
              case 5:
                // This makes sure the new Statsig client behaves like the old when bootstrap data is
                // passed, and `has_updates` isn't specified (which happens a lot in product integration tests).
                if (!Object.prototype.hasOwnProperty.call(initializeValues, 'has_updates')) {
                  initializeValues['has_updates'] = true;
                }
                startTime = performance.now();
                this.initOptions = clientOptionsWithDefaults;
                this.initPromise = this.initFromValues(clientOptionsWithDefaults, identifiers, customAttributes, initializeValues).then(function () {
                  _this5.initCompleted = true;
                  _this5.initWithDefaults = true;
                }).finally(function () {
                  var endTime = performance.now();
                  var totalTime = endTime - startTime;
                  _this5.fireClientEvent(startTime, totalTime, 'initializeFromValues', _this5.initCompleted);
                });
                return _context3.abrupt("return", this.initPromise);
              case 10:
              case "end":
                return _context3.stop();
            }
          }, _callee3, this);
        }));
        function initializeFromValues(_x8, _x9, _x10) {
          return _initializeFromValues.apply(this, arguments);
        }
        return initializeFromValues;
      }()
    }, {
      key: "assertInitialized",
      value: function assertInitialized(statsigClient) {
        if (!statsigClient) {
          throw new Error('Client must be initialized before using this method');
        }
      }

      /**
       * This method updates the user using a network call to fetch the new set of values.
       * @param fetchOptions {FetcherOptions}
       * @param identifiers {Identifiers}
       * @param customAttributes {CustomAttributes}
       */
    }, {
      key: "updateUser",
      value: function () {
        var _updateUser = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee4(fetchOptions, identifiers, customAttributes) {
          var fetchOptionsWithDefaults, initializeValuesProducer;
          return regenerator.wrap(function _callee4$(_context4) {
            while (1) switch (_context4.prev = _context4.next) {
              case 0:
                this.assertInitialized(this.statsigClient);
                fetchOptionsWithDefaults = getOptionsWithDefaults(fetchOptions);
                initializeValuesProducer = function initializeValuesProducer() {
                  return Fetcher.fetchExperimentValues(fetchOptionsWithDefaults, identifiers, customAttributes).then(function (_ref2) {
                    var experimentValues = _ref2.experimentValues,
                      customAttributes = _ref2.customAttributes;
                    return {
                      experimentValues: experimentValues,
                      customAttributesFromFetch: customAttributes
                    };
                  });
                };
                _context4.next = 5;
                return this.updateUserUsingInitializeValuesProducer(initializeValuesProducer, identifiers, customAttributes);
              case 5:
              case "end":
                return _context4.stop();
            }
          }, _callee4, this);
        }));
        function updateUser(_x11, _x12, _x13) {
          return _updateUser.apply(this, arguments);
        }
        return updateUser;
      }()
      /**
       * This method updates the user using the provider given on initialisation to get the new set of
       * values.
       * @param identifiers {Identifiers}
       * @param customAttributes {CustomAttributes}
       */
    }, {
      key: "updateUserWithProvider",
      value: function () {
        var _updateUserWithProvider = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee5(identifiers, customAttributes) {
          var _this6 = this;
          return regenerator.wrap(function _callee5$(_context5) {
            while (1) switch (_context5.prev = _context5.next) {
              case 0:
                this.assertInitialized(this.statsigClient);
                if (this.provider) {
                  _context5.next = 3;
                  break;
                }
                throw new Error('Cannot update user using provider as the client was not initialised with a provider');
              case 3:
                _context5.next = 5;
                return this.provider.setProfile(this.initOptions, identifiers, customAttributes);
              case 5:
                _context5.next = 7;
                return this.updateUserUsingInitializeValuesProducer(function () {
                  return _this6.provider.getExperimentValues();
                }, identifiers, customAttributes);
              case 7:
              case "end":
                return _context5.stop();
            }
          }, _callee5, this);
        }));
        function updateUserWithProvider(_x14, _x15) {
          return _updateUserWithProvider.apply(this, arguments);
        }
        return updateUserWithProvider;
      }()
      /**
       * This method updates the user given a new set of bootstrap values obtained from one of the
       * server-side SDKs.
       *
       * @param identifiers {Identifiers}
       * @param customAttributes {CustomAttributes}
       * @param initializeValues {Record<string,unknown>}
       */
    }, {
      key: "updateUserWithValues",
      value: function () {
        var _updateUserWithValues = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee6(identifiers, customAttributes) {
          var initializeValues,
            initializeValuesProducer,
            _args6 = arguments;
          return regenerator.wrap(function _callee6$(_context6) {
            while (1) switch (_context6.prev = _context6.next) {
              case 0:
                initializeValues = _args6.length > 2 && _args6[2] !== undefined ? _args6[2] : {};
                this.assertInitialized(this.statsigClient);
                initializeValuesProducer = function initializeValuesProducer() {
                  return Promise.resolve({
                    experimentValues: initializeValues,
                    customAttributesFromFetch: customAttributes
                  });
                };
                _context6.next = 5;
                return this.updateUserUsingInitializeValuesProducer(initializeValuesProducer, identifiers, customAttributes);
              case 5:
              case "end":
                return _context6.stop();
            }
          }, _callee6, this);
        }));
        function updateUserWithValues(_x16, _x17) {
          return _updateUserWithValues.apply(this, arguments);
        }
        return updateUserWithValues;
      }()
    }, {
      key: "initializeCalled",
      value: function initializeCalled() {
        return this.initPromise != null;
      }
    }, {
      key: "initializeCompleted",
      value: function initializeCompleted() {
        return this.initCompleted;
      }

      /**
       * Returns the value for a feature gate. Returns false if there are errors.
       * @param {string} gateName - The name of the feature gate.
       * @param {Object} options
       * @param {boolean} options.fireGateExposure
       *        Whether or not to fire the exposure event for the gate. Defaults to true.
       *        To log an exposure event manually at a later time, use {@link Client.manuallyLogGateExposure}
       *        (see [Statsig docs about manually logging exposures](https://docs.statsig.com/client/jsClientSDK#manual-exposures-)).
       */
    }, {
      key: "checkGate",
      value: function checkGate(gateName) {
        var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
        try {
          this.assertInitialized(this.statsigClient);
          var _options$fireGateExpo = options.fireGateExposure,
            fireGateExposure = _options$fireGateExpo === void 0 ? true : _options$fireGateExpo;
          return this.statsigClient.checkGate(gateName, {
            disableExposureLog: !fireGateExposure
          });
        } catch (error) {
          // Log the first occurrence of the error
          if (!this.hasCheckGateErrorOccurred) {
            // eslint-disable-next-line no-console
            console.warn({
              msg: 'An error has occurred checking the feature gate. Only the first occurrence of this error is logged.',
              gateName: gateName,
              error: error
            });
            this.hasCheckGateErrorOccurred = true;
          }
          return false;
        }
      }
    }, {
      key: "isGateExist",
      value: function isGateExist(gateName) {
        try {
          this.assertInitialized(this.statsigClient);
          var gate = this.statsigClient.getFeatureGate(gateName, {
            disableExposureLog: true
          });
          return !gate.details.reason.includes('Unrecognized');
        } catch (error) {
          // eslint-disable-next-line no-console
          console.error("Error occurred when trying to check FeatureGate: ".concat(error));
          // in case of error report true to avoid false positives.
          return true;
        }
      }
    }, {
      key: "isExperimentExist",
      value: function isExperimentExist(experimentName) {
        try {
          this.assertInitialized(this.statsigClient);
          var config = this.statsigClient.getExperiment(experimentName, {
            disableExposureLog: true
          });
          return !config.details.reason.includes('Unrecognized');
        } catch (error) {
          // eslint-disable-next-line no-console
          console.error("Error occurred when trying to check Experiment: ".concat(error));
          // in case of error report true to avoid false positives.
          return true;
        }
      }

      /**
       * Manually log a gate exposure (see [Statsig docs about manually logging exposures](https://docs.statsig.com/client/jsClientSDK#manual-exposures-)).
       * This is useful if you have evaluated a gate earlier via {@link Client.checkGate} where
       * <code>options.fireGateExposure</code> is false.
       * @param gateName
       */
    }, {
      key: "manuallyLogGateExposure",
      value: function manuallyLogGateExposure(gateName) {
        this.assertInitialized(this.statsigClient);
        // This is the approach recommended in the docs
        // https://docs.statsig.com/client/javascript-sdk/#manual-exposures-
        this.statsigClient.checkGate(gateName);
      }

      /**
       * Returns the entire config for a given experiment.
       *
       * @param {string} experimentName - The name of the experiment
       * @param {Object} options
       * @param {boolean} options.fireExperimentExposure - Whether or not to fire the exposure event
       * for the experiment. Defaults to true. To log an exposure event manually at a later time, use
       * {@link Client.manuallyLogExperimentExposure} (see [Statsig docs about manually logging exposures](https://docs.statsig.com/client/jsClientSDK#manual-exposures-)).
       * @returns The config for an experiment
       * @example
       * ```ts
       * const experimentConfig = client.getExperiment('example-experiment-name');
       * const backgroundColor: string = experimentConfig.get('backgroundColor', 'yellow');
       * ```
       */
    }, {
      key: "getExperiment",
      value: function getExperiment(experimentName) {
        var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
        try {
          this.assertInitialized(this.statsigClient);
          var _options$fireExperime = options.fireExperimentExposure,
            fireExperimentExposure = _options$fireExperime === void 0 ? true : _options$fireExperime;
          return DynamicConfig.fromExperiment(this.statsigClient.getExperiment(experimentName, {
            disableExposureLog: !fireExperimentExposure
          }));
        } catch (error) {
          // Log the first occurrence of the error
          if (!this.hasGetExperimentErrorOccurred) {
            // eslint-disable-next-line no-console
            console.warn({
              msg: 'An error has occurred getting the experiment. Only the first occurrence of this error is logged.',
              experimentName: experimentName,
              error: error
            });
            this.hasGetExperimentErrorOccurred = true;
          }

          // Return a default value
          return new DynamicConfig(experimentName, {}, '', {
            time: Date.now(),
            reason: EvaluationReason.Error
          });
        }
      }

      /**
       * Returns the value of a given parameter in an experiment config.
       *
       * @template T
       * @param {string} experimentName - The name of the experiment
       * @param {string} parameterName - The name of the parameter to fetch from the experiment config
       * @param {T} defaultValue - The value to serve if the experiment or parameter do not exist, or
       * if the returned value does not match the expected type.
       * @param {Object} options
       * @param {boolean} options.fireExperimentExposure - Whether or not to fire the exposure event
       * for the experiment. Defaults to true. To log an exposure event manually at a later time, use
       * {@link Client.manuallyLogExperimentExposure} (see [Statsig docs about manually logging exposures](https://docs.statsig.com/client/jsClientSDK#manual-exposures-))
       * @param {function} options.typeGuard - A function that asserts that the return value has the
       * expected type. If this function returns false, then the default value will be returned
       * instead. This can be set to protect your code from unexpected values being set remotely. By
       * default, this will be done by asserting that the default value and value are the same primitive
       * type.
       * @returns The value of the parameter if the experiment and parameter both exist, otherwise the
       * default value.
       * @example
       ``` ts
       type ValidColor = 'blue' | 'red' | 'yellow';
       type ValidColorTypeCheck = (value: unknown) => value is ValidColor;
      	 const isValidColor: ValidColorTypeCheck =
      		(value: unknown) => typeof value === 'string' && ['blue', 'red', 'yellow'].includes(value);
      	 const buttonColor: ValidColor = client.getExperimentValue(
      		'example-experiment-name',
      		'backgroundColor',
      		'yellow',
      		{
      				typeGuard: isValidColor
      		}
       );
       ```
      */
    }, {
      key: "getExperimentValue",
      value: function getExperimentValue(experimentName, parameterName, defaultValue) {
        var options = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
        var experiment = this.getExperiment(experimentName, options);
        try {
          var typeGuard = options.typeGuard;
          return experiment.get(parameterName, defaultValue, typeGuard);
        } catch (error) {
          // Log the first occurrence of the error
          if (!this.hasGetExperimentValueErrorOccurred) {
            // eslint-disable-next-line no-console
            console.warn({
              msg: 'An error has occurred getting the experiment value. Only the first occurrence of this error is logged.',
              experimentName: experimentName,
              defaultValue: defaultValue,
              options: options,
              error: error
            });
            this.hasGetExperimentValueErrorOccurred = true;
          }
          return defaultValue;
        }
      }

      /**
       * Manually log an experiment exposure (see [Statsig docs about manually logging exposures](https://docs.statsig.com/client/jsClientSDK#manual-exposures-)).
       * This is useful if you have evaluated an experiment earlier via {@link Client.getExperimentValue} or
       * {@link Client.getExperiment} where <code>options.fireExperimentExposure</code> is false.
       * @param experimentName
       */
    }, {
      key: "manuallyLogExperimentExposure",
      value: function manuallyLogExperimentExposure(experimentName) {
        this.assertInitialized(this.statsigClient);
        // This is the approach recommended in the docs
        // https://docs.statsig.com/client/javascript-sdk/#manual-exposures-
        this.statsigClient.getExperiment(experimentName);
      }

      /**
       * Manually log a layer exposure (see [Statsig docs about manually logging exposures](https://docs.statsig.com/client/jsClientSDK#manual-exposures-)).
       * This is useful if you have evaluated a layer earlier via {@link Client.getLayerValue} where <code>options.fireExperimentExposure</code> is false.
       * @param layerName
       * @param parameterName
       */
    }, {
      key: "manuallyLogLayerExposure",
      value: function manuallyLogLayerExposure(layerName, parameterName) {
        var _this$statsigClient$g;
        this.assertInitialized(this.statsigClient);
        // This is the approach recommended in the docs
        // https://docs.statsig.com/client/javascript-sdk/#manual-exposures-
        (_this$statsigClient$g = this.statsigClient.getLayer(layerName)) === null || _this$statsigClient$g === void 0 || _this$statsigClient$g.get(parameterName);
      }
    }, {
      key: "shutdownStatsig",
      value: function shutdownStatsig() {
        this.assertInitialized(this.statsigClient);
        this.statsigClient.shutdown();
      }

      /**
       * Adds a new override for the given gate.
       *
       * This method is additive, meaning you can call it multiple times with different gate names to
       * build your full set of overrides.
       *
       * Overrides are persisted to the `STATSIG_OVERRIDES` key in localStorage, so they
       * will continue to affect every client that is initialized on the same domain after this method
       * is called. If you are using this API for testing purposes, you should call
       * {@link Client.clearGateOverride} after your tests are completed to remove this
       * localStorage entry.
       *
       * @param {string} gateName
       * @param {boolean} value
       */
    }, {
      key: "overrideGate",
      value: function overrideGate(gateName, value) {
        this.overrideAdapter.overrideGate(gateName, value);
        // Trigger a reset of the memoized gate value
        if (this.user) {
          var _this$statsigClient;
          (_this$statsigClient = this.statsigClient) === null || _this$statsigClient === void 0 || _this$statsigClient.updateUserSync(this.user, {
            disableBackgroundCacheRefresh: true
          });
        }
        this.statsigValuesUpdated();
      }

      /**
       * Removes any overrides that have been set for the given gate.
       */
    }, {
      key: "clearGateOverride",
      value: function clearGateOverride(gateName) {
        this.overrideAdapter.removeGateOverride(gateName);
        this.statsigValuesUpdated();
      }

      /**
       * Adds a new override for the given config (or experiment).
       *
       * This method is additive, meaning you can call it multiple times with different experiment
       * names to build your full set of overrides.
       *
       * Overrides are persisted to the `STATSIG_OVERRIDES` key in localStorage, so they
       * will continue to affect every client that is initialized on the same domain after this method
       * is called. If you are using this API for testing purposes, you should call
       * {@link Client.clearConfigOverride} after your tests are completed to remove this
       * localStorage entry.
       *
       * @param {string} experimentName
       * @param {object} values
       */
    }, {
      key: "overrideConfig",
      value: function overrideConfig(experimentName, values) {
        this.overrideAdapter.overrideDynamicConfig(experimentName, values);
        this.statsigValuesUpdated();
      }

      /**
       * Removes any overrides that have been set for the given experiment.
       * @param {string} experimentName
       */
    }, {
      key: "clearConfigOverride",
      value: function clearConfigOverride(experimentName) {
        this.overrideAdapter.removeDynamicConfigOverride(experimentName);
        this.statsigValuesUpdated();
      }

      /**
       * Set overrides for gates, experiments and layers in batch.
       *
       * Note that these overrides are **not** additive and will completely replace any that have been
       * added via prior calls to {@link Client.overrideConfig} or
       * {@link Client.overrideGate}.
       *
       * Overrides are persisted to the `STATSIG_OVERRIDES` key in localStorage, so they
       * will continue to affect every client that is initialized on the same domain after this method
       * is called. If you are using this API for testing purposes, you should call
       * {@link Client.clearAllOverrides} after your tests are completed to remove this
       * localStorage entry.
       */
    }, {
      key: "setOverrides",
      value: function setOverrides(overrides) {
        this.overrideAdapter.setOverrides(overrides);
        this.statsigValuesUpdated();
      }

      /**
       * @returns The current overrides for gates, configs (including experiments) and layers.
       */
    }, {
      key: "getOverrides",
      value: function getOverrides() {
        return this.overrideAdapter.getOverrides();
      }

      /**
       * Clears overrides for all gates, configs (including experiments) and layers.
       */
    }, {
      key: "clearAllOverrides",
      value: function clearAllOverrides() {
        this.overrideAdapter.removeAllOverrides();
        this.statsigValuesUpdated();
      }

      /**
       * Returns whether the given identifiers and customAttributes align with the current
       * set that is being used by the client.
       *
       * If this method returns false, then the {@link Client.updateUser},
       * {@link Client.updateUserWithValues} or {@link Client.updateUserWithProvider}
       * methods can be used to re-align these values.
       *
       * @param identifiers
       * @param customAttributes
       * @returns a flag indicating whether the clients current configuration aligns with the given values
       */
    }, {
      key: "isCurrentUser",
      value: function isCurrentUser(identifiers, customAttributes) {
        return shallowEquals(this.currentIdentifiers, identifiers) && shallowEquals(this.currentAttributes, customAttributes);
      }

      /**
       * Subscribe to updates where the given callback will be called with the current checkGate value
       * @param gateName
       * @param callback
       * @param options
       * @returns off function to unsubscribe from updates
       */
    }, {
      key: "onGateUpdated",
      value: function onGateUpdated(gateName, callback) {
        var _this7 = this;
        var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
        var wrapCallback = function wrapCallback(value) {
          var _options$fireGateExpo2 = options.fireGateExposure,
            fireGateExposure = _options$fireGateExpo2 === void 0 ? true : _options$fireGateExpo2;
          if (fireGateExposure) {
            _this7.manuallyLogGateExposure(gateName);
          }
          try {
            callback(value);
          } catch (error) {
            // eslint-disable-next-line no-console
            console.warn("Error calling callback for gate ".concat(gateName, " with value ").concat(value), error);
          }
        };
        return this.subscriptions.onGateUpdated(gateName, wrapCallback, this.checkGate.bind(this), options);
      }

      /**
       * Subscribe to updates where the given callback will be called with the current experiment value
       * @param experimentName
       * @param parameterName
       * @param defaultValue
       * @param callback
       * @param options
       * @returns off function to unsubscribe from updates
       */
    }, {
      key: "onExperimentValueUpdated",
      value: function onExperimentValueUpdated(experimentName, parameterName, defaultValue, callback) {
        var _this8 = this;
        var options = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : {};
        var wrapCallback = function wrapCallback(value) {
          var _options$fireExperime2 = options.fireExperimentExposure,
            fireExperimentExposure = _options$fireExperime2 === void 0 ? true : _options$fireExperime2;
          if (fireExperimentExposure) {
            _this8.manuallyLogExperimentExposure(experimentName);
          }
          try {
            callback(value);
          } catch (error) {
            // eslint-disable-next-line no-console
            console.warn("Error calling callback for experiment ".concat(experimentName, " with value ").concat(value), error);
          }
        };
        return this.subscriptions.onExperimentValueUpdated(experimentName, parameterName, defaultValue, wrapCallback, this.getExperimentValue.bind(this), options);
      }

      /**
       * Subscribe so on any update the callback will be called.
       * NOTE: The callback will be called whenever the values are updated even if the values have not
       * changed.
       * @param callback
       * @returns off function to unsubscribe from updates
       */
    }, {
      key: "onAnyUpdated",
      value: function onAnyUpdated(callback) {
        return this.subscriptions.onAnyUpdated(callback);
      }

      /**
       * This method initializes the client using a network call to fetch the bootstrap values for the
       * given user.
       *
       * @param clientOptions
       * @param identifiers
       * @param customAttributes
       * @private
       */
    }, {
      key: "init",
      value: function () {
        var _init = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee7(clientOptions, identifiers, customAttributes) {
          var fromValuesClientOptions, experimentValues, customAttributesFromResult, clientSdkKeyPromise, experimentValuesPromise, _yield$Promise$all, _yield$Promise$all2, experimentValuesResult;
          return regenerator.wrap(function _callee7$(_context7) {
            while (1) switch (_context7.prev = _context7.next) {
              case 0:
                fromValuesClientOptions = _objectSpread$2({}, clientOptions);
                _context7.prev = 1;
                // If client sdk key fetch fails, an error would be thrown and handled instead of waiting for
                // the experiment values request to be settled, and it will fall back to use default values.
                clientSdkKeyPromise = Fetcher.fetchClientSdk(clientOptions).then(function (value) {
                  return fromValuesClientOptions.sdkKey = value.clientSdkKey;
                });
                experimentValuesPromise = Fetcher.fetchExperimentValues(clientOptions, identifiers, customAttributes); // Only wait for the experiment values request to finish and try to initialise the client
                // with experiment values if both requests are successful. Else an error would be thrown and
                // handled by the catch
                _context7.next = 6;
                return Promise.all([clientSdkKeyPromise, experimentValuesPromise]);
              case 6:
                _yield$Promise$all = _context7.sent;
                _yield$Promise$all2 = _slicedToArray(_yield$Promise$all, 2);
                experimentValuesResult = _yield$Promise$all2[1];
                experimentValues = experimentValuesResult.experimentValues;
                customAttributesFromResult = experimentValuesResult.customAttributes;
                _context7.next = 20;
                break;
              case 13:
                _context7.prev = 13;
                _context7.t0 = _context7["catch"](1);
                if (_context7.t0 instanceof Error) {
                  // eslint-disable-next-line no-console
                  console.error("Error occurred when trying to fetch the Feature Gates client values, error: ".concat(_context7.t0 === null || _context7.t0 === void 0 ? void 0 : _context7.t0.message));
                }
                // eslint-disable-next-line no-console
                console.warn("Initialising Statsig client without values");
                _context7.next = 19;
                return this.initFromValues(fromValuesClientOptions, identifiers, customAttributes);
              case 19:
                throw _context7.t0;
              case 20:
                return _context7.abrupt("return", this.initFromValues(fromValuesClientOptions, identifiers, customAttributesFromResult, experimentValues));
              case 21:
              case "end":
                return _context7.stop();
            }
          }, _callee7, this, [[1, 13]]);
        }));
        function init(_x18, _x19, _x20) {
          return _init.apply(this, arguments);
        }
        return init;
      }()
    }, {
      key: "initWithProvider",
      value: function () {
        var _initWithProvider = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee8(baseClientOptions, provider, identifiers, customAttributes) {
          var fromValuesClientOptions, experimentValues, customAttributesFromResult, clientSdkKeyPromise, experimentValuesPromise, _yield$Promise$all3, _yield$Promise$all4, experimentValuesResult;
          return regenerator.wrap(function _callee8$(_context8) {
            while (1) switch (_context8.prev = _context8.next) {
              case 0:
                fromValuesClientOptions = _objectSpread$2(_objectSpread$2({}, baseClientOptions), {}, {
                  disableCurrentPageLogging: true
                });
                _context8.prev = 1;
                _context8.next = 4;
                return provider.setProfile(baseClientOptions, identifiers, customAttributes);
              case 4:
                // If client sdk key fetch fails, an error would be thrown and handled instead of waiting for
                // the experiment values request to be settled, and it will fall back to use default values.
                clientSdkKeyPromise = provider.getClientSdkKey().then(function (value) {
                  return fromValuesClientOptions.sdkKey = value;
                });
                experimentValuesPromise = provider.getExperimentValues(); // Only wait for the experiment values request to finish and try to initialise the client
                // with experiment values if both requests are successful. Else an error would be thrown and
                // handled by the catch
                _context8.next = 8;
                return Promise.all([clientSdkKeyPromise, experimentValuesPromise]);
              case 8:
                _yield$Promise$all3 = _context8.sent;
                _yield$Promise$all4 = _slicedToArray(_yield$Promise$all3, 2);
                experimentValuesResult = _yield$Promise$all4[1];
                experimentValues = experimentValuesResult.experimentValues;
                customAttributesFromResult = experimentValuesResult.customAttributesFromFetch;
                _context8.next = 22;
                break;
              case 15:
                _context8.prev = 15;
                _context8.t0 = _context8["catch"](1);
                if (_context8.t0 instanceof Error) {
                  // eslint-disable-next-line no-console
                  console.error("Error occurred when trying to fetch the Feature Gates client values, error: ".concat(_context8.t0 === null || _context8.t0 === void 0 ? void 0 : _context8.t0.message));
                }
                // eslint-disable-next-line no-console
                console.warn("Initialising Statsig client without values");
                _context8.next = 21;
                return this.initFromValues(fromValuesClientOptions, identifiers, customAttributes);
              case 21:
                throw _context8.t0;
              case 22:
                return _context8.abrupt("return", this.initFromValues(fromValuesClientOptions, identifiers, customAttributesFromResult, experimentValues));
              case 23:
              case "end":
                return _context8.stop();
            }
          }, _callee8, this, [[1, 15]]);
        }));
        function initWithProvider(_x21, _x22, _x23, _x24) {
          return _initWithProvider.apply(this, arguments);
        }
        return initWithProvider;
      }()
      /**
       * This method initializes the client using a set of boostrap values obtained from one of the
       * server-side SDKs.
       *
       * @param clientOptions
       * @param identifiers
       * @param customAttributes
       * @param initializeValues
       * @private
       */
    }, {
      key: "initFromValues",
      value: function () {
        var _initFromValues = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee9(clientOptions, identifiers, customAttributes) {
          var _newClientOptions$net;
          var initializeValues,
            newClientOptions,
            sdkKey,
            environment,
            restClientOptions,
            statsigOptions,
            _args9 = arguments;
          return regenerator.wrap(function _callee9$(_context9) {
            while (1) switch (_context9.prev = _context9.next) {
              case 0:
                initializeValues = _args9.length > 3 && _args9[3] !== undefined ? _args9[3] : {};
                this.overrideAdapter.initFromStoredOverrides();
                this.currentIdentifiers = identifiers;
                this.currentAttributes = customAttributes;
                newClientOptions = migrateInitializationOptions(clientOptions);
                if (!newClientOptions.sdkKey) {
                  newClientOptions.sdkKey = DEFAULT_CLIENT_KEY;
                }
                if (!((_newClientOptions$net = newClientOptions.networkConfig) !== null && _newClientOptions$net !== void 0 && _newClientOptions$net.logEventUrl)) {
                  newClientOptions.networkConfig = _objectSpread$2(_objectSpread$2({}, newClientOptions.networkConfig), {}, {
                    logEventUrl: DEFAULT_EVENT_LOGGING_API
                  });
                }
                if (newClientOptions.perimeter === PerimeterType.FEDRAMP_MODERATE) {
                  // disable all logging in FedRAMP to prevent egress of sensitive data
                  newClientOptions.disableLogging = true;
                }
                sdkKey = newClientOptions.sdkKey, environment = newClientOptions.environment, newClientOptions.updateUserCompletionCallback, newClientOptions.perimeter, restClientOptions = _objectWithoutProperties(newClientOptions, _excluded);
                this.sdkKey = sdkKey;
                this.user = toStatsigUser(identifiers, customAttributes, this.sdkKey);
                statsigOptions = _objectSpread$2(_objectSpread$2({}, restClientOptions), {}, {
                  environment: {
                    tier: environment
                  },
                  includeCurrentPageUrlWithEvents: false,
                  dataAdapter: this.dataAdapter,
                  overrideAdapter: this.overrideAdapter
                });
                _context9.prev = 12;
                this.statsigClient = new jsClient.StatsigClient(sdkKey, this.user, statsigOptions);
                this.dataAdapter.setBootstrapData(initializeValues);
                _context9.next = 17;
                return this.statsigClient.initializeAsync();
              case 17:
                _context9.next = 29;
                break;
              case 19:
                _context9.prev = 19;
                _context9.t0 = _context9["catch"](12);
                if (_context9.t0 instanceof Error) {
                  // eslint-disable-next-line no-console
                  console.error("Error occurred when trying to initialise the Statsig client, error: ".concat(_context9.t0 === null || _context9.t0 === void 0 ? void 0 : _context9.t0.message));
                }
                // eslint-disable-next-line no-console
                console.warn("Initialising Statsig client with default sdk key and without values");
                this.statsigClient = new jsClient.StatsigClient(DEFAULT_CLIENT_KEY, this.user, statsigOptions);
                this.dataAdapter.setBootstrapData();
                _context9.next = 27;
                return this.statsigClient.initializeAsync();
              case 27:
                this.initWithDefaults = true;
                throw _context9.t0;
              case 29:
              case "end":
                return _context9.stop();
            }
          }, _callee9, this, [[12, 19]]);
        }));
        function initFromValues(_x25, _x26, _x27) {
          return _initFromValues.apply(this, arguments);
        }
        return initFromValues;
      }()
      /**
       * This method updates the user for this client with the bootstrap values returned from a given
       * Promise.
       * It uses the customAttributes from fetching experiment values to update the Statsig user but
       * uses the customAttributes from given input to check if the user has changed.
       *
       * @param {Identifiers} identifiers
       * @param {CustomAttributes} customAttributes
       * @param {Promise<InitializeValues>} getInitializeValues
       * @private
       */
    }, {
      key: "updateUserUsingInitializeValuesProducer",
      value: function () {
        var _updateUserUsingInitializeValuesProducer = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee11(getInitializeValues, identifiers, customAttributes) {
          var _this9 = this;
          var originalInitPromise, initializeValuesPromise, updateUserPromise;
          return regenerator.wrap(function _callee11$(_context11) {
            while (1) switch (_context11.prev = _context11.next) {
              case 0:
                this.assertInitialized(this.statsigClient);
                if (this.initPromise) {
                  _context11.next = 3;
                  break;
                }
                throw new Error('The client must be initialized before you can update the user.');
              case 3:
                if (!this.isCurrentUser(identifiers, customAttributes)) {
                  _context11.next = 5;
                  break;
                }
                return _context11.abrupt("return", this.initPromise);
              case 5:
                // Wait for the current initialize/update to finish
                originalInitPromise = this.initPromise;
                _context11.prev = 6;
                _context11.next = 9;
                return this.initPromise;
              case 9:
                _context11.next = 13;
                break;
              case 11:
                _context11.prev = 11;
                _context11.t0 = _context11["catch"](6);
              case 13:
                initializeValuesPromise = getInitializeValues();
                updateUserPromise = this.updateStatsigClientUser(initializeValuesPromise, identifiers, customAttributes); // We replace the init promise here since we are essentially re-initializing the client at this
                // point. Any subsequent calls to await client.initialize() or client.updateUser()
                // will now also await this user update.
                this.initPromise = updateUserPromise.catch(/*#__PURE__*/_asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee10() {
                  return regenerator.wrap(function _callee10$(_context10) {
                    while (1) switch (_context10.prev = _context10.next) {
                      case 0:
                        // If the update failed then it changed nothing, so revert back to the original promise.
                        _this9.initPromise = originalInitPromise;

                        // Set the user profile again to revert back to the current user
                        if (!_this9.provider) {
                          _context10.next = 4;
                          break;
                        }
                        _context10.next = 4;
                        return _this9.provider.setProfile(_this9.initOptions, _this9.currentIdentifiers, _this9.currentAttributes);
                      case 4:
                      case "end":
                        return _context10.stop();
                    }
                  }, _callee10);
                })));
                return _context11.abrupt("return", updateUserPromise);
              case 17:
              case "end":
                return _context11.stop();
            }
          }, _callee11, this, [[6, 11]]);
        }));
        function updateUserUsingInitializeValuesProducer(_x28, _x29, _x30) {
          return _updateUserUsingInitializeValuesProducer.apply(this, arguments);
        }
        return updateUserUsingInitializeValuesProducer;
      }()
      /**
       * This method updates the user on the nested Statsig client
       *
       * @param identifiers
       * @param customAttributes
       * @param initializeValuesPromise
       * @private
       */
    }, {
      key: "updateStatsigClientUser",
      value: function () {
        var _updateStatsigClientUser = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee12(initializeValuesPromise, identifiers, customAttributes) {
          var _this$initOptions, _this$initOptions$upd;
          var initializeValues, user, _updateUserCompletion, _ref4, errMsg, success, errorMessage;
          return regenerator.wrap(function _callee12$(_context12) {
            while (1) switch (_context12.prev = _context12.next) {
              case 0:
                this.assertInitialized(this.statsigClient);
                _context12.prev = 1;
                _context12.next = 4;
                return initializeValuesPromise;
              case 4:
                initializeValues = _context12.sent;
                user = toStatsigUser(identifiers, initializeValues.customAttributesFromFetch, this.sdkKey);
                _context12.next = 13;
                break;
              case 8:
                _context12.prev = 8;
                _context12.t0 = _context12["catch"](1);
                // Make sure the updateUserCompletionCallback is called for any errors in our custom code.
                // This is not necessary for the updateUserWithValues call, because the Statsig client will
                // already invoke the callback itself.
                errMsg = _context12.t0 instanceof Error ? _context12.t0.message : JSON.stringify(_context12.t0);
                (_updateUserCompletion = (_ref4 = this.initOptions).updateUserCompletionCallback) === null || _updateUserCompletion === void 0 || _updateUserCompletion.call(_ref4, false, errMsg);
                throw _context12.t0;
              case 13:
                success = true;
                errorMessage = null;
                _context12.prev = 15;
                this.dataAdapter.setBootstrapData(initializeValues.experimentValues);
                this.user = user;
                _context12.next = 20;
                return this.statsigClient.updateUserAsync(this.user);
              case 20:
                _context12.next = 26;
                break;
              case 22:
                _context12.prev = 22;
                _context12.t1 = _context12["catch"](15);
                success = false;
                errorMessage = String(_context12.t1);
              case 26:
                (_this$initOptions = this.initOptions) === null || _this$initOptions === void 0 || (_this$initOptions$upd = _this$initOptions.updateUserCompletionCallback) === null || _this$initOptions$upd === void 0 || _this$initOptions$upd.call(_this$initOptions, success, errorMessage);
                if (!success) {
                  _context12.next = 33;
                  break;
                }
                this.currentIdentifiers = identifiers;
                this.currentAttributes = customAttributes;
                this.subscriptions.anyUpdated();
                _context12.next = 34;
                break;
              case 33:
                throw new Error('Failed to update user. An unexpected error occured.');
              case 34:
              case "end":
                return _context12.stop();
            }
          }, _callee12, this, [[1, 8], [15, 22]]);
        }));
        function updateStatsigClientUser(_x31, _x32, _x33) {
          return _updateStatsigClientUser.apply(this, arguments);
        }
        return updateStatsigClientUser;
      }()
    }, {
      key: "getPackageVersion",
      value:
      /**
       * @returns string version of the current package in semver style.
       */
      function getPackageVersion() {
        return CLIENT_VERSION;
      }

      /**
       * Returns a specified layer otherwise returns an empty layer as a default value if the layer doesn't exist.
       *
       * @param {string} layerName - The name of the layer
       * @param {Object} options
       * @param {boolean} options.fireLayerExposure - Whether or not to fire the exposure event for the
       * layer. Defaults to true. To log an exposure event manually at a later time, use
       * {@link Client.manuallyLogLayerExposure} (see [Statsig docs about manually logging exposures](https://docs.statsig.com/client/jsClientSDK#manual-exposures-)).
       * @returns A layer
       * @example
       * ```ts
       * const layer = client.getLayer('example-layer-name');
       * const exampletitle: string = layer.get("title", "Welcome to Statsig!");
       * ```
       */
    }, {
      key: "getLayer",
      value: function getLayer(/** The name of the layer */
      layerName) {
        var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
        try {
          this.assertInitialized(this.statsigClient);
          var _options$fireLayerExp = options.fireLayerExposure,
            fireLayerExposure = _options$fireLayerExp === void 0 ? true : _options$fireLayerExp;
          return Layer.fromLayer(this.statsigClient.getLayer(layerName, {
            disableExposureLog: !fireLayerExposure
          }));
        } catch (error) {
          // Log the first occurrence of the error
          if (!this.hasGetLayerErrorOccurred) {
            // eslint-disable-next-line no-console
            console.warn({
              msg: 'An error has occurred getting the layer. Only the first occurrence of this error is logged.',
              layerName: layerName,
              error: error
            });
            this.hasGetLayerErrorOccurred = true;
          }

          // Return a default value
          return Layer.fromLayer(jsClient._makeLayer(layerName, {
            reason: 'Error'
          }, null));
        }
      }

      /**
       * Returns the value of a given parameter in a layer config.
       *
       * @template T
       * @param {string} layerName - The name of the layer
       * @param {string} parameterName - The name of the parameter to fetch from the layer config
       * @param {T} defaultValue - The value to serve if the layer or parameter do not exist, or if the
       * returned value does not match the expected type.
       * @param {Object} options
       * @param {boolean} options.fireLayerExposure - Whether or not to fire the exposure event for the
       * layer. Defaults to true. To log an exposure event manually at a later time, use
       * {@link Client.manuallyLogLayerExposure} (see [Statsig docs about manually logging exposures](https://docs.statsig.com/client/jsClientSDK#manual-exposures-))
       * @param {function} options.typeGuard - A function that asserts that the return value has the expected type. If this function returns false, then the default value will be returned instead. This can be set to protect your code from unexpected values being set remotely. By default, this will be done by asserting that the default value and value are the same primitive type.
       * @returns The value of the parameter if the layer and parameter both exist, otherwise the default value.
       * @example
       * ``` ts
       * type ValidColor = 'blue' | 'red' | 'yellow';
       * type ValidColorTypeCheck = (value: unknown) => value is ValidColor;
       *
       * const isValidColor: ValidColorTypeCheck =
       *    (value: unknown) => typeof value === 'string' && ['blue', 'red', 'yellow'].includes(value);
       *
       * const buttonColor: ValidColor = client.getLayerValue(
       *    'example-layer-name',
       *    'backgroundColor',
       *    'yellow',
       *    {
       *        typeGuard: isValidColor
       *    }
       * );
       * ```
       */
    }, {
      key: "getLayerValue",
      value: function getLayerValue(layerName, parameterName, defaultValue) {
        var options = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
        var layer = this.getLayer(layerName, options);
        try {
          var typeGuard = options.typeGuard;
          return layer.get(parameterName, defaultValue, typeGuard);
        } catch (error) {
          // Log the first occurrence of the error
          if (!this.hasGetLayerValueErrorOccurred) {
            // eslint-disable-next-line no-console
            console.warn({
              msg: 'An error has occurred getting the layer value. Only the first occurrence of this error is logged.',
              layerName: layerName,
              defaultValue: defaultValue,
              options: options,
              error: error
            });
            this.hasGetLayerValueErrorOccurred = true;
          }
          return defaultValue;
        }
      }
    }]);
  }();

  var _FeatureGates;
  /**
   * Access the FeatureGates object via the default export.
   * ```ts
   * import FeatureGates from '@atlaskit/feature-gate-js-client';
   * ```
   */
  var FeatureGates = /*#__PURE__*/function () {
    function FeatureGates() {
      _classCallCheck(this, FeatureGates);
    }
    return _createClass(FeatureGates, null, [{
      key: "isGateExists",
      value: function isGateExists(gateName) {
        return this.client.isGateExist(gateName);
      }
    }, {
      key: "isExperimentExists",
      value: function isExperimentExists(experimentName) {
        return this.client.isExperimentExist(experimentName);
      }
    }]);
  }();
  _FeatureGates = FeatureGates;
  _defineProperty(FeatureGates, "client", new Client());
  _defineProperty(FeatureGates, "hasCheckGateErrorOccurred", false);
  _defineProperty(FeatureGates, "hasGetExperimentValueErrorOccurred", false);
  _defineProperty(FeatureGates, "checkGate", function (gateName, options) {
    try {
      // Check if the CRITERION override mechanism is available
      if (typeof window !== 'undefined' && window.__CRITERION__ && typeof window.__CRITERION__.getFeatureFlagOverride === 'function') {
        // Attempt to retrieve an override value for the feature gate
        var overrideValue = window.__CRITERION__.getFeatureFlagOverride(gateName);
        // If an override value is found, return it immediately
        if (overrideValue !== undefined) {
          return overrideValue;
        }
      }
    } catch (error) {
      // Log the first occurrence of the error
      if (!_FeatureGates.hasCheckGateErrorOccurred) {
        // eslint-disable-next-line no-console
        console.warn({
          msg: 'An error has occurred checking the feature gate from criterion override. Only the first occurrence of this error is logged.',
          gateName: gateName,
          error: error
        });
        _FeatureGates.hasCheckGateErrorOccurred = true;
      }
    }

    // Proceed with the main logic if no override is found
    return _FeatureGates.client.checkGate(gateName, options);
  });
  _defineProperty(FeatureGates, "getExperimentValue", function (experimentName, parameterName, defaultValue, options) {
    try {
      // Check if the CRITERION override mechanism is available
      if (typeof window !== 'undefined' && window.__CRITERION__ && typeof window.__CRITERION__.getExperimentValueOverride === 'function') {
        var overrideValue = window.__CRITERION__.getExperimentValueOverride(experimentName, parameterName);
        if (overrideValue !== undefined && overrideValue !== null) {
          return overrideValue;
        }
      }
    } catch (error) {
      // Log the first occurrence of the error
      if (!_FeatureGates.hasGetExperimentValueErrorOccurred) {
        // eslint-disable-next-line no-console
        console.warn({
          msg: 'An error has occurred getting the experiment value from criterion override. Only the first occurrence of this error is logged.',
          experimentName: experimentName,
          defaultValue: defaultValue,
          options: options,
          error: error
        });
        _FeatureGates.hasGetExperimentValueErrorOccurred = true;
      }
      return defaultValue;
    }

    // Proceed with the main logic if no override is found
    return _FeatureGates.client.getExperimentValue(experimentName, parameterName, defaultValue, options);
  });
  _defineProperty(FeatureGates, "initializeCalled", _FeatureGates.client.initializeCalled.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "initializeCompleted", _FeatureGates.client.initializeCompleted.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "initialize", _FeatureGates.client.initialize.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "initializeWithProvider", _FeatureGates.client.initializeWithProvider.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "initializeFromValues", _FeatureGates.client.initializeFromValues.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "manuallyLogGateExposure", _FeatureGates.client.manuallyLogGateExposure.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "getExperiment", _FeatureGates.client.getExperiment.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "manuallyLogExperimentExposure", _FeatureGates.client.manuallyLogExperimentExposure.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "manuallyLogLayerExposure", _FeatureGates.client.manuallyLogLayerExposure.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "shutdownStatsig", _FeatureGates.client.shutdownStatsig.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "overrideGate", _FeatureGates.client.overrideGate.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "clearGateOverride", _FeatureGates.client.clearGateOverride.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "overrideConfig", _FeatureGates.client.overrideConfig.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "clearConfigOverride", _FeatureGates.client.clearConfigOverride.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "setOverrides", _FeatureGates.client.setOverrides.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "getOverrides", _FeatureGates.client.getOverrides.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "clearAllOverrides", _FeatureGates.client.clearAllOverrides.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "isCurrentUser", _FeatureGates.client.isCurrentUser.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "onGateUpdated", _FeatureGates.client.onGateUpdated.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "onExperimentValueUpdated", _FeatureGates.client.onExperimentValueUpdated.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "onAnyUpdated", _FeatureGates.client.onAnyUpdated.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "updateUser", _FeatureGates.client.updateUser.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "updateUserWithProvider", _FeatureGates.client.updateUserWithProvider.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "updateUserWithValues", _FeatureGates.client.updateUserWithValues.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "getPackageVersion", _FeatureGates.client.getPackageVersion.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "getLayer", _FeatureGates.client.getLayer.bind(_FeatureGates.client));
  _defineProperty(FeatureGates, "getLayerValue", _FeatureGates.client.getLayerValue.bind(_FeatureGates.client));
  var boundFGJS = FeatureGates;

  // This makes it possible to get a reference to the FeatureGates client at runtime.
  // This is important for overriding values in Cypress tests, as there needs to be a
  // way to get the exact instance for a window in order to mock some of its methods.
  if (typeof window !== 'undefined') {
    if (window.__FEATUREGATES_JS__ === undefined) {
      window.__FEATUREGATES_JS__ = FeatureGates;
    } else {
      var _boundFGJS, _boundFGJS$getPackage;
      boundFGJS = window.__FEATUREGATES_JS__;
      var boundVersion = ((_boundFGJS = boundFGJS) === null || _boundFGJS === void 0 || (_boundFGJS$getPackage = _boundFGJS.getPackageVersion) === null || _boundFGJS$getPackage === void 0 ? void 0 : _boundFGJS$getPackage.call(_boundFGJS)) || '4.10.0 or earlier';
      if (boundVersion !== CLIENT_VERSION) {
        var message = "Multiple versions of FeatureGateClients found on the current page.\n      The currently bound version is ".concat(boundVersion, " when module version ").concat(CLIENT_VERSION, " was loading.");
        // eslint-disable-next-line no-console
        console.warn(message);
      }
    }
  }

  /**
   * @property {FeatureGates} FeatureGate default export
   */
  var FeatureGates$1 = boundFGJS;

  var _process, _process2;
  // We can't rely on NODE_ENV === 'test' if its value is already configured by the consumer to some other value, so better to use JEST_WORKER_ID
  // https://jestjs.io/docs/environment-variables#jest_worker_id
  var TESTS_MODE = (globalThis === null || globalThis === void 0 || (_process = globalThis.process) === null || _process === void 0 || (_process = _process.env) === null || _process === void 0 ? void 0 : _process.JEST_WORKER_ID) !== undefined;
  var DEBUG_MODE = !TESTS_MODE && (globalThis === null || globalThis === void 0 || (_process2 = globalThis.process) === null || _process2 === void 0 || (_process2 = _process2.env) === null || _process2 === void 0 ? void 0 : _process2.NODE_ENV) !== 'production';
  var debug = function debug() {
    var _console;
    if (!DEBUG_MODE) {
      return;
    }

    // eslint-disable-next-line no-console
    (_console = console).debug.apply(_console, arguments);
  };

  var pkgName = '@atlaskit/platform-feature-flags';
  var PFF_GLOBAL_KEY = '__PLATFORM_FEATURE_FLAGS__';
  var hasProcessEnv = typeof process !== 'undefined' && typeof process.env !== 'undefined';

  // FF global overrides can be configured by test runners or Storybook
  var ENV_ENABLE_PLATFORM_FF = hasProcessEnv ?
  // Use global "process" variable and process.env['FLAG_NAME'] syntax, so it can be replaced by webpack DefinePlugin
  undefined === 'true' : false;

  // STORYBOOK_ENABLE_PLATFORM_FF is included as storybook only allows env vars prefixed with STORYBOOK
  // https://github.com/storybookjs/storybook/issues/12270

  var ENV_STORYBOOK_ENABLE_PLATFORM_FF = hasProcessEnv ?
  // Use global "process" variable and process.env['FLAG_NAME'] syntax, so it can be replaced by webpack DefinePlugin
  undefined === 'true' : false;
  var ENABLE_GLOBAL_PLATFORM_FF_OVERRIDE = ENV_ENABLE_PLATFORM_FF || ENV_STORYBOOK_ENABLE_PLATFORM_FF;
  var DEFAULT_PFF_GLOBAL = {
    booleanResolver: undefined
  };
  var globalVar = typeof window !== 'undefined' ? window : globalThis;
  globalVar[PFF_GLOBAL_KEY] = globalVar[PFF_GLOBAL_KEY] || DEFAULT_PFF_GLOBAL;
  function resolveBooleanFlag(flagKey) {
    if (ENABLE_GLOBAL_PLATFORM_FF_OVERRIDE) {
      debug('[%s]: The feature flags were enabled while running tests. The flag "%s" will be always enabled.', pkgName, flagKey);
      return true;
    }
    try {
      var _globalVar$PFF_GLOBAL, _globalVar$PFF_GLOBAL2, _globalVar$PFF_GLOBAL3;
      // booleanResolver will be empty for products like Trello, Elevate, Recruit etc.
      // Currently only Confluence, Jira and Bitbucket has set it.
      if (((_globalVar$PFF_GLOBAL = globalVar[PFF_GLOBAL_KEY]) === null || _globalVar$PFF_GLOBAL === void 0 ? void 0 : _globalVar$PFF_GLOBAL.booleanResolver) === undefined || ((_globalVar$PFF_GLOBAL2 = globalVar[PFF_GLOBAL_KEY]) === null || _globalVar$PFF_GLOBAL2 === void 0 ? void 0 : _globalVar$PFF_GLOBAL2.booleanResolver) === null) {
        // eslint-disable-next-line @atlaskit/platform/use-recommended-utils
        return FeatureGates$1.checkGate(flagKey);
      }
      var result = (_globalVar$PFF_GLOBAL3 = globalVar[PFF_GLOBAL_KEY]) === null || _globalVar$PFF_GLOBAL3 === void 0 ? void 0 : _globalVar$PFF_GLOBAL3.booleanResolver(flagKey);
      if (typeof result !== 'boolean') {
        // eslint-disable-next-line no-console
        console.warn("".concat(flagKey, " resolved to a non-boolean value, returning false for safety"));
        return false;
      }
      return result;
    } catch (e) {
      return false;
    }
  }

  /**
   * Returns the value of a feature flag. If the flag does not resolve, it returns the "false" as a default value.
   *
   * @param name
   */
  function fg(name) {
    return resolveBooleanFlag(name);
  }

  /**
   * Theme ids: The value that will be mounted to the DOM as a data attr
   * For example: `data-theme="light:light dark:dark spacing:spacing"
   *
   * These ids must be kebab case
   */
  var themeIds = ['light-increased-contrast', 'light', 'light-future', 'light-brand-refresh', 'dark', 'dark-future', 'dark-increased-contrast', 'dark-brand-refresh', 'legacy-light', 'legacy-dark', 'spacing', 'shape', 'typography-adg3', 'typography-modernized', 'typography-refreshed'];

  /**
   * Theme to use a base. This will create the theme as
   * an extension with all token values marked as optional
   * to allow tokens to be overridden as required.
   */

  /**
   * Palettes: The set of base tokens a given theme may be populated with.
   * For example: legacy light & dark themes use the "legacyPalette" containing colors from our
   * previous color set.
   */

  /**
   * ThemeConfig: the source of truth for all theme meta-data.
   * This object should be used whenever interfacing with themes.
   */

  var themeConfig = {
    'atlassian-light': {
      id: 'light',
      displayName: 'Light Theme',
      palette: 'defaultPalette',
      attributes: {
        type: 'color',
        mode: 'light'
      }
    },
    'atlassian-light-future': {
      id: 'light-future',
      displayName: 'Future Light Theme',
      palette: 'defaultPalette',
      attributes: {
        type: 'color',
        mode: 'light'
      },
      override: 'light'
    },
    'atlassian-light-increased-contrast': {
      id: 'light-increased-contrast',
      displayName: 'Light Theme (increased contrast)',
      palette: 'defaultPalette',
      attributes: {
        type: 'color',
        mode: 'light'
      },
      extends: 'light',
      increasesContrastFor: 'light'
    },
    'atlassian-dark': {
      id: 'dark',
      displayName: 'Dark Theme',
      palette: 'defaultPalette',
      attributes: {
        type: 'color',
        mode: 'dark'
      }
    },
    'atlassian-dark-future': {
      id: 'dark-future',
      displayName: 'Future Dark Theme',
      palette: 'defaultPalette',
      attributes: {
        type: 'color',
        mode: 'dark'
      },
      override: 'light'
    },
    'atlassian-dark-increased-contrast': {
      id: 'dark-increased-contrast',
      displayName: 'Dark Theme (increased contrast)',
      palette: 'defaultPalette',
      attributes: {
        type: 'color',
        mode: 'dark'
      },
      extends: 'dark',
      increasesContrastFor: 'dark'
    },
    'atlassian-legacy-light': {
      id: 'legacy-light',
      displayName: 'Light Theme (legacy)',
      palette: 'legacyPalette',
      attributes: {
        type: 'color',
        mode: 'light'
      }
    },
    'atlassian-legacy-dark': {
      id: 'legacy-dark',
      displayName: 'Dark Theme (legacy)',
      palette: 'legacyPalette',
      attributes: {
        type: 'color',
        mode: 'dark'
      }
    },
    'atlassian-spacing': {
      id: 'spacing',
      displayName: 'Atlassian Spacing',
      palette: 'spacingScale',
      attributes: {
        type: 'spacing'
      }
    },
    'atlassian-typography-adg3': {
      id: 'typography-adg3',
      displayName: 'ADG3 Typography',
      palette: 'typographyPalette',
      attributes: {
        type: 'typography'
      }
    },
    'atlassian-shape': {
      id: 'shape',
      displayName: 'Atlassian Shape',
      palette: 'shapePalette',
      attributes: {
        type: 'shape'
      }
    },
    'atlassian-typography-modernized': {
      id: 'typography-modernized',
      displayName: 'Atlassian Typography (Modernized)',
      palette: 'typographyPalette',
      attributes: {
        type: 'typography'
      }
    },
    'atlassian-typography-refreshed': {
      id: 'typography-refreshed',
      displayName: 'Atlassian Typography (Refreshed)',
      palette: 'typographyPalette',
      attributes: {
        type: 'typography'
      }
    },
    'atlassian-light-brand-refresh': {
      id: 'light-brand-refresh',
      displayName: 'Light theme (Brand refresh)',
      palette: 'brandRefreshPalette',
      attributes: {
        type: 'color',
        mode: 'light'
      }
    },
    'atlassian-dark-brand-refresh': {
      id: 'dark-brand-refresh',
      displayName: 'Dark theme (Brand refresh)',
      palette: 'brandRefreshPalette',
      attributes: {
        type: 'color',
        mode: 'dark'
      }
    }
  };

  /**
   * ThemeOptionsSchema: additional configuration options used to customize Atlassian's themes
   */

  /**
   * ThemeState: the standard representation of an app's current theme and preferences
   */

  /**
   * Can't evaluate typography feature flags at the module level,
   * it will always resolve to false when server side rendered or when flags are loaded async.
   */

  /**
   * themeStateDefaults: the default values for ThemeState used by theming utilities
   */
  var themeStateDefaults = {
    colorMode: 'auto',
    contrastMode: 'auto',
    dark: 'dark',
    light: 'light',
    shape: undefined,
    spacing: 'spacing',
    typography: function typography() {
      if (fg('platform-default-typography-modernized')) {
        return 'typography-modernized';
      }
      return undefined;
    },
    UNSAFE_themeOptions: undefined
  };

  /**
   * THIS FILE WAS CREATED VIA CODEGEN DO NOT MODIFY {@see http://go/af-codegen}
   * @codegen <<SignedSource::02412877f3dbfda4dbaee7a917f24b6c>>
   * @codegenCommand yarn build tokens
   */
  var tokens = {
    'color.text': '--ds-text',
    'color.text.accent.lime': '--ds-text-accent-lime',
    'color.text.accent.lime.bolder': '--ds-text-accent-lime-bolder',
    'color.text.accent.red': '--ds-text-accent-red',
    'color.text.accent.red.bolder': '--ds-text-accent-red-bolder',
    'color.text.accent.orange': '--ds-text-accent-orange',
    'color.text.accent.orange.bolder': '--ds-text-accent-orange-bolder',
    'color.text.accent.yellow': '--ds-text-accent-yellow',
    'color.text.accent.yellow.bolder': '--ds-text-accent-yellow-bolder',
    'color.text.accent.green': '--ds-text-accent-green',
    'color.text.accent.green.bolder': '--ds-text-accent-green-bolder',
    'color.text.accent.teal': '--ds-text-accent-teal',
    'color.text.accent.teal.bolder': '--ds-text-accent-teal-bolder',
    'color.text.accent.blue': '--ds-text-accent-blue',
    'color.text.accent.blue.bolder': '--ds-text-accent-blue-bolder',
    'color.text.accent.purple': '--ds-text-accent-purple',
    'color.text.accent.purple.bolder': '--ds-text-accent-purple-bolder',
    'color.text.accent.magenta': '--ds-text-accent-magenta',
    'color.text.accent.magenta.bolder': '--ds-text-accent-magenta-bolder',
    'color.text.accent.gray': '--ds-text-accent-gray',
    'color.text.accent.gray.bolder': '--ds-text-accent-gray-bolder',
    'color.text.disabled': '--ds-text-disabled',
    'color.text.inverse': '--ds-text-inverse',
    'color.text.selected': '--ds-text-selected',
    'color.text.brand': '--ds-text-brand',
    'color.text.danger': '--ds-text-danger',
    'color.text.warning': '--ds-text-warning',
    'color.text.warning.inverse': '--ds-text-warning-inverse',
    'color.text.success': '--ds-text-success',
    'color.text.discovery': '--ds-text-discovery',
    'color.text.information': '--ds-text-information',
    'color.text.subtlest': '--ds-text-subtlest',
    'color.text.subtle': '--ds-text-subtle',
    'color.link': '--ds-link',
    'color.link.pressed': '--ds-link-pressed',
    'color.link.visited': '--ds-link-visited',
    'color.link.visited.pressed': '--ds-link-visited-pressed',
    'color.icon': '--ds-icon',
    'color.icon.accent.lime': '--ds-icon-accent-lime',
    'color.icon.accent.red': '--ds-icon-accent-red',
    'color.icon.accent.orange': '--ds-icon-accent-orange',
    'color.icon.accent.yellow': '--ds-icon-accent-yellow',
    'color.icon.accent.green': '--ds-icon-accent-green',
    'color.icon.accent.teal': '--ds-icon-accent-teal',
    'color.icon.accent.blue': '--ds-icon-accent-blue',
    'color.icon.accent.purple': '--ds-icon-accent-purple',
    'color.icon.accent.magenta': '--ds-icon-accent-magenta',
    'color.icon.accent.gray': '--ds-icon-accent-gray',
    'color.icon.disabled': '--ds-icon-disabled',
    'color.icon.inverse': '--ds-icon-inverse',
    'color.icon.selected': '--ds-icon-selected',
    'color.icon.brand': '--ds-icon-brand',
    'color.icon.danger': '--ds-icon-danger',
    'color.icon.warning': '--ds-icon-warning',
    'color.icon.warning.inverse': '--ds-icon-warning-inverse',
    'color.icon.success': '--ds-icon-success',
    'color.icon.discovery': '--ds-icon-discovery',
    'color.icon.information': '--ds-icon-information',
    'color.icon.subtlest': '--ds-icon-subtlest',
    'color.icon.subtle': '--ds-icon-subtle',
    'color.border': '--ds-border',
    'color.border.accent.lime': '--ds-border-accent-lime',
    'color.border.accent.red': '--ds-border-accent-red',
    'color.border.accent.orange': '--ds-border-accent-orange',
    'color.border.accent.yellow': '--ds-border-accent-yellow',
    'color.border.accent.green': '--ds-border-accent-green',
    'color.border.accent.teal': '--ds-border-accent-teal',
    'color.border.accent.blue': '--ds-border-accent-blue',
    'color.border.accent.purple': '--ds-border-accent-purple',
    'color.border.accent.magenta': '--ds-border-accent-magenta',
    'color.border.accent.gray': '--ds-border-accent-gray',
    'color.border.disabled': '--ds-border-disabled',
    'color.border.focused': '--ds-border-focused',
    'color.border.input': '--ds-border-input',
    'color.border.inverse': '--ds-border-inverse',
    'color.border.selected': '--ds-border-selected',
    'color.border.brand': '--ds-border-brand',
    'color.border.danger': '--ds-border-danger',
    'color.border.warning': '--ds-border-warning',
    'color.border.success': '--ds-border-success',
    'color.border.discovery': '--ds-border-discovery',
    'color.border.information': '--ds-border-information',
    'color.border.bold': '--ds-border-bold',
    'color.background.accent.lime.subtlest': '--ds-background-accent-lime-subtlest',
    'color.background.accent.lime.subtlest.hovered': '--ds-background-accent-lime-subtlest-hovered',
    'color.background.accent.lime.subtlest.pressed': '--ds-background-accent-lime-subtlest-pressed',
    'color.background.accent.lime.subtler': '--ds-background-accent-lime-subtler',
    'color.background.accent.lime.subtler.hovered': '--ds-background-accent-lime-subtler-hovered',
    'color.background.accent.lime.subtler.pressed': '--ds-background-accent-lime-subtler-pressed',
    'color.background.accent.lime.subtle': '--ds-background-accent-lime-subtle',
    'color.background.accent.lime.subtle.hovered': '--ds-background-accent-lime-subtle-hovered',
    'color.background.accent.lime.subtle.pressed': '--ds-background-accent-lime-subtle-pressed',
    'color.background.accent.lime.bolder': '--ds-background-accent-lime-bolder',
    'color.background.accent.lime.bolder.hovered': '--ds-background-accent-lime-bolder-hovered',
    'color.background.accent.lime.bolder.pressed': '--ds-background-accent-lime-bolder-pressed',
    'color.background.accent.red.subtlest': '--ds-background-accent-red-subtlest',
    'color.background.accent.red.subtlest.hovered': '--ds-background-accent-red-subtlest-hovered',
    'color.background.accent.red.subtlest.pressed': '--ds-background-accent-red-subtlest-pressed',
    'color.background.accent.red.subtler': '--ds-background-accent-red-subtler',
    'color.background.accent.red.subtler.hovered': '--ds-background-accent-red-subtler-hovered',
    'color.background.accent.red.subtler.pressed': '--ds-background-accent-red-subtler-pressed',
    'color.background.accent.red.subtle': '--ds-background-accent-red-subtle',
    'color.background.accent.red.subtle.hovered': '--ds-background-accent-red-subtle-hovered',
    'color.background.accent.red.subtle.pressed': '--ds-background-accent-red-subtle-pressed',
    'color.background.accent.red.bolder': '--ds-background-accent-red-bolder',
    'color.background.accent.red.bolder.hovered': '--ds-background-accent-red-bolder-hovered',
    'color.background.accent.red.bolder.pressed': '--ds-background-accent-red-bolder-pressed',
    'color.background.accent.orange.subtlest': '--ds-background-accent-orange-subtlest',
    'color.background.accent.orange.subtlest.hovered': '--ds-background-accent-orange-subtlest-hovered',
    'color.background.accent.orange.subtlest.pressed': '--ds-background-accent-orange-subtlest-pressed',
    'color.background.accent.orange.subtler': '--ds-background-accent-orange-subtler',
    'color.background.accent.orange.subtler.hovered': '--ds-background-accent-orange-subtler-hovered',
    'color.background.accent.orange.subtler.pressed': '--ds-background-accent-orange-subtler-pressed',
    'color.background.accent.orange.subtle': '--ds-background-accent-orange-subtle',
    'color.background.accent.orange.subtle.hovered': '--ds-background-accent-orange-subtle-hovered',
    'color.background.accent.orange.subtle.pressed': '--ds-background-accent-orange-subtle-pressed',
    'color.background.accent.orange.bolder': '--ds-background-accent-orange-bolder',
    'color.background.accent.orange.bolder.hovered': '--ds-background-accent-orange-bolder-hovered',
    'color.background.accent.orange.bolder.pressed': '--ds-background-accent-orange-bolder-pressed',
    'color.background.accent.yellow.subtlest': '--ds-background-accent-yellow-subtlest',
    'color.background.accent.yellow.subtlest.hovered': '--ds-background-accent-yellow-subtlest-hovered',
    'color.background.accent.yellow.subtlest.pressed': '--ds-background-accent-yellow-subtlest-pressed',
    'color.background.accent.yellow.subtler': '--ds-background-accent-yellow-subtler',
    'color.background.accent.yellow.subtler.hovered': '--ds-background-accent-yellow-subtler-hovered',
    'color.background.accent.yellow.subtler.pressed': '--ds-background-accent-yellow-subtler-pressed',
    'color.background.accent.yellow.subtle': '--ds-background-accent-yellow-subtle',
    'color.background.accent.yellow.subtle.hovered': '--ds-background-accent-yellow-subtle-hovered',
    'color.background.accent.yellow.subtle.pressed': '--ds-background-accent-yellow-subtle-pressed',
    'color.background.accent.yellow.bolder': '--ds-background-accent-yellow-bolder',
    'color.background.accent.yellow.bolder.hovered': '--ds-background-accent-yellow-bolder-hovered',
    'color.background.accent.yellow.bolder.pressed': '--ds-background-accent-yellow-bolder-pressed',
    'color.background.accent.green.subtlest': '--ds-background-accent-green-subtlest',
    'color.background.accent.green.subtlest.hovered': '--ds-background-accent-green-subtlest-hovered',
    'color.background.accent.green.subtlest.pressed': '--ds-background-accent-green-subtlest-pressed',
    'color.background.accent.green.subtler': '--ds-background-accent-green-subtler',
    'color.background.accent.green.subtler.hovered': '--ds-background-accent-green-subtler-hovered',
    'color.background.accent.green.subtler.pressed': '--ds-background-accent-green-subtler-pressed',
    'color.background.accent.green.subtle': '--ds-background-accent-green-subtle',
    'color.background.accent.green.subtle.hovered': '--ds-background-accent-green-subtle-hovered',
    'color.background.accent.green.subtle.pressed': '--ds-background-accent-green-subtle-pressed',
    'color.background.accent.green.bolder': '--ds-background-accent-green-bolder',
    'color.background.accent.green.bolder.hovered': '--ds-background-accent-green-bolder-hovered',
    'color.background.accent.green.bolder.pressed': '--ds-background-accent-green-bolder-pressed',
    'color.background.accent.teal.subtlest': '--ds-background-accent-teal-subtlest',
    'color.background.accent.teal.subtlest.hovered': '--ds-background-accent-teal-subtlest-hovered',
    'color.background.accent.teal.subtlest.pressed': '--ds-background-accent-teal-subtlest-pressed',
    'color.background.accent.teal.subtler': '--ds-background-accent-teal-subtler',
    'color.background.accent.teal.subtler.hovered': '--ds-background-accent-teal-subtler-hovered',
    'color.background.accent.teal.subtler.pressed': '--ds-background-accent-teal-subtler-pressed',
    'color.background.accent.teal.subtle': '--ds-background-accent-teal-subtle',
    'color.background.accent.teal.subtle.hovered': '--ds-background-accent-teal-subtle-hovered',
    'color.background.accent.teal.subtle.pressed': '--ds-background-accent-teal-subtle-pressed',
    'color.background.accent.teal.bolder': '--ds-background-accent-teal-bolder',
    'color.background.accent.teal.bolder.hovered': '--ds-background-accent-teal-bolder-hovered',
    'color.background.accent.teal.bolder.pressed': '--ds-background-accent-teal-bolder-pressed',
    'color.background.accent.blue.subtlest': '--ds-background-accent-blue-subtlest',
    'color.background.accent.blue.subtlest.hovered': '--ds-background-accent-blue-subtlest-hovered',
    'color.background.accent.blue.subtlest.pressed': '--ds-background-accent-blue-subtlest-pressed',
    'color.background.accent.blue.subtler': '--ds-background-accent-blue-subtler',
    'color.background.accent.blue.subtler.hovered': '--ds-background-accent-blue-subtler-hovered',
    'color.background.accent.blue.subtler.pressed': '--ds-background-accent-blue-subtler-pressed',
    'color.background.accent.blue.subtle': '--ds-background-accent-blue-subtle',
    'color.background.accent.blue.subtle.hovered': '--ds-background-accent-blue-subtle-hovered',
    'color.background.accent.blue.subtle.pressed': '--ds-background-accent-blue-subtle-pressed',
    'color.background.accent.blue.bolder': '--ds-background-accent-blue-bolder',
    'color.background.accent.blue.bolder.hovered': '--ds-background-accent-blue-bolder-hovered',
    'color.background.accent.blue.bolder.pressed': '--ds-background-accent-blue-bolder-pressed',
    'color.background.accent.purple.subtlest': '--ds-background-accent-purple-subtlest',
    'color.background.accent.purple.subtlest.hovered': '--ds-background-accent-purple-subtlest-hovered',
    'color.background.accent.purple.subtlest.pressed': '--ds-background-accent-purple-subtlest-pressed',
    'color.background.accent.purple.subtler': '--ds-background-accent-purple-subtler',
    'color.background.accent.purple.subtler.hovered': '--ds-background-accent-purple-subtler-hovered',
    'color.background.accent.purple.subtler.pressed': '--ds-background-accent-purple-subtler-pressed',
    'color.background.accent.purple.subtle': '--ds-background-accent-purple-subtle',
    'color.background.accent.purple.subtle.hovered': '--ds-background-accent-purple-subtle-hovered',
    'color.background.accent.purple.subtle.pressed': '--ds-background-accent-purple-subtle-pressed',
    'color.background.accent.purple.bolder': '--ds-background-accent-purple-bolder',
    'color.background.accent.purple.bolder.hovered': '--ds-background-accent-purple-bolder-hovered',
    'color.background.accent.purple.bolder.pressed': '--ds-background-accent-purple-bolder-pressed',
    'color.background.accent.magenta.subtlest': '--ds-background-accent-magenta-subtlest',
    'color.background.accent.magenta.subtlest.hovered': '--ds-background-accent-magenta-subtlest-hovered',
    'color.background.accent.magenta.subtlest.pressed': '--ds-background-accent-magenta-subtlest-pressed',
    'color.background.accent.magenta.subtler': '--ds-background-accent-magenta-subtler',
    'color.background.accent.magenta.subtler.hovered': '--ds-background-accent-magenta-subtler-hovered',
    'color.background.accent.magenta.subtler.pressed': '--ds-background-accent-magenta-subtler-pressed',
    'color.background.accent.magenta.subtle': '--ds-background-accent-magenta-subtle',
    'color.background.accent.magenta.subtle.hovered': '--ds-background-accent-magenta-subtle-hovered',
    'color.background.accent.magenta.subtle.pressed': '--ds-background-accent-magenta-subtle-pressed',
    'color.background.accent.magenta.bolder': '--ds-background-accent-magenta-bolder',
    'color.background.accent.magenta.bolder.hovered': '--ds-background-accent-magenta-bolder-hovered',
    'color.background.accent.magenta.bolder.pressed': '--ds-background-accent-magenta-bolder-pressed',
    'color.background.accent.gray.subtlest': '--ds-background-accent-gray-subtlest',
    'color.background.accent.gray.subtlest.hovered': '--ds-background-accent-gray-subtlest-hovered',
    'color.background.accent.gray.subtlest.pressed': '--ds-background-accent-gray-subtlest-pressed',
    'color.background.accent.gray.subtler': '--ds-background-accent-gray-subtler',
    'color.background.accent.gray.subtler.hovered': '--ds-background-accent-gray-subtler-hovered',
    'color.background.accent.gray.subtler.pressed': '--ds-background-accent-gray-subtler-pressed',
    'color.background.accent.gray.subtle': '--ds-background-accent-gray-subtle',
    'color.background.accent.gray.subtle.hovered': '--ds-background-accent-gray-subtle-hovered',
    'color.background.accent.gray.subtle.pressed': '--ds-background-accent-gray-subtle-pressed',
    'color.background.accent.gray.bolder': '--ds-background-accent-gray-bolder',
    'color.background.accent.gray.bolder.hovered': '--ds-background-accent-gray-bolder-hovered',
    'color.background.accent.gray.bolder.pressed': '--ds-background-accent-gray-bolder-pressed',
    'color.background.disabled': '--ds-background-disabled',
    'color.background.input': '--ds-background-input',
    'color.background.input.hovered': '--ds-background-input-hovered',
    'color.background.input.pressed': '--ds-background-input-pressed',
    'color.background.inverse.subtle': '--ds-background-inverse-subtle',
    'color.background.inverse.subtle.hovered': '--ds-background-inverse-subtle-hovered',
    'color.background.inverse.subtle.pressed': '--ds-background-inverse-subtle-pressed',
    'color.background.neutral': '--ds-background-neutral',
    'color.background.neutral.hovered': '--ds-background-neutral-hovered',
    'color.background.neutral.pressed': '--ds-background-neutral-pressed',
    'color.background.neutral.subtle': '--ds-background-neutral-subtle',
    'color.background.neutral.subtle.hovered': '--ds-background-neutral-subtle-hovered',
    'color.background.neutral.subtle.pressed': '--ds-background-neutral-subtle-pressed',
    'color.background.neutral.bold': '--ds-background-neutral-bold',
    'color.background.neutral.bold.hovered': '--ds-background-neutral-bold-hovered',
    'color.background.neutral.bold.pressed': '--ds-background-neutral-bold-pressed',
    'color.background.selected': '--ds-background-selected',
    'color.background.selected.hovered': '--ds-background-selected-hovered',
    'color.background.selected.pressed': '--ds-background-selected-pressed',
    'color.background.selected.bold': '--ds-background-selected-bold',
    'color.background.selected.bold.hovered': '--ds-background-selected-bold-hovered',
    'color.background.selected.bold.pressed': '--ds-background-selected-bold-pressed',
    'color.background.brand.subtlest': '--ds-background-brand-subtlest',
    'color.background.brand.subtlest.hovered': '--ds-background-brand-subtlest-hovered',
    'color.background.brand.subtlest.pressed': '--ds-background-brand-subtlest-pressed',
    'color.background.brand.bold': '--ds-background-brand-bold',
    'color.background.brand.bold.hovered': '--ds-background-brand-bold-hovered',
    'color.background.brand.bold.pressed': '--ds-background-brand-bold-pressed',
    'color.background.brand.boldest': '--ds-background-brand-boldest',
    'color.background.brand.boldest.hovered': '--ds-background-brand-boldest-hovered',
    'color.background.brand.boldest.pressed': '--ds-background-brand-boldest-pressed',
    'color.background.danger': '--ds-background-danger',
    'color.background.danger.hovered': '--ds-background-danger-hovered',
    'color.background.danger.pressed': '--ds-background-danger-pressed',
    'color.background.danger.bold': '--ds-background-danger-bold',
    'color.background.danger.bold.hovered': '--ds-background-danger-bold-hovered',
    'color.background.danger.bold.pressed': '--ds-background-danger-bold-pressed',
    'color.background.warning': '--ds-background-warning',
    'color.background.warning.hovered': '--ds-background-warning-hovered',
    'color.background.warning.pressed': '--ds-background-warning-pressed',
    'color.background.warning.bold': '--ds-background-warning-bold',
    'color.background.warning.bold.hovered': '--ds-background-warning-bold-hovered',
    'color.background.warning.bold.pressed': '--ds-background-warning-bold-pressed',
    'color.background.success': '--ds-background-success',
    'color.background.success.hovered': '--ds-background-success-hovered',
    'color.background.success.pressed': '--ds-background-success-pressed',
    'color.background.success.bold': '--ds-background-success-bold',
    'color.background.success.bold.hovered': '--ds-background-success-bold-hovered',
    'color.background.success.bold.pressed': '--ds-background-success-bold-pressed',
    'color.background.discovery': '--ds-background-discovery',
    'color.background.discovery.hovered': '--ds-background-discovery-hovered',
    'color.background.discovery.pressed': '--ds-background-discovery-pressed',
    'color.background.discovery.bold': '--ds-background-discovery-bold',
    'color.background.discovery.bold.hovered': '--ds-background-discovery-bold-hovered',
    'color.background.discovery.bold.pressed': '--ds-background-discovery-bold-pressed',
    'color.background.information': '--ds-background-information',
    'color.background.information.hovered': '--ds-background-information-hovered',
    'color.background.information.pressed': '--ds-background-information-pressed',
    'color.background.information.bold': '--ds-background-information-bold',
    'color.background.information.bold.hovered': '--ds-background-information-bold-hovered',
    'color.background.information.bold.pressed': '--ds-background-information-bold-pressed',
    'color.blanket': '--ds-blanket',
    'color.blanket.selected': '--ds-blanket-selected',
    'color.blanket.danger': '--ds-blanket-danger',
    'color.interaction.hovered': '--ds-interaction-hovered',
    'color.interaction.pressed': '--ds-interaction-pressed',
    'color.skeleton': '--ds-skeleton',
    'color.skeleton.subtle': '--ds-skeleton-subtle',
    'color.chart.categorical.1': '--ds-chart-categorical-1',
    'color.chart.categorical.1.hovered': '--ds-chart-categorical-1-hovered',
    'color.chart.categorical.2': '--ds-chart-categorical-2',
    'color.chart.categorical.2.hovered': '--ds-chart-categorical-2-hovered',
    'color.chart.categorical.3': '--ds-chart-categorical-3',
    'color.chart.categorical.3.hovered': '--ds-chart-categorical-3-hovered',
    'color.chart.categorical.4': '--ds-chart-categorical-4',
    'color.chart.categorical.4.hovered': '--ds-chart-categorical-4-hovered',
    'color.chart.categorical.5': '--ds-chart-categorical-5',
    'color.chart.categorical.5.hovered': '--ds-chart-categorical-5-hovered',
    'color.chart.categorical.6': '--ds-chart-categorical-6',
    'color.chart.categorical.6.hovered': '--ds-chart-categorical-6-hovered',
    'color.chart.categorical.7': '--ds-chart-categorical-7',
    'color.chart.categorical.7.hovered': '--ds-chart-categorical-7-hovered',
    'color.chart.categorical.8': '--ds-chart-categorical-8',
    'color.chart.categorical.8.hovered': '--ds-chart-categorical-8-hovered',
    'color.chart.lime.bold': '--ds-chart-lime-bold',
    'color.chart.lime.bold.hovered': '--ds-chart-lime-bold-hovered',
    'color.chart.lime.bolder': '--ds-chart-lime-bolder',
    'color.chart.lime.bolder.hovered': '--ds-chart-lime-bolder-hovered',
    'color.chart.lime.boldest': '--ds-chart-lime-boldest',
    'color.chart.lime.boldest.hovered': '--ds-chart-lime-boldest-hovered',
    'color.chart.neutral': '--ds-chart-neutral',
    'color.chart.neutral.hovered': '--ds-chart-neutral-hovered',
    'color.chart.red.bold': '--ds-chart-red-bold',
    'color.chart.red.bold.hovered': '--ds-chart-red-bold-hovered',
    'color.chart.red.bolder': '--ds-chart-red-bolder',
    'color.chart.red.bolder.hovered': '--ds-chart-red-bolder-hovered',
    'color.chart.red.boldest': '--ds-chart-red-boldest',
    'color.chart.red.boldest.hovered': '--ds-chart-red-boldest-hovered',
    'color.chart.orange.bold': '--ds-chart-orange-bold',
    'color.chart.orange.bold.hovered': '--ds-chart-orange-bold-hovered',
    'color.chart.orange.bolder': '--ds-chart-orange-bolder',
    'color.chart.orange.bolder.hovered': '--ds-chart-orange-bolder-hovered',
    'color.chart.orange.boldest': '--ds-chart-orange-boldest',
    'color.chart.orange.boldest.hovered': '--ds-chart-orange-boldest-hovered',
    'color.chart.yellow.bold': '--ds-chart-yellow-bold',
    'color.chart.yellow.bold.hovered': '--ds-chart-yellow-bold-hovered',
    'color.chart.yellow.bolder': '--ds-chart-yellow-bolder',
    'color.chart.yellow.bolder.hovered': '--ds-chart-yellow-bolder-hovered',
    'color.chart.yellow.boldest': '--ds-chart-yellow-boldest',
    'color.chart.yellow.boldest.hovered': '--ds-chart-yellow-boldest-hovered',
    'color.chart.green.bold': '--ds-chart-green-bold',
    'color.chart.green.bold.hovered': '--ds-chart-green-bold-hovered',
    'color.chart.green.bolder': '--ds-chart-green-bolder',
    'color.chart.green.bolder.hovered': '--ds-chart-green-bolder-hovered',
    'color.chart.green.boldest': '--ds-chart-green-boldest',
    'color.chart.green.boldest.hovered': '--ds-chart-green-boldest-hovered',
    'color.chart.teal.bold': '--ds-chart-teal-bold',
    'color.chart.teal.bold.hovered': '--ds-chart-teal-bold-hovered',
    'color.chart.teal.bolder': '--ds-chart-teal-bolder',
    'color.chart.teal.bolder.hovered': '--ds-chart-teal-bolder-hovered',
    'color.chart.teal.boldest': '--ds-chart-teal-boldest',
    'color.chart.teal.boldest.hovered': '--ds-chart-teal-boldest-hovered',
    'color.chart.blue.bold': '--ds-chart-blue-bold',
    'color.chart.blue.bold.hovered': '--ds-chart-blue-bold-hovered',
    'color.chart.blue.bolder': '--ds-chart-blue-bolder',
    'color.chart.blue.bolder.hovered': '--ds-chart-blue-bolder-hovered',
    'color.chart.blue.boldest': '--ds-chart-blue-boldest',
    'color.chart.blue.boldest.hovered': '--ds-chart-blue-boldest-hovered',
    'color.chart.purple.bold': '--ds-chart-purple-bold',
    'color.chart.purple.bold.hovered': '--ds-chart-purple-bold-hovered',
    'color.chart.purple.bolder': '--ds-chart-purple-bolder',
    'color.chart.purple.bolder.hovered': '--ds-chart-purple-bolder-hovered',
    'color.chart.purple.boldest': '--ds-chart-purple-boldest',
    'color.chart.purple.boldest.hovered': '--ds-chart-purple-boldest-hovered',
    'color.chart.magenta.bold': '--ds-chart-magenta-bold',
    'color.chart.magenta.bold.hovered': '--ds-chart-magenta-bold-hovered',
    'color.chart.magenta.bolder': '--ds-chart-magenta-bolder',
    'color.chart.magenta.bolder.hovered': '--ds-chart-magenta-bolder-hovered',
    'color.chart.magenta.boldest': '--ds-chart-magenta-boldest',
    'color.chart.magenta.boldest.hovered': '--ds-chart-magenta-boldest-hovered',
    'color.chart.gray.bold': '--ds-chart-gray-bold',
    'color.chart.gray.bold.hovered': '--ds-chart-gray-bold-hovered',
    'color.chart.gray.bolder': '--ds-chart-gray-bolder',
    'color.chart.gray.bolder.hovered': '--ds-chart-gray-bolder-hovered',
    'color.chart.gray.boldest': '--ds-chart-gray-boldest',
    'color.chart.gray.boldest.hovered': '--ds-chart-gray-boldest-hovered',
    'color.chart.brand': '--ds-chart-brand',
    'color.chart.brand.hovered': '--ds-chart-brand-hovered',
    'color.chart.danger': '--ds-chart-danger',
    'color.chart.danger.hovered': '--ds-chart-danger-hovered',
    'color.chart.danger.bold': '--ds-chart-danger-bold',
    'color.chart.danger.bold.hovered': '--ds-chart-danger-bold-hovered',
    'color.chart.warning': '--ds-chart-warning',
    'color.chart.warning.hovered': '--ds-chart-warning-hovered',
    'color.chart.warning.bold': '--ds-chart-warning-bold',
    'color.chart.warning.bold.hovered': '--ds-chart-warning-bold-hovered',
    'color.chart.success': '--ds-chart-success',
    'color.chart.success.hovered': '--ds-chart-success-hovered',
    'color.chart.success.bold': '--ds-chart-success-bold',
    'color.chart.success.bold.hovered': '--ds-chart-success-bold-hovered',
    'color.chart.discovery': '--ds-chart-discovery',
    'color.chart.discovery.hovered': '--ds-chart-discovery-hovered',
    'color.chart.discovery.bold': '--ds-chart-discovery-bold',
    'color.chart.discovery.bold.hovered': '--ds-chart-discovery-bold-hovered',
    'color.chart.information': '--ds-chart-information',
    'color.chart.information.hovered': '--ds-chart-information-hovered',
    'color.chart.information.bold': '--ds-chart-information-bold',
    'color.chart.information.bold.hovered': '--ds-chart-information-bold-hovered',
    'elevation.surface': '--ds-surface',
    'elevation.surface.hovered': '--ds-surface-hovered',
    'elevation.surface.pressed': '--ds-surface-pressed',
    'elevation.surface.overlay': '--ds-surface-overlay',
    'elevation.surface.overlay.hovered': '--ds-surface-overlay-hovered',
    'elevation.surface.overlay.pressed': '--ds-surface-overlay-pressed',
    'elevation.surface.raised': '--ds-surface-raised',
    'elevation.surface.raised.hovered': '--ds-surface-raised-hovered',
    'elevation.surface.raised.pressed': '--ds-surface-raised-pressed',
    'elevation.surface.sunken': '--ds-surface-sunken',
    'elevation.shadow.overflow': '--ds-shadow-overflow',
    'elevation.shadow.overflow.perimeter': '--ds-shadow-overflow-perimeter',
    'elevation.shadow.overflow.spread': '--ds-shadow-overflow-spread',
    'elevation.shadow.overlay': '--ds-shadow-overlay',
    'elevation.shadow.raised': '--ds-shadow-raised',
    'opacity.disabled': '--ds-opacity-disabled',
    'opacity.loading': '--ds-opacity-loading',
    'utility.UNSAFE.textTransformUppercase': '--ds-UNSAFE-textTransformUppercase',
    'utility.UNSAFE.transparent': '--ds-UNSAFE-transparent',
    'utility.elevation.surface.current': '--ds-elevation-surface-current',
    'border.radius.050': '--ds-border-radius-050',
    'border.radius': '--ds-border-radius',
    'border.radius.100': '--ds-border-radius-100',
    'border.radius.200': '--ds-border-radius-200',
    'border.radius.300': '--ds-border-radius-300',
    'border.radius.400': '--ds-border-radius-400',
    'border.radius.circle': '--ds-border-radius-circle',
    'border.width': '--ds-border-width',
    'border.width.0': '--ds-border-width-0',
    'border.width.indicator': '--ds-border-width-indicator',
    'border.width.outline': '--ds-border-width-outline',
    'space.0': '--ds-space-0',
    'space.025': '--ds-space-025',
    'space.050': '--ds-space-050',
    'space.075': '--ds-space-075',
    'space.100': '--ds-space-100',
    'space.150': '--ds-space-150',
    'space.200': '--ds-space-200',
    'space.250': '--ds-space-250',
    'space.300': '--ds-space-300',
    'space.400': '--ds-space-400',
    'space.500': '--ds-space-500',
    'space.600': '--ds-space-600',
    'space.800': '--ds-space-800',
    'space.1000': '--ds-space-1000',
    'space.negative.025': '--ds-space-negative-025',
    'space.negative.050': '--ds-space-negative-050',
    'space.negative.075': '--ds-space-negative-075',
    'space.negative.100': '--ds-space-negative-100',
    'space.negative.150': '--ds-space-negative-150',
    'space.negative.200': '--ds-space-negative-200',
    'space.negative.250': '--ds-space-negative-250',
    'space.negative.300': '--ds-space-negative-300',
    'space.negative.400': '--ds-space-negative-400',
    'font.heading.xxlarge': '--ds-font-heading-xxlarge',
    'font.heading.xlarge': '--ds-font-heading-xlarge',
    'font.heading.large': '--ds-font-heading-large',
    'font.heading.medium': '--ds-font-heading-medium',
    'font.heading.small': '--ds-font-heading-small',
    'font.heading.xsmall': '--ds-font-heading-xsmall',
    'font.heading.xxsmall': '--ds-font-heading-xxsmall',
    'font.body.large': '--ds-font-body-large',
    'font.body': '--ds-font-body',
    'font.body.small': '--ds-font-body-small',
    'font.body.UNSAFE_small': '--ds-font-body-UNSAFE_small',
    'font.code': '--ds-font-code',
    'font.weight.regular': '--ds-font-weight-regular',
    'font.weight.medium': '--ds-font-weight-medium',
    'font.weight.semibold': '--ds-font-weight-semibold',
    'font.weight.bold': '--ds-font-weight-bold',
    'font.family.heading': '--ds-font-family-heading',
    'font.family.body': '--ds-font-family-body',
    'font.family.code': '--ds-font-family-code',
    'font.family.brand.heading': '--ds-font-family-brand-heading',
    'font.family.brand.body': '--ds-font-family-brand-body'
  };
  var tokens$1 = tokens;

  var THEME_DATA_ATTRIBUTE = 'data-theme';
  var COLOR_MODE_ATTRIBUTE = 'data-color-mode';
  var CONTRAST_MODE_ATTRIBUTE = 'data-contrast-mode';
  var CUSTOM_THEME_ATTRIBUTE = 'data-custom-theme';

  // valid hex color with 6 digits
  var isValidBrandHex = function isValidBrandHex(hex) {
    return /^#[0-9A-F]{6}$/i.test(hex);
  };

  // valid hex color with 4, 6 or 8 digits
  var isValidHex = function isValidHex(hex) {
    return /^#([A-Fa-f0-9]{3,4}){1,2}$/.test(hex);
  };
  function rgbToHex(r, g, b) {
    return '#' + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
  }
  function getAlpha(hex) {
    if (hex.length === 9) {
      var int = parseInt(hex.slice(7, 9), 16) / 255;
      return Number(parseFloat(int.toString()).toFixed(2));
    }
    return 1;
  }
  function hexToRgbA(hex) {
    if (!isValidHex(hex)) {
      throw new Error('Invalid HEX');
    }
    var c;
    c = hex.substring(1).split('');
    if (c.length === 3) {
      c = [c[0], c[0], c[1], c[1], c[2], c[2]];
    }
    c = '0x' + c.join('');
    return [c >> 16 & 255, c >> 8 & 255, c & 255, getAlpha(hex)];
  }
  function hexToRgb(hex) {
    if (!isValidHex(hex)) {
      throw new Error('Invalid HEX');
    }
    var c;
    c = hex.substring(1).split('');
    if (c.length === 3) {
      c = [c[0], c[0], c[1], c[1], c[2], c[2]];
    }
    c = '0x' + c.join('');
    return [c >> 16 & 255, c >> 8 & 255, c & 255];
  }
  function hexToHSL(hex) {
    if (!isValidHex(hex)) {
      throw new Error('Invalid HEX');
    }
    var r = 0,
      g = 0,
      b = 0;
    if (hex.length === 4) {
      r = '0x' + hex[1] + hex[1];
      g = '0x' + hex[2] + hex[2];
      b = '0x' + hex[3] + hex[3];
    } else if (hex.length === 7) {
      r = '0x' + hex[1] + hex[2];
      g = '0x' + hex[3] + hex[4];
      b = '0x' + hex[5] + hex[6];
    }
    // Then to HSL
    r /= 255;
    g /= 255;
    b /= 255;
    var cmin = Math.min(r, g, b),
      cmax = Math.max(r, g, b),
      delta = cmax - cmin,
      h = 0,
      s = 0,
      l = 0;
    if (delta === 0) {
      h = 0;
    } else if (cmax === r) {
      h = (g - b) / delta % 6;
    } else if (cmax === g) {
      h = (b - r) / delta + 2;
    } else {
      h = (r - g) / delta + 4;
    }
    h = Math.round(h * 60);
    if (h < 0) {
      h += 360;
    }
    l = (cmax + cmin) / 2;
    s = delta === 0 ? 0 : delta / (1 - Math.abs(2 * l - 1));
    s = +(s * 100).toFixed(1);
    l = +(l * 100).toFixed(1);
    return [h, s, l];
  }
  function HSLToRGB(h, s, l) {
    s /= 100;
    l /= 100;
    var k = function k(n) {
      return (n + h / 30) % 12;
    };
    var a = s * Math.min(l, 1 - l);
    var f = function f(n) {
      return l - a * Math.max(-1, Math.min(k(n) - 3, Math.min(9 - k(n), 1)));
    };
    return [255 * f(0), 255 * f(8), 255 * f(4)];
  }
  function relativeLuminanceW3C(r, g, b) {
    var RsRGB = r / 255;
    var GsRGB = g / 255;
    var BsRGB = b / 255;
    var R = RsRGB <= 0.03928 ? RsRGB / 12.92 : Math.pow((RsRGB + 0.055) / 1.055, 2.4);
    var G = GsRGB <= 0.03928 ? GsRGB / 12.92 : Math.pow((GsRGB + 0.055) / 1.055, 2.4);
    var B = BsRGB <= 0.03928 ? BsRGB / 12.92 : Math.pow((BsRGB + 0.055) / 1.055, 2.4);

    // For the sRGB colorspace, the relative luminance of a color is defined as:
    var L = 0.2126 * R + 0.7152 * G + 0.0722 * B;
    return L;
  }
  function getContrastRatio(foreground, background) {
    if (!isValidHex(foreground) || !isValidHex(background)) {
      throw new Error('Invalid HEX');
    }
    var foregroundRgb = hexToRgb(foreground);
    var backgroundRgb = hexToRgb(background);
    var foregroundLuminance = relativeLuminanceW3C(foregroundRgb[0], foregroundRgb[1], foregroundRgb[2]);
    var backgroundLuminance = relativeLuminanceW3C(backgroundRgb[0], backgroundRgb[1], backgroundRgb[2]);
    // calculate the color contrast ratio
    var brightest = Math.max(foregroundLuminance, backgroundLuminance);
    var darkest = Math.min(foregroundLuminance, backgroundLuminance);
    return (brightest + 0.05) / (darkest + 0.05);
  }
  function deltaE(rgbA, rgbB) {
    var labA = rgbToLab(rgbA);
    var labB = rgbToLab(rgbB);
    var deltaL = labA[0] - labB[0];
    var deltaA = labA[1] - labB[1];
    var deltaB = labA[2] - labB[2];
    var c1 = Math.sqrt(labA[1] * labA[1] + labA[2] * labA[2]);
    var c2 = Math.sqrt(labB[1] * labB[1] + labB[2] * labB[2]);
    var deltaC = c1 - c2;
    var deltaH = deltaA * deltaA + deltaB * deltaB - deltaC * deltaC;
    deltaH = deltaH < 0 ? 0 : Math.sqrt(deltaH);
    var sc = 1.0 + 0.045 * c1;
    var sh = 1.0 + 0.015 * c1;
    var deltaLKlsl = deltaL / 1.0;
    var deltaCkcsc = deltaC / sc;
    var deltaHkhsh = deltaH / sh;
    var i = deltaLKlsl * deltaLKlsl + deltaCkcsc * deltaCkcsc + deltaHkhsh * deltaHkhsh;
    return i < 0 ? 0 : Math.sqrt(i);
  }
  function rgbToLab(rgb) {
    var r = rgb[0] / 255,
      g = rgb[1] / 255,
      b = rgb[2] / 255,
      x,
      y,
      z;
    r = r > 0.04045 ? Math.pow((r + 0.055) / 1.055, 2.4) : r / 12.92;
    g = g > 0.04045 ? Math.pow((g + 0.055) / 1.055, 2.4) : g / 12.92;
    b = b > 0.04045 ? Math.pow((b + 0.055) / 1.055, 2.4) : b / 12.92;
    x = (r * 0.4124 + g * 0.3576 + b * 0.1805) / 0.95047;
    y = (r * 0.2126 + g * 0.7152 + b * 0.0722) / 1.0;
    z = (r * 0.0193 + g * 0.1192 + b * 0.9505) / 1.08883;
    x = x > 0.008856 ? Math.pow(x, 1 / 3) : 7.787 * x + 16 / 116;
    y = y > 0.008856 ? Math.pow(y, 1 / 3) : 7.787 * y + 16 / 116;
    z = z > 0.008856 ? Math.pow(z, 1 / 3) : 7.787 * z + 16 / 116;
    return [116 * y - 16, 500 * (x - y), 200 * (y - z)];
  }

  var themeKinds = ['light', 'dark', 'spacing', 'typography', 'shape'];
  var customThemeOptions = 'UNSAFE_themeOptions';
  var isThemeKind = function isThemeKind(themeKind) {
    return themeKinds.find(function (kind) {
      return kind === themeKind;
    }) !== undefined;
  };
  var isThemeIds = function isThemeIds(themeId) {
    return themeIds.find(function (id) {
      return id === themeId;
    }) !== undefined;
  };
  var isColorMode = function isColorMode(modeId) {
    return ['light', 'dark', 'auto'].includes(modeId);
  };

  /**
   * Converts a theme object to a string formatted for the `data-theme` HTML attribute.
   *
   * @param {object} themes The themes that should be applied.
   *
   * @example
   * ```
   * themeObjectToString({ dark: 'dark', light: 'legacy-light', spacing: 'spacing' });
   * // returns 'dark:dark light:legacy-light spacing:spacing'
   * ```
   */
  var themeObjectToString = function themeObjectToString(themeState) {
    return Object.entries(themeState).reduce(function (themeString, _ref3) {
      var _ref4 = _slicedToArray(_ref3, 2),
        kind = _ref4[0],
        id = _ref4[1];
      if (
      // colorMode theme state
      kind === 'colorMode' && typeof id === 'string' && isColorMode(id) ||
      // custom theme state
      kind === customThemeOptions && _typeof(id) === 'object' ||
      // other theme states
      isThemeKind(kind) && typeof id === 'string' && isThemeIds(id)) {
        return themeString + "".concat(themeString ? ' ' : '') + "".concat(kind, ":").concat(_typeof(id) === 'object' ? JSON.stringify(id) : id);
      }
      return themeString;
    }, '');
  };

  var hash = function hash(str) {
    var hash = 0;
    for (var i = 0; i < str.length; i++) {
      var char = str.charCodeAt(i);
      hash = (hash << 5) - hash + char;
      hash &= hash; // Convert to 32bit integer
    }
    return new Uint32Array([hash])[0].toString(36);
  };

  function ownKeys$1(e, r) {
    var t = Object.keys(e);
    if (Object.getOwnPropertySymbols) {
      var o = Object.getOwnPropertySymbols(e);
      r && (o = o.filter(function (r) {
        return Object.getOwnPropertyDescriptor(e, r).enumerable;
      })), t.push.apply(t, o);
    }
    return t;
  }
  function _objectSpread$1(e) {
    for (var r = 1; r < arguments.length; r++) {
      var t = null != arguments[r] ? arguments[r] : {};
      r % 2 ? ownKeys$1(Object(t), !0).forEach(function (r) {
        _defineProperty(e, r, t[r]);
      }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys$1(Object(t)).forEach(function (r) {
        Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r));
      });
    }
    return e;
  }
  var defaultColorMode = 'light';
  var defaultContrastMode = 'no-preference';

  /**
   * Server-side rendering utility. Generates the valid HTML attributes for a given theme.
   * Note: this utility does not handle automatic theme switching.
   *
   * @param {Object<string, string>} themeOptions - Theme options object
   * @param {string} themeState.colorMode Determines which color theme is applied. If set to `auto`, the theme applied will be determined by the OS setting.
   * @param {string} themeState.dark The color theme to be applied when the color mode resolves to 'dark'.
   * @param {string} themeState.light The color theme to be applied when the color mode resolves to 'light'.
   * @param {string} themeState.spacing The spacing theme to be applied.
   * @param {string} themeState.typography The typography theme to be applied.
   * @param {Object} themeState.UNSAFE_themeOptions The custom branding options to be used for custom theme generation
   *
   * @returns {Object} Object of HTML attributes to be applied to the document root
   */
  var getThemeHtmlAttrs = function getThemeHtmlAttrs() {
    var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      _ref$colorMode = _ref.colorMode,
      colorMode = _ref$colorMode === void 0 ? themeStateDefaults['colorMode'] : _ref$colorMode,
      _ref$dark = _ref.dark,
      dark = _ref$dark === void 0 ? themeStateDefaults['dark'] : _ref$dark,
      _ref$light = _ref.light,
      light = _ref$light === void 0 ? themeStateDefaults['light'] : _ref$light,
      _ref$contrastMode = _ref.contrastMode,
      contrastMode = _ref$contrastMode === void 0 ? themeStateDefaults['contrastMode'] : _ref$contrastMode,
      _ref$shape = _ref.shape,
      shape = _ref$shape === void 0 ? themeStateDefaults['shape'] : _ref$shape,
      _ref$spacing = _ref.spacing,
      spacing = _ref$spacing === void 0 ? themeStateDefaults['spacing'] : _ref$spacing,
      _ref$typography = _ref.typography,
      typography = _ref$typography === void 0 ? themeStateDefaults['typography']() : _ref$typography,
      _ref$UNSAFE_themeOpti = _ref.UNSAFE_themeOptions,
      UNSAFE_themeOptions = _ref$UNSAFE_themeOpti === void 0 ? themeStateDefaults['UNSAFE_themeOptions'] : _ref$UNSAFE_themeOpti;
    var themeAttribute = themeObjectToString({
      dark: dark,
      light: light,
      shape: shape,
      spacing: spacing,
      typography: typography
    });
    var result = _defineProperty(_defineProperty({}, THEME_DATA_ATTRIBUTE, themeAttribute), COLOR_MODE_ATTRIBUTE, colorMode === 'auto' ? defaultColorMode : colorMode);
    if (fg('platform_increased-contrast-themes')) {
      result = _objectSpread$1(_objectSpread$1({}, result), {}, _defineProperty({}, CONTRAST_MODE_ATTRIBUTE, contrastMode === 'auto' ? defaultContrastMode : contrastMode));
    }
    if (UNSAFE_themeOptions && isValidBrandHex(UNSAFE_themeOptions.brandColor)) {
      var optionString = JSON.stringify(UNSAFE_themeOptions);
      var uniqueId = hash(optionString);
      result[CUSTOM_THEME_ATTRIBUTE] = uniqueId;
    }
    return result;
  };
  var getThemeHtmlAttrs$1 = getThemeHtmlAttrs;

  var dist = {};

  var bind$1 = {};

  Object.defineProperty(bind$1, "__esModule", {
    value: true
  });
  bind$1.bind = void 0;
  function bind(target, _a) {
    var type = _a.type,
      listener = _a.listener,
      options = _a.options;
    target.addEventListener(type, listener, options);
    return function unbind() {
      target.removeEventListener(type, listener, options);
    };
  }
  bind$1.bind = bind;

  var bindAll$1 = {};

  var __assign = commonjsGlobal && commonjsGlobal.__assign || function () {
    __assign = Object.assign || function (t) {
      for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
      }
      return t;
    };
    return __assign.apply(this, arguments);
  };
  Object.defineProperty(bindAll$1, "__esModule", {
    value: true
  });
  bindAll$1.bindAll = void 0;
  var bind_1 = bind$1;
  function toOptions(value) {
    if (typeof value === 'undefined') {
      return undefined;
    }
    if (typeof value === 'boolean') {
      return {
        capture: value
      };
    }
    return value;
  }
  function getBinding(original, sharedOptions) {
    if (sharedOptions == null) {
      return original;
    }
    var binding = __assign(__assign({}, original), {
      options: __assign(__assign({}, toOptions(sharedOptions)), toOptions(original.options))
    });
    return binding;
  }
  function bindAll(target, bindings, sharedOptions) {
    var unbinds = bindings.map(function (original) {
      var binding = getBinding(original, sharedOptions);
      return (0, bind_1.bind)(target, binding);
    });
    return function unbindAll() {
      unbinds.forEach(function (unbind) {
        return unbind();
      });
    };
  }
  bindAll$1.bindAll = bindAll;

  (function (exports) {

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.bindAll = exports.bind = void 0;
  var bind_1 = bind$1;
  Object.defineProperty(exports, "bind", {
    enumerable: true,
    get: function get() {
      return bind_1.bind;
    }
  });
  var bind_all_1 = bindAll$1;
  Object.defineProperty(exports, "bindAll", {
    enumerable: true,
    get: function get() {
      return bind_all_1.bindAll;
    }
  });
  }(dist));

  /**
   * THIS FILE WAS CREATED VIA CODEGEN DO NOT MODIFY {@see http://go/af-codegen}
   *
   * This file contains a dynamic import for each theme this package exports.
   * Themes are loaded asynchronously at runtime to minimise the amount of CSS we send to the client.
   * This allows users to compose their themes and only use the tokens that are requested.
   * When a new theme is created, the import should automatically be added to the map
   *
   * @codegen <<SignedSource::8352f41e09dfe9d45ead708661744456>>
   * @codegenCommand yarn build tokens
   */

  var themeImportsMap = {
    'light': function light() {
      return Promise.resolve().then(function () { return atlassianLight$1; });
    },
    'light-future': function lightFuture() {
      return Promise.resolve().then(function () { return atlassianLightFuture$1; });
    },
    'light-increased-contrast': function lightIncreasedContrast() {
      return Promise.resolve().then(function () { return atlassianLightIncreasedContrast$1; });
    },
    'dark': function dark() {
      return Promise.resolve().then(function () { return atlassianDark$1; });
    },
    'dark-future': function darkFuture() {
      return Promise.resolve().then(function () { return atlassianDarkFuture$1; });
    },
    'dark-increased-contrast': function darkIncreasedContrast() {
      return Promise.resolve().then(function () { return atlassianDarkIncreasedContrast$1; });
    },
    'legacy-light': function legacyLight() {
      return Promise.resolve().then(function () { return atlassianLegacyLight$1; });
    },
    'legacy-dark': function legacyDark() {
      return Promise.resolve().then(function () { return atlassianLegacyDark$1; });
    },
    'spacing': function spacing() {
      return Promise.resolve().then(function () { return atlassianSpacing$1; });
    },
    'typography-adg3': function typographyAdg3() {
      return Promise.resolve().then(function () { return atlassianTypographyAdg3$1; });
    },
    'shape': function shape() {
      return Promise.resolve().then(function () { return atlassianShape$1; });
    },
    'typography-modernized': function typographyModernized() {
      return Promise.resolve().then(function () { return atlassianTypographyModernized$1; });
    },
    'typography-refreshed': function typographyRefreshed() {
      return Promise.resolve().then(function () { return atlassianTypographyRefreshed$1; });
    },
    'light-brand-refresh': function lightBrandRefresh() {
      return Promise.resolve().then(function () { return atlassianLightBrandRefresh$1; });
    },
    'dark-brand-refresh': function darkBrandRefresh() {
      return Promise.resolve().then(function () { return atlassianDarkBrandRefresh$1; });
    }
  };
  var themeImportMap = themeImportsMap;

  var loadAndAppendThemeCss = /*#__PURE__*/function () {
    var _ref = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee(themeId) {
      var themeCss, style;
      return regenerator.wrap(function _callee$(_context) {
        while (1) switch (_context.prev = _context.next) {
          case 0:
            if (!document.head.querySelector("style[".concat(THEME_DATA_ATTRIBUTE, "=\"").concat(themeId, "\"]:not([").concat(CUSTOM_THEME_ATTRIBUTE, "])"))) {
              _context.next = 2;
              break;
            }
            return _context.abrupt("return");
          case 2:
            if (themeId) {
              _context.next = 4;
              break;
            }
            return _context.abrupt("return");
          case 4:
            _context.next = 6;
            return loadThemeCss(themeId);
          case 6:
            themeCss = _context.sent;
            style = document.createElement('style');
            style.textContent = themeCss;
            style.dataset.theme = themeId;
            document.head.appendChild(style);
          case 11:
          case "end":
            return _context.stop();
        }
      }, _callee);
    }));
    return function loadAndAppendThemeCss(_x) {
      return _ref.apply(this, arguments);
    };
  }();
  var loadThemeCss = /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee2(themeId) {
      var _yield$themeImportMap, themeCss;
      return regenerator.wrap(function _callee2$(_context2) {
        while (1) switch (_context2.prev = _context2.next) {
          case 0:
            if ((themeId === 'light' || themeId === 'dark') && fg('platform-component-visual-refresh')) {
              themeId += '-brand-refresh';
            }
            _context2.next = 3;
            return themeImportMap[themeId]();
          case 3:
            _yield$themeImportMap = _context2.sent;
            themeCss = _yield$themeImportMap.default;
            return _context2.abrupt("return", themeCss);
          case 6:
          case "end":
            return _context2.stop();
        }
      }, _callee2);
    }));
    return function loadThemeCss(_x2) {
      return _ref2.apply(this, arguments);
    };
  }();
  var darkModeMediaQuery = '(prefers-color-scheme: dark)';
  var moreContrastMediaQuery = '(prefers-contrast: more)';

  var isMatchMediaAvailable$1 = typeof window !== 'undefined' && 'matchMedia' in window;

  /**
   * Updates the current theme when the system theme changes. Should be bound
   * to an event listener listening on the '(prefers-color-scheme: dark)' query
   * @param e The event representing a change in system theme.
   */
  function checkNativeListener$1(e) {
    var element = document.documentElement;
    element.setAttribute(COLOR_MODE_ATTRIBUTE, e.matches ? 'dark' : 'light');
  }
  var darkModeMql = isMatchMediaAvailable$1 && window.matchMedia(darkModeMediaQuery);
  var ColorModeObserver = /*#__PURE__*/function () {
    function ColorModeObserver() {
      _classCallCheck(this, ColorModeObserver);
      _defineProperty(this, "unbindThemeChangeListener", null);
    }
    return _createClass(ColorModeObserver, [{
      key: "getColorMode",
      value: function getColorMode() {
        if (!darkModeMql) {
          return 'light';
        }
        return darkModeMql !== null && darkModeMql !== void 0 && darkModeMql.matches ? 'dark' : 'light';
      }
    }, {
      key: "bind",
      value: function bind() {
        if (darkModeMql && this.unbindThemeChangeListener === null) {
          this.unbindThemeChangeListener = dist.bind(darkModeMql, {
            type: 'change',
            listener: checkNativeListener$1
          });
        }
      }
    }, {
      key: "unbind",
      value: function unbind() {
        if (this.unbindThemeChangeListener) {
          this.unbindThemeChangeListener();
          this.unbindThemeChangeListener = null;
        }
      }
    }]);
  }();
  /**
   * A singleton color mode observer - binds "auto" switching logic to a single `mediaQueryList` listener
   * that can be unbound by any consumer when no longer needed.
   */
  var SingletonColorModeObserver = new ColorModeObserver();
  var ColorModeObserver$1 = SingletonColorModeObserver;

  var isMatchMediaAvailable = typeof window !== 'undefined' && 'matchMedia' in window;

  /**
   * Updates the current theme when the system contrast preference changes. Should be bound
   * to an event listener listening on the '(prefers-contrast: more)' query
   * @param e The event representing a change in system theme.
   */
  function checkNativeListener(e) {
    var element = document.documentElement;
    element.setAttribute(CONTRAST_MODE_ATTRIBUTE, e.matches ? 'more' : 'no-preference');
  }
  var contrastModeMql = isMatchMediaAvailable && window.matchMedia(moreContrastMediaQuery);
  var ContrastModeObserver = /*#__PURE__*/function () {
    function ContrastModeObserver() {
      _classCallCheck(this, ContrastModeObserver);
      _defineProperty(this, "unbindContrastChangeListener", null);
    }
    return _createClass(ContrastModeObserver, [{
      key: "getContrastMode",
      value: function getContrastMode() {
        if (!contrastModeMql) {
          return 'no-preference';
        }
        return contrastModeMql !== null && contrastModeMql !== void 0 && contrastModeMql.matches ? 'more' : 'no-preference';
      }
    }, {
      key: "bind",
      value: function bind() {
        if (contrastModeMql && this.unbindContrastChangeListener === null) {
          this.unbindContrastChangeListener = dist.bind(contrastModeMql, {
            type: 'change',
            listener: checkNativeListener
          });
        }
      }
    }, {
      key: "unbind",
      value: function unbind() {
        if (this.unbindContrastChangeListener) {
          this.unbindContrastChangeListener();
          this.unbindContrastChangeListener = null;
        }
      }
    }]);
  }();
  /**
   * A singleton contrast mode observer - binds "auto" switching logic to a single `mediaQueryList` listener
   * that can be unbound by any consumer when no longer needed.
   */
  var SingletonContrastModeObserver = new ContrastModeObserver();
  var ContrastModeObserver$1 = SingletonContrastModeObserver;

  /**
   * Given ThemeState, sets appropriate html attributes on the documentElement,
   * adds a listener to keep colorMode updated, and returns a function to unbind.
   */
  function configurePage(themeState) {
    if (themeState.colorMode === 'auto') {
      // Set colorMode based on the user preference
      themeState.colorMode = ColorModeObserver$1.getColorMode();
      // Bind a listener (if one doesn't already exist) to keep colorMode updated
      ColorModeObserver$1.bind();
    } else {
      ColorModeObserver$1.unbind();
    }
    if (fg('platform_increased-contrast-themes')) {
      if (themeState.contrastMode === 'auto') {
        // Set contrastMode based on the user preference
        themeState.contrastMode = ContrastModeObserver$1.getContrastMode();
        // Bind a listener (if one doesn't already exist) to keep contrastMode updated
        ContrastModeObserver$1.bind();
      } else {
        ContrastModeObserver$1.unbind();
      }
    }
    var themeAttributes = getThemeHtmlAttrs$1(themeState);
    Object.entries(themeAttributes).forEach(function (_ref) {
      var _ref2 = _slicedToArray(_ref, 2),
        key = _ref2[0],
        value = _ref2[1];
      document.documentElement.setAttribute(key, value);
    });
    return function () {
      ColorModeObserver$1.unbind();
      if (fg('platform_increased-contrast-themes')) {
        ContrastModeObserver$1.unbind();
      }
    };
  }

  function _arrayWithoutHoles(arr) {
    if (Array.isArray(arr)) return _arrayLikeToArray$2(arr);
  }

  function _iterableToArray(iter) {
    if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter);
  }

  function _nonIterableSpread() {
    throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
  }

  function _toConsumableArray(arr) {
    return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray$2(arr) || _nonIterableSpread();
  }

  function findMissingCustomStyleElements(UNSAFE_themeOptions, mode) {
    var optionString = JSON.stringify(UNSAFE_themeOptions);
    var uniqueId = hash(optionString);
    var attrOfMissingCustomStyles = [];
    (mode === 'auto' ? ['light', 'dark'] : [mode]).forEach(function (themeId) {
      var element = document.head.querySelector("style[".concat(CUSTOM_THEME_ATTRIBUTE, "=\"").concat(uniqueId, "\"][").concat(THEME_DATA_ATTRIBUTE, "=\"").concat(themeId, "\"]"));
      if (element) {
        // Append the existing custom styles to take precedence over others
        document.head.appendChild(element);
      } else {
        attrOfMissingCustomStyles.push(themeId);
      }
    });
    return attrOfMissingCustomStyles;
  }
  function limitSizeOfCustomStyleElements(sizeThreshold) {
    var styleTags = _toConsumableArray(Array.from(document.head.querySelectorAll("style[".concat(CUSTOM_THEME_ATTRIBUTE, "][").concat(THEME_DATA_ATTRIBUTE, "]"))));
    if (styleTags.length < sizeThreshold) {
      return;
    }
    styleTags.slice(0, styleTags.length - (sizeThreshold - 1)).forEach(function (element) {
      return element.remove();
    });
  }
  function reduceTokenMap(tokenMap, themeRamp) {
    return Object.entries(tokenMap).reduce(function (acc, _ref) {
      var _ref2 = _slicedToArray(_ref, 2),
        key = _ref2[0],
        value = _ref2[1];
      var cssVar = tokens$1[key];
      return cssVar ? "".concat(acc, "\n  ").concat(cssVar, ": ").concat(typeof value === 'string' ? value : themeRamp[value], ";") : acc;
    }, '');
  }

  /**
   * Finds any matching increased contrast theme available for a selected theme.
   */
  function getIncreasedContrastTheme(themeId) {
    var _Object$entries$find;
    return (_Object$entries$find = Object.entries(themeConfig).find(function (_ref) {
      var _ref2 = _slicedToArray(_ref, 2),
        increasesContrastFor = _ref2[1].increasesContrastFor;
      return increasesContrastFor === themeId;
    })) === null || _Object$entries$find === void 0 ? void 0 : _Object$entries$find[1].id;
  }

  var getThemePreferences = function getThemePreferences(themeState) {
    var colorMode = themeState.colorMode,
      contrastMode = themeState.contrastMode,
      dark = themeState.dark,
      light = themeState.light,
      shape = themeState.shape,
      spacing = themeState.spacing,
      typography = themeState.typography;
    var autoColorModeThemes = [light, dark];
    var themePreferences = [];
    if (colorMode === 'auto') {
      if (contrastMode !== 'no-preference' && fg('platform_increased-contrast-themes')) {
        autoColorModeThemes.forEach(function (normalTheme) {
          var increasedContrastTheme = getIncreasedContrastTheme(normalTheme);
          if (increasedContrastTheme) {
            autoColorModeThemes.push(increasedContrastTheme);
          }
        });
      }
      themePreferences.push.apply(themePreferences, autoColorModeThemes);
    } else {
      themePreferences.push(themeState[colorMode]);
      if (contrastMode !== 'no-preference' && fg('platform_increased-contrast-themes')) {
        var increasedContrastTheme = getIncreasedContrastTheme(themeState[colorMode]);
        if (increasedContrastTheme) {
          themePreferences.push(increasedContrastTheme);
        }
      }
    }
    [shape, spacing, typography].forEach(function (themeId) {
      if (themeId) {
        themePreferences.push(themeId);
      }
    });
    return _toConsumableArray(new Set(themePreferences));
  };
  var getThemeOverridePreferences = function getThemeOverridePreferences(_themeState) {
    var themeOverridePreferences = [];
    return _toConsumableArray(new Set(themeOverridePreferences));
  };

  function _createForOfIteratorHelper(r, e) {
    var t = "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"];
    if (!t) {
      if (Array.isArray(r) || (t = _unsupportedIterableToArray(r)) || e && r && "number" == typeof r.length) {
        t && (r = t);
        var _n = 0,
          F = function F() {};
        return {
          s: F,
          n: function n() {
            return _n >= r.length ? {
              done: !0
            } : {
              done: !1,
              value: r[_n++]
            };
          },
          e: function e(r) {
            throw r;
          },
          f: F
        };
      }
      throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
    }
    var o,
      a = !0,
      u = !1;
    return {
      s: function s() {
        t = t.call(r);
      },
      n: function n() {
        var r = t.next();
        return a = r.done, r;
      },
      e: function e(r) {
        u = !0, o = r;
      },
      f: function f() {
        try {
          a || null == t.return || t.return();
        } finally {
          if (u) throw o;
        }
      }
    };
  }
  function _unsupportedIterableToArray(r, a) {
    if (r) {
      if ("string" == typeof r) return _arrayLikeToArray(r, a);
      var t = {}.toString.call(r).slice(8, -1);
      return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray(r, a) : void 0;
    }
  }
  function _arrayLikeToArray(r, a) {
    (null == a || a > r.length) && (a = r.length);
    for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e];
    return n;
  }

  /**
   * Sets the theme globally at runtime. This updates the `data-theme` and `data-color-mode` attributes on your page's <html> tag.
   *
   * @param {Object<string, string>} themeState The themes and color mode that should be applied.
   * @param {string} themeState.colorMode Determines which color theme is applied. If set to `auto`, the theme applied will be determined by the OS setting.
   * @param {string} themeState.contrastMode The contrast mode theme to be applied. If set to `auto`, the theme applied will be determined by the OS setting.set to `auto`, the theme applied will be determined by the OS setting.
   * @param {string} themeState.dark The color theme to be applied when the color mode resolves to 'dark'.
   * @param {string} themeState.light The color theme to be applied when the color mode resolves to 'light'.
   * @param {string} themeState.shape The shape theme to be applied.
   * @param {string} themeState.spacing The spacing theme to be applied.
   * @param {string} themeState.typography The typography theme to be applied.
   * @param {Object} themeState.UNSAFE_themeOptions The custom branding options to be used for custom theme generation
   * @param {function} themeLoader Callback function used to override the default theme loading functionality.
   *
   * @returns A Promise of an unbind function, that can be used to stop listening for changes to system theme.
   *
   * @example
   * ```
   * setGlobalTheme({colorMode: 'auto', light: 'light', dark: 'dark', spacing: 'spacing'});
   * ```
   */
  var setGlobalTheme = /*#__PURE__*/function () {
    var _ref = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee3() {
      var _ref2,
        _ref2$colorMode,
        colorMode,
        _ref2$contrastMode,
        contrastMode,
        _ref2$dark,
        dark,
        _ref2$light,
        light,
        _ref2$shape,
        shape,
        _ref2$spacing,
        spacing,
        _ref2$typography,
        typography,
        _ref2$UNSAFE_themeOpt,
        UNSAFE_themeOptions,
        themeLoader,
        themeState,
        themePreferences,
        loadingStrategy,
        loadingTasks,
        mode,
        attrOfMissingCustomStyles,
        themeOverridePreferences,
        _iterator,
        _step,
        themeId,
        autoUnbind,
        _args3 = arguments;
      return regenerator.wrap(function _callee3$(_context3) {
        while (1) switch (_context3.prev = _context3.next) {
          case 0:
            _ref2 = _args3.length > 0 && _args3[0] !== undefined ? _args3[0] : {}, _ref2$colorMode = _ref2.colorMode, colorMode = _ref2$colorMode === void 0 ? themeStateDefaults['colorMode'] : _ref2$colorMode, _ref2$contrastMode = _ref2.contrastMode, contrastMode = _ref2$contrastMode === void 0 ? themeStateDefaults['contrastMode'] : _ref2$contrastMode, _ref2$dark = _ref2.dark, dark = _ref2$dark === void 0 ? themeStateDefaults['dark'] : _ref2$dark, _ref2$light = _ref2.light, light = _ref2$light === void 0 ? themeStateDefaults['light'] : _ref2$light, _ref2$shape = _ref2.shape, shape = _ref2$shape === void 0 ? themeStateDefaults['shape'] : _ref2$shape, _ref2$spacing = _ref2.spacing, spacing = _ref2$spacing === void 0 ? themeStateDefaults['spacing'] : _ref2$spacing, _ref2$typography = _ref2.typography, typography = _ref2$typography === void 0 ? themeStateDefaults['typography']() : _ref2$typography, _ref2$UNSAFE_themeOpt = _ref2.UNSAFE_themeOptions, UNSAFE_themeOptions = _ref2$UNSAFE_themeOpt === void 0 ? themeStateDefaults['UNSAFE_themeOptions'] : _ref2$UNSAFE_themeOpt;
            themeLoader = _args3.length > 1 ? _args3[1] : undefined;
            // CLEANUP: Remove. This blocks application of increased contrast themes
            // without the feature flag enabled.
            if (!fg('platform_increased-contrast-themes')) {
              if (light === 'light-increased-contrast') {
                light = 'light';
              }
              if (dark === 'dark-increased-contrast') {
                dark = 'dark';
              }
            }
            themeState = {
              colorMode: colorMode,
              contrastMode: contrastMode,
              dark: dark,
              light: light,
              shape: shape,
              spacing: spacing,
              typography: typography,
              UNSAFE_themeOptions: themeLoader ? undefined : UNSAFE_themeOptions
            }; // Determine what to load and loading strategy
            themePreferences = getThemePreferences(themeState);
            loadingStrategy = themeLoader ? themeLoader : loadAndAppendThemeCss; // Load standard themes
            loadingTasks = themePreferences.map(/*#__PURE__*/function () {
              var _ref3 = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee(themeId) {
                return regenerator.wrap(function _callee$(_context) {
                  while (1) switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return loadingStrategy(themeId);
                    case 2:
                      return _context.abrupt("return", _context.sent);
                    case 3:
                    case "end":
                      return _context.stop();
                  }
                }, _callee);
              }));
              return function (_x) {
                return _ref3.apply(this, arguments);
              };
            }()); // Load custom themes if needed
            if (!themeLoader && UNSAFE_themeOptions && isValidBrandHex(UNSAFE_themeOptions === null || UNSAFE_themeOptions === void 0 ? void 0 : UNSAFE_themeOptions.brandColor)) {
              mode = colorMode || themeStateDefaults['colorMode'];
              attrOfMissingCustomStyles = findMissingCustomStyleElements(UNSAFE_themeOptions, mode);
              if (attrOfMissingCustomStyles.length > 0) {
                // Load custom theme styles
                loadingTasks.push(_asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee2() {
                  var _yield$import, loadAndAppendCustomThemeCss;
                  return regenerator.wrap(function _callee2$(_context2) {
                    while (1) switch (_context2.prev = _context2.next) {
                      case 0:
                        _context2.next = 2;
                        return Promise.resolve().then(function () { return customTheme; });
                      case 2:
                        _yield$import = _context2.sent;
                        loadAndAppendCustomThemeCss = _yield$import.loadAndAppendCustomThemeCss;
                        loadAndAppendCustomThemeCss({
                          colorMode: attrOfMissingCustomStyles.length === 2 ? 'auto' :
                          // only load the missing custom theme styles
                          attrOfMissingCustomStyles[0],
                          UNSAFE_themeOptions: UNSAFE_themeOptions
                        });
                      case 5:
                      case "end":
                        return _context2.stop();
                    }
                  }, _callee2);
                }))());
              }
            }
            _context3.next = 10;
            return Promise.all(loadingTasks);
          case 10:
            // Load override themes after standard themes
            themeOverridePreferences = getThemeOverridePreferences();
            _iterator = _createForOfIteratorHelper(themeOverridePreferences);
            _context3.prev = 12;
            _iterator.s();
          case 14:
            if ((_step = _iterator.n()).done) {
              _context3.next = 20;
              break;
            }
            themeId = _step.value;
            _context3.next = 18;
            return loadingStrategy(themeId);
          case 18:
            _context3.next = 14;
            break;
          case 20:
            _context3.next = 25;
            break;
          case 22:
            _context3.prev = 22;
            _context3.t0 = _context3["catch"](12);
            _iterator.e(_context3.t0);
          case 25:
            _context3.prev = 25;
            _iterator.f();
            return _context3.finish(25);
          case 28:
            autoUnbind = configurePage(themeState);
            return _context3.abrupt("return", autoUnbind);
          case 30:
          case "end":
            return _context3.stop();
        }
      }, _callee3, null, [[12, 22, 25, 28]]);
    }));
    return function setGlobalTheme() {
      return _ref.apply(this, arguments);
    };
  }();
  var setGlobalTheme$1 = setGlobalTheme;

  function appendStyle(id, url, attr) {
    if (attr === void 0) {
      attr = 'theme';
    }
    var link = document.createElement('link');
    link.rel = 'stylesheet';
    link.href = url;
    link.dataset[attr] = id;
    document.head.appendChild(link);
  }
  function themeLoader(id) {
    var stylesheetUrl = "https://connect-cdn.atl-paas.net/themes/atlaskit-tokens_" + id + ".css";
    if (document.querySelector("link[href=\"" + stylesheetUrl + "\"]")) {
      return;
    }
    appendStyle(id, stylesheetUrl);
  }
  function appendSurface(surface) {
    document.documentElement.dataset.surface = surface;
    if (document.querySelector('link[data-surface]')) {
      return;
    }
    appendStyle(surface, 'https://connect-cdn.atl-paas.net/surfaces.css', 'surface');
  }
  function removeSurface() {
    delete document.documentElement.dataset.surface;
  }
  function loadLegacyTextColorStyles() {
    return _loadLegacyTextColorStyles.apply(this, arguments);
  }
  function _loadLegacyTextColorStyles() {
    _loadLegacyTextColorStyles = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee3() {
      return regenerator.wrap(function _callee3$(_context3) {
        while (1) switch (_context3.prev = _context3.next) {
          case 0:
            if (!(combined._data.options.moduleType !== 'dynamicContentMacros')) {
              _context3.next = 2;
              break;
            }
            return _context3.abrupt("return");
          case 2:
            loadLegacyTextColorStylesCdn(); // Load via Connect CDN stylesheet
          case 3:
          case "end":
            return _context3.stop();
        }
      }, _callee3);
    }));
    return _loadLegacyTextColorStyles.apply(this, arguments);
  }
  function loadLegacyTextColorStylesCdn() {
    if (document.querySelector('link[data-legacy-text-colors]')) {
      return;
    }
    appendStyle('legacy-text-colors', 'https://connect-cdn.atl-paas.net/legacy-text-colors.css', 'legacyTextColors');
  }
  function setTheme(_x) {
    return _setTheme.apply(this, arguments);
  }
  function _setTheme() {
    _setTheme = _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee4(options) {
      return regenerator.wrap(function _callee4$(_context4) {
        while (1) switch (_context4.prev = _context4.next) {
          case 0:
            _context4.next = 2;
            return setGlobalTheme$1(options, themeLoader);
          case 2:
          case "end":
            return _context4.stop();
        }
      }, _callee4);
    }));
    return _setTheme.apply(this, arguments);
  }
  var hasRegisteredCSPViolationHandler = false;
  function registerCSPViolationHandler() {
    if (hasRegisteredCSPViolationHandler) {
      return;
    }
    hasRegisteredCSPViolationHandler = true;
    window.addEventListener('securitypolicyviolation', function (event) {
      if (event.blockedURI.includes('connect-cdn.atl-paas.net')) {
        console.warn("CSP violation detected for " + event.violatedDirective + " in app " + event.documentURI + "\nPlease add https://connect-cdn.atl-paas.net to your security policy for script-src and style-src.\nSee https://developer.atlassian.com/platform/marketplace/security-requirements/#connect-apps for details.");
      }
    });
  }
  var Theming = {
    isThemingEnabled: false,
    forceNoCleanup: false,
    onThemeInitialized: function onThemeInitialized(data) {
      return _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee() {
        return regenerator.wrap(function _callee$(_context) {
          while (1) switch (_context.prev = _context.next) {
            case 0:
              Theming.isThemingEnabled = true;
              Theming.forceNoCleanup = data.forceNoCleanup || false;
              registerCSPViolationHandler();
              _context.next = 5;
              return Theming.onThemeChanged(data);
            case 5:
              if (window.AP && window.AP.theming && window.AP.theming._finishedInitTheming) {
                window.AP.theming._finishedInitTheming();
              }
            case 6:
            case "end":
              return _context.stop();
          }
        }, _callee);
      }))();
    },
    onThemeChanged: function onThemeChanged(data) {
      return _asyncToGenerator(/*#__PURE__*/regenerator.mark(function _callee2() {
        var newTheme;
        return regenerator.wrap(function _callee2$(_context2) {
          while (1) switch (_context2.prev = _context2.next) {
            case 0:
              if (Theming.isThemingEnabled) {
                _context2.next = 2;
                break;
              }
              return _context2.abrupt("return");
            case 2:
              newTheme = data.initialTheme || data.newTheme;
              if (!(!newTheme || Object.keys(newTheme).length === 0)) {
                _context2.next = 13;
                break;
              }
              if (!Theming.forceNoCleanup) {
                _context2.next = 9;
                break;
              }
              _context2.next = 7;
              return setTheme({
                colorMode: 'light'
              });
            case 7:
              removeSurface();
              return _context2.abrupt("return");
            case 9:
              removeSurface();
              document.documentElement.removeAttribute('data-theme');
              document.documentElement.removeAttribute('data-color-mode');
              return _context2.abrupt("return");
            case 13:
              _context2.next = 15;
              return setTheme(newTheme);
            case 15:
              // Remove existing surface styles.
              removeSurface();
              if (!!data.surface) {
                // Surface styles must be applied after themes for specificity.
                appendSurface(data.surface);
              }
            case 17:
            case "end":
              return _context2.stop();
          }
        }, _callee2);
      }))();
    },
    initializeThemeListeners: function initializeThemeListeners() {
      if (!combined.theming) {
        return;
      }
      combined.register({
        theme_initialized: Theming.onThemeInitialized,
        theme_changed: Theming.onThemeChanged
      });
    }
  };

  // duplicated from ./host/stores/extension_configuration_options_store
  var ExtensionConfigurationOptionsStore = /*#__PURE__*/function () {
    function ExtensionConfigurationOptionsStore() {
      this.store = {};
    }
    var _proto = ExtensionConfigurationOptionsStore.prototype;
    _proto.set = function set(obj, val) {
      if (val) {
        var toSet = {};
        toSet[obj] = val;
      } else {
        toSet = obj;
      }
      _util.extend(this.store, toSet);
    };
    _proto.get = function get(key) {
      if (key) {
        return this.store[key];
      }
      return _util.extend({}, this.store); //clone
    };
    return ExtensionConfigurationOptionsStore;
  }();
  var ExtensionConfigurationOptionsStore$1 = new ExtensionConfigurationOptionsStore();

  var o = -1,
    a = function a(e) {
      addEventListener("pageshow", function (n) {
        n.persisted && (o = n.timeStamp, e(n));
      }, !0);
    },
    c = function c() {
      var e = self.performance && performance.getEntriesByType && performance.getEntriesByType("navigation")[0];
      if (e && e.responseStart > 0 && e.responseStart < performance.now()) return e;
    },
    u = function u() {
      var e = c();
      return e && e.activationStart || 0;
    },
    f = function f(e, n) {
      var t = c(),
        r = "navigate";
      o >= 0 ? r = "back-forward-cache" : t && (document.prerendering || u() > 0 ? r = "prerender" : document.wasDiscarded ? r = "restore" : t.type && (r = t.type.replace(/_/g, "-")));
      return {
        name: e,
        value: void 0 === n ? -1 : n,
        rating: "good",
        delta: 0,
        entries: [],
        id: "v4-".concat(Date.now(), "-").concat(Math.floor(8999999999999 * Math.random()) + 1e12),
        navigationType: r
      };
    },
    s = function s(e, n, t) {
      try {
        if (PerformanceObserver.supportedEntryTypes.includes(e)) {
          var r = new PerformanceObserver(function (e) {
            Promise.resolve().then(function () {
              n(e.getEntries());
            });
          });
          return r.observe(Object.assign({
            type: e,
            buffered: !0
          }, t || {})), r;
        }
      } catch (e) {}
    },
    d = function d(e, n, t, r) {
      var i, o;
      return function (a) {
        n.value >= 0 && (a || r) && ((o = n.value - (i || 0)) || void 0 === i) && (i = n.value, n.delta = o, n.rating = function (e, n) {
          return e > n[1] ? "poor" : e > n[0] ? "needs-improvement" : "good";
        }(n.value, t), e(n));
      };
    },
    l = function l(e) {
      requestAnimationFrame(function () {
        return requestAnimationFrame(function () {
          return e();
        });
      });
    },
    p = function p(e) {
      document.addEventListener("visibilitychange", function () {
        "hidden" === document.visibilityState && e();
      });
    },
    v = function v(e) {
      var n = !1;
      return function () {
        n || (e(), n = !0);
      };
    },
    m = -1,
    h = function h() {
      return "hidden" !== document.visibilityState || document.prerendering ? 1 / 0 : 0;
    },
    g = function g(e) {
      "hidden" === document.visibilityState && m > -1 && (m = "visibilitychange" === e.type ? e.timeStamp : 0, T());
    },
    y = function y() {
      addEventListener("visibilitychange", g, !0), addEventListener("prerenderingchange", g, !0);
    },
    T = function T() {
      removeEventListener("visibilitychange", g, !0), removeEventListener("prerenderingchange", g, !0);
    },
    E = function E() {
      return m < 0 && (m = h(), y(), a(function () {
        setTimeout(function () {
          m = h(), y();
        }, 0);
      })), {
        get firstHiddenTime() {
          return m;
        }
      };
    },
    C = function C(e) {
      document.prerendering ? addEventListener("prerenderingchange", function () {
        return e();
      }, !0) : e();
    },
    b = [1800, 3e3],
    S = function S(e, n) {
      n = n || {}, C(function () {
        var t,
          r = E(),
          i = f("FCP"),
          o = s("paint", function (e) {
            e.forEach(function (e) {
              "first-contentful-paint" === e.name && (o.disconnect(), e.startTime < r.firstHiddenTime && (i.value = Math.max(e.startTime - u(), 0), i.entries.push(e), t(!0)));
            });
          });
        o && (t = d(e, i, b, n.reportAllChanges), a(function (r) {
          i = f("FCP"), t = d(e, i, b, n.reportAllChanges), l(function () {
            i.value = performance.now() - r.timeStamp, t(!0);
          });
        }));
      });
    },
    L = [.1, .25],
    w = function w(e, n) {
      n = n || {}, S(v(function () {
        var t,
          r = f("CLS", 0),
          i = 0,
          o = [],
          c = function c(e) {
            e.forEach(function (e) {
              if (!e.hadRecentInput) {
                var n = o[0],
                  t = o[o.length - 1];
                i && e.startTime - t.startTime < 1e3 && e.startTime - n.startTime < 5e3 ? (i += e.value, o.push(e)) : (i = e.value, o = [e]);
              }
            }), i > r.value && (r.value = i, r.entries = o, t());
          },
          u = s("layout-shift", c);
        u && (t = d(e, r, L, n.reportAllChanges), p(function () {
          c(u.takeRecords()), t(!0);
        }), a(function () {
          i = 0, r = f("CLS", 0), t = d(e, r, L, n.reportAllChanges), l(function () {
            return t();
          });
        }), setTimeout(t, 0));
      }));
    },
    O = function O(e) {
      var n = self.requestIdleCallback || self.setTimeout,
        t = -1;
      return e = v(e), "hidden" === document.visibilityState ? e() : (t = n(e), p(e)), t;
    },
    _ = [2500, 4e3],
    z = {},
    G = function G(e, n) {
      n = n || {}, C(function () {
        var t,
          r = E(),
          i = f("LCP"),
          o = function o(e) {
            n.reportAllChanges || (e = e.slice(-1)), e.forEach(function (e) {
              e.startTime < r.firstHiddenTime && (i.value = Math.max(e.startTime - u(), 0), i.entries = [e], t());
            });
          },
          c = s("largest-contentful-paint", o);
        if (c) {
          t = d(e, i, _, n.reportAllChanges);
          var m = v(function () {
            z[i.id] || (o(c.takeRecords()), c.disconnect(), z[i.id] = !0, t(!0));
          });
          ["keydown", "click"].forEach(function (e) {
            addEventListener(e, function () {
              return O(m);
            }, {
              once: !0,
              capture: !0
            });
          }), p(m), a(function (r) {
            i = f("LCP"), t = d(e, i, _, n.reportAllChanges), l(function () {
              i.value = performance.now() - r.timeStamp, z[i.id] = !0, t(!0);
            });
          });
        }
      });
    },
    J = [800, 1800],
    K = function e(n) {
      document.prerendering ? C(function () {
        return e(n);
      }) : "complete" !== document.readyState ? addEventListener("load", function () {
        return e(n);
      }, !0) : setTimeout(n, 0);
    },
    Q = function Q(e, n) {
      n = n || {};
      var t = f("TTFB"),
        r = d(e, t, J, n.reportAllChanges);
      K(function () {
        var i = c();
        i && (t.value = Math.max(i.responseStart - u(), 0), t.entries = [i], r(!0), a(function () {
          t = f("TTFB", 0), (r = d(e, t, J, n.reportAllChanges))(!0);
        }));
      });
    };

  function getMetrics() {
    if (window.performance && window.performance.getEntries) {
      var navigationEntries = window.performance.getEntriesByType('navigation');
      if (navigationEntries && navigationEntries[0]) {
        var timingInfo = navigationEntries[0];
        // dns loookup time
        var domainLookupTime = timingInfo.domainLookupEnd - timingInfo.domainLookupStart;
        var connectStart = timingInfo.connectStart;
        // if it's a tls connection, use the secure connection start instead
        if (timingInfo.secureConnectionStart > 0) {
          connectStart = timingInfo.secureConnectionStart;
        }
        // connection negotiation time
        var connectionTime = timingInfo.connectEnd - connectStart;
        // page body size
        var decodedBodySize = timingInfo.decodedBodySize;
        // time to load dom
        var domContentLoadedTime = timingInfo.domContentLoadedEventEnd - timingInfo.domContentLoadedEventStart;
        // time to download the page
        var fetchTime = timingInfo.responseEnd - timingInfo.fetchStart;
        return {
          domainLookupTime: domainLookupTime,
          connectionTime: connectionTime,
          decodedBodySize: decodedBodySize,
          domContentLoadedTime: domContentLoadedTime,
          fetchTime: fetchTime
        };
      }
    }
  }
  function sendMetrics() {
    var metrics = getMetrics();
    if (combined._analytics && combined._analytics.trackIframePerformanceMetrics) {
      combined._analytics.trackIframePerformanceMetrics(metrics);
    }
  }
  function setupWebVitals(timeout) {
    var metrics = {};
    var collectMetrics = function collectMetrics(source) {
      if (Object.keys(metrics).length > 0) {
        console.log("[web-vitals] Sending metrics triggered by: " + source, metrics);
        if (combined._analytics && combined._analytics.trackWebVitals) {
          combined._analytics.trackWebVitals(metrics);
          metrics = {};
        }
      }
    };

    // Report all available metrics whenever the page is backgrounded or unloaded.
    // https://github.com/GoogleChrome/web-vitals/blob/main/README.md#batch-multiple-reports-together
    addEventListener('visibilitychange', function () {
      if (document.visibilityState === 'hidden') {
        collectMetrics('visibility change');
      }
    });
    Promise.all([w, S, G, Q].map(function (collector) {
      return new Promise(function (resolve) {
        collector(function (metric) {
          var name = metric.name,
            value = metric.value;
          // Copied from https://bitbucket.org/atlassian/atlassian-frontend-monorepo/src/master/platform/packages/performance/browser-metrics/src/observer/web-vitals-observer.ts#web-vitals-observer.ts-36
          // round FCP, LCP, TTFB to nearest integer.
          // CLS is rounded to two decimal places.
          var metricName = name.toLowerCase();
          metrics["metric:" + metricName] = metricName === 'cls' ? parseFloat(value.toFixed(2)) : Math.round(value);
          resolve();
        });
      });
    })).then(function () {
      return collectMetrics('all collected');
    });

    // Collect any metrics we can after the timeout, if they haven't already been collected.
    setTimeout(function () {
      return collectMetrics('timeout');
    }, timeout);
  }
  var analytics = {
    sendMetrics: sendMetrics,
    setupWebVitals: setupWebVitals
  };

  var ECOSYSTEM_PFF_GLOBAL_KEY = '__ECOSYSTEM_PLATFORM_FEATURE_FLAGS__';
  var allowedPlatformFeatureFlags = ['platform-visual-refresh-icons'];

  // Sets feature flags in the iframe
  var setPlatformFeatureFlags = function setPlatformFeatureFlags() {
    window[ECOSYSTEM_PFF_GLOBAL_KEY] = {};
    if (combined._featureFlag && combined._featureFlag.getBooleanFeatureFlag) {
      var _loop = function _loop() {
        var flag = _allowedPlatformFeatu[_i];
        combined._featureFlag.getBooleanFeatureFlag(flag).then(function (value) {
          window[ECOSYSTEM_PFF_GLOBAL_KEY][flag] = value;
        }).catch(function (err) {
          console.error(err);
        });
      };
      for (var _i = 0, _allowedPlatformFeatu = allowedPlatformFeatureFlags; _i < _allowedPlatformFeatu.length; _i++) {
        _loop();
      }
    }
  };

  combined._hostModules._dollar = $$1;
  combined._hostModules['inline-dialog'] = combined._hostModules.inlineDialog;
  if (consumerOptions.get('sizeToParent') === true) {
    combined.env && combined.env.sizeToParent(consumerOptions.get('hideFooter') === true);
  } else {
    combined.env && combined.env.hideFooter(consumerOptions.get('hideFooter') === true);
  }
  if (consumerOptions.get('base') === true) {
    combined.env && combined.env.getLocation(function (loc) {
      $$1('head').append({
        tag: 'base',
        href: loc,
        target: '_parent'
      });
    });
  }
  $$1.each(EventsInstance.methods, function (i, method) {
    if (combined._hostModules && combined._hostModules.events) {
      combined._hostModules.events[method] = combined.events[method] = EventsInstance[method].bind(EventsInstance);
      combined._hostModules.events[method + 'Public'] = combined.events[method + 'Public'] = PublicEventsInstance[method].bind(PublicEventsInstance);
    }
  });
  combined.define = deprecate(function () {
    return AMD.define.apply(AMD, arguments);
  }, 'AP.define()', null, '5.0');
  combined.require = deprecate(function () {
    return AMD.require.apply(AMD, arguments);
  }, 'AP.require()', null, '5.0');
  var margin = combined._data.options.isDialog ? '10px 10px 0 10px' : '0';
  if (consumerOptions.get('margin') !== false) {
    var setBodyMargin = function setBodyMargin() {
      if (document.body) {
        document.body.style.setProperty('margin', margin, 'important');
      }
    };
    setBodyMargin(); // Try to set it straight away
    window.addEventListener('DOMContentLoaded', setBodyMargin); // If it doesn't exist now (likely) we can set it later
  }
  combined.Meta = {
    get: Meta.getMeta
  };
  combined.meta = Meta.getMeta;
  combined.localUrl = Meta.localUrl;
  combined._hostModules._util = combined._util = {
    each: _util.each,
    log: _util.log,
    decodeQueryComponent: _util.decodeQueryComponent,
    bind: _util.bind,
    unbind: _util.unbind,
    extend: _util.extend,
    trim: _util.trim,
    debounce: _util.debounce,
    isFunction: _util.isFunction,
    handleError: _util.handleError
  };
  if (combined.defineModule) {
    combined.defineModule('env', {
      resize: function resize(w, h, callback) {
        var iframe = document.getElementById(callback._context.extension_id);
        iframe.style.width = w + (typeof w === 'number' ? 'px' : '');
        iframe.style.height = h + (typeof h === 'number' ? 'px' : '');
      }
    });
  }
  if (combined._data && combined._data.origin) {
    combined.registerAny(function (data, callback) {
      // dialog.close event doesn't have event data
      if (data && data.event && data.sender) {
        PublicEventsInstance._anyListener(data, callback);
      } else {
        EventsInstance._anyListener(data, callback);
      }
    });
  }
  var WEB_VITALS_TIMEOUT = 10000; // 10 seconds
  analytics.setupWebVitals(WEB_VITALS_TIMEOUT);

  // Sets up theme-related event listeners but DOES NOT react to them.
  // The Connect app has to call AP.theming.initializeTheming() to start reacting to theme changes in the host.
  Theming.initializeThemeListeners();
  // Also load in the confluence macro body stylesheet
  loadLegacyTextColorStyles();

  // Reads platform feature flags from the parent and sets them as global variables.
  setPlatformFeatureFlags();

  // gets the global options from the parent iframe (if present) so they can propagate to future sub-iframes.
  ExtensionConfigurationOptionsStore$1.set(combined._data.options.globalOptions);
  if (document.readyState === 'complete') {
    if (typeof window.requestIdleCallback === 'function') {
      window.requestIdleCallback(analytics.sendMetrics, {
        timeout: 1000
      });
    } else {
      analytics.sendMetrics();
    }
  } else {
    window.addEventListener('load', analytics.sendMetrics);
  }

  /**
   * THIS FILE WAS CREATED VIA CODEGEN DO NOT MODIFY {@see http://go/af-codegen}
   * @codegen <<SignedSource::ce98235c36b768192d87afe4e3c81ead>>
   * @codegenCommand yarn build tokens
   */
  var atlassianLight = "\nhtml[data-color-mode=\"light\"][data-theme~=\"light:light\"],\nhtml[data-color-mode=\"dark\"][data-theme~=\"dark:light\"] {\n  color-scheme: light;\n  --ds-text: #172B4D;\n  --ds-text-accent-lime: #4C6B1F;\n  --ds-text-accent-lime-bolder: #37471F;\n  --ds-text-accent-red: #AE2E24;\n  --ds-text-accent-red-bolder: #5D1F1A;\n  --ds-text-accent-orange: #A54800;\n  --ds-text-accent-orange-bolder: #702E00;\n  --ds-text-accent-yellow: #7F5F01;\n  --ds-text-accent-yellow-bolder: #533F04;\n  --ds-text-accent-green: #216E4E;\n  --ds-text-accent-green-bolder: #164B35;\n  --ds-text-accent-teal: #206A83;\n  --ds-text-accent-teal-bolder: #164555;\n  --ds-text-accent-blue: #0055CC;\n  --ds-text-accent-blue-bolder: #09326C;\n  --ds-text-accent-purple: #5E4DB2;\n  --ds-text-accent-purple-bolder: #352C63;\n  --ds-text-accent-magenta: #943D73;\n  --ds-text-accent-magenta-bolder: #50253F;\n  --ds-text-accent-gray: #44546F;\n  --ds-text-accent-gray-bolder: #091E42;\n  --ds-text-disabled: #091E424F;\n  --ds-text-inverse: #FFFFFF;\n  --ds-text-selected: #0C66E4;\n  --ds-text-brand: #0C66E4;\n  --ds-text-danger: #AE2E24;\n  --ds-text-warning: #A54800;\n  --ds-text-warning-inverse: #172B4D;\n  --ds-text-success: #216E4E;\n  --ds-text-discovery: #5E4DB2;\n  --ds-text-information: #0055CC;\n  --ds-text-subtlest: #626F86;\n  --ds-text-subtle: #44546F;\n  --ds-link: #0C66E4;\n  --ds-link-pressed: #0055CC;\n  --ds-link-visited: #5E4DB2;\n  --ds-link-visited-pressed: #352C63;\n  --ds-icon: #44546F;\n  --ds-icon-accent-lime: #6A9A23;\n  --ds-icon-accent-red: #C9372C;\n  --ds-icon-accent-orange: #E56910;\n  --ds-icon-accent-yellow: #B38600;\n  --ds-icon-accent-green: #22A06B;\n  --ds-icon-accent-teal: #2898BD;\n  --ds-icon-accent-blue: #1D7AFC;\n  --ds-icon-accent-purple: #8270DB;\n  --ds-icon-accent-magenta: #CD519D;\n  --ds-icon-accent-gray: #758195;\n  --ds-icon-disabled: #091E424F;\n  --ds-icon-inverse: #FFFFFF;\n  --ds-icon-selected: #0C66E4;\n  --ds-icon-brand: #0C66E4;\n  --ds-icon-danger: #C9372C;\n  --ds-icon-warning: #E56910;\n  --ds-icon-warning-inverse: #172B4D;\n  --ds-icon-success: #22A06B;\n  --ds-icon-discovery: #8270DB;\n  --ds-icon-information: #1D7AFC;\n  --ds-icon-subtle: #626F86;\n  --ds-border: #091E4224;\n  --ds-border-accent-lime: #6A9A23;\n  --ds-border-accent-red: #E2483D;\n  --ds-border-accent-orange: #E56910;\n  --ds-border-accent-yellow: #B38600;\n  --ds-border-accent-green: #22A06B;\n  --ds-border-accent-teal: #2898BD;\n  --ds-border-accent-blue: #1D7AFC;\n  --ds-border-accent-purple: #8270DB;\n  --ds-border-accent-magenta: #CD519D;\n  --ds-border-accent-gray: #758195;\n  --ds-border-disabled: #091E420F;\n  --ds-border-focused: #388BFF;\n  --ds-border-input: #8590A2;\n  --ds-border-inverse: #FFFFFF;\n  --ds-border-selected: #0C66E4;\n  --ds-border-brand: #0C66E4;\n  --ds-border-danger: #E2483D;\n  --ds-border-warning: #E56910;\n  --ds-border-success: #22A06B;\n  --ds-border-discovery: #8270DB;\n  --ds-border-information: #1D7AFC;\n  --ds-border-bold: #758195;\n  --ds-background-accent-lime-subtlest: #EFFFD6;\n  --ds-background-accent-lime-subtlest-hovered: #D3F1A7;\n  --ds-background-accent-lime-subtlest-pressed: #B3DF72;\n  --ds-background-accent-lime-subtler: #D3F1A7;\n  --ds-background-accent-lime-subtler-hovered: #B3DF72;\n  --ds-background-accent-lime-subtler-pressed: #94C748;\n  --ds-background-accent-lime-subtle: #94C748;\n  --ds-background-accent-lime-subtle-hovered: #B3DF72;\n  --ds-background-accent-lime-subtle-pressed: #D3F1A7;\n  --ds-background-accent-lime-bolder: #5B7F24;\n  --ds-background-accent-lime-bolder-hovered: #4C6B1F;\n  --ds-background-accent-lime-bolder-pressed: #37471F;\n  --ds-background-accent-red-subtlest: #FFECEB;\n  --ds-background-accent-red-subtlest-hovered: #FFD5D2;\n  --ds-background-accent-red-subtlest-pressed: #FD9891;\n  --ds-background-accent-red-subtler: #FFD5D2;\n  --ds-background-accent-red-subtler-hovered: #FD9891;\n  --ds-background-accent-red-subtler-pressed: #F87168;\n  --ds-background-accent-red-subtle: #F87168;\n  --ds-background-accent-red-subtle-hovered: #FD9891;\n  --ds-background-accent-red-subtle-pressed: #FFD5D2;\n  --ds-background-accent-red-bolder: #C9372C;\n  --ds-background-accent-red-bolder-hovered: #AE2E24;\n  --ds-background-accent-red-bolder-pressed: #5D1F1A;\n  --ds-background-accent-orange-subtlest: #FFF3EB;\n  --ds-background-accent-orange-subtlest-hovered: #FEDEC8;\n  --ds-background-accent-orange-subtlest-pressed: #FEC195;\n  --ds-background-accent-orange-subtler: #FEDEC8;\n  --ds-background-accent-orange-subtler-hovered: #FEC195;\n  --ds-background-accent-orange-subtler-pressed: #FEA362;\n  --ds-background-accent-orange-subtle: #FEA362;\n  --ds-background-accent-orange-subtle-hovered: #FEC195;\n  --ds-background-accent-orange-subtle-pressed: #FEDEC8;\n  --ds-background-accent-orange-bolder: #C25100;\n  --ds-background-accent-orange-bolder-hovered: #A54800;\n  --ds-background-accent-orange-bolder-pressed: #702E00;\n  --ds-background-accent-yellow-subtlest: #FFF7D6;\n  --ds-background-accent-yellow-subtlest-hovered: #F8E6A0;\n  --ds-background-accent-yellow-subtlest-pressed: #F5CD47;\n  --ds-background-accent-yellow-subtler: #F8E6A0;\n  --ds-background-accent-yellow-subtler-hovered: #F5CD47;\n  --ds-background-accent-yellow-subtler-pressed: #E2B203;\n  --ds-background-accent-yellow-subtle: #F5CD47;\n  --ds-background-accent-yellow-subtle-hovered: #E2B203;\n  --ds-background-accent-yellow-subtle-pressed: #CF9F02;\n  --ds-background-accent-yellow-bolder: #946F00;\n  --ds-background-accent-yellow-bolder-hovered: #7F5F01;\n  --ds-background-accent-yellow-bolder-pressed: #533F04;\n  --ds-background-accent-green-subtlest: #DCFFF1;\n  --ds-background-accent-green-subtlest-hovered: #BAF3DB;\n  --ds-background-accent-green-subtlest-pressed: #7EE2B8;\n  --ds-background-accent-green-subtler: #BAF3DB;\n  --ds-background-accent-green-subtler-hovered: #7EE2B8;\n  --ds-background-accent-green-subtler-pressed: #4BCE97;\n  --ds-background-accent-green-subtle: #4BCE97;\n  --ds-background-accent-green-subtle-hovered: #7EE2B8;\n  --ds-background-accent-green-subtle-pressed: #BAF3DB;\n  --ds-background-accent-green-bolder: #1F845A;\n  --ds-background-accent-green-bolder-hovered: #216E4E;\n  --ds-background-accent-green-bolder-pressed: #164B35;\n  --ds-background-accent-teal-subtlest: #E7F9FF;\n  --ds-background-accent-teal-subtlest-hovered: #C6EDFB;\n  --ds-background-accent-teal-subtlest-pressed: #9DD9EE;\n  --ds-background-accent-teal-subtler: #C6EDFB;\n  --ds-background-accent-teal-subtler-hovered: #9DD9EE;\n  --ds-background-accent-teal-subtler-pressed: #6CC3E0;\n  --ds-background-accent-teal-subtle: #6CC3E0;\n  --ds-background-accent-teal-subtle-hovered: #9DD9EE;\n  --ds-background-accent-teal-subtle-pressed: #C6EDFB;\n  --ds-background-accent-teal-bolder: #227D9B;\n  --ds-background-accent-teal-bolder-hovered: #206A83;\n  --ds-background-accent-teal-bolder-pressed: #164555;\n  --ds-background-accent-blue-subtlest: #E9F2FF;\n  --ds-background-accent-blue-subtlest-hovered: #CCE0FF;\n  --ds-background-accent-blue-subtlest-pressed: #85B8FF;\n  --ds-background-accent-blue-subtler: #CCE0FF;\n  --ds-background-accent-blue-subtler-hovered: #85B8FF;\n  --ds-background-accent-blue-subtler-pressed: #579DFF;\n  --ds-background-accent-blue-subtle: #579DFF;\n  --ds-background-accent-blue-subtle-hovered: #85B8FF;\n  --ds-background-accent-blue-subtle-pressed: #CCE0FF;\n  --ds-background-accent-blue-bolder: #0C66E4;\n  --ds-background-accent-blue-bolder-hovered: #0055CC;\n  --ds-background-accent-blue-bolder-pressed: #09326C;\n  --ds-background-accent-purple-subtlest: #F3F0FF;\n  --ds-background-accent-purple-subtlest-hovered: #DFD8FD;\n  --ds-background-accent-purple-subtlest-pressed: #B8ACF6;\n  --ds-background-accent-purple-subtler: #DFD8FD;\n  --ds-background-accent-purple-subtler-hovered: #B8ACF6;\n  --ds-background-accent-purple-subtler-pressed: #9F8FEF;\n  --ds-background-accent-purple-subtle: #9F8FEF;\n  --ds-background-accent-purple-subtle-hovered: #B8ACF6;\n  --ds-background-accent-purple-subtle-pressed: #DFD8FD;\n  --ds-background-accent-purple-bolder: #6E5DC6;\n  --ds-background-accent-purple-bolder-hovered: #5E4DB2;\n  --ds-background-accent-purple-bolder-pressed: #352C63;\n  --ds-background-accent-magenta-subtlest: #FFECF8;\n  --ds-background-accent-magenta-subtlest-hovered: #FDD0EC;\n  --ds-background-accent-magenta-subtlest-pressed: #F797D2;\n  --ds-background-accent-magenta-subtler: #FDD0EC;\n  --ds-background-accent-magenta-subtler-hovered: #F797D2;\n  --ds-background-accent-magenta-subtler-pressed: #E774BB;\n  --ds-background-accent-magenta-subtle: #E774BB;\n  --ds-background-accent-magenta-subtle-hovered: #F797D2;\n  --ds-background-accent-magenta-subtle-pressed: #FDD0EC;\n  --ds-background-accent-magenta-bolder: #AE4787;\n  --ds-background-accent-magenta-bolder-hovered: #943D73;\n  --ds-background-accent-magenta-bolder-pressed: #50253F;\n  --ds-background-accent-gray-subtlest: #F1F2F4;\n  --ds-background-accent-gray-subtlest-hovered: #DCDFE4;\n  --ds-background-accent-gray-subtlest-pressed: #B3B9C4;\n  --ds-background-accent-gray-subtler: #DCDFE4;\n  --ds-background-accent-gray-subtler-hovered: #B3B9C4;\n  --ds-background-accent-gray-subtler-pressed: #8590A2;\n  --ds-background-accent-gray-subtle: #8590A2;\n  --ds-background-accent-gray-subtle-hovered: #B3B9C4;\n  --ds-background-accent-gray-subtle-pressed: #DCDFE4;\n  --ds-background-accent-gray-bolder: #626F86;\n  --ds-background-accent-gray-bolder-hovered: #44546F;\n  --ds-background-accent-gray-bolder-pressed: #2C3E5D;\n  --ds-background-disabled: #091E4208;\n  --ds-background-input: #FFFFFF;\n  --ds-background-input-hovered: #F7F8F9;\n  --ds-background-input-pressed: #FFFFFF;\n  --ds-background-inverse-subtle: #00000029;\n  --ds-background-inverse-subtle-hovered: #0000003D;\n  --ds-background-inverse-subtle-pressed: #00000052;\n  --ds-background-neutral: #091E420F;\n  --ds-background-neutral-hovered: #091E4224;\n  --ds-background-neutral-pressed: #091E424F;\n  --ds-background-neutral-subtle: #00000000;\n  --ds-background-neutral-subtle-hovered: #091E420F;\n  --ds-background-neutral-subtle-pressed: #091E4224;\n  --ds-background-neutral-bold: #44546F;\n  --ds-background-neutral-bold-hovered: #2C3E5D;\n  --ds-background-neutral-bold-pressed: #172B4D;\n  --ds-background-selected: #E9F2FF;\n  --ds-background-selected-hovered: #CCE0FF;\n  --ds-background-selected-pressed: #85B8FF;\n  --ds-background-selected-bold: #0C66E4;\n  --ds-background-selected-bold-hovered: #0055CC;\n  --ds-background-selected-bold-pressed: #09326C;\n  --ds-background-brand-subtlest: #E9F2FF;\n  --ds-background-brand-subtlest-hovered: #CCE0FF;\n  --ds-background-brand-subtlest-pressed: #85B8FF;\n  --ds-background-brand-bold: #0C66E4;\n  --ds-background-brand-bold-hovered: #0055CC;\n  --ds-background-brand-bold-pressed: #09326C;\n  --ds-background-brand-boldest: #1C2B41;\n  --ds-background-brand-boldest-hovered: #09326C;\n  --ds-background-brand-boldest-pressed: #0055CC;\n  --ds-background-danger: #FFECEB;\n  --ds-background-danger-hovered: #FFD5D2;\n  --ds-background-danger-pressed: #FD9891;\n  --ds-background-danger-bold: #C9372C;\n  --ds-background-danger-bold-hovered: #AE2E24;\n  --ds-background-danger-bold-pressed: #5D1F1A;\n  --ds-background-warning: #FFF7D6;\n  --ds-background-warning-hovered: #F8E6A0;\n  --ds-background-warning-pressed: #F5CD47;\n  --ds-background-warning-bold: #F5CD47;\n  --ds-background-warning-bold-hovered: #E2B203;\n  --ds-background-warning-bold-pressed: #CF9F02;\n  --ds-background-success: #DCFFF1;\n  --ds-background-success-hovered: #BAF3DB;\n  --ds-background-success-pressed: #7EE2B8;\n  --ds-background-success-bold: #1F845A;\n  --ds-background-success-bold-hovered: #216E4E;\n  --ds-background-success-bold-pressed: #164B35;\n  --ds-background-discovery: #F3F0FF;\n  --ds-background-discovery-hovered: #DFD8FD;\n  --ds-background-discovery-pressed: #B8ACF6;\n  --ds-background-discovery-bold: #6E5DC6;\n  --ds-background-discovery-bold-hovered: #5E4DB2;\n  --ds-background-discovery-bold-pressed: #352C63;\n  --ds-background-information: #E9F2FF;\n  --ds-background-information-hovered: #CCE0FF;\n  --ds-background-information-pressed: #85B8FF;\n  --ds-background-information-bold: #0C66E4;\n  --ds-background-information-bold-hovered: #0055CC;\n  --ds-background-information-bold-pressed: #09326C;\n  --ds-blanket: #091E427D;\n  --ds-blanket-selected: #388BFF14;\n  --ds-blanket-danger: #EF5C4814;\n  --ds-interaction-hovered: #00000029;\n  --ds-interaction-pressed: #00000052;\n  --ds-skeleton: #091E420F;\n  --ds-skeleton-subtle: #091E4208;\n  --ds-chart-categorical-1: #2898BD;\n  --ds-chart-categorical-1-hovered: #227D9B;\n  --ds-chart-categorical-2: #5E4DB2;\n  --ds-chart-categorical-2-hovered: #352C63;\n  --ds-chart-categorical-3: #E56910;\n  --ds-chart-categorical-3-hovered: #C25100;\n  --ds-chart-categorical-4: #943D73;\n  --ds-chart-categorical-4-hovered: #50253F;\n  --ds-chart-categorical-5: #09326C;\n  --ds-chart-categorical-5-hovered: #1C2B41;\n  --ds-chart-categorical-6: #8F7EE7;\n  --ds-chart-categorical-6-hovered: #8270DB;\n  --ds-chart-categorical-7: #50253F;\n  --ds-chart-categorical-7-hovered: #3D2232;\n  --ds-chart-categorical-8: #A54800;\n  --ds-chart-categorical-8-hovered: #702E00;\n  --ds-chart-lime-bold: #6A9A23;\n  --ds-chart-lime-bold-hovered: #5B7F24;\n  --ds-chart-lime-bolder: #5B7F24;\n  --ds-chart-lime-bolder-hovered: #4C6B1F;\n  --ds-chart-lime-boldest: #4C6B1F;\n  --ds-chart-lime-boldest-hovered: #37471F;\n  --ds-chart-neutral: #8590A2;\n  --ds-chart-neutral-hovered: #758195;\n  --ds-chart-red-bold: #F15B50;\n  --ds-chart-red-bold-hovered: #E2483D;\n  --ds-chart-red-bolder: #E2483D;\n  --ds-chart-red-bolder-hovered: #C9372C;\n  --ds-chart-red-boldest: #AE2E24;\n  --ds-chart-red-boldest-hovered: #5D1F1A;\n  --ds-chart-orange-bold: #E56910;\n  --ds-chart-orange-bold-hovered: #C25100;\n  --ds-chart-orange-bolder: #C25100;\n  --ds-chart-orange-bolder-hovered: #A54800;\n  --ds-chart-orange-boldest: #A54800;\n  --ds-chart-orange-boldest-hovered: #702E00;\n  --ds-chart-yellow-bold: #B38600;\n  --ds-chart-yellow-bold-hovered: #946F00;\n  --ds-chart-yellow-bolder: #946F00;\n  --ds-chart-yellow-bolder-hovered: #7F5F01;\n  --ds-chart-yellow-boldest: #7F5F01;\n  --ds-chart-yellow-boldest-hovered: #533F04;\n  --ds-chart-green-bold: #22A06B;\n  --ds-chart-green-bold-hovered: #1F845A;\n  --ds-chart-green-bolder: #1F845A;\n  --ds-chart-green-bolder-hovered: #216E4E;\n  --ds-chart-green-boldest: #216E4E;\n  --ds-chart-green-boldest-hovered: #164B35;\n  --ds-chart-teal-bold: #2898BD;\n  --ds-chart-teal-bold-hovered: #227D9B;\n  --ds-chart-teal-bolder: #227D9B;\n  --ds-chart-teal-bolder-hovered: #206A83;\n  --ds-chart-teal-boldest: #206A83;\n  --ds-chart-teal-boldest-hovered: #164555;\n  --ds-chart-blue-bold: #388BFF;\n  --ds-chart-blue-bold-hovered: #1D7AFC;\n  --ds-chart-blue-bolder: #1D7AFC;\n  --ds-chart-blue-bolder-hovered: #0C66E4;\n  --ds-chart-blue-boldest: #0055CC;\n  --ds-chart-blue-boldest-hovered: #09326C;\n  --ds-chart-purple-bold: #8F7EE7;\n  --ds-chart-purple-bold-hovered: #8270DB;\n  --ds-chart-purple-bolder: #8270DB;\n  --ds-chart-purple-bolder-hovered: #6E5DC6;\n  --ds-chart-purple-boldest: #5E4DB2;\n  --ds-chart-purple-boldest-hovered: #352C63;\n  --ds-chart-magenta-bold: #DA62AC;\n  --ds-chart-magenta-bold-hovered: #CD519D;\n  --ds-chart-magenta-bolder: #CD519D;\n  --ds-chart-magenta-bolder-hovered: #AE4787;\n  --ds-chart-magenta-boldest: #943D73;\n  --ds-chart-magenta-boldest-hovered: #50253F;\n  --ds-chart-gray-bold: #8590A2;\n  --ds-chart-gray-bold-hovered: #758195;\n  --ds-chart-gray-bolder: #758195;\n  --ds-chart-gray-bolder-hovered: #626F86;\n  --ds-chart-gray-boldest: #44546F;\n  --ds-chart-gray-boldest-hovered: #2C3E5D;\n  --ds-chart-brand: #1D7AFC;\n  --ds-chart-brand-hovered: #0C66E4;\n  --ds-chart-danger: #F15B50;\n  --ds-chart-danger-hovered: #E2483D;\n  --ds-chart-danger-bold: #AE2E24;\n  --ds-chart-danger-bold-hovered: #5D1F1A;\n  --ds-chart-warning: #B38600;\n  --ds-chart-warning-hovered: #946F00;\n  --ds-chart-warning-bold: #7F5F01;\n  --ds-chart-warning-bold-hovered: #533F04;\n  --ds-chart-success: #22A06B;\n  --ds-chart-success-hovered: #1F845A;\n  --ds-chart-success-bold: #216E4E;\n  --ds-chart-success-bold-hovered: #164B35;\n  --ds-chart-discovery: #8F7EE7;\n  --ds-chart-discovery-hovered: #8270DB;\n  --ds-chart-discovery-bold: #5E4DB2;\n  --ds-chart-discovery-bold-hovered: #352C63;\n  --ds-chart-information: #388BFF;\n  --ds-chart-information-hovered: #1D7AFC;\n  --ds-chart-information-bold: #0055CC;\n  --ds-chart-information-bold-hovered: #09326C;\n  --ds-surface: #FFFFFF;\n  --ds-surface-hovered: #F1F2F4;\n  --ds-surface-pressed: #DCDFE4;\n  --ds-surface-overlay: #FFFFFF;\n  --ds-surface-overlay-hovered: #F1F2F4;\n  --ds-surface-overlay-pressed: #DCDFE4;\n  --ds-surface-raised: #FFFFFF;\n  --ds-surface-raised-hovered: #F1F2F4;\n  --ds-surface-raised-pressed: #DCDFE4;\n  --ds-surface-sunken: #F7F8F9;\n  --ds-shadow-overflow: 0px 0px 8px #091E4229, 0px 0px 1px #091E421F;\n  --ds-shadow-overflow-perimeter: #091e421f;\n  --ds-shadow-overflow-spread: #091e4229;\n  --ds-shadow-overlay: 0px 8px 12px #091E4226, 0px 0px 1px #091E424F;\n  --ds-shadow-raised: 0px 1px 1px #091E4240, 0px 0px 1px #091E424F;\n  --ds-opacity-disabled: 0.4;\n  --ds-opacity-loading: 0.2;\n  --ds-UNSAFE-transparent: transparent;\n  --ds-elevation-surface-current: #FFFFFF;\n}\n";

  var atlassianLight$1 = /*#__PURE__*/Object.freeze({
    __proto__: null,
    'default': atlassianLight
  });

  /**
   * THIS FILE WAS CREATED VIA CODEGEN DO NOT MODIFY {@see http://go/af-codegen}
   * @codegen <<SignedSource::b66f003f52ebad6767e95eaeb646085b>>
   * @codegenCommand yarn build tokens
   */
  var atlassianLightFuture = "\nhtml[data-color-mode=\"light\"][data-theme~=\"light:light\"],\nhtml[data-color-mode=\"dark\"][data-theme~=\"dark:light\"] {\n  color-scheme: light;\n  --ds-background-disabled: #28311B;\n}\n";

  var atlassianLightFuture$1 = /*#__PURE__*/Object.freeze({
    __proto__: null,
    'default': atlassianLightFuture
  });

  /**
   * THIS FILE WAS CREATED VIA CODEGEN DO NOT MODIFY {@see http://go/af-codegen}
   * @codegen <<SignedSource::45944bee0e892c48bba65b33810e0acd>>
   * @codegenCommand yarn build tokens
   */
  var atlassianLightIncreasedContrast = "\nhtml[data-color-mode=\"light\"][data-theme~=\"light:light-increased-contrast\"],\nhtml[data-color-mode=\"dark\"][data-theme~=\"dark:light-increased-contrast\"],\nhtml[data-color-mode=\"light\"][data-contrast-mode=\"more\"][data-theme~=\"light:light\"],\nhtml[data-color-mode=\"dark\"][data-contrast-mode=\"more\"][data-theme~=\"dark:light\"] {\n  color-scheme: light;\n  --ds-text: #091E42;\n  --ds-text-accent-lime: #37471F;\n  --ds-text-accent-lime-bolder: #28311B;\n  --ds-text-accent-red: #5D1F1A;\n  --ds-text-accent-red-bolder: #42221F;\n  --ds-text-accent-orange: #702E00;\n  --ds-text-accent-orange-bolder: #38291E;\n  --ds-text-accent-yellow: #533F04;\n  --ds-text-accent-yellow-bolder: #332E1B;\n  --ds-text-accent-green: #164B35;\n  --ds-text-accent-green-bolder: #1C3329;\n  --ds-text-accent-teal: #164555;\n  --ds-text-accent-teal-bolder: #1E3137;\n  --ds-text-accent-blue: #09326C;\n  --ds-text-accent-blue-bolder: #1C2B41;\n  --ds-text-accent-purple: #352C63;\n  --ds-text-accent-purple-bolder: #2B273F;\n  --ds-text-accent-magenta: #50253F;\n  --ds-text-accent-magenta-bolder: #3D2232;\n  --ds-text-accent-gray: #2C3E5D;\n  --ds-text-accent-gray-bolder: #172B4D;\n  --ds-text-disabled: #091E424F;\n  --ds-text-inverse: #FFFFFF;\n  --ds-text-selected: #09326C;\n  --ds-text-brand: #09326C;\n  --ds-text-danger: #5D1F1A;\n  --ds-text-warning: #702E00;\n  --ds-text-warning-inverse: #FFFFFF;\n  --ds-text-success: #164B35;\n  --ds-text-discovery: #352C63;\n  --ds-text-information: #09326C;\n  --ds-text-subtlest: #2C3E5D;\n  --ds-text-subtle: #172B4D;\n  --ds-link: #09326C;\n  --ds-link-pressed: #09326C;\n  --ds-link-visited: #5E4DB2;\n  --ds-link-visited-pressed: #352C63;\n  --ds-icon: #172B4D;\n  --ds-icon-accent-lime: #4C6B1F;\n  --ds-icon-accent-red: #AE2E24;\n  --ds-icon-accent-orange: #A54800;\n  --ds-icon-accent-yellow: #7F5F01;\n  --ds-icon-accent-green: #216E4E;\n  --ds-icon-accent-teal: #206A83;\n  --ds-icon-accent-blue: #0055CC;\n  --ds-icon-accent-purple: #5E4DB2;\n  --ds-icon-accent-magenta: #943D73;\n  --ds-icon-accent-gray: #44546F;\n  --ds-icon-disabled: #091E424F;\n  --ds-icon-inverse: #FFFFFF;\n  --ds-icon-selected: #0055CC;\n  --ds-icon-brand: #0055CC;\n  --ds-icon-danger: #C9372C;\n  --ds-icon-warning: #A54800;\n  --ds-icon-warning-inverse: #FFFFFF;\n  --ds-icon-success: #216E4E;\n  --ds-icon-discovery: #6E5DC6;\n  --ds-icon-information: #0055CC;\n  --ds-icon-subtle: #2C3E5D;\n  --ds-border: #9BB4CA80;\n  --ds-border-accent-lime: #4C6B1F;\n  --ds-border-accent-red: #AE2E24;\n  --ds-border-accent-orange: #A54800;\n  --ds-border-accent-yellow: #7F5F01;\n  --ds-border-accent-green: #216E4E;\n  --ds-border-accent-teal: #206A83;\n  --ds-border-accent-blue: #0055CC;\n  --ds-border-accent-purple: #5E4DB2;\n  --ds-border-accent-magenta: #943D73;\n  --ds-border-accent-gray: #44546F;\n  --ds-border-disabled: #091E4224;\n  --ds-border-focused: #0055CC;\n  --ds-border-input: #44546F;\n  --ds-border-inverse: #FFFFFF;\n  --ds-border-selected: #0055CC;\n  --ds-border-brand: #0055CC;\n  --ds-border-danger: #C9372C;\n  --ds-border-warning: #A54800;\n  --ds-border-success: #216E4E;\n  --ds-border-discovery: #6E5DC6;\n  --ds-border-information: #0055CC;\n  --ds-border-bold: #44546F;\n  --ds-background-accent-lime-subtlest: #EFFFD6;\n  --ds-background-accent-lime-subtlest-hovered: #D3F1A7;\n  --ds-background-accent-lime-subtlest-pressed: #B3DF72;\n  --ds-background-accent-lime-subtler: #D3F1A7;\n  --ds-background-accent-lime-subtler-hovered: #B3DF72;\n  --ds-background-accent-lime-subtler-pressed: #94C748;\n  --ds-background-accent-lime-subtle: #B3DF72;\n  --ds-background-accent-lime-subtle-hovered: #D3F1A7;\n  --ds-background-accent-lime-subtle-pressed: #EFFFD6;\n  --ds-background-accent-lime-bolder: #37471F;\n  --ds-background-accent-lime-bolder-hovered: #28311B;\n  --ds-background-accent-lime-bolder-pressed: #233013;\n  --ds-background-accent-red-subtlest: #FFECEB;\n  --ds-background-accent-red-subtlest-hovered: #FFD5D2;\n  --ds-background-accent-red-subtlest-pressed: #FD9891;\n  --ds-background-accent-red-subtler: #FFD5D2;\n  --ds-background-accent-red-subtler-hovered: #FD9891;\n  --ds-background-accent-red-subtler-pressed: #F87168;\n  --ds-background-accent-red-subtle: #fbb7ae;\n  --ds-background-accent-red-subtle-hovered: #FFD5D2;\n  --ds-background-accent-red-subtle-pressed: #FFECEB;\n  --ds-background-accent-red-bolder: #5D1F1A;\n  --ds-background-accent-red-bolder-hovered: #42221F;\n  --ds-background-accent-red-bolder-pressed: #49120C;\n  --ds-background-accent-orange-subtlest: #FFF3EB;\n  --ds-background-accent-orange-subtlest-hovered: #FEDEC8;\n  --ds-background-accent-orange-subtlest-pressed: #FEC195;\n  --ds-background-accent-orange-subtler: #FEDEC8;\n  --ds-background-accent-orange-subtler-hovered: #FEC195;\n  --ds-background-accent-orange-subtler-pressed: #FEA362;\n  --ds-background-accent-orange-subtle: #FEC195;\n  --ds-background-accent-orange-subtle-hovered: #FEDEC8;\n  --ds-background-accent-orange-subtle-pressed: #FFF3EB;\n  --ds-background-accent-orange-bolder: #702E00;\n  --ds-background-accent-orange-bolder-hovered: #38291E;\n  --ds-background-accent-orange-bolder-pressed: #3E2108;\n  --ds-background-accent-yellow-subtlest: #FFF7D6;\n  --ds-background-accent-yellow-subtlest-hovered: #F8E6A0;\n  --ds-background-accent-yellow-subtlest-pressed: #F5CD47;\n  --ds-background-accent-yellow-subtler: #F8E6A0;\n  --ds-background-accent-yellow-subtler-hovered: #F5CD47;\n  --ds-background-accent-yellow-subtler-pressed: #E2B203;\n  --ds-background-accent-yellow-subtle: #F5CD47;\n  --ds-background-accent-yellow-subtle-hovered: #F8E6A0;\n  --ds-background-accent-yellow-subtle-pressed: #FFF7D6;\n  --ds-background-accent-yellow-bolder: #533F04;\n  --ds-background-accent-yellow-bolder-hovered: #332E1B;\n  --ds-background-accent-yellow-bolder-pressed: #342800;\n  --ds-background-accent-green-subtlest: #DCFFF1;\n  --ds-background-accent-green-subtlest-hovered: #BAF3DB;\n  --ds-background-accent-green-subtlest-pressed: #7EE2B8;\n  --ds-background-accent-green-subtler: #BAF3DB;\n  --ds-background-accent-green-subtler-hovered: #7EE2B8;\n  --ds-background-accent-green-subtler-pressed: #4BCE97;\n  --ds-background-accent-green-subtle: #7EE2B8;\n  --ds-background-accent-green-subtle-hovered: #BAF3DB;\n  --ds-background-accent-green-subtle-pressed: #DCFFF1;\n  --ds-background-accent-green-bolder: #164B35;\n  --ds-background-accent-green-bolder-hovered: #1C3329;\n  --ds-background-accent-green-bolder-pressed: #0F3324;\n  --ds-background-accent-teal-subtlest: #E7F9FF;\n  --ds-background-accent-teal-subtlest-hovered: #C6EDFB;\n  --ds-background-accent-teal-subtlest-pressed: #9DD9EE;\n  --ds-background-accent-teal-subtler: #C6EDFB;\n  --ds-background-accent-teal-subtler-hovered: #9DD9EE;\n  --ds-background-accent-teal-subtler-pressed: #6CC3E0;\n  --ds-background-accent-teal-subtle: #9DD9EE;\n  --ds-background-accent-teal-subtle-hovered: #C6EDFB;\n  --ds-background-accent-teal-subtle-pressed: #E7F9FF;\n  --ds-background-accent-teal-bolder: #164555;\n  --ds-background-accent-teal-bolder-hovered: #1E3137;\n  --ds-background-accent-teal-bolder-pressed: #103034;\n  --ds-background-accent-blue-subtlest: #E9F2FF;\n  --ds-background-accent-blue-subtlest-hovered: #CCE0FF;\n  --ds-background-accent-blue-subtlest-pressed: #85B8FF;\n  --ds-background-accent-blue-subtler: #CCE0FF;\n  --ds-background-accent-blue-subtler-hovered: #85B8FF;\n  --ds-background-accent-blue-subtler-pressed: #579DFF;\n  --ds-background-accent-blue-subtle: #85B8FF;\n  --ds-background-accent-blue-subtle-hovered: #CCE0FF;\n  --ds-background-accent-blue-subtle-pressed: #E9F2FF;\n  --ds-background-accent-blue-bolder: #09326C;\n  --ds-background-accent-blue-bolder-hovered: #1C2B41;\n  --ds-background-accent-blue-bolder-pressed: #022353;\n  --ds-background-accent-purple-subtlest: #F3F0FF;\n  --ds-background-accent-purple-subtlest-hovered: #DFD8FD;\n  --ds-background-accent-purple-subtlest-pressed: #B8ACF6;\n  --ds-background-accent-purple-subtler: #DFD8FD;\n  --ds-background-accent-purple-subtler-hovered: #B8ACF6;\n  --ds-background-accent-purple-subtler-pressed: #9F8FEF;\n  --ds-background-accent-purple-subtle: #c3b9fa;\n  --ds-background-accent-purple-subtle-hovered: #DFD8FD;\n  --ds-background-accent-purple-subtle-pressed: #F3F0FF;\n  --ds-background-accent-purple-bolder: #352C63;\n  --ds-background-accent-purple-bolder-hovered: #2B273F;\n  --ds-background-accent-purple-bolder-pressed: #211A47;\n  --ds-background-accent-magenta-subtlest: #FFECF8;\n  --ds-background-accent-magenta-subtlest-hovered: #FDD0EC;\n  --ds-background-accent-magenta-subtlest-pressed: #F797D2;\n  --ds-background-accent-magenta-subtler: #FDD0EC;\n  --ds-background-accent-magenta-subtler-hovered: #F797D2;\n  --ds-background-accent-magenta-subtler-pressed: #E774BB;\n  --ds-background-accent-magenta-subtle: #f2a6d4;\n  --ds-background-accent-magenta-subtle-hovered: #FDD0EC;\n  --ds-background-accent-magenta-subtle-pressed: #FFECF8;\n  --ds-background-accent-magenta-bolder: #50253F;\n  --ds-background-accent-magenta-bolder-hovered: #3D2232;\n  --ds-background-accent-magenta-bolder-pressed: #37172A;\n  --ds-background-accent-gray-subtlest: #F1F2F4;\n  --ds-background-accent-gray-subtlest-hovered: #DCDFE4;\n  --ds-background-accent-gray-subtlest-pressed: #B3B9C4;\n  --ds-background-accent-gray-subtler: #DCDFE4;\n  --ds-background-accent-gray-subtler-hovered: #B3B9C4;\n  --ds-background-accent-gray-subtler-pressed: #8590A2;\n  --ds-background-accent-gray-subtle: #B3B9C4;\n  --ds-background-accent-gray-subtle-hovered: #DCDFE4;\n  --ds-background-accent-gray-subtle-pressed: #F1F2F4;\n  --ds-background-accent-gray-bolder: #44546F;\n  --ds-background-accent-gray-bolder-hovered: #2C3E5D;\n  --ds-background-accent-gray-bolder-pressed: #172B4D;\n  --ds-background-disabled: #091E4208;\n  --ds-background-input: #FFFFFF;\n  --ds-background-input-hovered: #F7F8F9;\n  --ds-background-input-pressed: #FFFFFF;\n  --ds-background-inverse-subtle: #00000029;\n  --ds-background-inverse-subtle-hovered: #0000003D;\n  --ds-background-inverse-subtle-pressed: #00000052;\n  --ds-background-neutral: #091E420F;\n  --ds-background-neutral-hovered: #091E4224;\n  --ds-background-neutral-pressed: #091E424F;\n  --ds-background-neutral-subtle: #00000000;\n  --ds-background-neutral-subtle-hovered: #091E420F;\n  --ds-background-neutral-subtle-pressed: #091E4224;\n  --ds-background-neutral-bold: #44546F;\n  --ds-background-neutral-bold-hovered: #2C3E5D;\n  --ds-background-neutral-bold-pressed: #172B4D;\n  --ds-background-selected: #E9F2FF;\n  --ds-background-selected-hovered: #CCE0FF;\n  --ds-background-selected-pressed: #85B8FF;\n  --ds-background-selected-bold: #09326C;\n  --ds-background-selected-bold-hovered: #1C2B41;\n  --ds-background-selected-bold-pressed: #022353;\n  --ds-background-brand-subtlest: #E9F2FF;\n  --ds-background-brand-subtlest-hovered: #CCE0FF;\n  --ds-background-brand-subtlest-pressed: #85B8FF;\n  --ds-background-brand-bold: #09326C;\n  --ds-background-brand-bold-hovered: #1C2B41;\n  --ds-background-brand-bold-pressed: #022353;\n  --ds-background-brand-boldest: #1C2B41;\n  --ds-background-brand-boldest-hovered: #09326C;\n  --ds-background-brand-boldest-pressed: #0055CC;\n  --ds-background-danger: #FFECEB;\n  --ds-background-danger-hovered: #FFD5D2;\n  --ds-background-danger-pressed: #FD9891;\n  --ds-background-danger-bold: #5D1F1A;\n  --ds-background-danger-bold-hovered: #42221F;\n  --ds-background-danger-bold-pressed: #49120C;\n  --ds-background-warning: #FFF7D6;\n  --ds-background-warning-hovered: #F8E6A0;\n  --ds-background-warning-pressed: #F5CD47;\n  --ds-background-warning-bold: #533F04;\n  --ds-background-warning-bold-hovered: #332E1B;\n  --ds-background-warning-bold-pressed: #342800;\n  --ds-background-success: #DCFFF1;\n  --ds-background-success-hovered: #BAF3DB;\n  --ds-background-success-pressed: #7EE2B8;\n  --ds-background-success-bold: #164B35;\n  --ds-background-success-bold-hovered: #1C3329;\n  --ds-background-success-bold-pressed: #0F3324;\n  --ds-background-discovery: #F3F0FF;\n  --ds-background-discovery-hovered: #DFD8FD;\n  --ds-background-discovery-pressed: #B8ACF6;\n  --ds-background-discovery-bold: #352C63;\n  --ds-background-discovery-bold-hovered: #2B273F;\n  --ds-background-discovery-bold-pressed: #211A47;\n  --ds-background-information: #E9F2FF;\n  --ds-background-information-hovered: #CCE0FF;\n  --ds-background-information-pressed: #85B8FF;\n  --ds-background-information-bold: #09326C;\n  --ds-background-information-bold-hovered: #1C2B41;\n  --ds-background-information-bold-pressed: #022353;\n  --ds-blanket: #091E427D;\n  --ds-blanket-selected: #388BFF14;\n  --ds-blanket-danger: #EF5C4814;\n  --ds-interaction-hovered: #00000029;\n  --ds-interaction-pressed: #00000052;\n  --ds-skeleton: #091E420F;\n  --ds-skeleton-subtle: #091E4208;\n  --ds-chart-categorical-1: #206A83;\n  --ds-chart-categorical-1-hovered: #164555;\n  --ds-chart-categorical-2: #5E4DB2;\n  --ds-chart-categorical-2-hovered: #352C63;\n  --ds-chart-categorical-3: #A54800;\n  --ds-chart-categorical-3-hovered: #702E00;\n  --ds-chart-categorical-4: #943D73;\n  --ds-chart-categorical-4-hovered: #50253F;\n  --ds-chart-categorical-5: #09326C;\n  --ds-chart-categorical-5-hovered: #1C2B41;\n  --ds-chart-categorical-6: #5E4DB2;\n  --ds-chart-categorical-6-hovered: #352C63;\n  --ds-chart-categorical-7: #50253F;\n  --ds-chart-categorical-7-hovered: #3D2232;\n  --ds-chart-categorical-8: #A54800;\n  --ds-chart-categorical-8-hovered: #702E00;\n  --ds-chart-lime-bold: #4C6B1F;\n  --ds-chart-lime-bold-hovered: #37471F;\n  --ds-chart-lime-bolder: #37471F;\n  --ds-chart-lime-bolder-hovered: #28311B;\n  --ds-chart-lime-boldest: #28311B;\n  --ds-chart-lime-boldest-hovered: #233013;\n  --ds-chart-neutral: #626F86;\n  --ds-chart-neutral-hovered: #44546F;\n  --ds-chart-red-bold: #AE2E24;\n  --ds-chart-red-bold-hovered: #5D1F1A;\n  --ds-chart-red-bolder: #5D1F1A;\n  --ds-chart-red-bolder-hovered: #42221F;\n  --ds-chart-red-boldest: #42221F;\n  --ds-chart-red-boldest-hovered: #49120C;\n  --ds-chart-orange-bold: #A54800;\n  --ds-chart-orange-bold-hovered: #702E00;\n  --ds-chart-orange-bolder: #702E00;\n  --ds-chart-orange-bolder-hovered: #38291E;\n  --ds-chart-orange-boldest: #38291E;\n  --ds-chart-orange-boldest-hovered: #3E2108;\n  --ds-chart-yellow-bold: #7F5F01;\n  --ds-chart-yellow-bold-hovered: #533F04;\n  --ds-chart-yellow-bolder: #533F04;\n  --ds-chart-yellow-bolder-hovered: #332E1B;\n  --ds-chart-yellow-boldest: #332E1B;\n  --ds-chart-yellow-boldest-hovered: #342800;\n  --ds-chart-green-bold: #216E4E;\n  --ds-chart-green-bold-hovered: #164B35;\n  --ds-chart-green-bolder: #164B35;\n  --ds-chart-green-bolder-hovered: #1C3329;\n  --ds-chart-green-boldest: #1C3329;\n  --ds-chart-green-boldest-hovered: #0F3324;\n  --ds-chart-teal-bold: #206A83;\n  --ds-chart-teal-bold-hovered: #164555;\n  --ds-chart-teal-bolder: #164555;\n  --ds-chart-teal-bolder-hovered: #1E3137;\n  --ds-chart-teal-boldest: #1E3137;\n  --ds-chart-teal-boldest-hovered: #103034;\n  --ds-chart-blue-bold: #0055CC;\n  --ds-chart-blue-bold-hovered: #09326C;\n  --ds-chart-blue-bolder: #09326C;\n  --ds-chart-blue-bolder-hovered: #1C2B41;\n  --ds-chart-blue-boldest: #1C2B41;\n  --ds-chart-blue-boldest-hovered: #022353;\n  --ds-chart-purple-bold: #5E4DB2;\n  --ds-chart-purple-bold-hovered: #352C63;\n  --ds-chart-purple-bolder: #352C63;\n  --ds-chart-purple-bolder-hovered: #2B273F;\n  --ds-chart-purple-boldest: #2B273F;\n  --ds-chart-purple-boldest-hovered: #211A47;\n  --ds-chart-magenta-bold: #943D73;\n  --ds-chart-magenta-bold-hovered: #50253F;\n  --ds-chart-magenta-bolder: #50253F;\n  --ds-chart-magenta-bolder-hovered: #3D2232;\n  --ds-chart-magenta-boldest: #3D2232;\n  --ds-chart-magenta-boldest-hovered: #37172A;\n  --ds-chart-gray-bold: #626F86;\n  --ds-chart-gray-bold-hovered: #44546F;\n  --ds-chart-gray-bolder: #44546F;\n  --ds-chart-gray-bolder-hovered: #2C3E5D;\n  --ds-chart-gray-boldest: #2C3E5D;\n  --ds-chart-gray-boldest-hovered: #172B4D;\n  --ds-chart-brand: #0C66E4;\n  --ds-chart-brand-hovered: #0055CC;\n  --ds-chart-danger: #C9372C;\n  --ds-chart-danger-hovered: #AE2E24;\n  --ds-chart-danger-bold: #5D1F1A;\n  --ds-chart-danger-bold-hovered: #42221F;\n  --ds-chart-warning: #7F5F01;\n  --ds-chart-warning-hovered: #533F04;\n  --ds-chart-warning-bold: #533F04;\n  --ds-chart-warning-bold-hovered: #332E1B;\n  --ds-chart-success: #216E4E;\n  --ds-chart-success-hovered: #164B35;\n  --ds-chart-success-bold: #164B35;\n  --ds-chart-success-bold-hovered: #1C3329;\n  --ds-chart-discovery: #6E5DC6;\n  --ds-chart-discovery-hovered: #5E4DB2;\n  --ds-chart-discovery-bold: #352C63;\n  --ds-chart-discovery-bold-hovered: #2B273F;\n  --ds-chart-information: #0C66E4;\n  --ds-chart-information-hovered: #0055CC;\n  --ds-chart-information-bold: #09326C;\n  --ds-chart-information-bold-hovered: #1C2B41;\n  --ds-surface: #FFFFFF;\n  --ds-surface-hovered: #F1F2F4;\n  --ds-surface-pressed: #DCDFE4;\n  --ds-surface-overlay: #FFFFFF;\n  --ds-surface-overlay-hovered: #F1F2F4;\n  --ds-surface-overlay-pressed: #DCDFE4;\n  --ds-surface-raised: #FFFFFF;\n  --ds-surface-raised-hovered: #F1F2F4;\n  --ds-surface-raised-pressed: #DCDFE4;\n  --ds-surface-sunken: #FCFDFE;\n  --ds-shadow-overflow: 0px 0px 8px #091E4229;\n  --ds-shadow-overflow-perimeter: #091e421f;\n  --ds-shadow-overflow-spread: #091e4229;\n  --ds-shadow-overlay: inset 0px 0px 0px 1px #091E4280;\n  --ds-shadow-raised: inset 0px 0px 0px 1px #091E4280;\n  --ds-opacity-disabled: 0.4;\n  --ds-opacity-loading: 0.2;\n  --ds-UNSAFE-transparent: transparent;\n  --ds-elevation-surface-current: #FFFFFF;\n}\n";

  var atlassianLightIncreasedContrast$1 = /*#__PURE__*/Object.freeze({
    __proto__: null,
    'default': atlassianLightIncreasedContrast
  });

  /**
   * THIS FILE WAS CREATED VIA CODEGEN DO NOT MODIFY {@see http://go/af-codegen}
   * @codegen <<SignedSource::bedf13dfb458956bcc31c32e7a96df13>>
   * @codegenCommand yarn build tokens
   */
  var atlassianDark = "\nhtml[data-color-mode=\"light\"][data-theme~=\"light:dark\"],\nhtml[data-color-mode=\"dark\"][data-theme~=\"dark:dark\"] {\n  color-scheme: dark;\n  --ds-text: #B6C2CF;\n  --ds-text-accent-lime: #B3DF72;\n  --ds-text-accent-lime-bolder: #D3F1A7;\n  --ds-text-accent-red: #FD9891;\n  --ds-text-accent-red-bolder: #FFD5D2;\n  --ds-text-accent-orange: #FEC195;\n  --ds-text-accent-orange-bolder: #FEDEC8;\n  --ds-text-accent-yellow: #F5CD47;\n  --ds-text-accent-yellow-bolder: #F8E6A0;\n  --ds-text-accent-green: #7EE2B8;\n  --ds-text-accent-green-bolder: #BAF3DB;\n  --ds-text-accent-teal: #9DD9EE;\n  --ds-text-accent-teal-bolder: #C6EDFB;\n  --ds-text-accent-blue: #85B8FF;\n  --ds-text-accent-blue-bolder: #CCE0FF;\n  --ds-text-accent-purple: #B8ACF6;\n  --ds-text-accent-purple-bolder: #DFD8FD;\n  --ds-text-accent-magenta: #F797D2;\n  --ds-text-accent-magenta-bolder: #FDD0EC;\n  --ds-text-accent-gray: #9FADBC;\n  --ds-text-accent-gray-bolder: #DEE4EA;\n  --ds-text-disabled: #BFDBF847;\n  --ds-text-inverse: #1D2125;\n  --ds-text-selected: #579DFF;\n  --ds-text-brand: #579DFF;\n  --ds-text-danger: #FD9891;\n  --ds-text-warning: #F5CD47;\n  --ds-text-warning-inverse: #1D2125;\n  --ds-text-success: #7EE2B8;\n  --ds-text-discovery: #B8ACF6;\n  --ds-text-information: #85B8FF;\n  --ds-text-subtlest: #8C9BAB;\n  --ds-text-subtle: #9FADBC;\n  --ds-link: #579DFF;\n  --ds-link-pressed: #85B8FF;\n  --ds-link-visited: #B8ACF6;\n  --ds-link-visited-pressed: #DFD8FD;\n  --ds-icon: #9FADBC;\n  --ds-icon-accent-lime: #82B536;\n  --ds-icon-accent-red: #E2483D;\n  --ds-icon-accent-orange: #F38A3F;\n  --ds-icon-accent-yellow: #F5CD47;\n  --ds-icon-accent-green: #2ABB7F;\n  --ds-icon-accent-teal: #42B2D7;\n  --ds-icon-accent-blue: #388BFF;\n  --ds-icon-accent-purple: #8F7EE7;\n  --ds-icon-accent-magenta: #DA62AC;\n  --ds-icon-accent-gray: #738496;\n  --ds-icon-disabled: #BFDBF847;\n  --ds-icon-inverse: #1D2125;\n  --ds-icon-selected: #579DFF;\n  --ds-icon-brand: #579DFF;\n  --ds-icon-danger: #F15B50;\n  --ds-icon-warning: #F5CD47;\n  --ds-icon-warning-inverse: #1D2125;\n  --ds-icon-success: #2ABB7F;\n  --ds-icon-discovery: #8F7EE7;\n  --ds-icon-information: #388BFF;\n  --ds-icon-subtle: #8C9BAB;\n  --ds-border: #A6C5E229;\n  --ds-border-accent-lime: #82B536;\n  --ds-border-accent-red: #F15B50;\n  --ds-border-accent-orange: #F38A3F;\n  --ds-border-accent-yellow: #CF9F02;\n  --ds-border-accent-green: #2ABB7F;\n  --ds-border-accent-teal: #42B2D7;\n  --ds-border-accent-blue: #388BFF;\n  --ds-border-accent-purple: #8F7EE7;\n  --ds-border-accent-magenta: #DA62AC;\n  --ds-border-accent-gray: #738496;\n  --ds-border-disabled: #A1BDD914;\n  --ds-border-focused: #85B8FF;\n  --ds-border-input: #738496;\n  --ds-border-inverse: #161A1D;\n  --ds-border-selected: #579DFF;\n  --ds-border-brand: #579DFF;\n  --ds-border-danger: #F15B50;\n  --ds-border-warning: #CF9F02;\n  --ds-border-success: #2ABB7F;\n  --ds-border-discovery: #8F7EE7;\n  --ds-border-information: #388BFF;\n  --ds-border-bold: #738496;\n  --ds-background-accent-lime-subtlest: #28311B;\n  --ds-background-accent-lime-subtlest-hovered: #37471F;\n  --ds-background-accent-lime-subtlest-pressed: #4C6B1F;\n  --ds-background-accent-lime-subtler: #37471F;\n  --ds-background-accent-lime-subtler-hovered: #4C6B1F;\n  --ds-background-accent-lime-subtler-pressed: #5B7F24;\n  --ds-background-accent-lime-subtle: #4C6B1F;\n  --ds-background-accent-lime-subtle-hovered: #37471F;\n  --ds-background-accent-lime-subtle-pressed: #28311B;\n  --ds-background-accent-lime-bolder: #94C748;\n  --ds-background-accent-lime-bolder-hovered: #B3DF72;\n  --ds-background-accent-lime-bolder-pressed: #D3F1A7;\n  --ds-background-accent-red-subtlest: #42221F;\n  --ds-background-accent-red-subtlest-hovered: #5D1F1A;\n  --ds-background-accent-red-subtlest-pressed: #AE2E24;\n  --ds-background-accent-red-subtler: #5D1F1A;\n  --ds-background-accent-red-subtler-hovered: #AE2E24;\n  --ds-background-accent-red-subtler-pressed: #C9372C;\n  --ds-background-accent-red-subtle: #AE2E24;\n  --ds-background-accent-red-subtle-hovered: #5D1F1A;\n  --ds-background-accent-red-subtle-pressed: #42221F;\n  --ds-background-accent-red-bolder: #F87168;\n  --ds-background-accent-red-bolder-hovered: #FD9891;\n  --ds-background-accent-red-bolder-pressed: #FFD5D2;\n  --ds-background-accent-orange-subtlest: #38291E;\n  --ds-background-accent-orange-subtlest-hovered: #702E00;\n  --ds-background-accent-orange-subtlest-pressed: #A54800;\n  --ds-background-accent-orange-subtler: #702E00;\n  --ds-background-accent-orange-subtler-hovered: #A54800;\n  --ds-background-accent-orange-subtler-pressed: #C25100;\n  --ds-background-accent-orange-subtle: #A54800;\n  --ds-background-accent-orange-subtle-hovered: #702E00;\n  --ds-background-accent-orange-subtle-pressed: #38291E;\n  --ds-background-accent-orange-bolder: #FEA362;\n  --ds-background-accent-orange-bolder-hovered: #FEC195;\n  --ds-background-accent-orange-bolder-pressed: #FEDEC8;\n  --ds-background-accent-yellow-subtlest: #332E1B;\n  --ds-background-accent-yellow-subtlest-hovered: #533F04;\n  --ds-background-accent-yellow-subtlest-pressed: #7F5F01;\n  --ds-background-accent-yellow-subtler: #533F04;\n  --ds-background-accent-yellow-subtler-hovered: #7F5F01;\n  --ds-background-accent-yellow-subtler-pressed: #946F00;\n  --ds-background-accent-yellow-subtle: #7F5F01;\n  --ds-background-accent-yellow-subtle-hovered: #533F04;\n  --ds-background-accent-yellow-subtle-pressed: #332E1B;\n  --ds-background-accent-yellow-bolder: #E2B203;\n  --ds-background-accent-yellow-bolder-hovered: #F5CD47;\n  --ds-background-accent-yellow-bolder-pressed: #F8E6A0;\n  --ds-background-accent-green-subtlest: #1C3329;\n  --ds-background-accent-green-subtlest-hovered: #164B35;\n  --ds-background-accent-green-subtlest-pressed: #216E4E;\n  --ds-background-accent-green-subtler: #164B35;\n  --ds-background-accent-green-subtler-hovered: #216E4E;\n  --ds-background-accent-green-subtler-pressed: #1F845A;\n  --ds-background-accent-green-subtle: #216E4E;\n  --ds-background-accent-green-subtle-hovered: #164B35;\n  --ds-background-accent-green-subtle-pressed: #1C3329;\n  --ds-background-accent-green-bolder: #4BCE97;\n  --ds-background-accent-green-bolder-hovered: #7EE2B8;\n  --ds-background-accent-green-bolder-pressed: #BAF3DB;\n  --ds-background-accent-teal-subtlest: #1E3137;\n  --ds-background-accent-teal-subtlest-hovered: #164555;\n  --ds-background-accent-teal-subtlest-pressed: #206A83;\n  --ds-background-accent-teal-subtler: #164555;\n  --ds-background-accent-teal-subtler-hovered: #206A83;\n  --ds-background-accent-teal-subtler-pressed: #227D9B;\n  --ds-background-accent-teal-subtle: #206A83;\n  --ds-background-accent-teal-subtle-hovered: #164555;\n  --ds-background-accent-teal-subtle-pressed: #1E3137;\n  --ds-background-accent-teal-bolder: #6CC3E0;\n  --ds-background-accent-teal-bolder-hovered: #9DD9EE;\n  --ds-background-accent-teal-bolder-pressed: #C6EDFB;\n  --ds-background-accent-blue-subtlest: #1C2B41;\n  --ds-background-accent-blue-subtlest-hovered: #09326C;\n  --ds-background-accent-blue-subtlest-pressed: #0055CC;\n  --ds-background-accent-blue-subtler: #09326C;\n  --ds-background-accent-blue-subtler-hovered: #0055CC;\n  --ds-background-accent-blue-subtler-pressed: #0C66E4;\n  --ds-background-accent-blue-subtle: #0055CC;\n  --ds-background-accent-blue-subtle-hovered: #09326C;\n  --ds-background-accent-blue-subtle-pressed: #1C2B41;\n  --ds-background-accent-blue-bolder: #579DFF;\n  --ds-background-accent-blue-bolder-hovered: #85B8FF;\n  --ds-background-accent-blue-bolder-pressed: #CCE0FF;\n  --ds-background-accent-purple-subtlest: #2B273F;\n  --ds-background-accent-purple-subtlest-hovered: #352C63;\n  --ds-background-accent-purple-subtlest-pressed: #5E4DB2;\n  --ds-background-accent-purple-subtler: #352C63;\n  --ds-background-accent-purple-subtler-hovered: #5E4DB2;\n  --ds-background-accent-purple-subtler-pressed: #6E5DC6;\n  --ds-background-accent-purple-subtle: #5E4DB2;\n  --ds-background-accent-purple-subtle-hovered: #352C63;\n  --ds-background-accent-purple-subtle-pressed: #2B273F;\n  --ds-background-accent-purple-bolder: #9F8FEF;\n  --ds-background-accent-purple-bolder-hovered: #B8ACF6;\n  --ds-background-accent-purple-bolder-pressed: #DFD8FD;\n  --ds-background-accent-magenta-subtlest: #3D2232;\n  --ds-background-accent-magenta-subtlest-hovered: #50253F;\n  --ds-background-accent-magenta-subtlest-pressed: #943D73;\n  --ds-background-accent-magenta-subtler: #50253F;\n  --ds-background-accent-magenta-subtler-hovered: #943D73;\n  --ds-background-accent-magenta-subtler-pressed: #AE4787;\n  --ds-background-accent-magenta-subtle: #943D73;\n  --ds-background-accent-magenta-subtle-hovered: #50253F;\n  --ds-background-accent-magenta-subtle-pressed: #3D2232;\n  --ds-background-accent-magenta-bolder: #E774BB;\n  --ds-background-accent-magenta-bolder-hovered: #F797D2;\n  --ds-background-accent-magenta-bolder-pressed: #FDD0EC;\n  --ds-background-accent-gray-subtlest: #2C333A;\n  --ds-background-accent-gray-subtlest-hovered: #38414A;\n  --ds-background-accent-gray-subtlest-pressed: #454F59;\n  --ds-background-accent-gray-subtler: #454F59;\n  --ds-background-accent-gray-subtler-hovered: #596773;\n  --ds-background-accent-gray-subtler-pressed: #738496;\n  --ds-background-accent-gray-subtle: #596773;\n  --ds-background-accent-gray-subtle-hovered: #454F59;\n  --ds-background-accent-gray-subtle-pressed: #38414A;\n  --ds-background-accent-gray-bolder: #8C9BAB;\n  --ds-background-accent-gray-bolder-hovered: #9FADBC;\n  --ds-background-accent-gray-bolder-pressed: #B6C2CF;\n  --ds-background-disabled: #BCD6F00A;\n  --ds-background-input: #22272B;\n  --ds-background-input-hovered: #282E33;\n  --ds-background-input-pressed: #22272B;\n  --ds-background-inverse-subtle: #FFFFFF29;\n  --ds-background-inverse-subtle-hovered: #FFFFFF3D;\n  --ds-background-inverse-subtle-pressed: #FFFFFF52;\n  --ds-background-neutral: #A1BDD914;\n  --ds-background-neutral-hovered: #A6C5E229;\n  --ds-background-neutral-pressed: #BFDBF847;\n  --ds-background-neutral-subtle: #00000000;\n  --ds-background-neutral-subtle-hovered: #A1BDD914;\n  --ds-background-neutral-subtle-pressed: #A6C5E229;\n  --ds-background-neutral-bold: #9FADBC;\n  --ds-background-neutral-bold-hovered: #B6C2CF;\n  --ds-background-neutral-bold-pressed: #C7D1DB;\n  --ds-background-selected: #1C2B41;\n  --ds-background-selected-hovered: #09326C;\n  --ds-background-selected-pressed: #0055CC;\n  --ds-background-selected-bold: #579DFF;\n  --ds-background-selected-bold-hovered: #85B8FF;\n  --ds-background-selected-bold-pressed: #CCE0FF;\n  --ds-background-brand-subtlest: #1C2B41;\n  --ds-background-brand-subtlest-hovered: #09326C;\n  --ds-background-brand-subtlest-pressed: #0055CC;\n  --ds-background-brand-bold: #579DFF;\n  --ds-background-brand-bold-hovered: #85B8FF;\n  --ds-background-brand-bold-pressed: #CCE0FF;\n  --ds-background-brand-boldest: #E9F2FF;\n  --ds-background-brand-boldest-hovered: #CCE0FF;\n  --ds-background-brand-boldest-pressed: #85B8FF;\n  --ds-background-danger: #42221F;\n  --ds-background-danger-hovered: #5D1F1A;\n  --ds-background-danger-pressed: #AE2E24;\n  --ds-background-danger-bold: #F87168;\n  --ds-background-danger-bold-hovered: #FD9891;\n  --ds-background-danger-bold-pressed: #FFD5D2;\n  --ds-background-warning: #332E1B;\n  --ds-background-warning-hovered: #533F04;\n  --ds-background-warning-pressed: #7F5F01;\n  --ds-background-warning-bold: #F5CD47;\n  --ds-background-warning-bold-hovered: #E2B203;\n  --ds-background-warning-bold-pressed: #CF9F02;\n  --ds-background-success: #1C3329;\n  --ds-background-success-hovered: #164B35;\n  --ds-background-success-pressed: #216E4E;\n  --ds-background-success-bold: #4BCE97;\n  --ds-background-success-bold-hovered: #7EE2B8;\n  --ds-background-success-bold-pressed: #BAF3DB;\n  --ds-background-discovery: #2B273F;\n  --ds-background-discovery-hovered: #352C63;\n  --ds-background-discovery-pressed: #5E4DB2;\n  --ds-background-discovery-bold: #9F8FEF;\n  --ds-background-discovery-bold-hovered: #B8ACF6;\n  --ds-background-discovery-bold-pressed: #DFD8FD;\n  --ds-background-information: #1C2B41;\n  --ds-background-information-hovered: #09326C;\n  --ds-background-information-pressed: #0055CC;\n  --ds-background-information-bold: #579DFF;\n  --ds-background-information-bold-hovered: #85B8FF;\n  --ds-background-information-bold-pressed: #CCE0FF;\n  --ds-blanket: #10121499;\n  --ds-blanket-selected: #1D7AFC14;\n  --ds-blanket-danger: #E3493514;\n  --ds-interaction-hovered: #ffffff33;\n  --ds-interaction-pressed: #ffffff5c;\n  --ds-skeleton: #A1BDD914;\n  --ds-skeleton-subtle: #BCD6F00A;\n  --ds-chart-categorical-1: #2898BD;\n  --ds-chart-categorical-1-hovered: #42B2D7;\n  --ds-chart-categorical-2: #B8ACF6;\n  --ds-chart-categorical-2-hovered: #DFD8FD;\n  --ds-chart-categorical-3: #E56910;\n  --ds-chart-categorical-3-hovered: #F38A3F;\n  --ds-chart-categorical-4: #F797D2;\n  --ds-chart-categorical-4-hovered: #FDD0EC;\n  --ds-chart-categorical-5: #CCE0FF;\n  --ds-chart-categorical-5-hovered: #E9F2FF;\n  --ds-chart-categorical-6: #8270DB;\n  --ds-chart-categorical-6-hovered: #8F7EE7;\n  --ds-chart-categorical-7: #FDD0EC;\n  --ds-chart-categorical-7-hovered: #FFECF8;\n  --ds-chart-categorical-8: #FEC195;\n  --ds-chart-categorical-8-hovered: #FEDEC8;\n  --ds-chart-lime-bold: #82B536;\n  --ds-chart-lime-bold-hovered: #94C748;\n  --ds-chart-lime-bolder: #94C748;\n  --ds-chart-lime-bolder-hovered: #B3DF72;\n  --ds-chart-lime-boldest: #B3DF72;\n  --ds-chart-lime-boldest-hovered: #D3F1A7;\n  --ds-chart-neutral: #738496;\n  --ds-chart-neutral-hovered: #8C9BAB;\n  --ds-chart-red-bold: #E2483D;\n  --ds-chart-red-bold-hovered: #F15B50;\n  --ds-chart-red-bolder: #F15B50;\n  --ds-chart-red-bolder-hovered: #F87168;\n  --ds-chart-red-boldest: #FD9891;\n  --ds-chart-red-boldest-hovered: #FFD5D2;\n  --ds-chart-orange-bold: #F38A3F;\n  --ds-chart-orange-bold-hovered: #FEA362;\n  --ds-chart-orange-bolder: #FEA362;\n  --ds-chart-orange-bolder-hovered: #FEC195;\n  --ds-chart-orange-boldest: #FEC195;\n  --ds-chart-orange-boldest-hovered: #FEDEC8;\n  --ds-chart-yellow-bold: #CF9F02;\n  --ds-chart-yellow-bold-hovered: #E2B203;\n  --ds-chart-yellow-bolder: #E2B203;\n  --ds-chart-yellow-bolder-hovered: #F5CD47;\n  --ds-chart-yellow-boldest: #F5CD47;\n  --ds-chart-yellow-boldest-hovered: #F8E6A0;\n  --ds-chart-green-bold: #2ABB7F;\n  --ds-chart-green-bold-hovered: #4BCE97;\n  --ds-chart-green-bolder: #4BCE97;\n  --ds-chart-green-bolder-hovered: #7EE2B8;\n  --ds-chart-green-boldest: #7EE2B8;\n  --ds-chart-green-boldest-hovered: #BAF3DB;\n  --ds-chart-teal-bold: #42B2D7;\n  --ds-chart-teal-bold-hovered: #6CC3E0;\n  --ds-chart-teal-bolder: #6CC3E0;\n  --ds-chart-teal-bolder-hovered: #9DD9EE;\n  --ds-chart-teal-boldest: #9DD9EE;\n  --ds-chart-teal-boldest-hovered: #C6EDFB;\n  --ds-chart-blue-bold: #1D7AFC;\n  --ds-chart-blue-bold-hovered: #388BFF;\n  --ds-chart-blue-bolder: #388BFF;\n  --ds-chart-blue-bolder-hovered: #579DFF;\n  --ds-chart-blue-boldest: #85B8FF;\n  --ds-chart-blue-boldest-hovered: #CCE0FF;\n  --ds-chart-purple-bold: #8270DB;\n  --ds-chart-purple-bold-hovered: #8F7EE7;\n  --ds-chart-purple-bolder: #8F7EE7;\n  --ds-chart-purple-bolder-hovered: #9F8FEF;\n  --ds-chart-purple-boldest: #B8ACF6;\n  --ds-chart-purple-boldest-hovered: #DFD8FD;\n  --ds-chart-magenta-bold: #CD519D;\n  --ds-chart-magenta-bold-hovered: #DA62AC;\n  --ds-chart-magenta-bolder: #DA62AC;\n  --ds-chart-magenta-bolder-hovered: #E774BB;\n  --ds-chart-magenta-boldest: #F797D2;\n  --ds-chart-magenta-boldest-hovered: #FDD0EC;\n  --ds-chart-gray-bold: #738496;\n  --ds-chart-gray-bold-hovered: #8C9BAB;\n  --ds-chart-gray-bolder: #8C9BAB;\n  --ds-chart-gray-bolder-hovered: #9FADBC;\n  --ds-chart-gray-boldest: #9FADBC;\n  --ds-chart-gray-boldest-hovered: #B6C2CF;\n  --ds-chart-brand: #388BFF;\n  --ds-chart-brand-hovered: #579DFF;\n  --ds-chart-danger: #E2483D;\n  --ds-chart-danger-hovered: #F15B50;\n  --ds-chart-danger-bold: #FD9891;\n  --ds-chart-danger-bold-hovered: #FFD5D2;\n  --ds-chart-warning: #CF9F02;\n  --ds-chart-warning-hovered: #E2B203;\n  --ds-chart-warning-bold: #F5CD47;\n  --ds-chart-warning-bold-hovered: #F8E6A0;\n  --ds-chart-success: #2ABB7F;\n  --ds-chart-success-hovered: #4BCE97;\n  --ds-chart-success-bold: #7EE2B8;\n  --ds-chart-success-bold-hovered: #BAF3DB;\n  --ds-chart-discovery: #8270DB;\n  --ds-chart-discovery-hovered: #8F7EE7;\n  --ds-chart-discovery-bold: #B8ACF6;\n  --ds-chart-discovery-bold-hovered: #DFD8FD;\n  --ds-chart-information: #1D7AFC;\n  --ds-chart-information-hovered: #388BFF;\n  --ds-chart-information-bold: #85B8FF;\n  --ds-chart-information-bold-hovered: #CCE0FF;\n  --ds-surface: #1D2125;\n  --ds-surface-hovered: #22272B;\n  --ds-surface-pressed: #282E33;\n  --ds-surface-overlay: #282E33;\n  --ds-surface-overlay-hovered: #2C333A;\n  --ds-surface-overlay-pressed: #38414A;\n  --ds-surface-raised: #22272B;\n  --ds-surface-raised-hovered: #282E33;\n  --ds-surface-raised-pressed: #2C333A;\n  --ds-surface-sunken: #161A1D;\n  --ds-shadow-overflow: 0px 0px 12px #0304048F, 0px 0px 1px #03040480;\n  --ds-shadow-overflow-perimeter: #03040480;\n  --ds-shadow-overflow-spread: #0304048f;\n  --ds-shadow-overlay: 0px 0px 0px 1px #39424a, 0px 8px 12px #0304045C, 0px 0px 1px 1px #03040480;\n  --ds-shadow-raised: 0px 0px 0px 1px #00000000, 0px 1px 1px #03040480, 0px 0px 1px #03040480;\n  --ds-opacity-disabled: 0.4;\n  --ds-opacity-loading: 0.2;\n  --ds-UNSAFE-transparent: transparent;\n  --ds-elevation-surface-current: #1D2125;\n}\n";

  var atlassianDark$1 = /*#__PURE__*/Object.freeze({
    __proto__: null,
    'default': atlassianDark
  });

  /**
   * THIS FILE WAS CREATED VIA CODEGEN DO NOT MODIFY {@see http://go/af-codegen}
   * @codegen <<SignedSource::d075fc63f2fcbc4c48f9221541c1d067>>
   * @codegenCommand yarn build tokens
   */
  var atlassianDarkFuture = "\nhtml[data-color-mode=\"light\"][data-theme~=\"light:light\"],\nhtml[data-color-mode=\"dark\"][data-theme~=\"dark:light\"] {\n  color-scheme: dark;\n  --ds-background-disabled: #B3DF72;\n}\n";

  var atlassianDarkFuture$1 = /*#__PURE__*/Object.freeze({
    __proto__: null,
    'default': atlassianDarkFuture
  });

  /**
   * THIS FILE WAS CREATED VIA CODEGEN DO NOT MODIFY {@see http://go/af-codegen}
   * @codegen <<SignedSource::bf757d2e6e845640909422b79fff5c32>>
   * @codegenCommand yarn build tokens
   */
  var atlassianDarkIncreasedContrast = "\nhtml[data-color-mode=\"light\"][data-theme~=\"light:dark-increased-contrast\"],\nhtml[data-color-mode=\"dark\"][data-theme~=\"dark:dark-increased-contrast\"],\nhtml[data-color-mode=\"light\"][data-contrast-mode=\"more\"][data-theme~=\"light:dark\"],\nhtml[data-color-mode=\"dark\"][data-contrast-mode=\"more\"][data-theme~=\"dark:dark\"] {\n  color-scheme: dark;\n  --ds-text: #DEE4EA;\n  --ds-text-accent-lime: #D3F1A7;\n  --ds-text-accent-lime-bolder: #EFFFD6;\n  --ds-text-accent-red: #FFD5D2;\n  --ds-text-accent-red-bolder: #FFECEB;\n  --ds-text-accent-orange: #FEDEC8;\n  --ds-text-accent-orange-bolder: #FFF3EB;\n  --ds-text-accent-yellow: #F8E6A0;\n  --ds-text-accent-yellow-bolder: #FFF7D6;\n  --ds-text-accent-green: #BAF3DB;\n  --ds-text-accent-green-bolder: #DCFFF1;\n  --ds-text-accent-teal: #C6EDFB;\n  --ds-text-accent-teal-bolder: #E7F9FF;\n  --ds-text-accent-blue: #CCE0FF;\n  --ds-text-accent-blue-bolder: #E9F2FF;\n  --ds-text-accent-purple: #DFD8FD;\n  --ds-text-accent-purple-bolder: #F3F0FF;\n  --ds-text-accent-magenta: #FDD0EC;\n  --ds-text-accent-magenta-bolder: #FFECF8;\n  --ds-text-accent-gray: #B6C2CF;\n  --ds-text-accent-gray-bolder: #DEE4EA;\n  --ds-text-disabled: #BFDBF847;\n  --ds-text-inverse: #1D2125;\n  --ds-text-selected: #CCE0FF;\n  --ds-text-brand: #CCE0FF;\n  --ds-text-danger: #FFD5D2;\n  --ds-text-warning: #FEDEC8;\n  --ds-text-warning-inverse: #161A1D;\n  --ds-text-success: #BAF3DB;\n  --ds-text-discovery: #DFD8FD;\n  --ds-text-information: #CCE0FF;\n  --ds-text-subtlest: #B6C2CF;\n  --ds-text-subtle: #C7D1DB;\n  --ds-link: #CCE0FF;\n  --ds-link-pressed: #CCE0FF;\n  --ds-link-visited: #B8ACF6;\n  --ds-link-visited-pressed: #DFD8FD;\n  --ds-icon: #C7D1DB;\n  --ds-icon-accent-lime: #B3DF72;\n  --ds-icon-accent-red: #FD9891;\n  --ds-icon-accent-orange: #FEC195;\n  --ds-icon-accent-yellow: #F5CD47;\n  --ds-icon-accent-green: #7EE2B8;\n  --ds-icon-accent-teal: #9DD9EE;\n  --ds-icon-accent-blue: #85B8FF;\n  --ds-icon-accent-purple: #B8ACF6;\n  --ds-icon-accent-magenta: #F797D2;\n  --ds-icon-accent-gray: #9FADBC;\n  --ds-icon-disabled: #BFDBF847;\n  --ds-icon-inverse: #1D2125;\n  --ds-icon-selected: #85B8FF;\n  --ds-icon-brand: #85B8FF;\n  --ds-icon-danger: #F87168;\n  --ds-icon-warning: #FEC195;\n  --ds-icon-warning-inverse: #161A1D;\n  --ds-icon-success: #7EE2B8;\n  --ds-icon-discovery: #9F8FEF;\n  --ds-icon-information: #85B8FF;\n  --ds-icon-subtle: #B6C2CF;\n  --ds-border: #9BB4CA80;\n  --ds-border-accent-lime: #B3DF72;\n  --ds-border-accent-red: #FD9891;\n  --ds-border-accent-orange: #FEC195;\n  --ds-border-accent-yellow: #F5CD47;\n  --ds-border-accent-green: #7EE2B8;\n  --ds-border-accent-teal: #9DD9EE;\n  --ds-border-accent-blue: #85B8FF;\n  --ds-border-accent-purple: #B8ACF6;\n  --ds-border-accent-magenta: #F797D2;\n  --ds-border-accent-gray: #9FADBC;\n  --ds-border-disabled: #A6C5E229;\n  --ds-border-focused: #85B8FF;\n  --ds-border-input: #9FADBC;\n  --ds-border-inverse: #161A1D;\n  --ds-border-selected: #85B8FF;\n  --ds-border-brand: #85B8FF;\n  --ds-border-danger: #F87168;\n  --ds-border-warning: #FEC195;\n  --ds-border-success: #7EE2B8;\n  --ds-border-discovery: #9F8FEF;\n  --ds-border-information: #85B8FF;\n  --ds-border-bold: #9FADBC;\n  --ds-background-accent-lime-subtlest: #28311B;\n  --ds-background-accent-lime-subtlest-hovered: #37471F;\n  --ds-background-accent-lime-subtlest-pressed: #4C6B1F;\n  --ds-background-accent-lime-subtler: #37471F;\n  --ds-background-accent-lime-subtler-hovered: #4C6B1F;\n  --ds-background-accent-lime-subtler-pressed: #5B7F24;\n  --ds-background-accent-lime-subtle: #4C6B1F;\n  --ds-background-accent-lime-subtle-hovered: #37471F;\n  --ds-background-accent-lime-subtle-pressed: #28311B;\n  --ds-background-accent-lime-bolder: #D3F1A7;\n  --ds-background-accent-lime-bolder-hovered: #EFFFD6;\n  --ds-background-accent-lime-bolder-pressed: #D3F1A7;\n  --ds-background-accent-red-subtlest: #42221F;\n  --ds-background-accent-red-subtlest-hovered: #5D1F1A;\n  --ds-background-accent-red-subtlest-pressed: #AE2E24;\n  --ds-background-accent-red-subtler: #5D1F1A;\n  --ds-background-accent-red-subtler-hovered: #AE2E24;\n  --ds-background-accent-red-subtler-pressed: #C9372C;\n  --ds-background-accent-red-subtle: #AE2E24;\n  --ds-background-accent-red-subtle-hovered: #5D1F1A;\n  --ds-background-accent-red-subtle-pressed: #42221F;\n  --ds-background-accent-red-bolder: #FFD5D2;\n  --ds-background-accent-red-bolder-hovered: #FFECEB;\n  --ds-background-accent-red-bolder-pressed: #FFD5D2;\n  --ds-background-accent-orange-subtlest: #38291E;\n  --ds-background-accent-orange-subtlest-hovered: #702E00;\n  --ds-background-accent-orange-subtlest-pressed: #A54800;\n  --ds-background-accent-orange-subtler: #702E00;\n  --ds-background-accent-orange-subtler-hovered: #A54800;\n  --ds-background-accent-orange-subtler-pressed: #C25100;\n  --ds-background-accent-orange-subtle: #A54800;\n  --ds-background-accent-orange-subtle-hovered: #702E00;\n  --ds-background-accent-orange-subtle-pressed: #38291E;\n  --ds-background-accent-orange-bolder: #FEDEC8;\n  --ds-background-accent-orange-bolder-hovered: #FFF3EB;\n  --ds-background-accent-orange-bolder-pressed: #FEDEC8;\n  --ds-background-accent-yellow-subtlest: #332E1B;\n  --ds-background-accent-yellow-subtlest-hovered: #533F04;\n  --ds-background-accent-yellow-subtlest-pressed: #7F5F01;\n  --ds-background-accent-yellow-subtler: #533F04;\n  --ds-background-accent-yellow-subtler-hovered: #7F5F01;\n  --ds-background-accent-yellow-subtler-pressed: #946F00;\n  --ds-background-accent-yellow-subtle: #7F5F01;\n  --ds-background-accent-yellow-subtle-hovered: #533F04;\n  --ds-background-accent-yellow-subtle-pressed: #332E1B;\n  --ds-background-accent-yellow-bolder: #F8E6A0;\n  --ds-background-accent-yellow-bolder-hovered: #FFF7D6;\n  --ds-background-accent-yellow-bolder-pressed: #F8E6A0;\n  --ds-background-accent-green-subtlest: #1C3329;\n  --ds-background-accent-green-subtlest-hovered: #164B35;\n  --ds-background-accent-green-subtlest-pressed: #216E4E;\n  --ds-background-accent-green-subtler: #164B35;\n  --ds-background-accent-green-subtler-hovered: #216E4E;\n  --ds-background-accent-green-subtler-pressed: #1F845A;\n  --ds-background-accent-green-subtle: #216E4E;\n  --ds-background-accent-green-subtle-hovered: #164B35;\n  --ds-background-accent-green-subtle-pressed: #1C3329;\n  --ds-background-accent-green-bolder: #BAF3DB;\n  --ds-background-accent-green-bolder-hovered: #DCFFF1;\n  --ds-background-accent-green-bolder-pressed: #BAF3DB;\n  --ds-background-accent-teal-subtlest: #1E3137;\n  --ds-background-accent-teal-subtlest-hovered: #164555;\n  --ds-background-accent-teal-subtlest-pressed: #206A83;\n  --ds-background-accent-teal-subtler: #164555;\n  --ds-background-accent-teal-subtler-hovered: #206A83;\n  --ds-background-accent-teal-subtler-pressed: #227D9B;\n  --ds-background-accent-teal-subtle: #206A83;\n  --ds-background-accent-teal-subtle-hovered: #164555;\n  --ds-background-accent-teal-subtle-pressed: #1E3137;\n  --ds-background-accent-teal-bolder: #C6EDFB;\n  --ds-background-accent-teal-bolder-hovered: #E7F9FF;\n  --ds-background-accent-teal-bolder-pressed: #C6EDFB;\n  --ds-background-accent-blue-subtlest: #1C2B41;\n  --ds-background-accent-blue-subtlest-hovered: #09326C;\n  --ds-background-accent-blue-subtlest-pressed: #0055CC;\n  --ds-background-accent-blue-subtler: #09326C;\n  --ds-background-accent-blue-subtler-hovered: #0055CC;\n  --ds-background-accent-blue-subtler-pressed: #0C66E4;\n  --ds-background-accent-blue-subtle: #0055CC;\n  --ds-background-accent-blue-subtle-hovered: #09326C;\n  --ds-background-accent-blue-subtle-pressed: #1C2B41;\n  --ds-background-accent-blue-bolder: #579DFF;\n  --ds-background-accent-blue-bolder-hovered: #85B8FF;\n  --ds-background-accent-blue-bolder-pressed: #CCE0FF;\n  --ds-background-accent-purple-subtlest: #2B273F;\n  --ds-background-accent-purple-subtlest-hovered: #352C63;\n  --ds-background-accent-purple-subtlest-pressed: #5E4DB2;\n  --ds-background-accent-purple-subtler: #352C63;\n  --ds-background-accent-purple-subtler-hovered: #5E4DB2;\n  --ds-background-accent-purple-subtler-pressed: #6E5DC6;\n  --ds-background-accent-purple-subtle: #5E4DB2;\n  --ds-background-accent-purple-subtle-hovered: #352C63;\n  --ds-background-accent-purple-subtle-pressed: #2B273F;\n  --ds-background-accent-purple-bolder: #DFD8FD;\n  --ds-background-accent-purple-bolder-hovered: #F3F0FF;\n  --ds-background-accent-purple-bolder-pressed: #DFD8FD;\n  --ds-background-accent-magenta-subtlest: #3D2232;\n  --ds-background-accent-magenta-subtlest-hovered: #50253F;\n  --ds-background-accent-magenta-subtlest-pressed: #943D73;\n  --ds-background-accent-magenta-subtler: #50253F;\n  --ds-background-accent-magenta-subtler-hovered: #943D73;\n  --ds-background-accent-magenta-subtler-pressed: #AE4787;\n  --ds-background-accent-magenta-subtle: #943D73;\n  --ds-background-accent-magenta-subtle-hovered: #50253F;\n  --ds-background-accent-magenta-subtle-pressed: #3D2232;\n  --ds-background-accent-magenta-bolder: #FDD0EC;\n  --ds-background-accent-magenta-bolder-hovered: #FFECF8;\n  --ds-background-accent-magenta-bolder-pressed: #FDD0EC;\n  --ds-background-accent-gray-subtlest: #2C333A;\n  --ds-background-accent-gray-subtlest-hovered: #38414A;\n  --ds-background-accent-gray-subtlest-pressed: #454F59;\n  --ds-background-accent-gray-subtler: #454F59;\n  --ds-background-accent-gray-subtler-hovered: #596773;\n  --ds-background-accent-gray-subtler-pressed: #738496;\n  --ds-background-accent-gray-subtle: #454F59;\n  --ds-background-accent-gray-subtle-hovered: #2C333A;\n  --ds-background-accent-gray-subtle-pressed: #22272B;\n  --ds-background-accent-gray-bolder: #9FADBC;\n  --ds-background-accent-gray-bolder-hovered: #B6C2CF;\n  --ds-background-accent-gray-bolder-pressed: #C7D1DB;\n  --ds-background-disabled: #BCD6F00A;\n  --ds-background-input: #22272B;\n  --ds-background-input-hovered: #282E33;\n  --ds-background-input-pressed: #22272B;\n  --ds-background-inverse-subtle: #FFFFFF29;\n  --ds-background-inverse-subtle-hovered: #FFFFFF3D;\n  --ds-background-inverse-subtle-pressed: #FFFFFF52;\n  --ds-background-neutral: #A1BDD914;\n  --ds-background-neutral-hovered: #A6C5E229;\n  --ds-background-neutral-pressed: #BFDBF847;\n  --ds-background-neutral-subtle: #00000000;\n  --ds-background-neutral-subtle-hovered: #A1BDD914;\n  --ds-background-neutral-subtle-pressed: #A6C5E229;\n  --ds-background-neutral-bold: #9FADBC;\n  --ds-background-neutral-bold-hovered: #B6C2CF;\n  --ds-background-neutral-bold-pressed: #C7D1DB;\n  --ds-background-selected: #1C2B41;\n  --ds-background-selected-hovered: #09326C;\n  --ds-background-selected-pressed: #0055CC;\n  --ds-background-selected-bold: #CCE0FF;\n  --ds-background-selected-bold-hovered: #E9F2FF;\n  --ds-background-selected-bold-pressed: #DEE4EA;\n  --ds-background-brand-subtlest: #1C2B41;\n  --ds-background-brand-subtlest-hovered: #09326C;\n  --ds-background-brand-subtlest-pressed: #0055CC;\n  --ds-background-brand-bold: #CCE0FF;\n  --ds-background-brand-bold-hovered: #E9F2FF;\n  --ds-background-brand-bold-pressed: #DEE4EA;\n  --ds-background-brand-boldest: #E9F2FF;\n  --ds-background-brand-boldest-hovered: #CCE0FF;\n  --ds-background-brand-boldest-pressed: #85B8FF;\n  --ds-background-danger: #42221F;\n  --ds-background-danger-hovered: #5D1F1A;\n  --ds-background-danger-pressed: #AE2E24;\n  --ds-background-danger-bold: #FFD5D2;\n  --ds-background-danger-bold-hovered: #FFECEB;\n  --ds-background-danger-bold-pressed: #DEE4EA;\n  --ds-background-warning: #332E1B;\n  --ds-background-warning-hovered: #533F04;\n  --ds-background-warning-pressed: #7F5F01;\n  --ds-background-warning-bold: #F8E6A0;\n  --ds-background-warning-bold-hovered: #FFF7D6;\n  --ds-background-warning-bold-pressed: #DEE4EA;\n  --ds-background-success: #1C3329;\n  --ds-background-success-hovered: #164B35;\n  --ds-background-success-pressed: #216E4E;\n  --ds-background-success-bold: #BAF3DB;\n  --ds-background-success-bold-hovered: #DCFFF1;\n  --ds-background-success-bold-pressed: #DEE4EA;\n  --ds-background-discovery: #2B273F;\n  --ds-background-discovery-hovered: #352C63;\n  --ds-background-discovery-pressed: #5E4DB2;\n  --ds-background-discovery-bold: #DFD8FD;\n  --ds-background-discovery-bold-hovered: #F3F0FF;\n  --ds-background-discovery-bold-pressed: #DEE4EA;\n  --ds-background-information: #1C2B41;\n  --ds-background-information-hovered: #09326C;\n  --ds-background-information-pressed: #0055CC;\n  --ds-background-information-bold: #CCE0FF;\n  --ds-background-information-bold-hovered: #E9F2FF;\n  --ds-background-information-bold-pressed: #DEE4EA;\n  --ds-blanket: #10121499;\n  --ds-blanket-selected: #1D7AFC14;\n  --ds-blanket-danger: #E3493514;\n  --ds-interaction-hovered: #ffffff33;\n  --ds-interaction-pressed: #ffffff5c;\n  --ds-skeleton: #A1BDD914;\n  --ds-skeleton-subtle: #BCD6F00A;\n  --ds-chart-categorical-1: #9DD9EE;\n  --ds-chart-categorical-1-hovered: #C6EDFB;\n  --ds-chart-categorical-2: #B8ACF6;\n  --ds-chart-categorical-2-hovered: #DFD8FD;\n  --ds-chart-categorical-3: #FEC195;\n  --ds-chart-categorical-3-hovered: #FEDEC8;\n  --ds-chart-categorical-4: #F797D2;\n  --ds-chart-categorical-4-hovered: #FDD0EC;\n  --ds-chart-categorical-5: #CCE0FF;\n  --ds-chart-categorical-5-hovered: #E9F2FF;\n  --ds-chart-categorical-6: #B8ACF6;\n  --ds-chart-categorical-6-hovered: #DFD8FD;\n  --ds-chart-categorical-7: #FDD0EC;\n  --ds-chart-categorical-7-hovered: #FFECF8;\n  --ds-chart-categorical-8: #FEC195;\n  --ds-chart-categorical-8-hovered: #FEDEC8;\n  --ds-chart-lime-bold: #B3DF72;\n  --ds-chart-lime-bold-hovered: #D3F1A7;\n  --ds-chart-lime-bolder: #D3F1A7;\n  --ds-chart-lime-bolder-hovered: #EFFFD6;\n  --ds-chart-lime-boldest: #EFFFD6;\n  --ds-chart-lime-boldest-hovered: #D3F1A7;\n  --ds-chart-neutral: #8C9BAB;\n  --ds-chart-neutral-hovered: #9FADBC;\n  --ds-chart-red-bold: #FD9891;\n  --ds-chart-red-bold-hovered: #FFD5D2;\n  --ds-chart-red-bolder: #FFD5D2;\n  --ds-chart-red-bolder-hovered: #FFECEB;\n  --ds-chart-red-boldest: #FFECEB;\n  --ds-chart-red-boldest-hovered: #FFD5D2;\n  --ds-chart-orange-bold: #FEC195;\n  --ds-chart-orange-bold-hovered: #FEDEC8;\n  --ds-chart-orange-bolder: #FEDEC8;\n  --ds-chart-orange-bolder-hovered: #FFF3EB;\n  --ds-chart-orange-boldest: #FFF3EB;\n  --ds-chart-orange-boldest-hovered: #FEDEC8;\n  --ds-chart-yellow-bold: #F5CD47;\n  --ds-chart-yellow-bold-hovered: #F8E6A0;\n  --ds-chart-yellow-bolder: #F8E6A0;\n  --ds-chart-yellow-bolder-hovered: #FFF7D6;\n  --ds-chart-yellow-boldest: #FFF7D6;\n  --ds-chart-yellow-boldest-hovered: #F8E6A0;\n  --ds-chart-green-bold: #7EE2B8;\n  --ds-chart-green-bold-hovered: #4BCE97;\n  --ds-chart-green-bolder: #BAF3DB;\n  --ds-chart-green-bolder-hovered: #DCFFF1;\n  --ds-chart-green-boldest: #DCFFF1;\n  --ds-chart-green-boldest-hovered: #BAF3DB;\n  --ds-chart-teal-bold: #9DD9EE;\n  --ds-chart-teal-bold-hovered: #C6EDFB;\n  --ds-chart-teal-bolder: #C6EDFB;\n  --ds-chart-teal-bolder-hovered: #E7F9FF;\n  --ds-chart-teal-boldest: #E7F9FF;\n  --ds-chart-teal-boldest-hovered: #C6EDFB;\n  --ds-chart-blue-bold: #85B8FF;\n  --ds-chart-blue-bold-hovered: #CCE0FF;\n  --ds-chart-blue-bolder: #CCE0FF;\n  --ds-chart-blue-bolder-hovered: #E9F2FF;\n  --ds-chart-blue-boldest: #E9F2FF;\n  --ds-chart-blue-boldest-hovered: #CCE0FF;\n  --ds-chart-purple-bold: #B8ACF6;\n  --ds-chart-purple-bold-hovered: #DFD8FD;\n  --ds-chart-purple-bolder: #DFD8FD;\n  --ds-chart-purple-bolder-hovered: #F3F0FF;\n  --ds-chart-purple-boldest: #F3F0FF;\n  --ds-chart-purple-boldest-hovered: #DFD8FD;\n  --ds-chart-magenta-bold: #F797D2;\n  --ds-chart-magenta-bold-hovered: #FDD0EC;\n  --ds-chart-magenta-bolder: #FDD0EC;\n  --ds-chart-magenta-bolder-hovered: #FFECF8;\n  --ds-chart-magenta-boldest: #FFECF8;\n  --ds-chart-magenta-boldest-hovered: #FDD0EC;\n  --ds-chart-gray-bold: #8C9BAB;\n  --ds-chart-gray-bold-hovered: #9FADBC;\n  --ds-chart-gray-bolder: #9FADBC;\n  --ds-chart-gray-bolder-hovered: #B6C2CF;\n  --ds-chart-gray-boldest: #B6C2CF;\n  --ds-chart-gray-boldest-hovered: #C7D1DB;\n  --ds-chart-brand: #579DFF;\n  --ds-chart-brand-hovered: #85B8FF;\n  --ds-chart-danger: #F87168;\n  --ds-chart-danger-hovered: #F15B50;\n  --ds-chart-danger-bold: #FD9891;\n  --ds-chart-danger-bold-hovered: #FFD5D2;\n  --ds-chart-warning: #CF9F02;\n  --ds-chart-warning-hovered: #E2B203;\n  --ds-chart-warning-bold: #F5CD47;\n  --ds-chart-warning-bold-hovered: #F8E6A0;\n  --ds-chart-success: #2ABB7F;\n  --ds-chart-success-hovered: #4BCE97;\n  --ds-chart-success-bold: #7EE2B8;\n  --ds-chart-success-bold-hovered: #BAF3DB;\n  --ds-chart-discovery: #9F8FEF;\n  --ds-chart-discovery-hovered: #8F7EE7;\n  --ds-chart-discovery-bold: #B8ACF6;\n  --ds-chart-discovery-bold-hovered: #DFD8FD;\n  --ds-chart-information: #579DFF;\n  --ds-chart-information-hovered: #388BFF;\n  --ds-chart-information-bold: #85B8FF;\n  --ds-chart-information-bold-hovered: #CCE0FF;\n  --ds-surface: #1D2125;\n  --ds-surface-hovered: #22272B;\n  --ds-surface-pressed: #282E33;\n  --ds-surface-overlay: #282E33;\n  --ds-surface-overlay-hovered: #2C333A;\n  --ds-surface-overlay-pressed: #38414A;\n  --ds-surface-raised: #22272B;\n  --ds-surface-raised-hovered: #282E33;\n  --ds-surface-raised-pressed: #2C333A;\n  --ds-surface-sunken: #161A1D;\n  --ds-shadow-overflow: 0px 0px 12px #0304048F;\n  --ds-shadow-overflow-perimeter: #03040480;\n  --ds-shadow-overflow-spread: #0304048f;\n  --ds-shadow-overlay: inset 0px 0px 0px 1px #9BB4CA80;\n  --ds-shadow-raised: inset 0px 0px 0px 1px #9BB4CA80;\n  --ds-opacity-disabled: 0.4;\n  --ds-opacity-loading: 0.2;\n  --ds-UNSAFE-transparent: transparent;\n  --ds-elevation-surface-current: #1D2125;\n}\n";

  var atlassianDarkIncreasedContrast$1 = /*#__PURE__*/Object.freeze({
    __proto__: null,
    'default': atlassianDarkIncreasedContrast
  });

  /**
   * THIS FILE WAS CREATED VIA CODEGEN DO NOT MODIFY {@see http://go/af-codegen}
   * @codegen <<SignedSource::01b0c7fb11a8188e21446b5be31c37fb>>
   * @codegenCommand yarn build tokens
   */
  var atlassianLegacyLight = "\nhtml[data-color-mode=\"light\"][data-theme~=\"light:legacy-light\"],\nhtml[data-color-mode=\"dark\"][data-theme~=\"dark:legacy-light\"] {\n  color-scheme: light;\n  --ds-text: #172B4D;\n  --ds-text-accent-lime: #4C6B1F;\n  --ds-text-accent-lime-bolder: #37471F;\n  --ds-text-accent-red: #DE350B;\n  --ds-text-accent-red-bolder: #BF2600;\n  --ds-text-accent-orange: #F18D13;\n  --ds-text-accent-orange-bolder: #B65C02;\n  --ds-text-accent-yellow: #FF991F;\n  --ds-text-accent-yellow-bolder: #FF8B00;\n  --ds-text-accent-green: #00875A;\n  --ds-text-accent-green-bolder: #006644;\n  --ds-text-accent-teal: #00A3BF;\n  --ds-text-accent-teal-bolder: #008DA6;\n  --ds-text-accent-blue: #0052CC;\n  --ds-text-accent-blue-bolder: #0747A6;\n  --ds-text-accent-purple: #5243AA;\n  --ds-text-accent-purple-bolder: #403294;\n  --ds-text-accent-magenta: #E774BB;\n  --ds-text-accent-magenta-bolder: #DA62AC;\n  --ds-text-accent-gray: #505F79;\n  --ds-text-accent-gray-bolder: #172B4D;\n  --ds-text-disabled: #A5ADBA;\n  --ds-text-inverse: #FFFFFF;\n  --ds-text-selected: #0052CC;\n  --ds-text-brand: #0065FF;\n  --ds-text-danger: #DE350B;\n  --ds-text-warning: #974F0C;\n  --ds-text-warning-inverse: #172B4D;\n  --ds-text-success: #006644;\n  --ds-text-discovery: #403294;\n  --ds-text-information: #0052CC;\n  --ds-text-subtlest: #7A869A;\n  --ds-text-subtle: #42526E;\n  --ds-link: #0052CC;\n  --ds-link-pressed: #0747A6;\n  --ds-link-visited: #403294;\n  --ds-link-visited-pressed: #403294;\n  --ds-icon: #505F79;\n  --ds-icon-accent-lime: #6A9A23;\n  --ds-icon-accent-red: #FF5630;\n  --ds-icon-accent-orange: #D94008;\n  --ds-icon-accent-yellow: #FFAB00;\n  --ds-icon-accent-green: #36B37E;\n  --ds-icon-accent-teal: #00B8D9;\n  --ds-icon-accent-blue: #0065FF;\n  --ds-icon-accent-purple: #6554C0;\n  --ds-icon-accent-magenta: #CD519D;\n  --ds-icon-accent-gray: #5E6C84;\n  --ds-icon-disabled: #8993A4;\n  --ds-icon-inverse: #FFFFFF;\n  --ds-icon-selected: #0052CC;\n  --ds-icon-brand: #0065FF;\n  --ds-icon-danger: #DE350B;\n  --ds-icon-warning: #FFC400;\n  --ds-icon-warning-inverse: #253858;\n  --ds-icon-success: #00875A;\n  --ds-icon-discovery: #8777D9;\n  --ds-icon-information: #0747A6;\n  --ds-icon-subtle: #6B778C;\n  --ds-border: #091e4221;\n  --ds-border-accent-lime: #6A9A23;\n  --ds-border-accent-red: #FF5630;\n  --ds-border-accent-orange: #D94008;\n  --ds-border-accent-yellow: #FFAB00;\n  --ds-border-accent-green: #36B37E;\n  --ds-border-accent-teal: #00B8D9;\n  --ds-border-accent-blue: #0065FF;\n  --ds-border-accent-purple: #6554C0;\n  --ds-border-accent-magenta: #CD519D;\n  --ds-border-accent-gray: #5E6C84;\n  --ds-border-disabled: #FAFBFC;\n  --ds-border-focused: #2684FF;\n  --ds-border-input: #FAFBFC;\n  --ds-border-inverse: #FFFFFF;\n  --ds-border-selected: #0052CC;\n  --ds-border-brand: #0052CC;\n  --ds-border-danger: #FF5630;\n  --ds-border-warning: #FFC400;\n  --ds-border-success: #00875A;\n  --ds-border-discovery: #998DD9;\n  --ds-border-information: #0065FF;\n  --ds-border-bold: #344563;\n  --ds-background-accent-lime-subtlest: #EEFBDA;\n  --ds-background-accent-lime-subtlest-hovered: #D3F1A7;\n  --ds-background-accent-lime-subtlest-pressed: #B3DF72;\n  --ds-background-accent-lime-subtler: #D3F1A7;\n  --ds-background-accent-lime-subtler-hovered: #B3DF72;\n  --ds-background-accent-lime-subtler-pressed: #94C748;\n  --ds-background-accent-lime-subtle: #94C748;\n  --ds-background-accent-lime-subtle-hovered: #B3DF72;\n  --ds-background-accent-lime-subtle-pressed: #D3F1A7;\n  --ds-background-accent-lime-bolder: #5B7F24;\n  --ds-background-accent-lime-bolder-hovered: #37471F;\n  --ds-background-accent-lime-bolder-pressed: #37471F;\n  --ds-background-accent-red-subtlest: #FF8F73;\n  --ds-background-accent-red-subtlest-hovered: #FF7452;\n  --ds-background-accent-red-subtlest-pressed: #FF5630;\n  --ds-background-accent-red-subtler: #FF7452;\n  --ds-background-accent-red-subtler-hovered: #FF5630;\n  --ds-background-accent-red-subtler-pressed: #DE350B;\n  --ds-background-accent-red-subtle: #DE350B;\n  --ds-background-accent-red-subtle-hovered: #FF5630;\n  --ds-background-accent-red-subtle-pressed: #FF7452;\n  --ds-background-accent-red-bolder: #DE350B;\n  --ds-background-accent-red-bolder-hovered: #FF5630;\n  --ds-background-accent-red-bolder-pressed: #FF7452;\n  --ds-background-accent-orange-subtlest: #F18D13;\n  --ds-background-accent-orange-subtlest-hovered: #FEC57B;\n  --ds-background-accent-orange-subtlest-pressed: #FFE2BD;\n  --ds-background-accent-orange-subtler: #B65C02;\n  --ds-background-accent-orange-subtler-hovered: #F18D13;\n  --ds-background-accent-orange-subtler-pressed: #FEC57B;\n  --ds-background-accent-orange-subtle: #5F3811;\n  --ds-background-accent-orange-subtle-hovered: #974F0C;\n  --ds-background-accent-orange-subtle-pressed: #B65C02;\n  --ds-background-accent-orange-bolder: #43290F;\n  --ds-background-accent-orange-bolder-hovered: #5F3811;\n  --ds-background-accent-orange-bolder-pressed: #974F0C;\n  --ds-background-accent-yellow-subtlest: #FFE380;\n  --ds-background-accent-yellow-subtlest-hovered: #FFC400;\n  --ds-background-accent-yellow-subtlest-pressed: #FFAB00;\n  --ds-background-accent-yellow-subtler: #FFC400;\n  --ds-background-accent-yellow-subtler-hovered: #FFAB00;\n  --ds-background-accent-yellow-subtler-pressed: #FF991F;\n  --ds-background-accent-yellow-subtle: #FF991F;\n  --ds-background-accent-yellow-subtle-hovered: #FFAB00;\n  --ds-background-accent-yellow-subtle-pressed: #FFC400;\n  --ds-background-accent-yellow-bolder: #FF991F;\n  --ds-background-accent-yellow-bolder-hovered: #FFAB00;\n  --ds-background-accent-yellow-bolder-pressed: #FFC400;\n  --ds-background-accent-green-subtlest: #79F2C0;\n  --ds-background-accent-green-subtlest-hovered: #57D9A3;\n  --ds-background-accent-green-subtlest-pressed: #36B37E;\n  --ds-background-accent-green-subtler: #57D9A3;\n  --ds-background-accent-green-subtler-hovered: #36B37E;\n  --ds-background-accent-green-subtler-pressed: #00875A;\n  --ds-background-accent-green-subtle: #00875A;\n  --ds-background-accent-green-subtle-hovered: #36B37E;\n  --ds-background-accent-green-subtle-pressed: #57D9A3;\n  --ds-background-accent-green-bolder: #00875A;\n  --ds-background-accent-green-bolder-hovered: #36B37E;\n  --ds-background-accent-green-bolder-pressed: #57D9A3;\n  --ds-background-accent-teal-subtlest: #79E2F2;\n  --ds-background-accent-teal-subtlest-hovered: #00C7E6;\n  --ds-background-accent-teal-subtlest-pressed: #00B8D9;\n  --ds-background-accent-teal-subtler: #00C7E6;\n  --ds-background-accent-teal-subtler-hovered: #00B8D9;\n  --ds-background-accent-teal-subtler-pressed: #00A3BF;\n  --ds-background-accent-teal-subtle: #00A3BF;\n  --ds-background-accent-teal-subtle-hovered: #00B8D9;\n  --ds-background-accent-teal-subtle-pressed: #00C7E6;\n  --ds-background-accent-teal-bolder: #00A3BF;\n  --ds-background-accent-teal-bolder-hovered: #00B8D9;\n  --ds-background-accent-teal-bolder-pressed: #00C7E6;\n  --ds-background-accent-blue-subtlest: #4C9AFF;\n  --ds-background-accent-blue-subtlest-hovered: #2684FF;\n  --ds-background-accent-blue-subtlest-pressed: #0065FF;\n  --ds-background-accent-blue-subtler: #2684FF;\n  --ds-background-accent-blue-subtler-hovered: #0065FF;\n  --ds-background-accent-blue-subtler-pressed: #0052CC;\n  --ds-background-accent-blue-subtle: #0052CC;\n  --ds-background-accent-blue-subtle-hovered: #0065FF;\n  --ds-background-accent-blue-subtle-pressed: #2684FF;\n  --ds-background-accent-blue-bolder: #0052CC;\n  --ds-background-accent-blue-bolder-hovered: #0065FF;\n  --ds-background-accent-blue-bolder-pressed: #2684FF;\n  --ds-background-accent-purple-subtlest: #998DD9;\n  --ds-background-accent-purple-subtlest-hovered: #8777D9;\n  --ds-background-accent-purple-subtlest-pressed: #6554C0;\n  --ds-background-accent-purple-subtler: #8777D9;\n  --ds-background-accent-purple-subtler-hovered: #6554C0;\n  --ds-background-accent-purple-subtler-pressed: #5243AA;\n  --ds-background-accent-purple-subtle: #5243AA;\n  --ds-background-accent-purple-subtle-hovered: #6554C0;\n  --ds-background-accent-purple-subtle-pressed: #8777D9;\n  --ds-background-accent-purple-bolder: #5243AA;\n  --ds-background-accent-purple-bolder-hovered: #6554C0;\n  --ds-background-accent-purple-bolder-pressed: #8777D9;\n  --ds-background-accent-magenta-subtlest: #FFECF8;\n  --ds-background-accent-magenta-subtlest-hovered: #FDD0EC;\n  --ds-background-accent-magenta-subtlest-pressed: #F797D2;\n  --ds-background-accent-magenta-subtler: #FDD0EC;\n  --ds-background-accent-magenta-subtler-hovered: #F797D2;\n  --ds-background-accent-magenta-subtler-pressed: #E774BB;\n  --ds-background-accent-magenta-subtle: #E774BB;\n  --ds-background-accent-magenta-subtle-hovered: #F797D2;\n  --ds-background-accent-magenta-subtle-pressed: #FDD0EC;\n  --ds-background-accent-magenta-bolder: #AE4787;\n  --ds-background-accent-magenta-bolder-hovered: #943D73;\n  --ds-background-accent-magenta-bolder-pressed: #50253F;\n  --ds-background-accent-gray-subtlest: #6B778C;\n  --ds-background-accent-gray-subtlest-hovered: #5E6C84;\n  --ds-background-accent-gray-subtlest-pressed: #505F79;\n  --ds-background-accent-gray-subtler: #5E6C84;\n  --ds-background-accent-gray-subtler-hovered: #505F79;\n  --ds-background-accent-gray-subtler-pressed: #42526E;\n  --ds-background-accent-gray-subtle: #505F79;\n  --ds-background-accent-gray-subtle-hovered: #5E6C84;\n  --ds-background-accent-gray-subtle-pressed: #6B778C;\n  --ds-background-accent-gray-bolder: #42526E;\n  --ds-background-accent-gray-bolder-hovered: #344563;\n  --ds-background-accent-gray-bolder-pressed: #253858;\n  --ds-background-disabled: #091e4289;\n  --ds-background-input: #FAFBFC;\n  --ds-background-input-hovered: #EBECF0;\n  --ds-background-input-pressed: #FFFFFF;\n  --ds-background-inverse-subtle: #00000029;\n  --ds-background-inverse-subtle-hovered: #0000003D;\n  --ds-background-inverse-subtle-pressed: #00000052;\n  --ds-background-neutral: #DFE1E6;\n  --ds-background-neutral-hovered: #091e4214;\n  --ds-background-neutral-pressed: #B3D4FF;\n  --ds-background-neutral-subtle: transparent;\n  --ds-background-neutral-subtle-hovered: #091e4214;\n  --ds-background-neutral-subtle-pressed: #B3D4FF;\n  --ds-background-neutral-bold: #42526E;\n  --ds-background-neutral-bold-hovered: #505F79;\n  --ds-background-neutral-bold-pressed: #344563;\n  --ds-background-selected: #DEEBFF;\n  --ds-background-selected-hovered: #B3D4FF;\n  --ds-background-selected-pressed: #4C9AFF;\n  --ds-background-selected-bold: #0052CC;\n  --ds-background-selected-bold-hovered: #2684FF;\n  --ds-background-selected-bold-pressed: #0052CC;\n  --ds-background-brand-subtlest: #B3D4FF;\n  --ds-background-brand-subtlest-hovered: #DEEBFF;\n  --ds-background-brand-subtlest-pressed: #4C9AFF;\n  --ds-background-brand-bold: #0052CC;\n  --ds-background-brand-bold-hovered: #0065FF;\n  --ds-background-brand-bold-pressed: #0747A6;\n  --ds-background-brand-boldest: #0747A6;\n  --ds-background-brand-boldest-hovered: #0052CC;\n  --ds-background-brand-boldest-pressed: #0747A6;\n  --ds-background-danger: #FFEBE6;\n  --ds-background-danger-hovered: #FFBDAD;\n  --ds-background-danger-pressed: #FF8F73;\n  --ds-background-danger-bold: #DE350B;\n  --ds-background-danger-bold-hovered: #FF5630;\n  --ds-background-danger-bold-pressed: #BF2600;\n  --ds-background-warning: #FFFAE6;\n  --ds-background-warning-hovered: #FFF0B3;\n  --ds-background-warning-pressed: #FFE380;\n  --ds-background-warning-bold: #FFAB00;\n  --ds-background-warning-bold-hovered: #FFC400;\n  --ds-background-warning-bold-pressed: #FF991F;\n  --ds-background-success: #E3FCEF;\n  --ds-background-success-hovered: #ABF5D1;\n  --ds-background-success-pressed: #79F2C0;\n  --ds-background-success-bold: #00875A;\n  --ds-background-success-bold-hovered: #57D9A3;\n  --ds-background-success-bold-pressed: #00875A;\n  --ds-background-discovery: #EAE6FF;\n  --ds-background-discovery-hovered: #C0B6F2;\n  --ds-background-discovery-pressed: #998DD9;\n  --ds-background-discovery-bold: #5243AA;\n  --ds-background-discovery-bold-hovered: #8777D9;\n  --ds-background-discovery-bold-pressed: #5243AA;\n  --ds-background-information: #DEEBFF;\n  --ds-background-information-hovered: #B3D4FF;\n  --ds-background-information-pressed: #4C9AFF;\n  --ds-background-information-bold: #0052CC;\n  --ds-background-information-bold-hovered: #2684FF;\n  --ds-background-information-bold-pressed: #0052CC;\n  --ds-blanket: #091e4289;\n  --ds-blanket-selected: #388BFF14;\n  --ds-blanket-danger: #EF5C4814;\n  --ds-interaction-hovered: #00000029;\n  --ds-interaction-pressed: #00000052;\n  --ds-skeleton: #F4F5F7;\n  --ds-skeleton-subtle: #091e420a;\n  --ds-chart-categorical-1: #00B8D9;\n  --ds-chart-categorical-1-hovered: #00A3BF;\n  --ds-chart-categorical-2: #5243AA;\n  --ds-chart-categorical-2-hovered: #403294;\n  --ds-chart-categorical-3: #D94008;\n  --ds-chart-categorical-3-hovered: #B65C02;\n  --ds-chart-categorical-4: #943D73;\n  --ds-chart-categorical-4-hovered: #50253F;\n  --ds-chart-categorical-5: #0052CC;\n  --ds-chart-categorical-5-hovered: #0747A6;\n  --ds-chart-categorical-6: #5243AA;\n  --ds-chart-categorical-6-hovered: #403294;\n  --ds-chart-categorical-7: #50253F;\n  --ds-chart-categorical-7-hovered: #341829;\n  --ds-chart-categorical-8: #974F0C;\n  --ds-chart-categorical-8-hovered: #5F3811;\n  --ds-chart-lime-bold: #6A9A23;\n  --ds-chart-lime-bold-hovered: #5B7F24;\n  --ds-chart-lime-bolder: #5B7F24;\n  --ds-chart-lime-bolder-hovered: #4C6B1F;\n  --ds-chart-lime-boldest: #4C6B1F;\n  --ds-chart-lime-boldest-hovered: #37471F;\n  --ds-chart-neutral: #5E6C84;\n  --ds-chart-neutral-hovered: #505F79;\n  --ds-chart-red-bold: #FF5630;\n  --ds-chart-red-bold-hovered: #DE350B;\n  --ds-chart-red-bolder: #DE350B;\n  --ds-chart-red-bolder-hovered: #BF2600;\n  --ds-chart-red-boldest: #BF2600;\n  --ds-chart-red-boldest-hovered: #BF2600;\n  --ds-chart-orange-bold: #D97008;\n  --ds-chart-orange-bold-hovered: #B65C02;\n  --ds-chart-orange-bolder: #B65C02;\n  --ds-chart-orange-bolder-hovered: #974F0C;\n  --ds-chart-orange-boldest: #974F0C;\n  --ds-chart-orange-boldest-hovered: #5F3811;\n  --ds-chart-yellow-bold: #FFAB00;\n  --ds-chart-yellow-bold-hovered: #FF991F;\n  --ds-chart-yellow-bolder: #FF991F;\n  --ds-chart-yellow-bolder-hovered: #FF8B00;\n  --ds-chart-yellow-boldest: #FF8B00;\n  --ds-chart-yellow-boldest-hovered: #FF8B00;\n  --ds-chart-green-bold: #36B37E;\n  --ds-chart-green-bold-hovered: #00875A;\n  --ds-chart-green-bolder: #00875A;\n  --ds-chart-green-bolder-hovered: #006644;\n  --ds-chart-green-boldest: #006644;\n  --ds-chart-green-boldest-hovered: #006644;\n  --ds-chart-teal-bold: #00B8D9;\n  --ds-chart-teal-bold-hovered: #00A3BF;\n  --ds-chart-teal-bolder: #00A3BF;\n  --ds-chart-teal-bolder-hovered: #008DA6;\n  --ds-chart-teal-boldest: #008DA6;\n  --ds-chart-teal-boldest-hovered: #008DA6;\n  --ds-chart-blue-bold: #0065FF;\n  --ds-chart-blue-bold-hovered: #0052CC;\n  --ds-chart-blue-bolder: #0052CC;\n  --ds-chart-blue-bolder-hovered: #0747A6;\n  --ds-chart-blue-boldest: #0747A6;\n  --ds-chart-blue-boldest-hovered: #0747A6;\n  --ds-chart-purple-bold: #6554C0;\n  --ds-chart-purple-bold-hovered: #5243AA;\n  --ds-chart-purple-bolder: #5243AA;\n  --ds-chart-purple-bolder-hovered: #403294;\n  --ds-chart-purple-boldest: #403294;\n  --ds-chart-purple-boldest-hovered: #403294;\n  --ds-chart-magenta-bold: #DA62AC;\n  --ds-chart-magenta-bold-hovered: #CD519D;\n  --ds-chart-magenta-bolder: #CD519D;\n  --ds-chart-magenta-bolder-hovered: #AE4787;\n  --ds-chart-magenta-boldest: #943D73;\n  --ds-chart-magenta-boldest-hovered: #50253F;\n  --ds-chart-gray-bold: #5E6C84;\n  --ds-chart-gray-bold-hovered: #505F79;\n  --ds-chart-gray-bolder: #505F79;\n  --ds-chart-gray-bolder-hovered: #42526E;\n  --ds-chart-gray-boldest: #42526E;\n  --ds-chart-gray-boldest-hovered: #42526E;\n  --ds-chart-brand: #0065FF;\n  --ds-chart-brand-hovered: #0052CC;\n  --ds-chart-danger: #FF5630;\n  --ds-chart-danger-hovered: #DE350B;\n  --ds-chart-danger-bold: #DE350B;\n  --ds-chart-danger-bold-hovered: #BF2600;\n  --ds-chart-warning: #FFAB00;\n  --ds-chart-warning-hovered: #FF991F;\n  --ds-chart-warning-bold: #FF991F;\n  --ds-chart-warning-bold-hovered: #FF8B00;\n  --ds-chart-success: #36B37E;\n  --ds-chart-success-hovered: #00875A;\n  --ds-chart-success-bold: #00875A;\n  --ds-chart-success-bold-hovered: #006644;\n  --ds-chart-discovery: #6554C0;\n  --ds-chart-discovery-hovered: #5243AA;\n  --ds-chart-discovery-bold: #5243AA;\n  --ds-chart-discovery-bold-hovered: #403294;\n  --ds-chart-information: #0065FF;\n  --ds-chart-information-hovered: #0052CC;\n  --ds-chart-information-bold: #0052CC;\n  --ds-chart-information-bold-hovered: #0747A6;\n  --ds-surface: #FFFFFF;\n  --ds-surface-hovered: #FAFBFC;\n  --ds-surface-pressed: #F4F5F7;\n  --ds-surface-overlay: #FFFFFF;\n  --ds-surface-overlay-hovered: #FAFBFC;\n  --ds-surface-overlay-pressed: #F4F5F7;\n  --ds-surface-raised: #FFFFFF;\n  --ds-surface-raised-hovered: #FAFBFC;\n  --ds-surface-raised-pressed: #F4F5F7;\n  --ds-surface-sunken: #F4F5F7;\n  --ds-shadow-overflow: 0px 0px 8px #091e4229, 0px 0px 1px #091e421F;\n  --ds-shadow-overflow-perimeter: #091e421f;\n  --ds-shadow-overflow-spread: #091e4229;\n  --ds-shadow-overlay: 0px 8px 12px #091e4226, 0px 0px 1px #091e424F;\n  --ds-shadow-raised: 0px 1px 1px #091e4240, 0px 0px 1px #091e424F;\n  --ds-opacity-disabled: 0.4;\n  --ds-opacity-loading: 0.2;\n  --ds-UNSAFE-transparent: transparent;\n  --ds-elevation-surface-current: #FFFFFF;\n}\n";

  var atlassianLegacyLight$1 = /*#__PURE__*/Object.freeze({
    __proto__: null,
    'default': atlassianLegacyLight
  });

  /**
   * THIS FILE WAS CREATED VIA CODEGEN DO NOT MODIFY {@see http://go/af-codegen}
   * @codegen <<SignedSource::b5eb40f6aea97bcbbb771ec3de00b47c>>
   * @codegenCommand yarn build tokens
   */
  var atlassianLegacyDark = "\nhtml[data-color-mode=\"light\"][data-theme~=\"light:legacy-dark\"],\nhtml[data-color-mode=\"dark\"][data-theme~=\"dark:legacy-dark\"] {\n  color-scheme: dark;\n  --ds-text: #E6EDFA;\n  --ds-text-accent-lime: #4C6B1F;\n  --ds-text-accent-lime-bolder: #37471F;\n  --ds-text-accent-red: #FF5630;\n  --ds-text-accent-red-bolder: #FF7452;\n  --ds-text-accent-orange: #FEC57B;\n  --ds-text-accent-orange-bolder: #F18D13;\n  --ds-text-accent-yellow: #FFAB00;\n  --ds-text-accent-yellow-bolder: #FFC400;\n  --ds-text-accent-green: #36B37E;\n  --ds-text-accent-green-bolder: #57D9A3;\n  --ds-text-accent-teal: #00B8D9;\n  --ds-text-accent-teal-bolder: #00C7E6;\n  --ds-text-accent-blue: #0065FF;\n  --ds-text-accent-blue-bolder: #2684FF;\n  --ds-text-accent-purple: #6554C0;\n  --ds-text-accent-purple-bolder: #8777D9;\n  --ds-text-accent-magenta: #F797D2;\n  --ds-text-accent-magenta-bolder: #FDD0EC;\n  --ds-text-accent-gray: #8C9CB8;\n  --ds-text-accent-gray-bolder: #ABBBD6;\n  --ds-text-disabled: #1B2638;\n  --ds-text-inverse: #0D1424;\n  --ds-text-selected: #0052CC;\n  --ds-text-brand: #0052CC;\n  --ds-text-danger: #FF5630;\n  --ds-text-warning: #172B4D;\n  --ds-text-warning-inverse: #0D1424;\n  --ds-text-success: #36B37E;\n  --ds-text-discovery: #6554C0;\n  --ds-text-information: #0065FF;\n  --ds-text-subtlest: #7988A3;\n  --ds-text-subtle: #9FB0CC;\n  --ds-link: #0052CC;\n  --ds-link-pressed: #0065FF;\n  --ds-link-visited: #6554C0;\n  --ds-link-visited-pressed: #6554C0;\n  --ds-icon: #DCE5F5;\n  --ds-icon-accent-lime: #6A9A23;\n  --ds-icon-accent-red: #BF2600;\n  --ds-icon-accent-orange: #F18D13;\n  --ds-icon-accent-yellow: #FF8B00;\n  --ds-icon-accent-green: #006644;\n  --ds-icon-accent-teal: #008DA6;\n  --ds-icon-accent-blue: #0747A6;\n  --ds-icon-accent-purple: #403294;\n  --ds-icon-accent-magenta: #DA62AC;\n  --ds-icon-accent-gray: #B8C7E0;\n  --ds-icon-disabled: #0d14245b;\n  --ds-icon-inverse: #202B3D;\n  --ds-icon-selected: #4C9AFF;\n  --ds-icon-brand: #4C9AFF;\n  --ds-icon-danger: #FF8F73;\n  --ds-icon-warning: #FFF0B3;\n  --ds-icon-warning-inverse: #202B3D;\n  --ds-icon-success: #57D9A3;\n  --ds-icon-discovery: #998DD9;\n  --ds-icon-information: #B3D4FF;\n  --ds-icon-subtle: #202B3D;\n  --ds-border: #202B3D;\n  --ds-border-accent-lime: #6A9A23;\n  --ds-border-accent-red: #BF2600;\n  --ds-border-accent-orange: #F18D13;\n  --ds-border-accent-yellow: #FF8B00;\n  --ds-border-accent-green: #006644;\n  --ds-border-accent-teal: #008DA6;\n  --ds-border-accent-blue: #0747A6;\n  --ds-border-accent-purple: #403294;\n  --ds-border-accent-magenta: #DA62AC;\n  --ds-border-accent-gray: #B8C7E0;\n  --ds-border-disabled: #0E1624;\n  --ds-border-focused: #B3D4FF;\n  --ds-border-input: #202B3D;\n  --ds-border-inverse: #0D1424;\n  --ds-border-selected: #0052CC;\n  --ds-border-brand: #0052CC;\n  --ds-border-danger: #DE350B;\n  --ds-border-warning: #FF8B00;\n  --ds-border-success: #006644;\n  --ds-border-discovery: #403294;\n  --ds-border-information: #0747A6;\n  --ds-border-bold: #7988A3;\n  --ds-background-accent-lime-subtlest: #2A3818;\n  --ds-background-accent-lime-subtlest-hovered: #37471F;\n  --ds-background-accent-lime-subtlest-pressed: #4C6B1F;\n  --ds-background-accent-lime-subtler: #37471F;\n  --ds-background-accent-lime-subtler-hovered: #4C6B1F;\n  --ds-background-accent-lime-subtler-pressed: #5B7F24;\n  --ds-background-accent-lime-subtle: #4C6B1F;\n  --ds-background-accent-lime-subtle-hovered: #37471F;\n  --ds-background-accent-lime-subtle-pressed: #2A3818;\n  --ds-background-accent-lime-bolder: #94C748;\n  --ds-background-accent-lime-bolder-hovered: #B3DF72;\n  --ds-background-accent-lime-bolder-pressed: #D3F1A7;\n  --ds-background-accent-red-subtlest: #BF2600;\n  --ds-background-accent-red-subtlest-hovered: #DE350B;\n  --ds-background-accent-red-subtlest-pressed: #FF5630;\n  --ds-background-accent-red-subtler: #DE350B;\n  --ds-background-accent-red-subtler-hovered: #FF5630;\n  --ds-background-accent-red-subtler-pressed: #FF7452;\n  --ds-background-accent-red-subtle: #FF5630;\n  --ds-background-accent-red-subtle-hovered: #DE350B;\n  --ds-background-accent-red-subtle-pressed: #BF2600;\n  --ds-background-accent-red-bolder: #FF8F73;\n  --ds-background-accent-red-bolder-hovered: #FFBDAD;\n  --ds-background-accent-red-bolder-pressed: #FFEBE6;\n  --ds-background-accent-orange-subtlest: #43290F;\n  --ds-background-accent-orange-subtlest-hovered: #5F3811;\n  --ds-background-accent-orange-subtlest-pressed: #974F0C;\n  --ds-background-accent-orange-subtler: #5F3811;\n  --ds-background-accent-orange-subtler-hovered: #974F0C;\n  --ds-background-accent-orange-subtler-pressed: #B65C02;\n  --ds-background-accent-orange-subtle: #974F0C;\n  --ds-background-accent-orange-subtle-hovered: #5F3811;\n  --ds-background-accent-orange-subtle-pressed: #4A2B0F;\n  --ds-background-accent-orange-bolder: #F18D13;\n  --ds-background-accent-orange-bolder-hovered: #FEC57B;\n  --ds-background-accent-orange-bolder-pressed: #FFE2BD;\n  --ds-background-accent-yellow-subtlest: #FF8B00;\n  --ds-background-accent-yellow-subtlest-hovered: #FF991F;\n  --ds-background-accent-yellow-subtlest-pressed: #FFAB00;\n  --ds-background-accent-yellow-subtler: #FF991F;\n  --ds-background-accent-yellow-subtler-hovered: #FFAB00;\n  --ds-background-accent-yellow-subtler-pressed: #FFC400;\n  --ds-background-accent-yellow-subtle: #FFAB00;\n  --ds-background-accent-yellow-subtle-hovered: #FF991F;\n  --ds-background-accent-yellow-subtle-pressed: #FF8B00;\n  --ds-background-accent-yellow-bolder: #FFE380;\n  --ds-background-accent-yellow-bolder-hovered: #FFF0B3;\n  --ds-background-accent-yellow-bolder-pressed: #FFFAE6;\n  --ds-background-accent-green-subtlest: #006644;\n  --ds-background-accent-green-subtlest-hovered: #00875A;\n  --ds-background-accent-green-subtlest-pressed: #36B37E;\n  --ds-background-accent-green-subtler: #00875A;\n  --ds-background-accent-green-subtler-hovered: #36B37E;\n  --ds-background-accent-green-subtler-pressed: #57D9A3;\n  --ds-background-accent-green-subtle: #36B37E;\n  --ds-background-accent-green-subtle-hovered: #00875A;\n  --ds-background-accent-green-subtle-pressed: #006644;\n  --ds-background-accent-green-bolder: #79F2C0;\n  --ds-background-accent-green-bolder-hovered: #ABF5D1;\n  --ds-background-accent-green-bolder-pressed: #E3FCEF;\n  --ds-background-accent-teal-subtlest: #008DA6;\n  --ds-background-accent-teal-subtlest-hovered: #00A3BF;\n  --ds-background-accent-teal-subtlest-pressed: #00B8D9;\n  --ds-background-accent-teal-subtler: #00A3BF;\n  --ds-background-accent-teal-subtler-hovered: #00B8D9;\n  --ds-background-accent-teal-subtler-pressed: #00C7E6;\n  --ds-background-accent-teal-subtle: #00B8D9;\n  --ds-background-accent-teal-subtle-hovered: #00A3BF;\n  --ds-background-accent-teal-subtle-pressed: #008DA6;\n  --ds-background-accent-teal-bolder: #79E2F2;\n  --ds-background-accent-teal-bolder-hovered: #B3F5FF;\n  --ds-background-accent-teal-bolder-pressed: #E6FCFF;\n  --ds-background-accent-blue-subtlest: #0747A6;\n  --ds-background-accent-blue-subtlest-hovered: #0052CC;\n  --ds-background-accent-blue-subtlest-pressed: #0065FF;\n  --ds-background-accent-blue-subtler: #0052CC;\n  --ds-background-accent-blue-subtler-hovered: #0065FF;\n  --ds-background-accent-blue-subtler-pressed: #2684FF;\n  --ds-background-accent-blue-subtle: #0065FF;\n  --ds-background-accent-blue-subtle-hovered: #0052CC;\n  --ds-background-accent-blue-subtle-pressed: #0747A6;\n  --ds-background-accent-blue-bolder: #4C9AFF;\n  --ds-background-accent-blue-bolder-hovered: #B3D4FF;\n  --ds-background-accent-blue-bolder-pressed: #DEEBFF;\n  --ds-background-accent-purple-subtlest: #403294;\n  --ds-background-accent-purple-subtlest-hovered: #5243AA;\n  --ds-background-accent-purple-subtlest-pressed: #6554C0;\n  --ds-background-accent-purple-subtler: #5243AA;\n  --ds-background-accent-purple-subtler-hovered: #6554C0;\n  --ds-background-accent-purple-subtler-pressed: #8777D9;\n  --ds-background-accent-purple-subtle: #6554C0;\n  --ds-background-accent-purple-subtle-hovered: #5243AA;\n  --ds-background-accent-purple-subtle-pressed: #403294;\n  --ds-background-accent-purple-bolder: #998DD9;\n  --ds-background-accent-purple-bolder-hovered: #C0B6F2;\n  --ds-background-accent-purple-bolder-pressed: #EAE6FF;\n  --ds-background-accent-magenta-subtlest: #341829;\n  --ds-background-accent-magenta-subtlest-hovered: #50253F;\n  --ds-background-accent-magenta-subtlest-pressed: #943D73;\n  --ds-background-accent-magenta-subtler: #50253F;\n  --ds-background-accent-magenta-subtler-hovered: #943D73;\n  --ds-background-accent-magenta-subtler-pressed: #AE4787;\n  --ds-background-accent-magenta-subtle: #943D73;\n  --ds-background-accent-magenta-subtle-hovered: #50253F;\n  --ds-background-accent-magenta-subtle-pressed: #421F34;\n  --ds-background-accent-magenta-bolder: #E774BB;\n  --ds-background-accent-magenta-bolder-hovered: #F797D2;\n  --ds-background-accent-magenta-bolder-pressed: #FDD0EC;\n  --ds-background-accent-gray-subtlest: #8C9CB8;\n  --ds-background-accent-gray-subtlest-hovered: #9FB0CC;\n  --ds-background-accent-gray-subtlest-pressed: #ABBBD6;\n  --ds-background-accent-gray-subtler: #9FB0CC;\n  --ds-background-accent-gray-subtler-hovered: #8C9CB8;\n  --ds-background-accent-gray-subtler-pressed: #7988A3;\n  --ds-background-accent-gray-subtle: #ABBBD6;\n  --ds-background-accent-gray-subtle-hovered: #B8C7E0;\n  --ds-background-accent-gray-subtle-pressed: #CED9EB;\n  --ds-background-accent-gray-bolder: #CED9EB;\n  --ds-background-accent-gray-bolder-hovered: #B8C7E0;\n  --ds-background-accent-gray-bolder-pressed: #ABBBD6;\n  --ds-background-disabled: #3B475C;\n  --ds-background-input: #0E1624;\n  --ds-background-input-hovered: #1B2638;\n  --ds-background-input-pressed: #0D1424;\n  --ds-background-inverse-subtle: #FFFFFF29;\n  --ds-background-inverse-subtle-hovered: #FFFFFF3D;\n  --ds-background-inverse-subtle-pressed: #FFFFFF52;\n  --ds-background-neutral: #3B475C;\n  --ds-background-neutral-hovered: #313D52;\n  --ds-background-neutral-pressed: #B3D4FF;\n  --ds-background-neutral-subtle: transparent;\n  --ds-background-neutral-subtle-hovered: #313D52;\n  --ds-background-neutral-subtle-pressed: #B3D4FF;\n  --ds-background-neutral-bold: #CED9EB;\n  --ds-background-neutral-bold-hovered: #B8C7E0;\n  --ds-background-neutral-bold-pressed: #DCE5F5;\n  --ds-background-selected: #DEEBFF;\n  --ds-background-selected-hovered: #B3D4FF;\n  --ds-background-selected-pressed: #4C9AFF;\n  --ds-background-selected-bold: #0052CC;\n  --ds-background-selected-bold-hovered: #0065FF;\n  --ds-background-selected-bold-pressed: #0747A6;\n  --ds-background-brand-subtlest: #0747A6;\n  --ds-background-brand-subtlest-hovered: #0052CC;\n  --ds-background-brand-subtlest-pressed: #0747A6;\n  --ds-background-brand-bold: #0052CC;\n  --ds-background-brand-bold-hovered: #0065FF;\n  --ds-background-brand-bold-pressed: #0747A6;\n  --ds-background-brand-boldest: #B3D4FF;\n  --ds-background-brand-boldest-hovered: #DEEBFF;\n  --ds-background-brand-boldest-pressed: #4C9AFF;\n  --ds-background-danger: #FFEBE6;\n  --ds-background-danger-hovered: #FFBDAD;\n  --ds-background-danger-pressed: #FF8F73;\n  --ds-background-danger-bold: #FF5630;\n  --ds-background-danger-bold-hovered: #FF7452;\n  --ds-background-danger-bold-pressed: #DE350B;\n  --ds-background-warning: #FFFAE6;\n  --ds-background-warning-hovered: #FFF0B3;\n  --ds-background-warning-pressed: #FFE380;\n  --ds-background-warning-bold: #FFAB00;\n  --ds-background-warning-bold-hovered: #FFC400;\n  --ds-background-warning-bold-pressed: #FF991F;\n  --ds-background-success: #E3FCEF;\n  --ds-background-success-hovered: #ABF5D1;\n  --ds-background-success-pressed: #79F2C0;\n  --ds-background-success-bold: #00875A;\n  --ds-background-success-bold-hovered: #36B37E;\n  --ds-background-success-bold-pressed: #006644;\n  --ds-background-discovery: #EAE6FF;\n  --ds-background-discovery-hovered: #C0B6F2;\n  --ds-background-discovery-pressed: #998DD9;\n  --ds-background-discovery-bold: #5243AA;\n  --ds-background-discovery-bold-hovered: #6554C0;\n  --ds-background-discovery-bold-pressed: #403294;\n  --ds-background-information: #DEEBFF;\n  --ds-background-information-hovered: #B3D4FF;\n  --ds-background-information-pressed: #4C9AFF;\n  --ds-background-information-bold: #0052CC;\n  --ds-background-information-bold-hovered: #0065FF;\n  --ds-background-information-bold-pressed: #0747A6;\n  --ds-blanket: #0d1424a0;\n  --ds-blanket-selected: #1D7AFC14;\n  --ds-blanket-danger: #E3493514;\n  --ds-interaction-hovered: #ffffff33;\n  --ds-interaction-pressed: #ffffff5c;\n  --ds-skeleton: #0d1424f2;\n  --ds-skeleton-subtle: #0d1424f7;\n  --ds-chart-categorical-1: #00B8D9;\n  --ds-chart-categorical-1-hovered: #00C7E6;\n  --ds-chart-categorical-2: #6554C0;\n  --ds-chart-categorical-2-hovered: #8777D9;\n  --ds-chart-categorical-3: #D97008;\n  --ds-chart-categorical-3-hovered: #F18D13;\n  --ds-chart-categorical-4: #F797D2;\n  --ds-chart-categorical-4-hovered: #FDD0EC;\n  --ds-chart-categorical-5: #0065FF;\n  --ds-chart-categorical-5-hovered: #2684FF;\n  --ds-chart-categorical-6: #6554C0;\n  --ds-chart-categorical-6-hovered: #8777D9;\n  --ds-chart-categorical-7: #FDD0EC;\n  --ds-chart-categorical-7-hovered: #FFECF8;\n  --ds-chart-categorical-8: #FEC57B;\n  --ds-chart-categorical-8-hovered: #FFE2BD;\n  --ds-chart-lime-bold: #6A9A23;\n  --ds-chart-lime-bold-hovered: #5B7F24;\n  --ds-chart-lime-bolder: #5B7F24;\n  --ds-chart-lime-bolder-hovered: #4C6B1F;\n  --ds-chart-lime-boldest: #4C6B1F;\n  --ds-chart-lime-boldest-hovered: #37471F;\n  --ds-chart-neutral: #B8C7E0;\n  --ds-chart-neutral-hovered: #ABBBD6;\n  --ds-chart-red-bold: #BF2600;\n  --ds-chart-red-bold-hovered: #DE350B;\n  --ds-chart-red-bolder: #DE350B;\n  --ds-chart-red-bolder-hovered: #FF5630;\n  --ds-chart-red-boldest: #FF5630;\n  --ds-chart-red-boldest-hovered: #FF7452;\n  --ds-chart-orange-bold: #F18D13;\n  --ds-chart-orange-bold-hovered: #FAA53D;\n  --ds-chart-orange-bolder: #FAA53D;\n  --ds-chart-orange-bolder-hovered: #FEC57B;\n  --ds-chart-orange-boldest: #FEC57B;\n  --ds-chart-orange-boldest-hovered: #FFE2BD;\n  --ds-chart-yellow-bold: #FF8B00;\n  --ds-chart-yellow-bold-hovered: #FF991F;\n  --ds-chart-yellow-bolder: #FF991F;\n  --ds-chart-yellow-bolder-hovered: #FFAB00;\n  --ds-chart-yellow-boldest: #FFAB00;\n  --ds-chart-yellow-boldest-hovered: #FFC400;\n  --ds-chart-green-bold: #006644;\n  --ds-chart-green-bold-hovered: #00875A;\n  --ds-chart-green-bolder: #00875A;\n  --ds-chart-green-bolder-hovered: #36B37E;\n  --ds-chart-green-boldest: #36B37E;\n  --ds-chart-green-boldest-hovered: #57D9A3;\n  --ds-chart-teal-bold: #008DA6;\n  --ds-chart-teal-bold-hovered: #00A3BF;\n  --ds-chart-teal-bolder: #00A3BF;\n  --ds-chart-teal-bolder-hovered: #00B8D9;\n  --ds-chart-teal-boldest: #00B8D9;\n  --ds-chart-teal-boldest-hovered: #00C7E6;\n  --ds-chart-blue-bold: #0747A6;\n  --ds-chart-blue-bold-hovered: #0052CC;\n  --ds-chart-blue-bolder: #0052CC;\n  --ds-chart-blue-bolder-hovered: #0065FF;\n  --ds-chart-blue-boldest: #0065FF;\n  --ds-chart-blue-boldest-hovered: #2684FF;\n  --ds-chart-purple-bold: #403294;\n  --ds-chart-purple-bold-hovered: #5243AA;\n  --ds-chart-purple-bolder: #5243AA;\n  --ds-chart-purple-bolder-hovered: #6554C0;\n  --ds-chart-purple-boldest: #6554C0;\n  --ds-chart-purple-boldest-hovered: #8777D9;\n  --ds-chart-magenta-bold: #CD519D;\n  --ds-chart-magenta-bold-hovered: #DA62AC;\n  --ds-chart-magenta-bolder: #DA62AC;\n  --ds-chart-magenta-bolder-hovered: #E774BB;\n  --ds-chart-magenta-boldest: #F797D2;\n  --ds-chart-magenta-boldest-hovered: #FDD0EC;\n  --ds-chart-gray-bold: #B8C7E0;\n  --ds-chart-gray-bold-hovered: #CED9EB;\n  --ds-chart-gray-bolder: #CED9EB;\n  --ds-chart-gray-bolder-hovered: #DCE5F5;\n  --ds-chart-gray-boldest: #DCE5F5;\n  --ds-chart-gray-boldest-hovered: #E6EDFA;\n  --ds-chart-brand: #0052CC;\n  --ds-chart-brand-hovered: #0065FF;\n  --ds-chart-danger: #DE350B;\n  --ds-chart-danger-hovered: #FF5630;\n  --ds-chart-danger-bold: #FF5630;\n  --ds-chart-danger-bold-hovered: #FF7452;\n  --ds-chart-warning: #FF991F;\n  --ds-chart-warning-hovered: #FFAB00;\n  --ds-chart-warning-bold: #FFAB00;\n  --ds-chart-warning-bold-hovered: #FFC400;\n  --ds-chart-success: #00875A;\n  --ds-chart-success-hovered: #36B37E;\n  --ds-chart-success-bold: #36B37E;\n  --ds-chart-success-bold-hovered: #57D9A3;\n  --ds-chart-discovery: #5243AA;\n  --ds-chart-discovery-hovered: #6554C0;\n  --ds-chart-discovery-bold: #6554C0;\n  --ds-chart-discovery-bold-hovered: #8777D9;\n  --ds-chart-information: #0052CC;\n  --ds-chart-information-hovered: #0065FF;\n  --ds-chart-information-bold: #0065FF;\n  --ds-chart-information-bold-hovered: #2684FF;\n  --ds-surface: #1B2638;\n  --ds-surface-hovered: #202B3D;\n  --ds-surface-pressed: #283447;\n  --ds-surface-overlay: #7988A3;\n  --ds-surface-overlay-hovered: #8C9CB8;\n  --ds-surface-overlay-pressed: #9FB0CC;\n  --ds-surface-raised: #455166;\n  --ds-surface-raised-hovered: #56637A;\n  --ds-surface-raised-pressed: #67758F;\n  --ds-surface-sunken: #67758F;\n  --ds-shadow-overflow: 0px 0px 12px #0304048F, 0px 0px 1px #03040480;\n  --ds-shadow-overflow-perimeter: #03040480;\n  --ds-shadow-overflow-spread: #0304048f;\n  --ds-shadow-overlay: inset 0px 0px 0px 1px #0d14240A, 0px 8px 12px #0d14245C, 0px 0px 1px #0d142480;\n  --ds-shadow-raised: inset 0px 0px 0px 1px #00000000, 0px 1px 1px #0d142480, 0px 0px 1px #0d142480;\n  --ds-opacity-disabled: 0.4;\n  --ds-opacity-loading: 0.2;\n  --ds-UNSAFE-transparent: transparent;\n  --ds-elevation-surface-current: #1B2638;\n}\n";

  var atlassianLegacyDark$1 = /*#__PURE__*/Object.freeze({
    __proto__: null,
    'default': atlassianLegacyDark
  });

  /**
   * THIS FILE WAS CREATED VIA CODEGEN DO NOT MODIFY {@see http://go/af-codegen}
   * @codegen <<SignedSource::50b812651f792cc85f9294bc1f28b366>>
   * @codegenCommand yarn build tokens
   */
  var atlassianSpacing = "\nhtml[data-theme~=\"spacing:spacing\"] {\n  --ds-space-0: 0rem;\n  --ds-space-025: 0.125rem;\n  --ds-space-050: 0.25rem;\n  --ds-space-075: 0.375rem;\n  --ds-space-100: 0.5rem;\n  --ds-space-150: 0.75rem;\n  --ds-space-200: 1rem;\n  --ds-space-250: 1.25rem;\n  --ds-space-300: 1.5rem;\n  --ds-space-400: 2rem;\n  --ds-space-500: 2.5rem;\n  --ds-space-600: 3rem;\n  --ds-space-800: 4rem;\n  --ds-space-1000: 5rem;\n  --ds-space-negative-025: -0.125rem;\n  --ds-space-negative-050: -0.25rem;\n  --ds-space-negative-075: -0.375rem;\n  --ds-space-negative-100: -0.5rem;\n  --ds-space-negative-150: -0.75rem;\n  --ds-space-negative-200: -1rem;\n  --ds-space-negative-250: -1.25rem;\n  --ds-space-negative-300: -1.5rem;\n  --ds-space-negative-400: -2rem;\n}\n";

  var atlassianSpacing$1 = /*#__PURE__*/Object.freeze({
    __proto__: null,
    'default': atlassianSpacing
  });

  /**
   * THIS FILE WAS CREATED VIA CODEGEN DO NOT MODIFY {@see http://go/af-codegen}
   * @codegen <<SignedSource::0eab025fe6a1c066de1ae91979f5a92d>>
   * @codegenCommand yarn build tokens
   */
  var atlassianTypographyAdg3 = "\nhtml[data-theme~=\"typography:typography-adg3\"] {\n  --ds-UNSAFE-textTransformUppercase: uppercase;\n  --ds-font-heading-xxlarge: normal 500 2.1875rem/2.5rem ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-heading-xlarge: normal 600 1.8125rem/2rem ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-heading-large: normal 500 1.5rem/1.75rem ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-heading-medium: normal 500 1.25rem/1.5rem ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-heading-small: normal 600 1rem/1.25rem ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-heading-xsmall: normal 600 0.875rem/1rem ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-heading-xxsmall: normal 600 0.75rem/1rem ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-body-large: normal 400 1rem/1.5rem ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-body: normal 400 0.875rem/1.25rem ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-body-small: normal 400 0.6875rem/1rem ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-body-UNSAFE_small: normal 400 0.75rem/1rem ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-code: normal 400 0.875em/1 ui-monospace, Menlo, \"Segoe UI Mono\", \"Ubuntu Mono\", monospace;\n  --ds-font-weight-regular: 400;\n  --ds-font-weight-medium: 500;\n  --ds-font-weight-semibold: 600;\n  --ds-font-weight-bold: 700;\n  --ds-font-family-heading: ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-family-body: ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-family-code: ui-monospace, Menlo, \"Segoe UI Mono\", \"Ubuntu Mono\", monospace;\n  --ds-font-family-brand-heading: \"Charlie Display\", ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-family-brand-body: \"Charlie Text\", ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n}\n";

  var atlassianTypographyAdg3$1 = /*#__PURE__*/Object.freeze({
    __proto__: null,
    'default': atlassianTypographyAdg3
  });

  /**
   * THIS FILE WAS CREATED VIA CODEGEN DO NOT MODIFY {@see http://go/af-codegen}
   * @codegen <<SignedSource::57c7e229053412965e492af01b042778>>
   * @codegenCommand yarn build tokens
   */
  var atlassianShape = "\nhtml[data-theme~=\"shape:shape\"] {\n  --ds-border-radius-050: 0.125rem;\n  --ds-border-radius: 0.25rem;\n  --ds-border-radius-100: 0.25rem;\n  --ds-border-radius-200: 0.5rem;\n  --ds-border-radius-300: 0.75rem;\n  --ds-border-radius-400: 1rem;\n  --ds-border-radius-circle: 2002rem;\n  --ds-border-width: 0.0625rem;\n  --ds-border-width-0: 0rem;\n  --ds-border-width-indicator: 0.1875rem;\n  --ds-border-width-outline: 0.125rem;\n}\n";

  var atlassianShape$1 = /*#__PURE__*/Object.freeze({
    __proto__: null,
    'default': atlassianShape
  });

  /**
   * THIS FILE WAS CREATED VIA CODEGEN DO NOT MODIFY {@see http://go/af-codegen}
   * @codegen <<SignedSource::04ce6f89b744c2a0ec7cf592807e1ba2>>
   * @codegenCommand yarn build tokens
   */
  var atlassianTypographyModernized = "\nhtml[data-theme~=\"typography:typography-modernized\"] {\n  --ds-UNSAFE-textTransformUppercase: uppercase;\n  --ds-font-heading-xxlarge: normal 653 2rem/2.25rem ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-heading-xlarge: normal 653 1.75rem/2rem ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-heading-large: normal 653 1.5rem/1.75rem ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-heading-medium: normal 653 1.25rem/1.5rem ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-heading-small: normal 653 1rem/1.25rem ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-heading-xsmall: normal 653 0.875rem/1.25rem ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-heading-xxsmall: normal 653 0.75rem/1rem ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-body-large: normal 400 1rem/1.5rem ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-body: normal 400 0.875rem/1.25rem ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-body-small: normal 400 0.75rem/1rem ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-body-UNSAFE_small: normal 400 0.75rem/1rem ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-code: normal 400 0.875em/1 ui-monospace, Menlo, \"Segoe UI Mono\", \"Ubuntu Mono\", monospace;\n  --ds-font-weight-regular: 400;\n  --ds-font-weight-medium: 500;\n  --ds-font-weight-semibold: 600;\n  --ds-font-weight-bold: 653;\n  --ds-font-family-heading: ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-family-body: ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-family-code: ui-monospace, Menlo, \"Segoe UI Mono\", \"Ubuntu Mono\", monospace;\n  --ds-font-family-brand-heading: \"Charlie Display\", ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-family-brand-body: \"Charlie Text\", ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n}\n";

  var atlassianTypographyModernized$1 = /*#__PURE__*/Object.freeze({
    __proto__: null,
    'default': atlassianTypographyModernized
  });

  /**
   * THIS FILE WAS CREATED VIA CODEGEN DO NOT MODIFY {@see http://go/af-codegen}
   * @codegen <<SignedSource::bf2bf094d07b0dde7b12bdde7afc0493>>
   * @codegenCommand yarn build tokens
   */
  var atlassianTypographyRefreshed = "\nhtml[data-theme~=\"typography:typography-refreshed\"] {\n  --ds-UNSAFE-textTransformUppercase: uppercase;\n  --ds-font-heading-xxlarge: normal 653 2rem/2.25rem \"Atlassian Sans\", ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-heading-xlarge: normal 653 1.75rem/2rem \"Atlassian Sans\", ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-heading-large: normal 653 1.5rem/1.75rem \"Atlassian Sans\", ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-heading-medium: normal 653 1.25rem/1.5rem \"Atlassian Sans\", ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-heading-small: normal 653 1rem/1.25rem \"Atlassian Sans\", ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-heading-xsmall: normal 653 0.875rem/1.25rem \"Atlassian Sans\", ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-heading-xxsmall: normal 653 0.75rem/1rem \"Atlassian Sans\", ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-body-large: normal 400 1rem/1.5rem \"Atlassian Sans\", ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-body: normal 400 0.875rem/1.25rem \"Atlassian Sans\", ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-body-small: normal 400 0.75rem/1rem \"Atlassian Sans\", ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-body-UNSAFE_small: normal 400 0.75rem/1rem \"Atlassian Sans\", ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-code: normal 400 0.875em/1 \"Atlassian Mono\", ui-monospace, Menlo, \"Segoe UI Mono\", \"Ubuntu Mono\", monospace;\n  --ds-font-weight-regular: 400;\n  --ds-font-weight-medium: 500;\n  --ds-font-weight-semibold: 600;\n  --ds-font-weight-bold: 653;\n  --ds-font-family-heading: \"Atlassian Sans\", ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-family-body: \"Atlassian Sans\", ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-family-code: \"Atlassian Mono\", ui-monospace, Menlo, \"Segoe UI Mono\", \"Ubuntu Mono\", monospace;\n  --ds-font-family-brand-heading: \"Charlie Display\", ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n  --ds-font-family-brand-body: \"Charlie Text\", ui-sans-serif, -apple-system, BlinkMacSystemFont, \"Segoe UI\", Ubuntu, system-ui, \"Helvetica Neue\", sans-serif;\n}\n";

  var atlassianTypographyRefreshed$1 = /*#__PURE__*/Object.freeze({
    __proto__: null,
    'default': atlassianTypographyRefreshed
  });

  /**
   * THIS FILE WAS CREATED VIA CODEGEN DO NOT MODIFY {@see http://go/af-codegen}
   * @codegen <<SignedSource::c366b35cbbeedcaf13c5d825a911a14a>>
   * @codegenCommand yarn build tokens
   */
  var atlassianLightBrandRefresh = "\nhtml[data-color-mode=\"light\"][data-theme~=\"light:light\"],\nhtml[data-color-mode=\"dark\"][data-theme~=\"dark:light\"] {\n  color-scheme: light;\n  --ds-text: #292A2E;\n  --ds-text-accent-lime: #4C6B1F;\n  --ds-text-accent-lime-bolder: #37471F;\n  --ds-text-accent-red: #AE2E24;\n  --ds-text-accent-red-bolder: #5D1F1A;\n  --ds-text-accent-orange: #9E4C00;\n  --ds-text-accent-orange-bolder: #693200;\n  --ds-text-accent-yellow: #7F5F01;\n  --ds-text-accent-yellow-bolder: #533F04;\n  --ds-text-accent-green: #216E4E;\n  --ds-text-accent-green-bolder: #164B35;\n  --ds-text-accent-teal: #206A83;\n  --ds-text-accent-teal-bolder: #164555;\n  --ds-text-accent-blue: #1558BC;\n  --ds-text-accent-blue-bolder: #123263;\n  --ds-text-accent-purple: #803FA5;\n  --ds-text-accent-purple-bolder: #48245D;\n  --ds-text-accent-magenta: #943D73;\n  --ds-text-accent-magenta-bolder: #50253F;\n  --ds-text-accent-gray: #505258;\n  --ds-text-accent-gray-bolder: #1E1F21;\n  --ds-text-disabled: #080F214A;\n  --ds-text-inverse: #FFFFFF;\n  --ds-text-selected: #1868DB;\n  --ds-text-brand: #1868DB;\n  --ds-text-danger: #AE2E24;\n  --ds-text-warning: #9E4C00;\n  --ds-text-warning-inverse: #292A2E;\n  --ds-text-success: #4C6B1F;\n  --ds-text-discovery: #803FA5;\n  --ds-text-information: #1558BC;\n  --ds-text-subtlest: #6B6E76;\n  --ds-text-subtle: #505258;\n  --ds-link: #1868DB;\n  --ds-link-pressed: #1558BC;\n  --ds-link-visited: #803FA5;\n  --ds-link-visited-pressed: #48245D;\n  --ds-icon: #292A2E;\n  --ds-icon-accent-lime: #6A9A23;\n  --ds-icon-accent-red: #C9372C;\n  --ds-icon-accent-orange: #E06C00;\n  --ds-icon-accent-yellow: #B38600;\n  --ds-icon-accent-green: #22A06B;\n  --ds-icon-accent-teal: #2898BD;\n  --ds-icon-accent-blue: #357DE8;\n  --ds-icon-accent-purple: #AF59E1;\n  --ds-icon-accent-magenta: #CD519D;\n  --ds-icon-accent-gray: #7D818A;\n  --ds-icon-disabled: #080F214A;\n  --ds-icon-inverse: #FFFFFF;\n  --ds-icon-selected: #1868DB;\n  --ds-icon-brand: #1868DB;\n  --ds-icon-danger: #C9372C;\n  --ds-icon-warning: #E06C00;\n  --ds-icon-warning-inverse: #292A2E;\n  --ds-icon-success: #6A9A23;\n  --ds-icon-discovery: #AF59E1;\n  --ds-icon-information: #357DE8;\n  --ds-icon-subtlest: #6B6E76;\n  --ds-icon-subtle: #505258;\n  --ds-border: #0B120E24;\n  --ds-border-accent-lime: #6A9A23;\n  --ds-border-accent-red: #E2483D;\n  --ds-border-accent-orange: #E06C00;\n  --ds-border-accent-yellow: #B38600;\n  --ds-border-accent-green: #22A06B;\n  --ds-border-accent-teal: #2898BD;\n  --ds-border-accent-blue: #357DE8;\n  --ds-border-accent-purple: #AF59E1;\n  --ds-border-accent-magenta: #CD519D;\n  --ds-border-accent-gray: #7D818A;\n  --ds-border-disabled: #0515240F;\n  --ds-border-focused: #4688EC;\n  --ds-border-input: #8C8F97;\n  --ds-border-inverse: #FFFFFF;\n  --ds-border-selected: #1868DB;\n  --ds-border-brand: #1868DB;\n  --ds-border-danger: #E2483D;\n  --ds-border-warning: #E06C00;\n  --ds-border-success: #6A9A23;\n  --ds-border-discovery: #AF59E1;\n  --ds-border-information: #357DE8;\n  --ds-border-bold: #7D818A;\n  --ds-background-accent-lime-subtlest: #EFFFD6;\n  --ds-background-accent-lime-subtlest-hovered: #D3F1A7;\n  --ds-background-accent-lime-subtlest-pressed: #B3DF72;\n  --ds-background-accent-lime-subtler: #D3F1A7;\n  --ds-background-accent-lime-subtler-hovered: #B3DF72;\n  --ds-background-accent-lime-subtler-pressed: #94C748;\n  --ds-background-accent-lime-subtle: #94C748;\n  --ds-background-accent-lime-subtle-hovered: #B3DF72;\n  --ds-background-accent-lime-subtle-pressed: #D3F1A7;\n  --ds-background-accent-lime-bolder: #5B7F24;\n  --ds-background-accent-lime-bolder-hovered: #4C6B1F;\n  --ds-background-accent-lime-bolder-pressed: #37471F;\n  --ds-background-accent-red-subtlest: #FFECEB;\n  --ds-background-accent-red-subtlest-hovered: #FFD5D2;\n  --ds-background-accent-red-subtlest-pressed: #FD9891;\n  --ds-background-accent-red-subtler: #FFD5D2;\n  --ds-background-accent-red-subtler-hovered: #FD9891;\n  --ds-background-accent-red-subtler-pressed: #F87168;\n  --ds-background-accent-red-subtle: #F87168;\n  --ds-background-accent-red-subtle-hovered: #FD9891;\n  --ds-background-accent-red-subtle-pressed: #FFD5D2;\n  --ds-background-accent-red-bolder: #C9372C;\n  --ds-background-accent-red-bolder-hovered: #AE2E24;\n  --ds-background-accent-red-bolder-pressed: #5D1F1A;\n  --ds-background-accent-orange-subtlest: #FFF5DB;\n  --ds-background-accent-orange-subtlest-hovered: #FCE4A6;\n  --ds-background-accent-orange-subtlest-pressed: #FBC828;\n  --ds-background-accent-orange-subtler: #FCE4A6;\n  --ds-background-accent-orange-subtler-hovered: #FBC828;\n  --ds-background-accent-orange-subtler-pressed: #FCA700;\n  --ds-background-accent-orange-subtle: #FCA700;\n  --ds-background-accent-orange-subtle-hovered: #FBC828;\n  --ds-background-accent-orange-subtle-pressed: #FCE4A6;\n  --ds-background-accent-orange-bolder: #BD5B00;\n  --ds-background-accent-orange-bolder-hovered: #9E4C00;\n  --ds-background-accent-orange-bolder-pressed: #693200;\n  --ds-background-accent-yellow-subtlest: #FEF7C8;\n  --ds-background-accent-yellow-subtlest-hovered: #F5E989;\n  --ds-background-accent-yellow-subtlest-pressed: #EED12B;\n  --ds-background-accent-yellow-subtler: #F5E989;\n  --ds-background-accent-yellow-subtler-hovered: #EED12B;\n  --ds-background-accent-yellow-subtler-pressed: #DDB30E;\n  --ds-background-accent-yellow-subtle: #EED12B;\n  --ds-background-accent-yellow-subtle-hovered: #DDB30E;\n  --ds-background-accent-yellow-subtle-pressed: #CF9F02;\n  --ds-background-accent-yellow-bolder: #946F00;\n  --ds-background-accent-yellow-bolder-hovered: #7F5F01;\n  --ds-background-accent-yellow-bolder-pressed: #533F04;\n  --ds-background-accent-green-subtlest: #DCFFF1;\n  --ds-background-accent-green-subtlest-hovered: #BAF3DB;\n  --ds-background-accent-green-subtlest-pressed: #7EE2B8;\n  --ds-background-accent-green-subtler: #BAF3DB;\n  --ds-background-accent-green-subtler-hovered: #7EE2B8;\n  --ds-background-accent-green-subtler-pressed: #4BCE97;\n  --ds-background-accent-green-subtle: #4BCE97;\n  --ds-background-accent-green-subtle-hovered: #7EE2B8;\n  --ds-background-accent-green-subtle-pressed: #BAF3DB;\n  --ds-background-accent-green-bolder: #1F845A;\n  --ds-background-accent-green-bolder-hovered: #216E4E;\n  --ds-background-accent-green-bolder-pressed: #164B35;\n  --ds-background-accent-teal-subtlest: #E7F9FF;\n  --ds-background-accent-teal-subtlest-hovered: #C6EDFB;\n  --ds-background-accent-teal-subtlest-pressed: #9DD9EE;\n  --ds-background-accent-teal-subtler: #C6EDFB;\n  --ds-background-accent-teal-subtler-hovered: #9DD9EE;\n  --ds-background-accent-teal-subtler-pressed: #6CC3E0;\n  --ds-background-accent-teal-subtle: #6CC3E0;\n  --ds-background-accent-teal-subtle-hovered: #9DD9EE;\n  --ds-background-accent-teal-subtle-pressed: #C6EDFB;\n  --ds-background-accent-teal-bolder: #227D9B;\n  --ds-background-accent-teal-bolder-hovered: #206A83;\n  --ds-background-accent-teal-bolder-pressed: #164555;\n  --ds-background-accent-blue-subtlest: #E9F2FE;\n  --ds-background-accent-blue-subtlest-hovered: #CFE1FD;\n  --ds-background-accent-blue-subtlest-pressed: #8FB8F6;\n  --ds-background-accent-blue-subtler: #CFE1FD;\n  --ds-background-accent-blue-subtler-hovered: #8FB8F6;\n  --ds-background-accent-blue-subtler-pressed: #669DF1;\n  --ds-background-accent-blue-subtle: #669DF1;\n  --ds-background-accent-blue-subtle-hovered: #8FB8F6;\n  --ds-background-accent-blue-subtle-pressed: #CFE1FD;\n  --ds-background-accent-blue-bolder: #1868DB;\n  --ds-background-accent-blue-bolder-hovered: #1558BC;\n  --ds-background-accent-blue-bolder-pressed: #123263;\n  --ds-background-accent-purple-subtlest: #F8EEFE;\n  --ds-background-accent-purple-subtlest-hovered: #EED7FC;\n  --ds-background-accent-purple-subtlest-pressed: #D8A0F7;\n  --ds-background-accent-purple-subtler: #EED7FC;\n  --ds-background-accent-purple-subtler-hovered: #D8A0F7;\n  --ds-background-accent-purple-subtler-pressed: #C97CF4;\n  --ds-background-accent-purple-subtle: #C97CF4;\n  --ds-background-accent-purple-subtle-hovered: #D8A0F7;\n  --ds-background-accent-purple-subtle-pressed: #EED7FC;\n  --ds-background-accent-purple-bolder: #964AC0;\n  --ds-background-accent-purple-bolder-hovered: #803FA5;\n  --ds-background-accent-purple-bolder-pressed: #48245D;\n  --ds-background-accent-magenta-subtlest: #FFECF8;\n  --ds-background-accent-magenta-subtlest-hovered: #FDD0EC;\n  --ds-background-accent-magenta-subtlest-pressed: #F797D2;\n  --ds-background-accent-magenta-subtler: #FDD0EC;\n  --ds-background-accent-magenta-subtler-hovered: #F797D2;\n  --ds-background-accent-magenta-subtler-pressed: #E774BB;\n  --ds-background-accent-magenta-subtle: #E774BB;\n  --ds-background-accent-magenta-subtle-hovered: #F797D2;\n  --ds-background-accent-magenta-subtle-pressed: #FDD0EC;\n  --ds-background-accent-magenta-bolder: #AE4787;\n  --ds-background-accent-magenta-bolder-hovered: #943D73;\n  --ds-background-accent-magenta-bolder-pressed: #50253F;\n  --ds-background-accent-gray-subtlest: #F0F1F2;\n  --ds-background-accent-gray-subtlest-hovered: #DDDEE1;\n  --ds-background-accent-gray-subtlest-pressed: #B7B9BE;\n  --ds-background-accent-gray-subtler: #DDDEE1;\n  --ds-background-accent-gray-subtler-hovered: #B7B9BE;\n  --ds-background-accent-gray-subtler-pressed: #8C8F97;\n  --ds-background-accent-gray-subtle: #8C8F97;\n  --ds-background-accent-gray-subtle-hovered: #B7B9BE;\n  --ds-background-accent-gray-subtle-pressed: #DDDEE1;\n  --ds-background-accent-gray-bolder: #6B6E76;\n  --ds-background-accent-gray-bolder-hovered: #505258;\n  --ds-background-accent-gray-bolder-pressed: #3B3D42;\n  --ds-background-disabled: #17171708;\n  --ds-background-input: #FFFFFF;\n  --ds-background-input-hovered: #F8F8F8;\n  --ds-background-input-pressed: #FFFFFF;\n  --ds-background-inverse-subtle: #00000029;\n  --ds-background-inverse-subtle-hovered: #0000003D;\n  --ds-background-inverse-subtle-pressed: #00000052;\n  --ds-background-neutral: #0515240F;\n  --ds-background-neutral-hovered: #0B120E24;\n  --ds-background-neutral-pressed: #080F214A;\n  --ds-background-neutral-subtle: #00000000;\n  --ds-background-neutral-subtle-hovered: #0515240F;\n  --ds-background-neutral-subtle-pressed: #0B120E24;\n  --ds-background-neutral-bold: #292A2E;\n  --ds-background-neutral-bold-hovered: #3B3D42;\n  --ds-background-neutral-bold-pressed: #505258;\n  --ds-background-selected: #E9F2FE;\n  --ds-background-selected-hovered: #CFE1FD;\n  --ds-background-selected-pressed: #8FB8F6;\n  --ds-background-selected-bold: #1868DB;\n  --ds-background-selected-bold-hovered: #1558BC;\n  --ds-background-selected-bold-pressed: #123263;\n  --ds-background-brand-subtlest: #E9F2FE;\n  --ds-background-brand-subtlest-hovered: #CFE1FD;\n  --ds-background-brand-subtlest-pressed: #8FB8F6;\n  --ds-background-brand-bold: #1868DB;\n  --ds-background-brand-bold-hovered: #1558BC;\n  --ds-background-brand-bold-pressed: #123263;\n  --ds-background-brand-boldest: #1C2B42;\n  --ds-background-brand-boldest-hovered: #123263;\n  --ds-background-brand-boldest-pressed: #1558BC;\n  --ds-background-danger: #FFECEB;\n  --ds-background-danger-hovered: #FFD5D2;\n  --ds-background-danger-pressed: #FD9891;\n  --ds-background-danger-bold: #C9372C;\n  --ds-background-danger-bold-hovered: #AE2E24;\n  --ds-background-danger-bold-pressed: #5D1F1A;\n  --ds-background-warning: #FFF5DB;\n  --ds-background-warning-hovered: #FCE4A6;\n  --ds-background-warning-pressed: #FBC828;\n  --ds-background-warning-bold: #FBC828;\n  --ds-background-warning-bold-hovered: #FCA700;\n  --ds-background-warning-bold-pressed: #F68909;\n  --ds-background-success: #EFFFD6;\n  --ds-background-success-hovered: #D3F1A7;\n  --ds-background-success-pressed: #B3DF72;\n  --ds-background-success-bold: #5B7F24;\n  --ds-background-success-bold-hovered: #4C6B1F;\n  --ds-background-success-bold-pressed: #37471F;\n  --ds-background-discovery: #F8EEFE;\n  --ds-background-discovery-hovered: #EED7FC;\n  --ds-background-discovery-pressed: #D8A0F7;\n  --ds-background-discovery-bold: #964AC0;\n  --ds-background-discovery-bold-hovered: #803FA5;\n  --ds-background-discovery-bold-pressed: #48245D;\n  --ds-background-information: #E9F2FE;\n  --ds-background-information-hovered: #CFE1FD;\n  --ds-background-information-pressed: #8FB8F6;\n  --ds-background-information-bold: #1868DB;\n  --ds-background-information-bold-hovered: #1558BC;\n  --ds-background-information-bold-pressed: #123263;\n  --ds-blanket: #050C1F75;\n  --ds-blanket-selected: #388BFF14;\n  --ds-blanket-danger: #EF5C4814;\n  --ds-interaction-hovered: #00000029;\n  --ds-interaction-pressed: #00000052;\n  --ds-skeleton: #0515240F;\n  --ds-skeleton-subtle: #17171708;\n  --ds-chart-categorical-1: #357DE8;\n  --ds-chart-categorical-1-hovered: #1868DB;\n  --ds-chart-categorical-2: #82B536;\n  --ds-chart-categorical-2-hovered: #6A9A23;\n  --ds-chart-categorical-3: #BF63F3;\n  --ds-chart-categorical-3-hovered: #AF59E1;\n  --ds-chart-categorical-4: #F68909;\n  --ds-chart-categorical-4-hovered: #E06C00;\n  --ds-chart-categorical-5: #1558BC;\n  --ds-chart-categorical-5-hovered: #123263;\n  --ds-chart-categorical-6: #964AC0;\n  --ds-chart-categorical-6-hovered: #803FA5;\n  --ds-chart-categorical-7: #42B2D7;\n  --ds-chart-categorical-7-hovered: #2898BD;\n  --ds-chart-categorical-8: #BD5B00;\n  --ds-chart-categorical-8-hovered: #9E4C00;\n  --ds-chart-lime-bold: #6A9A23;\n  --ds-chart-lime-bold-hovered: #5B7F24;\n  --ds-chart-lime-bolder: #5B7F24;\n  --ds-chart-lime-bolder-hovered: #4C6B1F;\n  --ds-chart-lime-boldest: #4C6B1F;\n  --ds-chart-lime-boldest-hovered: #37471F;\n  --ds-chart-neutral: #8C8F97;\n  --ds-chart-neutral-hovered: #7D818A;\n  --ds-chart-red-bold: #F15B50;\n  --ds-chart-red-bold-hovered: #E2483D;\n  --ds-chart-red-bolder: #E2483D;\n  --ds-chart-red-bolder-hovered: #C9372C;\n  --ds-chart-red-boldest: #AE2E24;\n  --ds-chart-red-boldest-hovered: #5D1F1A;\n  --ds-chart-orange-bold: #E06C00;\n  --ds-chart-orange-bold-hovered: #BD5B00;\n  --ds-chart-orange-bolder: #BD5B00;\n  --ds-chart-orange-bolder-hovered: #9E4C00;\n  --ds-chart-orange-boldest: #9E4C00;\n  --ds-chart-orange-boldest-hovered: #693200;\n  --ds-chart-yellow-bold: #B38600;\n  --ds-chart-yellow-bold-hovered: #946F00;\n  --ds-chart-yellow-bolder: #946F00;\n  --ds-chart-yellow-bolder-hovered: #7F5F01;\n  --ds-chart-yellow-boldest: #7F5F01;\n  --ds-chart-yellow-boldest-hovered: #533F04;\n  --ds-chart-green-bold: #22A06B;\n  --ds-chart-green-bold-hovered: #1F845A;\n  --ds-chart-green-bolder: #1F845A;\n  --ds-chart-green-bolder-hovered: #216E4E;\n  --ds-chart-green-boldest: #216E4E;\n  --ds-chart-green-boldest-hovered: #164B35;\n  --ds-chart-teal-bold: #2898BD;\n  --ds-chart-teal-bold-hovered: #227D9B;\n  --ds-chart-teal-bolder: #227D9B;\n  --ds-chart-teal-bolder-hovered: #206A83;\n  --ds-chart-teal-boldest: #206A83;\n  --ds-chart-teal-boldest-hovered: #164555;\n  --ds-chart-blue-bold: #4688EC;\n  --ds-chart-blue-bold-hovered: #357DE8;\n  --ds-chart-blue-bolder: #357DE8;\n  --ds-chart-blue-bolder-hovered: #1868DB;\n  --ds-chart-blue-boldest: #1558BC;\n  --ds-chart-blue-boldest-hovered: #123263;\n  --ds-chart-purple-bold: #BF63F3;\n  --ds-chart-purple-bold-hovered: #AF59E1;\n  --ds-chart-purple-bolder: #AF59E1;\n  --ds-chart-purple-bolder-hovered: #964AC0;\n  --ds-chart-purple-boldest: #803FA5;\n  --ds-chart-purple-boldest-hovered: #48245D;\n  --ds-chart-magenta-bold: #DA62AC;\n  --ds-chart-magenta-bold-hovered: #CD519D;\n  --ds-chart-magenta-bolder: #CD519D;\n  --ds-chart-magenta-bolder-hovered: #AE4787;\n  --ds-chart-magenta-boldest: #943D73;\n  --ds-chart-magenta-boldest-hovered: #50253F;\n  --ds-chart-gray-bold: #8C8F97;\n  --ds-chart-gray-bold-hovered: #7D818A;\n  --ds-chart-gray-bolder: #7D818A;\n  --ds-chart-gray-bolder-hovered: #6B6E76;\n  --ds-chart-gray-boldest: #505258;\n  --ds-chart-gray-boldest-hovered: #3B3D42;\n  --ds-chart-brand: #357DE8;\n  --ds-chart-brand-hovered: #1868DB;\n  --ds-chart-danger: #E2483D;\n  --ds-chart-danger-hovered: #C9372C;\n  --ds-chart-danger-bold: #AE2E24;\n  --ds-chart-danger-bold-hovered: #5D1F1A;\n  --ds-chart-warning: #F68909;\n  --ds-chart-warning-hovered: #E06C00;\n  --ds-chart-warning-bold: #BD5B00;\n  --ds-chart-warning-bold-hovered: #9E4C00;\n  --ds-chart-success: #82B536;\n  --ds-chart-success-hovered: #6A9A23;\n  --ds-chart-success-bold: #5B7F24;\n  --ds-chart-success-bold-hovered: #4C6B1F;\n  --ds-chart-discovery: #BF63F3;\n  --ds-chart-discovery-hovered: #AF59E1;\n  --ds-chart-discovery-bold: #803FA5;\n  --ds-chart-discovery-bold-hovered: #48245D;\n  --ds-chart-information: #357DE8;\n  --ds-chart-information-hovered: #1868DB;\n  --ds-chart-information-bold: #1558BC;\n  --ds-chart-information-bold-hovered: #123263;\n  --ds-surface: #FFFFFF;\n  --ds-surface-hovered: #F0F1F2;\n  --ds-surface-pressed: #DDDEE1;\n  --ds-surface-overlay: #FFFFFF;\n  --ds-surface-overlay-hovered: #F0F1F2;\n  --ds-surface-overlay-pressed: #DDDEE1;\n  --ds-surface-raised: #FFFFFF;\n  --ds-surface-raised-hovered: #F0F1F2;\n  --ds-surface-raised-pressed: #DDDEE1;\n  --ds-surface-sunken: #F8F8F8;\n  --ds-shadow-overflow: 0px 0px 8px #1E1F2129, 0px 0px 1px #1E1F211F;\n  --ds-shadow-overflow-perimeter: #1E1F211f;\n  --ds-shadow-overflow-spread: #1E1F2129;\n  --ds-shadow-overlay: 0px 8px 12px #1E1F2126, 0px 0px 1px #1E1F214F;\n  --ds-shadow-raised: 0px 1px 1px #1E1F2140, 0px 0px 1px #1E1F214F;\n  --ds-opacity-disabled: 0.4;\n  --ds-opacity-loading: 0.2;\n  --ds-UNSAFE-transparent: transparent;\n  --ds-elevation-surface-current: #FFFFFF;\n}\n";

  var atlassianLightBrandRefresh$1 = /*#__PURE__*/Object.freeze({
    __proto__: null,
    'default': atlassianLightBrandRefresh
  });

  /**
   * THIS FILE WAS CREATED VIA CODEGEN DO NOT MODIFY {@see http://go/af-codegen}
   * @codegen <<SignedSource::79c6a5e270639e09058c67c298dbdce4>>
   * @codegenCommand yarn build tokens
   */
  var atlassianDarkBrandRefresh = "\nhtml[data-color-mode=\"light\"][data-theme~=\"light:dark\"],\nhtml[data-color-mode=\"dark\"][data-theme~=\"dark:dark\"] {\n  color-scheme: dark;\n  --ds-text: #BFC1C4;\n  --ds-text-accent-lime: #B3DF72;\n  --ds-text-accent-lime-bolder: #D3F1A7;\n  --ds-text-accent-red: #FD9891;\n  --ds-text-accent-red-bolder: #FFD5D2;\n  --ds-text-accent-orange: #FBC828;\n  --ds-text-accent-orange-bolder: #FCE4A6;\n  --ds-text-accent-yellow: #EED12B;\n  --ds-text-accent-yellow-bolder: #F5E989;\n  --ds-text-accent-green: #7EE2B8;\n  --ds-text-accent-green-bolder: #BAF3DB;\n  --ds-text-accent-teal: #9DD9EE;\n  --ds-text-accent-teal-bolder: #C6EDFB;\n  --ds-text-accent-blue: #8FB8F6;\n  --ds-text-accent-blue-bolder: #CFE1FD;\n  --ds-text-accent-purple: #D8A0F7;\n  --ds-text-accent-purple-bolder: #EED7FC;\n  --ds-text-accent-magenta: #F797D2;\n  --ds-text-accent-magenta-bolder: #FDD0EC;\n  --ds-text-accent-gray: #A9ABAF;\n  --ds-text-accent-gray-bolder: #E2E3E4;\n  --ds-text-disabled: #E5E9F640;\n  --ds-text-inverse: #1F1F21;\n  --ds-text-selected: #669DF1;\n  --ds-text-brand: #669DF1;\n  --ds-text-danger: #FD9891;\n  --ds-text-warning: #FBC828;\n  --ds-text-warning-inverse: #1F1F21;\n  --ds-text-success: #B3DF72;\n  --ds-text-discovery: #D8A0F7;\n  --ds-text-information: #8FB8F6;\n  --ds-text-subtlest: #96999E;\n  --ds-text-subtle: #A9ABAF;\n  --ds-link: #669DF1;\n  --ds-link-pressed: #8FB8F6;\n  --ds-link-visited: #D8A0F7;\n  --ds-link-visited-pressed: #EED7FC;\n  --ds-icon: #CECFD2;\n  --ds-icon-accent-lime: #82B536;\n  --ds-icon-accent-red: #E2483D;\n  --ds-icon-accent-orange: #F68909;\n  --ds-icon-accent-yellow: #EED12B;\n  --ds-icon-accent-green: #2ABB7F;\n  --ds-icon-accent-teal: #42B2D7;\n  --ds-icon-accent-blue: #4688EC;\n  --ds-icon-accent-purple: #BF63F3;\n  --ds-icon-accent-magenta: #DA62AC;\n  --ds-icon-accent-gray: #7E8188;\n  --ds-icon-disabled: #E5E9F640;\n  --ds-icon-inverse: #1F1F21;\n  --ds-icon-selected: #669DF1;\n  --ds-icon-brand: #669DF1;\n  --ds-icon-danger: #F15B50;\n  --ds-icon-warning: #FBC828;\n  --ds-icon-warning-inverse: #1F1F21;\n  --ds-icon-success: #B3DF72;\n  --ds-icon-discovery: #BF63F3;\n  --ds-icon-information: #4688EC;\n  --ds-icon-subtlest: #96999E;\n  --ds-icon-subtle: #A9ABAF;\n  --ds-border: #E3E4F21F;\n  --ds-border-accent-lime: #82B536;\n  --ds-border-accent-red: #F15B50;\n  --ds-border-accent-orange: #F68909;\n  --ds-border-accent-yellow: #CF9F02;\n  --ds-border-accent-green: #2ABB7F;\n  --ds-border-accent-teal: #42B2D7;\n  --ds-border-accent-blue: #4688EC;\n  --ds-border-accent-purple: #BF63F3;\n  --ds-border-accent-magenta: #DA62AC;\n  --ds-border-accent-gray: #7E8188;\n  --ds-border-disabled: #CECED912;\n  --ds-border-focused: #8FB8F6;\n  --ds-border-input: #7E8188;\n  --ds-border-inverse: #18191A;\n  --ds-border-selected: #669DF1;\n  --ds-border-brand: #669DF1;\n  --ds-border-danger: #F15B50;\n  --ds-border-warning: #F68909;\n  --ds-border-success: #82B536;\n  --ds-border-discovery: #BF63F3;\n  --ds-border-information: #4688EC;\n  --ds-border-bold: #7E8188;\n  --ds-background-accent-lime-subtlest: #28311B;\n  --ds-background-accent-lime-subtlest-hovered: #37471F;\n  --ds-background-accent-lime-subtlest-pressed: #4C6B1F;\n  --ds-background-accent-lime-subtler: #37471F;\n  --ds-background-accent-lime-subtler-hovered: #4C6B1F;\n  --ds-background-accent-lime-subtler-pressed: #5B7F24;\n  --ds-background-accent-lime-subtle: #4C6B1F;\n  --ds-background-accent-lime-subtle-hovered: #37471F;\n  --ds-background-accent-lime-subtle-pressed: #28311B;\n  --ds-background-accent-lime-bolder: #94C748;\n  --ds-background-accent-lime-bolder-hovered: #B3DF72;\n  --ds-background-accent-lime-bolder-pressed: #D3F1A7;\n  --ds-background-accent-red-subtlest: #42221F;\n  --ds-background-accent-red-subtlest-hovered: #5D1F1A;\n  --ds-background-accent-red-subtlest-pressed: #AE2E24;\n  --ds-background-accent-red-subtler: #5D1F1A;\n  --ds-background-accent-red-subtler-hovered: #AE2E24;\n  --ds-background-accent-red-subtler-pressed: #C9372C;\n  --ds-background-accent-red-subtle: #AE2E24;\n  --ds-background-accent-red-subtle-hovered: #5D1F1A;\n  --ds-background-accent-red-subtle-pressed: #42221F;\n  --ds-background-accent-red-bolder: #F87168;\n  --ds-background-accent-red-bolder-hovered: #FD9891;\n  --ds-background-accent-red-bolder-pressed: #FFD5D2;\n  --ds-background-accent-orange-subtlest: #3A2C1F;\n  --ds-background-accent-orange-subtlest-hovered: #693200;\n  --ds-background-accent-orange-subtlest-pressed: #9E4C00;\n  --ds-background-accent-orange-subtler: #693200;\n  --ds-background-accent-orange-subtler-hovered: #9E4C00;\n  --ds-background-accent-orange-subtler-pressed: #BD5B00;\n  --ds-background-accent-orange-subtle: #9E4C00;\n  --ds-background-accent-orange-subtle-hovered: #693200;\n  --ds-background-accent-orange-subtle-pressed: #3A2C1F;\n  --ds-background-accent-orange-bolder: #FCA700;\n  --ds-background-accent-orange-bolder-hovered: #FBC828;\n  --ds-background-accent-orange-bolder-pressed: #FCE4A6;\n  --ds-background-accent-yellow-subtlest: #332E1B;\n  --ds-background-accent-yellow-subtlest-hovered: #533F04;\n  --ds-background-accent-yellow-subtlest-pressed: #7F5F01;\n  --ds-background-accent-yellow-subtler: #533F04;\n  --ds-background-accent-yellow-subtler-hovered: #7F5F01;\n  --ds-background-accent-yellow-subtler-pressed: #946F00;\n  --ds-background-accent-yellow-subtle: #7F5F01;\n  --ds-background-accent-yellow-subtle-hovered: #533F04;\n  --ds-background-accent-yellow-subtle-pressed: #332E1B;\n  --ds-background-accent-yellow-bolder: #DDB30E;\n  --ds-background-accent-yellow-bolder-hovered: #EED12B;\n  --ds-background-accent-yellow-bolder-pressed: #F5E989;\n  --ds-background-accent-green-subtlest: #1C3329;\n  --ds-background-accent-green-subtlest-hovered: #164B35;\n  --ds-background-accent-green-subtlest-pressed: #216E4E;\n  --ds-background-accent-green-subtler: #164B35;\n  --ds-background-accent-green-subtler-hovered: #216E4E;\n  --ds-background-accent-green-subtler-pressed: #1F845A;\n  --ds-background-accent-green-subtle: #216E4E;\n  --ds-background-accent-green-subtle-hovered: #164B35;\n  --ds-background-accent-green-subtle-pressed: #1C3329;\n  --ds-background-accent-green-bolder: #4BCE97;\n  --ds-background-accent-green-bolder-hovered: #7EE2B8;\n  --ds-background-accent-green-bolder-pressed: #BAF3DB;\n  --ds-background-accent-teal-subtlest: #1E3137;\n  --ds-background-accent-teal-subtlest-hovered: #164555;\n  --ds-background-accent-teal-subtlest-pressed: #206A83;\n  --ds-background-accent-teal-subtler: #164555;\n  --ds-background-accent-teal-subtler-hovered: #206A83;\n  --ds-background-accent-teal-subtler-pressed: #227D9B;\n  --ds-background-accent-teal-subtle: #206A83;\n  --ds-background-accent-teal-subtle-hovered: #164555;\n  --ds-background-accent-teal-subtle-pressed: #1E3137;\n  --ds-background-accent-teal-bolder: #6CC3E0;\n  --ds-background-accent-teal-bolder-hovered: #9DD9EE;\n  --ds-background-accent-teal-bolder-pressed: #C6EDFB;\n  --ds-background-accent-blue-subtlest: #1C2B42;\n  --ds-background-accent-blue-subtlest-hovered: #123263;\n  --ds-background-accent-blue-subtlest-pressed: #1558BC;\n  --ds-background-accent-blue-subtler: #123263;\n  --ds-background-accent-blue-subtler-hovered: #1558BC;\n  --ds-background-accent-blue-subtler-pressed: #1868DB;\n  --ds-background-accent-blue-subtle: #1558BC;\n  --ds-background-accent-blue-subtle-hovered: #123263;\n  --ds-background-accent-blue-subtle-pressed: #1C2B42;\n  --ds-background-accent-blue-bolder: #669DF1;\n  --ds-background-accent-blue-bolder-hovered: #8FB8F6;\n  --ds-background-accent-blue-bolder-pressed: #CFE1FD;\n  --ds-background-accent-purple-subtlest: #35243F;\n  --ds-background-accent-purple-subtlest-hovered: #48245D;\n  --ds-background-accent-purple-subtlest-pressed: #803FA5;\n  --ds-background-accent-purple-subtler: #48245D;\n  --ds-background-accent-purple-subtler-hovered: #803FA5;\n  --ds-background-accent-purple-subtler-pressed: #964AC0;\n  --ds-background-accent-purple-subtle: #803FA5;\n  --ds-background-accent-purple-subtle-hovered: #48245D;\n  --ds-background-accent-purple-subtle-pressed: #35243F;\n  --ds-background-accent-purple-bolder: #C97CF4;\n  --ds-background-accent-purple-bolder-hovered: #D8A0F7;\n  --ds-background-accent-purple-bolder-pressed: #EED7FC;\n  --ds-background-accent-magenta-subtlest: #3D2232;\n  --ds-background-accent-magenta-subtlest-hovered: #50253F;\n  --ds-background-accent-magenta-subtlest-pressed: #943D73;\n  --ds-background-accent-magenta-subtler: #50253F;\n  --ds-background-accent-magenta-subtler-hovered: #943D73;\n  --ds-background-accent-magenta-subtler-pressed: #AE4787;\n  --ds-background-accent-magenta-subtle: #943D73;\n  --ds-background-accent-magenta-subtle-hovered: #50253F;\n  --ds-background-accent-magenta-subtle-pressed: #3D2232;\n  --ds-background-accent-magenta-bolder: #E774BB;\n  --ds-background-accent-magenta-bolder-hovered: #F797D2;\n  --ds-background-accent-magenta-bolder-pressed: #FDD0EC;\n  --ds-background-accent-gray-subtlest: #303134;\n  --ds-background-accent-gray-subtlest-hovered: #3D3F43;\n  --ds-background-accent-gray-subtlest-pressed: #4B4D51;\n  --ds-background-accent-gray-subtler: #4B4D51;\n  --ds-background-accent-gray-subtler-hovered: #63666B;\n  --ds-background-accent-gray-subtler-pressed: #7E8188;\n  --ds-background-accent-gray-subtle: #63666B;\n  --ds-background-accent-gray-subtle-hovered: #4B4D51;\n  --ds-background-accent-gray-subtle-pressed: #3D3F43;\n  --ds-background-accent-gray-bolder: #96999E;\n  --ds-background-accent-gray-bolder-hovered: #A9ABAF;\n  --ds-background-accent-gray-bolder-pressed: #BFC1C4;\n  --ds-background-disabled: #BDBDBD0A;\n  --ds-background-input: #242528;\n  --ds-background-input-hovered: #2B2C2F;\n  --ds-background-input-pressed: #242528;\n  --ds-background-inverse-subtle: #FFFFFF29;\n  --ds-background-inverse-subtle-hovered: #FFFFFF3D;\n  --ds-background-inverse-subtle-pressed: #FFFFFF52;\n  --ds-background-neutral: #CECED912;\n  --ds-background-neutral-hovered: #E3E4F21F;\n  --ds-background-neutral-pressed: #E5E9F640;\n  --ds-background-neutral-subtle: #00000000;\n  --ds-background-neutral-subtle-hovered: #CECED912;\n  --ds-background-neutral-subtle-pressed: #E3E4F21F;\n  --ds-background-neutral-bold: #CECFD2;\n  --ds-background-neutral-bold-hovered: #BFC1C4;\n  --ds-background-neutral-bold-pressed: #A9ABAF;\n  --ds-background-selected: #1C2B42;\n  --ds-background-selected-hovered: #123263;\n  --ds-background-selected-pressed: #1558BC;\n  --ds-background-selected-bold: #669DF1;\n  --ds-background-selected-bold-hovered: #8FB8F6;\n  --ds-background-selected-bold-pressed: #CFE1FD;\n  --ds-background-brand-subtlest: #1C2B42;\n  --ds-background-brand-subtlest-hovered: #123263;\n  --ds-background-brand-subtlest-pressed: #1558BC;\n  --ds-background-brand-bold: #669DF1;\n  --ds-background-brand-bold-hovered: #8FB8F6;\n  --ds-background-brand-bold-pressed: #CFE1FD;\n  --ds-background-brand-boldest: #E9F2FE;\n  --ds-background-brand-boldest-hovered: #CFE1FD;\n  --ds-background-brand-boldest-pressed: #8FB8F6;\n  --ds-background-danger: #42221F;\n  --ds-background-danger-hovered: #5D1F1A;\n  --ds-background-danger-pressed: #AE2E24;\n  --ds-background-danger-bold: #F87168;\n  --ds-background-danger-bold-hovered: #FD9891;\n  --ds-background-danger-bold-pressed: #FFD5D2;\n  --ds-background-warning: #3A2C1F;\n  --ds-background-warning-hovered: #693200;\n  --ds-background-warning-pressed: #9E4C00;\n  --ds-background-warning-bold: #FBC828;\n  --ds-background-warning-bold-hovered: #FCA700;\n  --ds-background-warning-bold-pressed: #F68909;\n  --ds-background-success: #28311B;\n  --ds-background-success-hovered: #37471F;\n  --ds-background-success-pressed: #4C6B1F;\n  --ds-background-success-bold: #94C748;\n  --ds-background-success-bold-hovered: #B3DF72;\n  --ds-background-success-bold-pressed: #D3F1A7;\n  --ds-background-discovery: #35243F;\n  --ds-background-discovery-hovered: #48245D;\n  --ds-background-discovery-pressed: #803FA5;\n  --ds-background-discovery-bold: #C97CF4;\n  --ds-background-discovery-bold-hovered: #D8A0F7;\n  --ds-background-discovery-bold-pressed: #EED7FC;\n  --ds-background-information: #1C2B42;\n  --ds-background-information-hovered: #123263;\n  --ds-background-information-pressed: #1558BC;\n  --ds-background-information-bold: #669DF1;\n  --ds-background-information-bold-hovered: #8FB8F6;\n  --ds-background-information-bold-pressed: #CFE1FD;\n  --ds-blanket: #10121499;\n  --ds-blanket-selected: #1D7AFC14;\n  --ds-blanket-danger: #E3493514;\n  --ds-interaction-hovered: #ffffff33;\n  --ds-interaction-pressed: #ffffff5c;\n  --ds-skeleton: #CECED912;\n  --ds-skeleton-subtle: #BDBDBD0A;\n  --ds-chart-categorical-1: #4688EC;\n  --ds-chart-categorical-1-hovered: #669DF1;\n  --ds-chart-categorical-2: #94C748;\n  --ds-chart-categorical-2-hovered: #B3DF72;\n  --ds-chart-categorical-3: #C97CF4;\n  --ds-chart-categorical-3-hovered: #D8A0F7;\n  --ds-chart-categorical-4: #FCA700;\n  --ds-chart-categorical-4-hovered: #FBC828;\n  --ds-chart-categorical-5: #1558BC;\n  --ds-chart-categorical-5-hovered: #1868DB;\n  --ds-chart-categorical-6: #964AC0;\n  --ds-chart-categorical-6-hovered: #AF59E1;\n  --ds-chart-categorical-7: #42B2D7;\n  --ds-chart-categorical-7-hovered: #6CC3E0;\n  --ds-chart-categorical-8: #E06C00;\n  --ds-chart-categorical-8-hovered: #F68909;\n  --ds-chart-lime-bold: #82B536;\n  --ds-chart-lime-bold-hovered: #94C748;\n  --ds-chart-lime-bolder: #94C748;\n  --ds-chart-lime-bolder-hovered: #B3DF72;\n  --ds-chart-lime-boldest: #B3DF72;\n  --ds-chart-lime-boldest-hovered: #D3F1A7;\n  --ds-chart-neutral: #7E8188;\n  --ds-chart-neutral-hovered: #96999E;\n  --ds-chart-red-bold: #E2483D;\n  --ds-chart-red-bold-hovered: #F15B50;\n  --ds-chart-red-bolder: #F15B50;\n  --ds-chart-red-bolder-hovered: #F87168;\n  --ds-chart-red-boldest: #FD9891;\n  --ds-chart-red-boldest-hovered: #FFD5D2;\n  --ds-chart-orange-bold: #F68909;\n  --ds-chart-orange-bold-hovered: #FCA700;\n  --ds-chart-orange-bolder: #FCA700;\n  --ds-chart-orange-bolder-hovered: #FBC828;\n  --ds-chart-orange-boldest: #FBC828;\n  --ds-chart-orange-boldest-hovered: #FCE4A6;\n  --ds-chart-yellow-bold: #CF9F02;\n  --ds-chart-yellow-bold-hovered: #DDB30E;\n  --ds-chart-yellow-bolder: #DDB30E;\n  --ds-chart-yellow-bolder-hovered: #EED12B;\n  --ds-chart-yellow-boldest: #EED12B;\n  --ds-chart-yellow-boldest-hovered: #F5E989;\n  --ds-chart-green-bold: #2ABB7F;\n  --ds-chart-green-bold-hovered: #4BCE97;\n  --ds-chart-green-bolder: #4BCE97;\n  --ds-chart-green-bolder-hovered: #7EE2B8;\n  --ds-chart-green-boldest: #7EE2B8;\n  --ds-chart-green-boldest-hovered: #BAF3DB;\n  --ds-chart-teal-bold: #42B2D7;\n  --ds-chart-teal-bold-hovered: #6CC3E0;\n  --ds-chart-teal-bolder: #6CC3E0;\n  --ds-chart-teal-bolder-hovered: #9DD9EE;\n  --ds-chart-teal-boldest: #9DD9EE;\n  --ds-chart-teal-boldest-hovered: #C6EDFB;\n  --ds-chart-blue-bold: #357DE8;\n  --ds-chart-blue-bold-hovered: #4688EC;\n  --ds-chart-blue-bolder: #4688EC;\n  --ds-chart-blue-bolder-hovered: #669DF1;\n  --ds-chart-blue-boldest: #8FB8F6;\n  --ds-chart-blue-boldest-hovered: #CFE1FD;\n  --ds-chart-purple-bold: #AF59E1;\n  --ds-chart-purple-bold-hovered: #BF63F3;\n  --ds-chart-purple-bolder: #BF63F3;\n  --ds-chart-purple-bolder-hovered: #C97CF4;\n  --ds-chart-purple-boldest: #D8A0F7;\n  --ds-chart-purple-boldest-hovered: #EED7FC;\n  --ds-chart-magenta-bold: #CD519D;\n  --ds-chart-magenta-bold-hovered: #DA62AC;\n  --ds-chart-magenta-bolder: #DA62AC;\n  --ds-chart-magenta-bolder-hovered: #E774BB;\n  --ds-chart-magenta-boldest: #F797D2;\n  --ds-chart-magenta-boldest-hovered: #FDD0EC;\n  --ds-chart-gray-bold: #7E8188;\n  --ds-chart-gray-bold-hovered: #96999E;\n  --ds-chart-gray-bolder: #96999E;\n  --ds-chart-gray-bolder-hovered: #A9ABAF;\n  --ds-chart-gray-boldest: #A9ABAF;\n  --ds-chart-gray-boldest-hovered: #BFC1C4;\n  --ds-chart-brand: #4688EC;\n  --ds-chart-brand-hovered: #669DF1;\n  --ds-chart-danger: #E2483D;\n  --ds-chart-danger-hovered: #F15B50;\n  --ds-chart-danger-bold: #F87168;\n  --ds-chart-danger-bold-hovered: #FD9891;\n  --ds-chart-warning: #F68909;\n  --ds-chart-warning-hovered: #FCA700;\n  --ds-chart-warning-bold: #FBC828;\n  --ds-chart-warning-bold-hovered: #FCE4A6;\n  --ds-chart-success: #82B536;\n  --ds-chart-success-hovered: #94C748;\n  --ds-chart-success-bold: #B3DF72;\n  --ds-chart-success-bold-hovered: #D3F1A7;\n  --ds-chart-discovery: #BF63F3;\n  --ds-chart-discovery-hovered: #C97CF4;\n  --ds-chart-discovery-bold: #D8A0F7;\n  --ds-chart-discovery-bold-hovered: #EED7FC;\n  --ds-chart-information: #4688EC;\n  --ds-chart-information-hovered: #669DF1;\n  --ds-chart-information-bold: #8FB8F6;\n  --ds-chart-information-bold-hovered: #CFE1FD;\n  --ds-surface: #1F1F21;\n  --ds-surface-hovered: #242528;\n  --ds-surface-pressed: #2B2C2F;\n  --ds-surface-overlay: #2B2C2F;\n  --ds-surface-overlay-hovered: #303134;\n  --ds-surface-overlay-pressed: #3D3F43;\n  --ds-surface-raised: #242528;\n  --ds-surface-raised-hovered: #2B2C2F;\n  --ds-surface-raised-pressed: #303134;\n  --ds-surface-sunken: #18191A;\n  --ds-shadow-overflow: 0px 0px 12px #0104048F, 0px 0px 1px #01040480;\n  --ds-shadow-overflow-perimeter: #01040480;\n  --ds-shadow-overflow-spread: #0104048f;\n  --ds-shadow-overlay: 0px 0px 0px 1px #BDBDBD1F, 0px 8px 12px #0104045C, 0px 0px 1px 1px #01040480;\n  --ds-shadow-raised: 0px 0px 0px 1px #00000000, 0px 1px 1px #01040480, 0px 0px 1px #01040480;\n  --ds-opacity-disabled: 0.4;\n  --ds-opacity-loading: 0.2;\n  --ds-UNSAFE-transparent: transparent;\n  --ds-elevation-surface-current: #1F1F21;\n}\n";

  var atlassianDarkBrandRefresh$1 = /*#__PURE__*/Object.freeze({
    __proto__: null,
    'default': atlassianDarkBrandRefresh
  });

  /**
   * THIS FILE WAS CREATED VIA CODEGEN DO NOT MODIFY {@see http://go/af-codegen}
   *
   * Token names mapped to their values, used for contrast checking when generating custom themes
   *
   * @codegen <<SignedSource::023110f136ad929098f2d5a341b7ad7c>>
   * @codegenCommand yarn build tokens
   */
  var tokenValues$1 = {
    'color.text.brand': '#579DFF',
    'elevation.surface.overlay': '#282E33',
    'color.background.selected': '#1C2B41',
    'color.text.selected': '#579DFF',
    'color.border.brand': '#579DFF',
    'color.chart.brand': '#388BFF',
    'color.text.inverse': '#1D2125'
  };
  var rawTokensDark = tokenValues$1;

  /**
   * THIS FILE WAS CREATED VIA CODEGEN DO NOT MODIFY {@see http://go/af-codegen}
   *
   * Token names mapped to their values, used for contrast checking when generating custom themes
   *
   * @codegen <<SignedSource::9975d8c2d3b07e5b405782c2d24fab5f>>
   * @codegenCommand yarn build tokens
   */
  var tokenValues = {
    'color.text.brand': '#0C66E4',
    'elevation.surface.sunken': '#F7F8F9',
    'color.background.selected': '#E9F2FF',
    'color.text.selected': '#0C66E4',
    'color.border.brand': '#0C66E4',
    'color.chart.brand': '#1D7AFC',
    'color.text.inverse': '#FFFFFF'
  };
  var tokenValuesLight = tokenValues;

  var additionalChecks = [{
    foreground: 'color.text.brand',
    backgroundLight: 'elevation.surface.sunken',
    backgroundDark: 'elevation.surface.overlay',
    desiredContrast: 4.5,
    updatedTokens: [
    // In light mode: darken the following tokens by one base token
    // In dark mode: lighten the following tokens by one base token
    'color.text.brand', 'color.text.selected', 'color.link', 'color.link.pressed', 'color.icon.brand', 'color.icon.selected']
  }, {
    foreground: 'color.text.brand',
    backgroundLight: 'color.background.selected',
    backgroundDark: 'color.background.selected',
    desiredContrast: 4.5,
    // In light mode: darken the following tokens by one base token
    // In dark mode: lighten the following tokens by one base toke
    updatedTokens: ['color.text.brand', 'color.link', 'color.link.pressed']
  }, {
    foreground: 'color.text.selected',
    backgroundLight: 'color.background.selected',
    backgroundDark: 'color.background.selected',
    desiredContrast: 4.5,
    // In light mode: darken the following tokens by one base token
    // In dark mode: lighten the following tokens by one base token
    updatedTokens: ['color.text.selected', 'color.icon.selected']
  }, {
    foreground: 'color.border.brand',
    backgroundLight: 'elevation.surface.sunken',
    backgroundDark: 'elevation.surface.overlay',
    desiredContrast: 3,
    // In light mode: darken the following tokens by one base token
    // In dark mode: lighten the following tokens by one base toke
    updatedTokens: ['color.border.brand', 'color.border.selected']
  }, {
    foreground: 'color.chart.brand',
    backgroundLight: 'elevation.surface.sunken',
    backgroundDark: 'elevation.surface.overlay',
    desiredContrast: 3,
    // In light mode: darken the following tokens by one base token
    // In dark mode: lighten the following tokens by one base token
    updatedTokens: ['color.chart.brand', 'color.chart.brand.hovered']
  }];
  var getColorFromTokenRaw = function getColorFromTokenRaw(tokenName, mode) {
    return mode === 'light' ? tokenValuesLight[tokenName] : rawTokensDark[tokenName];
  };
  var additionalContrastChecker = function additionalContrastChecker(_ref) {
    var customThemeTokenMap = _ref.customThemeTokenMap,
      mode = _ref.mode,
      themeRamp = _ref.themeRamp;
    var updatedCustomThemeTokenMap = {};
    var brandTokens = Object.keys(customThemeTokenMap);
    additionalChecks.forEach(function (pairing) {
      var backgroundLight = pairing.backgroundLight,
        backgroundDark = pairing.backgroundDark,
        foreground = pairing.foreground,
        desiredContrast = pairing.desiredContrast,
        updatedTokens = pairing.updatedTokens;
      var background = mode === 'light' ? backgroundLight : backgroundDark;
      var foregroundTokenValue = customThemeTokenMap[foreground];
      var backgroundTokenValue = customThemeTokenMap[background];
      var foregroundColor = brandTokens.includes(foreground) ? typeof foregroundTokenValue === 'string' ? foregroundTokenValue : themeRamp[foregroundTokenValue] : getColorFromTokenRaw(foreground, mode);
      var backgroundColor = brandTokens.includes(background) ? typeof backgroundTokenValue === 'string' ? backgroundTokenValue : themeRamp[backgroundTokenValue] : getColorFromTokenRaw(background, mode);
      var contrast = getContrastRatio(foregroundColor, backgroundColor);
      if (contrast <= desiredContrast) {
        updatedTokens.forEach(function (token) {
          var rampValue = customThemeTokenMap[token];
          if (typeof rampValue === 'number') {
            updatedCustomThemeTokenMap[token] = mode === 'light' ? rampValue + 1 : rampValue - 1;
          }
        });
      }
    });
    return updatedCustomThemeTokenMap;
  };

  /**
   * Below lines are copied from @material/material-color-utilities.
   * Do not modify it.
   */

  /**
   * @license
   * Copyright 2021 Google LLC
   *
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *
   *      http://www.apache.org/licenses/LICENSE-2.0
   *
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
   */

  // This file is automatically generated. Do not modify it.

  /**
   * Utility methods for mathematical operations.
   */

  /**
   * The signum function.
   *
   * @return 1 if num > 0, -1 if num < 0, and 0 if num = 0
   */
  function signum(num) {
    if (num < 0) {
      return -1;
    } else if (num === 0) {
      return 0;
    } else {
      return 1;
    }
  }

  /**
   * The linear interpolation function.
   *
   * @return start if amount = 0 and stop if amount = 1
   */
  function lerp(start, stop, amount) {
    return (1.0 - amount) * start + amount * stop;
  }

  /**
   * Clamps an integer between two integers.
   *
   * @return input when min <= input <= max, and either min or max
   * otherwise.
   */
  function clampInt(min, max, input) {
    if (input < min) {
      return min;
    } else if (input > max) {
      return max;
    }
    return input;
  }

  /**
   * Clamps an integer between two floating-point numbers.
   *
   * @return input when min <= input <= max, and either min or max
   * otherwise.
   */
  function clampDouble(min, max, input) {
    if (input < min) {
      return min;
    } else if (input > max) {
      return max;
    }
    return input;
  }

  /**
   * Sanitizes a degree measure as a floating-point number.
   *
   * @return a degree measure between 0.0 (inclusive) and 360.0
   * (exclusive).
   */
  function sanitizeDegreesDouble(degrees) {
    degrees = degrees % 360.0;
    if (degrees < 0) {
      degrees = degrees + 360.0;
    }
    return degrees;
  }

  /**
   * Multiplies a 1x3 row vector with a 3x3 matrix.
   */
  function matrixMultiply(row, matrix) {
    var a = row[0] * matrix[0][0] + row[1] * matrix[0][1] + row[2] * matrix[0][2];
    var b = row[0] * matrix[1][0] + row[1] * matrix[1][1] + row[2] * matrix[1][2];
    var c = row[0] * matrix[2][0] + row[1] * matrix[2][1] + row[2] * matrix[2][2];
    return [a, b, c];
  }

  /**
   * Below lines are copied from @material/material-color-utilities.
   * Do not modify it.
   */

  /**
   * Color science utilities.
   *
   * Utility methods for color science constants and color space
   * conversions that aren't HCT or CAM16.
   */

  var SRGB_TO_XYZ = [[0.41233895, 0.35762064, 0.18051042], [0.2126, 0.7152, 0.0722], [0.01932141, 0.11916382, 0.95034478]];
  var XYZ_TO_SRGB = [[3.2413774792388685, -1.5376652402851851, -0.49885366846268053], [-0.9691452513005321, 1.8758853451067872, 0.04156585616912061], [0.05562093689691305, -0.20395524564742123, 1.0571799111220335]];
  var WHITE_POINT_D65 = [95.047, 100.0, 108.883];

  /**
   * Converts a color from RGB components to ARGB format.
   */
  function argbFromRgb(red, green, blue) {
    return (255 << 24 | (red & 255) << 16 | (green & 255) << 8 | blue & 255) >>> 0;
  }

  /**
   * Converts a color from linear RGB components to ARGB format.
   */
  function argbFromLinrgb(linrgb) {
    var r = delinearized(linrgb[0]);
    var g = delinearized(linrgb[1]);
    var b = delinearized(linrgb[2]);
    return argbFromRgb(r, g, b);
  }

  /**
   * Returns the alpha component of a color in ARGB format.
   */
  function alphaFromArgb(argb) {
    return argb >> 24 & 255;
  }

  /**
   * Returns the red component of a color in ARGB format.
   */
  function redFromArgb(argb) {
    return argb >> 16 & 255;
  }

  /**
   * Returns the green component of a color in ARGB format.
   */
  function greenFromArgb(argb) {
    return argb >> 8 & 255;
  }

  /**
   * Returns the blue component of a color in ARGB format.
   */
  function blueFromArgb(argb) {
    return argb & 255;
  }

  /**
   * Converts a color from ARGB to XYZ.
   */
  function argbFromXyz(x, y, z) {
    var matrix = XYZ_TO_SRGB;
    var linearR = matrix[0][0] * x + matrix[0][1] * y + matrix[0][2] * z;
    var linearG = matrix[1][0] * x + matrix[1][1] * y + matrix[1][2] * z;
    var linearB = matrix[2][0] * x + matrix[2][1] * y + matrix[2][2] * z;
    var r = delinearized(linearR);
    var g = delinearized(linearG);
    var b = delinearized(linearB);
    return argbFromRgb(r, g, b);
  }

  /**
   * Converts a color from XYZ to ARGB.
   */
  function xyzFromArgb(argb) {
    var r = linearized(redFromArgb(argb));
    var g = linearized(greenFromArgb(argb));
    var b = linearized(blueFromArgb(argb));
    return matrixMultiply([r, g, b], SRGB_TO_XYZ);
  }

  /**
   * Converts an L* value to an ARGB representation.
   *
   * @param lstar L* in L*a*b*
   * @return ARGB representation of grayscale color with lightness
   * matching L*
   */
  function argbFromLstar(lstar) {
    var y = yFromLstar(lstar);
    var component = delinearized(y);
    return argbFromRgb(component, component, component);
  }

  /**
   * Computes the L* value of a color in ARGB representation.
   *
   * @param argb ARGB representation of a color
   * @return L*, from L*a*b*, coordinate of the color
   */
  function lstarFromArgb(argb) {
    var y = xyzFromArgb(argb)[1];
    return 116.0 * labF(y / 100.0) - 16.0;
  }

  /**
   * Converts an L* value to a Y value.
   *
   * L* in L*a*b* and Y in XYZ measure the same quantity, luminance.
   *
   * L* measures perceptual luminance, a linear scale. Y in XYZ
   * measures relative luminance, a logarithmic scale.
   *
   * @param lstar L* in L*a*b*
   * @return Y in XYZ
   */
  function yFromLstar(lstar) {
    return 100.0 * labInvf((lstar + 16.0) / 116.0);
  }

  /**
   * Converts a Y value to an L* value.
   *
   * L* in L*a*b* and Y in XYZ measure the same quantity, luminance.
   *
   * L* measures perceptual luminance, a linear scale. Y in XYZ
   * measures relative luminance, a logarithmic scale.
   *
   * @param y Y in XYZ
   * @return L* in L*a*b*
   */
  function lstarFromY(y) {
    return labF(y / 100.0) * 116.0 - 16.0;
  }

  /**
   * Linearizes an RGB component.
   *
   * @param rgbComponent 0 <= rgb_component <= 255, represents R/G/B
   * channel
   * @return 0.0 <= output <= 100.0, color channel converted to
   * linear RGB space
   */
  function linearized(rgbComponent) {
    var normalized = rgbComponent / 255.0;
    if (normalized <= 0.040449936) {
      return normalized / 12.92 * 100.0;
    } else {
      return Math.pow((normalized + 0.055) / 1.055, 2.4) * 100.0;
    }
  }

  /**
   * Delinearizes an RGB component.
   *
   * @param rgbComponent 0.0 <= rgb_component <= 100.0, represents
   * linear R/G/B channel
   * @return 0 <= output <= 255, color channel converted to regular
   * RGB space
   */
  function delinearized(rgbComponent) {
    var normalized = rgbComponent / 100.0;
    var delinearized = 0.0;
    if (normalized <= 0.0031308) {
      delinearized = normalized * 12.92;
    } else {
      delinearized = 1.055 * Math.pow(normalized, 1.0 / 2.4) - 0.055;
    }
    return clampInt(0, 255, Math.round(delinearized * 255.0));
  }

  /**
   * Returns the standard white point; white on a sunny day.
   *
   * @return The white point
   */
  function whitePointD65() {
    return WHITE_POINT_D65;
  }

  /**
   * RGBA component
   *
   * @param r Red value should be between 0-255
   * @param g Green value should be between 0-255
   * @param b Blue value should be between 0-255
   * @param a Alpha value should be between 0-255
   */

  /**
   * Return RGBA from a given int32 color
   *
   * @param argb ARGB representation of a int32 color.
   * @return RGBA representation of a int32 color.
   */
  function rgbaFromArgb(argb) {
    var r = redFromArgb(argb);
    var g = greenFromArgb(argb);
    var b = blueFromArgb(argb);
    var a = alphaFromArgb(argb);
    return {
      r: r,
      g: g,
      b: b,
      a: a
    };
  }

  /**
   * Return int32 color from a given RGBA component
   *
   * @param rgba RGBA representation of a int32 color.
   * @returns ARGB representation of a int32 color.
   */
  function argbFromRgba(_ref) {
    var r = _ref.r,
      g = _ref.g,
      b = _ref.b,
      a = _ref.a;
    var rValue = clampComponent(r);
    var gValue = clampComponent(g);
    var bValue = clampComponent(b);
    var aValue = clampComponent(a);
    return aValue << 24 | rValue << 16 | gValue << 8 | bValue;
  }
  function clampComponent(value) {
    if (value < 0) {
      return 0;
    }
    if (value > 255) {
      return 255;
    }
    return value;
  }
  function labF(t) {
    var e = 216.0 / 24389.0;
    var kappa = 24389.0 / 27.0;
    if (t > e) {
      return Math.pow(t, 1.0 / 3.0);
    } else {
      return (kappa * t + 16) / 116;
    }
  }
  function labInvf(ft) {
    var e = 216.0 / 24389.0;
    var kappa = 24389.0 / 27.0;
    var ft3 = ft * ft * ft;
    if (ft3 > e) {
      return ft3;
    } else {
      return (116 * ft - 16) / kappa;
    }
  }

  var _ViewingConditions;

  /**
   * A color system built using CAM16 hue and chroma, and L* from
   * L*a*b*.
   *
   * Using L* creates a link between the color system, contrast, and thus
   * accessibility. Contrast ratio depends on relative luminance, or Y in the XYZ
   * color space. L*, or perceptual luminance can be calculated from Y.
   *
   * Unlike Y, L* is linear to human perception, allowing trivial creation of
   * accurate color tones.
   *
   * Unlike contrast ratio, measuring contrast in L* is linear, and simple to
   * calculate. A difference of 40 in HCT tone guarantees a contrast ratio >= 3.0,
   * and a difference of 50 guarantees a contrast ratio >= 4.5.
   */

  /**
   * HCT, hue, chroma, and tone. A color system that provides a perceptually
   * accurate color measurement system that can also accurately render what colors
   * will appear as in different lighting environments.
   */
  var Hct = /*#__PURE__*/function () {
    function Hct(argb) {
      _classCallCheck(this, Hct);
      this.argb = argb;
      var cam = Cam16.fromInt(argb);
      this.internalHue = cam.hue;
      this.internalChroma = cam.chroma;
      this.internalTone = lstarFromArgb(argb);
      this.argb = argb;
    }
    return _createClass(Hct, [{
      key: "toInt",
      value: function toInt() {
        return this.argb;
      }

      /**
       * A number, in degrees, representing ex. red, orange, yellow, etc.
       * Ranges from 0 <= hue < 360.
       */
    }, {
      key: "hue",
      get: function get() {
        return this.internalHue;
      }

      /**
       * @param newHue 0 <= newHue < 360; invalid values are corrected.
       * Chroma may decrease because chroma has a different maximum for any given
       * hue and tone.
       */,
      set: function set(newHue) {
        this.setInternalState(HctSolver.solveToInt(newHue, this.internalChroma, this.internalTone));
      }
    }, {
      key: "chroma",
      get: function get() {
        return this.internalChroma;
      }

      /**
       * @param newChroma 0 <= newChroma < ?
       * Chroma may decrease because chroma has a different maximum for any given
       * hue and tone.
       */,
      set: function set(newChroma) {
        this.setInternalState(HctSolver.solveToInt(this.internalHue, newChroma, this.internalTone));
      }

      /**
       * Lightness. Ranges from 0 to 100.
       */
    }, {
      key: "tone",
      get: function get() {
        return this.internalTone;
      }

      /**
       * @param newTone 0 <= newTone <= 100; invalid valids are corrected.
       * Chroma may decrease because chroma has a different maximum for any given
       * hue and tone.
       */,
      set: function set(newTone) {
        this.setInternalState(HctSolver.solveToInt(this.internalHue, this.internalChroma, newTone));
      }
    }, {
      key: "setInternalState",
      value: function setInternalState(argb) {
        var cam = Cam16.fromInt(argb);
        this.internalHue = cam.hue;
        this.internalChroma = cam.chroma;
        this.internalTone = lstarFromArgb(argb);
        this.argb = argb;
      }

      /**
       * Translates a color into different [ViewingConditions].
       *
       * Colors change appearance. They look different with lights on versus off,
       * the same color, as in hex code, on white looks different when on black.
       * This is called color relativity, most famously explicated by Josef Albers
       * in Interaction of Color.
       *
       * In color science, color appearance models can account for this and
       * calculate the appearance of a color in different settings. HCT is based on
       * CAM16, a color appearance model, and uses it to make these calculations.
       *
       * See [ViewingConditions.make] for parameters affecting color appearance.
       */
    }, {
      key: "inViewingConditions",
      value: function inViewingConditions(vc) {
        // 1. Use CAM16 to find XYZ coordinates of color in specified VC.
        var cam = Cam16.fromInt(this.toInt());
        var viewedInVc = cam.xyzInViewingConditions(vc);

        // 2. Create CAM16 of those XYZ coordinates in default VC.
        var recastInVc = Cam16.fromXyzInViewingConditions(viewedInVc[0], viewedInVc[1], viewedInVc[2], ViewingConditions.make());

        // 3. Create HCT from:
        // - CAM16 using default VC with XYZ coordinates in specified VC.
        // - L* converted from Y in XYZ coordinates in specified VC.
        var recastHct = Hct.from(recastInVc.hue, recastInVc.chroma, lstarFromY(viewedInVc[1]));
        return recastHct;
      }
    }], [{
      key: "from",
      value:
      /**
       * @param hue 0 <= hue < 360; invalid values are corrected.
       * @param chroma 0 <= chroma < ?; Informally, colorfulness. The color
       *     returned may be lower than the requested chroma. Chroma has a different
       *     maximum for any given hue and tone.
       * @param tone 0 <= tone <= 100; invalid values are corrected.
       * @return HCT representation of a color in default viewing conditions.
       */

      function from(hue, chroma, tone) {
        return new Hct(HctSolver.solveToInt(hue, chroma, tone));
      }

      /**
       * @param argb ARGB representation of a color.
       * @return HCT representation of a color in default viewing conditions
       */
    }, {
      key: "fromInt",
      value: function fromInt(argb) {
        return new Hct(argb);
      }
    }]);
  }();

  /**
   * CAM16, a color appearance model. Colors are not just defined by their hex
   * code, but rather, a hex code and viewing conditions.
   *
   * CAM16 instances also have coordinates in the CAM16-UCS space, called J*, a*,
   * b*, or jstar, astar, bstar in code. CAM16-UCS is included in the CAM16
   * specification, and should be used when measuring distances between colors.
   *
   * In traditional color spaces, a color can be identified solely by the
   * observer's measurement of the color. Color appearance models such as CAM16
   * also use information about the environment where the color was
   * observed, known as the viewing conditions.
   *
   * For example, white under the traditional assumption of a midday sun white
   * point is accurately measured as a slightly chromatic blue by CAM16. (roughly,
   * hue 203, chroma 3, lightness 100)
   */
  var Cam16 = /*#__PURE__*/function () {
    /**
     * All of the CAM16 dimensions can be calculated from 3 of the dimensions, in
     * the following combinations:
     *      -  {j or q} and {c, m, or s} and hue
     *      - jstar, astar, bstar
     * Prefer using a static method that constructs from 3 of those dimensions.
     * This constructor is intended for those methods to use to return all
     * possible dimensions.
     *
     * @param hue
     * @param chroma informally, colorfulness / color intensity. like saturation
     *     in HSL, except perceptually accurate.
     * @param j lightness
     * @param q brightness; ratio of lightness to white point's lightness
     * @param m colorfulness
     * @param s saturation; ratio of chroma to white point's chroma
     * @param jstar CAM16-UCS J coordinate
     * @param astar CAM16-UCS a coordinate
     * @param bstar CAM16-UCS b coordinate
     */
    function Cam16(hue, chroma, j, q, m, s, jstar, astar, bstar) {
      _classCallCheck(this, Cam16);
      this.hue = hue;
      this.chroma = chroma;
      this.j = j;
      this.q = q;
      this.m = m;
      this.s = s;
      this.jstar = jstar;
      this.astar = astar;
      this.bstar = bstar;
    }

    /**
     * CAM16 instances also have coordinates in the CAM16-UCS space, called J*,
     * a*, b*, or jstar, astar, bstar in code. CAM16-UCS is included in the CAM16
     * specification, and is used to measure distances between colors.
     */
    return _createClass(Cam16, [{
      key: "distance",
      value: function distance(other) {
        var dJ = this.jstar - other.jstar;
        var dA = this.astar - other.astar;
        var dB = this.bstar - other.bstar;
        var dEPrime = Math.sqrt(dJ * dJ + dA * dA + dB * dB);
        var dE = 1.41 * Math.pow(dEPrime, 0.63);
        return dE;
      }

      /**
       * @param argb ARGB representation of a color.
       * @return CAM16 color, assuming the color was viewed in default viewing
       *     conditions.
       */
    }, {
      key: "toInt",
      value:
      /**
       *  @return ARGB representation of color, assuming the color was viewed in
       *     default viewing conditions, which are near-identical to the default
       *     viewing conditions for sRGB.
       */
      function toInt() {
        return this.viewed(ViewingConditions.DEFAULT);
      }

      /**
       * @param viewingConditions Information about the environment where the color
       *     will be viewed.
       * @return ARGB representation of color
       */
    }, {
      key: "viewed",
      value: function viewed(viewingConditions) {
        var alpha = this.chroma === 0.0 || this.j === 0.0 ? 0.0 : this.chroma / Math.sqrt(this.j / 100.0);
        var t = Math.pow(alpha / Math.pow(1.64 - Math.pow(0.29, viewingConditions.n), 0.73), 1.0 / 0.9);
        var hRad = this.hue * Math.PI / 180.0;
        var eHue = 0.25 * (Math.cos(hRad + 2.0) + 3.8);
        var ac = viewingConditions.aw * Math.pow(this.j / 100.0, 1.0 / viewingConditions.c / viewingConditions.z);
        var p1 = eHue * (50000.0 / 13.0) * viewingConditions.nc * viewingConditions.ncb;
        var p2 = ac / viewingConditions.nbb;
        var hSin = Math.sin(hRad);
        var hCos = Math.cos(hRad);
        var gamma = 23.0 * (p2 + 0.305) * t / (23.0 * p1 + 11.0 * t * hCos + 108.0 * t * hSin);
        var a = gamma * hCos;
        var b = gamma * hSin;
        var rA = (460.0 * p2 + 451.0 * a + 288.0 * b) / 1403.0;
        var gA = (460.0 * p2 - 891.0 * a - 261.0 * b) / 1403.0;
        var bA = (460.0 * p2 - 220.0 * a - 6300.0 * b) / 1403.0;
        var rCBase = Math.max(0, 27.13 * Math.abs(rA) / (400.0 - Math.abs(rA)));
        var rC = signum(rA) * (100.0 / viewingConditions.fl) * Math.pow(rCBase, 1.0 / 0.42);
        var gCBase = Math.max(0, 27.13 * Math.abs(gA) / (400.0 - Math.abs(gA)));
        var gC = signum(gA) * (100.0 / viewingConditions.fl) * Math.pow(gCBase, 1.0 / 0.42);
        var bCBase = Math.max(0, 27.13 * Math.abs(bA) / (400.0 - Math.abs(bA)));
        var bC = signum(bA) * (100.0 / viewingConditions.fl) * Math.pow(bCBase, 1.0 / 0.42);
        var rF = rC / viewingConditions.rgbD[0];
        var gF = gC / viewingConditions.rgbD[1];
        var bF = bC / viewingConditions.rgbD[2];
        var x = 1.86206786 * rF - 1.01125463 * gF + 0.14918677 * bF;
        var y = 0.38752654 * rF + 0.62144744 * gF - 0.00897398 * bF;
        var z = -0.0158415 * rF - 0.03412294 * gF + 1.04996444 * bF;
        var argb = argbFromXyz(x, y, z);
        return argb;
      }

      /// Given color expressed in XYZ and viewed in [viewingConditions], convert to
      /// CAM16.
    }, {
      key: "xyzInViewingConditions",
      value:
      /// XYZ representation of CAM16 seen in [viewingConditions].
      function xyzInViewingConditions(viewingConditions) {
        var alpha = this.chroma === 0.0 || this.j === 0.0 ? 0.0 : this.chroma / Math.sqrt(this.j / 100.0);
        var t = Math.pow(alpha / Math.pow(1.64 - Math.pow(0.29, viewingConditions.n), 0.73), 1.0 / 0.9);
        var hRad = this.hue * Math.PI / 180.0;
        var eHue = 0.25 * (Math.cos(hRad + 2.0) + 3.8);
        var ac = viewingConditions.aw * Math.pow(this.j / 100.0, 1.0 / viewingConditions.c / viewingConditions.z);
        var p1 = eHue * (50000.0 / 13.0) * viewingConditions.nc * viewingConditions.ncb;
        var p2 = ac / viewingConditions.nbb;
        var hSin = Math.sin(hRad);
        var hCos = Math.cos(hRad);
        var gamma = 23.0 * (p2 + 0.305) * t / (23.0 * p1 + 11 * t * hCos + 108.0 * t * hSin);
        var a = gamma * hCos;
        var b = gamma * hSin;
        var rA = (460.0 * p2 + 451.0 * a + 288.0 * b) / 1403.0;
        var gA = (460.0 * p2 - 891.0 * a - 261.0 * b) / 1403.0;
        var bA = (460.0 * p2 - 220.0 * a - 6300.0 * b) / 1403.0;
        var rCBase = Math.max(0, 27.13 * Math.abs(rA) / (400.0 - Math.abs(rA)));
        var rC = signum(rA) * (100.0 / viewingConditions.fl) * Math.pow(rCBase, 1.0 / 0.42);
        var gCBase = Math.max(0, 27.13 * Math.abs(gA) / (400.0 - Math.abs(gA)));
        var gC = signum(gA) * (100.0 / viewingConditions.fl) * Math.pow(gCBase, 1.0 / 0.42);
        var bCBase = Math.max(0, 27.13 * Math.abs(bA) / (400.0 - Math.abs(bA)));
        var bC = signum(bA) * (100.0 / viewingConditions.fl) * Math.pow(bCBase, 1.0 / 0.42);
        var rF = rC / viewingConditions.rgbD[0];
        var gF = gC / viewingConditions.rgbD[1];
        var bF = bC / viewingConditions.rgbD[2];
        var x = 1.86206786 * rF - 1.01125463 * gF + 0.14918677 * bF;
        var y = 0.38752654 * rF + 0.62144744 * gF - 0.00897398 * bF;
        var z = -0.0158415 * rF - 0.03412294 * gF + 1.04996444 * bF;
        return [x, y, z];
      }
    }], [{
      key: "fromInt",
      value: function fromInt(argb) {
        return Cam16.fromIntInViewingConditions(argb, ViewingConditions.DEFAULT);
      }

      /**
       * @param argb ARGB representation of a color.
       * @param viewingConditions Information about the environment where the color
       *     was observed.
       * @return CAM16 color.
       */
    }, {
      key: "fromIntInViewingConditions",
      value: function fromIntInViewingConditions(argb, viewingConditions) {
        var red = (argb & 0x00ff0000) >> 16;
        var green = (argb & 0x0000ff00) >> 8;
        var blue = argb & 0x000000ff;
        var redL = linearized(red);
        var greenL = linearized(green);
        var blueL = linearized(blue);
        var x = 0.41233895 * redL + 0.35762064 * greenL + 0.18051042 * blueL;
        var y = 0.2126 * redL + 0.7152 * greenL + 0.0722 * blueL;
        var z = 0.01932141 * redL + 0.11916382 * greenL + 0.95034478 * blueL;
        var rC = 0.401288 * x + 0.650173 * y - 0.051461 * z;
        var gC = -0.250268 * x + 1.204414 * y + 0.045854 * z;
        var bC = -0.002079 * x + 0.048952 * y + 0.953127 * z;
        var rD = viewingConditions.rgbD[0] * rC;
        var gD = viewingConditions.rgbD[1] * gC;
        var bD = viewingConditions.rgbD[2] * bC;
        var rAF = Math.pow(viewingConditions.fl * Math.abs(rD) / 100.0, 0.42);
        var gAF = Math.pow(viewingConditions.fl * Math.abs(gD) / 100.0, 0.42);
        var bAF = Math.pow(viewingConditions.fl * Math.abs(bD) / 100.0, 0.42);
        var rA = signum(rD) * 400.0 * rAF / (rAF + 27.13);
        var gA = signum(gD) * 400.0 * gAF / (gAF + 27.13);
        var bA = signum(bD) * 400.0 * bAF / (bAF + 27.13);
        var a = (11.0 * rA + -12.0 * gA + bA) / 11.0;
        var b = (rA + gA - 2.0 * bA) / 9.0;
        var u = (20.0 * rA + 20.0 * gA + 21.0 * bA) / 20.0;
        var p2 = (40.0 * rA + 20.0 * gA + bA) / 20.0;
        var atan2 = Math.atan2(b, a);
        var atanDegrees = atan2 * 180.0 / Math.PI;
        var hue = atanDegrees < 0 ? atanDegrees + 360.0 : atanDegrees >= 360 ? atanDegrees - 360.0 : atanDegrees;
        var hueRadians = hue * Math.PI / 180.0;
        var ac = p2 * viewingConditions.nbb;
        var j = 100.0 * Math.pow(ac / viewingConditions.aw, viewingConditions.c * viewingConditions.z);
        var q = 4.0 / viewingConditions.c * Math.sqrt(j / 100.0) * (viewingConditions.aw + 4.0) * viewingConditions.fLRoot;
        var huePrime = hue < 20.14 ? hue + 360 : hue;
        var eHue = 0.25 * (Math.cos(huePrime * Math.PI / 180.0 + 2.0) + 3.8);
        var p1 = 50000.0 / 13.0 * eHue * viewingConditions.nc * viewingConditions.ncb;
        var t = p1 * Math.sqrt(a * a + b * b) / (u + 0.305);
        var alpha = Math.pow(t, 0.9) * Math.pow(1.64 - Math.pow(0.29, viewingConditions.n), 0.73);
        var c = alpha * Math.sqrt(j / 100.0);
        var m = c * viewingConditions.fLRoot;
        var s = 50.0 * Math.sqrt(alpha * viewingConditions.c / (viewingConditions.aw + 4.0));
        var jstar = (1.0 + 100.0 * 0.007) * j / (1.0 + 0.007 * j);
        var mstar = 1.0 / 0.0228 * Math.log(1.0 + 0.0228 * m);
        var astar = mstar * Math.cos(hueRadians);
        var bstar = mstar * Math.sin(hueRadians);
        return new Cam16(hue, c, j, q, m, s, jstar, astar, bstar);
      }

      /**
       * @param j CAM16 lightness
       * @param c CAM16 chroma
       * @param h CAM16 hue
       */
    }, {
      key: "fromJch",
      value: function fromJch(j, c, h) {
        return Cam16.fromJchInViewingConditions(j, c, h, ViewingConditions.DEFAULT);
      }

      /**
       * @param j CAM16 lightness
       * @param c CAM16 chroma
       * @param h CAM16 hue
       * @param viewingConditions Information about the environment where the color
       *     was observed.
       */
    }, {
      key: "fromJchInViewingConditions",
      value: function fromJchInViewingConditions(j, c, h, viewingConditions) {
        var q = 4.0 / viewingConditions.c * Math.sqrt(j / 100.0) * (viewingConditions.aw + 4.0) * viewingConditions.fLRoot;
        var m = c * viewingConditions.fLRoot;
        var alpha = c / Math.sqrt(j / 100.0);
        var s = 50.0 * Math.sqrt(alpha * viewingConditions.c / (viewingConditions.aw + 4.0));
        var hueRadians = h * Math.PI / 180.0;
        var jstar = (1.0 + 100.0 * 0.007) * j / (1.0 + 0.007 * j);
        var mstar = 1.0 / 0.0228 * Math.log(1.0 + 0.0228 * m);
        var astar = mstar * Math.cos(hueRadians);
        var bstar = mstar * Math.sin(hueRadians);
        return new Cam16(h, c, j, q, m, s, jstar, astar, bstar);
      }

      /**
       * @param jstar CAM16-UCS lightness.
       * @param astar CAM16-UCS a dimension. Like a* in L*a*b*, it is a Cartesian
       *     coordinate on the Y axis.
       * @param bstar CAM16-UCS b dimension. Like a* in L*a*b*, it is a Cartesian
       *     coordinate on the X axis.
       */
    }, {
      key: "fromUcs",
      value: function fromUcs(jstar, astar, bstar) {
        return Cam16.fromUcsInViewingConditions(jstar, astar, bstar, ViewingConditions.DEFAULT);
      }

      /**
       * @param jstar CAM16-UCS lightness.
       * @param astar CAM16-UCS a dimension. Like a* in L*a*b*, it is a Cartesian
       *     coordinate on the Y axis.
       * @param bstar CAM16-UCS b dimension. Like a* in L*a*b*, it is a Cartesian
       *     coordinate on the X axis.
       * @param viewingConditions Information about the environment where the color
       *     was observed.
       */
    }, {
      key: "fromUcsInViewingConditions",
      value: function fromUcsInViewingConditions(jstar, astar, bstar, viewingConditions) {
        var a = astar;
        var b = bstar;
        var m = Math.sqrt(a * a + b * b);
        var M = (Math.exp(m * 0.0228) - 1.0) / 0.0228;
        var c = M / viewingConditions.fLRoot;
        var h = Math.atan2(b, a) * (180.0 / Math.PI);
        if (h < 0.0) {
          h += 360.0;
        }
        var j = jstar / (1 - (jstar - 100) * 0.007);
        return Cam16.fromJchInViewingConditions(j, c, h, viewingConditions);
      }
    }, {
      key: "fromXyzInViewingConditions",
      value: function fromXyzInViewingConditions(x, y, z, viewingConditions) {
        // Transform XYZ to 'cone'/'rgb' responses

        var rC = 0.401288 * x + 0.650173 * y - 0.051461 * z;
        var gC = -0.250268 * x + 1.204414 * y + 0.045854 * z;
        var bC = -0.002079 * x + 0.048952 * y + 0.953127 * z;

        // Discount illuminant
        var rD = viewingConditions.rgbD[0] * rC;
        var gD = viewingConditions.rgbD[1] * gC;
        var bD = viewingConditions.rgbD[2] * bC;

        // chromatic adaptation
        var rAF = Math.pow(viewingConditions.fl * Math.abs(rD) / 100.0, 0.42);
        var gAF = Math.pow(viewingConditions.fl * Math.abs(gD) / 100.0, 0.42);
        var bAF = Math.pow(viewingConditions.fl * Math.abs(bD) / 100.0, 0.42);
        var rA = signum(rD) * 400.0 * rAF / (rAF + 27.13);
        var gA = signum(gD) * 400.0 * gAF / (gAF + 27.13);
        var bA = signum(bD) * 400.0 * bAF / (bAF + 27.13);

        // redness-greenness
        var a = (11.0 * rA + -12.0 * gA + bA) / 11.0;
        // yellowness-blueness
        var b = (rA + gA - 2.0 * bA) / 9.0;

        // auxiliary components
        var u = (20.0 * rA + 20.0 * gA + 21.0 * bA) / 20.0;
        var p2 = (40.0 * rA + 20.0 * gA + bA) / 20.0;

        // hue
        var atan2 = Math.atan2(b, a);
        var atanDegrees = atan2 * 180.0 / Math.PI;
        var hue = atanDegrees < 0 ? atanDegrees + 360.0 : atanDegrees >= 360 ? atanDegrees - 360 : atanDegrees;
        var hueRadians = hue * Math.PI / 180.0;

        // achromatic response to color
        var ac = p2 * viewingConditions.nbb;

        // CAM16 lightness and brightness
        var J = 100.0 * Math.pow(ac / viewingConditions.aw, viewingConditions.c * viewingConditions.z);
        var Q = 4.0 / viewingConditions.c * Math.sqrt(J / 100.0) * (viewingConditions.aw + 4.0) * viewingConditions.fLRoot;
        var huePrime = hue < 20.14 ? hue + 360 : hue;
        var eHue = 1.0 / 4.0 * (Math.cos(huePrime * Math.PI / 180.0 + 2.0) + 3.8);
        var p1 = 50000.0 / 13.0 * eHue * viewingConditions.nc * viewingConditions.ncb;
        var t = p1 * Math.sqrt(a * a + b * b) / (u + 0.305);
        var alpha = Math.pow(t, 0.9) * Math.pow(1.64 - Math.pow(0.29, viewingConditions.n), 0.73);
        // CAM16 chroma, colorfulness, chroma
        var C = alpha * Math.sqrt(J / 100.0);
        var M = C * viewingConditions.fLRoot;
        var s = 50.0 * Math.sqrt(alpha * viewingConditions.c / (viewingConditions.aw + 4.0));

        // CAM16-UCS components
        var jstar = (1.0 + 100.0 * 0.007) * J / (1.0 + 0.007 * J);
        var mstar = Math.log(1.0 + 0.0228 * M) / 0.0228;
        var astar = mstar * Math.cos(hueRadians);
        var bstar = mstar * Math.sin(hueRadians);
        return new Cam16(hue, C, J, Q, M, s, jstar, astar, bstar);
      }
    }]);
  }(); // This file is automatically generated. Do not modify it.
  // material_color_utilities is designed to have a consistent API across
  // platforms and modular components that can be moved around easily. Using a
  // class as a namespace facilitates this.
  //
  // tslint:disable:class-as-namespace
  /**
   * A class that solves the HCT equation.
   */
  var HctSolver = /*#__PURE__*/function () {
    function HctSolver() {
      _classCallCheck(this, HctSolver);
    }
    return _createClass(HctSolver, null, [{
      key: "sanitizeRadians",
      value:
      /**
       * Sanitizes a small enough angle in radians.
       *
       * @param angle An angle in radians; must not deviate too much
       * from 0.
       * @return A coterminal angle between 0 and 2pi.
       */
      function sanitizeRadians(angle) {
        return (angle + Math.PI * 8) % (Math.PI * 2);
      }

      /**
       * Delinearizes an RGB component, returning a floating-point
       * number.
       *
       * @param rgbComponent 0.0 <= rgb_component <= 100.0, represents
       * linear R/G/B channel
       * @return 0.0 <= output <= 255.0, color channel converted to
       * regular RGB space
       */
    }, {
      key: "trueDelinearized",
      value: function trueDelinearized(rgbComponent) {
        var normalized = rgbComponent / 100.0;
        var delinearized = 0.0;
        if (normalized <= 0.0031308) {
          delinearized = normalized * 12.92;
        } else {
          delinearized = 1.055 * Math.pow(normalized, 1.0 / 2.4) - 0.055;
        }
        return delinearized * 255.0;
      }
    }, {
      key: "chromaticAdaptation",
      value: function chromaticAdaptation(component) {
        var af = Math.pow(Math.abs(component), 0.42);
        return signum(component) * 400.0 * af / (af + 27.13);
      }

      /**
       * Returns the hue of a linear RGB color in CAM16.
       *
       * @param linrgb The linear RGB coordinates of a color.
       * @return The hue of the color in CAM16, in radians.
       */
    }, {
      key: "hueOf",
      value: function hueOf(linrgb) {
        var scaledDiscount = matrixMultiply(linrgb, HctSolver.SCALED_DISCOUNT_FROM_LINRGB);
        var rA = HctSolver.chromaticAdaptation(scaledDiscount[0]);
        var gA = HctSolver.chromaticAdaptation(scaledDiscount[1]);
        var bA = HctSolver.chromaticAdaptation(scaledDiscount[2]);
        // redness-greenness
        var a = (11.0 * rA + -12.0 * gA + bA) / 11.0;
        // yellowness-blueness
        var b = (rA + gA - 2.0 * bA) / 9.0;
        return Math.atan2(b, a);
      }
    }, {
      key: "areInCyclicOrder",
      value: function areInCyclicOrder(a, b, c) {
        var deltaAB = HctSolver.sanitizeRadians(b - a);
        var deltaAC = HctSolver.sanitizeRadians(c - a);
        return deltaAB < deltaAC;
      }

      /**
       * Solves the lerp equation.
       *
       * @param source The starting number.
       * @param mid The number in the middle.
       * @param target The ending number.
       * @return A number t such that lerp(source, target, t) = mid.
       */
    }, {
      key: "intercept",
      value: function intercept(source, mid, target) {
        return (mid - source) / (target - source);
      }
    }, {
      key: "lerpPoint",
      value: function lerpPoint(source, t, target) {
        return [source[0] + (target[0] - source[0]) * t, source[1] + (target[1] - source[1]) * t, source[2] + (target[2] - source[2]) * t];
      }

      /**
       * Intersects a segment with a plane.
       *
       * @param source The coordinates of point A.
       * @param coordinate The R-, G-, or B-coordinate of the plane.
       * @param target The coordinates of point B.
       * @param axis The axis the plane is perpendicular with. (0: R, 1:
       * G, 2: B)
       * @return The intersection point of the segment AB with the plane
       * R=coordinate, G=coordinate, or B=coordinate
       */
    }, {
      key: "setCoordinate",
      value: function setCoordinate(source, coordinate, target, axis) {
        var t = HctSolver.intercept(source[axis], coordinate, target[axis]);
        return HctSolver.lerpPoint(source, t, target);
      }
    }, {
      key: "isBounded",
      value: function isBounded(x) {
        return 0.0 <= x && x <= 100.0;
      }

      /**
       * Returns the nth possible vertex of the polygonal intersection.
       *
       * @param y The Y value of the plane.
       * @param n The zero-based index of the point. 0 <= n <= 11.
       * @return The nth possible vertex of the polygonal intersection
       * of the y plane and the RGB cube, in linear RGB coordinates, if
       * it exists. If this possible vertex lies outside of the cube,
       * [-1.0, -1.0, -1.0] is returned.
       */
    }, {
      key: "nthVertex",
      value: function nthVertex(y, n) {
        var kR = HctSolver.Y_FROM_LINRGB[0];
        var kG = HctSolver.Y_FROM_LINRGB[1];
        var kB = HctSolver.Y_FROM_LINRGB[2];
        var coordA = n % 4 <= 1 ? 0.0 : 100.0;
        var coordB = n % 2 === 0 ? 0.0 : 100.0;
        if (n < 4) {
          var g = coordA;
          var b = coordB;
          var r = (y - g * kG - b * kB) / kR;
          if (HctSolver.isBounded(r)) {
            return [r, g, b];
          } else {
            return [-1.0, -1.0, -1.0];
          }
        } else if (n < 8) {
          var _b = coordA;
          var _r = coordB;
          var _g = (y - _r * kR - _b * kB) / kG;
          if (HctSolver.isBounded(_g)) {
            return [_r, _g, _b];
          } else {
            return [-1.0, -1.0, -1.0];
          }
        } else {
          var _r2 = coordA;
          var _g2 = coordB;
          var _b2 = (y - _r2 * kR - _g2 * kG) / kB;
          if (HctSolver.isBounded(_b2)) {
            return [_r2, _g2, _b2];
          } else {
            return [-1.0, -1.0, -1.0];
          }
        }
      }

      /**
       * Finds the segment containing the desired color.
       *
       * @param y The Y value of the color.
       * @param targetHue The hue of the color.
       * @return A list of two sets of linear RGB coordinates, each
       * corresponding to an endpoint of the segment containing the
       * desired color.
       */
    }, {
      key: "bisectToSegment",
      value: function bisectToSegment(y, targetHue) {
        var left = [-1.0, -1.0, -1.0];
        var right = left;
        var leftHue = 0.0;
        var rightHue = 0.0;
        var initialized = false;
        var uncut = true;
        for (var _n = 0; _n < 12; _n++) {
          var mid = HctSolver.nthVertex(y, _n);
          if (mid[0] < 0) {
            continue;
          }
          var midHue = HctSolver.hueOf(mid);
          if (!initialized) {
            left = mid;
            right = mid;
            leftHue = midHue;
            rightHue = midHue;
            initialized = true;
            continue;
          }
          if (uncut || HctSolver.areInCyclicOrder(leftHue, midHue, rightHue)) {
            uncut = false;
            if (HctSolver.areInCyclicOrder(leftHue, targetHue, midHue)) {
              right = mid;
              rightHue = midHue;
            } else {
              left = mid;
              leftHue = midHue;
            }
          }
        }
        return [left, right];
      }
    }, {
      key: "midpoint",
      value: function midpoint(a, b) {
        return [(a[0] + b[0]) / 2, (a[1] + b[1]) / 2, (a[2] + b[2]) / 2];
      }
    }, {
      key: "criticalPlaneBelow",
      value: function criticalPlaneBelow(x) {
        return Math.floor(x - 0.5);
      }
    }, {
      key: "criticalPlaneAbove",
      value: function criticalPlaneAbove(x) {
        return Math.ceil(x - 0.5);
      }

      /**
       * Finds a color with the given Y and hue on the boundary of the
       * cube.
       *
       * @param y The Y value of the color.
       * @param targetHue The hue of the color.
       * @return The desired color, in linear RGB coordinates.
       */
    }, {
      key: "bisectToLimit",
      value: function bisectToLimit(y, targetHue) {
        var segment = HctSolver.bisectToSegment(y, targetHue);
        var left = segment[0];
        var leftHue = HctSolver.hueOf(left);
        var right = segment[1];
        for (var axis = 0; axis < 3; axis++) {
          if (left[axis] !== right[axis]) {
            var lPlane = -1;
            var rPlane = 255;
            if (left[axis] < right[axis]) {
              lPlane = HctSolver.criticalPlaneBelow(HctSolver.trueDelinearized(left[axis]));
              rPlane = HctSolver.criticalPlaneAbove(HctSolver.trueDelinearized(right[axis]));
            } else {
              lPlane = HctSolver.criticalPlaneAbove(HctSolver.trueDelinearized(left[axis]));
              rPlane = HctSolver.criticalPlaneBelow(HctSolver.trueDelinearized(right[axis]));
            }
            for (var i = 0; i < 8; i++) {
              if (Math.abs(rPlane - lPlane) <= 1) {
                break;
              } else {
                var mPlane = Math.floor((lPlane + rPlane) / 2.0);
                var midPlaneCoordinate = HctSolver.CRITICAL_PLANES[mPlane];
                var mid = HctSolver.setCoordinate(left, midPlaneCoordinate, right, axis);
                var midHue = HctSolver.hueOf(mid);
                if (HctSolver.areInCyclicOrder(leftHue, targetHue, midHue)) {
                  right = mid;
                  rPlane = mPlane;
                } else {
                  left = mid;
                  leftHue = midHue;
                  lPlane = mPlane;
                }
              }
            }
          }
        }
        return HctSolver.midpoint(left, right);
      }
    }, {
      key: "inverseChromaticAdaptation",
      value: function inverseChromaticAdaptation(adapted) {
        var adaptedAbs = Math.abs(adapted);
        var base = Math.max(0, 27.13 * adaptedAbs / (400.0 - adaptedAbs));
        return signum(adapted) * Math.pow(base, 1.0 / 0.42);
      }

      /**
       * Finds a color with the given hue, chroma, and Y.
       *
       * @param hueRadians The desired hue in radians.
       * @param chroma The desired chroma.
       * @param y The desired Y.
       * @return The desired color as a hexadecimal integer, if found; 0
       * otherwise.
       */
    }, {
      key: "findResultByJ",
      value: function findResultByJ(hueRadians, chroma, y) {
        // Initial estimate of j.
        var j = Math.sqrt(y) * 11.0;
        // ===========================================================
        // Operations inlined from Cam16 to avoid repeated calculation
        // ===========================================================
        var viewingConditions = ViewingConditions.DEFAULT;
        var tInnerCoeff = 1 / Math.pow(1.64 - Math.pow(0.29, viewingConditions.n), 0.73);
        var eHue = 0.25 * (Math.cos(hueRadians + 2.0) + 3.8);
        var p1 = eHue * (50000.0 / 13.0) * viewingConditions.nc * viewingConditions.ncb;
        var hSin = Math.sin(hueRadians);
        var hCos = Math.cos(hueRadians);
        for (var iterationRound = 0; iterationRound < 5; iterationRound++) {
          // ===========================================================
          // Operations inlined from Cam16 to avoid repeated calculation
          // ===========================================================
          var jNormalized = j / 100.0;
          var alpha = chroma === 0.0 || j === 0.0 ? 0.0 : chroma / Math.sqrt(jNormalized);
          var t = Math.pow(alpha * tInnerCoeff, 1.0 / 0.9);
          var ac = viewingConditions.aw * Math.pow(jNormalized, 1.0 / viewingConditions.c / viewingConditions.z);
          var p2 = ac / viewingConditions.nbb;
          var gamma = 23.0 * (p2 + 0.305) * t / (23.0 * p1 + 11 * t * hCos + 108.0 * t * hSin);
          var a = gamma * hCos;
          var b = gamma * hSin;
          var rA = (460.0 * p2 + 451.0 * a + 288.0 * b) / 1403.0;
          var gA = (460.0 * p2 - 891.0 * a - 261.0 * b) / 1403.0;
          var bA = (460.0 * p2 - 220.0 * a - 6300.0 * b) / 1403.0;
          var rCScaled = HctSolver.inverseChromaticAdaptation(rA);
          var gCScaled = HctSolver.inverseChromaticAdaptation(gA);
          var bCScaled = HctSolver.inverseChromaticAdaptation(bA);
          var linrgb = matrixMultiply([rCScaled, gCScaled, bCScaled], HctSolver.LINRGB_FROM_SCALED_DISCOUNT);
          // ===========================================================
          // Operations inlined from Cam16 to avoid repeated calculation
          // ===========================================================
          if (linrgb[0] < 0 || linrgb[1] < 0 || linrgb[2] < 0) {
            return 0;
          }
          var kR = HctSolver.Y_FROM_LINRGB[0];
          var kG = HctSolver.Y_FROM_LINRGB[1];
          var kB = HctSolver.Y_FROM_LINRGB[2];
          var fnj = kR * linrgb[0] + kG * linrgb[1] + kB * linrgb[2];
          if (fnj <= 0) {
            return 0;
          }
          if (iterationRound === 4 || Math.abs(fnj - y) < 0.002) {
            if (linrgb[0] > 100.01 || linrgb[1] > 100.01 || linrgb[2] > 100.01) {
              return 0;
            }
            return argbFromLinrgb(linrgb);
          }
          // Iterates with Newton method,
          // Using 2 * fn(j) / j as the approximation of fn'(j)
          j = j - (fnj - y) * j / (2 * fnj);
        }
        return 0;
      }

      /**
       * Finds an sRGB color with the given hue, chroma, and L*, if
       * possible.
       *
       * @param hueDegrees The desired hue, in degrees.
       * @param chroma The desired chroma.
       * @param lstar The desired L*.
       * @return A hexadecimal representing the sRGB color. The color
       * has sufficiently close hue, chroma, and L* to the desired
       * values, if possible; otherwise, the hue and L* will be
       * sufficiently close, and chroma will be maximized.
       */
    }, {
      key: "solveToInt",
      value: function solveToInt(hueDegrees, chroma, lstar) {
        if (chroma < 0.0001 || lstar < 0.0001 || lstar > 99.9999) {
          return argbFromLstar(lstar);
        }
        hueDegrees = sanitizeDegreesDouble(hueDegrees);
        var hueRadians = hueDegrees / 180 * Math.PI;
        var y = yFromLstar(lstar);
        var exactAnswer = HctSolver.findResultByJ(hueRadians, chroma, y);
        if (exactAnswer !== 0) {
          return exactAnswer;
        }
        var linrgb = HctSolver.bisectToLimit(y, hueRadians);
        return argbFromLinrgb(linrgb);
      }

      /**
       * Finds an sRGB color with the given hue, chroma, and L*, if
       * possible.
       *
       * @param hueDegrees The desired hue, in degrees.
       * @param chroma The desired chroma.
       * @param lstar The desired L*.
       * @return An CAM16 object representing the sRGB color. The color
       * has sufficiently close hue, chroma, and L* to the desired
       * values, if possible; otherwise, the hue and L* will be
       * sufficiently close, and chroma will be maximized.
       */
    }, {
      key: "solveToCam",
      value: function solveToCam(hueDegrees, chroma, lstar) {
        return Cam16.fromInt(HctSolver.solveToInt(hueDegrees, chroma, lstar));
      }
    }]);
  }();
  _defineProperty(HctSolver, "SCALED_DISCOUNT_FROM_LINRGB", [[0.001200833568784504, 0.002389694492170889, 0.0002795742885861124], [0.0005891086651375999, 0.0029785502573438758, 0.0003270666104008398], [0.00010146692491640572, 0.0005364214359186694, 0.0032979401770712076]]);
  _defineProperty(HctSolver, "LINRGB_FROM_SCALED_DISCOUNT", [[1373.2198709594231, -1100.4251190754821, -7.278681089101213], [-271.815969077903, 559.6580465940733, -32.46047482791194], [1.9622899599665666, -57.173814538844006, 308.7233197812385]]);
  _defineProperty(HctSolver, "Y_FROM_LINRGB", [0.2126, 0.7152, 0.0722]);
  _defineProperty(HctSolver, "CRITICAL_PLANES", [0.015176349177441876, 0.045529047532325624, 0.07588174588720938, 0.10623444424209313, 0.13658714259697685, 0.16693984095186062, 0.19729253930674434, 0.2276452376616281, 0.2579979360165119, 0.28835063437139563, 0.3188300904430532, 0.350925934958123, 0.3848314933096426, 0.42057480301049466, 0.458183274052838, 0.4976837250274023, 0.5391024159806381, 0.5824650784040898, 0.6277969426914107, 0.6751227633498623, 0.7244668422128921, 0.775853049866786, 0.829304845476233, 0.8848452951698498, 0.942497089126609, 1.0022825574869039, 1.0642236851973577, 1.1283421258858297, 1.1946592148522128, 1.2631959812511864, 1.3339731595349034, 1.407011200216447, 1.4823302800086415, 1.5599503113873272, 1.6398909516233677, 1.7221716113234105, 1.8068114625156377, 1.8938294463134073, 1.9832442801866852, 2.075074464868551, 2.1693382909216234, 2.2660538449872063, 2.36523901573795, 2.4669114995532007, 2.5710888059345764, 2.6777882626779785, 2.7870270208169257, 2.898822059350997, 3.0131901897720907, 3.1301480604002863, 3.2497121605402226, 3.3718988244681087, 3.4967242352587946, 3.624204428461639, 3.754355295633311, 3.887192587735158, 4.022731918402185, 4.160988767090289, 4.301978482107941, 4.445716283538092, 4.592217266055746, 4.741496401646282, 4.893568542229298, 5.048448422192488, 5.20615066083972, 5.3666897647573375, 5.5300801301023865, 5.696336044816294, 5.865471690767354, 6.037501145825082, 6.212438385869475, 6.390297286737924, 6.571091626112461, 6.7548350853498045, 6.941541251256611, 7.131223617812143, 7.323895587840543, 7.5195704746346665, 7.7182615035334345, 7.919981813454504, 8.124744458384042, 8.332562408825165, 8.543448553206703, 8.757415699253682, 8.974476575321063, 9.194643831691977, 9.417930041841839, 9.644347703669503, 9.873909240696694, 10.106627003236781, 10.342513269534024, 10.58158024687427, 10.8238400726681, 11.069304815507364, 11.317986476196008, 11.569896988756009, 11.825048221409341, 12.083451977536606, 12.345119996613247, 12.610063955123938, 12.878295467455942, 13.149826086772048, 13.42466730586372, 13.702830557985108, 13.984327217668513, 14.269168601521828, 14.55736596900856, 14.848930523210871, 15.143873411576273, 15.44220572664832, 15.743938506781891, 16.04908273684337, 16.35764934889634, 16.66964922287304, 16.985093187232053, 17.30399201960269, 17.62635644741625, 17.95219714852476, 18.281524751807332, 18.614349837764564, 18.95068293910138, 19.290534541298456, 19.633915083172692, 19.98083495742689, 20.331304511189067, 20.685334046541502, 21.042933821039977, 21.404114048223256, 21.76888489811322, 22.137256497705877, 22.50923893145328, 22.884842241736916, 23.264076429332462, 23.6469514538663, 24.033477234264016, 24.42366364919083, 24.817520537484558, 25.21505769858089, 25.61628489293138, 26.021211842414342, 26.429848230738664, 26.842203703840827, 27.258287870275353, 27.678110301598522, 28.10168053274597, 28.529008062403893, 28.96010235337422, 29.39497283293396, 29.83362889318845, 30.276079891419332, 30.722335150426627, 31.172403958865512, 31.62629557157785, 32.08401920991837, 32.54558406207592, 33.010999283389665, 33.4802739966603, 33.953417292456834, 34.430438229418264, 34.911345834551085, 35.39614910352207, 35.88485700094671, 36.37747846067349, 36.87402238606382, 37.37449765026789, 37.87891309649659, 38.38727753828926, 38.89959975977785, 39.41588851594697, 39.93615253289054, 40.460400508064545, 40.98864111053629, 41.520882981230194, 42.05713473317016, 42.597404951718396, 43.141702194811224, 43.6900349931913, 44.24241185063697, 44.798841244188324, 45.35933162437017, 45.92389141541209, 46.49252901546552, 47.065252796817916, 47.64207110610409, 48.22299226451468, 48.808024568002054, 49.3971762874833, 49.9904556690408, 50.587870934119984, 51.189430279724725, 51.79514187861014, 52.40501387947288, 53.0190544071392, 53.637271562750364, 54.259673423945976, 54.88626804504493, 55.517063457223934, 56.15206766869424, 56.79128866487574, 57.43473440856916, 58.08241284012621, 58.734331877617365, 59.39049941699807, 60.05092333227251, 60.715611475655585, 61.38457167773311, 62.057811747619894, 62.7353394731159, 63.417162620860914, 64.10328893648692, 64.79372614476921, 65.48848194977529, 66.18756403501224, 66.89098006357258, 67.59873767827808, 68.31084450182222, 69.02730813691093, 69.74813616640164, 70.47333615344107, 71.20291564160104, 71.93688215501312, 72.67524319850172, 73.41800625771542, 74.16517879925733, 74.9167682708136, 75.67278210128072, 76.43322770089146, 77.1981124613393, 77.96744375590167, 78.74122893956174, 79.51947534912904, 80.30219030335869, 81.08938110306934, 81.88105503125999, 82.67721935322541, 83.4778813166706, 84.28304815182372, 85.09272707154808, 85.90692527145302, 86.72564993000343, 87.54890820862819, 88.3767072518277, 89.2090541872801, 90.04595612594655, 90.88742016217518, 91.73345337380438, 92.58406282226491, 93.43925555268066, 94.29903859396902, 95.16341895893969, 96.03240364439274, 96.9059996312159, 97.78421388448044, 98.6670533535366, 99.55452497210776]);
  var ViewingConditions = /*#__PURE__*/function () {
    /**
     * Parameters are intermediate values of the CAM16 conversion process. Their
     * names are shorthand for technical color science terminology, this class
     * would not benefit from documenting them individually. A brief overview
     * is available in the CAM16 specification, and a complete overview requires
     * a color science textbook, such as Fairchild's Color Appearance Models.
     */
    function ViewingConditions(n, aw, nbb, ncb, c, nc, rgbD, fl, fLRoot, z) {
      _classCallCheck(this, ViewingConditions);
      this.n = n;
      this.aw = aw;
      this.nbb = nbb;
      this.ncb = ncb;
      this.c = c;
      this.nc = nc;
      this.rgbD = rgbD;
      this.fl = fl;
      this.fLRoot = fLRoot;
      this.z = z;
    }
    return _createClass(ViewingConditions, null, [{
      key: "make",
      value:
      /**
       * Create ViewingConditions from a simple, physically relevant, set of
       * parameters.
       *
       * @param whitePoint White point, measured in the XYZ color space.
       *     default = D65, or sunny day afternoon
       * @param adaptingLuminance The luminance of the adapting field. Informally,
       *     how bright it is in the room where the color is viewed. Can be
       *     calculated from lux by multiplying lux by 0.0586. default = 11.72,
       *     or 200 lux.
       * @param backgroundLstar The lightness of the area surrounding the color.
       *     measured by L* in L*a*b*. default = 50.0
       * @param surround A general description of the lighting surrounding the
       *     color. 0 is pitch dark, like watching a movie in a theater. 1.0 is a
       *     dimly light room, like watching TV at home at night. 2.0 means there
       *     is no difference between the lighting on the color and around it.
       *     default = 2.0
       * @param discountingIlluminant Whether the eye accounts for the tint of the
       *     ambient lighting, such as knowing an apple is still red in green light.
       *     default = false, the eye does not perform this process on
       *       self-luminous objects like displays.
       */
      function make() {
        var whitePoint = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : whitePointD65();
        var adaptingLuminance = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 200.0 / Math.PI * yFromLstar(50.0) / 100.0;
        var backgroundLstar = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 50.0;
        var surround = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 2.0;
        var discountingIlluminant = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;
        var xyz = whitePoint;
        var rW = xyz[0] * 0.401288 + xyz[1] * 0.650173 + xyz[2] * -0.051461;
        var gW = xyz[0] * -0.250268 + xyz[1] * 1.204414 + xyz[2] * 0.045854;
        var bW = xyz[0] * -0.002079 + xyz[1] * 0.048952 + xyz[2] * 0.953127;
        var f = 0.8 + surround / 10.0;
        var c = f >= 0.9 ? lerp(0.59, 0.69, (f - 0.9) * 10.0) : lerp(0.525, 0.59, (f - 0.8) * 10.0);
        var d = discountingIlluminant ? 1.0 : f * (1.0 - 1.0 / 3.6 * Math.exp((-adaptingLuminance - 42.0) / 92.0));
        d = d > 1.0 ? 1.0 : d < 0.0 ? 0.0 : d;
        var nc = f;
        var rgbD = [d * (100.0 / rW) + 1.0 - d, d * (100.0 / gW) + 1.0 - d, d * (100.0 / bW) + 1.0 - d];
        var k = 1.0 / (5.0 * adaptingLuminance + 1.0);
        var k4 = k * k * k * k;
        var k4F = 1.0 - k4;
        var fl = k4 * adaptingLuminance + 0.1 * k4F * k4F * Math.cbrt(5.0 * adaptingLuminance);
        var n = yFromLstar(backgroundLstar) / whitePoint[1];
        var z = 1.48 + Math.sqrt(n);
        var nbb = 0.725 / Math.pow(n, 0.2);
        var ncb = nbb;
        var rgbAFactors = [Math.pow(fl * rgbD[0] * rW / 100.0, 0.42), Math.pow(fl * rgbD[1] * gW / 100.0, 0.42), Math.pow(fl * rgbD[2] * bW / 100.0, 0.42)];
        var rgbA = [400.0 * rgbAFactors[0] / (rgbAFactors[0] + 27.13), 400.0 * rgbAFactors[1] / (rgbAFactors[1] + 27.13), 400.0 * rgbAFactors[2] / (rgbAFactors[2] + 27.13)];
        var aw = (2.0 * rgbA[0] + rgbA[1] + 0.05 * rgbA[2]) * nbb;
        return new ViewingConditions(n, aw, nbb, ncb, c, nc, rgbD, fl, Math.pow(fl, 0.25), z);
      }
    }]);
  }();
  _ViewingConditions = ViewingConditions;
  /**
   * sRGB-like viewing conditions.
   */
  _defineProperty(ViewingConditions, "DEFAULT", _ViewingConditions.make());

  /**
   * Utility methods for calculating contrast given two colors, or calculating a
   * color given one color and a contrast ratio.
   *
   * Contrast ratio is calculated using XYZ's Y. When linearized to match human
   * perception, Y becomes HCT's tone and L*a*b*'s' L*. Informally, this is the
   * lightness of a color.
   *
   * Methods refer to tone, T in the the HCT color space.
   * Tone is equivalent to L* in the L*a*b* color space, or L in the LCH color
   * space.
   */
  var Contrast = /*#__PURE__*/function () {
    function Contrast() {
      _classCallCheck(this, Contrast);
    }
    return _createClass(Contrast, null, [{
      key: "ratioOfTones",
      value:
      /**
       * Returns a contrast ratio, which ranges from 1 to 21.
       *
       * @param toneA Tone between 0 and 100. Values outside will be clamped.
       * @param toneB Tone between 0 and 100. Values outside will be clamped.
       */
      function ratioOfTones(toneA, toneB) {
        toneA = clampDouble(0.0, 100.0, toneA);
        toneB = clampDouble(0.0, 100.0, toneB);
        return Contrast.ratioOfYs(yFromLstar(toneA), yFromLstar(toneB));
      }
    }, {
      key: "ratioOfYs",
      value: function ratioOfYs(y1, y2) {
        var lighter = y1 > y2 ? y1 : y2;
        var darker = lighter === y2 ? y1 : y2;
        return (lighter + 5.0) / (darker + 5.0);
      }

      /**
       * Returns a tone >= tone parameter that ensures ratio parameter.
       * Return value is between 0 and 100.
       * Returns -1 if ratio cannot be achieved with tone parameter.
       *
       * @param tone Tone return value must contrast with.
       * Range is 0 to 100. Invalid values will result in -1 being returned.
       * @param ratio Contrast ratio of return value and tone.
       * Range is 1 to 21, invalid values have undefined behavior.
       */
    }, {
      key: "lighter",
      value: function lighter(tone, ratio) {
        if (tone < 0.0 || tone > 100.0) {
          return -1.0;
        }
        var darkY = yFromLstar(tone);
        var lightY = ratio * (darkY + 5.0) - 5.0;
        var realContrast = Contrast.ratioOfYs(lightY, darkY);
        var delta = Math.abs(realContrast - ratio);
        if (realContrast < ratio && delta > 0.04) {
          return -1;
        }

        // Ensure gamut mapping, which requires a 'range' on tone, will still result
        // the correct ratio by darkening slightly.
        var returnValue = lstarFromY(lightY) + 0.4;
        if (returnValue < 0 || returnValue > 100) {
          return -1;
        }
        return returnValue;
      }

      /**
       * Returns a tone <= tone parameter that ensures ratio parameter.
       * Return value is between 0 and 100.
       * Returns -1 if ratio cannot be achieved with tone parameter.
       *
       * @param tone Tone return value must contrast with.
       * Range is 0 to 100. Invalid values will result in -1 being returned.
       * @param ratio Contrast ratio of return value and tone.
       * Range is 1 to 21, invalid values have undefined behavior.
       */
    }, {
      key: "darker",
      value: function darker(tone, ratio) {
        if (tone < 0.0 || tone > 100.0) {
          return -1.0;
        }
        var lightY = yFromLstar(tone);
        var darkY = (lightY + 5.0) / ratio - 5.0;
        var realContrast = Contrast.ratioOfYs(lightY, darkY);
        var delta = Math.abs(realContrast - ratio);
        if (realContrast < ratio && delta > 0.04) {
          return -1;
        }

        // Ensure gamut mapping, which requires a 'range' on tone, will still result
        // the correct ratio by darkening slightly.
        var returnValue = lstarFromY(darkY) - 0.4;
        if (returnValue < 0 || returnValue > 100) {
          return -1;
        }
        return returnValue;
      }

      /**
       * Returns a tone >= tone parameter that ensures ratio parameter.
       * Return value is between 0 and 100.
       * Returns 100 if ratio cannot be achieved with tone parameter.
       *
       * This method is unsafe because the returned value is guaranteed to be in
       * bounds for tone, i.e. between 0 and 100. However, that value may not reach
       * the ratio with tone. For example, there is no color lighter than T100.
       *
       * @param tone Tone return value must contrast with.
       * Range is 0 to 100. Invalid values will result in 100 being returned.
       * @param ratio Desired contrast ratio of return value and tone parameter.
       * Range is 1 to 21, invalid values have undefined behavior.
       */
    }, {
      key: "lighterUnsafe",
      value: function lighterUnsafe(tone, ratio) {
        var lighterSafe = Contrast.lighter(tone, ratio);
        return lighterSafe < 0.0 ? 100.0 : lighterSafe;
      }

      /**
       * Returns a tone >= tone parameter that ensures ratio parameter.
       * Return value is between 0 and 100.
       * Returns 100 if ratio cannot be achieved with tone parameter.
       *
       * This method is unsafe because the returned value is guaranteed to be in
       * bounds for tone, i.e. between 0 and 100. However, that value may not reach
       * the [ratio with [tone]. For example, there is no color darker than T0.
       *
       * @param tone Tone return value must contrast with.
       * Range is 0 to 100. Invalid values will result in 0 being returned.
       * @param ratio Desired contrast ratio of return value and tone parameter.
       * Range is 1 to 21, invalid values have undefined behavior.
       */
    }, {
      key: "darkerUnsafe",
      value: function darkerUnsafe(tone, ratio) {
        var darkerSafe = Contrast.darker(tone, ratio);
        return darkerSafe < 0.0 ? 0.0 : darkerSafe;
      }
    }]);
  }();

  function ownKeys(e, r) {
    var t = Object.keys(e);
    if (Object.getOwnPropertySymbols) {
      var o = Object.getOwnPropertySymbols(e);
      r && (o = o.filter(function (r) {
        return Object.getOwnPropertyDescriptor(e, r).enumerable;
      })), t.push.apply(t, o);
    }
    return t;
  }
  function _objectSpread(e) {
    for (var r = 1; r < arguments.length; r++) {
      var t = null != arguments[r] ? arguments[r] : {};
      r % 2 ? ownKeys(Object(t), !0).forEach(function (r) {
        _defineProperty(e, r, t[r]);
      }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) {
        Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r));
      });
    }
    return e;
  }
  var lowLuminanceContrastRatios = [1.12, 1.33, 2.03, 2.73, 3.33, 4.27, 5.2, 6.62, 12.46, 14.25];
  var highLuminanceContrastRatios = [1.08, 1.24, 1.55, 1.99, 2.45, 3.34, 4.64, 6.1, 10.19, 12.6];
  var getClosestColorIndex = function getClosestColorIndex(themeRamp, brandColor) {
    // Iterate over themeRamp and find whichever color is closest to brandColor
    var closestColorIndex = 0;
    var closestColorDistance = null;
    themeRamp.forEach(function (value, index) {
      var distance = deltaE(hexToRgb(value), hexToRgb(brandColor));
      if (closestColorDistance === null || distance < closestColorDistance) {
        closestColorIndex = index;
        closestColorDistance = distance;
      }
    });
    return closestColorIndex;
  };
  var generateColors = function generateColors(brandColor) {
    // Determine luminance
    var HSLBrandColorHue = hexToHSL(brandColor)[0];
    var baseRgb = HSLToRGB(HSLBrandColorHue, 100, 60);
    var isLowLuminance = relativeLuminanceW3C(baseRgb[0], baseRgb[1], baseRgb[2]) < 0.4;
    // Choose right palette
    var themeRatios = isLowLuminance ? lowLuminanceContrastRatios : highLuminanceContrastRatios;
    var brandRgba = hexToRgbA(brandColor);
    var hctColor = Hct.fromInt(argbFromRgba({
      r: brandRgba[0],
      g: brandRgba[1],
      b: brandRgba[2],
      a: brandRgba[3]
    }));
    var themeRamp = themeRatios.map(function (contrast) {
      var rgbaColor = rgbaFromArgb(Hct.from(hctColor.hue, hctColor.chroma, Contrast.darker(100, contrast) + 0.25 // Material's utils provide an offset
      ).toInt());
      return rgbToHex(rgbaColor.r, rgbaColor.g, rgbaColor.b);
    });
    var closestColorIndex = getClosestColorIndex(themeRamp, brandColor);

    // Replace closet color with brandColor
    var updatedThemeRamp = _toConsumableArray(themeRamp);
    updatedThemeRamp[closestColorIndex] = brandColor;
    return {
      ramp: updatedThemeRamp,
      // add the replaced color into the result
      replacedColor: themeRamp[closestColorIndex]
    };
  };

  /**
   * Return the interaction tokens for a color, given its ramp position and the number of
   * needed interaction states. Use higher-indexed colors (i.e. darker colors) if possible;
   * if there's not enough room to shift up for the required number of interaction tokens,
   * it goes as far as it can, then returns lighter colors lower down the ramp instead.
   *
   * Returns an array of the resulting colors
   */
  function getInteractionStates(rampPosition, number, colors) {
    var result = [];
    for (var i = 1; i <= number; i++) {
      if (rampPosition + i < colors.length) {
        result.push(rampPosition + i);
      } else {
        result.push(rampPosition - (i - (colors.length - 1 - rampPosition)));
      }
    }
    return result;
  }
  var generateTokenMap = function generateTokenMap(brandColor, mode, themeRamp) {
    var _generateColors = generateColors(brandColor),
      ramp = _generateColors.ramp,
      replacedColor = _generateColors.replacedColor;
    var colors = themeRamp || ramp;
    var closestColorIndex = getClosestColorIndex(colors, brandColor);
    var customThemeTokenMapLight = {};
    var customThemeTokenMapDark = {};
    var inputContrast = getContrastRatio(brandColor, '#FFFFFF');
    // Branch based on brandColor's contrast against white
    if (inputContrast >= 4.5) {
      /**
       * Generate interaction tokens for
       * - color.background.brand.bold
       * - color.background.selected.bold
       */
      var _getInteractionStates = getInteractionStates(closestColorIndex, 2, colors),
        _getInteractionStates2 = _slicedToArray(_getInteractionStates, 2),
        brandBoldSelectedHoveredIndex = _getInteractionStates2[0],
        brandBoldSelectedPressedIndex = _getInteractionStates2[1];
      var brandTextIndex = closestColorIndex;
      if (inputContrast < 5.4 && inputContrast >= 4.8 && closestColorIndex === 6) {
        // Use the one-level darker closest color (X800) for color.text.brand
        // and color.link to avoid contrast breaches
        brandTextIndex = closestColorIndex + 1;
      }

      /**
       * Generate interaction token for color.link:
       * If inputted color replaces X1000
       * - Pressed = X900
       *
       * If inputted color replaces X700-X900
       * - Shift one 1 step darker
       */
      var _getInteractionStates3 = getInteractionStates(brandTextIndex, 1, colors),
        _getInteractionStates4 = _slicedToArray(_getInteractionStates3, 1),
        linkPressed = _getInteractionStates4[0];
      customThemeTokenMapLight = {
        'color.text.brand': brandTextIndex,
        'color.icon.brand': closestColorIndex,
        'color.background.brand.subtlest': 0,
        'color.background.brand.subtlest.hovered': 1,
        'color.background.brand.subtlest.pressed': 2,
        'color.background.brand.bold': closestColorIndex,
        'color.background.brand.bold.hovered': brandBoldSelectedHoveredIndex,
        'color.background.brand.bold.pressed': brandBoldSelectedPressedIndex,
        'color.background.brand.boldest': 9,
        'color.background.brand.boldest.hovered': 8,
        'color.background.brand.boldest.pressed': 7,
        'color.border.brand': closestColorIndex,
        'color.text.selected': brandTextIndex,
        'color.icon.selected': closestColorIndex,
        'color.background.selected.bold': closestColorIndex,
        'color.background.selected.bold.hovered': brandBoldSelectedHoveredIndex,
        'color.background.selected.bold.pressed': brandBoldSelectedPressedIndex,
        'color.border.selected': closestColorIndex,
        'color.link': brandTextIndex,
        'color.link.pressed': linkPressed,
        'color.chart.brand': 5,
        'color.chart.brand.hovered': 6,
        'color.background.selected': 0,
        'color.background.selected.hovered': 1,
        'color.background.selected.pressed': 2
      };
    } else {
      var brandBackgroundIndex = 6;
      if (inputContrast < 4.5 && inputContrast >= 4 && closestColorIndex === 6) {
        // Use the generated closest color instead of the input brand color for
        // color.background.selected.bold and color.background.brand.bold
        // to avoid contrast breaches
        brandBackgroundIndex = replacedColor;
      }
      customThemeTokenMapLight = {
        'color.background.brand.subtlest': 0,
        'color.background.brand.subtlest.hovered': 1,
        'color.background.brand.subtlest.pressed': 2,
        'color.background.brand.bold': brandBackgroundIndex,
        'color.background.brand.bold.hovered': 7,
        'color.background.brand.bold.pressed': 8,
        'color.background.brand.boldest': 9,
        'color.background.brand.boldest.hovered': 8,
        'color.background.brand.boldest.pressed': 7,
        'color.border.brand': 6,
        'color.background.selected.bold': brandBackgroundIndex,
        'color.background.selected.bold.hovered': 7,
        'color.background.selected.bold.pressed': 8,
        'color.text.brand': 6,
        'color.icon.brand': 6,
        'color.chart.brand': 5,
        'color.chart.brand.hovered': 6,
        'color.text.selected': 6,
        'color.icon.selected': 6,
        'color.border.selected': 6,
        'color.background.selected': 0,
        'color.background.selected.hovered': 1,
        'color.background.selected.pressed': 2,
        'color.link': 6,
        'color.link.pressed': 7
      };
    }
    if (mode === 'light') {
      return {
        light: customThemeTokenMapLight
      };
    }

    /**
     * Generate dark mode values using rule of symmetry
     */
    Object.entries(customThemeTokenMapLight).forEach(function (_ref) {
      var _ref2 = _slicedToArray(_ref, 2),
        key = _ref2[0],
        value = _ref2[1];
      customThemeTokenMapDark[key] = 9 - (typeof value === 'string' ? closestColorIndex : value);
    });

    /**
     * If the input brand color < 4.5, and it meets 4.5 contrast again inverse text color
     * in dark mode, shift color.background.brand.bold to the brand color
     */
    if (inputContrast < 4.5) {
      var inverseTextColor = rawTokensDark['color.text.inverse'];
      if (getContrastRatio(inverseTextColor, brandColor) >= 4.5 && closestColorIndex >= 2) {
        customThemeTokenMapDark['color.background.brand.bold'] = closestColorIndex;
        customThemeTokenMapDark['color.background.brand.bold.hovered'] = closestColorIndex - 1;
        customThemeTokenMapDark['color.background.brand.bold.pressed'] = closestColorIndex - 2;
      }
    }
    if (mode === 'dark') {
      return {
        dark: customThemeTokenMapDark
      };
    }
    return {
      light: customThemeTokenMapLight,
      dark: customThemeTokenMapDark
    };
  };
  var generateTokenMapWithContrastCheck = function generateTokenMapWithContrastCheck(brandColor, mode, themeRamp) {
    var colors = themeRamp || generateColors(brandColor).ramp;
    var tokenMaps = generateTokenMap(brandColor, mode, colors);
    var result = {};
    Object.entries(tokenMaps).forEach(function (_ref3) {
      var _ref4 = _slicedToArray(_ref3, 2),
        mode = _ref4[0],
        map = _ref4[1];
      if (mode === 'light' || mode === 'dark') {
        result[mode] = _objectSpread(_objectSpread({}, map), additionalContrastChecker({
          customThemeTokenMap: map,
          mode: mode,
          themeRamp: colors
        }));
      }
    });
    return result;
  };

  var CUSTOM_STYLE_ELEMENTS_SIZE_THRESHOLD = 10;

  /**
   *
   * @param themeSchema The schema of available themes
   * @returns a string with the CSS for the custom theme
   */
  /**
   * Takes a color mode and custom branding options, and returns an array of objects for use in applying custom styles to the document head.
   * Only supplies the color themes necessary for initial render, based on the current themeState. I.e. if in light mode, dark mode themes are not returned.
   *
   * @param {Object<string, string>} themeState The themes and color mode that should be applied.
   * @param {string} themeState.colorMode Determines which color theme is applied
   * @param {Object} themeState.UNSAFE_themeOptions The custom branding options to be used for custom theme generation
   *
   * @returns An object array, containing theme IDs, data-attributes to attach to the theme, and the theme CSS.
   * If an error is encountered while loading themes, the themes array will be empty.
   */
  function getCustomThemeStyles(themeState) {
    var _themeState$UNSAFE_th;
    var brandColor = themeState === null || themeState === void 0 || (_themeState$UNSAFE_th = themeState.UNSAFE_themeOptions) === null || _themeState$UNSAFE_th === void 0 ? void 0 : _themeState$UNSAFE_th.brandColor;
    var mode = (themeState === null || themeState === void 0 ? void 0 : themeState.colorMode) || themeStateDefaults['colorMode'];
    var optionString = JSON.stringify(themeState === null || themeState === void 0 ? void 0 : themeState.UNSAFE_themeOptions);
    var uniqueId = hash(optionString);
    var themeRamp = generateColors(brandColor).ramp;

    // outputs object to generate to CSS from
    var themes = [];
    var tokenMaps = generateTokenMapWithContrastCheck(brandColor, mode, themeRamp);
    if ((mode === 'light' || mode === 'auto') && tokenMaps.light) {
      // Light mode theming
      themes.push({
        id: 'light',
        attrs: {
          'data-theme': 'light',
          'data-custom-theme': uniqueId
        },
        css: "\nhtml[".concat(CUSTOM_THEME_ATTRIBUTE, "=\"").concat(uniqueId, "\"][").concat(COLOR_MODE_ATTRIBUTE, "=\"light\"][data-theme~=\"light:light\"] {\n  /* Branded tokens */\n    ").concat(reduceTokenMap(tokenMaps.light, themeRamp), "\n}")
      });
    }
    if ((mode === 'dark' || mode === 'auto') && tokenMaps.dark) {
      // Dark mode theming
      themes.push({
        id: 'dark',
        attrs: {
          'data-theme': 'dark',
          'data-custom-theme': uniqueId
        },
        css: "\nhtml[".concat(CUSTOM_THEME_ATTRIBUTE, "=\"").concat(uniqueId, "\"][").concat(COLOR_MODE_ATTRIBUTE, "=\"dark\"][data-theme~=\"dark:dark\"] {\n  /* Branded tokens */\n    ").concat(reduceTokenMap(tokenMaps.dark, themeRamp), "\n}")
      });
    }
    return themes;
  }
  function loadAndAppendCustomThemeCss(themeState) {
    var themes = getCustomThemeStyles(themeState);
    limitSizeOfCustomStyleElements(CUSTOM_STYLE_ELEMENTS_SIZE_THRESHOLD);
    themes.map(function (theme) {
      var styleTag = document.createElement('style');
      document.head.appendChild(styleTag);
      styleTag.dataset.theme = theme.attrs['data-theme'];
      styleTag.dataset.customTheme = theme.attrs['data-custom-theme'];
      styleTag.textContent = theme.css;
    });
  }

  var customTheme = /*#__PURE__*/Object.freeze({
    __proto__: null,
    CUSTOM_STYLE_ELEMENTS_SIZE_THRESHOLD: CUSTOM_STYLE_ELEMENTS_SIZE_THRESHOLD,
    getCustomThemeStyles: getCustomThemeStyles,
    loadAndAppendCustomThemeCss: loadAndAppendCustomThemeCss
  });

  return combined;

})(jsClient, clientCore);
